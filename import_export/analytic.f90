!> A module for generating analytic expressions for
!! geometry, profiles and sources based on parameters
!! given in the namelists.
module analytic

  implicit none

contains

  subroutine import_geometry_analytic

    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    !    use constants_trin, only: pi => dpi
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: rad_out, aspr, rgeo, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: testing, dfac, nrad, bmag, vtfac, ntdelt
    use trinity_input, only: n_ion_spec
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trin_power, only: alpha_power
    use trinity_input, only: equal_ion_temps
    use miller_geometry, only: init_miller_geo, get_Iflxfnc
    use chease_ecom, only: update_geometry, get_chease_profs
    use nteqns_arrays
    use norms, only: amin_ref, aref, psitor_a
    use iterdb, only: import_geometry_iterdb, import_initial_profiles_iterdb

    implicit none

    if (proc0) then

      ! in case of analytic specification of geometry, use r/a as flux label
      aref = rgeo/aspr
      amin_ref = aref
      if (phia_in > 0.0) then
        psitor_a = phia_in
      else
        psitor_a = bmag*pi*aref**2
      end if

      !bmag_grid now set in initialise_bmag_grid
      !bmag_grid = bmag
      bmag_old_grid = bmag
      bmag_axis = bmag
      bunit_grid = bmag
      btori_grid = bmag * (aspr*amin_ref)

      ! analytic specification of safety factor and magnetic shear profiles
      qval_grid = q0 + alph_q*(qa-q0)*rad_grid**2 + (1.-alph_q)*(qa-q0)*rad_grid**6 &
        + sh0*sqrt(rad_grid)*exp(-rad_grid**2)
      shat_grid = ( 2.0*(qa-q0)*rad_grid**2*(alph_q+3.*rad_grid**4*(1.-alph_q)) &
        + 0.5*sh0*sqrt(rad_grid)*exp(-rad_grid**2)*(1.-4.*rad_grid**2) ) / qval_grid
      kappa_grid = kap0 + alph_k*(ka-kap0)*rad_grid**2 + (1.-alph_k)*(ka-kap0)*rad_grid**6
      kapprim_grid = 2.*alph_k*(ka-kap0)*rad_grid + 6.*(1.-alph_k)*(ka-kap0)*rad_grid**5
      ! TMP -- MAB
      delta_grid = 0.0
      deltprim_grid = 0.0
      ! assume shifted circles...perhaps to be overwritten later by values from flux code
      area_grid = 4*aspr*rad_grid*pi**2
      grho_grid = 1.0
      drhotordrho_grid = 1.0
      drmindrho_grid = 1.0
      shift_grid = 0.0
      rmajor_grid = aspr
      rhotor_grid = rad_grid
      rmin_grid = rad_grid

    end if
  end subroutine import_geometry_analytic
  subroutine import_initial_profiles_analytic
    use mp_trin, only: broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: rad_out, aspr, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: flux_option_switch, flux_option_test1
    use trinity_input, only: flux_option_test2, flux_option_test3
    use trinity_input, only: testing, dfac, nrad, vtfac, ntdelt
    use trinity_input, only: z_ion
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: init_file, init_time, geo_file, geo_time
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: rlti, rlte, tiedge, teedge, amin_in, ledge, rll
    use trinity_input, only: rlni, rlne, niedge, needge
    use trinity_input, only: densfac, tifac, tefac
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, te_fixed
    use nteqns_arrays
    implicit none


    !real, intent (in out) :: time
    !integer, intent (out) :: itstep_tot, neval_tot


    if (testing) then

      select case (flux_option_switch)

      case (flux_option_test1)
        if (evolve_density) then
          dens_grid = exp(-0.25*spread(rad_grid**2,2,nspec)/dfac)
          if (evolve_temperature) then
            temp_grid = 1.0
            if (evolve_flow) then
              mom_grid = exp(-0.25*rad_grid**2/dfac)
            else
              mom_grid = 1.0
            end if
          else
            ! need to check to see if should be minus sign below
            temp_grid = exp(0.25*spread(rad_grid**2,2,nspec)/dfac)
            mom_grid = 1.0
          end if
        else if (evolve_temperature) then
          dens_grid = 1.0
          temp_grid = exp(-0.25*spread(rad_grid**2,2,nspec)/dfac)
          mom_grid = 1.0
        else if (evolve_flow) then
          dens_grid = 1.0
          temp_grid = 1.0
          mom_grid = exp(-0.25*rad_grid**2/dfac)
        else
          write (*,*) 'no profiles selected to evolve.'
          write (*,*) 'please retry setting evolve_density, '
          write (*,*) 'evolve_temperature, or evolve_flow to .true.'
          write (*,*) 'aborting.'
          call abort_mp
        end if

      case (flux_option_test2)

        if (evolve_density) then
          if (evolve_temperature) then
            dens_grid = 1.0
            temp_grid(:,1) = 0.5*(tiedge+teedge)*exp(rlte*(rad_out-rad_grid)/aspr)
            temp_grid(:,2) = 0.5*(tiedge+teedge)*exp(rlti*(rad_out-rad_grid)/aspr)
            if (n_ion_spec > 1) temp_grid(:,3:) = spread(temp_grid(:,2),2,n_ion_spec-1)
            mom_grid = 1.0
          else
            write (*,*) "Cannot test temperature equilibration if you don't evolve temperature"
            write (*,*) "aborting."
            call abort_mp
          end if
        else if (te_equal_ti .or. te_fixed) then
          write (*,*) "Cannot test temperature equilibration with only 1 species evolved"
          write (*,*) "aborting."
          call abort_mp
        else
          dens_grid = 1.0
          temp_grid(:,1) = 0.5*(tiedge+teedge)*exp(rlte*(rad_out-rad_grid)/aspr)
          temp_grid(:,2) = 0.5*(tiedge+teedge)*exp(rlti*(rad_out-rad_grid)/aspr)
          if (n_ion_spec > 1) temp_grid(:,3:) = spread(temp_grid(:,2),2,n_ion_spec-1)
          mom_grid = 1.0
        end if

      case (flux_option_test3)

        dens_grid = 1.0
        temp_grid = spread(exp(-rad_grid),2,nspec)
        mom_grid = 1.0

      end select

    else

      temp_grid(:,1) = teedge*exp(rlte*(rad_out-rad_grid)/aspr)
      temp_grid(:,2) = tiedge*exp(rlti*(rad_out-rad_grid)/aspr)
      dens_grid(:,1) = needge*exp(rlne*(rad_out-rad_grid)/aspr)
      if (n_ion_spec > 1) then
        dens_grid(:,2) = niedge*exp(rlni*(rad_out-rad_grid)/aspr)
        dens_grid(:,3:) = 0.
        dens_grid(:,3) = (dens_grid(:,1)-z_ion(1)*dens_grid(:,2))/z_ion(2)
        temp_grid(:,3:) = spread(temp_grid(:,2),2,n_ion_spec-1)
      else
        dens_grid(:,2) = dens_grid(:,1)/z_ion(1)
      end if
      mom_grid = ledge*exp(rll*(rad_out-rad_grid)/aspr)
      zeff_grid = 1.0
    end if

    !Now set below
    !zeff_grid = zeff
    omega_grid = mom_grid*inv_mominertia_fnc(dens_grid,spread(aspr,1,nrad))

  end subroutine import_initial_profiles_analytic
  subroutine import_sources_analytic
    use mp_trin, only: broadcast, abort_mp
    use file_utils_trin, only: open_output_file, flush_output_file
    use interp, only: get_cc
    !use trinity_input, only: m_ion
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: te_equal_ti, te_fixed
    use trinity_input, only: nrad, vtfac
    use trinity_input, only: powerin, densin, psig, nsig
    use trinity_input, only: include_alphas, include_radiation, write_pwr_profs
    use trinity_input, only: source_file, source_time, radin, temp_equil
    use trinity_input, only: pioq
    use trinity_input, only: nbi_mult, dens_mult
    use trinity_input, only: icrh_mult
    use trinity_input, only: ech_mult
    use trinity_input, only: lh_mult
    use trinity_input, only: ibw_mult
    use trinity_input, only: dwn_mult
    use trin_power, only: alpha_power, radiated_power
    use nteqns_arrays
    use norms, only: aref, srcnorm, lsrcnorm
    use iterdb, only: import_sources_iterdb
    use trinity_input, only: z_ion

    implicit none

    real :: nsrc0, aux_int
    !real, save :: srcnorm, nsrcnorm, lsrcnorm
    !logical :: get_sources_first = .true.





    ! might need to fix this -- MAB

    ! analytic specification of auxiliary heat source
    call volint (aref**3*exp(-(rad_grid-radin)**2/(2*psig**2))/psig, aux_int)
    !src_aux = spread(powerin,1,nrad)*srcnorm*1.e6*exp(-(spread(rad_grid,2,nspec)-radin)**2/(2*psig**2))/(psig*aux_int)
    src_heat_unspecified = spread(powerin,1,nrad)*1.e6*exp(-(spread(rad_grid,2,nspec)-radin)**2/(2*psig**2))/(psig*aux_int)

    ! analytic specification of density source needs to be improved
    nsrc0 = densin
    !src_dens = nsrc0*exp(-spread(rad_grid**2,2,nspec)/(2*nsig**2))/nsig
    src_part_unspecified = nsrc0*exp(-spread(rad_grid**2,2,nspec)/(2*nsig**2))/nsig
    src_part_unspecified(:,1) = sum(spread(z_ion(:n_ion_spec),1,nrad)*src_part_unspecified(:,2:n_ion_spec),2)
    ! need to add in specification for momentum source -- MAB
    !src_mom_unspecified = src_aux(:,2)*pioq
    src_mom_unspecified = src_heat_unspecified(:,2)*srcnorm/lsrcnorm*pioq

  end subroutine import_sources_analytic
end module analytic
