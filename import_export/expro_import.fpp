!> A module for reading in equilibrium information
!! using the GA expro library.
module expro_import
  implicit none
  logical :: expro_init = .false.
  real, dimension (:), allocatable, save :: rho
contains
  subroutine import_geometry_expro
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    !    use constants_trin, only: pi => dpi
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: rad_out, aspr, rgeo, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: testing, dfac, nrad, bmag, vtfac, ntdelt
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: rlti, rlte, tiedge, teedge, amin_in, ledge, rll
    use trinity_input, only: rlni, rlne, niedge, needge
    use trinity_input, only: zeff, densfac, tifac, tefac
    use trinity_input, only: te_equal_ti, te_fixed
    use trin_power, only: alpha_power
    use trinity_input, only: equal_ion_temps
    use miller_geometry, only: init_miller_geo, get_Iflxfnc
    use nteqns_arrays
    use norms, only: amin_ref, aref, psitor_a
    implicit none

    real :: amin
    
    if (proc0) then
       if (.not. expro_init) then
          call init_expro_io (rad_grid, rhotor_grid, rmin_grid, drhotordrho_grid, drmindrho_grid, &
               psitor_a, rgeo, amin, bmag, zeff)
          expro_init = .true.
       end if
       call expro_io_geo (rad_grid, qval_grid, shat_grid, kappa_grid, kapprim_grid, &
            rmajor_grid, shift_grid, delta_grid, deltprim_grid, area_grid, grho_grid, &
            bunit_grid, btori_grid, bmag_axis)
       
        ! TODO: convert derivatives from d/drmin to d/drho
        ! At the moment we abort with fluxlabel_option_torflux so at least
        ! it will not silently give wrong answers :-)

        ! ONLY USE WHEN BENCHMARKING WITH TGYRO
        !          grho_grid = 1.0



        !bmag_grid now set in initialise_bmag_grid

        bmag_old_grid = bunit_grid



        ! TODO... does this mean expro requires fluxlabel_option_aminor ? 
        aspr = rgeo/amin
        aref = amin
        amin_ref = amin


        ! convert from area in units of [m]^2 to units of a^2
        area_grid = area_grid/aref**2
      end if
  end subroutine import_geometry_expro
  subroutine import_initial_profiles_expro
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    !    use constants_trin, only: pi => dpi
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: rad_out, rgeo, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: testing, dfac, nrad, bmag, vtfac, ntdelt
    use trinity_input, only: z_ion
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: init_option_expro
    use trinity_input, only: init_file, init_time, geo_file, geo_time
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: rlti, rlte, tiedge, teedge, amin_in, ledge, rll
    use trinity_input, only: rlni, rlne, niedge, needge
    use trinity_input, only: zeff, densfac, tifac, tefac
    use trinity_input, only: te_equal_ti, te_fixed
    use miller_geometry, only: init_miller_geo, get_Iflxfnc
    use nteqns_arrays
    use norms, only: psitor_a
    implicit none

    integer :: is
    real :: amin
    if (proc0) then
      if (.not. expro_init) then
        call init_expro_io (rad_grid, rhotor_grid, rmin_grid, drhotordrho_grid, drmindrho_grid, &
          psitor_a, rgeo, amin, bmag, zeff)
        expro_init = .true.
      end if
      call expro_io_profs (rad_grid, dens_grid, temp_grid, omega_grid)
      ! ensure initial density of final ion species satisfies quasineutrality
      if (n_ion_spec > 1) then
        dens_grid(:,nspec) = dens_grid(:,1) &
          - sum(spread(z_ion(:n_ion_spec-1),1,nrad)*dens_grid(:,2:n_ion_spec),2)
      else
        ! if only one ion species, then Zi*ni = ne
        dens_grid(:,nspec) = dens_grid(:,1)/z_ion(1)
      end if

      mom_grid = omega_grid/inv_mominertia_fnc(dens_grid,rmajor_grid)

      call expro_io_species (rad_grid, zeff_grid)

    end if

    do is = 1, nspec
      call broadcast (dens_grid(:,is))
      call broadcast (temp_grid(:,is))
    end do
    call broadcast (mom_grid)
    call broadcast (omega_grid)
    call broadcast (zeff_grid)

    !       call broadcast (chie_exp)
    !       call broadcast (chii_exp)

    !       call get_cc (chie_exp, chie_exp_cc)
    !       call get_cc (chii_exp, chii_exp_cc)
  end subroutine import_initial_profiles_expro
  subroutine import_sources_expro
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: open_output_file, flush_output_file
    use interp, only: get_cc
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: te_equal_ti, te_fixed
    use nteqns_arrays
    use norms, only: aref
    use trinity_input, only: nrad
    real, dimension (:), allocatable :: int_src_mom
    real, dimension (:,:), allocatable :: int_src_aux
    integer :: ix !, is
      allocate (int_src_mom(nrad))
      allocate (int_src_aux(nrad,nspec))

      ! need to fix this.  expro returns the integrated source instead of the local source...
      if (proc0) then
        if (.not. expro_init) then
          write (*,*) 'Currently "expro" must be used for more than just source_option="expro"'
          write (*,*) 'Aborting simulation.'
          call abort_mp
        end if

        call expro_io_source (rad_grid, int_src_aux, int_src_mom)

        !src_dens = 0.0
        ! No density sources available
        ! Sources default to 0

        ! int_src_mom is integrated torque in Newton-meters
        ! need to convert to local torque deposition
        !src_mom(1) = int_src_mom(1)*grho_grid(1)*lsrcnorm &
          !/ (area_grid(1)*drad*aref**3)
        src_mom_unspecified(1) = int_src_mom(1)*grho_grid(1)&
          / (area_grid(1)*drad*aref**3)
        do ix = 2, nrad
          src_mom_unspecified(ix) = (int_src_mom(ix) - int_src_mom(ix-1))*&
            grho_grid(ix)/ (area_grid(ix)*drad*aref**3)
        end do

        ! int_src_aux is integrated power in meters
        ! need to convert to local power deposition in trin normalization
        !src_aux(1,:) = int_src_aux(1,:)/(drad*aref**3*area_grid(1)) &
          !*grho_grid(1)*srcnorm*1.e6
        src_heat_unspecified(1,:) = int_src_aux(1,:)/(drad*aref**3*area_grid(1)) &
          *grho_grid(1)*1.e6
        do ix = 2, nrad
          src_heat_unspecified(ix,:) = (int_src_aux(ix,:) - int_src_aux(ix-1,:))/&
            (drad*aref**3*area_grid(ix)) &
            *grho_grid(ix)*1.e6
        end do

      end if

      deallocate (int_src_mom, int_src_aux)
  end subroutine import_sources_expro

  ! subroutine that uses EXPRO/input.profiles to get expt. data
  subroutine init_expro_io (rad, rhotor_out, rmin_out, drhotordrho_out, drmindrho_out, &
       phia_out, rgeo_out, amin_out, bmag_out, zeff_out)

# ifdef USE_EXPRO
    use EXPRO_interface
# endif

    use mp_trin, only: abort_mp
    use trinity_input, only: fluxlabel_option_aminor, fluxlabel_option_torflux
    use trinity_input, only: fluxlabel_option_switch
    use trinity_input, only: z_ion, n_ion_spec
    use interp, only: fpintrp

    implicit none

    integer :: ix, nrad

    real, dimension (:), intent (in) :: rad
    real, dimension (:), intent (out) :: rhotor_out, rmin_out, drhotordrho_out, drmindrho_out
    real, intent (out) :: phia_out, rgeo_out, amin_out, bmag_out, zeff_out

# ifdef USE_EXPRO

    real, dimension (:), allocatable :: drhotordrho

    nrad = size(rad)

    call EXPRO_alloc('',1)

    ! apparently deprecated variables
    !    EXPRO_ctrl_density_method = 2
    !    EXPRO_ctrl_rotation_method = 1

    EXPRO_ctrl_quasineutral_flag = 1
    EXPRO_ctrl_numeq_flag = 0
    EXPRO_ctrl_n_ion = n_ion_spec
    EXPRO_ctrl_z(:n_ion_spec) = z_ion
    EXPRO_ctrl_silent_flag = 0

    call EXPRO_read

    if (.not. allocated(rho)) allocate (rho(EXPRO_n_exp))
    if (.not. allocated(drhotordrho)) allocate (drhotordrho(EXPRO_n_exp))

    select case (fluxlabel_option_switch)

    ! use r/r(separatrix) as flux label
    case (fluxlabel_option_aminor)

       rho = EXPRO_rmin/EXPRO_rmin(EXPRO_n_exp)
       drmindrho_out = 1.0
       do ix = 2, EXPRO_n_exp-1
          drhotordrho(ix) = (EXPRO_rho(ix+1) - EXPRO_rho(ix-1)) / (rho(ix+1) - rho(ix-1))
       end do
       drhotordrho(1) = (EXPRO_rho(2)-EXPRO_rho(1))/(rho(2)-rho(1))
       drhotordrho(EXPRO_n_exp) = (EXPRO_rho(EXPRO_n_exp)-EXPRO_rho(EXPRO_n_exp-1)) &
            / (rho(EXPRO_n_exp)-rho(EXPRO_n_exp-1))
       
       do ix = 1, nrad
          drhotordrho_out(ix) = fpintrp(rad(ix),drhotordrho,rho,EXPRO_n_exp)
       end do

    ! use sqrt(psitor/psitor(separatrix)) as flux label
    case (fluxlabel_option_torflux)

       rho = EXPRO_rho
       write (*,*) 'Currently, only fluxlabel_option_aminor is supported for use with EXPRO.'
       write (*,*) 'Aborting simulation.'
       call abort_mp

    end select

    do ix = 1, nrad
       rhotor_out(ix) = fpintrp(rad(ix),EXPRO_rho,rho,EXPRO_n_exp)
       rmin_out(ix) = fpintrp(rad(ix),EXPRO_rmin/EXPRO_rmin(EXPRO_n_exp),rho,EXPRO_n_exp)
    end do

    phia_out = EXPRO_arho
    rgeo_out = EXPRO_rmaj(EXPRO_n_exp)
    amin_out = EXPRO_rmin(EXPRO_n_exp)
    bmag_out = EXPRO_b_ref
    zeff_out = sum(EXPRO_z_eff)/EXPRO_n_exp

    deallocate (drhotordrho)

# else

    write (*,*) 'trinity must be compiled with "USE_EXPRO=on" to use geo_option="expro".'
    write (*,*) "aborting simulation."
    call abort_mp

# endif

  end subroutine init_expro_io

  subroutine finish_expro_io

# ifdef USE_EXPRO
    use EXPRO_interface

    implicit none

    call EXPRO_alloc('',0)
# endif
    if (allocated(rho)) deallocate(rho)

  end subroutine finish_expro_io

  subroutine expro_io_geo (rad, q_out, shat_out, kappa_out, kapprim_out, rmajor_out, shift_out, &
       delta_out, deltprim_out, area_out, grho_out, bunit_out, btori_out, bmag_axis)

# ifdef USE_EXPRO
    use EXPRO_interface
# endif

    use interp, only: fpintrp

    implicit none

    real, dimension (:), intent (in) :: rad
    real, dimension (:), intent (out) :: q_out, shat_out, kappa_out, kapprim_out, rmajor_out, shift_out
    real, dimension (:), intent (out) :: delta_out, deltprim_out, area_out, grho_out, bunit_out, btori_out
    real, intent(out) :: bmag_axis

# ifdef USE_EXPRO
    integer :: ix, nrad
    real, dimension (:), allocatable :: rmin, rmaj, btori

    allocate (rmin(EXPRO_n_exp), rmaj(EXPRO_n_exp))
    allocate (btori(EXPRO_n_exp))

    nrad = size(q_out)
    
    rmin = EXPRO_rmin / EXPRO_rmin(EXPRO_n_exp)
    rmaj = EXPRO_rmaj / EXPRO_rmin(EXPRO_n_exp)

    ! EXPRO_bt0 is the toroidal field at theta = 0
    ! Therefore I = btori = EXPRO_bt0 * R(theta=0)
    ! where R(theta=0) = Rout
    ! and Rout = EXPRO_rmin + EXPRO_rmaj
    btori = EXPRO_bt0 * (EXPRO_rmin + EXPRO_rmaj)

    bmag_axis = EXPRO_bt0(1)

    do ix = 1, nrad
       q_out(ix) = abs(fpintrp(rad(ix),EXPRO_q,rho,EXPRO_n_exp))
       shat_out(ix) = fpintrp(rad(ix),EXPRO_s,rho,EXPRO_n_exp)
       kappa_out(ix) = fpintrp(rad(ix),EXPRO_kappa,rho,EXPRO_n_exp)
       kapprim_out(ix) = fpintrp(rad(ix),EXPRO_skappa*EXPRO_kappa/(rho+1.e-6),rho,EXPRO_n_exp)
       ! major radius normalized by minor radius of separatrix
       rmajor_out(ix) = fpintrp(rad(ix),rmaj,rho,EXPRO_n_exp)
       shift_out(ix) = fpintrp(rad(ix),EXPRO_drmaj,rho,EXPRO_n_exp)
       delta_out(ix) = fpintrp(rad(ix),EXPRO_delta,rho,EXPRO_n_exp)
       deltprim_out(ix) = fpintrp(rad(ix),EXPRO_sdelta/(rho+1.e-6),rho,EXPRO_n_exp)
       ! this is the area in m^2
       area_out(ix) = fpintrp(rad(ix),EXPRO_volp*EXPRO_grad_r0,rho,EXPRO_n_exp)
       ! grho_out is |grad r| at theta=0
       grho_out(ix) = fpintrp(rad(ix),EXPRO_grad_r0,rho,EXPRO_n_exp)
! FOR COMPARISON WITH TGYRO
!       grho_out(ix) = 1.0
       bunit_out(ix) = fpintrp(rad(ix),EXPRO_bunit,rho,EXPRO_n_exp)
       btori_out(ix) = fpintrp(rad(ix),btori,rho,EXPRO_n_exp)
    end do

    deallocate (rmin, rmaj)
# endif

  end subroutine expro_io_geo

  subroutine expro_io_profs (rad, dens_out, temp_out, omega_out)

# ifdef USE_EXPRO
    use EXPRO_interface
# endif

    use interp, only: fpintrp
    use trinity_input, only: vtfac
    use trinity_input, only: m_ion, n_ion_spec

    implicit none

    real, dimension (:), intent (in) :: rad
    real, dimension (:), intent (out) :: omega_out
    real, dimension (:,:), intent (out) :: dens_out, temp_out

# ifdef USE_EXPRO
    integer :: ix, nrad, is

    real, dimension (:), allocatable :: a_over_vt

    nrad = size(rad)

    allocate (a_over_vt(EXPRO_n_exp))

    ! a_over_vt = a / vt0 in seconds, where a is minor radius in meters, and
    ! vt0 = sqrt(vtfac * [1 kev] / m_ref)
    a_over_vt = EXPRO_rmin(EXPRO_n_exp)*sqrt(m_ion(1)/vtfac)/3.094e5

    do ix = 1, nrad
       dens_out(ix,1) = fpintrp(rad(ix),0.1*EXPRO_ne,rho,EXPRO_n_exp)
    end do
    do is = 1, n_ion_spec
       do ix = 1, nrad
          dens_out(ix,is+1) = fpintrp(rad(ix),0.1*EXPRO_ni(is,:),rho,EXPRO_n_exp)
       end do
    end do

    do ix = 1, nrad
       temp_out(ix,1) = fpintrp(rad(ix),EXPRO_te,rho,EXPRO_n_exp)
       temp_out(ix,2) = fpintrp(rad(ix),EXPRO_ti(1,:),rho,EXPRO_n_exp)
       omega_out(ix) = fpintrp(rad(ix),EXPRO_w0*a_over_vt,rho,EXPRO_n_exp)
    end do

    ! assume all ions have same temperature
    if (n_ion_spec > 1) temp_out(:,3:) = spread(temp_out(:,2),2,n_ion_spec-1)

    deallocate (a_over_vt)
# endif

  end subroutine expro_io_profs

  subroutine expro_io_species (rad, zeff_out)

# ifdef USE_EXPRO
    use EXPRO_interface
# endif

    use interp, only: fpintrp

    implicit none

    real, dimension (:), intent (in) :: rad
    real, dimension (:), intent (out) :: zeff_out

# ifdef USE_EXPRO
    integer :: ix, nrad

    nrad = size(rad)

    do ix = 1, nrad
       zeff_out(ix) = fpintrp(rad(ix),EXPRO_z_eff,rho,EXPRO_n_exp)
    end do
# endif

  end subroutine expro_io_species

  subroutine expro_io_source (rad, src_aux, src_mom)

# ifdef USE_EXPRO
    use EXPRO_interface
# endif

    use interp, only: fpintrp
    use trinity_input, only: n_ion_spec

    implicit none

    real, dimension (:), intent (in) :: rad
    real, dimension (:,:), intent (out) :: src_aux
    real, dimension (:), intent (out) :: src_mom

# ifdef USE_EXPRO
    integer :: ix, nrad

    nrad = size(rad)

    write (*,*) 'EXPRO SOURCES', EXPRO_pow_e, EXPRO_pow_i, EXPRO_pow_ei

    do ix = 1, nrad
       src_mom(ix) = fpintrp(rad(ix),EXPRO_flow_mom,rho,EXPRO_n_exp)
       src_aux(ix,1) = fpintrp(rad(ix),EXPRO_pow_e+EXPRO_pow_ei,rho,EXPRO_n_exp)
       src_aux(ix,2) = fpintrp(rad(ix),EXPRO_pow_i-EXPRO_pow_ei,rho,EXPRO_n_exp)
    end do

    if (n_ion_spec > 1) src_aux(:,3:) = 0.
# endif

  end subroutine expro_io_source
end module expro_import
