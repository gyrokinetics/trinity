!> Module to read and write EXPEQ files for CHEASE 
!! See CHEASE paper

module ecom_io
  
  public

  !real, dimension (:), allocatable :: surface_r
  !real, dimension (:), allocatable :: surface_z

  real, dimension (:), allocatable :: pressure

  real, dimension (:), allocatable :: current_func

  real, dimension (:), allocatable :: flux_coordinate

  !integer :: nsurf

  integer :: nflux
  
  !integer :: nsttp
  
  !integer :: nrhomesh
  
  integer :: infile=1283
  integer :: outfile=1284


  !real :: aspct,rz0c,predge

  contains
  subroutine ecom_read_infile(filename)
    character(*), intent(in) :: filename
    integer :: i
    open(infile,file= filename)
    write (*,*) 'Opened ecom infile'
    read(infile,*)
    write (*,*) 'Reading nflux'
    read(infile, *) nflux
    write (*,*) 'nflux is', nflux
    allocate(flux_coordinate(nflux))
    allocate(pressure(nflux))
    allocate(current_func(nflux))
    read (infile,*)
    write (*,*) 'Reading flux_coordinate'
    read(infile, *) (flux_coordinate(i),i=1,nflux)
    read (infile,*)
    write (*,*) 'Reading pressure'
    read(infile, *) (pressure(i),i=1,nflux)
    read (infile,*)
    write (*,*) 'Reading current_func'
    read(infile, *) (current_func(i),i=1,nflux)
    close(infile)


    
  end subroutine ecom_read_infile

  subroutine ecom_write_outfile(filename)
    character (*), intent(in) :: filename
    integer :: i
    open(outfile,file= filename)
    write(outfile, *) 'Number of points in profile'
    write(outfile, '(I5)') nflux
    write(outfile, *) 'Normalized flux coordinate (rho)'
    !write(outfile, '(E18.8)') flux_coordinate
    write(outfile, *) (flux_coordinate(i),i=1,nflux)
    write(outfile, *) 'Pressure (SI units)'
    write(outfile, *) (pressure(i),i=1,nflux)
    write(outfile, *) 'F'
    write(outfile, *) (current_func(i),i=1,nflux)
    close(outfile)


    
  end subroutine ecom_write_outfile
  
  subroutine deallocate_ecom_io
    !deallocate(surface_r)
    !deallocate(surface_z)
    deallocate(flux_coordinate)
    deallocate(pressure)
    deallocate(current_func)
  end subroutine deallocate_ecom_io

end module ecom_io

!program test_ecom_io
  !use ecom_io
  !character (len=80) :: filename
  !filename = 'EXPEQ.OUT'
  !call read_infile(filename)
  !write (*,*) nflux, '<---nflux'
  !filename = 'EXPEQ.OUT.TEST'
  !call write_outfile(filename)
  !call deallocate_ecom_io
!end program test_ecom_io
