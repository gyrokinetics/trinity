!> A module for importing equilibrium data from the trinity
!! netcdf file format.
module trinity_import

  use trindiag_config, only: trindiag_type

  implicit none

contains

  function trinity_import_t_index()
    integer :: trinity_import_t_index
    trinity_import_t_index = 1
  end function trinity_import_t_index
  
  subroutine open_trinity_file(file_name, gnostics_input, input_time)

    use file_utils_trin, only: error_unit
    use mp_trin, only: proc0, abort_mp
    use simpledataio, only: open_file
    use simpledataio, only: sdatio_init
    use simpledataio, only: dimension_size
    use simpledataio, only: set_dimension_start
    use simpledataio_read, only: read_variable
    use trinity_input, only: m_ion, aspr
    use norms, only: aref, amin_ref, psipol_a, psitor_a

    character(len=*), intent(in) :: file_name
    type(trindiag_type), intent(inout) :: gnostics_input
    real, intent(in) :: input_time
    !real, dimension(:), allocatable :: time_values
    real :: old_t, new_t, min_t, max_t
    integer :: ntstep_netcdf, it
    logical :: has_input_time
    gnostics_input%parallel = .false.
    gnostics_input%wryte = .false.
    gnostics_input%create = .false.
    if (proc0) then
      gnostics_input%reed = .true.
    else
      gnostics_input%reed = .false.
    end if
    if (gnostics_input%reed) then
      call sdatio_init(gnostics_input%sfile, trim(file_name))
      call open_file(gnostics_input%sfile)
      call dimension_size(gnostics_input%sfile, "t", ntstep_netcdf)
      call set_dimension_start(gnostics_input%sfile, "t", ntstep_netcdf)
      call read_variable(gnostics_input%sfile, "t", max_t) 
      call set_dimension_start(gnostics_input%sfile, "t", 1)
      call read_variable(gnostics_input%sfile, "t", min_t) 

      if (ntstep_netcdf .eq. 1) then 
        call set_dimension_start(gnostics_input%sfile, "t", ntstep_netcdf)
        has_input_time = .true.
      else if (input_time .le. min_t) then
        if (input_time .lt. min_t) &
          write (error_unit(),*) 'WARNING: input_time ', input_time, &
          ' is below the min t value of this input file ', min_t, &
          ' -- setting to min value'
        call set_dimension_start(gnostics_input%sfile, "t", 1)
        has_input_time = .true.
      else if (input_time .ge. max_t) then
        if (input_time .gt. max_t) &
          write (error_unit(),*) 'WARNING: input_time ', input_time, &
          ' is above the max t value of this input file ', max_t, &
          ' -- setting to max value'
        call set_dimension_start(gnostics_input%sfile, "t", ntstep_netcdf)
        has_input_time = .true.
      else

         !write (*,*) 'ntstep_netcdf', ntstep_netcdf
        !allocate(time_values(ntstep_netcdf))
        !write (*,*) 'time_values', time_values
        has_input_time = .false.
        !call read_variable(gnostics_input%sfile, "t", ol) 
        !old_t = 0.0
        old_t = min_t
        do it = 2,ntstep_netcdf
          call set_dimension_start(gnostics_input%sfile, "t", it)
          call read_variable(gnostics_input%sfile, "t", new_t) 
          if (it > 1 .and. old_t.le.input_time .and. &
              new_t .gt.  input_time) then
            has_input_time = .true.
            if (input_time - old_t < (new_t-old_t)/2.0) then
              call set_dimension_start(gnostics_input%sfile, "t", it-1)
            end if
            exit
          end if
          old_t = new_t
        end do
      end if
      if (.not. has_input_time) then
        write (*,*) 'ERROR: input_time is outside the range of time values&
          & in this input file. input_time is ', input_time, ' and must be&
          & between ', min_t , 'and', max_t
        call abort_mp
      end if
      call read_variable(gnostics_input%sfile, "aref", aref)
      call read_variable(gnostics_input%sfile, "amin_ref", amin_ref)
      call read_variable(gnostics_input%sfile, "psitor_a", psitor_a)
      call read_variable(gnostics_input%sfile, "psipol_a", psipol_a)
      call read_variable(gnostics_input%sfile, "m_ion", m_ion)
      call read_variable(gnostics_input%sfile, "aspr", aspr)
    end if
    !call read_variable(gnostics_input%sfile, "vtfac", vtfac)
    !call read_variable(gnostics_input%sfile, "aref", fluxlabel_option)
  end subroutine open_trinity_file

  subroutine close_trinity_file(gnostics_input)
    use mp_trin, only: proc0
    use simpledataio, only: closefile
    use simpledataio, only: sdatio_free
    type(trindiag_type), intent(inout) :: gnostics_input
    if (proc0) then
      call closefile(gnostics_input%sfile)
      call sdatio_free(gnostics_input%sfile)
    end if
  end subroutine close_trinity_file

  subroutine calculate_derived_geometric_quantities
    use nteqns_arrays, only: delta_grid, kappa_grid, rmajor_grid
    use nteqns_arrays, only: rad_grid, deltprim_grid, kapprim_grid
    use nteqns_arrays, only: shift_grid
    use nteqns_arrays, only: rmin_grid, drmindrho_grid
    use nteqns_arrays, only: rhotor_grid, drhotordrho_grid
    use nteqns_arrays, only: psipol_grid, dpsipoldrho_grid
    use nteqns_arrays, only: bunit_grid, area_grid, grho_grid
    use nteqns_arrays, only: deltprim_grid_missing, kapprim_grid_missing
    use nteqns_arrays, only: shift_grid_missing, drmindrho_grid_missing
    use nteqns_arrays, only: dpsipoldrho_grid_missing, drhotordrho_grid_missing
    use nteqns_arrays, only: bunit_grid_missing
    use constants_trin, only: pi
    use norms, only: amin_ref, psitor_a
    use interp, only: fd3pt
    use trinity_input, only: nrad
    use miller_geometry, only: init_miller_geo

    real, dimension(nrad-1) :: drho
    drho= rad_grid(2:)-rad_grid(:nrad-1)
    if (deltprim_grid_missing) call fd3pt (delta_grid, deltprim_grid, drho)
    if (kapprim_grid_missing) call fd3pt (kappa_grid, kapprim_grid, drho)
    ! get dR/dr
    if (shift_grid_missing) call fd3pt (rmajor_grid, shift_grid, drho)
    if (drmindrho_grid_missing) call fd3pt (rmin_grid, drmindrho_grid, drho)
    if (dpsipoldrho_grid_missing) call fd3pt (psipol_grid, dpsipoldrho_grid, drho)
    if (drhotordrho_grid_missing) call fd3pt (rhotor_grid, drhotordrho_grid, drho)
    if (bunit_grid_missing) bunit_grid =  drhotordrho_grid*rhotor_grid*psitor_a&
      /drmindrho_grid/rmin_grid/pi/amin_ref**2

    !call init_miller_geo (rmin_grid, rmajor_grid, shift_grid/drmindrho_grid, kappa_grid, kapprim_grid/drmindrho_grid, &
    !asin(delta_grid), deltprim_grid/sqrt(1-(delta_grid)**2)/drmindrho_grid)
    !call get_grho_av(grho_grid)
  end subroutine calculate_derived_geometric_quantities

  subroutine calculate_derived_profiles
    use nteqns_arrays, only: omega_grid, mom_grid, inv_mominertia_fnc
    use nteqns_arrays, only: dens_grid, rmajor_grid!, temp_grid
    use nteqns_arrays, only: mom_grid_missing
!    use trinity_input, only: equal_ion_temps, n_ion_spec
    !integer :: i
    if (mom_grid_missing) mom_grid = omega_grid/inv_mominertia_fnc(dens_grid,rmajor_grid)
    !if (equal_ion_temps) then
      !do i = 1,n_ion_spec
        !temp_grid(:,i+1) =temp_grid(:,2)
      !end do
    !end if
  end subroutine calculate_derived_profiles

  subroutine import_geometry_trinity
    use trinity_input, only: geo_file
    use nteqns_arrays, only: write_grids
    use trinity_input, only: geo_time
    type(trindiag_type) :: gnostics_input
    real :: rad_out_input
    integer :: nrad_input
    call open_trinity_file(geo_file, gnostics_input, geo_time)
    ! NB open_trinity_file sets flags so that write_grids actually reads
    call write_grids(gnostics_input, nrad_input, rad_out_input, 'geo')
    call close_trinity_file(gnostics_input)
    call calculate_derived_geometric_quantities
  end subroutine import_geometry_trinity

  subroutine import_initial_profiles_trinity
    use trinity_input, only: init_file
    use nteqns_arrays, only: write_grids
    use trinity_input, only: init_time
    type(trindiag_type) :: gnostics_input
    real :: rad_out_input
    integer :: nrad_input
    call open_trinity_file(init_file, gnostics_input, init_time)
    ! NB open_trinity_file sets flags so that write_grids actually reads
    call write_grids(gnostics_input, nrad_input, rad_out_input, 'profiles')
    call close_trinity_file(gnostics_input)
    call calculate_derived_profiles
  end subroutine import_initial_profiles_trinity

  subroutine import_sources_trinity
    use trinity_input, only: source_file
    use nteqns_arrays, only: write_grids
    use trinity_input, only: geo_time
    type(trindiag_type) :: gnostics_input
    real :: rad_out_input
    integer :: nrad_input
    call open_trinity_file(source_file, gnostics_input, geo_time)
    ! NB open_trinity_file sets flags so that write_grids actually reads
    call write_grids(gnostics_input, nrad_input, rad_out_input, 'sources')
    call close_trinity_file(gnostics_input)
  end subroutine import_sources_trinity
end module trinity_import
