
!> This module contains routines for reading
!! equilibrium data from the ASCII files provided
!! by the tokamak profile database (otherwise known as the
!! iter profile database) hosted at 
!! http://tokamak-profiledb.ccfe.ac.uk/

module iterdb
contains
  subroutine import_geometry_iterdb
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    !    use constants_trin, only: pi => dpi
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: rad_out, aspr, rgeo, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: testing, dfac, nrad, bmag, vtfac, ntdelt
    use trinity_input, only: n_ion_spec
    use trinity_input, only: init_file, init_time, geo_file, geo_time
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: rlti, rlte, tiedge, teedge, amin_in, ledge, rll
    use trinity_input, only: rlni, rlne, niedge, needge
    use trinity_input, only: zeff, densfac, tifac, tefac
    use trinity_input, only: fluxlabel_option_switch, fluxlabel_option_torflux, fluxlabel_option_aminor
    use trinity_input, only: peaking_factor
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, te_fixed
    use trin_power, only: alpha_power
    use expt_profs, only: get_nrad_db, iter_db_io, init_iter_db_io
    use trinity_input, only: equal_ion_temps
    use miller_geometry, only: init_miller_geo, get_Iflxfnc
    use nteqns_arrays
    use norms, only: amin_ref, aref, psitor_a
    implicit none


    !integer :: restart_unit, is
    integer :: nrad_db
    real :: amin

    logical, dimension (:), allocatable :: error_flag
    character (10), dimension (:), allocatable :: var_name
    real, dimension (:), allocatable :: inner_values
    real, dimension (:), allocatable :: profs
    real, dimension (:), allocatable :: rhotor_exp_grid, rmin_exp_grid
    if (proc0) then
      call get_nrad_db (geo_file, nrad_db)

      allocate (rhotor_exp_grid(nrad_db))
      allocate (rmin_exp_grid(nrad_db))

      ! drhotordrho = drhotor/drho, rhotor = sqrt(tor flux/psitor_a), psitor_a=tor flux enclosed by separatrix
      ! drmindrho = d(r/a)/drho
      ! if using rho=rhotor, then drhotordrho=1 and rhotor=rad
      call init_iter_db_io (geo_file, geo_time, rad_grid, drhotordrho_grid, &
        drmindrho_grid, rhotor_grid, rmin_grid, psitor_a, rgeo, amin, bmag, zeff, &
        rhotor_exp_grid, rmin_exp_grid)

      ! overwrite input values with ITER DB values if they are present
      if (overwrite_db_input) then
        if (abs(phia_in) > epsilon(0.0)) psitor_a = abs(phia_in)
        if (rgeo_in > epsilon(0.0)) rgeo = rgeo_in
        if (amin_in > epsilon(0.0)) amin = amin_in
        if (abs(bt_in) > epsilon(0.0)) bmag = abs(bt_in)
        !if (zeff_in > epsilon(0.0)) zeff = zeff_in
      end if
      ! aspr is aspect ratio of LCFS
      aspr = rgeo/amin

      !bmag_grid now set in initialise_bmag_grid
      bmag_vac_geo = bmag

      bmag_old_grid = bmag

      ! aref is in meters (\tld{a}_0 from notes)
      select case (fluxlabel_option_switch)
      case (fluxlabel_option_torflux)
        ! TODO: Is this correct? 
        aref = sqrt(abs(psitor_a/(pi*bmag)))
        amin_ref = rgeo/aspr
      case (fluxlabel_option_aminor)
        aref = rgeo/aspr
        amin_ref = aref
      end select

      allocate (inner_values(6))
      allocate (var_name(6)) ! Rmajor, Q, kappa, delta, surf, grho
      allocate (profs((size(var_name)+4)*nrad)) ! +4 are dRmajor/drho, rho/q dq/drho, dkappa/drho, ddelta/drho
      allocate (error_flag(size(var_name))) ; error_flag = .false.
      var_name = (/ "RMAJOR", "Q     ", "KAPPAR", "DELTAR", "SURF  ", "GRHO1 " /)
      ! optional argument '4' tells iter_db_io to return radial derivatives of first 4 vars
      call iter_db_io (geo_file, geo_time, var_name, rad_grid, profs, error_flag, inner_values, 4)

      ! use analytic value if variable not found in data file

      if (error_flag(1)) then
        rmajor_grid = aspr
        shift_grid = 0.0
        rmajor_axis = aspr
        write (*,*) "RMAJOR not present in ITER DB file."
        write (*,*) "Setting rmajor profile to constant value from input file and Shafranov shift to zero."
      else
        rmajor_grid = profs(:nrad)/aref
        shift_grid = profs(6*nrad+1:7*nrad)/aref
        !> We don't actually have rmajor_axis...
        !! we approximate it via the inner value which
        !! is a pretty good approximation.
        rmajor_axis = inner_values(1)/aref
      end if

      if (error_flag(2)) then
        write (*,*) "Using analytic specification for q and shat."
        qval_grid = q0 + alph_q*(qa-q0)*rad_grid**2 + (1.-alph_q)*(qa-q0)*rad_grid**6 + sh0*sqrt(rad_grid)*exp(-rad_grid**2)
        shat_grid = ( 2.0*(qa-q0)*rad_grid**2*(alph_q+3.*rad_grid**4*(1.-alph_q)) &
          + 0.5*sh0*sqrt(rad_grid)*exp(-rad_grid**2)*(1.-4.*rad_grid**2) ) / qval_grid
      else
        qval_grid = profs(nrad+1:2*nrad)
        shat_grid = profs(7*nrad+1:8*nrad)
      end if

      if (error_flag(3)) then
        write (*,*) "Using analytic specification for kappa and kappa'."
        kappa_grid = kap0 + alph_k*(ka-kap0)*rad_grid**2 + (1.-alph_k)*(ka-kap0)*rad_grid**6
        kapprim_grid = 2.*alph_k*(ka-kap0)*rad_grid + 6.*(1.-alph_k)*(ka-kap0)*rad_grid**5
      else
        kappa_grid = profs(2*nrad+1:3*nrad)
        kapprim_grid = profs(8*nrad+1:9*nrad)
      end if

      if (error_flag(4)) then
        write (*,*) "Setting delta and delta' to zero."
        delta_grid = 0.0
        deltprim_grid = 0.0
      else
        delta_grid = profs(3*nrad+1:4*nrad)
        deltprim_grid = profs(9*nrad+1:10*nrad)
      end if

      if (error_flag(5)) then
        area_grid = 4.0*aspr*rad_grid*pi**2
        write (*,*) "Setting flux surface area based on assumption of circular flux surfaces."
      else
        ! converting from area in units of [m]^2 to units of a^2
        area_grid = profs(4*nrad+1:5*nrad)/aref**2
      end if

      if (error_flag(6)) then
        grho_grid = 1.0
        write (*,*) "Setting <| grad rho |> = 1."
      else
        ! grho from file is grad rho_tor in units of 1/[m].  converting to grad rho in units of 1/a
        grho_grid = profs(5*nrad+1:6*nrad)*aref/drhotordrho_grid
      end if

      deallocate (var_name, profs, error_flag, inner_values)

      ! initialize Miller geometry calculations
      ! note that we are passing tri = ArcSin(delta) and tripri = d(tri)/d(r/a)
      ! rmin_grid is already normalised here
      call init_miller_geo (rmin_grid, rmajor_grid, shift_grid, &
        kappa_grid, kapprim_grid, asin(delta_grid), &
        deltprim_grid/sqrt(1-(delta_grid)**2))

      ! We need to pass the unnormalised rmin_exp_grid in here
      ! to get the dimensional value of dpsi_toroidal / d rmin
      !write (*,*) 'rad_grid', rad_grid, '----', rmin_exp_grid
      call get_Iflxfnc(rad_grid,rhotor_exp_grid,rmin_exp_grid*amin_ref, &
        psitor_a, btori_grid) 
      !write (*,*) 'btori_grid', btori_grid, '--------', rhotor_exp_grid

      !> Approximate bmag_axis by assuming that btori_grid levels off towards
      !! the center
      bmag_axis = btori_grid(1)/rmajor_axis
      !> Above is not very good. Set bmag_axis to 0 as we can't work it out
      bmag_axis = 0.0

      ! bunit = 1/r dtorflux / dr / 2 pi according to the gacode website
      ! drhotordrho_grid = 
      ! d(sqrt(torflux/psitor_a))/drho =
      ! sqrt(torflux/phi)^-1 * 0.5 * dtorflux/drho / psitor_a
      ! => d torflux/drho = drhotordrho_grid * rhotor_grid * phi_a * 2
      ! => bunit_grid = drhotordrho_grid*rhotor_grid*phi_a*2/drmindrho_grid /rmin_grid/2 pi
      ! Don't forget we need r = rmin_grid*amin_ref
      bunit_grid =  drhotordrho_grid*rhotor_grid*psitor_a/drmindrho_grid/rmin_grid/pi/amin_ref**2




      deallocate (rhotor_exp_grid, rmin_exp_grid)

    end if
  end subroutine import_geometry_iterdb
  subroutine import_initial_profiles_iterdb
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    !    use constants_trin, only: pi => dpi
    use constants_trin, only: pi
    use trinity_input, only: rad_out, aspr, q0, qa, alph_q, sh0
    use trinity_input, only: ka, alph_k, kap0
    use trinity_input, only: testing, dfac, nrad, vtfac, ntdelt
    use trinity_input, only: z_ion
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: init_file, init_time, geo_file, geo_time
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: rlti, rlte, tiedge, teedge, amin_in, ledge, rll
    use trinity_input, only: rlni, rlne, niedge, needge
    use trinity_input, only: densfac, tifac, tefac
    use trinity_input, only: include_alphas
    use trinity_input, only: peaking_factor
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, te_fixed
    use trin_power, only: alpha_power
    use expt_profs, only: get_nrad_db, iter_db_io
    use trinity_input, only: equal_ion_temps
    use miller_geometry, only: init_miller_geo, get_Iflxfnc
    use chease_ecom, only: update_geometry, get_chease_profs
    use nteqns_arrays
    use norms, only: aref, vtref
    use interp, only: get_cc
    implicit none

    !integer :: restart_unit, is, ix
    !integer :: nrad_db
    !real :: amin

    logical, dimension (:), allocatable :: error_flag
    character (10), dimension (:), allocatable :: var_name
    real, dimension (:), allocatable :: inner_values
    real, dimension (:), allocatable :: profs
    ! get electron density, ion and electron temperatures, rotation, 
    ! and experimental chi values
    if (proc0) then


      allocate (var_name(7))
      allocate (profs((size(var_name)+1)*nrad)) ! +1 is domega/drho
      allocate (error_flag(size(var_name))) ; error_flag = .false.
      allocate (inner_values(7))
      var_name = &
        (/ "VROT ", "NE   ", "TI   ", "TE   ", "CHIE ", "CHII ", "VROTM" /)
      call iter_db_io (init_file, init_time, var_name, rad_grid, profs, error_flag, inner_values, 1)

      if (needge > 0.0) then
        dens_grid(:,1) = needge*exp(rlne*(rad_out-rad_grid)/aspr)
      else if (error_flag(2)) then
        write (*,*) "Electron density not specified in ITER DB file."
        write (*,*) "Setting electron density = exp((rad_out-rad)/aspr)."
        dens_grid(:,1) = exp((rad_out-rad_grid)/aspr)
      else
        ! convert from n in 1/m^3 to 10^20/m^3
        dens_grid(:,1) = profs(nrad+1:2*nrad)*1.e-20*densfac
        if (peaking_factor>0.0) then
          write(*,*) 'More thought required'
          stop 1
          !dens_grid(:,:) = (spread(dens_grid(nrad,:),1,nrad)**3.0 + &
          !(dens_grid(:,:)**3.0-dens_grid(nrad,:)**3.0) * peaking_factor &
          !)**(1.0/3.0)
        end if
      end if

      if (tiedge > 0.0) then
        temp_grid(:,2) = tiedge*exp(rlti*(rad_out-rad_grid)/aspr)
      else if (error_flag(3)) then
        if (error_flag(4)) then
          write (*,*) "Ti and Te not specified in ITER DB file."
          write (*,*) "Setting Ti = exp(rlti*(rad_out-rad)/aspr)."
          temp_grid(:,2) = exp(rlti*(rad_out-rad_grid)/aspr)
        else
          write (*,*) "Ti not specified in ITER DB file."
          write (*,*) "Setting Ti = Te."
          temp_grid(:,2) = profs(3*nrad+1:4*nrad)*1.e-3
        end if
      else
        ! convert from T in eV to keV
        temp_grid(:,2) = profs(2*nrad+1:3*nrad)*1.e-3*tifac
        if (peaking_factor>0.0) then
          temp_grid(:,2) = (temp_grid(nrad,2)**3.0 + &
            (temp_grid(:,2)**3.0-temp_grid(nrad,2)**3.0) * peaking_factor &
            )**(1.0/3.0)
        end if
      end if

      if (teedge > 0.0) then
        temp_grid(:,1) = teedge*exp(rlte*(rad_out-rad_grid)/aspr)
      else if (error_flag(4)) then
        write (*,*) "Te not specified in ITER DB file."
        write (*,*) "Setting Te = exp(rlte*(rad_out-rad)/aspr)."
        temp_grid(:,1) = exp(rlte*(rad_out-rad_grid)/aspr)
      else
        ! convert from Te in eV to keV
        temp_grid(:,1) = profs(3*nrad+1:4*nrad)*1.e-3*tefac
        if (peaking_factor>0.0) then
          temp_grid(:,1) = (temp_grid(nrad,1)**3.0 + &
            (temp_grid(:,1)**3.0-temp_grid(nrad,1)**3.0) * peaking_factor &
            )**(1.0/3.0)
        end if
      end if

      ! for now assume that initial ion temperatures are all equal
      if (n_ion_spec > 1) temp_grid(:,3:) = spread(temp_grid(:,2),2,n_ion_spec-1)

      if (error_flag(5)) then
        write (*,*) "Experimental chi_e not specified."
        chie_exp = 0.0
      else
        chie_exp = profs(4*nrad+1:5*nrad)
      end if

      if (error_flag(6)) then
        write (*,*) "Experimental chi_i not specified."
        chii_exp = 0.0
      else
        chii_exp = profs(5*nrad+1:6*nrad)
      end if

      if (error_flag(1)) then
        write (*,*) "VROT not specified in ITER DB file."
        write (*,*) "Attempting to use VROTM"
        if (error_flag(7)) then
          write (*,*) "VROTM not specified in ITER DB file."
          write (*,*) "Setting initial flow shear to zero."
          omega_grid = 0.0
        else
          omega_grid = profs(6*nrad+1:7*nrad)*aref/vtref
        end if

      else
        omega_grid = profs(:nrad)*aref/vtref
      end if

      deallocate (var_name, profs, error_flag, inner_values)

      ! now get ion densities and zeff

      allocate (var_name(4))
      allocate (profs(size(var_name)*nrad))
      allocate (error_flag(size(var_name))) ; error_flag = .false.
      allocate (inner_values(7))
      var_name = (/ "ZEFFR", "NM1  ", "NM2  ", "NM3  " /)
      call iter_db_io (init_file, init_time, var_name, rad_grid, profs, error_flag, inner_values)

      if (n_ion_spec > 1) then
        if (error_flag(2)) then
          write (*,*) "Primary ion density (NM1) not present in iterdb file."
          write (*,*) "Setting initial primary ion density = electron density."
          write (*,*) "Setting all other initial ion densities to zero."
          dens_grid(:,2) = dens_grid(:,1)
          error_flag(3) = .true. ; error_flag(4) = .true.
        else
          dens_grid(:,2) = profs(nrad+1:2*nrad)*1.e-20*densfac
        end if
        if (n_ion_spec > 2) then
          if (error_flag(3)) then
            write (*,*) "Secondary ion density (NM2) not present in iterdb file."
            write (*,*) "Setting initial secondary ion density = 0"
            dens_grid(:,3) = 0.0
          else
            dens_grid(:,3) = profs(2*nrad+1:3*nrad)*1.e-20*densfac
          end if
          if (n_ion_spec > 3) then
            if (error_flag(4)) then
              write (*,*) "Tertiary ion density (NM3) not present in iterdb file."
              write (*,*) "Setting initial tertiary ion density = 0"
              dens_grid(:,4) = 0.0
            else
              dens_grid(:,4) = profs(3*nrad+1:4*nrad)*1.e-20*densfac
            end if
          end if
        end if
        ! get initial density of final ion species using quasineutrality
        dens_grid(:,nspec) = dens_grid(:,1) &
          - sum(spread(z_ion(:n_ion_spec-1),1,nrad)*dens_grid(:,2:n_ion_spec),2)
      else
        ! if only one ion species, then Zi*ni = ne
        dens_grid(:,nspec) = dens_grid(:,1)/z_ion(1)
      end if

      ! Zeff = sum_i Z_i^2 * n_i / n_e
      if (error_flag(1) .or. any(z_ion .gt. 1)) then
        zeff_grid = 1.0
        write (error_unit(), *) 'WARNING: no zeff in the iterdb file. zeff will be calculated &
          & self-consistently from the densities of the evolved ion species'
      else
        write (error_unit(),*) 'INFO: You are reading in zeff from the iterdb file &
          & which means it may not be consistent with your evolved ion species'
        zeff_grid = profs(:nrad)
      end if

      deallocate (var_name, profs, error_flag, inner_values)

      mom_grid = omega_grid/inv_mominertia_fnc(dens_grid,rmajor_grid)

    end if

    !do is = 1, nspec
      !call broadcast (dens_grid(:,is))
      !call broadcast (temp_grid(:,is))
    !end do
!
    !call broadcast (mom_grid)
    !call broadcast (omega_grid)
    !call broadcast (chie_exp)
    !call broadcast (chii_exp)
    !call broadcast (zeff_grid)
!
    !call get_cc (chie_exp, chie_exp_cc)
    !call get_cc (chii_exp, chii_exp_cc)
!
  end subroutine import_initial_profiles_iterdb

  subroutine import_sources_iterdb
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: open_output_file, flush_output_file
    use interp, only: get_cc
    use trinity_input, only: n_ion_spec
    use trinity_input, only: te_equal_ti, te_fixed
    use trinity_input, only: nrad, vtfac
    use trinity_input, only: densin, psig, nsig
    use trinity_input, only: include_alphas, include_radiation, write_pwr_profs
    !use trinity_input, only: source_option_switch, source_option_analytic, source_option_iterdb
    !use trinity_input, only: source_option_expro
    use trinity_input, only: source_file, source_time, radin, temp_equil
    use trinity_input, only: pioq
    use trinity_input, only: nbi_mult, dens_mult
    use trinity_input, only: icrh_mult
    use trinity_input, only: ech_mult
    use trinity_input, only: lh_mult
    use trinity_input, only: ibw_mult
    use trinity_input, only: dwn_mult
    use trin_power, only: alpha_power, radiated_power
    use expt_profs, only: iter_db_io
    use nteqns_arrays
    use norms, only: lsrcnorm, srcnorm

    implicit none

    integer :: ivar

    logical, dimension (:), allocatable :: error_flag
    character (10), dimension (:), allocatable :: var_name
    real, dimension(:), allocatable :: inner_values
    real, dimension (:), allocatable :: profs

      if (proc0) then

        allocate (var_name(21))
        allocate (profs(size(var_name)*nrad))
        allocate (error_flag(size(var_name))) ; error_flag = .false.
        allocate (inner_values(21))
        var_name = (/ "QNBII ", "QNBIE ", "QICRHI", "QICRHE", "QECHI ", "QECHE ", &
          "QLHI  ", "QLHE  ", "QIBWI ", "QIBWE ", "QWALLI", "QWALLE", &
          "QOHM  ", "DWIR  ", "DWER  ", "SBE   ", "SNBII ", "SNBIE ", "SWALL ", "DNER  ", &
          "TORQ  " /)
        call iter_db_io (source_file, source_time, var_name, rad_grid, profs, error_flag, inner_values)

        ! set power input from a given source to zero if not found in data file
        do ivar = 1, size(var_name)
          if (error_flag(ivar)) then
            profs((ivar-1)*nrad+1:ivar*nrad) = 0.0
            write (*,*) "Setting ", trim(var_name(ivar)), " to zero."
          end if
        end do

        !              ! convert from W/m^3 to code units
        !              src_aux(:,1) = srcnorm*(profs(:nrad) + profs(2*nrad+1:3*nrad) - profs(6*nrad+1:7*nrad))
        !              src_aux(:,2) = srcnorm*(profs(nrad+1:2*nrad) + profs(3*nrad+1:4*nrad) - profs(7*nrad+1:8*nrad))
        !              ! convert from 1/(m^3 s) to code units
        !              src_dens = 1.e-20*nsrcnorm*(profs(4*nrad+1:5*nrad) - profs(5*nrad+1:6*nrad))
        ! convert from W/m^3 to code units
        !src_aux(:,1) = srcnorm*(&
          !nbi_mult*profs(nrad+1:2*nrad) + & ! QNBIE
          !icrh_mult*profs(3*nrad+1:4*nrad) + & ! QICRHE
          !ech_mult*profs(5*nrad+1:6*nrad) + & ! QECHE
          !lh_mult*profs(7*nrad+1:8*nrad) + & ! QLHE 
          !ibw_mult*profs(9*nrad+1:10*nrad) + & ! QIBWE
          !profs(11*nrad+1:12*nrad) + & ! QWALLE
          !profs(12*nrad+1:13*nrad) - & ! QOHM
          !dwn_mult*profs(14*nrad+1:15*nrad)) ! DWER
        src_heat_nbi(:,1) = profs(nrad+1:2*nrad)  ! QNBIE
        src_heat_icrh(:, 1) =  profs(3*nrad+1:4*nrad)  ! QICRHE
        src_heat_ecrh(:, 1) = profs(5*nrad+1:6*nrad)  ! QECHE
        src_heat_lh(:,1) = profs(7*nrad+1:8*nrad)  ! QLHE 
        src_heat_ibw(:,1) = profs(9*nrad+1:10*nrad)  ! QIBWE
        src_heat_wall(:,1) = profs(11*nrad+1:12*nrad)  ! QWALLE
        src_heat_ohm(:,1) = profs(12*nrad+1:13*nrad)  ! QOHM
        src_heat_dwdt(:,1) =  profs(14*nrad+1:15*nrad) ! DWER
        src_heat_nbi(:,2) = profs(:nrad)  ! QNBII
        src_heat_icrh(:,2) = profs(2*nrad+1:3*nrad)  ! QICRHI
        src_heat_ecrh(:,2) = profs(4*nrad+1:5*nrad)  !QECHI
        src_heat_lh(:,2) = profs(6*nrad+1:7*nrad)  ! QLHI
        src_heat_ibw(:,2) = profs(8*nrad+1:9*nrad)  ! QIBWI
        src_heat_wall(:,2) = profs(10*nrad+1:11*nrad)  ! QWALLI
        src_heat_dwdt(:,2) = profs(13*nrad+1:14*nrad) ! DWIR
        !src_aux(:,2) = srcnorm*(&
          !nbi_mult*profs(:nrad) + & ! QNBII
          !icrh_mult*profs(2*nrad+1:3*nrad) + & ! QICRHI
          !ech_mult*profs(4*nrad+1:5*nrad) + & !QECHI
          !lh_mult*profs(6*nrad+1:7*nrad) + & ! QLHI
          !ibw_mult*profs(8*nrad+1:9*nrad) + & ! QIBWI
          !profs(10*nrad+1:11*nrad) - & ! QWALLI
          !dwn_mult*profs(13*nrad+1:14*nrad)) ! DWIR

        ! convert from 1/(m^3 s) to code units
        !src_dens(:,1) = 1.e-20*nsrcnorm*(&
          !profs(15*nrad+1:16*nrad) + & ! SBE
          !nbi_mult*profs(17*nrad+1:18*nrad) + & ! SNBIE
          !dwn_mult*profs(19*nrad+1:20*nrad)) ! DNER
        !src_dens(:,2) = 1.e-20*nsrcnorm*(&
          !nbi_mult*profs(16*nrad+1:17*nrad) + & ! SNBII
          !profs(18*nrad+1:19*nrad) - & ! SWALL
          !dwn_mult*profs(19*nrad+1:20*nrad)) ! DNER
        src_part_ionisation(:,1) = profs(15*nrad+1:16*nrad)  ! SBE
        src_part_nbi(:,1) = profs(17*nrad+1:18*nrad)  ! SNBIE
        src_part_dndt(:,1) = profs(19*nrad+1:20*nrad) ! DNER
        src_part_nbi(:,2) = profs(16*nrad+1:17*nrad)  ! SNBII
        src_part_wall(:,2) = profs(18*nrad+1:19*nrad)  ! SWALL
        src_part_dndt(:,2) = profs(19*nrad+1:20*nrad) ! DNER

        !src_dens = src_dens * dens_mult

        ! set particle source for minority ions to zero since
        ! not given in iterdb file
        if (n_ion_spec > 1) then
          write (*,*) 'Minority ion particle sources not given in iterdb file.'
          write (*,*) 'Setting minority ion particles sources to zero.'
          src_dens(:,3:) = 0.0
        end if

        ! convert from N/m^2 to code units
        ! src_mom = physical mom source / (m_p n_0 vtref^2 rhostar0^2)
        ! n_0 is 10^20 / m^3, m_p is mass of proton
        if (error_flag(19)) then
          write (*,*) 'TORQ not found: setting src_mom to src_aux_beam * pioq'
          !src_mom = srcnorm*(profs(:nrad)+profs(nrad+1:2*nrad))*pioq*nbi_mult
          src_mom_nbi = (src_heat_nbi(:,1) + src_heat_nbi(:,2))*srcnorm/lsrcnorm*pioq
        else
          !src_mom = profs(18*nrad+1:19*nrad)*lsrcnorm*nbi_mult
          src_mom_nbi =  profs(18*nrad+1:19*nrad)
        end if
        deallocate (var_name, profs, error_flag, inner_values)

      end if

      !do is = 1, nspec
        !call broadcast (src_aux(:,is))
        !call broadcast (src_dens(:,is))
      !end do
      !call broadcast (src_mom)
  end subroutine import_sources_iterdb
end module iterdb
