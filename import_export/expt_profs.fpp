module expt_profs

  implicit none

  public :: init_iter_db_io, iter_db_io, get_nrad_db
  public :: init_iter_db_io_reset

  private

  integer, save :: db_unit
  real, dimension (:), allocatable, save :: rad1d, psi1d, drad1d, rmin1d

  ! Subroutine states
  logical :: init_iter_db_io_first  = .true.
  logical :: init_iter_db_io_reset = .false.
  logical :: iter_db_io_first

contains

  subroutine get_nrad_db (infile, nrad_db)

    use mp_trin, only: abort_mp
    use file_utils_trin, only: get_unused_unit
    
    implicit none

    character (*), intent (in) :: infile
    integer, intent (out) :: nrad_db
    
    integer :: in_unit, ierr
    character (500) :: line
    
    ! open the input file for reading
    call get_unused_unit (in_unit)
    open (unit=in_unit, file=trim(infile), status="old", action="read",&
         &iostat=ierr)
    if (ierr.ne.0) then
       write(*,'(3A)') "ERROR: file ",trim(infile)," not found!"
       call abort_mp
    endif

    ! get shot number
    read (in_unit,*,IOSTAT=ierr) line

    ! read in some lines of text
    read (in_unit,*) line
    read (in_unit,*) line
    read (in_unit,*) line
    read (in_unit,*) line

    ! var is name of current variable in input file
    read (in_unit,*) line
    read (in_unit,*) line

    ! nrad_db is number of radial points at which data is given
    read (in_unit,*) nrad_db

    close (in_unit)
    
  end subroutine get_nrad_db
  
  subroutine init_iter_db_io (infile, target_time, rad, drhotordrho, drmindrho, rhotor, rmin, &
       psitor_a, rgeo, amin, bt, zeff_0d, psi1d_out, rmin1d_out)

    use mp_trin, only: abort_mp
    use trinity_input, only: phia_in, rgeo_in, amin_in, bt_in, zeff_in
    use trinity_input, only: fluxlabel_option_switch, fluxlabel_option_torflux, fluxlabel_option_aminor
    use trinity_input, only: write_dbinfo
    use file_utils_trin, only: get_unused_unit, open_output_file
    use interp, only: fpintrp, fd3pt, fd5pt

    implicit none

    character (*), intent (in) :: infile
    real, intent (in) :: target_time
    real, dimension (:), intent (in) :: rad
    real, intent (out) :: psitor_a, rgeo, amin, bt, zeff_0d
    real, dimension (:), intent (out) :: drhotordrho, drmindrho, rhotor, rmin
    real, dimension (:), intent (out) :: psi1d_out, rmin1d_out
    
    integer :: in_unit, ierr, ix, it, iline, ix_up
    integer :: nrad_db, ntime, nlines, l
    logical :: phia_finished = .false., rgeo_finished = .false.
    logical :: amin_finished = .false., bt_finished = .false.
    logical :: zeff_finished = .false.
    logical :: finished = .false.
    !logical :: first = .true.

    integer, dimension (1) :: arraymin
    character (100) :: db1dfile

    real, dimension (:), allocatable :: radtmp, rad_db, time, dpsi1d, drmin1d
    real, dimension (:), allocatable :: phia_db, rgeo_db, amin_db, bt_db, zeff_db
    real, dimension (:,:), allocatable :: rad2d

    character (10) :: var
    character (500) :: line

    iter_db_io_first = .true.

    if (init_iter_db_io_reset) then
      phia_finished = .false.
      rgeo_finished  = .false.
      bt_finished = .false.
      zeff_finished = .false.
      finished = .false.
      init_iter_db_io_reset = .false.
    end if

      

    if (init_iter_db_io_first) then
       call open_output_file (db_unit,".iterdb")
       init_iter_db_io_first = .false.
    end if


    call get_unused_unit (in_unit)
    l = len(trim(infile))-6
    db1dfile = trim(infile(:l))//"1d.dat"
    open (unit=in_unit, file=trim(db1dfile), status="old", action="read")
    do while (.not. finished)
       ! get shot number
       read (in_unit,*,IOSTAT=ierr) line

       ! if end of file, print error message and set flag
       ! so that default values will be used for variables not found
       if (ierr < 0) then
          if (.not. phia_finished) then
             write (*,*) "Variable PHIA not found in 1D data file."
             if (abs(phia_in) > epsilon(0.0)) then
                write (*,*) "Using phia_in specified in trinity input file."
                psitor_a = phia_in
                write (*,*)
             else
                write (*,*) "phia_in not specified in trinity input file."
                write (*,*) "aborting run."
                call abort_mp
             end if
          end if
          if (.not. rgeo_finished) then
             write (*,*) "Variable RGEO not found in 1D data file."
             if (abs(rgeo_in) > epsilon(0.0)) then
                write (*,*) "Using rgeo_in specified in trinity input file."
                rgeo = rgeo_in
                write (*,*)
             else
                write (*,*) "rgeo_in not specified in trinity input file."
                write (*,*) "aborting run."
                call abort_mp
             end if
          end if
          if (.not. amin_finished) then
             write (*,*) "Variable AMIN not found in 1D data file."
             if (abs(amin_in) > epsilon(0.0)) then
                write (*,*) "Using amin_in specified in trinity input file."
                amin = amin_in
                write (*,*)
             else
                write (*,*) "amin_in not specified in trinity input file."
                write (*,*) "aborting run."
                call abort_mp
             end if
          end if
          if (.not. bt_finished) then
             write (*,*) "Variable BT not found in 1D data file."
             if (abs(bt_in) > epsilon(0.0)) then
                write (*,*) "Using bt_in specified in trinity input file."
                bt = bt_in
                write (*,*)
             else
                write (*,*) "bt_in not specified in trinity input file."
                write (*,*) "aborting run."
                call abort_mp
             end if
          end if
          if (.not. zeff_finished) then
             write (*,*) "Variable ZEFF not found in 1D data file."
             if (abs(zeff_in) > epsilon(0.0)) then
                write (*,*) "Using zeff_in specified in trinity input file."
                write (*,*) "This will be overwritten with zeff(r) coming from 2D file if available."
                write (*,*)
                zeff_0d = zeff_in
             else
                write (*,*) "zeff_in not specified in trinity input file, so setting zeff=1."
                write (*,*) "This will be overwritten with zeff(r) coming from 2D file if available."
                write (*,*)
                zeff_0d = 1.0
             end if
          end if
          exit
       end if
       ! read in some lines of text
       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line

       ! var is name of current variable in input file
       read (in_unit,*) var, line
       read (in_unit,*) line

       ! ntime is number of time slices at which data is given
       read (in_unit,*) ntime

       ! total number of lines of data for each variable
       nlines = (ntime+5)/6

       allocate (time(ntime))

       ! time is an array containing time slices
       read (in_unit,*) time

       ! location in time array containing the element closest to the user-specified time
       arraymin = minloc(abs(time-target_time))

       ! if the current variable is the ref toroidal flux, read it into the saved array psitor_a
       if (trim(var) == "PHIA") then
             
          allocate (phia_db(6*nlines))
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, phia_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          if (mod(ntime,6) == 0) then
             ix_up = 6
          else
             ix_up = mod(ntime,6)
          end if
          do ix = 1, ix_up
             if (ix == mod(ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') phia_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') phia_db(6*(nlines-1)+ix)
             end if
          end do
          psitor_a = phia_db(arraymin(1))
          
          phia_finished = .true.
          finished = phia_finished .and. rgeo_finished .and. amin_finished .and. bt_finished .and. zeff_finished
          deallocate (phia_db)

          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line

       else if (trim(var) == "RGEO") then

          allocate (rgeo_db(6*nlines))
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, rgeo_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          if (mod(ntime,6) == 0) then
             ix_up = 6
          else
             ix_up = mod(ntime,6)
          end if
          do ix = 1, ix_up
             if (ix == mod(ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') rgeo_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') rgeo_db(6*(nlines-1)+ix)
             end if
          end do
          rgeo = rgeo_db(arraymin(1))
          
          rgeo_finished = .true.
          finished = phia_finished .and. rgeo_finished .and. amin_finished .and. bt_finished .and. zeff_finished
          deallocate (rgeo_db)

          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line

       else if (trim(var) == "AMIN") then

          allocate (amin_db(6*nlines))
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, amin_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          if (mod(ntime,6) == 0) then
             ix_up = 6
          else
             ix_up = mod(ntime,6)
          end if
          do ix = 1, ix_up
             if (ix == mod(ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') amin_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') amin_db(6*(nlines-1)+ix)
             end if
          end do
          amin = amin_db(arraymin(1))
          
          amin_finished = .true.
          finished = phia_finished .and. rgeo_finished .and. amin_finished .and. bt_finished .and. zeff_finished
          deallocate (amin_db)

          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line

       else if (trim(var) == "BT") then

          allocate (bt_db(6*nlines))
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, bt_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          if (mod(ntime,6) == 0) then
             ix_up = 6
          else
             ix_up = mod(ntime,6)
          end if
          do ix = 1, ix_up
             if (ix == mod(ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') bt_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') bt_db(6*(nlines-1)+ix)
             end if
          end do
          bt = bt_db(arraymin(1))
          
          bt_finished = .true.
          finished = phia_finished .and. rgeo_finished .and. amin_finished .and. bt_finished .and. zeff_finished
          deallocate (bt_db)

          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line

       else if (trim(var) == "ZEFF") then

          allocate (zeff_db(6*nlines))
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, zeff_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          if (mod(ntime,6) == 0) then
             ix_up = 6
          else
             ix_up = mod(ntime,6)
          end if
          do ix = 1, ix_up
             if (ix == mod(ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') zeff_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') zeff_db(6*(nlines-1)+ix)
             end if
          end do
          zeff_0d = zeff_db(arraymin(1))
          
          zeff_finished = .true.
          finished = phia_finished .and. rgeo_finished .and. amin_finished .and. bt_finished .and. zeff_finished
          deallocate (zeff_db)

          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line

       else
             
          ! read in the data for current variable, but do not save it
          do iline = 1, nlines
             read (in_unit,*) line
          end do
          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line
             
       end if

       deallocate (time)
    end do
    close (in_unit)
    finished = .false.

    ! open the input file for reading
    call get_unused_unit (in_unit)
    open (unit=in_unit, file=trim(infile), status="old", action="read",&
         &iostat=ierr)
    if (ierr.ne.0) then
       write(*,'(3A)') "ERROR: file ",trim(infile)," not found!"
       call abort_mp
    endif
    
    ! loop through input file until RMINOR variable has been read in
    do while (.not. finished)

       ! get shot number
       read (in_unit,*,IOSTAT=ierr) line
       ! if end of file, print error message and set flag
       ! so that default values will be used for variables not found
       if (ierr < 0) then
          write (*,*) "Variable RMINOR not found in data file."
          write (*,*) "Running with default flux label from ITER database (sqrt(psitor/psitor_separatrix))."
          write (*,*) "Note that this is inconsistent with using the Miller local equilibrium in GS2."
          write (*,*)
          if (allocated(radtmp)) then
             if (.not. allocated(rad1d)) allocate (rad1d(size(radtmp)))
             rad1d = radtmp; rhotor=radtmp; rmin=rhotor
          endif
          drhotordrho = 1.0; drmindrho = 1.0
          exit
       end if

       ! read in some lines of text
       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line

       ! var is name of current variable in input file
       read (in_unit,*) var, line
       read (in_unit,*) line

       ! nrad_db is number of radial points at which data is given
       read (in_unit,*) nrad_db

       ! ntime is number of time slices at which data is given
       read (in_unit,*) ntime

       ! total number of lines of data for each variable
       nlines = (nrad_db*ntime+5)/6

       allocate (rad_db(nrad_db*ntime))
       allocate (rad2d(nrad_db,ntime))
       allocate (time(ntime))
       allocate (radtmp(nrad_db))       

       ! radtmp is an array containing radial locations (in units of sqrt of normalized toroidal flux)
       read (in_unit,*) radtmp
       ! time is an array containing time slices
       read (in_unit,*) time

       ! if the current variable is the minor radius, read it into the saved array rad1d
       if (trim(var) == "RMINOR") then

          ! location in time array containing the element closest to the user-specified time
          arraymin = minloc(abs(time-target_time))
          
          if (.not. allocated(rad1d)) then
             allocate (rad1d(nrad_db))
             allocate (psi1d(nrad_db))
             allocate (rmin1d(nrad_db))
             allocate (dpsi1d(nrad_db))
             allocate (drmin1d(nrad_db))
             allocate (drad1d(nrad_db))
          end if
          
          if (write_dbinfo) then
             write (db_unit,*) 'variable: ', trim(var)
             write (db_unit,*) 'number of radial points in ITER DB file:', nrad_db
             write (db_unit,*) 'number of time slices in ITER DB file:', ntime
          end if
          
          ! psi1d = radtmp is sqrt(psi_tor/psi_separatrix)
          psi1d = radtmp
          psi1d_out = psi1d
          
          do ix = 1, nlines-1
             read (in_unit,'(A1,6F13.1)') line, rad_db((ix-1)*6+1:ix*6)
          end do
          read (in_unit,'(A1)',advance='no') line
          do ix = 1, mod(nrad_db*ntime,6)
             if (ix == mod(nrad_db*ntime,6)) then
                read (in_unit,'(F13.1)',advance='yes') rad_db(6*(nlines-1)+ix)
             else
                read (in_unit,'(F13.1)',advance='no') rad_db(6*(nlines-1)+ix)
             end if
          end do
             
          ! rad2d has rminor in meters for each time slice
          do it = 1, ntime
             rad2d(:,it) = rad_db ((it-1)*nrad_db+1:it*nrad_db)
          end do
          ! rmin1d has r/a for the requested time slice
          rmin1d = rad2d (:,arraymin(1)) / amin
          rmin1d_out = rmin1d
          
          if (write_dbinfo) then
             do ix = 1, nrad_db
                write (db_unit,*) 'sqrt(torflux): ', psi1d(ix), 'rminor: ', rmin1d(ix)
             end do
             write (db_unit,*)
          end if

          select case (fluxlabel_option_switch)
             
          case (fluxlabel_option_torflux)
             
             ! since flux label is psi1d, set rad1d = psi1d
             ! and dspidrho = 1
             rad1d = psi1d
             drhotordrho = 1.0
             rhotor = rad
             
             ! obtain Delta sqrt(torflux)
             do ix = 1, size(rad1d)-1
                drad1d(ix) = psi1d(ix+1)-psi1d(ix)
             end do
             drad1d(size(psi1d)) = psi1d(size(psi1d))-psi1d(size(psi1d)-1)

             ! get d(r/a)/d(sqrt(torflux))
             call fd3pt (rmin1d, drmin1d, drad1d)
             ! interpolate from data grid for minor radius to radial grid used in Trinity simulation
             do ix = 1, size(rad)
                drmindrho(ix) = fpintrp(rad(ix),drmin1d,rad1d,size(rad1d))
                rmin(ix) = fpintrp(rad(ix),rmin1d,rad1d,size(rad1d))
             end do

          case (fluxlabel_option_aminor)

             ! since flux label is rmin1d, set rad1d = rmin1d
             ! and drmindrho = 1
             rad1d = rmin1d
             drmindrho = 1.0
             rmin = rad
                                       
             ! obtain Delta (r/a)
             do ix = 1, size(rad1d)-1
                drad1d(ix) = rad1d(ix+1)-rad1d(ix)
             end do
             drad1d(size(rad1d)) = rad1d(size(rad1d))-rad1d(size(rad1d)-1)
             
             call fd3pt (psi1d, dpsi1d, drad1d)
             ! interpolate from data grid for minor radius to radial grid used in Trinity simulation
             do ix = 1, size(rad)
                drhotordrho(ix) = fpintrp(rad(ix),dpsi1d,rad1d,size(rad1d))
                rhotor(ix) = fpintrp(rad(ix),psi1d,rad1d,size(rad1d))
             end do
             
          end select
       
          deallocate (dpsi1d, drmin1d)

          finished = .true.
   
       else
          
          ! read in the data for current variable, but do not save it
          do iline = 1, nlines
             read (in_unit,*) line
          end do
          read (in_unit,*) line
          read (in_unit,*) line
          read (in_unit,*) line
             
       end if

       deallocate (rad_db, rad2d, time, radtmp)

    end do
    
!    deallocate (rad_db, rad2d, time, radtmp)

    close (in_unit)

  end subroutine init_iter_db_io

  subroutine iter_db_io (infile, target_time, var_name, rad, var_out, error_flag, inner_values, ndvar)

    use mp_trin, only: abort_mp
    use trinity_input, only: write_dbinfo
    use trinity_input, only: fluxlabel_option_switch, fluxlabel_option_torflux, fluxlabel_option_aminor
    use interp, only: fpintrp, fd3pt, fd5pt
    use file_utils_trin, only: get_unused_unit

    implicit none

    character (*), intent (in) :: infile
    real, intent (in) :: target_time
    character (*), dimension (:), intent (in) :: var_name
    real, dimension (:), intent (in) :: rad
    real, dimension (:), intent (out) :: var_out
    logical, dimension (:), intent (out) :: error_flag
    !> This is set to be the values of the quantites on var_name
    !! on axis. Strictly speaking it is not on axis but on the smallest rho
    !! value, which is typically very close to the axis (and the discrepancy
    !! well within experimental error).
    real, dimension (:), intent (out) :: inner_values
    integer, intent (in), optional :: ndvar

    logical :: finished !, first
!    logical :: first = .true.
    integer :: it, iline, ivar, ix, ierr
    integer :: in_unit, nvars_read, nvars_read_old
    integer :: nrad, ntime, nlines, nvars, nrad_db, nend
    integer, dimension (1) :: arraymin
    character (10) :: var
    character (1000) :: line
    character (13), dimension (:), allocatable :: tmp_array
    real :: dum = -999999.9
    real, dimension (:), allocatable :: time, var_array, radtmp, var1d, dvar1d
    real, dimension (:,:), allocatable :: var2d

    finished = .false.
    nvars_read = 0 ; nvars_read_old = 0

    nvars  = size(var_name)
    nrad = size(rad)

    call get_unused_unit (in_unit)
    open (unit=in_unit, file=trim(infile), status="old", action="read",&
         iostat=ierr)
    if (ierr.ne.0) then
       write(*,'(3A)') "ERROR: file ",trim(infile)," not found!"
       call abort_mp
    endif

    iter_db_io_first = .true. ; error_flag = .false.

    ! continue until all variables asked for have been obtained
    do while (.not. finished)

       ! get shot number
       read (in_unit,*,IOSTAT=ierr) line
       ! if end of file, print error message and set flag
       ! so that default values will be used for variables not found
       if (ierr < 0) then
          do ivar = 1, nvars
             if (abs(var_out(ivar*nrad) + dum) < epsilon(0.0)) then
                error_flag(ivar) = .true.
                write (*,*) "Variable ", trim(var_name(ivar)), " not found in data file."
                write (*,*) "Running with default values."
                write (*,*)
             end if
          end do
          exit
       end if
       ! first read in some lines with info we do not need
       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line
       ! var contains the current ITER DB variable being read
       read (in_unit,*) var, line
       read (in_unit,*) line
       ! nrad_db is the number of radial grid points at which variable info is given
       read (in_unit,*) nrad_db
       ! ntime is the number of time points at which variable info is given
       read (in_unit,*) ntime

       ! number of lines of data for current variable
       nlines = (nrad_db*ntime+5)/6

       if (iter_db_io_first) then
          ! initialize to unphysical value so we can detect
          ! if there are problems later
          var_out = -dum
          error_flag = .false.
          iter_db_io_first = .false.
       end if

       allocate (time(ntime))
       allocate (var_array(nrad_db*ntime)) ; var_array = 0
       allocate (tmp_array(nrad_db*ntime))
       allocate (var2d(nrad_db,ntime))
       allocate (radtmp(nrad_db))       
       allocate (var1d(size(rad1d)))
       allocate (dvar1d(size(rad1d)))
       if (.not. allocated(drad1d)) then
          allocate (drad1d(size(rad1d)))
          do ix = 1, size(rad1d)-1
             drad1d(ix) = rad1d(ix+1)-rad1d(ix)
          end do
          drad1d(size(rad1d)) = rad1d(size(rad1d))-rad1d(size(rad1d)-1)
       end if

       ! radtmp is an array containing default radial variable from ITER DB (sqrt(psitor/psi95))
       read (in_unit,*) radtmp
       ! time is an array containing time points at which data is given
       read (in_unit,*) time

       ! find the location in time array nearest the requested time from TRINITY input file
       arraymin = minloc(abs(time-target_time))

       ! loop over variables to be read from ITER DB file
       do ivar = 1, nvars

          ! if variable in ITER DB file is same as variable we are looking for,
          ! we want to read it into a variable, which we interpolate
          ! onto the Trinity radial grid
          if (trim(var) == trim(var_name(ivar))) then

             if (write_dbinfo) then
                write (db_unit,*) 'variable: ', trim(var)
                write (db_unit,*) 'number of radial points in ITER DB file:', nrad_db
                write (db_unit,*) 'number of time slices in ITER DB file:', ntime
             end if
             ! must treat the last line of data carefully since
             ! it may not contain as many elements (6) as a standard line
!             if (mod(nrad*ntime,6) == 0) then
             if (mod(nrad_db*ntime,6) == 0) then
                nend = nlines
             else
                nend = nlines-1
             end if
             ! read in all lines of data that have 6 elements
             do ix = 1, nend
                read (in_unit,'(A1,6F13.1)') line, var_array ((ix-1)*6+1:ix*6)
             end do
             ! if last line has less than 6 elements read in those elements
             if (nend < nlines) then
                ! read blank space at beginning of line
                read (in_unit,'(A1)',advance='no') line
                ! loop over remaining elements
                do ix = 1, mod(nrad_db*ntime,6)
                   ! if final element, advance to next line after reading. otherwise, do not advance.
                   if (ix == mod(nrad_db*ntime,6)) then
                      read (in_unit,'(F13.1)',advance='yes') var_array (6*(nlines-1)+ix)
                   else
                      read (in_unit,'(F13.1)',advance='no') var_array (6*(nlines-1)+ix)
                   end if
                end do
             end if

             ! convert 1d array with time and radial info into a 2d array
             ! with time and radius indices separated
             do it = 1, ntime
                var2d(:,it) = var_array ((it-1)*nrad_db+1:it*nrad_db)
             end do

             ! if number of data points for this variable is different than number of data points
             ! for minor radius, interpolate variable onto the data grid for the minor radius variable
             if (nrad_db /= size(rad1d)) then
                do ix = 1, size(psi1d)
                   var1d(ix) = fpintrp(psi1d(ix),var2d(:,arraymin(1)),radtmp,nrad_db)
                end do
             else
                var1d = var2d(:,arraymin(1))
             end if

             if (write_dbinfo) then
                select case (fluxlabel_option_switch)
                case (fluxlabel_option_aminor)
                   do ix = 1, size(var1d)
                      write (db_unit,*) 'aminor: ', rad1d(ix), trim(var), ': ', var1d(ix)
                   end do
                case (fluxlabel_option_torflux)
                   do ix = 1, size(var1d)
                      write (db_unit,*) 'sqrt(torflux): ', rad1d(ix), trim(var), ': ', var1d(ix)
                   end do
                end select
                write (db_unit,*)
             end if

             ! interpolate from data grid for minor radius to radial grid used in Trinity simulation
             do ix = 1, nrad
                var_out(nrad*(ivar-1)+ix) = fpintrp(rad(ix),var1d,rad1d,size(rad1d))
             end do
             inner_values(ivar) = var1d(1)
             nvars_read = nvars_read + 1

             if (present(ndvar)) then
                ! if this variable is one of the ones for which we want to calculate the gradient
                ! (such as kappa, delta, etc.), then take the derivative and interpolate it
                ! onto the Trinity radial grid
                if (ivar <= ndvar) then
!                   call fd3pt (var1d, dvar1d, (rad1d(size(rad1d))-rad1d(1))/(size(rad1d)-1))
!                   call fd5pt (var1d, dvar1d, (rad1d(size(rad1d))-rad1d(1))/(size(rad1d)-1))
!                   call fd5pt (var1d, dvar1d, drad1d)
                   call fd3pt (var1d, dvar1d, drad1d)
                   ! for special case of safety factor variable, calculate shat instead of dq/dr
                   if (trim(var) == "Q") dvar1d = dvar1d*rad1d/var1d ! calculate shat instead of dq/dr
                   ! interpolate from data grid for minor radius to radial grid used in Trinity simulation
                   do ix = 1, nrad
                      var_out(nrad*(ivar+nvars-1)+ix) = fpintrp(rad(ix),dvar1d,rad1d,size(rad1d))
                   end do
                   if (write_dbinfo) then
                      select case (fluxlabel_option_switch)
                      case (fluxlabel_option_aminor)
                         do ix = 1, size(var1d)
                            if (trim(var) == "Q") then
                               write (db_unit,*) 'aminor: ', rad1d(ix), 'shat: ', dvar1d(ix)
                            else
                               write (db_unit,*) 'aminor: ', rad1d(ix), 'd', trim(var), '/drho: ', dvar1d(ix)
                            end if
                         end do
                      case (fluxlabel_option_torflux)
                         do ix = 1, size(var1d)
                            if (trim(var) == "Q") then
                               write (db_unit,*) 'sqrt(torflux): ', rad1d(ix), 'shat: ', dvar1d(ix), 'drad1d: '
                            else
                               write (db_unit,*) 'sqrt(torflux): ', rad1d(ix), 'd', trim(var), '/drho: ', dvar1d(ix)
                            end if
                         end do
                      end select
                      write (db_unit,*)
                   end if
                end if
             end if

             exit

          end if
       end do
       if (nvars_read == nvars_read_old) then
          do iline = 1, nlines
             read (in_unit,*) line
          end do
       end if

       read (in_unit,*) line
       read (in_unit,*) line
       read (in_unit,*) line

       if (nvars_read == nvars) finished = .true.

       nvars_read_old = nvars_read
       
       deallocate (time, var_array, tmp_array, var2d, radtmp, var1d, dvar1d)

    end do

    close (in_unit)

  end subroutine iter_db_io


  ! subroutine to read in iterdb files (coming from GAs ONETWO)
  ! not finished but the bones are in place
!   subroutine ga_iter_db_io (infile, rad)

!     use mp_trin, only: abort_mp
!     use file_utils_trin, only: get_unused_unit

!     implicit none

!     character (*), intent (in) :: infile
!     real, dimension (:), intent (in) :: rad

!     integer :: in_unit, ierr, nlines, nend
!     integer :: shot, nrad_db, nspec_db, nprim_db, nimp_db, nneu_db
!     integer :: ibion
!     real :: time_db, rgeo, rmag, r0, kappa, delta, pindent, volo
!     real :: cxareao, btor, totI, ohmI, bsI, beamI, RFI
!     real :: betap, beta, ali, te0, ti0
!     real, dimension (:), allocatable :: psi1d
!     character (2) :: namep, namei, namen
!     character (1000) :: line

!     call get_unused_unit (in_unit)
!     open (unit=in_unit, file=trim(infile), status="old", action="read", &
!          iostat=ierr)
!     if (ierr .ne. 0) then
!        write (*,'(3a)') "Error: File ", trim(infile), " not found."
!        call abort_mp
!     end if

!     ! read in a couple of descriptor lines
!     read (in_unit,*) line
!     read (in_unit,*) line

!     ! shot number
!     read (in_unit,*) shot

!     ! number of radial grid points at which data is given
!     read (in_unit,*) line
!     read (in_unit,*) nrad_db

!     ! number of ion species
!     read (in_unit,*) line
!     read (in_unit,*) nspec_db

!     ! number of primary ion species
!     read (in_unit,*) line
!     read (in_unit,*) nprim_db

!     ! number of impurity ion species
!     read (in_unit,*) line
!     read (in_unit,*) nimp_db

!     ! number of neutral ion species
!     read (in_unit,*) line
!     read (in_unit,*) nneu_db

!     ! index of beam species (-1 is dt mixture)
!     read (in_unit,*) line
!     read (in_unit,*) ibion

!     ! name of primary ion species
!     read (in_unit,*) line
!     read (in_unit,*) namep

!     ! name of impurity ion species
!     read (in_unit,*) line
!     read (in_unit,*) namei

!     ! name of neutral ion species
!     read (in_unit,*) line
!     read (in_unit,*) namen

!     ! time at which data is printed
!     read (in_unit,*) line
!     read (in_unit,*) time_db

!     ! geometrical major radius in meters
!     read (in_unit,*) line
!     read (in_unit,*) rgeo

!     ! major radius of magnetic axis in meters
!     read (in_unit,*) line
!     read (in_unit,*) rmag

!     ! major radius of vacuum btor ref location in meters
!     read (in_unit,*) line
!     read (in_unit,*) r0

!     ! plasma elongation
!     read (in_unit,*) line
!     read (in_unit,*) kappa

!     ! plasma triangularity
!     read (in_unit,*) line
!     read (in_unit,*) delta

!     ! plasma indentation
!     read (in_unit,*) line
!     read (in_unit,*) pindent

!     ! plasma volume in meters**3
!     read (in_unit,*) line
!     read (in_unit,*) volo

!     ! plasma cross-sectional area in meters**2
!     read (in_unit,*) line
!     read (in_unit,*) cxareao

!     ! vacuum toroidal field at rmajor in tesla
!     read (in_unit,*) line
!     read (in_unit,*) btor

!     ! total, ohmic, bootstrap, beam, and RF currents in Amps
!     read (in_unit,*) line
!     read (in_unit,*) totI, ohmI, bsI, beamI, RFI

!     ! poloidal beta
!     read (in_unit,*) line
!     read (in_unit,*) betap

!     ! toroidal beta
!     read (in_unit,*) line
!     read (in_unit,*) beta

!     ! plasma inductance
!     read (in_unit,*) line
!     read (in_unit,*) ali

!     ! central electron temperature
!     read (in_unit,*) line
!     read (in_unit,*) te0

!     ! central ion temperature
!     read (in_unit,*) line
!     read (in_unit,*) ti0
    
!     nlines = (nrad_db+5)/6
    
!     ! must treat the last line of data carefully since
!     ! it may not contain as many elements (6) as a standard line
!     if (mod(nrad_db,6)==0) then
!        nend = nlines
!     else
!        nend = nlines-1
!     end if
    
!     allocate (psi1d(nrad_db))
    
!     ! psi on rho grid in Volt*second/radian
!     read (in_unit,*) line
!     call get_var (psi1d)
    
!     close (in_unit)

!     deallocate (psi1d)
    
!   contains
    
!     subroutine get_var (var)
      
!       implicit none
      
!       real, dimension (:), intent (out) :: var

!       integer :: ix
      
!       ! read in all lines of data that have 6 elements
!       do ix = 1, nend
!          read (in_unit,'(6F13.1)') var((ix-1)*6+1:ix*6)
!       end do
!       ! if last lines has less than 6 elements read in those elements
!       if (nend < nlines) then
!          ! loop over remaining elements
!          do ix = 1, mod(nrad_db,6)
!             ! if final element, advance to next line after reading. otherwise, do not advance.
!             if (ix == mod(nrad_db,6)) then
!                read (in_unit,'(F13.1)',advance='yes') var(6*(nlines-1)+ix)
!             else
!                read (in_unit,'(F13.1)',advance='no') var(6*(nlines-1)+ix)
!             end if
!          end do
!       end if

!     end subroutine get_var
    
!   end subroutine ga_iter_db_io

end module expt_profs
