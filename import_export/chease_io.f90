!> Module to read and write EXPEQ files for CHEASE 
!! See CHEASE paper

module chease_io
  
  public

  real, dimension (:), allocatable :: surface_r
  real, dimension (:), allocatable :: surface_z

  real, dimension (:), allocatable :: pressure_gradient

  real, dimension (:), allocatable :: current_func

  real, dimension (:), allocatable :: flux_coordinate

  integer :: nsurf

  integer :: nflux
  
  integer :: nsttp
  
  integer :: nrhomesh
  
  integer :: infile=1283
  integer :: outfile=1284


  real :: aspct,rz0c,predge

  contains
  subroutine read_infile(filename)
    character(*), intent(in) :: filename
    integer :: i
    open(infile,file= filename)
    read(infile, *) aspct,rz0c,predge
    read(infile, *) nsurf
    allocate(surface_r(nsurf))
    allocate(surface_z(nsurf))
    read(infile, *) (surface_r(i), surface_z(i), i=1,nsurf)
    read(infile, *) nflux
    read(infile, *) nsttp, nrhomesh
    allocate(flux_coordinate(nflux))
    allocate(pressure_gradient(nflux))
    allocate(current_func(nflux))
    read(infile, *) flux_coordinate
    read(infile, *) pressure_gradient
    read(infile, *) current_func
    close(infile)


    
  end subroutine read_infile

  subroutine write_outfile(filename)
    character (*), intent(in) :: filename
    integer :: i
    open(outfile,file= filename)
    write(outfile, '(E18.8)') aspct,rz0c,predge
    write(outfile, '(I5)') nsurf
    write(outfile, '(2E18.8)') (surface_r(i), surface_z(i), i=1,nsurf)
    write(outfile, '(I5)') nflux
    write(outfile, '(2I5)') nsttp, nrhomesh
    write(outfile, '(E18.8)') flux_coordinate
    write(outfile, '(E18.8)') pressure_gradient
    write(outfile, '(E18.8)') current_func
    close(outfile)


    
  end subroutine write_outfile

  subroutine write_outfile_no_surf(filename)
    character (*), intent(in) :: filename
    integer :: i
    open(outfile,file= filename)
    write(outfile, '(E18.8)') aspct,rz0c,predge
    write(outfile, '(I5)') nflux
    write(outfile, '(2I5)') nsttp, nrhomesh
    write(outfile, '(E18.8)') flux_coordinate
    write(outfile, '(E18.8)') pressure_gradient
    write(outfile, '(E18.8)') current_func
    close(outfile)
  end subroutine write_outfile_no_surf
  
  subroutine deallocate_chease_io
    deallocate(surface_r)
    deallocate(surface_z)
    deallocate(flux_coordinate)
    deallocate(pressure_gradient)
    deallocate(current_func)
  end subroutine deallocate_chease_io

end module chease_io

!program test_chease_io
  !use chease_io
  !character (len=80) :: filename
  !filename = 'EXPEQ.OUT'
  !call read_infile(filename)
  !write (*,*) nflux, '<---nflux'
  !filename = 'EXPEQ.OUT.TEST'
  !call write_outfile(filename)
  !call deallocate_chease_io
!end program test_chease_io
