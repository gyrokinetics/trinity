!> A module for importing equilibrium data from the transp
!! netcdf file format.
module transp_import

  use trindiag_config, only: trindiag_type

  implicit none

  public :: import_geometry_transp
  public :: import_initial_profiles_transp
  public :: import_sources_transp
  public :: update_boundary_transp

  private

  interface get_transp_variable
     module procedure get_transp_variable_1d
     module procedure get_transp_variable_2d
  end interface

  ! controls windowed time average for input
  integer :: idx_window = 20
  integer :: idx_window2 = 300

  integer :: nrad_transp, ntime_transp
  integer :: it_idx
  real, dimension (:), allocatable :: time
  real, dimension (:), allocatable :: rhotor_out_transp
  real, dimension (:), allocatable :: aref_vs_time
  real, dimension (:,:), allocatable :: rmin_transp
  real, dimension (:,:), allocatable :: rhotor_transp
  real, dimension (:,:), allocatable :: rhopol_transp
  real, dimension (:,:), allocatable :: rho_transp
  real, dimension (:,:), allocatable :: rad_grid_transp

contains

  subroutine open_transp_file (file_name, gnostics_input, input_time)

    use file_utils_trin, only: error_unit
    use mp_trin, only: proc0, abort_mp
    use simpledataio, only: open_file
    use simpledataio, only: sdatio_init
    use simpledataio, only: dimension_size
    use simpledataio, only: set_dimension_start
    use simpledataio_read, only: read_variable

    implicit none
    
    character(len=*), intent(in) :: file_name
    type(trindiag_type), intent(inout) :: gnostics_input
    real, intent(in) :: input_time
    real :: old_t, new_t, min_t, max_t
    integer :: it
    logical :: has_input_time

    gnostics_input%parallel = .false.
    gnostics_input%wryte = .false.
    gnostics_input%create = .false.
    if (proc0) then
       gnostics_input%reed = .true.
    else
       gnostics_input%reed = .false.
    end if
    if (gnostics_input%reed) then
       call sdatio_init(gnostics_input%sfile, trim(file_name))
       call open_file(gnostics_input%sfile)
       if (.not. allocated(time)) then
          call dimension_size(gnostics_input%sfile, "TIME3", ntime_transp)
          allocate (time(ntime_transp))
          call read_variable(gnostics_input%sfile, "TIME3", time)
       end if
       min_t = time(1) ; max_t = time(ntime_transp)

       if (ntime_transp .eq. 1) then 
          it_idx = ntime_transp
          has_input_time = .true.
       else if (input_time .le. min_t) then
          if (input_time .lt. min_t) &
               write (error_unit(),*) 'WARNING: input_time ', input_time, &
               ' is below the min time value of this input file ', min_t, &
               ' -- setting to min value'
          it_idx = 1
          has_input_time = .true.
       else if (input_time .ge. max_t) then
          if (input_time .gt. max_t) &
               write (error_unit(),*) 'WARNING: input_time ', input_time, &
               ' is above the max time value of this input file ', max_t, &
               ' -- setting to max value'
          it_idx = ntime_transp
          has_input_time = .true.
       else
          has_input_time = .false.
          old_t = min_t
          do it = 2, ntime_transp
             new_t = time(it)
             if (it > 1 .and. old_t.le.input_time .and. &
                  new_t .gt. input_time) then
                has_input_time = .true.
                if (input_time - old_t < (new_t-old_t)/2.0) then
                   it_idx = it-1
                else
                   it_idx = it
                end if
                exit
             end if
             old_t = new_t
          end do

       end if
       if (.not. has_input_time) then
          if (proc0) write (*,*) 'ERROR: input_time is outside the range of time values&
               & in this input file. input_time is ', input_time, ' and must be&
               & between ', min_t , 'and', max_t
          call abort_mp
       end if

       ! obtains rhotor grid used in TRANSP
       call get_rho_grid (gnostics_input%sfile)

    end if

  end subroutine open_transp_file

  subroutine close_transp_file(gnostics_input)
    use mp_trin, only: proc0
    use simpledataio, only: closefile
    use simpledataio, only: sdatio_free
    type(trindiag_type), intent(inout) :: gnostics_input
    if (proc0) then
      call closefile(gnostics_input%sfile)
      call sdatio_free(gnostics_input%sfile)
    end if
  end subroutine close_transp_file

  subroutine import_geometry_transp (target_time_trin)

    use trinity_input, only: geo_file
    use trinity_input, only: geo_time

    real, intent (in) :: target_time_trin

    type(trindiag_type) :: gnostics_input

    real :: target_time
    real :: rad_out_input
    integer :: nrad_input

    target_time = target_time_trin + geo_time

    call open_transp_file(geo_file, gnostics_input, target_time)
    call get_transp_geo(gnostics_input%sfile)
    call close_transp_file(gnostics_input)

  end subroutine import_geometry_transp

  subroutine get_rho_grid (sfile)

    use mp_trin, only: proc0
    use simpledataio, only: sdatio_file, dimension_size
    use simpledataio_read, only: read_variable
    use trinity_input, only: fluxlabel_option_switch
    use trinity_input, only: fluxlabel_option_torflux
    use trinity_input, only: fluxlabel_option_aminor
    use trinity_input, only: aspr
    use norms, only: psitor_a, psipol_a, amin_ref, aref

    implicit none

    type(sdatio_file), intent(inout) :: sfile

    integer :: i, j
    real, dimension (:,:), allocatable :: rmaj_transp
    real, dimension (:,:), allocatable :: psitor_transp, psipol_transp

    if (.not. allocated(rhotor_transp)) then
       call dimension_size(sfile, "XB", nrad_transp)
       allocate (rhotor_transp(nrad_transp,ntime_transp))
       call read_variable(sfile, "XB", rhotor_transp)
       allocate (rhotor_out_transp(ntime_transp))
       rhotor_out_transp = rhotor_transp(nrad_transp,:)

       ! rmin_transp is r in cm on transp grid
       allocate (rmin_transp(nrad_transp,ntime_transp))
       call read_variable(sfile, "RMNMP", rmin_transp)
       ! amin_ref is minor radius of LCFS in meters
       allocate (aref_vs_time(ntime_transp))
       aref_vs_time = rmin_transp(nrad_transp,:)*0.01
       amin_ref = aref_vs_time(it_idx)
       ! rho_transp = r/a on transp grid
       allocate (rho_transp(nrad_transp,ntime_transp))
       rho_transp = rmin_transp/spread(rmin_transp(nrad_transp,:),1,nrad_transp)

       ! rmaj_transp is geometric major radius in cm on transp grid
       allocate (rmaj_transp(nrad_transp,ntime_transp))
       call read_variable(sfile, "RMJMP", rmaj_transp)
       ! aspr is aspect ratio of LCFS
       aspr = rmaj_transp(nrad_transp,it_idx)/rmin_transp(nrad_transp,it_idx)
       deallocate (rmaj_transp)

       ! may want to know the enclosed toroidal flux
       ! at LCFS, so compute it here (in Webers)
       ! as psitor_a
       allocate (psitor_transp(nrad_transp,ntime_transp))
       call read_variable(sfile, "TRFLX", psitor_transp)
       psitor_a = psitor_transp(nrad_transp,it_idx)
       deallocate (psitor_transp)

       ! may want to know the enclosed poloidal flux
       ! at LCFS, so compute it here (in Webers)
       ! as psipol_a
       allocate (psipol_transp(nrad_transp,ntime_transp))
       call read_variable(sfile, "PLFLX", psipol_transp)
       psipol_a = psipol_transp(nrad_transp,it_idx)
       allocate (rhopol_transp(nrad_transp,ntime_transp))
       rhopol_transp = sqrt(psipol_transp/spread(psipol_transp(nrad_transp,:),1,nrad_transp))
       deallocate (psipol_transp)

       ! could be rhotor or rho, depending on fluxlabel_option
       allocate (rad_grid_transp(nrad_transp,ntime_transp))

       select case (fluxlabel_option_switch)
       case (fluxlabel_option_torflux)
          if (proc0) write (*,*) 'WARNING: currently using aref=amin_ref for all flux labels with TRANSP'
          aref = amin_ref
          rad_grid_transp = rhotor_transp
       case (fluxlabel_option_aminor)
          aref = amin_ref
          rad_grid_transp = rho_transp
       end select
       
    end if

  end subroutine get_rho_grid

  subroutine get_transp_geo (sfile)

    use interp, only: spline, smooth
    use simpledataio, only: sdatio_file
    use mp_trin, only: proc0, broadcast
    use nteqns_arrays, only: rad_grid
    use nteqns_arrays, only: rmin_grid, drmindrho_grid
    use nteqns_arrays, only: rmajor_grid, shift_grid
    use nteqns_arrays, only: qval_grid, shat_grid
    use nteqns_arrays, only: kappa_grid, kapprim_grid
    use nteqns_arrays, only: delta_grid, deltprim_grid
    use nteqns_arrays, only: rhotor_grid, drhotordrho_grid
    use nteqns_arrays, only: rhopol_grid, drhopoldrho_grid
    use nteqns_arrays, only: psipol_grid, dpsipoldrho_grid
    use nteqns_arrays, only: btori_grid, bunit_grid
    use nteqns_arrays, only: area_grid, grho_grid
    use trinity_input, only: nrad
    use miller_geometry, only: init_miller_geo, get_Iflxfnc, get_grho_av
    use norms, only: psitor_a, aref

    implicit none

    type(sdatio_file), intent (in) :: sfile

    integer :: i
    real, dimension (:), allocatable :: dum1d
    real, dimension (:,:), allocatable :: dum2d, dum2d_smooth

    if (proc0) then
       allocate (dum1d(nrad))
       allocate (dum2d(nrad_transp,ntime_transp))
       allocate (dum2d_smooth(nrad_transp,ntime_transp))
       
       call get_transp_variable (sfile, "RMJMP", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       ! convert from R in cm to R/aref
       dum2d_smooth = 0.01*dum2d_smooth/spread(aref_vs_time,1,nrad_transp)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, rmajor_grid, shift_grid)

       call get_transp_variable (sfile, "RMNMP", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       ! convert from r in cm to r/aref
       dum2d_smooth = 0.01*dum2d_smooth/spread(aref_vs_time,1,nrad_transp)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, rmin_grid, drmindrho_grid)

       call get_transp_variable (sfile, "Q", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, qval_grid, dum1d)
       shat_grid = dum1d*rad_grid/qval_grid
       
       call get_transp_variable (sfile, "ELONG", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, kappa_grid, kapprim_grid)
       
       call get_transp_variable (sfile, "TRIANG", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, delta_grid, deltprim_grid)
       
       ! this is area of the flux surface in cm^2
       call get_transp_variable (sfile, "SURF", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, area_grid, dum1d)
       ! convert to area/aref^2. note aref is in meters and area in cm.
       area_grid = area_grid/(100.*aref)**2

       call get_transp_variable (sfile, "PLFLX", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, psipol_grid, dpsipoldrho_grid)
       
       call spline (rad_grid_transp(:,it_idx), rhotor_transp(:,it_idx), &
            rad_grid, rhotor_grid, drhotordrho_grid)
       call spline (rad_grid_transp(:,it_idx), rhopol_transp(:,it_idx), &
            rad_grid, rhopol_grid, drhopoldrho_grid)
       
       call init_miller_geo (rad_grid, rmajor_grid, shift_grid, &
            kappa_grid, kapprim_grid, delta_grid, deltprim_grid)
       call get_Iflxfnc (rad_grid, rhotor_transp(:,it_idx), 0.01*rmin_transp(:,it_idx), &
            psitor_a, btori_grid, bunit_grid)
       ! this is flux surface average of |grad r|
       call get_grho_av (grho_grid)
       ! want flux surface average of |aref*grad rho|
       ! no change below if rho = r/a -- otherwise converts to grad rhotor
       grho_grid = grho_grid/drmindrho_grid

       deallocate (dum1d, dum2d, dum2d_smooth)
    end if

    ! may be able to comment this out
    ! need to check if broadcasted at initial time step otherwise
    call broadcast (rmajor_grid)
    call broadcast (shift_grid)
    call broadcast (rmin_grid)
    call broadcast (drmindrho_grid)
    call broadcast (qval_grid)
    call broadcast (shat_grid)
    call broadcast (kappa_grid)
    call broadcast (kapprim_grid)
    call broadcast (delta_grid)
    call broadcast (deltprim_grid)
    call broadcast (area_grid)
    call broadcast (psipol_grid)
    call broadcast (dpsipoldrho_grid)
    call broadcast (rhotor_grid)
    call broadcast (drhotordrho_grid)
    call broadcast (rhopol_grid)
    call broadcast (drhopoldrho_grid)
    call broadcast (btori_grid)
    call broadcast (bunit_grid)
    call broadcast (grho_grid)

  end subroutine get_transp_geo

  subroutine get_transp_variable_1d (sfile, var_name, var_arr)

    use simpledataio, only: variable_exists, sdatio_file
    use simpledataio_read, only: read_variable

    implicit none

    type(sdatio_file), intent(in) :: sfile
    character (*), intent (in) :: var_name
    real, dimension (:), intent (out) :: var_arr

    logical :: exists

    exists = variable_exists(sfile, var_name)
    if (exists) then
       call read_variable(sfile, var_name, var_arr)
    else
       write (*,*) 'WARNING: ', var_name, 'not found in TRANSP file.  Setting to zero.'
       var_arr = 0.0
    end if

  end subroutine get_transp_variable_1d

  subroutine get_transp_variable_2d (sfile, var_name, var_arr)

    use simpledataio, only: variable_exists, sdatio_file
    use simpledataio_read, only: read_variable

    implicit none

    type(sdatio_file), intent(in) :: sfile
    character (*), intent (in) :: var_name
    real, dimension (:,:), intent (out) :: var_arr

    logical :: exists

    exists = variable_exists(sfile, var_name)
    if (exists) then
       call read_variable(sfile, var_name, var_arr)
    else
       write (*,*) 'WARNING: ', var_name, 'not found in TRANSP file.  Setting to zero.'
       var_arr = 0.0
    end if

  end subroutine get_transp_variable_2d

  subroutine import_initial_profiles_transp

    use mp_trin, only: abort_mp, proc0
    use trinity_input, only: init_file
    use trinity_input, only: init_time
    use trinity_input, only: n_ion_spec

    type(trindiag_type) :: gnostics_input
    real :: rad_out_input
    integer :: nrad_input

    if (n_ion_spec > 2) then
       if (proc0) write (*,*) &
            "ERROR: TRANSP input option restricted to deuterium ion species and composite impurity species.  Aborting."
       call abort_mp
    else if (n_ion_spec == 2) then
       if (proc0) write (*,*) &
            'INFO: TRANSP input option currently treats only single composite impurity.'
    else
       if (proc0) write (*,*) &
            'INFO: TRANSP input option currently treats only deuterium main ion species'
    end if

    call open_transp_file(init_file, gnostics_input, init_time)
    call get_transp_profs(gnostics_input%sfile)
    call close_transp_file(gnostics_input)

  end subroutine import_initial_profiles_transp

  subroutine get_transp_profs (sfile)

    use interp, only: spline, smooth
    use simpledataio, only: sdatio_file
    use mp_trin, only: proc0, broadcast
    use nteqns_arrays, only: dens_grid, fprim_grid
    use nteqns_arrays, only: temp_grid, tprim_grid
    use nteqns_arrays, only: pres_grid, pprim_grid
    use nteqns_arrays, only: omega_grid, mom_grid, gexb_grid
    use nteqns_arrays, only: inv_mominertia_fnc
    use nteqns_arrays, only: rad_grid, rmajor_grid, qval_grid
    use nteqns_arrays, only: zeff_grid
    use trinity_input, only: nrad, n_ion_spec, m_ion, z_ion
    use trinity_input, only: set_mratio_and_charge
    use norms, only: aref, vtref

    implicit none

    type(sdatio_file), intent (in) :: sfile

    integer :: i
    real, dimension (:), allocatable :: dum1d
    real, dimension (:,:), allocatable :: dum2d, dum2d_smooth

    if (proc0) then
       allocate (dum1d(nrad))
       allocate (dum2d(nrad_transp,ntime_transp))
       allocate (dum2d_smooth(nrad_transp,ntime_transp))
       
       ! NE is electron density in 1/cm^3
       call get_transp_variable (sfile, "NE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, dens_grid(:,1), fprim_grid(:,1))
       ! ND is deuterium density in 1/cm^3
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "ND", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, dens_grid(:,2), fprim_grid(:,2))
       if (n_ion_spec == 2) then
          ! include a composite impurity species
          ! that accounts for all impurities
          call get_transp_variable (sfile, "NIMP", dum2d)
          call smooth (time, idx_window, dum2d, dum2d_smooth)
          call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
               rad_grid, dens_grid(:,3), fprim_grid(:,3))
       end if
       ! convert from dn/drho to -1/n * dn/drho
       fprim_grid = -fprim_grid/dens_grid
       ! convert dens from 1/cm^3 to 10^20/m^3
       dens_grid = dens_grid * 1.e-14
       
       ! TE is electron temperature in eV
       call get_transp_variable (sfile, "TE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, temp_grid(:,1), tprim_grid(:,1))
       ! TI is deuterium temperature in eV
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "TI", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, temp_grid(:,2), tprim_grid(:,2))
       if (n_ion_spec == 2) then
          ! include a composite impurity species
          ! that accounts for all impurities
          write (*,*) "INFO: TRANSP input does not contain impurity temperature info."
          write (*,*) "INFO: Setting initial impurity temperature equal to deuterium temperature."
          temp_grid(:,3) = temp_grid(:,2)
          tprim_grid(:,3) = tprim_grid(:,2)
       end if
       
       ! convert from dn/drho to -1/T * dT/drho
       tprim_grid = -tprim_grid/temp_grid
       ! convert from temp in eV to keV
       temp_grid = temp_grid * 1.e-3
       
       pres_grid = dens_grid*temp_grid
       pprim_grid = fprim_grid + tprim_grid
       
       ! OMEGA is toroidal angular velocity in rad/sec
       ! difference with OMEGDATA?
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "OMEGA", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       ! normalize omega by a/vt
       dum2d_smooth = dum2d_smooth*aref/vtref
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, omega_grid, gexb_grid)
       mom_grid = omega_grid/inv_mominertia_fnc(dens_grid,rmajor_grid)
       ! gamma_{ExB} = (rho/q)(domega/drho) (a/v_{t,0})
       gexb_grid = gexb_grid*rad_grid/qval_grid
       
       m_ion(1) = 2.0
       z_ion(1) = 1.0
       if (n_ion_spec == 2) then
          call get_transp_variable (sfile, "XZIMP", dum2d(1,:))
          z_ion(2) = dum2d(1,it_idx)
          call get_transp_variable (sfile, "AIMP", dum2d(1,:))
          m_ion(2) = dum2d(1,it_idx)
          zeff_grid = (z_ion(2)**2*dens_grid(:,3)+dens_grid(:,2))/dens_grid(:,1)
          write (*,*) 'INFO: using TRANSP impurity with charge number ', z_ion(2), &
               ' and atomic number ', m_ion(2)
       else
          call get_transp_variable (sfile, "ZEFFP", dum2d)
          call smooth (time, idx_window, dum2d, dum2d_smooth)
          call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
               rad_grid, zeff_grid, dum1d)
       end if

       deallocate (dum1d, dum2d, dum2d_smooth)
    end if

    call broadcast (dens_grid)
    call broadcast (fprim_grid)
    call broadcast (temp_grid)
    call broadcast (tprim_grid)
    call broadcast (pres_grid)
    call broadcast (pprim_grid)
    call broadcast (omega_grid)
    call broadcast (gexb_grid)
    call broadcast (m_ion)
    call broadcast (z_ion)
    call broadcast (zeff_grid)

    call set_mratio_and_charge

  end subroutine get_transp_profs

  subroutine import_sources_transp (target_time_trin)

    use trinity_input, only: source_file
    use trinity_input, only: geo_time

    implicit none

    real, intent (in) :: target_time_trin

    type(trindiag_type) :: gnostics_input
    integer :: nrad_input
    real :: rad_out_input
    real :: target_time

    target_time = geo_time + target_time_trin

    call open_transp_file(source_file, gnostics_input, target_time)
    call get_transp_sources(gnostics_input%sfile)
    call close_transp_file(gnostics_input)

  end subroutine import_sources_transp

  subroutine get_transp_sources (sfile)

    use interp, only: spline, smooth
    use simpledataio, only: sdatio_file
    use mp_trin, only: proc0, broadcast
    use nteqns_arrays, only: src_part_nbi, src_part_wall
    use nteqns_arrays, only: src_part_ionisation
    use nteqns_arrays, only: src_part_unspecified
    use nteqns_arrays, only: src_part_dndt
    use nteqns_arrays, only: src_heat_nbi, src_heat_ohm
    use nteqns_arrays, only: src_mom_unspecified
    use nteqns_arrays, only: rad_grid
    use trinity_input, only: n_ion_spec, nrad
    use trinity_input, only: evolve_sources
    use trinity_input, only: adjust_beam_modulation, modulation_factor

    implicit none

    type(sdatio_file), intent (in) :: sfile

    integer :: i, j
    real, dimension (:), allocatable :: dum1d, tmp
    real, dimension (:,:), allocatable :: dum2d, dum2d_smooth

    if (proc0) then
       allocate (dum1d(nrad), tmp(nrad))
       allocate (dum2d(nrad_transp,ntime_transp))
       allocate (dum2d_smooth(nrad_transp,ntime_transp))
       
       ! NBI deposition electron source (1/cm^3/s)
       ! seems to be same as SBE_D
       call get_transp_variable (sfile, "SBE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_nbi(:,1), dum1d)
       ! NBI ion particle deposition
       ! not sure of difference with BDEP_D
       call get_transp_variable (sfile, "SVD", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_nbi(:,2), dum1d)
       src_part_nbi = src_part_nbi*1.e6
       
       ! Electron particle source coming from wall
       call get_transp_variable (sfile, "SCEW", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_wall(:,1), dum1d)
       ! ion particle source coming from wall
       call get_transp_variable (sfile, "SWD", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_wall(:,2), dum1d)
       src_part_wall = src_part_wall*1.e6
       
       ! electron particle source due to impurity ionization
       call get_transp_variable (sfile, "SCEZ", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_ionisation(:,1), dum1d)
       src_part_ionisation(:,1) = src_part_ionisation(:,1)*1.e6    
       
       ! Electron particle source coming from thermal neutrals
       call get_transp_variable (sfile, "SCEE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, tmp, dum1d)
       
       ! electron particle source coming from volume neutrals (recombination?)
       call get_transp_variable (sfile, "SCEV", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_part_unspecified(:,1), dum1d)
       src_part_unspecified(:,1) = src_part_unspecified(:,1) + tmp
       src_part_unspecified(:,1) = src_part_unspecified(:,1)*1.e6
       
       ! if doing steady-state simulation
       if (.not. evolve_sources) then
          call get_transp_variable (sfile, "DNEDT", dum2d)
          call smooth (time, idx_window, dum2d, dum2d_smooth)
          call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
               rad_grid, src_part_dndt(:,1), dum1d)
          call get_transp_variable (sfile, "DNDDT", dum2d)
          call smooth (time, idx_window, dum2d, dum2d_smooth)
          call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
               rad_grid, src_part_dndt(:,2), dum1d)
          if (n_ion_spec == 2) then
             call get_transp_variable (sfile, "DNIMP", dum2d)
             call smooth (time, idx_window, dum2d, dum2d_smooth)
             call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
                  rad_grid, src_part_dndt(:,3), dum1d)
          end if
          src_part_dndt = src_part_dndt*1.e6
       end if
       
       ! neutral beam ion power deposition (MW/m^3)
       call get_transp_variable (sfile, "PBE", dum2d)
       call smooth(time, idx_window, dum2d, dum2d_smooth)
       if (adjust_beam_modulation) then
          ! get un-modulated beam deposition (average over modulation)
          call smooth(time, idx_window2, dum2d_smooth, dum2d)
          ! dum2d_smooth - dum2d is the modulation in beam deposition
          dum2d_smooth = dum2d + modulation_factor*(dum2d_smooth-dum2d)
       end if
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_heat_nbi(:,1), dum1d)
       call get_transp_variable (sfile, "PBI", dum2d)
       call smooth(time, idx_window, dum2d, dum2d_smooth)
       if (adjust_beam_modulation) then
          ! get un-modulated beam deposition (average over modulation)
          call smooth(time, idx_window2, dum2d_smooth, dum2d)
          ! dum2d_smooth - dum2d is the modulation in beam deposition
          dum2d_smooth = dum2d + modulation_factor*(dum2d_smooth-dum2d)
       end if
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_heat_nbi(:,2), dum1d)
       src_heat_nbi = src_heat_nbi*1.e6
       
       call get_transp_variable (sfile, "POH", dum2d)
       call smooth(time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_heat_ohm(:,1), dum1d)
       src_heat_ohm = src_heat_ohm*1.e6
       
       ! TQIN is total input torque in Nt*m/cm^3
       call get_transp_variable (sfile, "TQIN", dum2d)
       call smooth(time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            rad_grid, src_mom_unspecified, dum1d)
       ! convert to N/m^2
       src_mom_unspecified = src_mom_unspecified*1.e6
       
       deallocate (dum1d, dum2d, dum2d_smooth, tmp)
    end if

    call broadcast (src_part_nbi)
    call broadcast (src_part_wall)
    call broadcast (src_part_ionisation)
    call broadcast (src_part_unspecified)
    call broadcast (src_part_dndt)
    call broadcast (src_heat_nbi)
    call broadcast (src_heat_ohm)
    call broadcast (src_mom_unspecified)

  end subroutine get_transp_sources

  subroutine update_boundary_transp (target_time_trin)

    use interp, only: spline, smooth
    use simpledataio, only: sdatio_file
    use mp_trin, only: proc0, broadcast
    use nteqns_arrays, only: dens_grid, fprim_grid
    use trinity_input, only: init_time, init_file
    use trinity_input, only: nrad, n_ion_spec
    use norms, only: aref, vtref
    use nteqns_arrays, only: temp_grid, tprim_grid
    use nteqns_arrays, only: pres_grid, pprim_grid
    use nteqns_arrays, only: omega_grid, mom_grid, gexb_grid
    use nteqns_arrays, only: inv_mominertia_fnc
    use nteqns_arrays, only: rad_grid, rmajor_grid, qval_grid

    implicit none

    real, intent (in) :: target_time_trin

    type(trindiag_type) :: gnostics_input
    type(sdatio_file) :: sfile
    integer :: i
    real :: target_time
    real, dimension (:), allocatable :: radout, dum1, dum2, tmp
    real, dimension (:,:), allocatable :: dum2d, dum2d_smooth

    target_time = init_time + target_time_trin

    call open_transp_file (init_file, gnostics_input, target_time)
    sfile = gnostics_input%sfile
    if (proc0) then
       allocate (radout(1), dum1(1), dum2(1))
       allocate (tmp(nrad))
       allocate (dum2d(nrad_transp,ntime_transp))
       allocate (dum2d_smooth(nrad_transp,ntime_transp))
       
       radout = rad_grid(nrad)

       ! NE is electron density in 1/cm^3
       call get_transp_variable (sfile, "NE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            radout, dum1, dum2)
       dens_grid(nrad,1) = dum1(1)
       fprim_grid(nrad,1) = dum2(1)
       ! ND is deuterium density in 1/cm^3
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "ND", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            radout, dum1, dum2)
       dens_grid(nrad,2) = dum1(1)
       fprim_grid(nrad,2) = dum2(1)
       if (n_ion_spec == 2) then
          ! include a composite impurity species
          ! that accounts for all impurities
          call get_transp_variable (sfile, "NIMP", dum2d)
          call smooth (time, idx_window, dum2d, dum2d_smooth)
          call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
               radout, dum1, dum2)
          dens_grid(nrad,3) = dum1(1)
          fprim_grid(nrad,3) = dum2(1)
       end if
       ! convert from dn/drho to -1/n * dn/drho
       fprim_grid(nrad,:) = -fprim_grid(nrad,:)/dens_grid(nrad,:)
       ! convert dens from 1/cm^3 to 10^20/m^3
       dens_grid(nrad,:) = dens_grid(nrad,:) * 1.e-14
       
       ! TE is electron temperature in eV
       call get_transp_variable (sfile, "TE", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            radout, dum1, dum2)
       temp_grid(nrad,1) = dum1(1)
       tprim_grid(nrad,1) = dum2(1)
       ! TI is deuterium temperature in eV
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "TI", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            radout, dum1, dum2)
       temp_grid(nrad,2) = dum1(1)
       tprim_grid(nrad,2) = dum2(1)
       if (n_ion_spec == 2) then
          ! include a composite impurity species
          ! that accounts for all impurities
          temp_grid(nrad,3) = temp_grid(nrad,2)
          tprim_grid(nrad,3) = tprim_grid(nrad,2)
       end if
       
       ! convert from dn/drho to -1/T * dT/drho
       tprim_grid(nrad,:) = -tprim_grid(nrad,:)/temp_grid(nrad,:)
       ! convert from temp in eV to keV
       temp_grid(nrad,:) = temp_grid(nrad,:) * 1.e-3
       
       pres_grid(nrad,:) = dens_grid(nrad,:)*temp_grid(nrad,:)
       pprim_grid(nrad,:) = fprim_grid(nrad,:) + tprim_grid(nrad,:)
       
       ! OMEGA is toroidal angular velocity in rad/sec
       ! difference with OMEGDATA?
       ! TRANSP input doesn't seem to provide other options
       call get_transp_variable (sfile, "OMEGA", dum2d)
       call smooth (time, idx_window, dum2d, dum2d_smooth)
       ! normalize omega by a/vt
       dum2d_smooth = dum2d_smooth*aref/vtref
       call spline (rad_grid_transp(:,it_idx), dum2d_smooth(:,it_idx), &
            radout, dum1, dum2)
       omega_grid(nrad) = dum1(1)
       gexb_grid(nrad) = dum2(1)
       tmp = inv_mominertia_fnc(dens_grid,rmajor_grid)
       mom_grid(nrad) = omega_grid(nrad)/tmp(nrad)
       ! gamma_{ExB} = (rho/q)(domega/drho) (a/v_{t,0})
       gexb_grid(nrad) = gexb_grid(nrad)*rad_grid(nrad)/qval_grid(nrad)

       deallocate (radout, dum2d, dum2d_smooth, dum1, dum2, tmp)
    end if
    call close_transp_file (gnostics_input)

  end subroutine update_boundary_transp

  subroutine finish_transp_import

    implicit none

    if (allocated(time)) deallocate(time)
    if (allocated(rhotor_transp)) deallocate(rhotor_transp)
    if (allocated(rhopol_transp)) deallocate(rhopol_transp)
    if (allocated(rhotor_out_transp)) deallocate(rhotor_out_transp)
    if (allocated(rho_transp)) deallocate (rho_transp)
    if (allocated(rad_grid_transp)) deallocate (rad_grid_transp)
    if (allocated(aref_vs_time)) deallocate (aref_vs_time)
    if (allocated(rmin_transp)) deallocate (rmin_transp)

  end subroutine finish_transp_import

end module transp_import
