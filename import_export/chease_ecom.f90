!> This is a module for handling interactions with the Grad-Shafranov
!! solvers CHEASE and ECOM. It handles reading the output from these
!! codes, and also handles updating their input pressure profiles and
!! rerunning them to get an updated equilibrium, when evolve_geometry
!! is set to true.
module chease_ecom

  type chease_ecom_type
    logical :: initialized = .false.
    logical :: should_run_chease = .false.
    character(2000) :: chease_executable = './chease'
    character(2000) :: chease_indata_file = 'chease/EXPEQ'
    character(2000) :: chease_outdata_file = 'chease/EXPEQ.OUT'
    !> Size of internal CHEASE/ECOM grid
    integer :: ncegrid
    !> Size of array of boundary points
    integer :: ncesurf
    !> Number of CHEASE runs during the trinity sim
    integer :: nruns
    !> Values of original C/E flux coordinate
    real, dimension(:), allocatable :: cegrid_original
    !> Values of current C/E flux coordinate
    real, dimension(:), allocatable :: cegrid
    !> Values of original C/E pressure gradient
    real, dimension(:), allocatable :: dpdpsi_cegrid_original
    !> Values of current C/E pressure gradient
    real, dimension(:), allocatable :: dpdpsi_cegrid
    !> Values of current C/E current function
    real, dimension(:), allocatable :: current_function_cegrid
    !> Integer specifying (when using CHEASE) which current function we are using,
    !! see CHEASE paper for details
    integer :: current_function_def
    !> Values of major radius on the boundary surface
    real, dimension(:), allocatable :: boundary_r
    !> Values of vertical height on the boundary surface
    real, dimension(:), allocatable :: boundary_z
    real, dimension(:), allocatable :: pressure_psigrid
  end type chease_ecom_type

  contains

    subroutine init_chease_ecom(checom, ncegrid, ncesurf)
      type(chease_ecom_type), intent(inout) :: checom
      integer, intent(in) :: ncegrid, ncesurf
      allocate(checom%cegrid_original(ncegrid))
      allocate(checom%cegrid(ncegrid))
      allocate(checom%dpdpsi_cegrid_original(ncegrid))
      allocate(checom%dpdpsi_cegrid(ncegrid))
      allocate(checom%current_function_cegrid(ncegrid))
      allocate(checom%boundary_r(ncesurf))
      allocate(checom%boundary_z(ncesurf))
      checom%ncegrid = ncegrid
      checom%ncesurf = ncesurf
      checom%initialized = .true.
      checom%nruns = 1
    end subroutine init_chease_ecom

    subroutine finish_chease_ecom(checom)
      type(chease_ecom_type), intent(inout) :: checom
      deallocate(checom%cegrid_original)
      deallocate(checom%cegrid)
      deallocate(checom%dpdpsi_cegrid_original)
      deallocate(checom%dpdpsi_cegrid)
      deallocate(checom%current_function_cegrid)
      deallocate(checom%boundary_r)
      deallocate(checom%boundary_z)
      checom%initialized = .false.
    end subroutine finish_chease_ecom

    function should_restart_chease_ecom(checom)
      use trinity_input, only: init_option_switch, init_option_restart
      type(chease_ecom_type), intent(inout) :: checom
      logical :: should_restart_chease_ecom
      should_restart_chease_ecom = &
        init_option_switch == init_option_restart .and. &
        .not. checom%initialized
    end function should_restart_chease_ecom

    function should_update_chease_ecom(checom)
      use trinity_input, only: geo_option_switch, geo_option_chease
      use trinity_input, only: geo_option_ecom
      type(chease_ecom_type), intent(inout) :: checom
      logical :: should_update_chease_ecom

      ! Check if the EXPEQ.OUT is available
      inquire(file=checom%chease_outdata_file, exist=should_update_chease_ecom)

      ! Check if geometry is coming from chease/ecom.
      should_update_chease_ecom = should_update_chease_ecom .and. &
        (geo_option_switch == geo_option_chease .or. &
         geo_option_switch == geo_option_ecom) 
    end function should_update_chease_ecom

  !> This updates the CHEASE input data file (EXPEQ.OUT) with the pressure gradient
  !! that has been calculated from Trinity. 

  function update_chease(checom, should_run_chease)

    !use mp_trin, only: proc0
    use chease_io, only: read_infile, write_outfile, deallocate_chease_io
    use chease_io, only: write_outfile_no_surf
    use chease_io, only: pressure_gradient, flux_coordinate, current_func, nsurf
    use chease_io, only: nsurf, surface_r, surface_z
    use ecom_io, only: pressure, ecom_flux_coordinate=>flux_coordinate
    use ecom_io, only: ecom_read_infile, ecom_write_outfile, deallocate_ecom_io
    !use trinity_input, only: chease_infile
    use interp, only: spline 
    use trinity_input, only: nrad, nspec
    use trinity_input, only: geo_option_switch
    use trinity_input, only: geo_option_chease
    use trinity_input, only: geo_option_ecom
    use nteqns_arrays, only: pprim_grid, dpsipoldrho_grid, pres_grid
    use nteqns_arrays, only: rhotor_grid, rhopol_grid
    use norms, only: b0exp_chse, r0exp_chse, psipol_a
    use unit_tests_trin, only: debug_message, verb_timestep
    use syscall_pli, only: syscall

    !use read_chease
!
    implicit none
    real, dimension (:), allocatable :: s_grid, dpdpsipol_grid, dummy
    real, dimension (:), allocatable :: pressure_gradient_temp, flux_coordinate_temp
    real, dimension (:), allocatable :: pressure_temp
    integer :: is, iedge, i, ierror
    character(len=80) :: filename
    type(chease_ecom_type), intent(inout) :: checom
    logical, intent(in), optional :: should_run_chease
    logical :: should_run_chease_actual
    integer :: update_chease

    should_run_chease_actual = .true.
    if (present(should_run_chease)) should_run_chease_actual = should_run_chease

    update_chease = 0

    if (geo_option_switch.eq.geo_option_chease) then

      allocate(dpdpsipol_grid(nrad))
      allocate(s_grid(nrad))

      ! pprim_grid =
      ! - 1 / p dp / drho
      ! Need dp /  drhopol
      ! = sum over species (- pprim_grid / dpsipoldrho_grid * pres_grid)
      ! 
      ! use spline in interp.f90
      ! (rad_grid, above, flux_function, return_value (ie. pressure_gradient),
      ! dummy)


      ! in CHEASE paper s = (1-psipol_CH/psipolmin_CH)^(1/2)
      ! psipol_CH is 0 at the boundary and negative inside.
      ! Therefore psipolmin_CH = -psipol_a where psipol_a is
      ! the poloidal flux enclosed by the boundary in trinity terms
      ! and psipol_CH = (psipol - psipol_a)
      ! so s = (1 + (psipol-psipol_a)/psipol_a)^0.5
      !      = (1 - 1 + psipol/psipol_a)^0.5
      !      = (psipol/psipol_a)^0.5 = rhopol_grid
      s_grid = rhopol_grid

      !write (*,*) 's_grid', s_grid
      !write (*,*) 'psipol_a', psipol_a
      !write (*,*) 'rhopol_grid', rhopol_grid

      ! NB need to multiply dpsipoldrho_grid by psipol_a because we
      ! want the actual flux in Webers
      dpdpsipol_grid = 0.0
      do is = 1,nspec
        dpdpsipol_grid(:) = dpdpsipol_grid(:) - &
           pprim_grid(:,is) / (dpsipoldrho_grid(:)*psipol_a) * pres_grid(:,is) * &
              1.0e20 * 1.0e3 * 1.6e-19 ! Factors to convert pressure into Pascals
      end do

      ! The CHEASE input array is 
      ! (dp/dpsi) / (b0exp_chse**2/mu0) * (b0exp_chse * r0exp_chse**2)
      dpdpsipol_grid = dpdpsipol_grid /  b0exp_chse ** 2 * &
                         (4.0 * 3.14159 * 1.0e-7 * b0exp_chse * r0exp_chse**2)
      !write (*,*) 'dpdpsipol_grid 2', dpdpsipol_grid
                                      
      call debug_message(&
        verb_timestep, 'chease_ecom::update_chease reading infile')
      call read_infile(checom%chease_outdata_file)

      ! The trinity grid does not extend to the LCFS
      ! We only want to modify the CHEASE pressure gradient
      ! across the range of the trinity grid
      do iedge = 1,size(flux_coordinate)
        if (flux_coordinate(iedge).gt.s_grid(nrad)) exit
      end do

      iedge = iedge - 1
      write (*,*) 'iedge', iedge, 'size(flux_coordinate)', size(flux_coordinate)
      allocate(pressure_gradient_temp(iedge))
      allocate(flux_coordinate_temp(iedge))
      flux_coordinate_temp = -100000.0
      pressure_gradient_temp = -100000.0
      do  i = 1,iedge
       flux_coordinate_temp(i) = flux_coordinate(i)
      end do 

      allocate(dummy(size(flux_coordinate)))

      ! Now we check if the original pressure gradient
      ! has been set. If so, we want to keep the outer 
      ! section of the original pressure gradient, in
      ! order to keep it constant during the simulation. 
      ! (Specifically when Trinity has been restarted
      ! using input from a CHEASE run which was different
      ! to the CHEASE run used for the previous Trinity run).
      ! We do this by overwriting the 
      ! entire chease pressure gradient with the original one,
      ! before copying the Trinity section over.
      ! Otherwise, we assume this is the 
      ! first time that chease has been rerun, and save the
      ! original pressure gradient (and grid)

      if (checom%initialized) then
        !write (*,*) 'cegrid_original', checom%cegrid_original, checom%dpdpsi_cegrid_original
        call spline(checom%cegrid_original, checom%dpdpsi_cegrid_original, &
          flux_coordinate, pressure_gradient, dummy)
        ! Subsqequent times restart from existing chease solution
        ierror = syscall("sed -i 's/nopt.*/nopt=-1/' chease/chease_namelist")
        ierror = syscall("cd chease; cp NOUT NIN")
      else
        call init_chease_ecom(checom, size(flux_coordinate),nsurf)
        checom%cegrid_original = flux_coordinate
        checom%dpdpsi_cegrid_original = pressure_gradient
        ! The first time start afresh in case we changed the pressure
        ! gradient in initialization
        ierror = syscall("sed -i 's/nopt.*/nopt=0/' chease/chease_namelist")
      end if



      !write (*,*) 'Old pressure gradient', pressure_gradient
      !write (*,*) 'New pressure grid', dpdpsipol_grid
      call spline(s_grid, dpdpsipol_grid, flux_coordinate_temp, & 
          pressure_gradient_temp, dummy)

      ! Now copy the new pressure gradient across the trinity
      ! region onto the CHEASE pressure gradient which spans 
      ! the entire region
      do  i = 1,iedge
       pressure_gradient(i) = pressure_gradient_temp(i)
      end do 

      ! Save current values
      checom%dpdpsi_cegrid = pressure_gradient
      checom%cegrid = flux_coordinate
      checom%current_function_cegrid = current_func
      checom%boundary_r = surface_r
      checom%boundary_z = surface_z

      !write (*,*) 'New pressure gradient', pressure_gradient
      filename ='chease/EXPEQ        '
      call debug_message(&
        verb_timestep, 'chease_ecom::update_chease writing outfile')
      call write_outfile(filename)
      call write_outfile_no_surf(trim(filename)//'.NOSURF')
      call deallocate_chease_io
      deallocate(dpdpsipol_grid)
      deallocate(dummy)
      deallocate(pressure_gradient_temp)
      deallocate(flux_coordinate_temp)


      call debug_message(&
        verb_timestep, 'chease_ecom::update_chease calling run_chease')
      if (should_run_chease_actual) update_chease = run_chease(checom)
      call debug_message(&
        verb_timestep, 'chease_ecom::update_chease called run_chease')
   else if (geo_option_switch.eq. geo_option_ecom) then


      filename ='chease/Profile.dat'
      write (*,*) 'Reading ecom infile'
      call ecom_read_infile(filename)
      write (*,*) 'Read ecom infile'


      ! The trinity grid does not extend to the LCFS
      ! We only want to modify the ECOM pressure gradient
      ! across the range of the trinity grid
      do iedge = 1,size(ecom_flux_coordinate)
        if (ecom_flux_coordinate(iedge).gt.rhotor_grid(nrad)) exit
      end do
      iedge = iedge - 1
      !write (*,*) 'iedge', iedge, 'size(flux_coordinate)', size(flux_coordinate)
      allocate(pressure_temp(iedge))
      allocate(flux_coordinate_temp(iedge))
      flux_coordinate_temp = -100000.0
      pressure_temp = -100000.0
      write (*,*) 'Setting flux_coordinate_temp'
      write (*,*) iedge, 'iedge', size(flux_coordinate_temp), size(ecom_flux_coordinate)
      do  i = 1,iedge
       flux_coordinate_temp(i) = ecom_flux_coordinate(i)
      end do 


      allocate(dummy(size(flux_coordinate_temp)))
      !write (*,*) 'Old pressure gradient', pressure_gradient
      !write (*,*) 'New pressure grid', dpdpsipol_grid
      write (*,*) 'splining...'
      call spline(rhotor_grid, sum(pres_grid, 2), flux_coordinate_temp, & 
          pressure_temp, dummy)

      ! Now copy the new pressure gradient across the trinity
      ! region onto the CHEASE pressure gradient which spans 
      ! the entire region
      write (*,*) 'rhotor_grid', rhotor_grid, 'flux_coordinate_temp', ecom_flux_coordinate
      write (*,*) 'old pressure',  pressure(1), pressure(iedge), 'iedge', iedge
      do  i = 1,iedge
       pressure(i) = pressure_temp(i)
      end do 
      write (*,*) 'new pressure', pressure(1), pressure(iedge)

      !write (*,*) 'New pressure gradient', pressure_gradient
      filename ='chease/Profile.dat'
      write (*,*) 'Writing ecom infile'
      call ecom_write_outfile(filename)
      call deallocate_ecom_io
      !deallocate(dpdpsipol_grid)
      deallocate(dummy)
      deallocate(pressure_temp)
      deallocate(flux_coordinate_temp)


      if (should_run_chease_actual) call run_ecom
   end if
        

    !logical, save :: first = .true.

    !if (proc0) then
       !if (first) then
          !! read in parameters from chease input file, to be stored in chease_io module
          !call read_chease_infile (trim(chease_infile)//".out")
          !first = .false.
       !end if
       !! pressure_gradient used in chease is species-summed pressure_gradient,
       !! where each species pressure gradient is normalized as
       !! pres_grad_s = d(p_chease)/d(psi_chease),
       !! with p_chease = mu0*p/(B0**2)
       !! and psi_chease = 2*pi*rhopol/(B0*R0**2)
!!       pressure_gradient = sum(pprim_grid(,:)
       !! write new chease input file using stored values from chease_io module, but
       !! with updated pressure_gradient
        !call write_chease_infile (chease_infile)







    !end if

  end function update_chease

  function chease_ecom_unit_test_update_chease(eps)
    use unit_tests_trin
    use chease_io, only: read_infile, write_outfile, deallocate_chease_io
    use chease_io, only: pressure_gradient
    use mp_trin, only: proc0
    logical :: chease_ecom_unit_test_update_chease
    real, dimension(:), allocatable :: old_pressure_gradient
    real, intent(in) :: eps
    type(chease_ecom_type) :: checom
    integer :: ierror
    chease_ecom_unit_test_update_chease = .true.
    if (proc0) then
      call read_infile('chease/EXPEQ.OUT')
      allocate(old_pressure_gradient(size(pressure_gradient)))
      old_pressure_gradient = pressure_gradient
      call deallocate_chease_io
      ierror =  update_chease(checom, .false.)
      if (ierror.ne.0) stop 1
      call read_infile('chease/EXPEQ')
      call announce_check(&
        ' value of pressure gradient written to chease input file')
      call process_check(chease_ecom_unit_test_update_chease, &
        agrees_with(pressure_gradient(5:), old_pressure_gradient(5:), eps),&
        ' value of pressure gradient written to chease input file')
    end if
  end function chease_ecom_unit_test_update_chease

  subroutine run_ecom
    use mp_trin, only: abort_mp
    use syscall_pli, only: syscall
    

    implicit none
    integer, save :: nruns = 1
    character(len=80) :: command
    integer :: stat

    !call system ("cp ecom/ogyropsi.dat")
    write (command, '("cp chease/ogyropsi.dat chease/ogyropsi",i5.5,".dat")') &
        nruns
    stat = syscall(command)
    stat = syscall ("cd chease; ecom > ecom_out.out")
    if (stat.ne.0) call abort_mp
    !call system ("cd chease; ecom ")
    nruns = nruns + 1

  end subroutine run_ecom

  function run_chease(checom)
    use mp_trin, only: abort_mp
    use syscall_pli, only: syscall
    implicit none
    integer :: run_chease
    type(chease_ecom_type), intent(inout) :: checom
    character(len=80) :: command
    integer :: stat

    !call system ("cp chease/ogyropsi.dat")
    write (command, '("cp chease/ogyropsi.dat chease/ogyropsi",i5.5,".dat")') &
        checom%nruns
    stat = syscall(command)
    stat = syscall ("cd chease; chease > chease_out.out")
    if (stat .ne. 0) then
      write (*,*) 'ERROR: chease failed'
      run_chease = 1
    else
      run_chease = 0
    end if
    checom%nruns = checom%nruns + 1

  end function run_chease

!  subroutine update_geometry (prof)
  subroutine update_geometry

    use constants_trin, only: pi
    use mp_trin, only: proc0, broadcast
    use interp, only: get_cc
    use interp, only: fpintrp, fd3pt
    use trinity_input, only: fluxlabel_option_switch,aspr,bmag,rgeo,vtfac
    use trinity_input, only: zeff
    use trinity_input, only: m_ion
    use trinity_input, only: fluxlabel_option_aminor, fluxlabel_option_torflux
!    use trinity_input, only: chease_outfile
    use trinity_input, only: overwrite_db_input
    use trinity_input, only: phia_in, zeff_in, bt_in, rgeo_in, amin_in
    use read_chease, only: npsi_chease, rho_t_chease
    use read_chease, only: rgeom_chease, ageom_chease
    use read_chease, only: q_chease, kappa_chease, psi_chease, gdpsi_av_chease
    use read_chease, only: v_chease, dvdpsi_chease, dpsidrhotor_chease
    use read_chease, only: delta_upper_chease, delta_lower_chease
    use read_chease, only: f_chease, b0exp_chease, r0exp_chease
    use read_chease, only: read_chease_outfile => read_infile
    use read_chease, only: deallocate_chease_arrays => finish
    use read_chease, only: jbsbav_chease
    use nteqns_arrays
    use norms, only: psitor_a, psipol_a, aref, amin_ref, b0exp_chse, r0exp_chse, rhostar
    !use nteqns_arrays, only: rad_grid, rhotor_grid, rmin_grid
    !use nteqns_arrays, only: rad_grid, rhotor_grid, rmin_grid
    !use read_chease, only: dpsidrho_from_chease => dpsidrho_chse

    implicit none

!    real, dimension (:), intent (in), optional :: prof

    integer :: ix
    real :: amin

    real, dimension (:), allocatable :: rad_chse, drho_chse
    real, dimension (:), allocatable :: drmindrho_chse, drhotordrho_chse
    real, dimension (:), allocatable :: drhopoldrho_chse
    real, dimension (:), allocatable :: dpsipoldrho_chse
    real, dimension (:), allocatable :: shat_chse, shift_chse
    real, dimension (:), allocatable :: del_chse, delprim_chse,kapprim_chse
    character(len=80) :: file_name_temp

    if (proc0) then

       !call read_chease_outfile (chease_outfile)
       ! We don't have a choice about the file name that chease outputs.
       file_name_temp = 'chease/ogyropsi.dat'
       call read_chease_outfile (file_name_temp)

       b0exp_chse = b0exp_chease
       r0exp_chse = r0exp_chease

       allocate (drhotordrho_chse(npsi_chease))
       allocate (drhopoldrho_chse(npsi_chease))
       allocate (dpsipoldrho_chse(npsi_chease))
       allocate (drmindrho_chse(npsi_chease))       
       allocate (rad_chse(npsi_chease))
       allocate (shat_chse(npsi_chease))
       allocate (shift_chse(npsi_chease))
       allocate (del_chse(npsi_chease))
       allocate (delprim_chse(npsi_chease))
       allocate (kapprim_chse(npsi_chease))
       allocate (drho_chse(npsi_chease-1))

       ! psitor_a is total toroidal flux (in Webers) enclosed by plasma (i.e. at LCFS)
       ! EDMUND: is this one of the values given by CHEASE?
       ! to MICHAEL: it is not output, but looking at line 130
       ! of gloqua.f90 in the CHEASE source we find that

       psitor_a = rho_t_chease(npsi_chease) ** 2 * f_chease(npsi_chease) / &
                 rgeom_chease(npsi_chease) / 2.0

       psitor_a = rho_t_chease(npsi_chease) ** 2 * f_chease(npsi_chease) / &
                 rgeom_chease(npsi_chease) * pi

       ! NB, I checked this by getting the toroidal flux directly by 
       ! integrating the dpsi/dr * q

       !> Poloidal flux at LCFS
       psipol_a = psi_chease(npsi_chease)

       select case (fluxlabel_option_switch)
       case (fluxlabel_option_aminor)
          ! EDMUND: assuming here that ageom_chease is geometrical minor radius in meters
          ! and last value of ageom_chease is at LCFS.  Am I right?
          ! to MICHAEL: yes!
          rad_chse = ageom_chease/ageom_chease(npsi_chease)
       case (fluxlabel_option_torflux)
          ! EDMUND: what exactly is rho_t_chease? here, I want sqrt(torflux/torflux at LCFS)
          ! to MICHAEL: that is exactly what it is, i've just checked it!
          ! Update, it's sqrt(torflux) with some different
          ! normalisation, so we need to divide by the final 
          ! value
          rad_chse = rho_t_chease/rho_t_chease(npsi_chease)
       end select


       drho_chse = rad_chse(2:)-rad_chse(:npsi_chease-1)

       ! These two functions allow for conversion between
       ! flux labels... between rho, what ever it happens to be
       ! and rmin and rhotor
       ! NB psi for trinity means rho_tor = sqrt(torflux/psitor_a)
       ! psi for chease means poloidal flux in Webers
       call fd3pt (rho_t_chease/rho_t_chease(npsi_chease), drhotordrho_chse, drho_chse)
       call fd3pt (psi_chease/psipol_a, dpsipoldrho_chse, drho_chse)
       call fd3pt (sqrt(psi_chease/psipol_a), drhopoldrho_chse, drho_chse)
       call fd3pt (ageom_chease/ageom_chease(npsi_chease), drmindrho_chse, drho_chse)

       ! rmin is geometrical minor radius in meters. must interpolate from CHEASE 
       ! rho grid to TRIN rho grid
       do ix = 1, size(rad_grid)
         ! rhotor is sqrt(torflux/psitor_a)
         ! EDMUND: is rho_t_chease = sqrt(torflux/torflux at LCFS) ? 
         ! to MICHAEL: no... it sqrt(torflux/(pi*I(LCFS)/R_centre(LCFS)))
         rhotor_grid(ix) = fpintrp(rad_grid(ix),rho_t_chease/rho_t_chease(npsi_chease),&
           rad_chse,npsi_chease)

          ! EDMUND: assuming ageom_chease is geo minor radius in meters. is this right?
          !> to MICHAEL. affirmative
          ! NB rmin is normalised

          rmin_grid(ix) = fpintrp(rad_grid(ix), &
                                  ageom_chease/ageom_chease(npsi_chease), &
                                  rad_chse,npsi_chease)
          ! drhotordrho is derivative of sqrt(torflux/psitor_a) wrt to flux label rho
          drhotordrho_grid(ix) = fpintrp(rad_grid(ix),drhotordrho_chse,rad_chse,npsi_chease)
          ! drmindrho is derivative of (rmin/amin) wrt to flux label rho
          drmindrho_grid(ix) = fpintrp(rad_grid(ix),drmindrho_chse,rad_chse,npsi_chease)
          dpsipoldrho_grid(ix) = fpintrp(rad_grid(ix),dpsipoldrho_chse,rad_chse,npsi_chease)
          drhopoldrho_grid(ix) = fpintrp(rad_grid(ix),drhopoldrho_chse,rad_chse,npsi_chease)
       end do
       ! EDMUND: are last indices of rgeom_chease and ageom_chease the values at the separatrix or what?
       ! to MICHAEL: that's what they are.

       ! geometrical major radius of LFCS in meters
       ! EDMUND to do... check this is what it is
       rgeo = rgeom_chease(npsi_chease)
       ! geometrical minor radius of LFCS in meters
       amin = ageom_chease(npsi_chease)

       ! EGH: setting of bmag_grid is now handled in initialise_bmag_grid
       ! which needs bunit_grid and btori_grid

       ! bunit = 1/r dtorflux / dr / 2 pi according to the gacode website
       ! drhotordrho_grid = 
       ! d(sqrt(torflux/psitor_a))/drho =
       ! sqrt(torflux/psitor_a)^-1 * 0.5 * dtorflux/drho / psitor_a
       ! => d torflux/drho = drhotordrho_grid * rhotor_grid * phi_a * 2
       ! => bunit_grid = drhotordrho_grid*rhotor_grid*psitor_a*2/drmindrho_grid /rmin_grid/2 pi
       bunit_grid =  drhotordrho_grid*rhotor_grid*psitor_a/drmindrho_grid/rmin_grid/pi/amin**2

       !> Not quite exact but should be close enough
       bmag_axis = f_chease(1)/rgeom_chease(1)



       ! This must be whatever the normalising magnetic field is
       ! in the flux tube code
       bmag_old_grid = f_chease(npsi_chease)/rgeom_chease(npsi_chease)
       ! Here we set bmag = I(LCFS)/Rgeo(LCFS), as in ceq.f90 in GS2
       ! Note that with a chease equilibrium the normalising mag field
       ! is the same on every flux surface.
       !bmag_grid = bmag

       ! line-averaged Zeff
       ! to MICHAEL: While we are only using CHEASE input and analytic 
       ! sources we only have one ion species... I've set zeff to 1.0
       ! Actually, since CHEASE provides a zeff grid, this should
       ! never be used.... 
       zeff = 1.0
       
       ! overwrite CHEASE values with input values if overwrite_db_input = .true.
       if (overwrite_db_input) then
          if (abs(phia_in) > epsilon(0.0)) psitor_a = abs(phia_in)
          if (rgeo_in > epsilon(0.0)) rgeo = rgeo_in
          if (amin_in > epsilon(0.0)) amin = amin_in
          if (abs(bt_in) > epsilon(0.0)) bmag = abs(bt_in)
          !if (zeff_in > epsilon(0.0)) zeff = zeff_in
       end if
       ! aspr is aspect ratio of LCFS
       aspr = rgeo/amin

       ! aref is in meters (\tld{a}_0 from notes)
       select case (fluxlabel_option_switch)
       case (fluxlabel_option_torflux)
          aref = sqrt(psitor_a/(pi*bmag))
          amin_ref = amin
       case (fluxlabel_option_aminor)
          aref = amin !rgeo/aspr
          amin_ref = amin
       end select
!       write (*,*) 'aref is ', aref
       
       do ix = 1, size(rad_grid)
          ! How is psi_chease normalized?
          ! psipol_grid = psipol/psipol at LCFS
          ! to MICHAEL: psi_chease is in Webers.. does the line below
          ! require altering? I haven't done anything here
          psipol_grid(ix) = fpintrp(rad_grid(ix),&
            psi_chease/psi_chease(npsi_chease),rad_chse,npsi_chease)
          rmajor_grid(ix) = fpintrp(rad_grid(ix),&
            rgeom_chease/aref,rad_chse,npsi_chease)
          qval_grid(ix) = fpintrp(rad_grid(ix),q_chease,rad_chse,npsi_chease)
          !!!!!!!!!!
          !! area = dV/dpsi <|\nabla \psi|>, both of which are
          !! available from chease
          !! I've checked the definition of gdpsi_av in the chease source
          !! .. it comes from the variable iigradpsi_av, defined at line
          !! 491 of chipsimetrics.f90. I've also checked by plotting
          !! this quantity, 2\pi R 2\pi r, and 2V/r on the same plot
          !! and they are roughly similar (they should be equal for
          !1 the case of circular flux surfaces)

          area_grid(ix) = fpintrp(rad_grid(ix), &
                            dvdpsi_chease * gdpsi_av_chease, &
                            rad_chse, npsi_chease)/aref**2.0

          
          !! Assuming this grid is <| grad rho_t|> / aref, 
          !! as calculated above for iterdb
          !! We calculate it as <|grad psi|> / (dpsi/drho_t) / aref
          select case (fluxlabel_option_switch)
          case (fluxlabel_option_torflux)
          ! Temporary hack as dpsidrhotor_chease = 0 at mag axis
          dpsidrhotor_chease(1) = dpsidrhotor_chease(2)
          grho_grid(ix) = fpintrp(rad_grid(ix), &
                            gdpsi_av_chease / dpsidrhotor_chease / aref, &
                            rad_chse, npsi_chease)
          case (fluxlabel_option_aminor)
            ! This may be inaccurate... check?
          dpsidrhotor_chease(1) = dpsidrhotor_chease(2)
            grho_grid(ix) = fpintrp(rad_grid(ix), &
                            gdpsi_av_chease / dpsidrhotor_chease / aref, &
                            rad_chse, npsi_chease) / drhotordrho_grid(ix)
          end select
       end do

       rhopol_grid = sqrt(psipol_grid)


       ! kapp, shat, shift only needed for IFS-PPPL

       ! get dq/dr
       ! Could use dqdpsi_chease intead...
       call fd3pt (q_chease, shat_chse, drho_chse)
       ! get shat = (r/q) * dq/dr
       shat_chse = rad_chse*shat_chse/q_chease
       !NB In Trinity & TGLF delta = delta_Miller but in GS2 & GRYFX delta = sin(delta_Miller)
       del_chse = (delta_upper_chease + delta_lower_chease)/2.0

       call fd3pt (del_chse, delprim_chse, drho_chse)
       call fd3pt (kappa_chease, kapprim_chse, drho_chse)

       ! get dR/dr
       call fd3pt (rgeom_chease/aref, shift_chse, drho_chse)

       ! interpolate from chease grid to trinity grid
       do ix = 1, size(rad_grid)
          btori_grid(ix) = fpintrp(rad_grid(ix),f_chease,rad_chse,npsi_chease)
          shat_grid(ix) = fpintrp(rad_grid(ix),shat_chse,rad_chse,npsi_chease)
          shift_grid(ix) = fpintrp(rad_grid(ix),shift_chse,rad_chse,npsi_chease)
          kappa_grid(ix) = fpintrp(rad_grid(ix),kappa_chease,rad_chse,npsi_chease)
          kapprim_grid(ix) = fpintrp(rad_grid(ix),kapprim_chse,rad_chse,npsi_chease)
          delta_grid(ix) = fpintrp(rad_grid(ix),del_chse,rad_chse,npsi_chease)
          deltprim_grid(ix) = fpintrp(rad_grid(ix),delprim_chse,rad_chse,npsi_chease)
          bootstrap_current_import_grid(ix) = &
            fpintrp(rad_grid(ix),jbsbav_chease,rad_chse,npsi_chease)
       end do
          !write (*,*) 'rad_grid', rad_grid, 'rad_chse', rad_chse
          !write (*,*) 'zeff_chease', zeff_chease, 'zeff_grid', zeff_grid

       ! Convert derivatives for d/drho to d / d(r/a)
       deltprim_grid = deltprim_grid / drmindrho_grid
       kapprim_grid = kapprim_grid / drmindrho_grid
       
       ! these will not be needed since chease will specify equilibrium to flux tube code
       ! But we could calculate them to check miller maybe
       ! dkappa/drho
       !kapprim_grid = 0.0
       ! Maybe av of delta_lower_chease, delta_upper_chease 
       !delta_grid = 0.0
       ! d delta / drho
       !deltprim_grid = 0.0


       call deallocate_chease_arrays

    end if


    if (proc0) then
      deallocate (drhotordrho_chse)
      deallocate (drhopoldrho_chse)
      deallocate (dpsipoldrho_chse)
      deallocate (drmindrho_chse)       
      deallocate (rad_chse)
      deallocate (shat_chse)
      deallocate (shift_chse)
      deallocate (del_chse)
      deallocate (delprim_chse)
      deallocate (kapprim_chse)
      deallocate (drho_chse)
    end if

    !call broadcast (bmag)
    !call broadcast (aref)
    !call broadcast (rgeo)
    !call broadcast (aspr)
    !call broadcast (amin_ref)
    !call broadcast (psitor_a)
    !call broadcast (psipol_a)

    !call initialise_bmag_grid

    rhostar = 3.23e-3*sqrt(vtfac*m_ion(1))/aref

    ! update gexb, which depends on geometric quantities
!    if (present(prof)) call get_grads (prof)

  end subroutine update_geometry

  subroutine get_chease_profs

    use mp_trin, only: proc0
    use trinity_input, only: fluxlabel_option_switch
    use trinity_input, only: fluxlabel_option_aminor, fluxlabel_option_torflux
!    use trinity_input, only: chease_outfile
    use trinity_input, only: n_ion_spec, nspec, density_boost,nrad
    use trinity_input, only: z_ion, peaking_factor, tritium_fraction
    use read_chease, only: npsi_chease
    use read_chease, only: ne_chease, ti_chease, te_chease, zeff_chease
    use read_chease, only: ageom_chease, rho_t_chease
    use read_chease, only: read_chease_outfile => read_infile
    use read_chease, only: deallocate_chease_arrays => finish
    use interp, only: fpintrp, get_cc
    use nteqns_arrays

    implicit none

    integer :: ix,i

    real, dimension (:), allocatable :: rad_chse
    character(len=80) :: file_name_temp

    if (proc0) then

       allocate (rad_chse(npsi_chease))

       ! We don't have a choice about the file name that chease outputs.
       file_name_temp = 'chease/ogyropsi.dat'
       call read_chease_outfile (file_name_temp)
       !call read_chease_outfile ('chease/ogyropsi.dat')
       !call read_chease_outfile (chease_outfile)    

       select case (fluxlabel_option_switch)
       case (fluxlabel_option_aminor)
          ! EDMUND: assuming here that ageom_chease is geometrical minor radius in meters
          ! and last value of ageom_chease is at LCFS.  Am I right?
          ! to MICHAEL: yes
          rad_chse = ageom_chease/ageom_chease(size(ageom_chease))
       case (fluxlabel_option_torflux)
          ! EDMUND: what exactly is rho_t_chease? here, I want sqrt(torflux/torflux at LCFS)
          ! to MICHAEL: that's what it is
          rad_chse = rho_t_chease/rho_t_chease(npsi_chease)
       end select

       do ix = 1, size(rad_grid)
          ! rhotor is sqrt(torflux/psitor_a)
          ! EDMUND: is rho_t_chease = sqrt(torflux/torflux at LCFS) ?
         ! to MICHAEL: no... it sqrt(torflux/(pi*I(LCFS)/R_centre(LCFS)))
          rhotor_grid(ix) = fpintrp(rad_grid(ix),rho_t_chease/rho_t_chease(npsi_chease), &
          rad_chse,npsi_chease)
          
          ! dens_grid is electron density in 10^20/m^3
          ! to MICHAEL: presumably we need to divide the density by 10**20 then?
          dens_grid(ix,1) = fpintrp(rad_grid(ix),ne_chease/10.0**20,rad_chse,npsi_chease)
          ! CHEASE doesn't provide info for multiple ion species
          ! so assume Zi*ni=ne
          dens_grid(ix,2) = dens_grid(ix,1)/z_ion(1)
          if (n_ion_spec > 1)  then
            dens_grid(ix,2) = dens_grid(ix,2)/2.0
            dens_grid(ix,3) = dens_grid(ix,2)
            if (n_ion_spec>2) dens_grid(ix,4:) = 0.0
          end if 

          ! temp_grid is temperature in keV
          ! to MICHAEL: temps are in eV, so dividing by 10**3
          temp_grid(ix,1) = fpintrp(rad_grid(ix),te_chease/10.0**3.0,rad_chse,npsi_chease)
          temp_grid(ix,2) = fpintrp(rad_grid(ix),ti_chease/10.0**3.0,rad_chse,npsi_chease)
          ! assuming all ions have same temperature
          if (n_ion_spec > 1) temp_grid(ix,3:) = temp_grid(ix,2)

          ! omega_grid is toroidal rotation frequency x aref / vtref, 
          ! where vtref uses a temperature of 1 keV and the mass of the ion species
          ! doesn't look like chease outputs this quantity
          ! to MICHAEL: correct... CHEASE doesn't have flow... 
          ! once this is working, it should be fairly straightforward to
          ! add support for a code that does, probably via the library that
          ! Ferdinand and I are working on.
          omega_grid(ix) = 0.0

          zeff_grid(ix) = fpintrp(rad_grid(ix),zeff_chease,rad_chse,npsi_chease)
       end do
       ! We can't change the edge pressure from chease. However,
       ! we can swap temperature for density at constant pressure
       if (density_boost .gt. 0.0) then
         dens_grid = dens_grid * density_boost
         temp_grid = temp_grid / density_boost
       end if
       if (peaking_factor .gt. 0.0) then
         do i = 1,nspec
           dens_grid(:,i) = ( &
             dens_grid(nrad,i) + &
             (dens_grid(:,i)-dens_grid(nrad,i)) * peaking_factor &
           )
         end do
       end if
       call get_cc(zeff_grid, zeff_cc)

       mom_grid = omega_grid/inv_mominertia_fnc(dens_grid,rmajor_grid)
       
       ! guessing these are not given by CHEASE?
       ! to MICHAEL: you guessed right!
       chie_exp = 0.0
       chii_exp = 0.0

       deallocate (rad_chse)

       call deallocate_chease_arrays

    end if

  end subroutine get_chease_profs
end module chease_ecom

