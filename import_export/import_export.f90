!> A module for handling (via its submodules in the same folder)
!! the importing and exporting of equilibrium data 
!! to and from the various file formats that Trinity can 
!! interact with. Also handles the generating of analytic profiles
!! from parameters within the Trinity namelists instead.
module import_export

  implicit none

contains
  !> Import geometric data from whichever file format we are
  !! using (or create it analytically if appropriate).
  subroutine import_geometry (target_time)

    use trinity_input, only: geo_option_switch, geo_option_analytic, geo_option_iterdb, geo_option_expro
    use trinity_input, only: geo_option_chease, geo_option_ecom, geo_option_trinity, geo_option_transp
    use trinity_input, only: zeff, aspr, bmag, rgeo, nrad
    use mp_trin, only: broadcast
    use chease_ecom, only: update_geometry, get_chease_profs
    use nteqns_arrays
    use norms, only: amin_ref, aref, psitor_a, psipol_a
    use iterdb, only: import_geometry_iterdb, import_initial_profiles_iterdb
    use analytic, only: import_geometry_analytic
    use expro_import, only: import_geometry_expro
    use trinity_import, only: import_geometry_trinity
    use transp_import, only: import_geometry_transp

    implicit none
    
    real, intent (in), optional :: target_time
    
    ! Set to 0 in case not available
    current_grid = 0.0
    
    select case (geo_option_switch)
    case (geo_option_iterdb)
       call import_geometry_iterdb
    case (geo_option_chease, geo_option_ecom)
       call update_geometry
    case (geo_option_analytic)
       call import_geometry_analytic
    case (geo_option_expro)
       call import_geometry_expro
    case (geo_option_trinity)
       call import_geometry_trinity
    case (geo_option_transp)
       call import_geometry_transp (target_time)
    end select
    
    ! TODO: do we need to broadcast zeff
    ! and bmag here anymore?
    call broadcast (zeff)
    call broadcast (bmag)
    call broadcast (aref)
    call broadcast (rgeo)
    call broadcast (aspr)
    call broadcast (amin_ref)
    call broadcast (psitor_a)
    call broadcast (psipol_a)
    
    call initialise_bmag_grid
    
    ! Populate these in case they are not 
    ! initialized so that they don't cause
    ! problems with the netcdf file.
    if (.not.(allocated(Rofpsitheta))) then
       ntheta = 20
       allocate(Rofpsitheta(nrad,ntheta))
       allocate(Zofpsitheta(nrad,ntheta))
       Rofpsitheta = 0.; Zofpsitheta = 0.
    end if
    if (.not. (allocated(psiofRZ))) then
       nZ_eq = 1; nR_eq = 1
       allocate(psiofRZ(nR_eq, nZ_eq))
       psiofRZ = 0.
    end if
    
  end subroutine import_geometry
  
  subroutine import_initial_profiles(clock)
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: get_unused_unit, error_unit
    use constants_trin, only: pi
    use interp, only: get_cc
    use trinity_input, only: nrad, ntdelt, restart_file
    use trinity_input, only: z_ion
    use trinity_input, only:  n_ion_spec
    use trinity_input, only: init_option_switch, init_option_analytic, init_option_iterdb, init_option_restart, init_option_chease
    use trinity_input, only: init_option_trinity, init_option_transp
    use trinity_input, only: init_option_expro
    use trinity_input, only: init_file, init_time, geo_file, geo_time
    use trinity_input, only: overwrite_db_input, rgeo_in, phia_in, amin_in, bt_in, zeff_in
    use trinity_input, only: te_equal_ti, te_fixed
    use trinity_input, only: equal_ion_temps
    use nteqns_arrays
    use trinity_time, only: ntdelt_old
    use iterdb, only: import_initial_profiles_iterdb
    use analytic, only: import_initial_profiles_analytic
    use expro_import, only: import_initial_profiles_expro
    use chease_ecom, only: get_chease_profs
    use trinity_import, only: import_initial_profiles_trinity
    use transp_import, only: import_initial_profiles_transp
    use trindiag_restart, only: restart_trinity
    use trinity_input, only: geo_option_switch, geo_option_analytic, geo_option_iterdb, geo_option_expro
    use trinity_input, only: geo_option_chease, geo_option_ecom, geo_option_trinity, geo_option_transp
    use trinity_input, only: init_file, geo_file
    use file_utils_trin, only: error_unit
    use trindiag_restart, only: restart_trinity
    use trinity_type_module, only: trinity_clock_type
    implicit none
    type(trinity_clock_type), intent(inout) :: clock
    integer :: restart_unit, is, ix
    character(len=10000) :: init_file_temp

    select case (init_option_switch)

    case (init_option_iterdb)
      call import_initial_profiles_iterdb
    case (init_option_chease)
      if (proc0) then
        call get_chease_profs
      end if
    case (init_option_expro)
      call import_initial_profiles_expro
    case (init_option_trinity)
      call import_initial_profiles_trinity
   case (init_option_transp)
      call import_initial_profiles_transp
    case (init_option_restart)
      if (trim(restart_file) /= 'old') then
        ! In this case we restart using data from the netcdf output file of 
        ! the previous run. This is the recommended option.
        call restart_trinity(restart_file, clock)
      else
        if (proc0) &
        write (error_unit(), *) 'WARNING: You are using the old restart &
          &mechanism which is now deprecated. It will be maintained for &
          &the moment to allow restarts from old runs. However you are &
          &strongly advised to restart from the netcdf file, which is &
          &enabled by setting the restart_file input parameter to the &
          &name of the netcdf file which was output by the previous run.'

        init_file_temp = init_file
        init_file = geo_file

        ! The lines below are a deeply unpleasant hack 
        ! to ensure the treatment of zeff remains OK. 
        ! This hack is necessary because zeff is not stored
        ! in the restart files. It is not needed when
        ! using the new netcdf restart option
        select case (geo_option_switch)
        case (geo_option_iterdb)
          call import_initial_profiles_iterdb
        case (geo_option_chease, geo_option_ecom)
          if (proc0) call get_chease_profs
        case (geo_option_analytic)
          call import_initial_profiles_analytic
        case (geo_option_expro)
          call import_initial_profiles_expro
        case (geo_option_trinity)
          call import_initial_profiles_trinity
       case (geo_option_transp)
          call import_initial_profiles_transp
        end select
        init_file = trim(init_file_temp)
        if (proc0) then
          call get_unused_unit (restart_unit)
          open (unit=restart_unit, file=trim(init_file), status="old", action="read")
          do ix = 1, nrad
            read (restart_unit,*) clock%itstep_tot, clock%iter, ntdelt_old, ntdelt, clock%time, &
              rmajor_grid(ix), dens_grid(ix,:), temp_grid(ix,:), mom_grid(ix), &
              clock%neval
            write (error_unit(), *) 'WARNING: zeff is not handled in restarts and &
              & is being set to some unknown value.'
            !zeff_grid = 1.0
            !zeff_cc = 1.0
          end do
          close (restart_unit)
       end if
    end if
    call broadcast (clock%time)
    call broadcast (clock%iter)
    call broadcast (clock%itstep_tot)
    call broadcast (clock%neval)
    call broadcast (ntdelt)
    call broadcast (ntdelt_old)
      !do is = 1, nspec
        !call broadcast (dens_grid(:,is))
        !call broadcast (temp_grid(:,is))
      !end do
      !call broadcast (mom_grid)
      !call broadcast (rmajor_grid)

      omega_grid = mom_grid*inv_mominertia_fnc(dens_grid,rmajor_grid)

    case (init_option_analytic)
      call import_initial_profiles_analytic
   end select

    if (te_equal_ti) then
      temp_grid(:,1) = temp_grid(:,2)
    end if 
    if (equal_ion_temps .and. n_ion_spec > 1) then
      do is = 3, n_ion_spec+1
        temp_grid(:,is) = temp_grid(:,2)
      end do
    end if

    if (zeff_in > epsilon(0.0)) then
       zeff_grid = zeff_in
       if (proc0) then
          write (error_unit(), *) 'INFO: you have specified zeff_in in the input file &
               & and this will override any experimental values, and will not be self-consistent &
               & with your evolved ion species.'
       end if
    else if (any(z_ion .gt. 1)) then
       zeff_grid = sum(spread(z_ion**2,1,nrad)*dens_grid(:,2:),2)/dens_grid(:,1)
       if (proc0) then
          write (error_unit(),*) 'INFO: zeff has been calculated from evolved ion &
               & species only, and thus will not reflect any impurities you have neglected '
       end if
    end if
    pres_grid = dens_grid*temp_grid

    ! save initial density, pressure, and tor. ang. momentum
    ! profiles in case needed later
    ! If we restarted from the new netcdf file
    ! these will have been read from the
    ! restart file
    if (init_option_switch .ne. init_option_restart .or. &
       trim(restart_file) .eq. 'old') then
      dens_init = dens_grid
      pres_init = pres_grid
      mom_init = mom_grid
    end if 
  end subroutine import_initial_profiles

  subroutine import_sources (target_time_trin)
    use trinity_input, only: source_option_switch, source_option_analytic, source_option_iterdb
    use trinity_input, only: source_option_expro, source_option_trinity, source_option_transp
    use iterdb, only: import_sources_iterdb
    use analytic, only: import_sources_analytic
    use expro_import, only: import_sources_expro
    use trinity_import, only: import_sources_trinity
    use transp_import, only: import_sources_transp
    use nteqns_arrays
    use trinity_input, only: nbi_mult, icrh_mult, ech_mult
    use trinity_input, only: ibw_mult, lh_mult, dwn_mult, dens_mult
    use norms, only: srcnorm, lsrcnorm, nsrcnorm

    use trinity_input, only: nrad

    implicit none

    real, intent (in) :: target_time_trin
    integer :: ix

    select case (source_option_switch)

    case (source_option_iterdb)
      call import_sources_iterdb
    case (source_option_expro)
      call import_sources_expro
    case (source_option_trinity)
      call import_sources_trinity
    case (source_option_transp)
      call import_sources_transp (target_time_trin)
    case (source_option_analytic)
      call import_sources_analytic
    end select

    !> Add all the sources together.
    src_aux = &
         nbi_mult  * src_heat_nbi + &
         icrh_mult * src_heat_icrh + &
         ech_mult  * src_heat_ecrh + &
         lh_mult   * src_heat_lh + &
         ibw_mult  * src_heat_ibw + &
         src_heat_wall + &
         src_heat_ohm + &
         src_heat_unspecified - &
         dwn_mult  * src_heat_dwdt 
    src_dens = &
         nbi_mult  * src_part_nbi + &
         src_part_ionisation + &
         src_part_wall + &
         src_part_unspecified - &
         dwn_mult *  src_part_dndt 
    src_mom = &
         nbi_mult  * src_mom_nbi + &
         src_mom_unspecified
    
    !> Apply trinity normalisation
    src_aux = srcnorm * src_aux
    src_dens = 1.0e-20 * nsrcnorm * dens_mult * src_dens
    src_mom = lsrcnorm * src_mom

  end subroutine import_sources

  subroutine initialise_bmag_grid

    use file_utils_trin, only: error_unit
    use mp_trin, only: abort_mp
    use interp, only: get_cc
    use trinity_input, only: flux_option_switch
    use trinity_input, only: flux_option_gs2
    use trinity_input, only: geo_option_switch
    use trinity_input, only: geo_option_iterdb, geo_option_chease, geo_option_transp
    use trinity_input, only: geo_option_expro, geo_option_ecom, geo_option_trinity
    use trinity_input, only: aspr,nrad, use_external_geo
    use trinity_input, only: flux_option_ifspppl, flux_option_test1
    use trinity_input, only: flux_option_test2, flux_option_test3
    use trinity_input, only: flux_option_test_calibrate, flux_option_replay
    use trinity_input, only: flux_option_tglf, flux_option_gryfx
    use trinity_input, only: flux_option_matched
    use interp, only: get_cc
    use nteqns_arrays
    use norms, only: aref, amin_ref

    logical :: whitelist
    logical :: has_bunit_btori
    logical :: has_baxis 

    whitelist = .false.
    !> Are bunit, bmag_axis and btori known
    !! for the given geometry?
    has_bunit_btori = .false.
    has_baxis = .false.
    select case (geo_option_switch)
    case (geo_option_chease,&
        geo_option_ecom, &
        geo_option_iterdb,&
        geo_option_expro, &
        geo_option_trinity, &
        geo_option_transp)
       has_bunit_btori = .true.
    end select

    ! Here we whitelist known combinations that have been implemented
    ! Abort if not
    
    select case (flux_option_switch)
    case (flux_option_gs2,flux_option_gryfx)
       if (.not. use_external_geo) whitelist = has_bunit_btori
    case (flux_option_tglf)
       whitelist = has_bunit_btori
    case (flux_option_ifspppl)
       whitelist = has_bunit_btori
    case (flux_option_replay,&
         flux_option_test1,&
         flux_option_test2,&
         flux_option_test3,&
         flux_option_matched,&
         flux_option_test_calibrate)
       whitelist = .true.
    end select
    
    if (.not. whitelist) then 
       write (error_unit(), *) 'Your choice of flux_option and geo_option &
            & has not been double checked for inconsistencies in the definition & 
            & of bmag'
       call abort_mp
    end if
    
    !> This is the normalising field used in GS2 and GRYFX
    bgeo_grid = btori_grid / (aspr*amin_ref)

    ! As of Oct 2015 we now choose a universal Trinity normalisation
    ! and convert all flux I/O to that normalisation within the fluxes routine.
    ! We define that universal normalisation here:
    ! B_norm = btori(psi)/R_centre(psi) where btori is the poloidal 
    ! current flux function known variously as f, I or T in the literature
    ! and R_centre = (Rout+Rin)/2
    bmag_grid = btori_grid/rmajor_grid/aref

    !call get_cc(bmag_grid, bmag_cc)
    !call get_cc(bunit_grid, bunit_cc)
    !call get_cc(bgeo_grid, bgeo_cc)
    !call get_cc(bmag_old_grid, bmag_old_cc)

  end subroutine initialise_bmag_grid

  subroutine update_boundary (target_time)

    use mp_trin, only: proc0
    use trinity_input, only: init_option_switch, init_option_transp
    use transp_import, only: update_boundary_transp

    implicit none

    real, intent (in) :: target_time

    select case (init_option_switch)
    case (init_option_transp)
       call update_boundary_transp (target_time)
    case default
       if (proc0) then
          write (*,*) 'INFO: time-evolving boundary only implemented for init_option=transp'
          write (*,*) 'Boundary dens, temp, etc. are being held fixed.'
       end if
    end select

  end subroutine update_boundary

end module import_export
