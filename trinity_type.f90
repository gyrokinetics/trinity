!> A module for storing subtypes of the trinity type which
!! are not directly associated with another module
module trinity_type_module
  !> An object for storing information about the progress
  !! of the trinity simulation
  type trinity_clock_type
    !> Index of the current timestep in this run
    integer :: itstep
    !> Count of the total number of timesteps including
    !! restarts
    integer :: itstep_tot
    !> Index of the current iteration
    integer :: iter = 1
    !> Iteration index at the beginning of this simulation
    integer :: iter_start = 1
    !> Number of flux evalations
    integer :: neval = 0
    !> Simulation time in normalized units since the start
    real :: time = 0.0
    !> Simulation time in seconds since the start
    real :: seconds = 0.0
    !> True if this is the first iteration of this run.
    logical :: first_time = .true.
    !> True if the text files used for output need to 
    !! be opened.
    logical :: first_write = .true.
    !> If >0, the wall clock time (in seconds since the epoch)
    !! at which Trinity should terminate
    real :: end_wall_clock = -1.0

  end type trinity_clock_type

  !> An object for high level external control of Trinity,
  !! for example setting the mpi communicator and run name
  !! externally
  type trinity_control_type
    logical :: mp_comm_external = .false.
    integer :: mp_comm 
    logical :: run_name_external = .false.
    character(len=10000) :: run_name
    logical :: steady_state_converged = .false.
    real :: convergeval
  end type trinity_control_type

  !> A type for storing all the temporary arrays that are
  !! stored during the iteration, in case it is needed to 
  !! go back and change the timestep.
  type save_arrays_type
    real, dimension (:), allocatable :: nt, rhs, ntold, flx
    real, dimension (:,:,:), allocatable :: pflx, pflxold
    real, dimension (:,:,:), allocatable :: qflx, qflxold
    real, dimension (:,:,:), allocatable :: heat, heatold
    real, dimension (:,:), allocatable :: lflx, lflxold
    real, dimension (:,:), allocatable :: heatgrid
  end type save_arrays_type

  type globals_type
    logical :: initialized = .false.
    real :: ti_core, te_core
    real :: ne_core
    real :: ti_edge, te_edge
    real :: ne_edge
    real :: fusion_power
    real :: plasma_volume
    real :: plasma_current
    real :: fusion_gain
    real, dimension(:), allocatable :: net_power
    real, dimension(:), allocatable :: alph_power
    real, dimension(:), allocatable :: aux_power
    real, dimension(:), allocatable :: radiate_power
  end type globals_type


end module trinity_type_module
