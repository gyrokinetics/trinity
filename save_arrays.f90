module save_arrays
  !> A type for storing all the temporary information that are
  !! stored during the iteration, in case it is needed to 
  !! go back and change the timestep.
  !!
  !! sayv%nt, sayv%pflx, etc. are evaluated at m time level
  !! sayv%ntold, sayv%pflxold, etc. are evaluated at m-1 time level
  !! thus only needed for multi-step scheme
  type save_arrays_type
    logical :: initialized = .false.
    real :: rmserr
    real, dimension (:), allocatable :: nt, rhs, ntold, flx
    real, dimension (:,:,:), allocatable :: pflx, pflxold
    real, dimension (:,:,:), allocatable :: qflx, qflxold
    real, dimension (:,:,:), allocatable :: heat, heatold
    real, dimension (:,:), allocatable :: lflx, lflxold
    real, dimension (:,:), allocatable :: heatgrid
  end type save_arrays_type

contains 
  subroutine initialize_save_arrays(sayv)
    use trinity_input, only: nrad, ncc, njac, nspec, nprofs_evolved
    implicit none
    type(save_arrays_type), intent(inout) :: sayv
    integer :: np
    np=nprofs_evolved
    allocate (sayv%nt(np*nrad)) ; sayv%nt = 0.0
    allocate (sayv%rhs(np*nrad)) ; sayv%rhs = 0.0
    allocate (sayv%flx((2*nspec+1)*ncc)) ; sayv%flx = 0.0
    allocate (sayv%pflx(ncc,nspec,njac)) ; sayv%pflx = 0.0
    allocate (sayv%qflx(ncc,nspec,njac)) ; sayv%qflx = 0.0
    allocate (sayv%heat(ncc,nspec,njac)) ; sayv%heat = 0.0
    allocate (sayv%lflx(ncc,njac)) ; sayv%lflx = 0.0
    allocate (sayv%heatgrid(nrad,nspec)) ; sayv%heatgrid = 0.0
    allocate (sayv%ntold(np*nrad)) ; sayv%ntold = 0.0
    allocate (sayv%pflxold(ncc,nspec,njac)) ; sayv%pflxold = 0.0
    allocate (sayv%qflxold(ncc,nspec,njac)) ; sayv%qflxold = 0.0
    allocate (sayv%heatold(ncc,nspec,njac)) ; sayv%heatold = 0.0
    allocate (sayv%lflxold(ncc,njac)) ; sayv%lflxold = 0.0
    sayv%initialized = .true.
  end subroutine initialize_save_arrays

  subroutine finalize_save_arrays(sayv)
    implicit none
    type(save_arrays_type), intent(inout) :: sayv
    deallocate (sayv%nt) 
    deallocate (sayv%rhs) 
    deallocate (sayv%flx) 
    deallocate (sayv%pflx) 
    deallocate (sayv%qflx) 
    deallocate (sayv%heat) 
    deallocate (sayv%lflx) 
    deallocate (sayv%heatgrid) 
    deallocate (sayv%ntold) 
    deallocate (sayv%pflxold) 
    deallocate (sayv%qflxold) 
    deallocate (sayv%heatold) 
    deallocate (sayv%lflxold) 
    sayv%initialized = .false.
  end subroutine finalize_save_arrays
end module save_arrays
