!> Contains routines for numerical integration.
!! This is free software released under the MIT
!! License. 
!! Written by:
!!             Edmund Highcock
module trinity_integration

contains
  !> Calculate the integral of func from lower_lim to upper_lim
  !! where func is a real function which takes one real argument.
  !! resolution is the number of fence panels, i.e delta x = 
  !! (upper_lim - lower_lim) / resolution
  function simpson(lower_lim, upper_lim, func, resolution)
    use mp_trin, only: abort_mp
    real, intent(in) :: lower_lim, upper_lim
    real, external :: func
    integer, intent(in) :: resolution
    real :: simpson
    real :: delta_x
    integer :: i
    real :: x,w

    delta_x = (upper_lim-lower_lim)/real(resolution)
    simpson = 0.0
    x = lower_lim

    if (mod(resolution,2) .ne. 0) then
      write(*,*) 'ERROR: uneven resolution ', resolution, 'in simpson'
      call abort_mp
    end if

    do i = 1,resolution+1
      !w1 = mod(i,resolution)OD
      !w2 = w1 + (1-mod(w1,2)) + 
      
      ! This branching is very inefficient... should really
      ! do something smart with modulo arithmetic, but 
      ! that would be overkill for current purposes as this is only used
      ! for some small initial integrals
      if (i==1 .or. i==resolution-1) then
        w = 1.0
      else if (mod(i,2).eq.0) then
        w = 4.0
      else
        w = 2.0
      end if
      simpson = simpson + delta_x / 3.0 * w * func(x)
      x = x + delta_x
    end do
     

  end function simpson
end module trinity_integration
