!> A module which contains all the normalising
!! quantities needed to dimensionalise Trinity 
!! data
module norms
  implicit none
  real :: psitor_a, psipol_a, srcnorm, nsrcnorm, lsrcnorm, aref, amin_ref, rhostar, vtref
  real, dimension (:), allocatable :: nufac

  !> The gyroBohm normalisation used for the particle flux
  real, dimension (:,:), allocatable :: pflx_gb_cc
  !> The gyroBohm normalisation used for the heat flux
  real, dimension (:,:), allocatable :: qflx_gb_cc

  !> CHEASE normalising quantities
  real :: b0exp_chse, r0exp_chse
  contains
    !> Allocate arrays used to store normalising quantities
    subroutine allocate_norms
      use trinity_input, only: ncc, nspec
      ! array containing pre-factor for collision frequencies
      allocate (nufac(nspec))
      allocate (pflx_gb_cc(ncc,nspec))
      allocate (qflx_gb_cc(ncc,nspec))
    end subroutine allocate_norms
    subroutine deallocate_norms
      deallocate (nufac)
      deallocate(pflx_gb_cc)
      deallocate(qflx_gb_cc)
    end subroutine deallocate_norms

    subroutine set_gyrobohm_norm_arrays
      use trinity_input, only: nspec
      use nteqns_arrays, only: dens_cc, bmag_cc, pres_cc
      use trinity_input, only: m_ion, vtfac
      implicit none
      integer :: is
      do is=1,nspec
       ! see Eq. 17 of trin_norm_update.pdf
           pflx_gb_cc(:,is) = 3.22e20*dens_cc(:,2) &
            *(vtfac*pres_cc(:,2)/dens_cc(:,2))**1.5*sqrt(m_ion(1))/bmag_cc**2/aref**2.0
          ! gyrobohm heat flux in MW/m^2
           qflx_gb_cc(:,is) = 5.17655e-2* &
            dens_cc(:,2)*vtfac**1.5*(pres_cc(:,2)/dens_cc(:,2))**2.5*sqrt(m_ion(1))/bmag_cc**2/aref**2.0
        end do
    end subroutine set_gyrobohm_norm_arrays

    subroutine write_norms(gnostics)
      use trindiag_config, only: trindiag_type
      use trindiag_create_and_write, only: create_and_write_variable
      use trinity_input, only: m_ion, fluxlabel_option_switch, vtfac, aspr
      use simpledataio, only: SDATIO_INT
      type(trindiag_type), intent(inout) :: gnostics
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "aref", &
        "", "The normalising length (inout)", &
        "m", aref)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "amin_ref", &
        "", "The half radius of the last closed flux surface &
        & at the height of the midplane (inout)", &
        "m", aref)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "aspr", &
        "", "The aspect ratio of the last closed flux surface, &
        & equal to R_centre(LCFS)/rmin(LCFS), where rmin(LCFS) &
        & is, of course, amin_ref", &
        "1", aspr)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "psitor_a", &
        "", "The toroidal flux encompassed by the &
        & last closed flux surface. (inout)", &
        "Tesla m^2", aref)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "psipol_a", &
        "", "The poloidal flux encompassed by the &
        & last closed flux surface. (inout)", &
        "Tesla m^2", psipol_a)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "m_ion", &
        "ion", "The mass of each ion species (inout).", &
        "m_p", m_ion)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "vtfac", &
        "", "Factor in vth definition (inout).", &
        "1", vtfac)
      call create_and_write_variable(gnostics, SDATIO_INT, &
        "fluxlabeldef", &
        "", "Definition of flux label rho: 1 = rhotor, 2 = rmin (inout)", &
        "1", fluxlabel_option_switch)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "nsrcnorm", &
        "", "Density normalisation factor", &
        "10^20/m^3/s", nsrcnorm)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "srcnorm", &
        "", "Heating source normalisation factor", &
        "W/m^3", srcnorm)
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "lsrcnorm", &
        "", "Torque source normalisation factor", &
        "TBC", lsrcnorm)
    end subroutine write_norms
end module norms
