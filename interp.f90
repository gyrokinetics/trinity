module interp

  implicit none

  public :: fd2pt, fd2pt_cc, fd4pt_cc, fd3pt, spline, fpintrp, fd5pt, get_cc, get_gridval
  public :: smooth

  interface fd5pt
     module procedure fd5pt_real
     module procedure fd5pt_array
  end interface

  interface fd3pt
     module procedure fd3pt_real
     module procedure fd3pt_array
  end interface

  interface fd2pt
     module procedure fd2pt_real
     module procedure fd2pt_array
  end interface

contains
  
  ! linear interpolation (taken from NT code)

  function fpintrp(x,fa,xa,n)

! returns fpintrp= f(x)
! fa(i) = f(xa(i))set of n known values of x and f(x).
! xa is assumed to be ordered from smallest to largest

! if the xa array is equally spaced, then the faster finteq1d routine
! can be used.  Note that this routine does zero extrapolation beyond
! the endpoints of xa, while finteq1d does do extrapolation...

    integer, intent (in) :: n
    real, intent (in) :: x
    real, dimension (:), intent (in) :: fa, xa
    real :: fpintrp
    
    integer :: j

    j=lkupr(x,xa,n)+1
    
    if(j .eq. 1) then
       fpintrp=fa(1)    !default for x<xa(1)
       return
    endif
    
    if(j .gt. n)then
       fpintrp=fa(n)    !default for x>xa(n)
       return
    endif
    
    fpintrp=( fa(j-1)*(xa(j)-x) + fa(j)*(x-xa(j-1)) ) &
         / (xa(j)-xa(j-1))
    
    return

  end function fpintrp

  function lkupr(x,table,n)

! Look up a real number x in a table of length n.
! returns the value lkupr such that:

! table(lkupr) <= x < table(lkupr+1)

! assumes that table is in increasing order and does a binary search.
! This routine is called by fpintrp.

    integer, intent (in) :: n
    real, intent (in) :: x
    real, dimension (:), intent (in) :: table
    integer :: lkupr

    integer :: n1, n2, n3

    if (x .lt. table(1)) then
       lkupr=0
       go to 70000 !done
    end if

    if (x .ge. table(n)) then
       lkupr=n
       go to 70000 ! done
    end if

    n1=1
    n2=n

100 continue ! look again loop

    n3=(n2+n1)/2

    if (x .ge. table(n3)) then
       n1=n3
    else
       n2=n3
    end if

    if (n2 .eq. n1+1) then
       lkupr=n1
       go to 70000    ! done
    else
       go to 100      ! look again
    end if
    
70000 continue ! All returns from here

    return

  end function lkupr

  subroutine tridag (aa, bb, cc, sol)

    implicit none

    real, dimension (:), intent (in) :: aa, bb, cc
    real, dimension (:), intent (in out) :: sol

    integer :: ix, npts
    real :: bet

    real, dimension (:), allocatable :: gam
    
    npts = size(aa)
    allocate (gam(npts))

    bet = bb(1)
    sol(1) = sol(1)/bet

    do ix = 2, npts
       gam(ix) = cc(ix-1)/bet
       bet = bb(ix) - aa(ix)*gam(ix)
       if (bet == 0.0) write (*,*) 'tridiagonal solve failed'
       sol(ix) = (sol(ix)-aa(ix)*sol(ix-1))/bet
    end do

    do ix = npts-1, 1, -1
       sol(ix) = sol(ix) - gam(ix+1)*sol(ix+1)
    end do

    deallocate (gam)

  end subroutine tridag

  ! third order accurate scheme for cell-centered derivatives 
  ! using 4 grid values
  subroutine fd4pt_cc (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof
    real, dimension (:), intent (out) :: profgrad
    real, intent (in) :: dr

    integer :: ix

    ! note that the # of grid points is one greater than the # of cell-centers

    ! use uncentered differences for boundaries
    ix = 1
    profgrad(ix) = (-prof(ix+3) + 3.*prof(ix+2) + 21.*prof(ix+1) - 23.*prof(ix)) / (24.*dr)

    ix = size(profgrad)
    profgrad(ix) = (prof(ix-2) - 3*prof(ix-1) - 21.*prof(ix) + 23.*prof(ix+1)) / (24.*dr)

    ! use centered differences for non-boundary cell-centers
    do ix = 2, size(profgrad)-1
       profgrad(ix) = (prof(ix-1) - 27.*prof(ix) + 27.*prof(ix+1) - prof(ix+2)) / (24.*dr)
    end do

  end subroutine fd4pt_cc

  ! second order accurate derivative using 2 grid values
  ! and evaluating derivative at cell center (between 2 grid values)
  subroutine fd2pt_cc (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof
    real, dimension (:), intent (out) :: profgrad
    real, intent (in) :: dr

    integer :: ix

    do ix = 1, size(profgrad)
       profgrad(ix) = (prof(ix+1)-prof(ix))/dr
    end do

  end subroutine fd2pt_cc

  ! only good for equally-spaced grid-pts
  subroutine fd3pt_real (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof
    real, dimension (:), intent (out) :: profgrad
    real, intent (in) :: dr

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

    aa = 1.0 ; bb = 4.0 ; cc = 1.0
    aa(1) = 0.0 ; bb(1) = 0.5 ; cc(1) = 0.5
    aa(npts) = 0.5 ; bb(npts) = 0.5 ; cc(npts) = 0.0

    do ix = 2, npts-1
       profgrad(ix) = 3.0 * (prof(ix+1) - prof(ix-1)) / dr
    end do
    profgrad(1) = (prof(2)-prof(1))/dr
    profgrad(npts) = (prof(npts)-prof(npts-1))/dr

    call tridag (aa, bb, cc, profgrad)

    deallocate (aa, bb, cc)

  end subroutine fd3pt_real

  subroutine fd3pt_array (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof, dr
    real, dimension (:), intent (out) :: profgrad

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

!     aa = 1.0 ; bb = 4.0 ; cc = 1.0
!     aa(1) = 0.0 ; bb(1) = 0.5 ; cc(1) = 0.5
!     aa(npts) = 0.5 ; bb(npts) = 0.5 ; cc(npts) = 0.0

!     do ix = 2, npts-1
!        profgrad(ix) = 3.0 * (prof(ix+1) - prof(ix-1)) / dr(ix)
!     end do
!     profgrad(1) = (prof(2)-prof(1))/dr(1)
!     profgrad(npts) = (prof(npts)-prof(npts-1))/dr(npts-1)

!     call tridag (aa, bb, cc, profgrad)

    do ix = 2, npts-1
       profgrad(ix) = ((prof(ix)-prof(ix-1))*dr(ix)/dr(ix-1) &
            + (prof(ix+1)-prof(ix))*dr(ix-1)/dr(ix)) / (dr(ix-1)+dr(ix))
    end do
    profgrad(1) = (prof(2)-prof(1))/dr(1)
    profgrad(npts) = (prof(npts)-prof(npts-1))/dr(npts-1)

    deallocate (aa, bb, cc)

  end subroutine fd3pt_array

  ! boundary points are 2nd-order accurate (2-pt compact difference)
  ! next to boundary points are 4th-order accurate (2-pt centered compact difference)
  ! interior points are 6th-order accurate (4-pt centered compact difference)
  subroutine fd5pt_real (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof
    real, dimension (:), intent (out) :: profgrad
    real, intent (in) :: dr

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

    aa = 1.0 ; bb = 3.0 ; cc = 1.0
    aa(1) = 0.0 ; bb(1) = 0.5 ; cc(1) = 0.5
    aa(2) = 1.0 ; bb(2) = 4.0 ; cc(2) = 1.0
    aa(npts-1) = 1.0 ; bb(npts-1) = 4.0 ; cc(npts-1) = 1.0
    aa(npts) = 0.5 ; bb(npts) = 0.5 ; cc(npts) = 0.0

    do ix = 3, npts-2
       profgrad(ix) = (7.*(prof(ix+1) - prof(ix-1)) + 0.25*(prof(ix+2)-prof(ix-2))) / (3.*dr)
    end do
    profgrad(1) = (prof(2)-prof(1))/dr
    profgrad(2) = 3.0*(prof(3) - prof(1))/dr
    profgrad(npts-1) = 3.0*(prof(npts) - prof(npts-2))/dr
    profgrad(npts) = (prof(npts)-prof(npts-1))/dr

    call tridag (aa, bb, cc, profgrad)

    deallocate (aa, bb, cc)

  end subroutine fd5pt_real

  ! boundary points are 2nd-order accurate (2-pt compact difference)
  ! next to boundary points are 4th-order accurate (2-pt centered compact difference)
  ! interior points are 6th-order accurate (4-pt centered compact difference)
  subroutine fd5pt_array (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof, dr
    real, dimension (:), intent (out) :: profgrad

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

    aa = 1.0 ; bb = 3.0 ; cc = 1.0
    aa(1) = 0.0 ; bb(1) = 0.5 ; cc(1) = 0.5
    aa(2) = 1.0 ; bb(2) = 4.0 ; cc(2) = 1.0
    aa(npts-1) = 1.0 ; bb(npts-1) = 4.0 ; cc(npts-1) = 1.0
    aa(npts) = 0.5 ; bb(npts) = 0.5 ; cc(npts) = 0.0

    do ix = 3, npts-2
       profgrad(ix) = (7.*(prof(ix+1) - prof(ix-1)) + 0.25*(prof(ix+2)-prof(ix-2))) / (3.*dr(ix))
    end do
    profgrad(1) = (prof(2)-prof(1))/dr(1)
    profgrad(2) = 3.0*(prof(3) - prof(1))/dr(2)
    profgrad(npts-1) = 3.0*(prof(npts) - prof(npts-2))/dr(npts-1)
    profgrad(npts) = (prof(npts)-prof(npts-1))/dr(npts)

    call tridag (aa, bb, cc, profgrad)

    deallocate (aa, bb, cc)

  end subroutine fd5pt_array

  ! 2nd-order accurate (2-pt compact difference)
  subroutine fd2pt_real (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof
    real, dimension (:), intent (out) :: profgrad
    real, intent (in) :: dr

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

    aa = 0.0 ; bb = 0.5 ; cc = 0.5
    aa(npts) = 0.0 ; bb(npts) = 1.0 ; cc(npts) = 0.0

    do ix = 1, npts-1
       profgrad(ix) = (prof(ix+1)-prof(ix))/dr
    end do
    ! at boundary, use simple backwards difference to obtain answer
    ! this is only 1st-order accurate and could easily be improved
    profgrad(npts) = profgrad(npts-1)

    call tridag (aa, bb, cc, profgrad)

    deallocate (aa, bb, cc)

  end subroutine fd2pt_real

  ! 2nd-order accurate (2-pt compact difference)
  subroutine fd2pt_array (prof, profgrad, dr)

    implicit none

    real, dimension (:), intent (in) :: prof, dr
    real, dimension (:), intent (out) :: profgrad

    integer :: ix, npts
    real, dimension (:), allocatable :: aa, bb, cc

    npts = size(prof)
    allocate (aa(npts), bb(npts), cc(npts))

    aa = 0.0 ; bb = 0.5 ; cc = 0.5
    aa(npts) = 0.0 ; bb(npts) = 1.0 ; cc(npts) = 0.0

    do ix = 1, npts-1
       profgrad(ix) = (prof(ix+1)-prof(ix))/dr(ix)
    end do
    ! at boundary, use simple backwards difference to obtain answer
    ! this is only 1st-order accurate and could easily be improved
    profgrad(npts) = profgrad(npts-1)

    call tridag (aa, bb, cc, profgrad)

    deallocate (aa, bb, cc)

  end subroutine fd2pt_array

  ! takes array 'grid' containing values at grid locations and
  ! constructs array 'cc' containing values at cell centers
  ! uses 4 point stencil for central points (4th order accurate)
  ! and 3 point stencil for boundary points (3rd order accurate)
  subroutine get_cc (grid, cc)
    
    implicit none

    integer :: ix

    real, dimension (:), intent (in) :: grid
    real, dimension (:), intent (out) :: cc

    ! deal with inner boundary
    ix=1
    cc(ix) = 0.125*(3.0*grid(ix) + 6.0*grid(ix+1) - grid(ix+2))

    ! deal with outer boundary
    ix = size(cc)
    cc(ix) = 0.125*(3.0*grid(ix+1) + 6.0*grid(ix) - grid(ix-1))

    ! inner cell centers
    do ix = 2, size(cc)-1
       cc(ix) = 0.0625*(-grid(ix-1) + 9.0*grid(ix) + 9.0*grid(ix+1) - grid(ix+2))
    end do

  end subroutine get_cc

  ! takes array 'cc' containing values at cell centers and
  ! constructs array 'grid' containing values at grid locations
  subroutine get_gridval (cc, grid, axis_in)

    implicit none

    real, dimension (:), intent (in) :: cc
    real, dimension (:), intent (out) :: grid
    real, intent (in), optional :: axis_in

    integer :: npts, ix
    real :: axis

    if (present(axis_in)) then
       axis = axis_in
    else
       ! value of fluxes and area on axis are zero
       axis = 0.0
    end if

    npts = size(grid)

    ! this is 3rd order accurate at the boundaries
    ! and 4th order accurate at the central points
    grid(1) = 0.375*axis + 0.75*cc(1) - 0.125*cc(2)
    grid(2) = 0.5625*(cc(1)+cc(2)) - 0.0625*(cc(3)+axis)
    do ix = 3, npts-2
       grid(ix) = 0.0625*(-(cc(ix-2)+cc(ix+1))+9.*(cc(ix-1)+cc(ix)))
    end do
    grid(npts-1) = 0.375*cc(npts-1)+0.75*cc(npts-2)-0.125*cc(npts-3)
    ! this is only second order accurate, but shouldn't matter because
    ! should never really need these values at outer boundary
    grid(npts) = 2.0*cc(npts-1)-cc(npts-2)
    
!     do ix = 2, npts-1
!        grid(ix) = 0.5*(cc(ix-1)+cc(ix))
!     end do
!     grid(1) = 2.0*cc(2) - cc(3)
!     grid(npts) = 2.0*cc(npts-1)-cc(npts-2)
    
  end subroutine get_gridval

  !> Piecewise linear interpolation
  ! x and y are values
  ! xint is grid to interpolate onto
  ! yint is interpolated values
  ! size(x) must be gt 1
  ! x must be ordered left to right, i.e. 
  ! x(i+1) .gt. x(i), and similarly for xint
  ! yintp not implemented yet
  subroutine plinterp (x,y,xint,yint,yintp)
    use mp_trin, only: abort_mp
    implicit none
    real, dimension (:), intent (in) :: x,y,xint
    real, dimension (:), intent (out) :: yint
    real, dimension (:), intent (out), optional :: yintp
    integer :: i, iint
    real :: xv, yv

    if (xint(1) .lt. x(1)) then
      write (*,*) 'xint(1), ', xint(1), ' lt x(1) ', x(1)
      write (*,*) ' in plinterp'
      call abort_mp
    end if
    i = 1
    do iint = 1,size(xint)
      xv = xint(iint)
      !!if (abs(xv - x(i))abs(x(i)) .lt. epsilon(0.0)) then
        !yv = y(i)
      if (xv .gt. x(i+1)) then
        if (i .eq. size(x)) then
          write (*,*) 'xv, ', xv, 'is > x(size(x))', x(size(x))
          write (*,*) ' in plinterp'
          call abort_mp
        else
          i = i + 1
        end if
      end if
       !if (xv .gt. x(i) .and. xv .lt. x(i+1)) then 
      yv = y(i) + (y(i+1)-y(i)) * (xv - x(i))/(x(i+1) - x(i))
      !write (*,*) 'yv ', yv, 'y(i)', y(i), y(i+1), xv, x(i), x(i+1)
      !end if
      yint(iint) = yv
    end do
  end subroutine plinterp

  ! x will be rad
  ! y will be dens, temp, etc.
  ! xint will be interpolated grid points
  ! yint will be value of y at xint points
  ! yintp will be y' at xint points
  subroutine spline (x,y,xint,yint,yintp)
    implicit none
    real, dimension (:), intent (in) :: x,y,xint
    real, dimension (:), intent (out) :: yint, yintp
    integer :: n, ierr, ix
    real :: dum1, dum2, sigma
    real, dimension (:), allocatable :: ypp, dum3
    n = size(x)
    allocate (ypp(n),dum3(n))
    sigma = 1.0
    call fitp_curv1 (n,x,y,dum1,dum2,3,ypp,dum3,sigma,ierr)
    do ix = 1, size(xint)
       yint(ix) = fitp_curv2 (xint(ix),n,x,y,ypp,sigma)
       yintp(ix) = fitp_curvd (xint(ix),n,x,y,ypp,sigma)
    end do
    deallocate (ypp, dum3)
  end subroutine spline

  subroutine fitp_curv1 (n,x,y,slp1,slpn,islpsw,yp,temp,sigma,ierr)    

    integer n,islpsw,ierr
    real x(n),y(n),slp1,slpn,yp(n),temp(n),sigma
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this subroutine determines the parameters necessary to
! compute an interpolatory spline under tension through
! a sequence of functional values. the slopes at the two
! ends of the curve may be specified or omitted.  for actual
! computation of points on the curve it is necessary to call
! the function curv2.
!
! on input--
!
!   n is the number of values to be interpolated (n.ge.2).
!
!   x is an array of the n increasing abscissae of the
!   functional values.
!
!   y is an array of the n ordinates of the values, (i. e.
!   y(k) is the functional value corresponding to x(k) ).
!
!   slp1 and slpn contain the desired values for the first
!   derivative of the curve at x(1) and x(n), respectively.
!   the user may omit values for either or both of these
!   parameters and signal this with islpsw.
!
!   islpsw contains a switch indicating which slope data
!   should be used and which should be estimated by this
!   subroutine,
!          = 0 if slp1 and slpn are to be used,
!          = 1 if slp1 is to be used but not slpn,
!          = 2 if slpn is to be used but not slp1,
!          = 3 if both slp1 and slpn are to be estimated
!              internally.
!
!   yp is an array of length at least n.
!
!   temp is an array of length at least n which is used for
!   scratch storage.
!
! and
!
!   sigma contains the tension factor. this value indicates
!   the curviness desired. if abs(sigma) is nearly zero
!   (e.g. .001) the resulting curve is approximately a
!   cubic spline. if abs(sigma) is large (e.g. 50.) the
!   resulting curve is nearly a polygonal line. if sigma
!   equals zero a cubic spline results.  a standard value
!   for sigma is approximately 1. in absolute value.
!
! on output--
!
!   yp contains the values of the second derivative of the
!   curve at the given nodes.
!
!   ierr contains an error flag,
!        = 0 for normal return,
!        = 1 if n is less than 2,
!        = 2 if x-values are not strictly increasing.
!
! and
!
!   n, x, y, slp1, slpn, islpsw and sigma are unaltered.
!
! this subroutine references package modules ceez, terms,
! and snhcsh.
!
!-----------------------------------------------------------
    integer i, ibak, nm1, np1
    real sdiag1, diag1, delxnm, dx1, diag, sdiag2, dx2, diag2
    real delxn, slpp1, delx1, sigmap, c3, c2, c1, slppn, delx2
    
    nm1 = n-1
    np1 = n+1
    ierr = 0
    if (n .le. 1) go to 8
    if (x(n) .le. x(1)) go to 9
!
! denormalize tension factor
!
    sigmap = abs(sigma)*real(n-1)/(x(n)-x(1))
!
! approximate end slopes
!
    if (islpsw .ge. 2) go to 1
    slpp1 = slp1
    go to 2
1   delx1 = x(2)-x(1)
    delx2 = delx1+delx1
    if (n .gt. 2) delx2 = x(3)-x(1)
    if (delx1 .le. 0. .or. delx2 .le. delx1) go to 9
    call fitp_ceez (delx1,delx2,sigmap,c1,c2,c3,n)
    slpp1 = c1*y(1)+c2*y(2)
    if (n .gt. 2) slpp1 = slpp1+c3*y(3)
2   if (islpsw .eq. 1 .or. islpsw .eq. 3) go to 3
    slppn = slpn
    go to 4
3   delxn = x(n)-x(nm1)
    delxnm = delxn+delxn
    if (n .gt. 2) delxnm = x(n)-x(n-2)
    if (delxn .le. 0. .or. delxnm .le. delxn) go to 9
    call fitp_ceez (-delxn,-delxnm,sigmap,c1,c2,c3,n)
    slppn = c1*y(n)+c2*y(nm1)
    if (n .gt. 2) slppn = slppn+c3*y(n-2)
!
! set up right hand side and tridiagonal system for yp and
! perform forward elimination
!
4   delx1 = x(2)-x(1)
    if (delx1 .le. 0.) go to 9
    dx1 = (y(2)-y(1))/delx1
    call fitp_terms (diag1,sdiag1,sigmap,delx1)
    yp(1) = (dx1-slpp1)/diag1
    temp(1) = sdiag1/diag1
    if (n .eq. 2) go to 6
    do i = 2,nm1
       delx2 = x(i+1)-x(i)
       if (delx2 .le. 0.) go to 9
       dx2 = (y(i+1)-y(i))/delx2
       call fitp_terms (diag2,sdiag2,sigmap,delx2)
       diag = diag1+diag2-sdiag1*temp(i-1)
       yp(i) = (dx2-dx1-sdiag1*yp(i-1))/diag
       temp(i) = sdiag2/diag
       dx1 = dx2
       diag1 = diag2
       sdiag1 = sdiag2
    end do
6   diag = diag1-sdiag1*temp(nm1)
    yp(n) = (slppn-dx1-sdiag1*yp(nm1))/diag
!
! perform back substitution
!
    do i = 2,n
       ibak = np1-i
       yp(ibak) = yp(ibak)-temp(ibak)*yp(ibak+1)
    end do
    return
!
! too few points
!
8   ierr = 1
    return
!
! x-values not strictly increasing
!
9   ierr = 2
    return
  end subroutine fitp_curv1

  subroutine fitp_ceez (del1,del2,sigma,c1,c2,c3,n)

    real del1,del2,sigma,c1,c2,c3
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this subroutine determines the coefficients c1, c2, and c3
! used to determine endpoint slopes. specifically, if
! function values y1, y2, and y3 are given at points x1, x2,
! and x3, respectively, the quantity c1*y1 + c2*y2 + c3*y3
! is the value of the derivative at x1 of a spline under
! tension (with tension factor sigma) passing through the
! three points and having third derivative equal to zero at
! x1. optionally, only two values, c1 and c2 are determined.
!
! on input--
!
!   del1 is x2-x1 (.gt. 0.).
!
!   del2 is x3-x1 (.gt. 0.). if n .eq. 2, this parameter is
!   ignored.
!
!   sigma is the tension factor.
!
! and
!
!   n is a switch indicating the number of coefficients to
!   be returned. if n .eq. 2 only two coefficients are
!   returned. otherwise all three are returned.
!
! on output--
!
!   c1, c2, and c3 contain the coefficients.
!
! none of the input parameters are altered.
!
! this subroutine references package module snhcsh.
!
!-----------------------------------------------------------
    integer n
    real delm, delp, sinhmp, denom, sinhmm, del, dummy, coshm2, coshm1

    if (n .eq. 2) go to 2
    if (sigma .ne. 0.) go to 1
    del = del2-del1
!
! tension .eq. 0.
!
    c1 = -(del1+del2)/(del1*del2)
    c2 = del2/(del1*del)
    c3 = -del1/(del2*del)
    return
!
! tension .ne. 0.
!
1   call fitp_snhcsh (dummy,coshm1,sigma*del1,1)
    call fitp_snhcsh (dummy,coshm2,sigma*del2,1)
    delp = sigma*(del2+del1)/2.
    delm = sigma*(del2-del1)/2.
    call fitp_snhcsh (sinhmp,dummy,delp,-1)
    call fitp_snhcsh (sinhmm,dummy,delm,-1)
    denom = coshm1*(del2-del1)-2.*del1*delp*delm*(1.+sinhmp)*(1.+sinhmm)
    c1 = 2.*delp*delm*(1.+sinhmp)*(1.+sinhmm)/denom
    c2 = -coshm2/denom
    c3 = coshm1/denom
    return
!
! two coefficients
!
2   c1 = -1./del1
    c2 = -c1
    return
  end subroutine fitp_ceez

   subroutine fitp_terms (diag,sdiag,sigma,del)
!
     real diag,sdiag,sigma,del
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this subroutine computes the diagonal and superdiagonal
! terms of the tridiagonal linear system associated with
! spline under tension interpolation.
!
! on input--
!
!   sigma contains the tension factor.
!
! and
!
!   del contains the step size.
!
! on output--
!
!                sigma*del*cosh(sigma*del) - sinh(sigma*del)
!   diag = del*--------------------------------------------.
!                     (sigma*del)**2 * sinh(sigma*del)
!
!                   sinh(sigma*del) - sigma*del
!   sdiag = del*----------------------------------.
!                (sigma*del)**2 * sinh(sigma*del)
!
! and
!
!   sigma and del are unaltered.
!
! this subroutine references package module snhcsh.
!
!-----------------------------------------------------------
     real coshm, denom, sigdel, sinhm

     if (sigma .ne. 0.) go to 1
     diag = del/3.
     sdiag = del/6.
     return
1    sigdel = sigma*del
     call fitp_snhcsh (sinhm,coshm,sigdel,0)
     denom = sigma*sigdel*(1.+sinhm)
     diag = (coshm-sinhm)/denom
     sdiag = sinhm/denom
     return
   end subroutine fitp_terms

   subroutine fitp_snhcsh (sinhm,coshm,x,isw)

     integer isw
     real sinhm,coshm,x
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this subroutine returns approximations to
!       sinhm(x) = sinh(x)/x-1
!       coshm(x) = cosh(x)-1
! and
!       coshmm(x) = (cosh(x)-1-x*x/2)/(x*x)
! with relative error less than 1.0e-6
!
! on input--
!
!   x contains the value of the independent variable.
!
!   isw indicates the function desired
!           = -1 if only sinhm is desired,
!           =  0 if both sinhm and coshm are desired,
!           =  1 if only coshm is desired,
!           =  2 if only coshmm is desired,
!           =  3 if both sinhm and coshmm are desired.
!
! on output--
!
!   sinhm contains the value of sinhm(x) if isw .le. 0 or
!   isw .eq. 3 (sinhm is unaltered if isw .eq.1 or isw .eq.
!   2).
!
!   coshm contains the value of coshm(x) if isw .eq. 0 or
!   isw .eq. 1 and contains the value of coshmm(x) if isw
!   .ge. 2 (coshm is unaltered if isw .eq. -1).
!
! and
!
!   x and isw are unaltered.
!
!-----------------------------------------------------------
     real sp10, sp11, sp12, sp13
     real sp20, sp21, sp22, sp23, sp24
     real sp31, sp32, sp33
     real sq30, sq31, sq32
     real sp41, sp42, sp43
     real sq40, sq41, sq42
     real cp0, cp1, cp2, cp3, cp4
     real ax, xs, expx

     data sp13/.3029390e-5/, sp12/.1975135e-3/, sp11/.8334261e-2/, sp10/.1666665e0/
     data sp24/.3693467e-7/, sp23/.2459974e-5/, sp22/.2018107e-3/, sp21/.8315072e-2/, sp20/.1667035e0/
     data sp33/.6666558e-5/, sp32/.6646307e-3/, sp31/.4001477e-1/, sq32/.2037930e-3/, sq31/-.6372739e-1/, sq30/.6017497e1/
     data sp43/.2311816e-4/, sp42/.2729702e-3/, sp41/.9868757e-1/, sq42/.1776637e-3/, sq41/-.7549779e-1/, sq40/.9110034e1/
     data cp4/.2982628e-6/, cp3/.2472673e-4/, cp2/.1388967e-2/, cp1/.4166665e-1/, cp0/.5000000e0/

     ax = abs(x)
     if (isw .ge. 0) go to 5
!
! sinhm approximation
!
     if (ax .gt. 4.45) go to 2
     xs = ax*ax
     if (ax .gt. 2.3) go to 1
!
! sinhm approximation on (0.,2.3)
!
     sinhm = xs*(((sp13*xs+sp12)*xs+sp11)*xs+sp10)
     return
!
! sinhm approximation on (2.3,4.45)
!
1    sinhm = xs*((((sp24*xs+sp23)*xs+sp22)*xs+sp21)*xs+sp20)
     return
2    if (ax .gt. 7.65) go to 3
!
! sinhm approximation on (4.45,7.65)
!
     xs = ax*ax
     sinhm = xs*(((sp33*xs+sp32)*xs+sp31)*xs+1.)/((sq32*xs+sq31)*xs+sq30)
     return
3    if (ax .gt. 10.1) go to 4
!
! sinhm approximation on (7.65,10.1)
!
     xs = ax*ax
     sinhm = xs*(((sp43*xs+sp42)*xs+sp41)*xs+1.)/((sq42*xs+sq41)*xs+sq40)
     return
!
! sinhm approximation above 10.1
!
4    sinhm = exp(ax)/(ax+ax)-1.
     return
!
! coshm and (possibly) sinhm approximation
!
5    if (isw .ge. 2) go to 7
     if (ax .gt. 2.3) go to 6
     xs = ax*ax
     coshm = xs*((((cp4*xs+cp3)*xs+cp2)*xs+cp1)*xs+cp0)
     if (isw .eq. 0) sinhm = xs*(((sp13*xs+sp12)*xs+sp11)*xs+sp10)
     return
6    expx = exp(ax)
     coshm = (expx+1./expx)/2.-1.
     if (isw .eq. 0) sinhm = (expx-1./expx)/(ax+ax)-1.
     return
!
! coshmm and (possibly) sinhm approximation
!
7    xs = ax*ax
     if (ax .gt. 2.3) go to 8
     coshm = xs*(((cp4*xs+cp3)*xs+cp2)*xs+cp1)
     if (isw .eq. 3) sinhm = xs*(((sp13*xs+sp12)*xs+sp11)*xs+sp10)
     return
8    expx = exp(ax)
     coshm = ((expx+1./expx-xs)/2.-1.)/xs
     if (isw .eq. 3) sinhm = (expx-1./expx)/(ax+ax)-1.
     return
   end subroutine fitp_snhcsh

  real function fitp_curv2 (t,n,x,y,yp,sigma)

    integer n
    real t,x(n),y(n),yp(n),sigma
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this function interpolates a curve at a given point
! using a spline under tension. the subroutine curv1 should
! be called earlier to determine certain necessary
! parameters.
!
! on input--
!
!   t contains a real value to be mapped onto the interpo-
!   lating curve.
!
!   n contains the number of points which were specified to
!   determine the curve.
!
!   x and y are arrays containing the abscissae and
!   ordinates, respectively, of the specified points.
!
!   yp is an array of second derivative values of the curve
!   at the nodes.
!
! and
!
!   sigma contains the tension factor (its sign is ignored).
!
! the parameters n, x, y, yp, and sigma should be input
! unaltered from the output of curv1.
!
! on output--
!
!   curv2 contains the interpolated value.
!
! none of the input parameters are altered.
!
! this function references package modules intrvl and
! snhcsh.
!
!-----------------------------------------------------------
    integer i, im1
    real ss, sigdel, dummy, s1, s2, sum, sigmap
    real del1, del2, dels
!
! determine interval
!
    im1 = fitp_intrvl(t,x,n)
    i = im1+1
!
! denormalize tension factor
!
    sigmap = abs(sigma)*real(n-1)/(x(n)-x(1))
!
! set up and perform interpolation
!
    del1 = t-x(im1)
    del2 = x(i)-t
    dels = x(i)-x(im1)
    sum = (y(i)*del1+y(im1)*del2)/dels
    if (sigmap .ne. 0.) go to 1
    fitp_curv2 = sum-del1*del2*(yp(i)*(del1+dels)+yp(im1)*(del2+dels))/(6.*dels)
    return
1   sigdel = sigmap*dels
    call fitp_snhcsh (ss,dummy,sigdel,-1)
    call fitp_snhcsh (s1,dummy,sigmap*del1,-1)
    call fitp_snhcsh (s2,dummy,sigmap*del2,-1)
    fitp_curv2 = sum+(yp(i)*del1*(s1-ss)+yp(im1)*del2*(s2-ss))/(sigdel*sigmap*(1.+ss))
    return
  end function fitp_curv2

  real function fitp_curvd (t,n,x,y,yp,sigma)

    integer n
    real t,x(n),y(n),yp(n),sigma
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this function differentiates a curve at a given point
! using a spline under tension. the subroutine curv1 should
! be called earlier to determine certain necessary
! parameters.
!
! on input--
!
!   t contains a real value at which the derivative is to be
!   determined.
!
!   n contains the number of points which were specified to
!   determine the curve.
!
!   x and y are arrays containing the abscissae and
!   ordinates, respectively, of the specified points.
!
!   yp is an array of second derivative values of the curve
!   at the nodes.
!
! and
!
!   sigma contains the tension factor (its sign is ignored).
!
! the parameters n, x, y, yp, and sigma should be input
! unaltered from the output of curv1.
!
! on output--
!
!   curvd contains the derivative value.
!
! none of the input parameters are altered.
!
! this function references package modules intrvl and
! snhcsh.
!
!-----------------------------------------------------------
    integer i, im1
    real ss, sigdel, dummy, c1, c2, sum, sigmap
    real del1, del2, dels
!
! determine interval
!
    im1 = fitp_intrvl(t,x,n)
    i = im1+1
!
! denormalize tension factor
!
    sigmap = abs(sigma)*real(n-1)/(x(n)-x(1))
!
! set up and perform differentiation
!
    del1 = t-x(im1)
    del2 = x(i)-t
    dels = x(i)-x(im1)
    sum = (y(i)-y(im1))/dels
    if (sigmap .ne. 0.) go to 1
    fitp_curvd = sum+(yp(i)*(2.*del1*del1-del2*(del1+dels))- &
         yp(im1)*(2.*del2*del2-del1*(del2+dels))) &
         /(6.*dels)
    return
1   sigdel = sigmap*dels
    call fitp_snhcsh (ss,dummy,sigdel,-1)
    call fitp_snhcsh (dummy,c1,sigmap*del1,1)
    call fitp_snhcsh (dummy,c2,sigmap*del2,1)
    fitp_curvd = sum+(yp(i)*(c1-ss)-yp(im1)*(c2-ss))/(sigdel*sigmap*(1.+ss))
    return
  end function fitp_curvd

   integer function fitp_intrvl (t,x,n)

     integer n
     real t,x(n)
!
!                                 coded by alan kaylor cline
!                           from fitpack -- january 26, 1987
!                        a curve and surface fitting package
!                      a product of pleasant valley software
!                  8603 altus cove, austin, texas 78759, usa
!
! this function determines the index of the interval
! (determined by a given increasing sequence) in which
! a given value lies.
!
! on input--
!
!   t is the given value.
!
!   x is a vector of strictly increasing values.
!
! and
!
!   n is the length of x (n .ge. 2).
!
! on output--
!
!   intrvl returns an integer i such that
!
!          i =  1       if         e   t .lt. x(2)  ,
!          i =  n-1     if x(n-1) .le. t            ,
!          otherwise       x(i)  .le. t .le. x(i+1),
!
! none of the input parameters are altered.
!
!-----------------------------------------------------------
     integer il, ih, i
     real tt

     save i
     data i /1/

     tt = t
!
! check for illegal i
!
     if (i .ge. n) i = n/2
!
! check old interval and extremes
!
     if (tt .lt. x(i)) then
        if (tt .le. x(2)) then
           i = 1
           fitp_intrvl = 1
           return
        else
           il = 2
           ih = i
        end if
     else if (tt .le. x(i+1)) then
        fitp_intrvl = i
        return
     else if (tt .ge. x(n-1)) then
        i = n-1
        fitp_intrvl = n-1
        return
     else
        il = i+1
        ih = n-1
     end if
!
! binary search loop
!
1    i = (il+ih)/2
     if (tt .lt. x(i)) then
        ih = i
     else if (tt .gt. x(i+1)) then
        il = i+1
     else
        fitp_intrvl = i
        return
     end if
     go to 1
   end function fitp_intrvl

   subroutine smooth (grid, idx_window, unsmoothed, smoothed)

     implicit none

     real, dimension (:), intent (in) :: grid
     integer, intent (in) :: idx_window
     real, dimension (:,:), intent (in) :: unsmoothed
     real, dimension (:,:), intent (out) :: smoothed

     integer :: i, m, n, imin, imax
     real, dimension (:), allocatable :: dgrid

     m = size(unsmoothed,1)
     n = size(grid)
     allocate (dgrid(n-1))
     dgrid = grid(2:n)-grid(:n-1)
     
     do i = 1, n
        imin = i-idx_window ; imax = i+idx_window
        if (imin < 1 .or. imax > n-1) then
           smoothed(:,i) = unsmoothed(:,i)
        else
           smoothed(:,i) = sum(unsmoothed(:,imin:imax)*spread(dgrid(imin:imax),1,m),2)/spread(sum(dgrid(imin:imax)),1,m)
        end if
     end do
     

     deallocate (dgrid)
     
   end subroutine smooth

end module interp
