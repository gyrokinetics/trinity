DEBUG ?=
DBLE ?= on
OPT ?= on
USE_MPI = on
FLUXCODE ?=
STATIC ?=
USE_SEND ?=
USE_EXPRO ?=

USE_FPIC ?=
#
# Compile with the new netcdf diagnostics module
USE_NEW_DIAG ?= on

MAKE= make
F90FLAGS= 
F90OPTFLAGS=
FC= gfortran
MPIFC ?= mpif90
CPP= cpp
#CPPFLAGS= -C -P -traditional
CPPFLAGS= -C -P -nostdinc  
CC= gcc
MPICC ?= mpicc
CFLAGS= 
COPTFLAGS=
LIBS=
LDFLAGS=

MPI_INC ?=
MPI_LIB ?=
FLUXCODE_INCS ?=
FLUXCODE_LIBS ?=

ARCH 		= ar
ARCHFLAGS 	= cr
RANLIB		= ranlib

DYNAMIC_FLAG=-Bdynamic
DYNAMIC_FLAG=

#SYSTEM = mac
ifdef GK_SYSTEM
SYSTEM=$(GK_SYSTEM)
endif

ifndef SYSTEM
SYSTEM = mac
$(error: SYSTEM is not set)
endif

include Makefile.$(SYSTEM)

ifeq ($(USE_MPI),on)
	FC = $(MPIFC)
	CC = $(MPICC)
	CPPFLAGS += -DMPI
endif

ifeq ($(USE_NEW_DIAG),on)
	CPPFLAGS+=-DNEW_DIAG
	sinclude diagnostics/Makefile.diagnostics
else
distclean_simpledataio:
clean_simpledataio:
diagnostics:
simpledataio:
endif


ifeq ($(USE_EXPRO),on)
	GEO_LIBS += $(GACODE_ROOT)/shared/EXPRO/EXPRO_lib.a
	GEO_LIBS += $(GACODE_ROOT)/shared/GEO/GEO_lib.a
	GEO_LIBS += $(GACODE_ROOT)/shared/math/math_lib.a
	GEO_INC += -I$(GACODE_ROOT)/modules
	CPPFLAGS += -DUSE_EXPRO
endif

ifeq ($(FLUXCODE),tglf)
	GEO_LIBS += $(GACODE_ROOT)/shared/GEO/GEO_lib.a
	GEO_LIBS += $(GACODE_ROOT)/shared/harvest_client/libharvest.a
	GEO_LIBS += $(GACODE_ROOT)/shared/math/math_lib.a
	GEO_INC += -I$(GACODE_ROOT)/modules
	TGLF_LIB += $(GACODE_ROOT)/tglf/src/tglf_lib.a
	FLUXCODE_LIBS += $(TGLF_LIB) $(LAPACK_LIB) $(BLAS_LIB)
	CPPFLAGS += -DUSE_TGLF
endif

ifeq ($(USE_NEO),on)
	GEO_LIBS += $(GACODE_ROOT)/shared/GEO/GEO_lib.a
	GEO_LIBS += $(GACODE_ROOT)/shared/EXPRO/EXPRO_lib.a
	GEO_LIBS += $(GACODE_ROOT)/shared/harvest_client/libharvest.a
	GEO_LIBS += $(GACODE_ROOT)/shared/math/math_lib.a
	GEO_INC += -I$(GACODE_ROOT)/modules
	NEO_LIB += $(GACODE_ROOT)/neo/src/neo_lib.a $(GACODE_ROOT)/shared/UMFPACK/UMFPACK_lib.a  $(GACODE_ROOT)/shared/nclass/nclass_lib.a
	FLUXCODE_LIBS += $(NEO_LIB)
	CPPFLAGS += -DUSE_NEO
endif

#MPIINC = -I/usr/common/usg/openmpi/1.4.5/pgi/include

ifdef GRYFX
  LD = $(NVCC)
	CCLD = $(NVCC)
  ifndef GS2
	  $(error "Please specify GS2, the folder where the gs2 library is")
  endif
	GRYFX_LIB = $(GRYFX)/libgryfx.a
	FLUXCODE_LIBS += $(GRYFX_LIB) $(FFT_LIB) $(NETCDF_LIB) $(NVCCINCS) $(NVCCLIBS)
	FLUXCODE_INCS += $(FFT_INC) $(NETCDF_INC)
	CPPFLAGS += -DUSE_GRYFX
	CCLDFLAGS = --compiler-options '-fPIC'
	GEO_LIBS=${GS2}/geometry_c_interface.o #${GS2_DIR}/geo.a  ${GS2_DIR}/utils.a
	USE_FPIC=on
else
	GRYFX_LIB = $(PWD)/dummy_interfaces/libgryfx.a
  FLUXCODE_LIBS += $(GRYFX_LIB)
  LD = $(FC)
	CCLDFLAGS = -fPIC
  CCLD = $(CC)
endif

ifdef GS2
	GS2_LIB = $(GS2)/libgs2.a
	FLUXCODE_INCS += $(NETCDF_INC) $(FFT_INC) 
	ifdef OLD_GS2_INTERFACE
		CPPFLAGS += -DOLD_GS2_INTERFACE
	else
		F90FLAGS += -I$(GS2)
	endif


  # Here we check if gs2 version is greater than 6 
	# by searching for the symbol initialize_gs2 in the gs2 library
	HAS_GT_6 = $(shell if grep initialize_gs2 $(GS2)/libgs2.a > /dev/null; then echo "GOT"; else echo "NOTGOT"; fi)

	FLUXCODE_LIBS += $(GS2_LIB) $(SIMPLEDATAIO_LIBS) $(NETCDF_LIB) $(FFT_LIB) 
	GS2_LIB_DEP=$(GS2)/libgs2.a
else
	CPPFLAGS += -DGS2_DUMMY_INTERFACE
	FLUXCODE_LIBS += $(PWD)/dummy_interfaces/libgs2.a
	GS2_LIB_DEP = $(PWD)/dummy_interfaces/libgs2.a
	ifdef OLD_GS2_INTERFACE
		CPPFLAGS += -DOLD_GS2_INTERFACE
	else
		FLUXCODE_INCS += -I$(PWD)/dummy_interfaces/
	endif
	HAS_GT_6 = NOTGOT
endif


# For unit tests
export HAS_GT_6

# Must be after ifdef GRYFX
ifeq ($(USE_FPIC),on)
	CFLAGS += -fPIC
	F90FLAGS += -fPIC
endif

# If the gs2 version is less than or equal to 6,
# define a preprocessor flag to strip out references
# to the later interface.
ifeq ($(HAS_GT_6), NOTGOT)
	 CPPFLAGS += -DGS2_VERSION_LE_6
endif

ifeq ($(FLUXCODE),gene)
	GENEDIR ?= $(HOME)/gene11/
	FLUXCODE_LIBS += $(shell make -s -C $(GENEDIR) ABSBASEDIR=$(GENEDIR) \
	      BASEDIR=$(GENEDIR) liblinkline LIBN=libtringene)
	CPPFLAGS += -DUSE_GENE
endif

#
#
# Define RELEASE based on the contents of the RELEASE
# file, which is empty unless we are in a release.
RELEASE_FILE = $(shell cat RELEASE)
ifneq ($(RELEASE_FILE),) # If the file is not empty
	RELEASE=$(RELEASE_FILE)
	CPPFLAGS+=-DRELEASE=$(RELEASE_FILE)
	DOC_FOLDER=releases/$(RELEASE_FILE)/
endif

include Makefile.depend

trinity_mod+= trinity_c_functions.o

FORTFROMRUBY=$(subst generate_,,$(patsubst %.rb,%.f90,$(wildcard generate*.rb */generate*.rb)))

F90FROMFPP = $(patsubst %.fpp,%.f90,$(notdir $(wildcard *.fpp diagnostics/*.fpp flux_interfaces/*.fpp)))
F90FROMFPP += simpledataiof.f90 simpledataio_write.f90 simpledataio_read.f90

.SUFFIXES: .fpp .f90 .cu .c .o .F90 .rb

.rb.f90:
	ruby $^ $@
.f90.o:
	$(FC) $(F90FLAGS) -c $<
.fpp.f90:
	$(CPP) $(CPPFLAGS) $< $@
.F90.f90:
	$(CPP) $(CPPFLAGS) $< $@
.c.o:
	$(CC) $(CFLAGS) -c $<
.cu.o:
	nvcc $(NVCCFLAGS) ${NVCCLIBS} ${NVCCINCS} -c $<

%.o: %.mod

LIBS += $(FLUXCODE_LIBS) $(PWD)/libsimpledataio.a $(NETCDF_LIB) $(GEO_LIBS) $(MPI_LIB) 
F90FLAGS += $(F90OPTFLAGS) $(MPI_INC) $(FLUXCODE_INCS) $(NETCDF_INC) $(GEO_INC)
CFLAGS += $(COPTFLAGS) $(FLUXCODE_INCS) $(NETCDF_INC)

VPATH=.:flux_interfaces:diagnostics$(SIMPLEDATAIO_VPATH):import_export:

# Some template dependencies

nteqns_arrays.f90: templates/nteqns_arrays_template.f90

#MOD= text_options_trin.o command_line_trin.o constants_trin.o mp_trin.o \
	file_utils_trin.o linsolve.o trinity_input.o trin_power.o nteqns.o fluxes.o ifs_pppl.o \
	expt_profs.o interp.o chease_io.o read_chease.o unit_tests.o calibrate_type.o calibrate.o \
	fluxes_gs2.o fluxes_gryfx.o fluxes_shell_script.o fluxes_replay.o
#MODX= trinity.o trinity_prog.o $(TRINDIAG_MODULES)

#ifeq ($(USE_NEW_DIAG),on)
#diagnostics: $(MOD)
#
#endif

#all: disp

ifdef GRYFX
.DEFAULT_GOAL = trinity_cuda
else
.DEFAULT_GOAL = trinity
endif

DEPEND_CMD=$(PERL) scripts/fortdep

depend: $(FORTFROMRUBY) $(F90FROMFPP)
	@$(DEPEND_CMD) -m "$(MAKE)" -1 -o -v=1 $(VPATH)

#disp: $(MOD) $(MODX)
#	$(FC) $(LDFLAGS) -o trinity $(FLAGS) $(LIBS) $(MOD) $(MODX)
	#@echo $(GS2_LIB)
	#${FC} $(LDFLAGS) -o trinity $(FLAGS) $(MOD) $(MODX) $(LIBS) $(DYNAMIC_FLAG)

trinity: trinity_prog.o libtrinity.a
	${FC} $(LDFLAGS) -o trinity $(FLAGS) $^ $(LIBS) $(DYNAMIC_FLAG)


trinity_cuda: trinity_cuda.o libtrin.so $(GRYFX_LIB)
	nvcc $(LDFLAGS) -o trinity_cuda trinity_cuda.o -L. -ltrin $(FLAGS) $(LIBS) $(FORTRAN_LIBS)

#fluxes_gs2.o fluxes_gryfx.o: dummy_interfaces

libtrinity.a: trinity_c_interface.o $(patsubst trinity_prog.o,,$(trinity_mod)) 
	$(ARCH) $(ARCHFLAGS) $@ $^ 
	$(RANLIB) $@

libtrin.so: libtrinity.a trinity_c_interface.c $(GRYFX_LIB)
	$(CCLD) --shared -Wl,-keep_dwarf_unwind -o libtrin.so trinity_c_interface.c libtrinity.a libsimpledataio.a  $(LIBS) $(CCLDFLAGS) 


iodoc:
	$(MAKE) -C scripts/iodoc

doc: $(F90FROMFPP) $(FORTFROMRUBY)
	doxygen doxygen_config 
	rm -f $(F90FROMFPP)

sync_doc: 
	rsync -av --delete --exclude=releases doc/html/	${USER},gyrokinetics@web.sourceforge.net:htdocs/trinity_documentation/$(DOC_FOLDER)
	mkdir -p doc/html/releases
	echo "Options +Indexes" > doc/html/releases/.htaccess
	rsync -av doc/html/releases/ ${USER},gyrokinetics@web.sourceforge.net:htdocs/trinity_documentation/releases/


test_make: 
	echo "FLAGS is $(FLAGS)"
	echo "F90FLAGS is $(F90FLAGS)"
	echo "CPPFLAGS is $(CPPFLAGS)"
	echo "LDFLAGS is $(LDFLAGS)"
	echo "LIBS is $(LIBS)"
	echo "FORTRAN_LIBS is $(FORTRAN_LIBS)"
	echo "MOD is $(MOD)"
	echo "MODX is $(MODX)"

.PHONY: dummy_interfaces depend iodoc

dummy_interfaces/libgs2.a: dummy_interfaces/gs2_gryfx_zonal.f90 dummy_interfaces/gs2_main.fpp
	$(MAKETESTS) -C dummy_interfaces

dummy_interfaces/libgryfx.a: dummy_interfaces/gryfx_lib.c dummy_interfaces/gryfx_lib.h
	$(MAKETESTS) -C dummy_interfaces

dummy_interfaces:
	$(MAKETESTS) -C $@

clean_dummy_interfaces:
	$(MAKETESTS) -C dummy_interfaces clean

test_got:
	echo $(HAS_SIMP)
	echo $(SIMPLEDATAIO_LIBS)

ifdef STANDARD_SYSTEM_CONFIGURATION
system_config: Makefiles/Makefile.$(GK_SYSTEM) Makefile
	@echo "#!/bin/bash " > system_config
	@echo "$(STANDARD_SYSTEM_CONFIGURATION)" >> system_config
#	@sed -i 's/^\s//' system_config

else
.PHONY: system_config
system_config:
	$(error "STANDARD_SYSTEM_CONFIGURATION is not defined for this system")
endif

unlink:
	-rm -f $(F90FROMFPP)	

clean:
	rm -f *.o *.mod
	rm -f trinity

cleanlib:
	-rm -f *.a

cleanconfig:
	rm -f system_config .tmp_output

distclean: unlink clean cleanlib clean_tests cleanconfig clean_dummy_interfaces

#file_utils_trin.o: command_line_trin.o mp_trin.o
#mp_trin.o: constants_trin.o
#trin_power.o: interp.o trinity_input.o
#interp.o: mp_trin.o
#trinity_input.o: mp_trin.o file_utils_trin.o text_options_trin.o read_chease.o chease_io.o
#expt_profs.o: interp.o trinity_input.o file_utils_trin.o
#nteqns.o: constants_trin.o mp_trin.o file_utils_trin.o trinity_input.o \
									#trin_power.o expt_profs.o interp.o read_chease.o chease_io.o \
									unit_tests.o
#fluxes.o: mp_trin.o trinity_input.o interp.o nteqns.o ifs_pppl.o unit_tests.o dummy_interfaces/libgs2.a dummy_interfaces/libgryfx.a calibrate_type.o $(GS2_LIB_DEP) fluxes_gs2.o fluxes_gryfx.o fluxes_shell_script.o fluxes_replay.o
#fluxes.o: $(GS2_LIB_DEP) dummy_interfaces/libgryfx.a
fluxes_gs2.o: $(GS2_LIB_DEP)

fluxes_gryfx.o: dummy_interfaces/libgryfx.a
#trinity.o: mp_trin.o file_utils_trin.o trinity_input.o linsolve.o nteqns.o fluxes.o calibrate.o $(TRINDIAG_MODULES)
#functional_tests_trinity.o: trinity.o
#unit_tests.o: mp_trin.o
#trinity_prog.o: trinity.o
#calibrate.o: fluxes.o calibrate_type.o
#fluxes_gs2.o: trinity_input.o nteqns.o
#fluxes_gryfx.o: trinity_input.o nteqns.o
#fluxes_shell_script.o: trinity_input.o nteqns.o
#fluxes_replay.o: trinity_input.o nteqns.o trindiag_config.o



#trinity.o: trinity_c_functions.o

ifeq ($(FLUXCODE),gene)
$(GENEDIR)/bin/libtringene.a:
	@(cd $(GENEDIR) && make lib LIBN=libtringene && cd -)

trinity.o:		   $(GENEDIR)/bin/libtringene.a
endif


sinclude tests/Makefile.tests

