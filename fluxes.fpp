
module fluxes
  use fluxes_gryfx, only: fluxes_gryfx_type
  use fluxes_formulae, only: fluxes_formulae_type
  use fluxes_tglf, only: fluxes_tglf_type
  use flux_results, only: flux_results_type
  use fluxes_gryffin, only: fluxes_gryffin_type
  use fluxes_gene, only: fluxes_gene_type
  use fluxes_gs2, only: fluxes_gs2_type
  use trindiag_config, only: trindiag_type
  use fluxes_neo, only: fluxes_neo_type
  implicit none

  public :: init_fluxes, get_fluxes, finish_fluxes
  public :: fluxes_type, fluxes_converged

  ! Unit tests
  public :: fluxes_unit_test_get_fluxes
  
  private

  type fluxes_type
    logical :: initialized = .false.
    logical :: first = .true.
    logical :: neo_first = .true.
    logical :: can_check_convergence = .false.
    !> The results from the current call
    type(flux_results_type) :: flx
    !> The results from the previous flux call
    type(flux_results_type) :: flx_previous
    !> The results from the last iteration
    type(flux_results_type) :: flx_old
    type(fluxes_gryfx_type) :: gryfx
    type(fluxes_formulae_type) :: formulae
    type(fluxes_tglf_type) :: tglf
    type(fluxes_gene_type) :: gene
    type(fluxes_gryffin_type) :: gryffin
    type(fluxes_gs2_type) :: gs2
    type(fluxes_neo_type) :: neo
  end type fluxes_type




contains

  subroutine init_fluxes (fx)

    use mp_trin, only: scope, subprocs!, proc0
    use trinity_input, only: ncc, nspec, fork_flag, check_flux_converge
    use trinity_input, only: nrad_global, nrad, njac
    use interp, only: get_gridval
    use nteqns, only: broadcast_profiles
    use flux_results, only: init_flux_results

    implicit none

    type(fluxes_type), intent(inout) :: fx

    fx%neo_first = .true.
    fx%first = .true.
    fx%initialized = .true.
    fx%can_check_convergence = .false.

    call init_flux_results(flx=fx%flx,nspec=nspec, ncc=ncc, njac=njac, &
      nrad=nrad, nrad_global=nrad_global, fork_flag=fork_flag, &
      check_flux_converge=check_flux_converge)
    call init_flux_results(flx=fx%flx_old,nspec=nspec, ncc=ncc, njac=njac, &
      nrad=nrad, nrad_global=nrad_global, fork_flag=fork_flag, &
      check_flux_converge=check_flux_converge)
    call init_flux_results(flx=fx%flx_previous,nspec=nspec, ncc=ncc, njac=njac, &
      nrad=nrad, nrad_global=nrad_global, fork_flag=fork_flag, &
      check_flux_converge=check_flux_converge)

    ! if assigning set of independent processors to each flux calculation,
    ! change scope to subprocs, and broadcast profiles across subprocs processors
    if (fork_flag) then
       ! change mp scope to group processors
       call scope (subprocs)
       ! broadcast profiles within each group of processors
       call broadcast_profiles
    end if

  end subroutine init_fluxes

  subroutine get_fluxes (fx, clock, calib, model, gnostics)
 
    use fluxes_gs2, only: get_fluxes_gs2
    use fluxes_gryfx, only: get_fluxes_gryfx
    use fluxes_shell_script, only: get_fluxes_shell_script
    use fluxes_replay, only: get_fluxes_replay
    use fluxes_neo, only: get_fluxes_neo
    use fluxes_gryffin, only: get_fluxes_gryffin
    use fluxes_gene, only: get_fluxes_gene
    use fluxes_formulae, only: get_fluxes_formulae
    use fluxes_tglf, only: get_fluxes_tglf
    use fluxes_neo_analytic, only: get_fluxes_neo_analytic
    use fluxes_matched, only: get_fluxes_matched
    use trinity_type_module, only: trinity_clock_type
    use mp_trin, only: broadcast, subprocs, abort_mp
    use mp_trin, only: job_fork, scope, allprocs, barrier, proc0
    use constants_trin, only: pi, mp_over_me
    use trinity_input, only: flux_option_switch, flux_option_gs2, flux_option_gene
    use trinity_input, only: flux_option_ifspppl, flux_option_offlin, flux_option_gryffin
    use trinity_input, only: flux_option_quasilinear, flux_option_test1, flux_option_neo
    use trinity_input, only: flux_option_gryfx
    use trinity_input, only: flux_option_test2, flux_option_test3, flux_option_pb
    use trinity_input, only: flux_option_tglf, flux_option_test_calibrate
    use trinity_input, only: flux_option_replay, flux_option_matched
    use trinity_input, only: flux_option_shell_script
    use trinity_input, only: nifspppl_initial, turb_heat
    use trinity_input, only: include_neo, nspec, ncc, fork_flag, njac
    use trinity_input, only: single_radius, damping, neo_option
    use trinity_input, only: flux_match_switches
    use trinity_input, only: z_ion
    use interp, only: get_gridval
    use norms, only: amin_ref, aref, psitor_a
    use calibrate_type_module, only: calibrate_type
    use nteqns_arrays, only: drmindrho_ms
    use flux_results, only: gather_forked_fluxes
    use nteqns, only: init_geo
    use unit_tests_trin, only: verb_high, debug_message
    use file_utils_trin, only: error_unit

    implicit none
    type(trinity_clock_type), intent(inout), target :: clock
    integer, pointer :: itstep
    integer, pointer :: iter
    integer, pointer :: ncalls
    type(fluxes_type), intent(inout), target :: fx
    type(calibrate_type), intent(in) :: calib
    character(*), intent(in) :: model
    type(trindiag_type), intent(inout) :: gnostics

    type(flux_results_type), pointer :: flx

    integer :: nrad_per_group, is, igr
    integer :: flux_option_actual
    integer :: old_flux_option
    logical :: matchdens, matchtemp

    call debug_message(verb_high, &
      'fluxes::get_fluxes starting')

    ! Local pointers for convenience
    itstep => clock%itstep
    iter => clock%iter
    ncalls => clock%neval
    flx => fx%flx

    ! According to the Fortran 2008 standard 7.2.1.3 as summarised here:
    ! http://stackoverflow.com/questions/19111471/fortran-derived-type-assignment
    ! the line below should 'just work', i.e. we don't need to manually copy 
    ! the flux_results object
    if (.not. fx%first) then 
      fx%flx_previous = fx%flx
    end if
    
    if (model=="calculate") fx%can_check_convergence = .true.




    ! # of flux tubes per group of processors (subprocs)
    if (fork_flag) then
       nrad_per_group = 1
    else
       nrad_per_group = ncc
    end if

    if (nifspppl_initial.gt.0 .and. itstep .le. nifspppl_initial) then
      flux_option_actual = flux_option_ifspppl
    else
      !if (present(model) .and. present(calib)) then
      if (model .eq. "calibrate") then 
        flux_option_actual = calib%flux_option
      else
        flux_option_actual = flux_option_switch
      end if
      !end if
    end if

    ! obtain neoclassical contribution to fluxes
    if (include_neo) then
      select case (neo_option)
      case ('neo')
        call get_fluxes_neo(flx, fx%neo)
      case ('analytic')
        call get_fluxes_neo_analytic(flx)
      end select
    end if

    call debug_message(verb_high, &
      'fluxes::get_fluxes getting matched and damped fluxes')
    ! In this section we initialize the fluxes by setting them 
    ! equal to the flux which would be necessary to keep the profile
    ! gradients exactly fixed. This allows us, for example, to
    ! run gs2 at exactly one location.
    call get_fluxes_matched(flx, clock%neval)

    call debug_message(verb_high, &
      'fluxes::get_fluxes got matched and damped fluxes')

    ! We want the matched 'turbulent' flux, so we subtract the 
    ! neoclassical contribution to the matched fluxes
    flx%q_cc = flx%q_cc - flx%q_neo_cc 
    flx%p_cc = flx%p_cc - flx%p_neo_cc
    flx%l_cc = flx%l_cc - flx%l_neo_cc
    flx%q_matched = flx%q_cc
    flx%p_matched = flx%p_cc
    flx%l_matched = flx%l_cc

    !> Record which flux code was used.
    flx%flux_option_actual = flux_option_actual


    call debug_message(verb_high, &
      'fluxes::get_fluxes getting turbulent fluxes')
    
    select case (flux_option_actual)

    case (flux_option_matched)
      !call init_geo
      ! Do nothing
      if (fork_flag) call scope(allprocs)
    case (flux_option_shell_script)
       call get_fluxes_shell_script(&
               ncalls=ncalls, calib=calib, model=model,   &
               qflx_cc=flx%q_cc, pflx_cc=flx%p_cc, lflx_cc=flx%l_cc,     &
               heat_cc=flx%heat_cc,  dvdrho=flx%dvdrho, grho=flx%grho)
    case (flux_option_replay)

       call get_fluxes_replay(&
               itstep = itstep, iter=iter, &
               ncalls=gnostics%neval_this_run, calib=calib, model=model,   &
               qflx_cc=flx%q_cc, pflx_cc=flx%p_cc, lflx_cc=flx%l_cc,     &
               heat_cc=flx%heat_cc,  dvdrho=flx%dvdrho, grho=flx%grho, &
               old_flux_option=old_flux_option)
         if (fx%first) then
           call init_geo 
         end if
     case (flux_option_gs2)
       call get_fluxes_gs2(fx%flx, fx%gs2, ncalls, calib, model, gnostics)
       ! convert dvdrho and grho from r/a to sqrt(torflux) if torflux is the fluxlabel chosen
       flx%dvdrho = flx%dvdrho*drmindrho_ms ; flx%grho = flx%grho*aref/(amin_ref*drmindrho_ms)
       ! gather fluxes from group proc0s to global proc0
       call gather_forked_fluxes (flx, flx%dvdrho, flx%grho)
    case (flux_option_gryfx)
       call get_fluxes_gryfx(fx%flx, fx%flx_previous, fx%flx_old, fx%gryfx, clock)
       !flx%q_cc = flx%q_matched 
       if (fx%first) then
         call init_geo
       end if
    case (flux_option_gryffin)
      call get_fluxes_gryffin(fx%flx, fx%gryffin)
    case (flux_option_gene)
       call get_fluxes_gene(fx%flx, fx%gene)
       ! need to test! -- MAB
       if (abs(psitor_a) < epsilon(0.0)) then
          call gather_forked_fluxes(flx)
       else
          call gather_forked_fluxes (flx, flx%dvdrho, flx%grho)
       end if
    case (flux_option_tglf)
       call get_fluxes_tglf(fx%flx, fx%tglf)
    case (flux_option_neo)
       if (.not. include_neo) then
          write (*,*) "'neo' flux option chosen, but include_neo = F.  aborting run"
          stop
       end if
       if (fork_flag) then
          flx%p = 0. ; flx%q = 0. ; flx%heat = 0.
          call gather_forked_fluxes(flx)
       else
          flx%q_cc = 0.0 ; flx%l_cc = 0.0
       end if
       if (fx%first) then
          call init_geo
       end if
     case (flux_option_ifspppl, flux_option_pb, flux_option_test3, &
         flux_option_test1, flux_option_test2, flux_option_test_calibrate, &
         flux_option_offlin)

       call get_fluxes_formulae(&
         fx%flx, clock, calib, model, fx%formulae, flux_option_actual)

    end select

    ! Now we check flux matching options
    call debug_message(verb_high, 'fluxes::get_fluxes &
      & checking flux_match_switches ')
    do is = 1,nspec
      select case (flux_match_switches(is:is))
      case ('n')
        ! Do nothing
        matchdens = .false.
        matchtemp = .false.
      case ('d')
        matchdens=.true.
        matchtemp = .false.
      case ('t')
        matchdens = .false.
        matchtemp=.true.
      case ('a')
        matchdens = .true.
        matchtemp = .true.
      case default
        if (proc0) write (error_unit(),*) 'ERROR: unknown matching option', &
          flux_match_switches(is:is), ' for species ', is
        call abort_mp
      end select
      if (matchdens) then
        if (is.eq.1) then
          if (proc0) write (error_unit(),*) 'ERROR: you cannot flux match electron &
            & density.'
          call abort_mp
        end if
        flx%p_cc(:,1,:) = flx%p_cc(:,1,:) - flx%p_cc(:,is,:)*z_ion(is-1)
        flx%p_cc(:,is,:) = flx%p_matched(:,is,:) 
        flx%p_cc(:,1,:) = flx%p_cc(:,1,:) + flx%p_cc(:,is,:)*z_ion(is-1)
        !flx%p_cc(:,1,:) = sum(flx%p_cc(:,2:,:), 2)
      end if
      if (matchtemp) then
        flx%q_cc(:,is,:) = flx%q_matched(:,is,:)
      end if
    end do

    ! Flux interfaces should guarantee that the global proc0 has
    ! the correct results 
     call broadcast(flx%l_cc)
     call broadcast(flx%q_cc)
     call broadcast(flx%p_cc)
     call broadcast(flx%heat_cc)

    ! Store turbulent fluxes for convergence calc
    flx%q_turb_cc = flx%q_cc

    if (.not. fx%first) then
      !flx%q_relative_change = max( &
        !abs((flx%q_cc - fx%flx_old%q_turb_cc) / flx%q_cc), &
        !abs((flx%q_cc - fx%flx_old%q_turb_cc) / fx%flx_old%q_turb_cc) &
        !)
      flx%q_relative_change = &
        abs(flx%q_cc-fx%flx_previous%q_turb_cc) / &
        abs(flx%q_cc-fx%flx_old%q_turb_cc)
      ! Below we cater for the case where flx%q_cc >> flx_old%q_cc, and
      ! also flx%q_cc ~> flx_previous%q_cc
      where (abs(fx%flx_previous%q_turb_cc-fx%flx_old%q_turb_cc).gt.epsilon(0.0))&
        flx%q_relative_change = &
          max( &
          flx%q_relative_change,&
          abs(flx%q_cc-fx%flx_previous%q_turb_cc) / &
          abs(fx%flx_previous%q_turb_cc-fx%flx_old%q_turb_cc) &
          )
      ! These last two conditions are an insurance policy... they
      ! just check the result has stopped changing
      where (abs(fx%flx%q_turb_cc).gt.epsilon(0.0))&
        flx%q_relative_change = &
          max( &
          flx%q_relative_change,&
          abs(flx%q_cc-fx%flx_previous%q_turb_cc) / &
          abs(fx%flx%q_turb_cc) &
          )
      where (abs(fx%flx_previous%q_turb_cc).gt.epsilon(0.0))&
        flx%q_relative_change = &
          max( &
          flx%q_relative_change,&
          abs(flx%q_cc-fx%flx_previous%q_turb_cc) / &
          abs(fx%flx_previous%q_turb_cc) &
          )

        
      where (flx%q_cc - fx%flx_old%q_turb_cc .eq. 0.0) &
          flx%q_relative_change = 0.0
      ! If the heat flux is basically zero we require at least a factor
      ! of e^2 change to trigger a further flux call
      where (abs(flx%q_cc) .lt. 1e-7 .and. abs(fx%flx_old%q_turb_cc) .lt. 1e-7 &
          .and. log(abs(flx%q_cc)/abs(fx%flx_previous%q_turb_cc)).lt.2.0)&
          flx%q_relative_change = 0.0
      if (single_radius .gt. 0) then
        if(single_radius .gt. 1) &
          flx%q_relative_change(1:single_radius-1, :, :) = 0.0
        if(single_radius .lt. ncc) &
          flx%q_relative_change(single_radius+1:ncc, :, :) = 0.0
      end if
    end if


    ! total flux is turbulent + neoclassical
    ! The saved fluxes include neoclassical so don't add them
    ! again for replay
    if (.not. flux_option_actual == flux_option_replay) then
      
      flx%p_cc = flx%p_cc + flx%p_neo_cc
      flx%q_cc = flx%q_cc + flx%q_neo_cc
      flx%l_cc = flx%l_cc + flx%l_neo_cc
    end if

    !if (damping .gt. 0.0) then
      !flx%p_cc = flx%p_cc + flx%p_damp*damping
      !flx%q_cc = flx%q_cc + flx%q_damp*damping
      !flx%l_cc = flx%l_cc + flx%l_damp*damping
    !end if



    ! Update counter
    fx%flx%conv_count = fx%flx%conv_count + 1

    call write_flux_diagnostics(fx%flx, gnostics, flux_option_actual)

    ! set turbulent heating to zero if turb_heat is .false.
    if (.not. turb_heat) flx%heat_cc = 0.0

    ! interpolates from cell-centers to grid points
    do is = 1, nspec
       call get_gridval (flx%heat_cc(:,is,1), flx%heat_grid(:,is))
    end do

    ! converts from normalized to physical units for fluxes
    call get_physical_fluxes (flx%p_cc(:,:,1), flx%q_cc(:,:,1), flx%p_phys, flx%q_phys)

    fx%first = .false.

  end subroutine get_fluxes

  function fluxes_converged(fx)
    use unit_tests_trin, only: debug_message, verb_iteration
    use trinity_input, only: flux_option_gs2, flux_option_gryfx
    use trinity_input, only: flux_option_gene, flux_option_gryffin
    use trinity_input, only: flux_convergetol
    use mp_trin, only: broadcast
    type(fluxes_type), intent(inout), target :: fx
    logical :: fluxes_converged

    fluxes_converged = .true.
    fx%flx%q_converged = .true.
    if (.not. fx%can_check_convergence) then
      fx%flx%q_converged = .false.
    else if (flux_convergetol > 0.0) then 
      ! NB Simple flux models like ifspppl don't need a convergence check.
      select case(fx%flx%flux_option_actual)
      case (flux_option_gryfx) !, flux_option_gs2, flux_option_gene, &
          !flux_option_gryffin)
        where(&
          max(fx%flx%q_relative_change,fx%flx_previous%q_relative_change)&
          .gt. flux_convergetol &
          ) &
          fx%flx%q_converged = .false.
        if (fx%flx%conv_count .lt. 3) then
          where(abs(fx%flx%q_turb_cc) .gt. 1e-7)&
            fx%flx%q_converged = .false.
        end if
      end select
    end if

    if (.not. all(fx%flx%q_converged)) fluxes_converged = .false.

    if (fluxes_converged) then
      ! Now we've converged, need to check fluxes at least 
      ! once before we can recheck convergence.
      fx%can_check_convergence = .false.
      ! Set the old fluxes to be the current fluxes
      fx%flx_old = fx%flx
      fx%flx%conv_count = 0
      call debug_message(verb_iteration, 'fluxes::fluxes_converged is .true.')
    else
      call debug_message(verb_iteration, 'fluxes::fluxes_converged is .false.')
    end if
  end function fluxes_converged

  subroutine write_flux_diagnostics(flx, gnostics, flux_option_actual)
    use trindiag_fluxes_each_eval, only: trindiag_advance_eval
    use trindiag_fluxes_each_eval, only: write_fluxes_each_eval
    use trindiag_fluxes_each_eval, only: write_neo_fluxes_each_eval
    use trindiag_fluxes_each_eval, only: write_geo_each_eval
    use trindiag_fluxes_each_eval, only: write_flux_option_each_eval
    use trindiag_fluxes_each_eval, only: trindiag_set_conv
    use simpledataio, only: syncfile
    type(trindiag_type), intent(inout) :: gnostics
    type(flux_results_type), intent(inout) :: flx
    integer, intent(inout) :: flux_option_actual
    call trindiag_set_conv(gnostics, flx%conv_count)
    call write_fluxes_each_eval(gnostics, flx)
    call write_neo_fluxes_each_eval(gnostics, flx)
    call write_geo_each_eval(gnostics)
    call write_flux_option_each_eval(gnostics, flux_option_actual)
    call trindiag_advance_eval(gnostics)
    gnostics%neval_this_run = gnostics%neval_this_run + 1
    if (gnostics%wryte .or. gnostics%reed) call syncfile(gnostics%sfile)
  end subroutine write_flux_diagnostics

  function fluxes_unit_test_get_fluxes(fx, run_test, qflux_cc_results, gnostics, clock, eps)
    use unit_tests_trin
    use mp_trin,only: proc0
    use calibrate_type_module, only: calibrate_type
    use trindiag_config, only: trindiag_type
    use trinity_type_module, only: trinity_clock_type
    !#use nteqns, only: qval_cc
    logical, intent(in) :: run_test
    real, intent(inout), dimension(:,:) ::  qflux_cc_results
    real, intent(in) :: eps
    logical :: fluxes_unit_test_get_fluxes
    integer :: i
    character(len=18) :: message
    type(fluxes_type), intent(inout) :: fx
    type(calibrate_type) :: dummy_calib
    type(trinity_clock_type), intent(inout) :: clock
    type(trindiag_type), intent(inout) :: gnostics
    fluxes_unit_test_get_fluxes = .true.
    call get_fluxes(fx, clock, dummy_calib, "calculate", gnostics)
    if (proc0) write (*,*) fx%flx%q_cc, 'qflx_cc'
    if (run_test .and. proc0) then 
      do i = 1,size(fx%flx%q_cc, 2)


        write(message,'("heat flux spec ",I1)') i
        call announce_check(message)
        call process_check(fluxes_unit_test_get_fluxes, &
          agrees_with(fx%flx%q_cc(:,i,1), qflux_cc_results(:,i), eps), &
          message)
      end do
    end if

  end function fluxes_unit_test_get_fluxes


  subroutine get_physical_fluxes (pflx_norm, qflx_norm, pflx_units, qflx_units)

    use trinity_input, only: m_ion
    use trinity_input, only: vtfac, nspec, is_ref
    use nteqns_arrays, only: dens_cc, temp_cc, bmag_cc
    use norms, only: rhostar

    implicit none

    real, dimension (:,:), intent (in) :: pflx_norm, qflx_norm
    real, dimension (:,:), intent (out) :: pflx_units, qflx_units

    ! gets heat flux in units of MW / m^2
!    qflx_units = qflx_norm * 5.17e-2 * vtfac**1.5 * sqrt(m_ion(1)) / aref &
!         * spread(dens_cc(:,1)*temp_cc(:,2)**2.5/bmag_cc**2,2,nspec)

    ! gets particle flux in units of 10^20/m^2/s
    pflx_units = pflx_norm * 3.0948e+5 * sqrt(vtfac/m_ion(1)) * rhostar**2 &
         * spread(dens_cc(:,is_ref)*temp_cc(:,is_ref)**1.5/bmag_cc**2,2,nspec)

    ! gets heat flux in units of MW / m^2
    qflx_units = qflx_norm * 4.95787e+3 * sqrt(vtfac/m_ion(1)) * rhostar**2 &
         * spread(dens_cc(:,is_ref)*temp_cc(:,is_ref)**2.5/bmag_cc**2,2,nspec)
    
  end subroutine get_physical_fluxes

  subroutine finish_fluxes(fx)

    use trinity_input, only: fork_flag, check_flux_converge
    use fluxes_gs2, only: finish_fluxes_gs2
    use fluxes_gryfx, only: finish_fluxes_gryfx
    use flux_results, only: finish_flux_results

    use trinity_input, only: fork_flag, check_flux_converge
    
    implicit none
    type(fluxes_type), intent(inout) :: fx

    call finish_flux_results(fx%flx, fork_flag, check_flux_converge)
    call finish_flux_results(fx%flx_previous, fork_flag, check_flux_converge)
    call finish_flux_results(fx%flx_old, fork_flag, check_flux_converge)
    

    if (fx%gs2%initialized) call finish_fluxes_gs2(fx%gs2)
    if (fx%gryfx%initialized) call finish_fluxes_gryfx(fx%gryfx)

    fx%first = .true.
    fx%neo_first = .true.
    fx%initialized = .false.
    fx%can_check_convergence = .false.
    
  end subroutine finish_fluxes



!   subroutine construct_geneprofs_from_grads (prof_grid, grad, prof_cc)

!     implicit none

!     real, dimension (:), intent (in) :: prof_grid
!     real, dimension (:,:), intent (in) :: grad
!     real, dimension (:,:), intent (out) :: prof_cc

!     integer :: nr

!     nr = size(prof)

!     prof(nr) = dens
!     do ix = nr, 1, -1
!        prof(ix) = grad
!     end do

!   end subroutine construct_geneprofs_from_grads

end module fluxes
