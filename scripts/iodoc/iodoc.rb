
require 'numru/netcdf'

ncfile = '../../tests/functional_tests/gs2_iterdb/gs2_iterdb_input.out.nc'
if false and not FileTest.exist?(ncfile)
  puts "Cannot find #{ncfile}. Please run the gs2_iterdb functional test"
  exit 1
end
file = NumRu::NetCDF.open('../../tests/functional_tests/gs2_iterdb/gs2_iterdb_input.out.nc')

class TableGenerator < Array
  def initialize(name)
    @table_name = name
    super()
  end
  def latex
    map{|props| props2 = props.dup;
        props2[0] = "\\texttt{#{props2[0]}}"
        props2[1] = props2[1].split(/,/).reverse.join(',')
        props2[4].sub!(/\(inout\)/, '')
        props2.join(" & \n")}.join("\\\\\n\\hline\n").gsub(
      /([_])/){"\\#$1"}.gsub(
      /\^/){"\\^{}"}
  end
end

class TableManager
  def initialize
    @tables = {}
  end
  def new_table(name)
    @tables[name] = TableGenerator.new(name)
  end
  def add_props(props)
    case props[4]
    when /\(inout\)/
      @tables[:primary_input_output].push props
    else
      case props[0]
      when /_perturb$/
        @tables[:perturb].push props
      #when /(tprim|fprim|gexb|pprim|pres|mom|nu)_grid/
      when /_grid$/
        @tables[:derived_grids].push props
      when /bmag_old/
        @tables[:others].push props
      when /^t$/, /^rad$/, /axis/, /ref/,  /m_ion/,
        /fluxlabeldef/, /psi(pol|tor)_a/, /vtfac/, /aspr/
        @tables[:primary_input_output].push props
      when /eval/
        @tables[:fluxevals].push props
      when /_save/, /rmserr/
        @tables[:restart].push props
      when /cc/
        @tables[:cc].push props
      when /gs2/
        @tables[:gs2].push props
      when /_core/, /_edge/, /power/, /fusion/
        @tables[:globals].push props
      else
        @tables[:others].push props
      end
    end
  end
  def generate_latex
    @tables.each do |name,tgen|
      File.open(name.to_s + '.tex', 'w') do |f|
        f.puts tgen.latex
      end
    end
  end
end

manager = TableManager.new
manager.new_table(:primary_input_output)
manager.new_table(:derived_grids)
manager.new_table(:globals)
manager.new_table(:gs2)
manager.new_table(:fluxevals)
manager.new_table(:restart)
manager.new_table(:others)
manager.new_table(:cc)
manager.new_table(:perturb)

file.vars.each{|var|
  p (props = [var.name, var.dims.map{|d| d.name}.join(','), var.ntype.sub(/^float/, 'double'), var.att('units').get, var.att('description').get])
  #STDIN.gets
  manager.add_props(props)
}
manager.generate_latex

# We now proceed to the part of the file which requires CodeRunner

begin
  require 'coderunner'
  CodeRunner.setup_run_class('trinity')
rescue=>LoadError
  puts "\nCannot update namelist tables: CodeRunner not installed\n"
  sleep 0.3
  exit
end

str = ""

CodeRunner::Trinity.rcp.namelists.each do |namelist, namelist_hash|
  str << " \\namelisttable{#{namelist}namelist}{ Input parameters in the namelist: #{namelist}. #{namelist_hash[:help]}}\n"
  File.open("#{namelist}namelist.tex", 'w') do |f|
    namelist_hash[:variables].each do |var, varhash|
      f.puts "#{var} & #{varhash[:type]} & #{varhash[:help]}\\\\\\hline".gsub(
      /([_#])/){"\\#$1"}.gsub(
      /([><])/){"$#$1$"}.gsub(
      /\^/){"\\^{}"}
    end
  end
end
File.open('namelist_includes.tex', 'w'){|f| f.puts str}










