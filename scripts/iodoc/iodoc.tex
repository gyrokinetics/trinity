%        file: iodoc_template.tex
%     Created: Sat Oct 31 03:00 pm 2015 G
% Last Change: Sat Oct 31 03:00 pm 2015 G
%
\documentclass[a4paper]{article}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[english]{babel}
\usepackage{xspace}
\usepackage{array}
\usepackage{booktabs}
\usepackage{supertabular}
\usepackage{longtable}
\usepackage[originalparameters]{ragged2e}
\usepackage[scale=0.8]{geometry}
\newcommand{\trinity}{\textsc{Trinity}\xspace}
\newcommand{\millerr}[0]{r}
\newcommand{\majorrout}[0]{R_{\mathrm{out}}}
\newcommand{\majorrin}[0]{R_{\mathrm{in}}}
\newcommand{\majorrgeo}[0]{R_{\mathrm{geo}}}
\newcommand{\majorrcentre}[1]{\ensuremath{R_{\mathrm{centre}\, {#1}}}}
\newcommand{\fortran}{\textsc{Fortran}\xspace}

\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\newcommand{\aref}{a_0}

%\newcommand{\variablecolumns}{L{3.5cm}C{2cm}C{2cm}L{8cm}}

\newcommand{\namelisttable}[2]{
\begin{table}[hbp]
  \small
\makebox[\textwidth]{
\renewcommand{\arraystretch}{1.3}
  \centering
  %\begin{tabular}{L{3.5cm}C{2.5cm}C{2.5cm}L{8cm}}
  \begin{tabular}{ccL{11cm}}
\toprule
Variable name & Type & Description\\
\midrule
\input{#1.tex}
\\
\bottomrule
  \end{tabular}
}
  \caption{#2}
  \label{tab:#1}
\end{table}
}

\newcommand{\variabletable}[1]{
\makebox[\textwidth]{
\renewcommand{\arraystretch}{1.3}
  \centering
  %\begin{tabular}{L{3.5cm}C{2.5cm}C{2.5cm}L{8cm}}
  \begin{tabular}{ccccL{8cm}}
\toprule
Variable name & Dimensions & Type & Units & Description\\
\midrule
\input{#1}
\\
\bottomrule
  \end{tabular}
}
}

\begin{document}
\title{\trinity File Format and Input Parameter Specification}
\author{
Edmund Highcock\footnote{corresponding author}\\
\texttt{edmundhighcock@users.sourceforge.net}
\\
\\
Michael Barnes\\
}

\date{\today---Version 1.3(b)}


\maketitle
\begin{abstract}
  This document provides the full specification for the \trinity file format, 
  a portable, flat, netCDF file format that can be used as both the input and
  output for transport codes written for magnetically confined fusion devices.
  It includes a complete description of normalisations and dimensions used within
  the file format.
  It also provides a full description of the \trinity \fortran namelist control file.
\end{abstract}
\tableofcontents
\clearpage
\listoftables
\clearpage

\section{About this document}

\subsection{Introduction}

A transport code like \trinity requires certain data about the 
equilibrium---for example, profiles of heat and particle sources 
and information about geometry (which can come in various forms), 
as well as initial profiles of temperature and density etc.---in order to run. 
During, and at the end of the run, it outputs effectively the same quantities,
and so a single file format can serve as both input and output, although
it should be mentioned that as well as using its own format as input,
\trinity can also use the ITER profile database format\footnote{
\texttt{http://tokamak-profiledb.ccfe.ac.uk/}},
the output of the \texttt{OneTwo} code, and the output of the \texttt{CHEASE} 
and \texttt{ECOM} codes.

This document gives a complete description of:
\begin{itemize}
  \item the \trinity netCDF file format, which contains, among other things,
    geometry, profiles, fluxes and global quantities, and which is both
    output by \trinity and can be used as input by \trinity;
  \item the \trinity input file, which is a \fortran namelist file which
    contains the control parameters for a \trinity simulation.
\end{itemize}
It also includes a description of \trinity coordinates and normalisations. 
A list of known tools for converting to/from the \trinity netCDF format
is given in Section \ref{sec:conversions}.

\subsection{Licence and Purpose}
This guide is intended to serve as a manual for understanding \trinity
input and output.
However, given the absence of a lightweight, portable, well documented,
widespread
file format for distributing tokamak equilibrium information, it also
serves as the specification for such a file format, and may be freely
distributed under the Creative Commons License\footnote{
Specifically a Creative Commons Attribution and ShareAlike License, which means that
you are free to share and modify this work, so long as you give 
credit to previous authors and release your modifications under
the same license.
}, in the hope
that it may be useful to others facing problems as to how to share
data between codes.
If you would like to get involved with this specification,
or suggest extensions or improvements, please get 
in touch with the authors.

\subsection{Versions}

Version information for this document is as follows:

\begin{itemize}
  \item Specification Version: 1.3
  \item Document Version: 1.3(b)
  \item \trinity Version: 0.1.0
\end{itemize}
The specification version of the \trinity netCDF file
is stored in the \texttt{file\_specification} global 
attribute of the file.

\subsection{Change Log}

\subsubsection{Changes in Specification Version 1.3}

\begin{itemize}
  \item Added \texttt{rhopol\_grid} and \texttt{drhopoldrho\_grid}.
  \item Changed the order that the dimensions are displayed. Dimensions are now displayed as they appear in the netCDF file. See Section \ref{sec:dimensionorder}.
  \item Added details of conversion to/from other file formats.
\end{itemize}

\subsubsection{Changes in Specification Version 1.2}

\begin{itemize}
  \item Added \texttt{aspr}
  \item Changed \texttt{src\_part\_dndr} (a typo) to  \texttt{src\_part\_dndt}
  \item Changed all variables named \texttt{src\_torque\dots} to \texttt{src\_mom\dots}
  \item Corrected the units of \texttt{area\_grid}
  \item Added \texttt{Rofpsitheta}, \texttt{Zofpsitheta} and \texttt{psiofRZ}, used for storing equilibrium data where available
\end{itemize}

\subsection{Conventions}
\subsubsection{Subscripts}

In this document the subscript $_a$ refers to the value of that
quantity at the last closed flux surface. 

\subsubsection{Cell centres and cell boundaries}
\label{sec:cellcb}

In a finite volume code, one typically thinks of fluxes
being specified at cell boundaries, with the mean value of the
evolved quantity (e.g. temperature) being specified at the cell
centre.
However, for various reasons, partially historic, \trinity cells
are the inverse of this. Thus, fluxes are calculated at cell centres
and quantities such as temperature evolved at cell boundaries\footnote{
This make sense if you are thinking of each cell of your calculation
comprising a local flux tube, in which the flux code calculates the fluxes at the 
centre, given a gradient between two temperature points at the cell edges.}.

\subsubsection{$I$, the poloidal current function}
\label{sec:Idef}

The poloidal current flux function $I$ is a function such that
\begin{equation}
  B_t=\frac{I}{R}
  \label{eqn:Idef}
\end{equation}
where $B_t$ is the toroidal magnetic field on the given flux surface
at the points on the flux surface where the major radius (i.e. distance to the central axis) is equal to $R$.
In various papers this flux function is given various symbols, most commonly $f$,
 but also $I$ and $T$ (for example in the CHEASE paper). Here we call it
 $I$, following, among others, Abel et. al. (2013). In the code this variable
 is stored in the array \texttt{btori\_grid}.






\subsection{Coordinates}

Both \trinity input and \trinity output coordinates are given on a one-
dimensional flux surface grid\footnote{
Although \trinity can evolve the Grad-Shafranov equation,
it does not currently possess the ability to iteratively remap
diagnostic quantities on a real space grid onto an updated equilibrium 
solution.}.
There are many confusions that arise from the definition of radial and
flux surface coordinates and so we attempt to clearly define them all
here so that the variable descriptions below are unambiguous.


\subsubsection{The distance to the magnetic axis $r_\circ$}

This is exactly as it says, the distance to the magnetic axis (see Section \ref{toroidalcoordinatesystem}). It is equal to $r$ if and only if the flux surfaces are concentric circles.

\subsubsection{$\majorrout$ and $\majorrin$}

These are distances to the central axis from the places where a given flux surface cuts 
the horizontal plane at the height of the magnetic axis, on the outside and inside of the magnetic axis respectively. 
In an axisymmetric system they are flux functions.

\subsubsection{The half diameter in the plane of the magnetic axis, $r$}

\label{halfdiametersection}

This parameter is defined as $(\majorrout-\majorrin)/2.0$. In Miller 
geometry this definition is coincident with the Miller parameter of 
the same name, as can easily be seen from the Miller parameterisation of
$R$:

\begin{equation}
  R = R_0 + r\mathrm{cos}\left[ \theta +  \mathrm{sin}^{-1}\left( \delta \right)\mathrm{sin}\left( \theta \right)  \right]
  \label{millermajorr}
\end{equation}
where $\delta$ is the triangularity (remember that $\majorrout=R\left( \theta=0 \right)$ and $\majorrin=R\left( \theta=\pi \right)$).

\subsubsection{The variable \texttt{rmin}}

This is equal to $r/r_a$, regardless of the definition of $\rho$. 
Note that is not equal to $r/\aref$, where $\aref$ is the normalising length
which may or may not be equal to $r_a$.

\subsubsection{$\majorrcentre{}$ }

This is defined as 
\begin{equation}
  \majorrcentre{}\left( \rho \right) =\frac{ \majorrout + \majorrin}{2}.
  \label{rmajdef}
\end{equation}

\subsubsection{The variable \texttt{rmajor}}

This is equal to $\majorrcentre{}/\aref$. Note that this is a flux function:
\texttt{rmajor} is not equal to $R$, the distance to the central axis,
and often referred to as the major radius.

\subsubsection{The poloidal flux $\psi_p$}

This is the integral of the magnetic field perpendicular to a plane of constant $\theta$ which extends from the magnetic axis to the flux surface which it labels. As by definition, a flux surface encloses a fixed flux, its value is independent of the choice of $\theta$.
\begin{equation}
  \psi_p = \int_{}^{}d\phi \int_{0}^{r_\circ\left( \theta,\rho \right)} B_p R\left( \theta,\rho \right) dr_\circ'
  \label{psipdef}
\end{equation}
where $B_p = \mathbf{B}\cdot\nabla\theta/\left| \nabla\theta \right| = r_{\circ}\mathbf{B}\cdot\nabla\theta $.
Now since $\rho$ is a flux label this definition feels a little circular. 
We seem to need the function form of $r_\circ\left( \theta,\rho \right)$ in order to calculate $\psi_p$. 
One label for flux surfaces which needs no a priori knowledge of the magnetic equilibrium,
except for the location of the magnetic axis, is $\majorrout$ (or alternatively, $r=\left( \majorrout - \majorrin \right)/2)$.
Since we know that $\psi_p$ is a flux function and hence independent of $\theta$, we can set $\theta=0$, in which case the integral is across the plane of the magnetic axis:
\begin{equation}
  \psi_p = \int_{}^{}d\phi \int_{0}^{(\majorrout - R_{\mathrm{axis}})} B_p \left( R_{\mathrm{axis}} + r_\circ' \right) dr_\circ'
  \label{psipdef}
\end{equation}
Thus, knowing $R_{\mathrm{axis}}$ (and $Z_{\mathrm{axis}}$) we can define $\majorrout$, and knowing $\mathbf{B}$ (which we assume throughout these notes is always known), we can now calculate $\psi_p\left( \majorrout \right)$ with no other information required.

Note that this definition of $\psi_p$ is 0 on axis. 

\subsubsection{The toroidal flux $\psi_t$, variable name \texttt{torflux}}

This is the integral of the magnetic field perpendicular to a plane of constant $\phi$ which extends from the magnetic axis to the flux surface which it labels.

\begin{equation}
  \psi_t\left( \psi_p \right) = \int_{}^{}d\theta \int_{0}^{r_\circ\left( \theta,\psi_p \right)} dr_\circ' r_\circ' B_t 
  \label{psitdefgeo}
\end{equation}
where $B_t=R\mathbf{B}\cdot\nabla\phi$. For practical purposes, the value of $\psi_t$
can be calculated  using the definition of $q$, the safety factor:
\begin{equation}
  q\left( \psi_p \right) = \frac{d \psi_t}{d \psi_p}, 
  \label{qdef2}
\end{equation} 
which means that:
\begin{equation}
  \psi_t\left( \psi_p \right) = \int_{0}^{\psi_p} d\psi_t = \int_{0}^{\psi_p}qd\psi_p.
  \label{calculatingpsit}
\end{equation}


\subsubsection{$\rho$ (variable name: \texttt{rho})} 
\label{rhosec}

This is the flux label used inside \trinity. The definition of $\rho$ affects the meaning of some, but not all, quantities which involve derivatives w.r.t. the flux label, for example, the magnetic shear. The definition of $\rho$ is set by the choice of \texttt{fluxlabel\_option}, but regardless of that choice, it is a flux function which is 0 on axis and 1 at the last closed flux surface. 
\begin{equation}
  \rho= 
  \begin{cases} 
    \sqrt{\frac{\psi_t}{\psi_{ta}}} &\mbox{if } \mathtt{fluxlabel\_option} = torflux \\ 
    \frac{\millerr}{r_a} &\mbox{if } \mathtt{fluxlabel\_option} = aminor \\ 
  \end{cases}
  \label{rpdef}
\end{equation}

\subsection{Normalisations}

\trinity quantities are effectively in SI units (with some factors 
applied), except for certain coordinates and geometric quantities.
The normalisation for every quantity in the \trinity netCDF file
is given below next to its description. Here we describe the normalising
quantities that are used within \trinity.


\subsubsection{The normalising length}
\label{sec:normalisinglength}

The definition of the normalising length $\aref$, variable name \texttt{aref},
depends on the flux label definition (see Section \ref{rhosec}). 

\begin{equation}
  \aref= 
  \begin{cases} 
    \sqrt{\frac{\psi_{ta}}{I_a/ \majorrcentre{a}}} &\mbox{if } \mathtt{fluxlabel\_option} = torflux \\ 
    r_a &\mbox{if } \mathtt{fluxlabel\_option} = aminor \\ 
  \end{cases}
  \label{rpdef}
\end{equation}
where $r_a$ is the halfwidth of the last closed flux surface (see
Section \ref{halfdiametersection}).
Note that the variable \texttt{aref} should not be confused with the
variable \texttt{amin\_ref}, which is \textit{always} equal to $r_a$.

\subsubsection{Other normalising quantities}

Table \ref{othernorms} defines other normalising quantities defined
in the variable lists below.


\section{The \trinity netCDF file format}

\subsection{General Remarks}

The \trinity netCDF format is a very simple flat netCDF file
with no groups or custom datatypes. In essence it is a classic/netCDF3
file, but \trinity always outputs a netCDF4 file because of the need
for multiple infinite dimensions (one for time steps, and one for 
flux evaluations).
However, an input file for \trinity only needs one infinite dimension,
\texttt{t}, and so can be in netCDF3 format. 

\subsection{Conversion to/from other file formats}

\label{sec:conversions}

In this section we detail known ways of converting the \trinity netCDF
file format to/from other formats.

\subsubsection{Files that can be read by \trinity}
Any file format that is read by \trinity can be converted easily to 
the \trinity file format. Right now this is done as a hack by running
\trinity for one timestep, but a dedicated tool is under construction. 
These file formats include:

\begin{itemize}
  \item The ITPA profile database format (ASCII u-files).
    See \url{http://tokamak-profiledb.ccfe.ac.uk/}.
  \item Outputs from the CHEASE and ECOM Grad-Shafranov solvers.
  \item Any format that can be converted using \texttt{profiles\_gen}
    and then read by the EXPRO library from General Atomics, 
    including:
    \begin{itemize}
      \item ITERDB text file (*) (this is the output from the \textsc{OneTwo} transport code).
    \item ITERDB NetCDF file (*.nc)
    \item Plasma State NetCDF file (*.cdf)
    \item \textsc{Astra} text file (*.astra)
    \item \textsc{Corsica} text file (*.corsica)
    \item \textsc{Elite}/PEQDSK (pfile) text file (*.peq)
    \end{itemize}
    See \url{https://fusion.gat.com/theory/Profiles_gen} for more details.
\end{itemize}

\subsubsection{Python tool for conversion to/from \textsc{Jetto} and \textsc{Cronos} ppfs}.

A tool has been developed by Gabor Szepesi which can read ppf data (that is, data stored
within the JET analysis cluster database) from the output
of \textsc{Jetto} and \textsc{Cronos} simulations and write to the \trinity netCDF file format.

As of March 2016, this tool supports conversion from \textsc{Jetto} to \trinity netCDF only, but
work is in progress.

\subsubsection{Support Requests}

If you would like help converting to/from an unsupported file format please file a support 
request at: \url{http://sourceforge.net/p/gyrokinetics/support-requests/} (you will need 
a sourceforge user account).

\subsection{Dimensions}

\label{sec:dimensionorder}

The dimensions for each variable are listed in the tables in the 
following sections. 
The dimensions themselves are defined in Table \ref{tab:dimensions}.
It is important to note the order of the dimensions.
Because \trinity is a \fortran code,
dimensions within \trinity are listed in column-major order, i.e.
the most slowly varying dimension in on the right.
However, because netCDF originated as a C library, dimensions in
a netCDF file are listed in row-major order, and this 
document and the \trinity netCDF file format follow that convention: \textbf{\textit{in
both this document and in the \trinity netCDF format the
most slowly varying dimension is on the left.}}\footnote{
When writing netCDF files in this way, no transform is needed between the data in memory 
in a \trinity simulation and the data on disk.}
If you need to reverse the order of the dimensions for any reason, this can easily be done
using the \textsc{ncpdq} tool, available at \url{http://nco.sourceforge.net/nco.html#ncpdq}.

\subsection{Variables}

\subsubsection{Primary input and output variables}

Table \ref{tab:primaryvariables} shows a set of quantities as a function of time, iteration and 
$\rho$, the flux label. Quantities are given on \trinity cell boundaries
(i.e cell centres in finite volume terms, see Section \ref{sec:cellcb}), at $\rho$ values given by 
\texttt{rad\_grid}.
\textbf{\textit{These are the variables that must be in the netCDF file if it is being
used an input}}.

\subsubsection{Global output quantities}

Table \ref{tab:globalvariables} shows list of global output quantities.

\subsubsection{Quantities at cell centres}
Table \ref{tab:cc} shows quantities at cell centres. These are 
typically values passed to or calculated by the flux codes.
The most important cell-centered quantities are the fluxes output
by the codes.

\subsubsection{Quantities needed for restarting \trinity}

These quantities in Table \ref{tab:restartvariables} are updated at each iteration of a \trinity run.
When \trinity is restarted, the output file from a previous run
can be supplied and \trinity will restart at the precise point the
previous run finished, thus using no additional flux evaluations.
\subsubsection{The results of each flux evaluation (needed for replay)}
As a result of the timestep and convergence criteria within
the \trinity algorithm, there is not a one to one 
correspondence between timesteps and iterations on the one hand
and evaluations of the flux on the other. Table \ref{tab:fluxevals} 
contains variables which record the results of each flux evaluation.
This information is used when using the replay feature of \trinity.
The replay feature is a way of re-running an identical simulation
without invoking the (possibly expensive) flux evaluation. The primary
purpose of this feature is allowing you to output additional diagnostic 
information from a simulation (e.g. for debugging).
\subsubsection{\textsc{Gs2} inputs and outputs}

As \textsc{Gs2} is an expensive code, we record the input and output
parameters for each call for posterity. These variables, listed in Table
\ref{tab:gs2}, only present if \textsc{Gs2} is being used as the flux code.

\section{Parameters in the \trinity control file.}

The course of a \trinity simulation is determined by the parameters in an ascii
control composed of \fortran namelists and given as the first command line argument
to an invocation of the \trinity program.

In Tables \ref{tab:geometrynamelist}--\ref{tab:physicsnamelist} these
parameters are listed along with brief descriptions.

\begin{table}
  \centering
  \begin{tabular}{ccL{11cm}}
    \toprule
    Variable Name & Symbol & Description\\
    \midrule
    \texttt{aref} & $\aref$   & See Section \ref{sec:normalisinglength}\\\hline
    \texttt{amin\_ref} & $r_a$   & The half-width (in metres) of the last closed flux
                            surface at the height of the magnetic axis.\\\hline
    \texttt{psitor\_a} & $\psi_{ta}$   & The toroidal flux encompassed by the last closed flux surface. \\\hline
    \texttt{psipol\_a} & $\psi_{pa}$   & The poloidal flux encompassed by the last closed flux surface. \\\hline
                  & $T_0$ & The normalising temperature (i.e. energy), equal to 1 keV\\\hline
                  & $n_0$ & The normalising density, equal to $10^{20}$ particles per m$^3$\\\hline
                  & $B_0$ & The normalising magnetic field, equal to 1 Tesla. 
                          NB. This must not be confused with \texttt{bmag\_grid}, defined
                          below, which is the field strength used in the gyroBohm normalisation
                          of fluxes, etc.\\\hline
    \texttt{bmag\_grid} & $\tilde{B}_a$ & The normalising field used in the gyroBohm 
                          normalisation of fluxes, equal to $B_a/B_0$ where $B_a\equiv I/\majorrcentre{}$ ($I$ is the
                          poloidal current flux function, see Section \ref{sec:Idef}). Note that
                          the output of all flux codes is converted to this magnetic field
                          normalisation, regardless of the normalising magnetic field 
                          used in the flux code. Note that $I$ and $\majorrcentre{}$\xspace, and
                          hence $\tilde{B}_a$, are functions of flux label.\\\hline
                   & $m_r$   & The mass of the main ion species (species 2) in kg \\\hline
                   & $m_p$   & The mass of a proton in kg\\\hline 
   \texttt{m\_ion(1)} & $\tilde{m}_r$   & The normalised mass of the main ion species (species 2), equal to $m_r/m_p$ \\\hline
   \texttt{vtfac} & $\gamma$   & The choice of definition of thermal velocity, equal to 1 or 2. 
                          See the definition of $v_{th0}$ below. \\\hline
  \texttt{vtref} & $v_{th0}$   & $\sqrt{\gamma T_0/m_r}$ \\\hline
  & $\rho_{0}$   & The reference gyroradius, equal to $m_r v_{th0}/ e B_0$ \\\hline
  \texttt{rhostar} & $\rho_{\star}$   & $\rho_0 / a$ \\\hline
  \texttt{srcnorm} & & The quantity used to normalise sources of heat, equal to $(\aref/v_{th0}) \rho_{\star}^{-2} (n_0 T_0)^{-1}=1.93173\times 10^{-5} \aref^3/(\sqrt{\tilde{m}_r}\gamma^{1.5})$.\\\hline

  \texttt{nsrcnorm} & & The quantity used to normalise sources of density, equal to $(\aref/v_{th0}) \rho_{\star}^{-2} (n_0)^{-1}=3.23\times10^{-6}\aref\sqrt{\tilde{m}_r/\gamma}/\rho_{\star}^2$.\\\hline
  \texttt{lsrcnorm} & & The quantity used to normalise sources of angular momentum, equal to $(\aref/v_{th0}) \rho_{\star}^{-2} (n_0 \aref m_p v_{th0})^{-1}=1./(1.6726\times10^{-7}(v_{th0} \rho_{\star})^2)$.\\\hline
   & $n_r$ & The reference density used in the gyroBohm 
   normalisation, equal to the density of the main ions in m$^{-3}$ and 
  equal to \texttt{dens\_grid(:,2)*1e20} within the code.\\\hline
   & $T_r$ & The reference temperature used in the gyroBohm 
   normalisation, equal to the temperature of the main ions in Joules and 
  equal to \texttt{temp\_grid(:,2)*1e3*1.6e-19} within the code (remembering 
  that \texttt{temp\_grid} is in keV).\\\hline
  & $v_{th,r}$ & The reference thermal velocity used in the gyroBohm 
   normalisation, equal to $\sqrt{\gamma T_r/m_r}$. \\\hline
  & $\rho_{r}$ & The reference gyroradius used in the gyroBohm 
  normalisation, equal to $v_{th,r}/(e B_a / m_r) = \sqrt{\gamma T_r m_r}/(e B_a)$. \\\hline
  \texttt{Gamma\_gB} & & The normalising particle flux, equal to
  $n_r v_{th,r} (\rho_r/\aref)^2$ \\\hline
  \texttt{Q\_gB} & & The normalising heat flux, equal to
  $n_r T_r v_{th,r} (\rho_r/\aref)^2$ \\\hline
  \texttt{Pi\_gB} & & The normalising momentum flux, equal to
  $n_r m_r \aref v_{th,r}^2 (\rho_r/\aref)^2$ \\\hline
    \bottomrule
  \end{tabular}
  \caption[A list of the normalising quantities used in \trinity.]{A list of the normalising quantities used in \trinity. Variable names, where
  given, are what appears in the code. Symbols, where given, refer to definitions in this
  document or in M. Barnes' notes on the \trinity algorithm.}
  \label{othernorms}
\end{table}








\begin{table}[p]
  \centering
\renewcommand{\arraystretch}{1.2}
  \begin{tabular}{c<{\RaggedRight}m{15cm}}
   \toprule
    Name & Description \\
   \midrule
   \texttt{t} & 
   \textit{Unlimited}. Indexes quantities as a function of time.
   The corresponding variable \texttt{t} gives the time in seconds corresponding
   to each time point. When the \trinity netCDF file is being
   used as an input format, the parameter \texttt{geo\_time} selects the 
   value of the time where the input arrays will be read. Thus the time
   values can be anything (e.g. shot time, time since ramp-up, time since beginning of
   transport simulation etc.), provided \texttt{geo\_time} is set consistently
   with it. When the netCDF
   file is an output file, the time is the time since the beginning of the \trinity
   simulation. Note that \trinity is usually only run to steady-state with constant
   sources, and in this case the values of time are not particularly meaningful,
   except as measuring the rate of change of the profiles as they move to steady-state.\\
   \hline
   \texttt{eval} &
   \textit{Unlimited}. Indexes calls to the flux code, which do not have a one-to-one correspondence
   with timesteps and iterations. When the netCDF is being used as input file,
   this dimension can be omitted. This will mean there is only one infinite 
   dimension in the file and so the input file can be a netCDF3 (classic) file
   if need be.
   \\
   \hline
   \texttt{iter} & 
   Indexes the iteration of the implicit Gauss-Newton solve within each timestep.
   When the netCDF file is being used as an input file, this dimension is meaningless
   and should be set to have size 1. NB it cannot be omitted.\\
   \hline
   \texttt{rad} &
   This indexes quantities given as a function of flux label, specified at values 
   of $\rho$ given in the variable \texttt{rad}. These values of $\rho$ 
   are the values at the \trinity cell boundaries (see Section \ref{sec:cellcb}).\\
   \hline
   \texttt{cc} &
   This indexes quantities given as a function of flux label, specified at values 
   of $\rho$ given in the variable \texttt{cc}. These values of $\rho$ 
   are the values at the \trinity cell centres (see Section \ref{sec:cellcb}).
   Fluxes are evaluated at cell centres and hence all flux variables in the output
   are given at cell centres.\\
   \hline
   \texttt{tspec} &
   Indexes species. The first species in \trinity is always the electrons, 
   the second the main ions. When calculating normalising quantities such
   as $Q_{gB}$, the reference density and temperature are always those
   of the second species.
   \\ \hline
   \texttt{ion} &
   Indexes ion species. The first ion species must be the main ions and
   is used as the reference species for gyroBohm normalisations. Only used
   for a small number of variables (e.g. \texttt{m\_ion}).
   \\ \hline
   \texttt{mrow} &
   Indexes the row of the matrix used in the Gauss-Newton iteration.
   \\ \hline
   \texttt{mcol} &
   Indexes the col of the matrix used in the Gauss-Newton iteration.
   \\ \hline
   \texttt{ntheta} &
   Number of theta points used in equilibrium variables.
   \\ \hline
   \texttt{jac} &
   Indexes element of the differential fluxes needed for the Jacobian 
   used in the Gauss-Newton iteration.
   \trinity requires quantities such as $dQ/d(\aref/L_T)$. The first index
   of this dimension gives fluxes given current profile gradients. 
   The remaining elements give fluxes given small changes to each
   profile gradient in turn, which allow the necessary gradients to be calculated.
   \\ \hline
   \texttt{job} &
   Indexes actual flux tube code calls per flux evaluation and is equal to
   the size of the \textit{cc} dimension multiplied by the size of the \textit{jac}
   dimension.
   \\
   \bottomrule
  \end{tabular}
  \caption[Dimensions in the \trinity netCDF file.]{Dimensions in the \trinity netCDF file. There are two unlimited dimensions:
  \textit{t} and \textit{eval}. The \textit{eval} dimension can be omitted if creating
  an input file for \trinity (and thus netCDF3 can be used). Other dimensions that can be omitted for creating an input file are: \textit{cc}, \textit{mrow}, \textit{flxvec}, \textit{jac} and \textit{job}.}
  \label{tab:dimensions}
\end{table}


\clearpage
\LTcapwidth=\textwidth
\setlength{\LTleft}{-20cm plus -1fill}
\setlength{\LTright}{\LTleft}
\renewcommand{\arraystretch}{1.3}
\begin{longtable}{C{3.5cm}C{2.5cm}C{1.3cm}C{2.5cm}L{7.4cm}}
\toprule
Variable name & Dimensions & Type & Units & Description\\
\midrule
\endhead
\bottomrule
  \\\caption[]{A table of primary input and output values in the \trinity netCDF file.
  These quantities must be in the netCDF file when it is being used as an input. (Continued on next page).}\\
  \endfoot
\bottomrule
  \\\caption[A table of primary input and output values in the \trinity netCDF file.]{A table of primary input and output values in the \trinity netCDF file.
  These quantities must be in the netCDF file when it is being used as an input.}\\
  \endlastfoot
\input{primary_input_output.tex}
  \label{tab:primaryvariables}
\end{longtable}

%\begin{table}[h]
  %\variabletable{primary_input_output.tex}
  %\caption[A table of primary input and output values in the \trinity netCDF file.]{A table of primary input and output values in the \trinity netCDF file.
  %These quantities must be in the netCDF file when it is being used as an input.}
  %\label{tab:primaryvariables}
%\end{table}


\begin{table}[h]
  \variabletable{globals.tex}
  \caption{A list of global quantities output by \trinity}
  \label{tab:globalvariables}
\end{table}

\begin{table}[]
  \variabletable{restart.tex}
  \caption{A list of variables used by \trinity when restarting from 
  the middle of a run.}
  \label{tab:restartvariables}
\end{table}

\begin{table}[]
  \variabletable{fluxevals.tex}
  \caption{Results from each evaluation of the fluxes, i.e.
  each call to the flux code.}
  \label{tab:fluxevals}
\end{table}

\begin{table}[h]
  \variabletable{perturb.tex}
  \caption{A list of perturbed quantities, used as input for the flux code calculations. 
Each array contains the base value of the quantity at every cell centre, plus one perturbed
set of quantities for every profile which is being treated implicitly.}
  \label{tab:perturbed}
\end{table}


\begin{table}[h]
  \variabletable{derived_grids.tex}
  \caption{A list of grid quantities which can are derived from grid quantities in Table \ref{tab:primaryvariables}.}
  \label{tab:globalvariables}
\end{table}

\begin{table}[]
  \variabletable{cc.tex}
  \caption{Quantities evaluated at cell centres rather than at cell boundaries.}
  \label{tab:cc}
\end{table}



\begin{table}[]
  \variabletable{gs2.tex}
  \caption[Inputs and outputs of every call to \textsc{Gs2}]{Inputs and outputs of every call to \textsc{Gs2}, all normalised
  using \textsc{Gs2} normalisations. Only present when \textsc{Gs2} is called
  during the \trinity run.}
  \label{tab:gs2}
\end{table}


\begin{table}[]
  \variabletable{others.tex}
  \caption{Other parameters.}
  \label{tab:other}
\end{table}






\input{namelist_includes.tex}




\end{document}


