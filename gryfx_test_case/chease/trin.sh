#!/bin/bash

## job requirements for load leveler
######################################################################
#@bg_size=48
#@job_type=bluegene
#@class=prod
#@executable=trin.sh
#@environment=COPY_ALL
#@output=trin.$(jobid).output.txt
#@error=trin.$(jobid).error.txt
#@wall_clock_limit=12:00:00
#@notification=complete
#@queue

## commands to be executed
######################################################################
## This is any custom configuration required for the code:

printenv
echo "Submitting 1024x16 job on blue_joule for project ()..."
	
	

runjob --env-all --exe /gpfs/home/HCEA031/egh01/egh79-egh01/Code/trinity/trunk/trinity --np 768 --ranks-per-node 16 --args "shot42982_jet.trin"
