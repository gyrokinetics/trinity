
# This ruby script strips out all variables Trinity doesn't need from a 2d
# ITER DB file. The first argument is the full file, the second argument
# is the stripped file.
#
#

require 'pp'

txt = File.read ARGV[0]
vars = txt.scan(/\b([A-Z0-9]+)\b.*DEPEN/).map{|arr| arr[0]}
p vars

txt2 = File.read('../nteqns.f90') + "\n" + File.read('../expt_profs.fpp')
#vars2 = txt2.scan(/var_name\s*=[\s&(\\]*(\s*(?:"\s*[A-Z]+\s*"\s*,\s*)+)/m)
#vars2 = txt2.scan(/var_name\s*=[\s&(\/]*(\s*(?:"\s*[A-Z]+\s*"\s*,\s*)+)/m)
vars2 = txt2.scan(/[A-Z0-9]+/)
#p vars2

p varsunwanted = vars-vars2

p varswanted = vars - varsunwanted
#varswanted = vars

File.open(ARGV[1], 'w'){|file| file.puts txt.split(/(?=^\s*\d+.*IDENTIF)/).find_all{|txt| varswanted.find{|v| txt =~ Regexp.new("\\b#{v}\\b")}}}



