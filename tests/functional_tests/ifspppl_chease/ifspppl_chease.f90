!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      logical :: checks
      checks = .true.
      call check_alpha_power(checks, 0.0, 1.0e-7)
      ! Results changed after r3752 because the normalisation of area
      ! was corrected. 
      ! Results changed after  r3810 because dpspi was being calculated 
      ! incorrectly for update_chease (this will not have affected any known
      ! result).
      ! Results changed after r3945 after Trinity was changed to store the original
      ! chease grid and pressure gradient.
      call check_qflux_cc_1(checks, (/3.7599498547303399E-002, 5.0963783243790292E-002,&
        0.76105022094502595, 1.0329030532019705, 1.7692554825809821, 2.1761593305857869, &
        2.3313673772505230, 2.9124163692245513 , 3.4273125495199630, 3.7185567050016273, &
        4.9367953023980506, 5.5040007105805451, 6.6578053509149715, 8.6973308431560419, &
        10.523505485597532, 13.287458146929918, 16.454879223021528, 21.843442087922917, &
        33.318098471931883, 64.167090632908923/), 1.0e-7)
      ! We only expect this low res test to get the growth rate to 3%
      !checks =  check_growth_rate((/0.164/), 0.3)
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call system("cp chease/EXPEQ.OUT.ORIGINAL chease/EXPEQ.OUT")

  call test_trinity('IFS-PPPL fluxes, CHEASE input, analytic sources, update CHEASE geometry', checks)

  call system("cp chease/EXPEQ.OUT.ORIGINAL chease/EXPEQ.OUT")
  call system("cp chease/ogyropsi00001.dat chease/ogyropsi.dat")


end program ifspppl_iterdb
