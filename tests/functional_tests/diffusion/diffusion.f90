!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      logical :: checks
      checks = .true.
      call diffusion_check(checks, 1.0e-2)
    end function checks
end module checks_mod

program diffusion
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('Analytic diffusion test, see Barnes thesis', checks)


end program diffusion
