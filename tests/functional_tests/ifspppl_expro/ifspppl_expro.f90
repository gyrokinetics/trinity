!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      logical :: checks
      checks = .true.
      call check_temp_grid(checks, 1, (/ 2.3843543525886761,&
        1.7515248485804902, 1.6154725114377808, 1.3972275053044847,&
        1.3060064593185789, 1.1095065600333172, 1.0214730554718825,&
        0.82979926995705722, 0.60974654507331139, 0.33277967838442535/), 1.0e-7)
      call check_temp_grid(checks, 2, (/1.4004466017561381, 1.3623684558941933,&
        1.2548285373225807, 1.1615179200619423, 1.0530520412990241,&
        0.95914628069204122, 0.86314456277465323, 0.76279210856699020, &
        0.62880927908354012, 0.44537052922779063/), 1.0e-7)
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('IFS-PPPL fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
