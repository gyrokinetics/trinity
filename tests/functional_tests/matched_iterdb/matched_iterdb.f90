!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      use nteqns_arrays, only: tprim_cc_init, fprim_cc_init
      logical :: checks
      checks = .true.

      ! The algorithm is constructed to preserve the piecewise
      ! logarithmic gradient, not the Trinity 5-point derivative,
      ! so the agreement is only 1%
      call check_tprim_cc(checks, 1, tprim_cc_init(:,1), 1.0e-2)
      call check_fprim_cc(checks, 1, fprim_cc_init(:,1), 5.0e-3)
    end function checks
end module checks_mod

program matched_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('Matched fluxes, JET shot 42982 profile database format', checks)


end program matched_iterdb
