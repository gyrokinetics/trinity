!> A test program to run gs2 with the JET record 
!! iterdb input file
module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      use nteqns_arrays, only: qval_cc
      logical :: checks
      character(len=1000) :: gk_system
      checks = .true.
#ifndef GS2_DUMMY_INTERFACE
      call get_environment_variable("GK_SYSTEM", gk_system)
      ! Should get the same results with intel compilers when
      ! built with DEBUG=on. However, leave the 3 lines below 
      ! as reminder of the issue (see bugs#36)
      if (.false. .and. trim(gk_system) == 'archer') then
        call check_alpha_power(checks, 0.266659528925968, 1.0e-2)
        call check_qflux_cc_1(checks, (/0.943008902849823, 0.415311748327243, 13.7508731369878, 24.9161960368229/), 1.0e-1)
      else
        ! Results have changed r3740 because we changed the Trinity normalising
        ! field. Also increased nstep to get better calculation of the growth 
        ! rate
        ! Results have changed after r3471 because of corrected definition of
        ! triangularity.
        call check_qflux_cc_1(checks, (/9.7124359745918802, 9.7124359745918802, 9.7124359745918802, 9.7124359745918802/), 1.0e-7)
      end if
#else
      ! Results for the dummy interface
      !call check_qflux_cc_1(checks, (/0.96489436444043220, &
        !1.0664765725583980, 1.3629179698050797, 2.0362409803375008/), 1.0e-7)
      !call check_qflux_cc_1(checks, qval_cc + qflx_neo_cc(:,2,1), 1.0e-7)
      call check_qflux_cc_1(checks, (/0.91354438476902833, 1.0122054513992342, &
        1.3002442171918600 , 1.9599807715191924/), 1.0e-7)
#endif
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('GS2 fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
