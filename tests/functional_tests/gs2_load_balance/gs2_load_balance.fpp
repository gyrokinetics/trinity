!> A test program to run gs2 with the JET record 
!! iterdb input file
module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      logical :: checks
      character(len=1000) :: gk_system
      checks = .true.
#ifndef GS2_DUMMY_INTERFACE
      call get_environment_variable("GK_SYSTEM", gk_system)
      ! Should get the same results with intel compilers when
      ! built with DEBUG=on. However, leave the 3 lines below 
      ! as reminder of the issue (see bugs#36)
      if (.false. .and. trim(gk_system) == 'archer') then
        call check_alpha_power(checks, 0.274926061526287, 1.0e-2)
        call check_qflux_cc_1(checks, (/1.77738090008807,    0.304843353706228, 9.07811171540200, 25.9513822823159/), 1.0e-1)
      else
        ! These results came from gs2_calibrate... they are slightly 
        ! different because of the calibration factor not being exactly 1
        !call check_alpha_power(checks, 0.26723576833172313, 1.0e-7)
        !call check_qflux_cc_1(checks, (/1.7916666697432369E-003, 8.5116405785742949E-002,  9.6656634899125962,  27.511027623842729/), 1.0e-7)

        ! The tests changed again after r3652. This was because the move to multispecies
        ! meant including tritium as a third species. This allows the deuterium density
        ! to be different to the electron density which significantly changes the results
        ! of this test.

        ! The tests changed again at revision r3740. This was because of the fixing
        ! of the value of btori being passed to gs2 (a slight change) and the change
        ! in the Trinity normalising field (a bigger change).

        ! The tests changed again at r3741. This was because the definition of
        ! triangularity was fixed.

        ! Tests changed again after r3830 because GS2 was fixed at r3776 to correctly 
        ! set the boundary condition if shat < 1e-5
        call check_alpha_power(checks, 0.51987198139659918, 1.0e-7)
        call check_qflux_cc_1(checks, (/ 6.7291456627061388E-004, &
         0.66548995414451761, 8.6228249206346756 , 28.359690432093714/), 1.0e-7)
      end if
#else
      call check_alpha_power(checks, 3.2398977440874204E-003, 1.0e-7)
      call check_qflux_cc_1(checks, (/0.96489436444043220, &
        1.0664765725583980, 1.3629179698050797, 2.0362409803375008/), 1.0e-7)
      checks = .true.
#endif
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('GS2 fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
