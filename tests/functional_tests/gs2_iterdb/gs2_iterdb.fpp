!> A test program to run gs2 with the JET record 
!! iterdb input file
module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      use nteqns_arrays, only: qval_cc
      logical :: checks
      character(len=1000) :: gk_system
      checks = .true.
#ifndef GS2_DUMMY_INTERFACE
      call get_environment_variable("GK_SYSTEM", gk_system)
      ! Should get the same results with intel compilers when
      ! built with DEBUG=on. However, leave the 3 lines below 
      ! as reminder of the issue (see bugs#36)
      if (.false. .and. trim(gk_system) == 'archer') then
        call check_alpha_power(checks, 0.266659528925968, 1.0e-2)
        call check_qflux_cc_1(checks, (/0.943008902849823, 0.415311748327243, 13.7508731369878, 24.9161960368229/), 1.0e-1)
      else
        ! These tests changed with the new gs2 interface and constant_random and
        ! the diffusivity being treated correctly (diff not being divided by
        ! time_interval)
        ! The tests changed again at r3640. This was because the move to multispecies
        ! meant including tritium as a third species. This allows the deuterium density
        ! to be different to the electron density which significantly changes the results
        ! of this test.
        ! Tests changed again slightly after r3562 because the handling of zeff_grid was changed.
        ! In this test zeff_grid is now the iterdb zeff (as it was pre-multispecies)
        ! Tests changed again after r3740 because (1) the GS2 normalising field has been
        ! correctly calculated (a small change) and (2) the Trinity B field normalisation
        ! has been changed and these fluxes are normalised with the new normalisation.
        ! Tests changed again after r3830 because GS2 was fixed at r3776 to correctly 
        ! set the boundary condition if shat < 1e-5
        call check_alpha_power(checks, 0.52211749704135557, 1.0e-7)
        call check_qflux_cc_1(checks, (/6.9202919662999943E-004, 1.9578201093220322, 9.9797424257378324, 28.316466915665398/), 1.0e-7)
      end if
#else
      ! Results for the dummy interface
      call check_alpha_power(checks, 6.0605550817049211E-003, 1.0e-7)
      !call check_qflux_cc_1(checks, (/0.96489436444043220, &
        !1.0664765725583980, 1.3629179698050797, 2.0362409803375008/), 1.0e-7)
      !call check_qflux_cc_1(checks, qval_cc + qflx_neo_cc(:,2,1), 1.0e-7)
      call check_qflux_cc_1(checks, (/0.91406787979567816, 1.0134578808849084, &
        1.3032172303645586 , 1.9690112104348176/), 1.0e-7)
#endif
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('GS2 fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
