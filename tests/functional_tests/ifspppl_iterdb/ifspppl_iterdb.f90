!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      logical :: checks
      checks = .true.
      ! Results changed after r3652 because of the move to multispecies. We now 
      ! have tritium in the simulation, so the density gradient of the deuterium
      ! has changed.
      !
      ! Results have changed after r3740 because of the change in the Trinity
      ! field normalisation. The field used for ifspppl fluxes is just the Trinity
      ! normalising field, which changed from being bmag_vac_geo to its current 
      ! definition of btori_grid/rmajor_grid/aref
      ! I have checked the that the change is just what one would expect by 
      ! comparing the new result below to the old one multiplied by 
      ! (bmag_cc/bmag_vac_geo)**2.0. They are very similar. EGH

      call check_alpha_power(checks, 1.3165542915665209, 1.0e-7)
      call check_qflux_cc_1(checks, (/3.7820724654513888E-002,&
        9.5217399198699537E-002, 8.8604413624897058E-002, 0.14021681564453284,&
        0.25836333774342024, 0.48794720175788109, 1.0530190719220969, &
        2.8265828928951549/), 1.0e-7)

      ! We only expect this low res test to get the growth rate to 3%
      !checks =  check_growth_rate((/0.164/), 0.3)
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('IFS-PPPL fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
