!> A test program to run gs2 with the JET record 
!! iterdb input file
module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      use trinity_input, only: ncc_calibrate
      use trinity_input, only: calib_option_switch, &
        calib_option_piecewise_linear
      logical :: checks
      character(len=1000) :: gk_system
      checks = .true.
      !call check_alpha_power(checks, 3.2398977440874204E-003, 1.0e-7)
      if (ncc_calibrate.eq.4) then
        call check_qflux_cc_1(checks, (/3.0,3.0,3.0,3.0/), 1.0e-7)
      else if (ncc_calibrate.eq.3) then
        if (calib_option_switch .eq. calib_option_piecewise_linear) then
          call check_qflux_cc_1(checks, (/3.0,4.0,3.0,3.0/), 1.0e-7)
        else
          call check_qflux_cc_1(checks, (/3.0,3.6115497106927963,3.0,3.0/), 1.0e-7)
        end if
      end if
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('Test calibration system', checks)


end program ifspppl_iterdb
