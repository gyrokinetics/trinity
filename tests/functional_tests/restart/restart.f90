!> A test program to run a linear slab itg benchmark
!! at low resolution and check the growth rate

module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function nochecks()
      logical :: nochecks
      nochecks = .true.
    end function nochecks
    function checks()
      logical :: checks
      checks = .true.
      ! Results changed after r3652 because of the move to multispecies. We now 
      ! have tritium in the simulation, so the density gradient of the deuterium
      ! has changed.
      !
      ! Results have changed after r3740 because of the change in the Trinity
      ! field normalisation. The field used for ifspppl fluxes is just the Trinity
      ! normalising field, which changed from being bmag_vac_geo to its current 
      ! definition of btori_grid/rmajor_grid/aref
      ! I have checked the that the change is just what one would expect by 
      ! comparing the new result below to the old one multiplied by 
      ! (bmag_cc/bmag_vac_geo)**2.0. They are very similar. EGH

      call check_alpha_power(checks, 1.2992219909948484, 3.0e-2)
      !call check_qflux_cc_1(checks, (/3.8362429296385754E-003, &
        !0.44791634439359829, 1.7482507628243890E-003, 0.12781344796622601,&
        !0.11951179530922769, 0.32721569907096132, 0.95218942083480851,&
        !2.755273344472187/), 5.0e-2)

      call check_temp_grid(checks, 2, (/12.400697033917094,&
        10.260468900327879, 8.3553369683766636, 7.1412924819148200,&
        6.1283048482097522, 5.1267488848697820, 4.1074564576539885,&
        3.1651754792011650 , 2.1318694622020988/), 2.0e-2)

      ! We only expect this low res test to get the growth rate to 3%
      !checks =  check_growth_rate((/0.164/), 0.3)
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use command_line_trin, only: cl_iargc
  use checks_mod, only: checks, nochecks
  use trinity_main, only: check_restart_from_crash


  if (cl_iargc() .gt. 1) then
    if (cl_iargc() .gt. 2) check_restart_from_crash = .true.
    call test_trinity('IFS-PPPL fluxes, JET shot 42982 setup restart check', nochecks)
  else 
    call test_trinity('IFS-PPPL fluxes, JET shot 42982 test restart functionality', checks)
  end if


end program ifspppl_iterdb
