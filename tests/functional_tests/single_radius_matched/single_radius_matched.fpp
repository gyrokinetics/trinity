!> A test program to run gs2 with the JET record 
!! iterdb input file
module checks_mod
  use functional_tests_trinity
  public checks
  contains
    function checks()
      use nteqns_arrays, only: qval_cc
      logical :: checks
      real,dimension(4) :: results
      checks = .true.
#ifndef GS2_DUMMY_INTERFACE

      results = (/0.28652276836000518, 0.87496627013167427, 1.3446212357508203, 1.9459629000642609/)
      call check_tprim_cc(checks, 1, results, 2.0e-1)
#else
      ! Results for the dummy interface
      !call check_qflux_cc_1(checks, (/0.96489436444043220, &
        !1.0664765725583980, 1.3629179698050797, 2.0362409803375008/), 1.0e-7)
      !call check_tprim_cc(checks, 1, tprim_cc_init(:,1), 1.0e-1)
      results = (/0.28652276836000518, 0.87496627013167427, 1.3446212357508203, 1.9459629000642609/)
      call check_tprim_cc(checks, 1, results, 2.0e-1)
#endif
    end function checks
end module checks_mod

program ifspppl_iterdb
  use functional_tests_trinity, only: test_trinity
  use checks_mod


  call test_trinity('GS2 fluxes, JET shot 42982 profile database format', checks)


end program ifspppl_iterdb
