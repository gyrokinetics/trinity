
!> A program that runs unit tests on the gs2_interface module.
!! It just checks it can read the test file and checks the values
!! of a few random parameters
!!
!! This is free software released under GPLv3
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_gs2_interface
  use unit_tests_trin
  use trinity_input
  use fluxes
  use mp_trin, only: init_mp, finish_mp, job_fork
  use mp_trin, only: subprocs, proc0, broadcast
  use nteqns, only: nteqns_unit_test_init_nt
  use chease_ecom, only: chease_ecom_unit_test_update_chease
  use nteqns, only: distribute_profs
  use nteqns_arrays, only: qval_cc, temp_cc, bgeo_cc, bmag_cc
  use nteqns_arrays, only: rmin_grid, shift_grid, kappa_grid
  use nteqns_arrays, only: rmajor_grid, rad_grid,deltprim_grid
  use nteqns_arrays, only: kapprim_grid, delta_grid, btori_grid
  use file_utils_trin, only: init_file_utils, run_name
  use trindiag, only: init_trindiag
  use read_chease
  use read_chease, only: deallocate_chease_arrays => finish
  use miller_geometry, only: init_miller_geo, get_Iflxfnc, get_jacrho
  use trinity_main, only: trinity_type
  implicit none
  type(trinity_type) :: trin
  real :: eps
  real, dimension(5,2) :: results
  real, dimension(4,3) :: qflux_cc_results
  logical :: dummy_interface
  integer :: focus
  real :: time
  integer :: dummy
  character (10000), target :: cbuff
      character(len=1000) :: gk_system
  character (len=100) :: file_name_temp
  real, dimension(5) :: btori_grid_compare 
  !logical :: test_result

  call init_mp

  ! General config
  eps =3.0e-2

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'


  call announce_module_test('gs2_interface')



  if (proc0) then
    call init_file_utils
    cbuff = trim(run_name)
  end if
  call broadcast (cbuff)
  if (.not. proc0) run_name => cbuff

  results = 0.0

  ! In init_nt, values of geometry & profiles are read from
  ! chease/ogyropsi.dat. Here we check that the values of those
  ! quantities interpolated onto the Trinity grid are correct.
  ! At the moment, these are just values output by Trinity at 
  ! revision number we believed to be correct (though you can 
  ! visually double check that the ion temp matches that in 
  ! ogyropsi.dat (Trinity in keV, ogyropsi in eV).

  ! Psi_toroidal
  results(:,1) = (/2.9301508578983164E-002, 8.7351498344216572E-002, &
    0.14572820576557688, 0.20679038407961653, 0.26985912903411818/)

  ! The results changed at r3740 because the definition of psitor_a for CHEASE was wrong
  ! before. This should not have affected any simulations I know of. EGH
  results(:,1) = (/9.2537061822377351E-002, 0.27586467027004419, 0.46022408538602583, 0.65306448315681542 , 0.85224181681466493/)
  ! Ion Temperature
  results(:,2) = (/1.9963512628646314, 1.6380075072922708, 1.3055213829467285, 0.96167240679495436, 0.53383328591599000/)

  !call system('bunzip2 -c chease/ogyropsi.dat.bz2 > chease/ogyropsi.dat')

  call announce_test('init_nt with chease')
  call process_test(nteqns_unit_test_init_nt(results, eps), 'init_nt with chease')

  call init_trindiag(trin%gnostics, .false.)

  call job_fork (njobs, flux_groups, load_balance)
  call distribute_profs (time)

  ! allocate flux arrays and broadcast profiles
  ! scope changes to subprocs in init_fluxes
  call init_fluxes(trin%fx)
  focus = subprocs


  call announce_test('get fluxes with gs2')
  dummy_interface = .false.
#ifdef GS2_DUMMY_INTERFACE
  dummy_interface = .true.
#endif

  write (*,*) 'temp_cc', temp_cc
  if (dummy_interface) then
    qflux_cc_results(:,1) = qval_cc
    qflux_cc_results(:,2) = qval_cc
    qflux_cc_results(:,3) = 0.0
  else
    !qflux_cc_results(:,1) = (/2.6907833360381696E-002,  9.1579360569504331E-002, 0.21074985769220045,   0.26654223782400216/)
    !qflux_cc_results(:,2) = (/5.8054266604574099E-003,   7.5815853740621858E-002, 0.18490388870239283,  0.15258854776434211/)
    call get_environment_variable("GK_SYSTEM", gk_system)
      ! Should get the same results with intel compilers when
      ! built with DEBUG=on. However, leave the 5 lines below 
      ! as reminder of the issue (see bugs#36)
    if (.false. .and. trim(gk_system) == 'archer') then
      qflux_cc_results(:,1) = (/0.962796961084406,       12.3073935550108,28.7851058352153 ,6.93583885047976/)* (bgeo_cc/bmag_cc)**2.0
      qflux_cc_results(:,2) = (/0.941694554384481, 12.2916300481819, 28.7592598662255, 6.82188516042010/)* (bgeo_cc/bmag_cc)**2.0
      qflux_cc_results(:,3) = 0.0
      eps = 1.0e-1
    else 
      ! Results have changed after time interval being treated correctly.
      ! Results also changed after r3643 because the gradients were calculated
      ! in a different fashion, and also the delta, delta_prim and kappa_prim
      ! values were read properly from the chease file
      ! Results have also changed because we have corrected the definition of
      ! the normalising magnetic field for gs2: a normalisation which is imposed
      ! by our choice of I_N = R_geo/amin_ref = aspr

      ! Results have changed again at r3740 as a result of changing the Trinity field normalisation.
      ! The new results are not a straight forward factor times the old one because the 
      ! field used in the neoclassical estimates have changed.
      ! Results changed again after r3741 because the definition of triangularity was fixed.
      ! This produces very small changes.
      qflux_cc_results(:,1) = (/0.95089391882511198, 10.948222809712911, 18.336378304506489, 98.074022935308889/)
      qflux_cc_results(:,2) = (/0.97455548844512752, 10.966039851915257, 18.362249719717767, 98.235577987622676/)
      qflux_cc_results(:,3) = 0.0
    end if
  end if
  !call process_test(fluxes_unit_test_get_fluxes(&
    !trin%fx, .true., qflux_cc_results, trin%gnostics, trin%clock, eps), &
    !'get fluxes with gs2')
  
  call get_fluxes (trin%fx, trin%clock, trin%calib, "calculate", trin%gnostics)

  call finish_fluxes(trin%fx)


  call close_module_test('gs2_interface')

  call finish_mp
contains
  function input_values()
    logical :: input_values
    input_values = .true.
    call announce_check('flux_option')
    call process_check(input_values, &
      agrees_with(flux_option_gs2, flux_option_switch), 'flux_option')
    call announce_check('ntstep')
    call process_check(input_values, &
      agrees_with(2, ntstep), 'ntstep')
  end function input_values

end program test_gs2_interface
