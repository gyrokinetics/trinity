
!> A program that runs unit tests on the trinity_input module.
!! It just checks it can read the test file and checks the values
!! of a few random parameters
!!
!! This is free software released under GPLv3
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_trinity_input
  use unit_tests_trin
  use trinity_input
  use mp_trin, only: init_mp, finish_mp, proc0
  use file_utils_trin, only: init_file_utils
  implicit none
  real :: eps
  logical :: test_result

  call init_mp

  ! General config
  eps = 1.0e-7

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'


  call announce_module_test('trinity_input')

  if (proc0) call init_file_utils
  
  call announce_test('read_parameters')
  ! This is a bit of a cheat... we don't really test this function,
  ! we just call it and check that the program doesn't crash!
  call read_parameters
  call process_test(.true., 'read_parameters')

  call announce_test('input_values')
  call process_test(input_values(), 'input_values')

  !call announce_test('init_trinity_input')


  call close_module_test('trinity_input')

  call finish_mp
contains
  function input_values()
    logical :: input_values
    input_values = .true.
    call announce_check('flux_option')
    call process_check(input_values, &
      agrees_with(flux_option_gs2, flux_option_switch), 'flux_option')
    call announce_check('ntstep')
    call process_check(input_values, &
      agrees_with(2, ntstep), 'ntstep')
  end function input_values

end program test_trinity_input
