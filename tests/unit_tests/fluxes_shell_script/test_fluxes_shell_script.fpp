!> A program that runs unit tests on the fluxes_shell_script module.
!!
!! This is free software released under the MIT license.
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_fluxes_shell_script
  use unit_tests_trin
  use trinity_input
  use fluxes_shell_script
  use mp_trin, only: init_mp, finish_mp, job_fork
  use mp_trin, only: subprocs, proc0, broadcast
  use nteqns, only: nteqns_unit_test_init_nt
  !use nteqns, only: nteqns_unit_test_update_chease
  use nteqns, only: distribute_profs
  use nteqns_arrays, only: qval_cc
  use file_utils_trin, only: init_file_utils, run_name
  implicit none
  real :: eps
  real, dimension(5,2) :: results
  real, dimension(4,3) :: qflux_cc_results
  logical :: dummy_interface
  integer :: focus
  real :: time
  character (500), target :: cbuff
      character(len=1000) :: gk_system
  !logical :: test_result

  call init_mp

  ! General config
  eps =3.0e-2

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'

  flux_shell_script = 'sh make_fluxes.sh'

  call announce_module_test('fluxes_shell_script')

      ! Results have changed after time interval being treated correctly.
      qflux_cc_results(:,1) = (/1.3,2.3,3.3,4.3/)
      qflux_cc_results(:,2) = (/5.3,6.4,7.3,8.3/)
      qflux_cc_results(:,3) = (/1.4,2.4,3.4,4.4/)

  call init_file_utils

  call process_test(&
    fluxes_shell_script_unit_test_get_fluxes_shell_script(&
    .true., qflux_cc_results, eps), &
    'get fluxes with fluxes_shell_script')
  
  

  call close_module_test('fluxes_shell_script')

  call finish_mp
contains
  function input_values()
    logical :: input_values
    input_values = .true.
    call announce_check('flux_option')
    call process_check(input_values, &
      agrees_with(flux_option_gs2, flux_option_switch), 'flux_option')
    call announce_check('ntstep')
    call process_check(input_values, &
      agrees_with(2, ntstep), 'ntstep')
  end function input_values

end program test_fluxes_shell_script
