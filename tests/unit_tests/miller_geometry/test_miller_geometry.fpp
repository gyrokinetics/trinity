
!> A program that runs unit tests on the miller_geometry module.
!! It just checks it can read the test file and checks the values
!! of a few random parameters
!!
!! This is free software released under GPLv3
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_miller_geometry
  use unit_tests_trin
  use trinity_input
  use mp_trin, only: init_mp, finish_mp, job_fork
  use mp_trin, only: subprocs, proc0, broadcast
  use nteqns, only: init_nt
  use chease_ecom, only: chease_ecom_unit_test_update_chease
  use norms, only: psitor_a, amin_ref
  use nteqns, only:  distribute_profs
  use nteqns_arrays, only: qval_cc, temp_cc
  use nteqns_arrays, only: rmin_grid, shift_grid, kappa_grid
  use nteqns_arrays, only: rmajor_grid, rad_grid,deltprim_grid
  use nteqns_arrays, only: kapprim_grid, delta_grid, btori_grid
  use file_utils_trin, only: init_file_utils, run_name
  use trindiag, only: init_trindiag
  use trindiag_config, only: trindiag_type
  use read_chease
  use read_chease, only: deallocate_chease_arrays => finish
  use miller_geometry, only: init_miller_geo, get_Iflxfnc, get_jacrho
  use trinity_input, only: nrad
  use interp, only: fpintrp
  use constants_trin, only: pi
  use trinity_type_module, only: trinity_clock_type
  implicit none
  type(trindiag_type) :: gnostics
  real :: eps
  real, dimension(5,2) :: results
  real, dimension(4,3) :: qflux_cc_results
  logical :: dummy_interface
  integer :: focus
  real :: time
  integer :: dummy, dummy2
  integer, parameter :: test_res = 400
  character (10000), target :: cbuff
  character (len=100) :: file_name_temp
  real, dimension(80) :: btori_grid_compare 
  real, dimension(:), allocatable :: rmin_test, rmajor_test, rhot_test, psi_t_test
  real, dimension(:), allocatable :: kappa_test, delta_test, btori_test
  real, dimension(:), allocatable :: shift_test, kapprim_test, deltprim_test
  real, dimension(test_res) :: rho_t_exp_test, rmin_exp_test, psi_t_exp_test, btori_exp_test
  type(trinity_clock_type) :: dummy_cl
  real :: phia_test, raxis
  integer :: i
  !logical :: test_result

  call init_mp

  ! General config
  eps =3.0e-2

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'


  call announce_module_test('miller_geometry')



  if (proc0) then
    call init_file_utils
    cbuff = trim(run_name)
  end if
  call broadcast (cbuff)
  if (.not. proc0) run_name => cbuff

  time = 1.0
  dummy =1
  dummy2=1
  call init_nt(dummy_cl)
  
  call announce_test('Calculating I using concentric circles')
  if (proc0) then

      allocate(rmin_test(nrad), rmajor_test(nrad), shift_test(nrad))
      allocate(kappa_test(nrad), kapprim_test(nrad), delta_test(nrad))
      allocate(deltprim_test(nrad), btori_test(nrad))
      allocate(rhot_test(nrad), psi_t_test(nrad))

      ! Any constant number will do
      btori_exp_test = 2.0

      raxis = 3.0

      do i = 1,test_res
        ! Minor radius of LCFS is 1m, hence normalising length
        ! is 1m
        rmin_exp_test(i) = real(i)/real(test_res)
        ! Formula calculated in my notes 'Trinity Calculations' EGH
        ! Assumes concentric circles, constant btori
        psi_t_exp_test(i) = 2.0 * pi * btori_exp_test(1) * raxis * &
          (1.0 - sqrt(1.0 - (rmin_exp_test(i)/raxis)**2.0))
      end do
      rho_t_exp_test = sqrt(psi_t_exp_test/psi_t_exp_test(test_res))
      phia_test = psi_t_exp_test(test_res)

      !write (*,*) 'Interpolating results'

      do i = 1,nrad
        rmin_test(i) = real(i)/real(nrad) * 0.8
        rhot_test(i) = fpintrp(rmin_test(i),rho_t_exp_test,rmin_exp_test, test_res)
        rmajor_test(i) = raxis 
      end do 

      shift_test = 0.0
      kappa_test = 1.0
      kapprim_test = 0.0
      delta_test = 0.0
      deltprim_test = 0.0

      !write (*,*) 'Calling init_miller_geo'
      call init_miller_geo (rmin_test, rmajor_test, shift_test, &
           kappa_test, kapprim_test, asin(delta_test), &
           deltprim_test/sqrt(1-(rmin_test)**2))
         !call get_jacrho

      !write (*,*) 'Calling get_Iflxfnc'
      
      call get_Iflxfnc(rmin_test,rho_t_exp_test/rho_t_exp_test(test_res),&
        rmin_exp_test, &
        phia_test, btori_test) 

      call announce_test('Value of btori with concentric circles')
      call process_test(agrees_with(&
        ! Using kappa_test here just because it happens to be equal
        ! to 1 which is the correct answer
        btori_test(2:)/btori_exp_test(1), kappa_test(2:), 1.0e-4),&
        'Value of btori with concentric circles')
      !write (*,*) 'btori_test', btori_test, btori_exp_test(1), 'psitor_a', phia_test
      !write (*,*) 'btori_grid ratio', btori_test/btori_exp_test(1)
      !write (*,*) rad_grid,rho_t_chease/rho_t_chease(npsi_chease),&
        !ageom_chease/ageom_chease(npsi_chease), &
        !psitor_a, btori_grid_compare
      !write (*,*) '-------------------------'
      !write (*,*) rmin_grid, rmajor_grid, shift_grid, &
           !kappa_grid, kapprim_grid, asin(delta_grid), &
           !deltprim_grid/sqrt(1-(rmin_grid)**2)
  end if

  call announce_test('Calculating I using get_Iflxfnc')
  if (proc0) then
     file_name_temp = 'chease/ogyropsi.dat'
     !write(*,*) 'Before read_infile'
     call read_infile (file_name_temp)
     !write(*,*) 'after read_infile'
      call init_miller_geo (rmin_grid, rmajor_grid, shift_grid, &
           kappa_grid, kapprim_grid, asin(delta_grid), &
           deltprim_grid/sqrt(1-(rmin_grid)**2))
         !call get_jacrho

      
      call get_Iflxfnc(rad_grid,rho_t_chease/rho_t_chease(npsi_chease),&
        ageom_chease, &
        psitor_a, btori_grid_compare) 
      !write (*,*) 'ratio', btori_grid_compare(nrad-8:nrad)/btori_grid(nrad-8:nrad)
      !write (*,*) 'ratio', btori_grid_compare/btori_grid
      !write (*,*) 'psitor_a', psitor_a
      call announce_test('Value of btori using chease')
      ! There is not very good agreement in the test below because
      ! the CHEASE grid only has 40 radial points. When the CHEASE
      ! grid has 100 radial points the agreement is 1e-4 or better.
      call process_test(agrees_with(&
        btori_grid_compare(4:nrad), btori_grid(4:nrad), 2.0e-2),&
        'Value of btori using chease')
      !write (*,*) rad_grid,rho_t_chease/rho_t_chease(npsi_chease),&
        !ageom_chease/ageom_chease(npsi_chease), &
        !psitor_a, btori_grid_compare
      !write (*,*) '-------------------------'
      !write (*,*) rmin_grid, rmajor_grid, shift_grid, &
           !kappa_grid, kapprim_grid, asin(delta_grid), &
           !deltprim_grid/sqrt(1-(rmin_grid)**2)
     call deallocate_chease_arrays 
  end if
  call process_test(.true., 'Calculating I using get_Iflxfnc')
  !call read_parameters



  call close_module_test('miller_geometry')

  call finish_mp
contains

end program test_miller_geometry
