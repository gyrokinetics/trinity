
!> A program that runs unit tests on the gryfx_interface module.
!! It just checks it can read the test file and checks the values
!! of a few random parameters
!!
!! This is free software released under GPLv3
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_gryfx_interface
  use unit_tests_trin
  use trinity_input
  use fluxes
  use mp_trin, only: init_mp, finish_mp, proc0, broadcast
  use nteqns, only: nteqns_unit_test_init_nt
  use chease_ecom, only: chease_ecom_unit_test_update_chease
  use nteqns, only: distribute_profs
  use nteqns_arrays, only: qval_cc, bmag_cc, bgeo_cc
  use file_utils_trin, only: init_file_utils, run_name
  use trindiag, only: init_trindiag
  use trinity_main, only: trinity_type

  implicit none
  real :: eps
  real, dimension(5,2) :: results
  real, dimension(4,2) :: qflux_cc_results
  logical :: dummy_interface
  character (10000), target :: cbuff
  type(trinity_type) :: trin
  real :: time
  !logical :: test_result

  call init_mp

  ! General config
  eps = 1.0e-7

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'


  call announce_module_test('gryfx_interface')


  if (proc0) then
    call init_file_utils
    cbuff = trim(run_name)
  end if
  call broadcast (cbuff)
  if (.not. proc0) run_name => cbuff

  results = 0.0

  ! In init_nt, values of geometry & profiles are read from
  ! chease/ogyropsi.dat. Here we check that the values of those
  ! quantities interpolated onto the Trinity grid are correct.
  ! At the moment, these are just values output by Trinity at 
  ! revision number we believed to be correct (though you can 
  ! visually double check that the ion temp matches that in 
  ! ogyropsi.dat (Trinity in keV, ogyropsi in eV).

  ! Psi_toroidal
  !results(:,1) = (/2.9301508578983164E-002, 8.7351498344216572E-002, &
    !0.14572820576557688, 0.20679038407961653, 0.26985912903411818/)
  ! The results changed at r3740 because the definition of psitor_a for CHEASE was wrong
  ! before. This should not have affected any simulations I know of. EGH
  results(:,1) = (/9.2537061822377351E-002, 0.27586467027004419, 0.46022408538602583, 0.65306448315681542 , 0.85224181681466493/)
  ! Ion Temperature
  results(:,2) = (/1.9963512628646314, 1.6380075072922708, 1.3055213829467285, 0.96167240679495436, 0.53383328591599000/)

  !call system('bunzip2 -c chease/ogyropsi.dat.bz2 > chease/ogyropsi.dat')

  call announce_test('init_nt with chease')
  call process_test(nteqns_unit_test_init_nt(results, eps), 'init_nt with chease')
  call distribute_profs (time)
  call init_trindiag(trin%gnostics, .false.)

  call init_fluxes(trin%fx)
  call announce_test('get fluxes with gryfx')
  dummy_interface = .true.
#ifdef USE_GRYFX
  dummy_interface = .false.
#endif
  if (dummy_interface) then
    ! Need to convert from Trinity B norm to Gryfx B norm
    !qflux_cc_results(:,2) = (bgeo_cc/bmag_cc)**(-2.0)
    ! At the moment, the gryfx interface is hardwired to only do ions
    ! and the electron flux is set to 0.0
    qflux_cc_results(:,1) = 0.0
    qflux_cc_results(:,2) = qval_cc * (bgeo_cc/bmag_cc)**(-2.0)
  end if
  call process_test(fluxes_unit_test_get_fluxes(&
    trin%fx, dummy_interface, qflux_cc_results, trin%gnostics,trin%clock,eps), &
    'get fluxes with gryfx')
  

  
  !call read_parameters




  call close_module_test('gryfx_interface')

  call finish_mp
contains
  function input_values()
    logical :: input_values
    input_values = .true.
    call announce_check('flux_option')
    call process_check(input_values, &
      agrees_with(flux_option_gs2, flux_option_switch), 'flux_option')
    call announce_check('ntstep')
    call process_check(input_values, &
      agrees_with(2, ntstep), 'ntstep')
  end function input_values

end program test_gryfx_interface
