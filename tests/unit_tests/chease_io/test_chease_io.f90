
!> A program that runs unit tests on the trinity_input module.
!! It just checks it can read the test file and checks the values
!! of a few random parameters
!!
!! This is free software released under GPLv3
!!   Written by: Edmund Highcock (edmundhighcock@users.sourceforge.net)
program test_trinity_input
  use unit_tests_trin
  use trinity_input
  use mp_trin, only: init_mp, finish_mp, proc0
  use nteqns, only: nteqns_unit_test_init_nt
  use chease_ecom, only: chease_ecom_unit_test_update_chease
  use file_utils_trin, only: init_file_utils
  implicit none
  real :: eps
  real, dimension(21,2) :: results
  !logical :: test_result

  call init_mp

  ! General config
  eps = 1.0e-7

  if (precision(eps).lt. 11) eps = eps * 100.0
  !if (precision(eps).lt. 11) write (*,*) 'low precision'


  call announce_module_test('chease_io')


  if (proc0) call init_file_utils

  results = 0.0

  ! In init_nt, values of geometry & profiles are read from
  ! chease/ogyropsi.dat. Here we check that the values of those
  ! quantities interpolated onto the Trinity grid are correct.
  ! At the moment, these are just values output by Trinity at 
  ! revision number we believed to be correct (though you can 
  ! visually double check that the ion temp matches that in 
  ! ogyropsi.dat (Trinity in keV, ogyropsi in eV).

  ! Psi_toroidal
  results(:,1) = (/6.4297840879105079E-003,&
        1.9302276756633020E-002 ,&
        3.2161398146472175E-002,&
        4.5013363419419285E-002,&
        5.7802701357485567E-002 ,&
        7.0527863360985613E-002,&
        8.3155953579672717E-002,&
        9.5747445281644944E-002 ,&
        0.10841939861208538,&
        0.12122311527467111,&
        0.13414566461868496 ,&
        0.14718688682528563,&
        0.16035667228774014,&
        0.17366894659566204 ,&
        0.18711831065203272,&
        0.20070462986309739,&
        0.21443156354674830 ,&
        0.22827300298695005,&
        0.24218964601689535,&
        0.25609580804874860 ,&
        0.26985912903411813/)

  ! The results changed at r3740 because the definition of psitor_a for CHEASE was wrong
  ! before. This should not have affected any simulations I know of. EGH
  results(:,1) = (/2.0305894013739566E-002, 6.0958498867950503E-002, 0.10156887590111467, 0.14215665320944001,&
  0.18254664720077279, 0.22273396724415995, 0.26261472499133526, 0.30237990100336809, 0.34239918279523895,&
  0.38283458622056710, 0.42364527500563004, 0.46483074442672156, 0.50642222932387548, 0.54846370808624179,&
  0.59093812983149996, 0.63384506949926200, 0.67719608357731476, 0.72090871815843460, 0.76485885311311408,&
  0.80877588803931544, 0.85224181681466482/)
  ! Ion Temperature
  results(:,2) = (/2.0067379690893024,&
        2.0056740602775025 ,&
        1.9896081070880163,&
        1.9420101488865651,&
        1.8712011909609083 ,&
        1.7804213290259920,&
        1.6725464255574525,&
        1.5782792138212383 ,&
        1.5055113820074744,&
        1.4370461246544735,&
        1.3676938153071663 ,&
        1.2977020743470045,&
        1.2268452374638148,&
        1.1534719669394018 ,&
        1.0771048619726027,&
        0.99787057619471098,&
        0.91571540324909517 ,&
        0.83098291451602058,&
        0.74256372070250776,&
        0.64506493225151296 ,&
        0.53383328591599044/)

  !call system('bunzip2 -c chease/ogyropsi.dat.bz2 > chease/ogyropsi.dat')

  call announce_test('init_nt with chease')
  call process_test(nteqns_unit_test_init_nt(results, eps), 'init_nt with chease')
  

  call announce_test('update_chease')
  ! After all the interpolation that goes on, 0.1 is reasonably good!
  call process_test(chease_ecom_unit_test_update_chease(0.1), 'update_chease')
  
  !call read_parameters




  call close_module_test('chease_io')

  call finish_mp
contains
  function input_values()
    logical :: input_values
    input_values = .true.
    call announce_check('flux_option')
    call process_check(input_values, &
      agrees_with(flux_option_gs2, flux_option_switch), 'flux_option')
    call announce_check('ntstep')
    call process_check(input_values, &
      agrees_with(2, ntstep), 'ntstep')
  end function input_values

end program test_trinity_input
