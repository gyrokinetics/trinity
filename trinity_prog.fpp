!> \mainpage
!! \section intro Introduction
!! 
!! Trinity is a code which solves the equations for the transport of heat, 
!! particles and flow in a troidal magnetic field. Its
!! implicit algorithm allows it to take large timesteps and minimise 
!! turbulent transport calculation. 
!! This is the source code documentation for Trinity, and is aimed at developers. 
!!
#ifdef RELEASE
!! This is the documentation for release RELEASE.
!! For documentation for the development trunk please go to
!! http://gyrokinetics.sourceforge.net/trinity_documentation
#else
!! This is the documentation for the development trunk.
!! For documentation for specific releases please go to
!! http://gyrokinetics.sourceforge.net/trinity_documentation/releases
#endif
!!
!! \subsection udoc User Documentation
!! For a general introduction and user documentation please go to 
!!  - the Trinity homepage: http://www-thphys.physics.ox.ac.uk/people/mbarnes/projects/trinity/
!!  - the Gyrokinetics Wiki: http://gyrokinetics.sourceforge.net/wiki/index.php/Main_Page
!! 
!!
!! \section doc Documentation Structure
!! Documentation is categorized by data types (i.e. modules), and files. Within each namespace the subroutines
!! are documented in various ways, including developer comments and (very useful)
!! a chart showing which other subroutines  call the subroutine and which are
!! called by it.
!!
!! \section starting Starting Out
!!
!!  If you want to start at the beginning and work out what Trinity does, start at
!! trinity_prog.f90, and use the call graphs to follow the algorithm.
!!
!! \section update Updating this documentation.
!! \subsection incode Updating Source Code Comments
!! This documentation is generated from commments added to the source code; to
!! add to it, add more comments to the <i>trunk</i> source. All documenting
!! comments begin with <tt>!></tt> and are continued with <tt>!!</tt>. For more help see the
!! doxygen help: http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html
!!
!! DO NOT add comments in between ifdef/endif preprocessor blocks. Such
!! comments will probably be ignored.
!!
!! - Install doxygen: http://www.stack.nl/~dimitri/doxygen/
!! - Go to the trunk folder and type 
!! 
!!  <tt> make doc sync_doc USER=[sourceforge user name]</tt>


!> Main program. Used when running Trinity
!! standalone, as opposed as a library for, e.g. CodeRunner
program trinity
  use trinity_main
  type(trinity_type) :: trin
  functional_test_flag = .false.
  call run_trinity(trin)
  call finish_trinity(trin)
end program trinity
