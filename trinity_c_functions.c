#include "time.h"
/* Since the C time_t returned by the
 * time function is not supported by
 * iso_c_binding we make a new function
 * that returns the epoch as a double.
 * Doubles have 15 digits of precision
 * which is enough to accurately store
 * the epoch for the next 30 million years.*/ 
void trinity_time_double(double * time_out){
  *time_out = (double)time(NULL);
}
