module nteqns

  use nteqns_arrays
  use norms
  use trinity_time, only: ntdelt_old
  
  implicit none

  private

  public :: init_nt, get_flux_grads, init_geo
  public :: finish_nteqns
  public :: get_Hm!, get_jacob
  public :: get_grads, broadcast_profiles, get_nt_coefs
  public :: distribute_profs, get_delgrads
  public :: get_nt_vector_from_profs, get_profs_from_nt_vector
  public :: get_flx_vector_from_fluxes
  public ::  ix_mid
  public :: volint
  public :: dprims
  public :: dpflxdg, dqflxdg
  public :: get_sources, get_sources_first
  ! Unit tests
  public :: nteqns_unit_test_init_nt

  integer :: ix, ix_mid
  integer, save :: balance_unit

  !real :: rad_in, drad, aref, amin_ref, psitor_a, rhostar, ntdelt_old, bdfac
  real :: rad_in, bdfac

  real, dimension (:,:), allocatable :: dprims, dnt
  real, dimension (:,:,:), allocatable :: dpflxdg, dqflxdg, dheatdg
  real, dimension (:,:,:), allocatable :: dheat_grid
  real, dimension (:,:), allocatable :: dlflxdg !, dlflx_grid

  ! constant coefficients needed for calculation of Jacobian
  real, dimension (:), allocatable :: A_fac, B_fac

  ! Subroutine states
  logical :: get_sources_first
  logical :: get_grads_first
  logical :: broadcast_profiles_first
  logical :: distribute_profs_first
  logical :: get_nu_first

  interface loglamfnc
     module procedure loglamfnc_array
     module procedure loglamfnc_scalar
  end interface
  
contains
  
  subroutine init_geo (dvdrho, grho_in)

    use mp_trin, only: group_to_all, broadcast
    use trinity_input, only: testing, nrad_global
    use trinity_input, only: njobs
    use interp, only: get_gridval
    use trinity_input, only: grho_in_tinp => grho_in
    use expro_import, only: finish_expro_io
    use expro_import, only: expro_init

    implicit none

    real, dimension(nrad_global), intent (in), optional :: dvdrho, grho_in

    ! if dvdrho present, then override area and grho from
    ! ITER db file
    if (present(dvdrho)) then
       call group_to_all (dvdrho*grho_in, area_cc, njobs)
       call group_to_all (grho_in, grho_cc, njobs)
       call get_gridval (area_cc, area_grid)
       call get_gridval (grho_cc, grho_grid, grho_cc(1))
    end if

!    bmag_grid = bmag

    if (testing) then
       area_grid = 1.0
       grho_grid = 1.0
       bmag_grid = 1.0
       area_cc = 1.0
       grho_cc = 1.0
       bmag_cc = 1.0
    elseif (grho_in_tinp > epsilon(0.0)) then
       grho_grid = grho_in_tinp
       grho_cc = grho_in_tinp
    end if

    call broadcast (area_grid)
    call broadcast (grho_grid)
    call broadcast (area_cc)
    call broadcast (grho_cc)

    ! obtain initial sources
    call get_sources (0.0)
    
    if (expro_init) call finish_expro_io

  end subroutine init_geo

  subroutine init_nt (clock)

    use mp_trin, only: proc0
    use file_utils_trin, only: open_output_file
    use trinity_input, only: ntdelt
    use trinity_input, only: read_parameters, vtfac
    use trinity_input, only: m_ion
    use trinity_input, only: write_balance
    use import_export, only: import_geometry, import_initial_profiles
    use unit_tests_trin, only: debug_message, verb_main
    use trinity_type_module, only: trinity_clock_type
    use mp_trin, only: fork_gryfx, allprocs, scope
    use nteqns_arrays, only: broadcast_grids

    implicit none
    type(trinity_clock_type), intent(inout) :: clock

    get_sources_first = .true.
    get_grads_first = .true.
    broadcast_profiles_first = .true.
    distribute_profs_first = .true.
    get_nu_first = .true.

    call debug_message(verb_main, 'nteqns::init_nt starting')

    ! read input parameters from .trin file
    call read_parameters

    call debug_message(verb_main, 'nteqns::init_nt read parameters')

    ! ntdelt_old is old time step size
    ! needed for multi-step backwards difference scheme
    ntdelt_old = ntdelt

    ! reference thermal speed in m/s
    vtref = 3.094e5*sqrt(vtfac/m_ion(1))

    ! allocate arrays for density, pressure, etc.
    call allocate_arrays
    call debug_message(verb_main, 'nteqns::init_nt allocated arrays')

    ! get radial grid
    call get_radial_grid
    call debug_message(verb_main, 'nteqns::init_nt got radial grid')

    ! default for total number of time steps thus far is 1, and
    ! for total number of flux evaluations thus far is 0.  these are overwritten
    ! if running from restart file
    clock%itstep_tot = 1
!    neval_tot = 1
    clock%neval = 0

    call import_geometry (clock%time)
    call debug_message(verb_main, 'nteqns::init_nt got geometry')

    ! Broadcast imported geo grids
    call broadcast_grids('geo')
    call debug_message(verb_main, 'nteqns::init_nt broadcasted geo grids')

    ! aref is set in import_geometry
    rhostar = 3.23e-3*sqrt(vtfac*m_ion(1))/aref

    ! obtain initial profiles
    call import_initial_profiles(clock) 
    call debug_message(verb_main, 'nteqns::init_nt imported initial profs')

    call broadcast_grids('profiles')
    call debug_message(verb_main, 'nteqns::init_nt broadcasted initial profs')

    ! Get cell centred grids
    call get_cc_all
    call debug_message(verb_main, 'nteqns::init_nt got cell centered grids')

    ! Calculate pflux_gb_cc and qflux_gb_cc
    call set_gyrobohm_norm_arrays

    ! obtain collision frequency
    call get_nu
    call debug_message(verb_main, 'nteqns::init_nt calculated collision freq')

    ! compute gradient scale lengths of profiles (stored in fprim_cc, tprim_cc, gexb_cc)
    call get_grads (dens_grid, pres_grid, mom_grid)
    call debug_message(verb_main, 'nteqns::init_nt got grads')

    ! These arrays are needed for the 'matched' flux option
    ! which pins gradients at their initial values.
    tprim_cc_init = tprim_cc
    fprim_cc_init = fprim_cc

    ! calculate amounts by which to perturb gradient scale lengths
    call get_delgrads (dens_grid, pres_grid, mom_grid)
    call debug_message(verb_main, 'nteqns::init_nt got delgrads')

    ! chease_update runs chease equilibrium solver with pressure gradient profile
    ! and overwrites parameters related to flux label
!    select case (geo_option_switch)
!    case (geo_option_iterdb_chease,geo_option_chease)
!       call update_chease
!       call run_chease
!       ! have specified a pressure gradient profile as function
!       ! of chease poloidal flux variables.  must now take
!       ! this profile and work out what it should be in 
!       ! Trinity radial coordinates
!       call update_init_nt
!       call update_geometry (prof=prof_grid)
!    case default
!       ! do nothing extra
!    end select
    if (proc0 .and. write_balance) then
       call open_output_file (balance_unit, ".balance")
       write (balance_unit,'(a6,8a12)') '# spec', 'time', 'rad', 'delp', 'flx', 'source', 'psi', 'equil', 'heat'
    end if
    call debug_message(verb_main, 'nteqns::init_nt finished')

  end subroutine init_nt

  function nteqns_unit_test_init_nt(results, eps)
    use unit_tests_trin
    use trinity_type_module, only: trinity_clock_type
    logical :: nteqns_unit_test_init_nt
    real, dimension(:,:), intent(in) :: results
    real, intent(in) :: eps
    type(trinity_clock_type) :: dummy
    nteqns_unit_test_init_nt = .true.

    call init_nt(dummy)

    write (*,*) 'tem',temp_grid(:,1)
    write (*,*) 'ps', rhotor_grid(:)

    call announce_check('value of rhotor_grid')
    call process_check(nteqns_unit_test_init_nt, &
      agrees_with(results(:,1), rhotor_grid, eps), 'value of rhotor_grid')
    call announce_check('value of ion temp')
    call process_check(nteqns_unit_test_init_nt, &
      agrees_with(results(:,2), temp_grid(:,1), eps), 'value of ion tem')

  end function nteqns_unit_test_init_nt

  subroutine get_radial_grid

    use trinity_input, only: nrad, rad_out, nrad_global
    use interp, only: get_cc

    implicit none

    integer :: ix
    real :: diff_mid, diff_mid_old

    rad_in = rad_out/(2*nrad-1)  ! inner radial boundary
    
    ! set up radial grid
    drad = (rad_out - rad_in) / (nrad - 1)
    do ix = 1, nrad
       rad_grid(ix) = rad_in + (ix-1)*drad
    end do

    ! ix_mid is the index of rad_global closest to mid_radius
    diff_mid_old = 1.0
    ! this is grid for global code (GENE)
    do ix = 1, nrad_global
       rad_global(ix) = rad_in + (ix-1)*(rad_out-rad_in)/(nrad_global-1)
       diff_mid = rad_global(ix) - 0.5
       if (diff_mid > diff_mid_old .and. ix_mid==0) ix_mid = ix-1
       diff_mid_old = diff_mid
    end do

    ! create grid on which we calculate the fluxes (cell centers)
    call get_cc (rad_grid, rad_cc)

  end subroutine get_radial_grid

  subroutine get_sources (time)

    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: open_output_file, flush_output_file
    use interp, only: get_cc
    use trinity_input, only: m_ion
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: te_equal_ti, te_fixed
    use trinity_input, only: nrad, vtfac
    use trinity_input, only: densin, psig, nsig
    use trinity_input, only: include_alphas, include_radiation, write_pwr_profs
    use trinity_input, only: source_file, source_time, radin, temp_equil
    use trinity_input, only: pioq
    use trinity_input, only: nbi_mult, dens_mult
    use trinity_input, only: icrh_mult
    use trinity_input, only: ech_mult
    use trinity_input, only: lh_mult
    use trinity_input, only: ibw_mult
    use trinity_input, only: dwn_mult
    use trinity_input, only: evolve_sources
    use trin_power, only: alpha_power, radiated_power
    use import_export, only: import_sources

    implicit none

    integer :: is, ix, iu
    integer, save :: src_unit
    !real, save :: srcnorm, nsrcnorm, lsrcnorm
    !logical :: get_sources_first = .true.

    real :: dum
    real, dimension (:), allocatable :: dens_input
    real, dimension (:,:,:), allocatable :: nueq

    character (3) :: nspec_str
    character (100) :: str

    real, intent (in) :: time

    allocate (dens_input(nspec)) ; dens_input = 0.
    allocate (nueq(nrad,nspec,nspec))

    if (get_sources_first) then
       src_aux = 0.0 ; src_alph = 0.0 ; src_radiate = 0.0
       
       ! factors multiplying sources to be read in from ITER DB (or input parameters)
       
       ! Normalised heat source = source * (a0/vth0) / rhostar**-2.0 / (n0 T0) 
       ! vth0 = sqrt(vtfac * 1keV / (m_p*m_ion(1)))
       ! rho0 = vth0/Omega0 = sqrt(vtfac T0 m_p * m_ion(1)) / (eB)
       ! rhostar = rho0/a0
       ! T0 = 1keV, n0 = 1e20, a0=aref
       ! NB in SI units Omega = eB/m not eB/mc
       ! putting it altogether
       ! srcnorm = aref**3/(m_ion(1)**0.5*vtfac**1.5 * 
       !     (e B)**2 / (T0**2.5*m_p**0.5)
       srcnorm = 1.93173e-5*aref**3/(sqrt(m_ion(1))*vtfac**1.5)
       ! Normalised density source = source * (a0/vth0)/rhostar**2/n0
       ! vth0 = sqrt(vtfac * 1keV / (m_p*m_ion(1)))
       ! n0 = 1e20
       ! so nsrcnorm = sqrt(m_p/1keV)/1e20*aref*sqrt(m_ion(1)/vtfac)/rhostar**2 
       nsrcnorm = 3.23e-6*aref*sqrt(m_ion(1)/vtfac)/rhostar**2
       ! Normalised angular momentum source = source * (a0/vth0)/rhostar**2/(n0 a0 m_p vth0)
       ! So lsrcnorm = 1/rhostar**2/vth0**2/(n0*m_p)
       ! n0 = 1e20, m_p = 1.6726e-27
       lsrcnorm = 1./(1.6726e-7*(vtref*rhostar)**2)
    
       call import_sources (time)
       
       call broadcast_grids('sources')
       
       if (proc0) then
          call open_output_file (src_unit,".pwr")
          if (write_pwr_profs) then
             write (src_unit,*) '# 1: time, 2: rad, 3: i+ aux pwr dens, 4: e- aux pwr dens, ', &
                  '5: i+ alph pwr dens, 6: e- alph pwr dens, 7: i+ rad pwr dens, ', &
                  '8: e- rad pwr dens, 9: net pwr, 10: aux pwr, 11: alph pwr, 12: rad pwr, 13: dens src', &
                  '14: i+ int aux pwr, 15: e- int aux pwr, 16: i+ int tmp equil, 17: e- int tmp equil, 18: int pflux, 19: int lflux'
          else
             write (src_unit,*) '# 1: time, 2: net pwr, 3: aux pwr, 4: alph pwr, 5: rad pwr, 6: dens src'
          end if
       end if
       
       get_sources_first = .false.
    else if (evolve_sources) then
       call import_sources (time)
       call broadcast_grids('sources')
    end if

    ! src_alph and src_radiate are returned in MW/m^3
    if (include_alphas) call alpha_power (dens_grid, temp_grid, src_alph)
    if (include_radiation) call radiated_power (dens_grid(:,1), temp_grid(:,1), zeff_grid, src_radiate)

    ! normalized sources (in code units, not MW / m^3)
    ! see MAB thesis Eq. 7.28 for details
    src_pres = src_aux + 1.e+6*srcnorm*(src_alph - src_radiate)

    ! this is a hack that is not ideal, but there is no perfect solution
    ! as we would need info about electron heat fluxes to do better
    if (te_equal_ti) then
       ! if taking Te=Ti and 
       ! assume 50:50 split of heat between main ions and electrons
       ! since we are taking Te=Ti
       if (include_alphas .or. include_radiation) src_pres(:,2) = 0.5*(src_pres(:,1)+src_pres(:,2))
    end if

    ! aux_power, alph_power, radiate_power returned in MW
    do is = 1, nspec
       call volint (1.e-6*aref**3*src_aux(:,is)/srcnorm, aux_power(is))  ! units of MW
       call volint (aref**3*src_alph(:,is), alph_power(is))
       call volint (aref**3*src_radiate(:,is), radiate_power(is))
    end do
    do is = 1, nspec
       call volint (aref**3*src_dens(:,is)/nsrcnorm, dens_input(is)) ! units of 10^20/s
    end do

    ! integrated power in units of MW
    do is = 1, nspec
       do ix = 1, nrad
!          call volint (aref**3*(1.e-6*src_pres(:ix,is)/srcnorm), int_power(ix,is))
          call volint (aref**3*(1.e-6*src_pres(:ix,is)/srcnorm), int_power(ix,is))
          !call volint (5.17655e-2*sqrt(m_ion(1))*vtfac**1.5*src_pres(:ix,is), int_power(ix,is))
       end do
       call get_cc (int_power(:,is), int_power_cc(:,is))
    end do
    ! integrated particle source in 10**20/s
    do is = 1, nspec
       do ix = 1, nrad
          call volint (aref**3*(src_dens(:ix,is)/nsrcnorm), int_pflux(ix,is))
       end do
       call get_cc (int_pflux(:,is), int_pflux_cc(:,is))
    end do

    int_equil = 0. ; int_equil_cc = 0.
    ! get integrated temp equil term in MW
    if (temp_equil) then
       call get_nueq (dens_grid, temp_grid, nu_grid, nueq)
       do is = 1, nspec
          do ix = 1, nrad
             ! sum over equilibration rates with each species
             do iu = 1, nspec
                if (is == iu) cycle
                ! factor of 1.5 below takes into account factor of 3/2 appearing
                ! in equation for energy density evolution (i.e. 3/2 dp/dt)
                call volint (aref**3*1.e-6*dens_grid(:ix,is)/srcnorm &
                     *1.5*nueq(:ix,is,iu)*(temp_grid(:ix,iu)-temp_grid(:ix,is)), &
                     dum)
                int_equil(ix,is) = int_equil(ix,is) + dum
             end do
          end do
       end do
       do is = 1, nspec
          call get_cc (int_equil(:,is), int_equil_cc(:,is))
       end do
    end if

    ! integrated momentum flux in ...
    do ix = 1, nrad
       call volint (aref**3*(src_mom(:ix)/lsrcnorm), int_lflux(ix))
    end do
    call get_cc (int_lflux, int_lflux_cc)

    ! net power in units of MW
    net_power = aux_power + alph_power - radiate_power

    if (proc0) then
       if (write_pwr_profs) then
          write (nspec_str,'(i3)') 7+nspec*7
          str = trim('('//trim(nspec_str)//'(1x,1pg15.8))')
          do ix = 1, nrad
             write (src_unit,str) time, rad_grid(ix), 1.e-6*src_aux(ix,:)/srcnorm, &
                  src_alph(ix,:), src_radiate(ix,:), &
                  sum(net_power), sum(aux_power), sum(alph_power), &
                  sum(radiate_power), src_dens(ix,:)/nsrcnorm, &
                  int_power(ix,:), int_equil(ix,:), int_pflux(ix,:), int_lflux(ix)
          end do
       else
          write (src_unit,*) time, sum(net_power), sum(aux_power), sum(alph_power), sum(radiate_power), sum(dens_input)
       end if
       call flush_output_file (src_unit)
    end if

!    write (nspec_str,'(i3)') 7+nspec*7
!    str = trim(trim(nspec_str)//'(1x,1pg15.8)')
!101 format (str)
!101 format (21(1x,1pg15.8))
    deallocate (dens_input)
    deallocate (nueq)

  end subroutine get_sources

  subroutine get_nu

    use interp, only: get_cc
    use trinity_input, only: vtfac
    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: z_ion, m_ion
    use trinity_input, only: is_ref

    implicit none

    integer :: is

    if (get_nu_first) then
       ! nufac(1) is prefactor for nu_ee
       nufac(1) = 3.95e-2*aref*sqrt(m_ion(1)/vtfac)
       ! nufac(2:) is prefactor for nu_ii
       do is = 1, n_ion_spec
          nufac(is+1) = 9.21e-4*aref*z_ion(is)**4*sqrt(m_ion(1)/(m_ion(is)*vtfac))
       end do
       get_nu_first = .false.
    end if

    ! this is nu_ss * (a / v_th,i)
    do is = 1, nspec
       nu_grid(:,is) = nufac(is)*loglamfnc(is,is,dens_grid,temp_grid) &
            *dens_grid(:,is)/(sqrt(temp_grid(:,is_ref))*temp_grid(:,is)**1.5)
       ! get cell-centered values
       call get_cc (nu_grid(:,is), nu_cc(:,is))
    end do

  end subroutine get_nu

  subroutine get_nueq (dens, temp, nu, nueq)
    
    use constants_trin, only: pi, mp_over_me
    use trinity_input, only: is_ref, n_ion_spec, nspec
    use trinity_input, only: charge, m_ratio
    use trinity_input, only: te_equal_ti

    implicit none

    integer :: is, iu

    real, dimension (:,:), intent (in) :: dens, temp, nu
    real, dimension (:,:,:), intent (out) :: nueq

    nueq = 0.0

    ! if Te assumed equal to Ti, then also assume all ion temps the same
    ! so no need to calculate collisional equilibration
    if (.not. te_equal_ti) then

       do is = 1, nspec
          do iu = 1, nspec
             if (is == iu) cycle ! no equilibration of species with itself
             ! this is the inter-species collisional equilibration rate 
             ! without pre-factor of 3/2 from pressure evolution equation
             nueq(:,is,iu) = (8./3.)*nu(:,is)*sqrt(temp(:,is_ref)*m_ratio(is,iu)/pi) &
                  * (dens(:,iu)/dens(:,is)) * (charge(iu)/charge(is))**2  / rhostar**2 &
                  * loglamfnc(is,iu,dens,temp) / loglamfnc(is,is,dens,temp) &
                  / (m_ratio(is,iu) + temp(:,iu)/temp(:,is))**1.5
          end do
       end do
          
    end if

  end subroutine get_nueq

  subroutine get_deq_dn (is, isp, dens, temp, &
       tratio, nu, deqdn)

    use trinity_input, only: nspec, z_ion, n_ion_spec
    use trinity_input, only: m_ratio, charge

    implicit none

    integer, intent (in) :: is, isp
    real, dimension (:), intent (in) :: dens, temp
    real, dimension (:), intent (in) :: tratio, nu
    real, intent (out) :: deqdn

    integer :: iu

    deqdn = 0.

    if (isp == is) then

       do iu = 1, nspec
          deqdn = deqdn + nu(iu)*temp(iu) &
               * (2.5 + 1.5*( (1.-tratio(iu))/(m_ratio(is,iu)+tratio(iu)) &
               - 1./tratio(iu) ))
       end do
       deqdn = deqdn + nu(nspec)*temp(nspec) &
            * dens(is)/dens(nspec) * (1.5*(tratio(nspec)-1.) &
            / (m_ratio(is,nspec)+tratio(nspec)) &
            - 1./tratio(nspec)) * (-charge(is)/charge(nspec))

    else
       deqdn = deqdn + nu(isp)*temp(isp) &
            *dens(is)/dens(isp) * (1.5*(tratio(isp)-1.)/(m_ratio(is,isp)+tratio(isp)) &
            - 1./tratio(isp))
    end if

    if (is == 1) then
       if (isp == is) then
          do iu = 2, nspec
             ! contribution from d(loglam_ei)/dn
             deqdn = deqdn - 1.5*nu(iu)*temp(is) &
                  * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp)
          end do
       end if
    else
       ! contribution from d(loglam_ie)/dn
       if (isp == 1) then
          iu = 1
          deqdn = deqdn - 1.5*nu(iu)*dens(is)*temp(is)/dens(1) &
               * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp)
       ! contribution from d(loglam_ij)/dn
       else if (isp == is) then
          do iu = 2, nspec
             deqdn = deqdn - nu(iu)*dens(is)*temp(is) &
                  * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp) &
                  * (z_ion(is-1)**2/(temp(is)*(dens(is)*z_ion(is-1)**2/temp(is) &
                  + dens(iu)*z_ion(iu-1)**2/temp(iu))) &
                  + m_ratio(is,iu)/(dens(is)*(m_ratio(is,iu)+tratio(iu))))
          end do
       else
          iu = isp
          deqdn = deqdn - nu(iu)*dens(is)*temp(is) &
               * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp) &
               * (z_ion(iu-1)**2/(temp(iu)*(dens(is)*z_ion(is-1)**2/temp(is) &
               + dens(iu)*z_ion(iu-1)**2/temp(iu))) &
               + tratio(iu)/(dens(iu)*(tratio(iu)+m_ratio(is,iu))))
       end if
       ! contribution from d(loglam_ii)/dn, taking into
       ! account constraint on final ion density due to 
       ! quasineutrality
       iu = nspec
       deqdn = deqdn + charge(isp)/charge(nspec) &
            * nu(iu)*dens(is)*temp(is) &
            * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp) &
            * (z_ion(iu-1)**2/(temp(iu)*(dens(is)*z_ion(is-1)**2/temp(is) &
            + dens(iu)*z_ion(iu-1)**2/temp(iu))) &
            + tratio(iu)/(dens(iu)*(tratio(iu)+m_ratio(is,iu))))
    end if

  end subroutine get_deq_dn

  subroutine get_deq_dp (is, isp, dens, temp, pres, &
       tratio, nu, deqdp)

    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: equal_ion_temps
    use trinity_input, only: m_ratio, charge

    implicit none

    integer, intent (in) :: is, isp
    
    real, dimension (:), intent (in) :: dens, temp, pres
    real, dimension (:), intent (in) :: tratio, nu
    real, intent (out) :: deqdp

    integer :: iu

    deqdp = 0.

    ! deq_dp is the part of dS/dp coming from collisional temp. equilibration             
    if (isp == is) then
       do iu = 1, nspec
          deqdp = deqdp + nu(iu) &
               * (-1.0+1.5*(tratio(iu)-1.0)*(tratio(iu)/(m_ratio(is,iu)+tratio(iu)) &
               - 1.0))
       end do
    else
       deqdp = deqdp + nu(isp)*dens(is)/dens(isp) &
            * (1.0 - 1.5*(tratio(isp)-1.0)/(m_ratio(is,isp)+tratio(isp)))
    end if

    ! take into account implicit dependence of minority ion temperature
    ! on primary ion temperature when forcing all ion temperatures to be equal
    if (n_ion_spec > 1 .and. equal_ion_temps .and. isp == 2) then
       do iu = 3, nspec
          deqdp = deqdp + nu(iu)*dens(is)/dens(isp)
       end do
    end if

    ! contributions from d(loglam)/dp

    ! first get contribution to d(loglam_ei)/dp electrons
    if (is == 1) then
       if (isp == is) then
          do iu = 2, nspec
             ! contribution from d(loglam_ei)/dp
             deqdp = deqdp + nu(iu) &
                  * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp)
          end do
       end if
    else
       ! contribution from d(loglam_ie)/dp to ions
       if (isp == 1) then
          iu = 1
          deqdp = deqdp + nu(iu)*pres(is)/pres(1) &
                              * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp)
       ! contribution from d(loglam_ij)/dn to ions
       else if (isp == is) then
          do iu = 2, nspec
             deqdp = deqdp + nu(iu) &
                  * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp) &
                  * ( tratio(iu)*0.5 &
                  / (tratio(iu)+dens(iu)/dens(is)*(charge(iu)/charge(is))**2) &
                  + m_ratio(is,iu)/(tratio(iu)+m_ratio(is,iu)) )
          end do
       else
          iu = isp
!          deqdp = deqdp + nu(iu)*pres(is) &
          deqdp = deqdp + nu(iu) &
               * (tratio(iu)-1.0) / loglamfnc(is,iu,dens,temp) &
               * ( 0.5/(tratio(iu)*((charge(is)/charge(iu))**2*tratio(iu) + dens(iu)/dens(is))) &
               + dens(is)/(dens(iu)*(tratio(iu)+m_ratio(is,iu))) )
       end if
    end if

  end subroutine get_deq_dp

  subroutine allocate_arrays

    use trinity_input, only: nrad, ncc
    use trinity_input, only: nspec
    use trinity_input, only: nprofs_evolved

    implicit none

    call allocate_nteqns_arrays

    call allocate_norms

    ! derivatives of fluxes with respect to gradients (cell centers)
    ! max number of dependences for fluxes is
    ! nprofs_evolved for local gradients
    ! and nprofs_evolved for local values themselves
    allocate (dpflxdg(ncc,nspec,2*nprofs_evolved)) ; dpflxdg = 0.
    allocate (dqflxdg(ncc,nspec,2*nprofs_evolved)) ; dqflxdg = 0.
    allocate (dheatdg(ncc,nspec,2*nprofs_evolved)) ; dheatdg = 0.
    allocate (dlflxdg(ncc,2*nprofs_evolved)) ; dlflxdg = 0.

    ! derivatives of fluxes with respect to gradients (grid points)
    allocate (dheat_grid(nrad,nspec,2*nprofs_evolved)) ; dheat_grid = 0.

    ! coefficients used in calculating transport Jacobian
    allocate (A_fac(3)) ; A_fac(1) = -0.5 ; A_fac(2) = -1.0 ; A_fac(3) = -1.5
    allocate (B_fac(3)) ; B_fac(1) = 1.5 ; B_fac(2) = 2.0 ; B_fac(3) = 2.5

  end subroutine allocate_arrays

  subroutine deallocate_arrays

    implicit none

    call deallocate_nteqns_arrays

    call deallocate_norms

    ! derivatives of fluxes with respect to gradients (cell centers)
    deallocate(dpflxdg)
    deallocate(dqflxdg)
    deallocate(dheatdg)
    deallocate(dlflxdg)

    ! derivatives of fluxes with respect to gradients (grid points)
!    deallocate(dpflx_grid)
!    deallocate(dqflx_grid)
    deallocate(dheat_grid)
!    deallocate(dlflx_grid)

!
    ! coefficients used in calculating transport Jacobian
    deallocate(A_fac)
    deallocate(B_fac)

  end subroutine deallocate_arrays

  subroutine finish_nteqns
    call deallocate_arrays
  end subroutine finish_nteqns

!  subroutine reset_nteqns
!  end subroutine 

  subroutine get_grads (dens, pres, mom)

    use interp, only: fd5pt, get_cc
    use trinity_input, only: nrad
    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: is_ref

    implicit none

    integer :: is
    real :: betafac
    real, dimension (:,:), intent (in) :: dens, pres
    real, dimension (:), intent (in) :: mom

    do is = 1, nspec
       ! get derivatives of mean densities and pressures at grid points
       call fd5pt (dens(:,is), fprim_grid(:,is), drad)
       call fd5pt (pres(:,is), pprim_grid(:,is), drad)
    end do

    ! these are -1/n dn/drho, etc. at grid points
    fprim_grid = -fprim_grid/dens
    pprim_grid = -pprim_grid/pres
    tprim_grid = pprim_grid - fprim_grid
    
    do is = 1, nspec
       call get_cc (fprim_grid(:,is), fprim_cc(:,is))
       call get_cc (pprim_grid(:,is), pprim_cc(:,is))
       call get_cc (tprim_grid(:,is), tprim_cc(:,is))
    end do

    ! convert to omega from toroidal angular momentum
    ! normalized omega is omega*aref/vtref
    omega_grid = mom*inv_mominertia_fnc(dens,rmajor_grid)
    call get_cc (omega_grid, omega_cc)

    ! get domega/drho
    call fd5pt (omega_grid, gexb_grid, drad)

    ! gamma_{ExB} = (rho/q)(domega/drho) (a/v_{t,0})
    gexb_grid = (rad_grid/qval_grid)*gexb_grid
    call get_cc (gexb_grid, gexb_cc)

!    if (get_grads_first) then
       betafac = 403.e-4
       ! need to understand how Btor enters the GS2 calculation -- MAB
       ! is bmag_grid below total |B| or |Btor|?  should be |Btor|, but
       ! not sure if it actually is
       ! beta_cc is the gs2 reference beta (8*pi*p_ref / Ba**2)
       ! need to see how this should change for gene
       beta_grid = pres(:,is_ref)/bmag_grid**2 * betafac
       call get_cc (beta_grid, beta_cc)
!       get_grads_first = .false.
!    end if

    betaprim_grid = -beta_grid/pres(:,is_ref) * sum(pprim_grid*pres,2)
    call get_cc (betaprim_grid, betaprim_cc)
       
  end subroutine get_grads


  subroutine get_flux_grads (pflx, qflx, heat, lflx)

    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use interp, only: get_gridval

    implicit none

    real, dimension (:,:,:), intent (in) :: pflx, qflx, heat
    real, dimension (:,:), intent (in) :: lflx

    integer :: is, igr, idx

    idx = 1

    if (evolve_density) then
       do is = 1, n_ion_spec
          call set_dflxdg (idx, pflx, qflx, heat, lflx)
          ! increment idx to keep track of which index of the flux arrays
          ! should be accessed next
          idx = idx + 1
       end do
    end if
      
    if (evolve_temperature) then
       if (.not. te_equal_ti .and. .not. te_fixed) then
          call set_dflxdg (idx, pflx, qflx, heat, lflx)
          ! increment idx to keep track of which index of the flux arrays
          ! should be accessed next
          idx = idx + 1
       end if
       call set_dflxdg (idx, pflx, qflx, heat, lflx)
       ! increment idx to keep track of which index of the flux arrays
       ! should be accessed next
       idx = idx + 1
       if (n_ion_spec > 1 .and. .not.equal_ion_temps) then
          do is = 3, nspec
             call set_dflxdg (idx, pflx, qflx, heat, lflx)
             ! increment idx to keep track of which index of the flux arrays
             ! should be accessed next
             idx = idx + 1
          end do
       end if
    end if

    if (evolve_flow) then
       call set_dflxdg (idx, pflx, qflx, heat, lflx)
       ! increment idx to keep track of which index of the flux arrays
       ! should be accessed next
       idx = idx + 1
    end if

    do igr = 1, size(dheatdg,3)
       do is = 1, size(dheatdg,2)
          call get_gridval (dheatdg(:,is,igr), dheat_grid(:,is,igr), dheatdg(1,is,igr))
       end do
    end do

  end subroutine get_flux_grads

  subroutine get_nt_coefs (pflx, qflx, heat, lflx, jacob, nt_i, &
       time, itstep, nt_m, ntold, fluxes_m, heat_m)

    use trinity_input, only: nrad, ntdelt, impfac
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: nprofs_evolved

    implicit none

    integer :: ix, base

    real, dimension (:), allocatable :: ntsub

    integer, intent (in) :: itstep
    real, intent (in) :: time
    real, dimension (:), intent (in) :: lflx
    real, dimension (:,:), intent (in) :: pflx, qflx, heat, heat_m
    real, dimension (:), intent (in) :: nt_m, ntold, fluxes_m
    real, dimension (:), intent (in out) :: nt_i
    real, dimension (:,:), intent (out) :: jacob

    allocate (ntsub(nprofs_evolved*nrad))

    ! initialize Jacobian as identity
    jacob = 0.0
    do ix = 1, nprofs_evolved*nrad
       jacob(ix,ix) = 1.0
    end do

    ntsub = nt_i

    ! bdfac is Delta^{1} * dt^{m} from trin_norm_upadate
    ! bdfac = 1.0 corresponds to single-step
    ! bdfac =/= 1.0 corresponds to multi-step time difference
    if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
       bdfac = (2.*ntdelt + ntdelt_old)/(ntdelt+ntdelt_old)
    else
       bdfac = 1.0
    end if

    ! index offset for accessing elements of jacobian
    ! will be modified in calls to get_ncoefs, etc.
    base = 0
    if (evolve_density) call get_ncoefs (base, pflx, jacob, nt_i, ntsub, &
         time, itstep, nt_m, ntold, fluxes_m)
    if (evolve_temperature) call get_pcoefs (base, qflx, heat, jacob, nt_i, ntsub, &
         time, itstep, nt_m, ntold, fluxes_m, heat_m)
    if (evolve_flow) call get_lcoefs (base, lflx, jacob, nt_i, ntsub, &
         time, itstep, nt_m, ntold, fluxes_m)

    deallocate (ntsub)

  end subroutine get_nt_coefs

  subroutine broadcast_profiles

    use mp_trin, only: broadcast
    use trinity_input, only: nspec
    use trinity_input, only: evolve_geometry

    implicit none

    integer :: is

    if (broadcast_profiles_first .or. evolve_geometry) then
       call broadcast (rad_ms)
       call broadcast (rmajor_ms)
       call broadcast (shift_ms)
       call broadcast (rmin_ms)
       call broadcast (drmindrho_ms)
       call broadcast (qval_ms)
       call broadcast (shat_ms)
       call broadcast (kappa_ms)
       call broadcast (kapprim_ms)
       call broadcast (delta_ms)
       call broadcast (deltprim_ms)
       call broadcast (rhotor_ms)
       call broadcast (area_ms)
       call broadcast (grho_ms)
       call broadcast (psipol_ms)
       call broadcast (dpsipoldrho_ms)
       call broadcast (rhotor_ms)
       call broadcast (drhotordrho_ms)
       call broadcast (rhopol_ms)
       call broadcast (drhopoldrho_ms)
       call broadcast (btori_ms)
       call broadcast (bunit_ms)
       call broadcast (bgeo_ms)
       call broadcast (bmag_ms)
       call broadcast (bmag_old_ms)
       call broadcast (zeff_ms)
       broadcast_profiles_first = .false.
    end if

    call broadcast (omega_ms)
    call broadcast (gexb_ms)
    call broadcast (beta_ms)
    call broadcast (shift_ms)
    call broadcast (betaprim_ms)
    
    do is = 1, nspec
       call broadcast (dens_ms(:,is))
       call broadcast (temp_ms(:,is))
       call broadcast (pres_ms(:,is))
       call broadcast (fprim_ms(:,is))
       call broadcast (tprim_ms(:,is))
       call broadcast (nu_ms(:,is))
    end do

  end subroutine broadcast_profiles

  subroutine get_Hm (itstep, nt_i, nt_m, nt_mm1, flx, flx_m, heat, heat_m, H_m)

    use trinity_input, only: ntdelt, impfac
    use trinity_input, only: nrad, ncc
    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: m_ion, is_ref
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, temp_equil, te_fixed
    use interp, only: get_cc

    implicit none

    integer, intent (in) :: itstep
    real, dimension (:), intent (in) :: nt_i, nt_m, nt_mm1
    real, dimension (:), intent (in) :: flx, flx_m
    real, dimension (:,:), intent (in) :: heat, heat_m
    real, dimension (:), intent (out) :: H_m

    integer :: base, is, is_low, is_up
    real :: d0, dm1, bdfac, fac, fac_m
    real, dimension (:), allocatable :: G_fac
    real, dimension (:,:), allocatable :: dens, dens_m
    real, dimension (:,:), allocatable :: pres, pres_m
    real, dimension (:,:), allocatable :: temp, temp_m
    real, dimension (:), allocatable :: mom, mom_m
    real, dimension (:,:), allocatable :: denscc, denscc_m
    real, dimension (:,:), allocatable :: prescc, prescc_m
    real, dimension (:), allocatable :: momcc, momcc_m
    real, dimension (:,:), allocatable :: pflx, pflx_m
    real, dimension (:,:), allocatable :: qflx, qflx_m
    real, dimension (:), allocatable :: lflx, lflx_m
    real, dimension (:,:), allocatable :: capf, capf_m
    real, dimension (:,:), allocatable :: nu_grid_m
    real, dimension (:,:,:), allocatable :: nueq, nueq_m

    allocate (G_fac(nrad))
    allocate (dens(nrad,nspec), dens_m(nrad,nspec))
    allocate (pres(nrad,nspec), pres_m(nrad,nspec))
    allocate (temp(nrad,nspec), temp_m(nrad,nspec))
    allocate (mom(nrad), mom_m(nrad))
    allocate (denscc(ncc,nspec), denscc_m(ncc,nspec))
    allocate (prescc(ncc,nspec), prescc_m(ncc,nspec))
    allocate (momcc(ncc), momcc_m(ncc))
    allocate (pflx(nrad,nspec), pflx_m(nrad,nspec))
    allocate (qflx(nrad,nspec), qflx_m(nrad,nspec))
    allocate (lflx(nrad), lflx_m(nrad))
    allocate (capf(nrad,nspec), capf_m(nrad,nspec))

    ! bdfac is Delta^{1} * dt^{m} from trin_norm_upadate
    ! bdfac = 1.0 corresponds to single-step
    ! bdfac =/= 1.0 corresponds to multi-step time difference
    if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
       bdfac = (2.*ntdelt + ntdelt_old)/(ntdelt+ntdelt_old)
    else
       bdfac = 1.0
    end if

    ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
    ! 'Discretization' section of trin_norm_update.
    d0 = -(ntdelt + ntdelt_old)/ntdelt_old
    dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )

    G_fac = grho_grid/(area_grid*drad)

    call get_profs_from_nt_vector (nt_i, dens, pres, mom)
    call get_profs_from_nt_vector (nt_m, dens_m, pres_m, mom_m)
    do is = 1, nspec
       call get_cc (dens(:,is), denscc(:,is))
       call get_cc (pres(:,is), prescc(:,is))
       call get_cc (mom, momcc)
       call get_cc (dens_m(:,is), denscc_m(:,is))
       call get_cc (pres_m(:,is), prescc_m(:,is))
       call get_cc (mom_m, momcc_m)
    end do

    call get_fluxes_from_flx_vector (flx, pflx, qflx, lflx)
    call get_fluxes_from_flx_vector (flx_m, pflx_m, qflx_m, lflx_m)

    H_m = 0.0

    base = 0

    if (evolve_density) then

       capf = spread(area_cc*prescc(:,is_ref)**1.5/(sqrt(denscc(:,is_ref))*bmag_cc**2),2,nspec)*pflx
       capf_m = spread(area_cc*prescc_m(:,is_ref)**1.5/(sqrt(denscc_m(:,is_ref))*bmag_cc**2),2,nspec)*pflx_m

       do is = 1, n_ion_spec
          if (abs(bdfac-1.) > epsilon(0.)) then
             ix = 1
             H_m(base+ix) = -( -nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_dens(ix,is) + G_fac(ix)*(-capf(ix,is)) ) )
             do ix = 2, nrad-1
                H_m(base+ix) = -(-nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
                     + ntdelt*( src_dens(ix,is) + G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) ))
             end do
          else
             ix = 1
             H_m(base+ix) = -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_dens(ix,is) + impfac*( G_fac(ix)*(-capf(ix,is)) ) &
                  + (1.0-impfac)*( -G_fac(ix)*capf_m(ix,is) )))
             do ix = 2, nrad-1
                H_m(base+ix) =  -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
                     + ntdelt*( src_dens(ix,is) + impfac*( G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) ) &
                     + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)) )))
             end do
          end if
          base = base + nrad
       end do

    end if

    if (evolve_temperature) then

       if (te_equal_ti .or. te_fixed) then
          is_low = 2 ; is_up = 2
       else if (equal_ion_temps) then
          is_low = 1 ; is_up = 2
       else
          is_low = 1 ; is_up = nspec
       end if

       capf = spread(area_cc*prescc(:,is_ref)**2.5 &
            /(1.5*denscc(:,is_ref)**1.5*bmag_cc**2),2,nspec)*qflx
       capf_m = spread(area_cc*prescc_m(:,is_ref)**2.5 &
            /(1.5*denscc_m(:,is_ref)**1.5*bmag_cc**2),2,nspec)*qflx_m

       allocate (nu_grid_m(nrad,nspec)) ; nu_grid_m = 0.0
       allocate (nueq(nrad,nspec,nspec)) ; nueq = 0.0
       allocate (nueq_m(nrad,nspec,nspec)) ; nueq_m = 0.0

       call get_nu

       if (temp_equil) then
          do is = 1, nspec
             nu_grid_m(:,is) = nufac(is)*loglamfnc(is,is,dens_m,temp_m) &
                  *dens_m(:,is)/(sqrt(temp_m(:,is_ref))*temp_m(:,is)**1.5)
          end do
          
          call get_nueq (dens, temp, nu_grid, nueq)
          call get_nueq (dens_m, temp_m, nu_grid_m, nueq_m)
       else
          nueq = 0.0
          nueq_m = 0.0
       end if

       do is = is_low, is_up
          if (abs(bdfac-1.) > epsilon(0.)) then
             ix = 1
             fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)

             H_m(base+ix) = -( -nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_pres(ix,is)/1.5 + G_fac(ix)*(-capf(ix,is)) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is) ) )
             
             do ix = 2, nrad-1
                fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)

                H_m(base+ix) = -(-nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
                     + ntdelt*( src_pres(ix,is)/1.5 + G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                     + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                     + fac*heat(ix,is) ) )
             end do
          else
             ix = 1
             fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)
             fac_m = pres_m(ix,is_ref)**2.5/(1.5*dens_m(ix,is_ref)**1.5*bmag_grid(ix)**2)

             H_m(base+ix) = -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_pres(ix,is)/1.5 + impfac*( G_fac(ix)*(-capf(ix,is)) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is) ) &
                  + (1.0-impfac)*( -G_fac(ix)*capf_m(ix,is) &
                  + dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))) &
                  + fac_m*heat_m(ix,is) ) ))

             do ix = 2, nrad-1
                fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)
                fac_m = pres_m(ix,is_ref)**2.5/(1.5*dens_m(ix,is_ref)**1.5*bmag_grid(ix)**2)

                H_m(base+ix) = -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
                     + ntdelt*( src_pres(ix,is)/1.5 + impfac*( G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                     + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                     + fac*heat(ix,is) ) &
                     + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)) &
                     + fac_m*heat_m(ix,is) ) ))
             end do
             
          end if
          base = base + nrad
       end do

       deallocate (nu_grid_m)
       deallocate (nueq, nueq_m)

    end if

    if (evolve_flow) then
       capf(:,1) = area_cc*prescc(:,is_ref)**2*m_ion(1)/(denscc(:,is_ref)*bmag_cc**2)*lflx
       capf_m(:,1) = area_cc*prescc(:,is_ref)**2*m_ion(1)/(denscc(:,is_ref)*bmag_cc**2)*lflx_m
       if (abs(bdfac-1.) > epsilon(0.)) then
          ix = 1
          H_m(base+ix) = -( -nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
               + ntdelt*( src_mom(ix) - G_fac(ix)*capf(ix,1) ))
          do ix = 2, nrad-1
             H_m(base+ix) = -( -nt_m(base+ix)*d0 - nt_mm1(base+ix)*dm1 - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_mom(ix) + G_fac(ix)*(capf(ix-1,1)-capf(ix,1)) ) )
          end do
       else
          ix = 1
          H_m(base+ix) = -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
               + ntdelt*( src_mom(ix) + impfac*( G_fac(ix)*(-capf(ix,1)) ) &
               + (1.0-impfac)*( -G_fac(ix)*capf_m(ix,1) ) ) )
          do ix = 2, nrad-1
             H_m(base+ix) = -(nt_m(base+ix) - nt_i(base+ix)*bdfac &
                  + ntdelt*( src_mom(ix) + impfac*( G_fac(ix)*(capf(ix-1,1)-capf(ix,1)) ) &
                  + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,1)-capf_m(ix,1)) ) ))
          end do
       end if
    end if

    deallocate (G_fac)
    deallocate (dens, dens_m)
    deallocate (temp, temp_m)
    deallocate (pres, pres_m)
    deallocate (mom, mom_m)
    deallocate (denscc, denscc_m)
    deallocate (prescc, prescc_m)
    deallocate (momcc, momcc_m)
    deallocate (pflx, pflx_m)
    deallocate (qflx, qflx_m)
    deallocate (lflx, lflx_m)
    deallocate (capf, capf_m)

  end subroutine get_Hm

  ! subroutine get_jacob (nt_vec_in, nt_m)

  !   use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
  !   use trinity_input, only: te_equal_ti, equal_ion_temps
  !   use trinity_input, only: nspec
  !   use trinity_input, only: nrad
    
  !   implicit none

  !   real, dimension (:), intent (in) :: nt_vec_in, nt_m

  !   integer :: base

  !   real, dimension (:,:), allocatable :: psi_n, psi_p
  !   real, dimension (:), allocatable :: psi_l
  !   real, dimension (:,:), allocatable, save :: cm_coefs, cp_coefs
  !   real, dimension (:,:), allocatable, save :: dm_coefs, dp_coefs
  !   real, dimension (:,:), allocatable :: dens, dens_m
  !   real, dimension (:,:), allocatable :: pres, pres_m
  !   real, dimension (:,:), allocatable :: temp, temp_m
  !   real, dimension (:), allocatable :: mom, mom_m

  !   base = 0

  !   ! these are the coefficients multiplying the pressure at the m+1,i+1 time level
  !   ! for more details, see the 'Discretization' section of trin_norm_update.pdf
  !   allocate (psi_n(-2:3,nspec-1))
  !   allocate (psi_p(-2:3,nspec))
  !   allocate (psi_l(-2:3)) ; psi_l = 0.0

  !   ! these are the 'c' and 'd' coefficients used in calculating the 'psi' coefficients
  !   ! found in the 'Discretization' section of trin_norm_update.pdf
  !   if (.not. allocated(cm_coefs)) then
  !      allocate (cm_coefs(-2:3,nrad-1)) ; cm_coefs = 0.0
  !      allocate (cp_coefs(-2:3,nrad-1)) ; cp_coefs = 0.0
  !      allocate (dm_coefs(-2:3,nrad-1)) ; dm_coefs = 0.0
  !      allocate (dp_coefs(-2:3,nrad-1)) ; dp_coefs = 0.0

  !      ! corresponds with table 1 of trin_norm_update.pdf
  !      cp_coefs(-1,2:nrad-2) = -0.0625 ; cp_coefs(-1,nrad-1) = -0.125
  !      cp_coefs(0,1) = 0.375 ; cp_coefs(0,2:nrad-2) = 0.5625 ; cp_coefs(0,nrad-1) = 0.75
  !      cp_coefs(1,1) = 0.75 ; cp_coefs(1,2:nrad-2) = 0.5625 ; cp_coefs(1,nrad-1) = 0.375
  !      cp_coefs(2,1) = -0.125 ; cp_coefs(2,2:nrad-2) = -0.0625

  !      cm_coefs(-2,3:nrad-1) = -0.0625
  !      cm_coefs(-1,2) = 0.375 ; cm_coefs(-1,3:) = 0.5625
  !      cm_coefs(0,2) = 0.75 ; cm_coefs(0,3:) = 0.5625
  !      cm_coefs(1,2) = -0.125 ; cm_coefs(1,3:) = -0.0625

  !      ! corresponds with table 2 of trin_norm_update.pdf
  !      dp_coefs(-2,nrad-1) = 1./24.
  !      dp_coefs(-1,2:nrad-2) = 1./24. ; dp_coefs(-1,nrad-1) = -0.125
  !      dp_coefs(0,1) = -23./24. ; dp_coefs(0,2:nrad-2) = -1.125 ; dp_coefs(0,nrad-1) = -0.875
  !      dp_coefs(1,1) = 0.875 ; dp_coefs(1,2:nrad-2) = 1.125 ; dp_coefs(1,nrad-1) = 23./24.
  !      dp_coefs(2,1) = 0.125 ; dp_coefs(2,2:nrad-2) = -1./24.
  !      dp_coefs(3,1) = -1./24.
       
  !      dm_coefs(-2,3:) = 1./24.
  !      dm_coefs(-1,2) = -23./24. ; dm_coefs(-1,3:) = -1.125
  !      dm_coefs(0,2) = 0.875 ; dm_coefs(0,3:) = 1.125
  !      dm_coefs(1,2) = 0.125 ; dm_coefs(1,3:) = -1./24.
  !      dm_coefs(2,2) = -1./24.
  !   end if

  !   ! for convenience, break nt vector into constituent elements
  !   ! (density, ion pressure, electron pressure, toroidal angular momentum)
  !   ! these are the values at the time level m+1, iteration i
  !   call get_profs_from_nt_vector (nt_vec_in, dens, pres, mom)
  !   call get_profs_from_nt_vector (nt_m, dens_m, pres_m, mom_m)

  !   ! ion and electron pressure are derived quantities
  !   temp = pres/dens
  !   temp_m = pres_m/dens_m

  !   Gam = inv_mominertia_fnc(dens,rmajor_grid)

  !   if (evolve_density) then

  !      do is = 1, nspec-1
  !         idx = 1
  !         ix = 1
          
  !         llim = 1 ; ulim = 4
  !         l = 0 ; u = 3

  !         psi_n = 0.
  !         ! note that density is being evolved or would not have
  !         ! made it into this subroutine
  !         do isp = 1, nspec-1
  !            psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
  !                 dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                 cp_coefs(:,ix),dp_coefs(:,ix),A_fac(1),capf(ix,is),acapf(ix),is_ref_dens(isp)) )
             
  !            jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
  !            idx = idx + 1
  !         end do

  !         psi_p = 0.
  !         if (evolve_temperature) then
  !            do isp = is_low, is_up
  !               psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
  !                    dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                    cp_coefs(:,ix),dp_coefs(:,ix),B_fac(1),capf(ix,is),acapf(ix),is_ref_temp(isp)) )
  !               jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
  !               idx = idx + 1
  !            end do

  !         end if

  !         psi_l = 0.
  !         if (evolve_flow) then
  !            psi_l = G_fac(ix)* ( psi_l_fnc(dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                 cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
  !                 rad_cc(ix)/qval_cc(ix),acapf(ix)) )
  !            jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
             
  !            idx = idx + 1
  !         end if

  !         ! extra bdfac factor for diagonal
  !         jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)

  !         do ix = 2, nrad-1
             
  !            idx = 1
             
  !            ! get upper and lower limits of stencil
  !            ! these limits must change near boundaries
  !            llim = max(ix-2,1) ; ulim = min(ix+3,nrad)
  !            l = max(-2,llim-ix) ; u = min(3,ulim-ix)
             
  !            psi_n = 0.
  !            do isp = 1, n_ion_spec
  !               psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
  !                    dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                    cp_coefs(:,ix),dp_coefs(:,ix),A_fac(1),capf(ix,is),acapf(ix),is_ref_dens(isp)) &
  !                    - psi_nt_fnc(denscc(ix-1,isp),fprim_cc(ix-1,isp), &
  !                    dpflxdg(ix-1,is,idx),dpflxdg(ix-1,is,idx+np), &
  !                    cm_coefs(:,ix),dm_coefs(:,ix),A_fac(1),capf(ix-1,is),acapf(ix-1),is_ref_dens(isp)) )
                
  !               jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
  !               idx = idx + 1
  !            end do
             
  !            psi_p = 0.
  !            if (evolve_temperature) then
  !               do isp = is_low, is_up
  !                  psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
  !                       dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                       cp_coefs(:,ix),dp_coefs(:,ix),B_fac(1),capf(ix,is),acapf(ix),is_ref_temp(isp)) &
  !                       - psi_nt_fnc(prescc(ix-1,isp),pprim_cc(ix-1,isp), &
  !                       dpflxdg(ix-1,is,idx),dpflxdg(ix-1,is,idx+np), &
  !                       cm_coefs(:,ix),dm_coefs(:,ix),B_fac(1),capf(ix-1,is),acapf(ix-1),is_ref_temp(isp)) )
                   
  !                  jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) &
  !                       = ntdelt*impfac*psi_p(l:u,isp)
  !                  idx = idx + 1
  !               end do
  !            end if
             
  !            psi_l = 0.
  !            if (evolve_flow) then
  !               psi_l = G_fac(ix)* ( psi_l_fnc(dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
  !                    cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
  !                    rad_cc(ix)/qval_cc(ix),acapf(ix)) - psi_l_fnc(dpflxdg(ix-1,is,idx), &
  !                    dpflxdg(ix-1,is,idx+np),cm_coefs(:,ix),dm_coefs(:,ix),Gam(ix),Gamcc(ix-1), &
  !                    rad_cc(ix-1)/qval_cc(ix-1),acapf(ix-1)) )
                
  !               jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
  !               idx = idx + 1
  !            end if
             
  !            ! extra bdfac factor for diagonal
  !            jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)

  !         end do

  !         base = base + nrad

  !      end do

  !      deallocate (psi_n, psi_p, psi_l)

  !   end if

  !   if (evolve_temperature) then

  !   end if

  !   if (evolve_flow) then

  !   end if

  ! end subroutine get_jacob

  subroutine get_pcoefs (base, qflx, heat, jacob, &
       nt_out, nt_vec_in, time, itstep, nt_m, ntold, flx_m, heat_m)

    use constants_trin, only: mp_over_me
    use mp_trin, only: abort_mp
    use file_utils_trin, only: flush_output_file
    use trinity_input, only: nrad, rad_out, ntdelt, impfac
    use trinity_input, only: flux_option_switch, flux_option_test2
    use trinity_input, only: testing, dfac, temp_equil, turb_heat, ncc
    use trinity_input, only: write_balance
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: is_ref, is_ref_dens, is_ref_temp
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use interp, only: get_cc

    implicit none
    
    integer :: ix, llim, ulim, l, u, is
    integer :: is_low, is_up, isp, idx
    integer :: np
    real :: d0, dm1, fac, fac_m
    
    real, dimension (nrad) :: G_fac, Gam, mom, mom_m
    real, dimension (nrad,nspec) :: dens, pres, temp
    real, dimension (nrad,nspec) :: dens_m, pres_m, temp_m
    real, dimension (ncc) :: acapf, acapf_m
    real, dimension (ncc) :: Gamcc
    real, dimension (ncc,nspec) :: denscc, prescc
    real, dimension (ncc,nspec) :: denscc_m, prescc_m
    real, dimension (ncc,nspec) :: capf, capf_m, qflx_m
    real, dimension (:,:), allocatable :: psi_n, psi_p
    real, dimension (:), allocatable :: psi_l
    real, dimension (:), allocatable :: dheat_dn, dheat_dp, dheat_dL
    real, dimension (:), allocatable :: deq_dn, deq_dp
    real, dimension (:,:,:), allocatable :: nueq, nueq_m
    real, dimension (:,:), allocatable :: nu_grid_m
    real, dimension (:,:), allocatable :: t_ratio
    real, dimension (:,:), allocatable, save :: cm_coefs, cp_coefs
    real, dimension (:,:), allocatable, save :: dm_coefs, dp_coefs
    real, dimension (:,:), allocatable, save :: b_coefs
    real, dimension (:), allocatable, save :: djk

    integer, intent (in) :: itstep
    integer, intent (in out) :: base
    real, intent (in) :: time
    real, dimension (:,:), intent (in) :: qflx, heat, heat_m
    real, dimension (:), intent (in) :: nt_vec_in, nt_m, ntold, flx_m
    real, dimension (:), intent (in out) :: nt_out
    real, dimension (:,:), intent (in out) :: jacob

    ! these are the coefficients multiplying the pressure at the m+1,i+1 time level
    ! for more details, see the 'Discretization' section of trin_norm_update.pdf
    allocate (psi_n(-2:3,n_ion_spec))  ; psi_n = 0.0
    allocate (psi_p(-2:3,nspec)) ; psi_p = 0.0
    allocate (psi_l(-2:3))  ; psi_l = 0.0

    ! these are the contributions to dS/dn, dS/dp, and dS/dL
    ! coming from turbulent energy exchange (i.e. heating)
    allocate (dheat_dn(-2:3))
    allocate (dheat_dp(-2:3))
    allocate (dheat_dL(-2:3))

    ! these are the contributions to dS/dn, dS/dp, and dS/dL
    ! coming from collisional equilibration
    allocate (deq_dn(-2:3)) ; deq_dn = 0.0
    allocate (deq_dp(-2:3)) ; deq_dp = 0.0

    ! nu * a / v_tref evaluated at time level m
    allocate (nu_grid_m(nrad,nspec)) ; nu_grid_m = 0.0

    ! equilibration frequency (with 3/2 included) at current iteration
    allocate (nueq(nrad,nspec,nspec))
    ! equilibration frequency (with 3/2 included) at time level m
    allocate (nueq_m(nrad,nspec,nspec))

    ! temperature ratio needed for temperature equilibration part of Jacobian
    allocate (t_ratio(nrad,nspec))

    ! these are the 'c' and 'd' coefficients used in calculating the 'psi' coefficients
    ! found in the 'Discretization' section of trin_norm_update.pdf
    if (.not. allocated(cm_coefs)) then
       allocate (cm_coefs(-2:3,nrad-1)) ; cm_coefs = 0.0
       allocate (cp_coefs(-2:3,nrad-1)) ; cp_coefs = 0.0
       allocate (dm_coefs(-2:3,nrad-1)) ; dm_coefs = 0.0
       allocate (dp_coefs(-2:3,nrad-1)) ; dp_coefs = 0.0
       allocate (b_coefs(-2:3,nrad-1)) ; b_coefs = 0.0
       allocate (djk(-2:3)) ; djk = 0.0

       ! corresponds with table 1 of trin_norm_update.pdf
       cp_coefs(-1,2:nrad-2) = -0.0625 ; cp_coefs(-1,nrad-1) = -0.125
       cp_coefs(0,1) = 0.375 ; cp_coefs(0,2:nrad-2) = 0.5625 ; cp_coefs(0,nrad-1) = 0.75
       cp_coefs(1,1) = 0.75 ; cp_coefs(1,2:nrad-2) = 0.5625 ; cp_coefs(1,nrad-1) = 0.375
       cp_coefs(2,1) = -0.125 ; cp_coefs(2,2:nrad-2) = -0.0625

       cm_coefs(-2,3:nrad-1) = -0.0625
       cm_coefs(-1,2) = 0.375 ; cm_coefs(-1,3:) = 0.5625
       cm_coefs(0,2) = 0.75 ; cm_coefs(0,3:) = 0.5625
       cm_coefs(1,2) = -0.125 ; cm_coefs(1,3:) = -0.0625

       ! corresponds with table 2 of trin_norm_update.pdf
       dp_coefs(-2,nrad-1) = 1./24.
       dp_coefs(-1,2:nrad-2) = 1./24. ; dp_coefs(-1,nrad-1) = -0.125
       dp_coefs(0,1) = -23./24. ; dp_coefs(0,2:nrad-2) = -1.125 ; dp_coefs(0,nrad-1) = -0.875
       dp_coefs(1,1) = 0.875 ; dp_coefs(1,2:nrad-2) = 1.125 ; dp_coefs(1,nrad-1) = 23./24.
       dp_coefs(2,1) = 0.125 ; dp_coefs(2,2:nrad-2) = -1./24.
       dp_coefs(3,1) = -1./24.
       
       dm_coefs(-2,3:) = 1./24.
       dm_coefs(-1,2) = -23./24. ; dm_coefs(-1,3:) = -1.125
       dm_coefs(0,2) = 0.875 ; dm_coefs(0,3:) = 1.125
       dm_coefs(1,2) = 0.125 ; dm_coefs(1,3:) = -1./24.
       dm_coefs(2,2) = -1./24.

       ! corresponds with table 3 of trin_norm_update.pdf
       b_coefs(-2,3:nrad-2) = 1./12. ; b_coefs(-2,nrad-1) = 1./6.
       b_coefs(-1,2) = -1./3. ; b_coefs(-1,3:nrad-2) = -0.75 ; b_coefs(-1,nrad-1) = -1.0
       b_coefs(0,1) = -11./6. ; b_coefs(0,2) = -0.5 ; b_coefs(0,nrad-1) = 0.5
       b_coefs(1,1) = 3. ; b_coefs(1,2) = 1. ; b_coefs(1,3:nrad-2) = 0.75 ; b_coefs(1,nrad-1) = 1./3.
       b_coefs(2,1) = -1.5 ; b_coefs(2,2) = -1./6. ; b_coefs(2,3:nrad-2) = -1./12.
       b_coefs(3,1) = 1./3.

       ! djk is the kronecker delta
       djk(0) = 1.0
    end if

    np = nprofs_evolved

    ! for convenience, break nt vector into constituent elements
    ! (density, ion pressure, electron pressure, toroidal angular momentum)
    ! these are the values at the time level m+1, iteration i
    call get_profs_from_nt_vector (nt_vec_in, dens, pres, mom)
    call get_profs_from_nt_vector (nt_m, dens_m, pres_m, mom_m)

    ! ion and electron pressure are derived quantities
    temp = pres/dens

    Gam = inv_mominertia_fnc(dens,rmajor_grid)

    do is = 1, nspec
       qflx_m(:,is) = flx_m((nspec+is-1)*ncc+1:(nspec+is)*ncc)
    end do

    ! ion and electron pressure are derived quantities
    temp_m = pres_m/dens_m

    ! obtain necessary quantities at cell centers
    do is = 1, nspec
       call get_cc (dens(:,is), denscc(:,is))
       call get_cc (pres(:,is), prescc(:,is))
       call get_cc (dens_m(:,is), denscc_m(:,is))
       call get_cc (pres_m(:,is), prescc_m(:,is))
    end do
    call get_cc (Gam, Gamcc)

    ! corresponds to -G / (Delta rho) from notes
    G_fac = grho_grid/(area_grid*drad)

    ! capf is F from notes
    acapf = area_cc*prescc(:,is_ref)**2.5/(1.5*denscc(:,is_ref)**1.5*bmag_cc**2)
    capf = spread(acapf,2,nspec)*qflx

    ! this is capf at the m (old) time level
    acapf_m = area_cc*prescc_m(:,is_ref)**2.5/(1.5*denscc_m(:,is_ref)**1.5*bmag_cc**2)
    capf_m = spread(acapf_m,2,nspec)*qflx_m

    call get_nu
    
    if (temp_equil) then
       do is = 1, nspec
          nu_grid_m(:,is) = nufac(is)*loglamfnc(is,is,dens_m,temp_m) &
               *dens_m(:,is)/(sqrt(temp_m(:,is_ref))*temp_m(:,is)**1.5)
       end do

       call get_nueq (dens, temp, nu_grid, nueq)
       call get_nueq (dens_m, temp_m, nu_grid_m, nueq_m)
    else
       nueq = 0.0
       nueq_m = 0.0
    end if

    if (te_equal_ti .or. te_fixed) then
       is_low = 2 ; is_up = 2
    else if (equal_ion_temps) then
       is_low = 1 ; is_up = 2
    else
       is_low = 1 ; is_up = nspec
    end if

    ! unless testing, outer BC is constant pressure (no need to do anything)
    ! and inner BC is area*flux = 0
    do is = is_low, is_up

       if (testing .and. flux_option_switch /= flux_option_test2) then
          !!! need to fix !!!
          call set_test_BCs (jacob, nt_out, time, is)
       else
          ! equilibration is zero when all temperatures equal
          if (te_equal_ti .or. .not. temp_equil) then
             deq_dn = 0.
             deq_dp = 0.
          else
             t_ratio = temp/spread(temp(:,is),2,nspec)
          end if

          idx = 1
          ix = 1

          llim = 1 ; ulim = 4
          l = 0 ; u = 3

          fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)
          fac_m = pres_m(ix,is_ref)**2.5/(1.5*dens_m(ix,is_ref)**1.5*bmag_grid(ix)**2)

          psi_n = 0.
          if (evolve_density) then

             do isp = 1, n_ion_spec
                if (turb_heat) then
                   ! this is the contributions to dS/dn coming
                   ! from turbulent energy exchange
                   dheat_dn(l:u) = fac*(-(djk(l:u)*fprim_grid(ix,isp) &
                        + b_coefs(l:u,ix)/drad)*dheat_grid(ix,is,idx)/dens(ix,isp) &
                        + djk(l:u)*dheat_grid(ix,is,idx+np))
                   ! additional contribution if s' is reference species
                   if (isp == is_ref) then
                      dheat_dn(l:u) = dheat_dn(l:u) - fac*1.5*heat(ix,is)*djk(l:u)/dens(ix,is_ref)
                   end if
                else
                   dheat_dn(l:u) = 0.
                end if

                if (.not. te_equal_ti .and. temp_equil) then
                   call get_deq_dn (is, isp, dens(ix,:), temp(ix,:), &
                        t_ratio(ix,:), nueq(ix,is,:), deq_dn(0))
                end if
                
                psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                     dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),A_fac(3),capf(ix,is),acapf(ix),is_ref_dens(isp)) ) &
                     - (deq_dn + dheat_dn)
                
                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
                
                idx = idx + 1
             end do
          end if

          psi_p = 0.
          if (evolve_temperature) then

             do isp = is_low, is_up

                if (turb_heat) then
                   ! this is the contributions to dS/dp coming
                   ! from turbulent energy exchange
                   dheat_dp(l:u) = fac*(-(djk(l:u)*pprim_grid(ix,isp) &
                        + b_coefs(l:u,ix)/drad)*dheat_grid(ix,is,idx)/pres(ix,isp) &
                        + djk(l:u)*dheat_grid(ix,is,idx+np))
                   ! additional contribution if s' is reference species
                   if (isp == is_ref) then
                      dheat_dp(l:u) = dheat_dp(l:u) + fac*2.5*heat(ix,is)*djk(l:u)/pres(ix,is_ref)
                   end if
                else
                   dheat_dp(l:u) = 0.
                end if

                if (.not. te_equal_ti .and. temp_equil) then
                   call get_deq_dp (is, isp, dens(ix,:), temp(ix,:), pres(ix,:), &
                        t_ratio(ix,:), nueq(ix,is,:), deq_dp(0))
!                   deq_dp(0) = 0.
                end if

                psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                     dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),B_fac(3),capf(ix,is),acapf(ix),is_ref_temp(isp)) ) &
                     - (deq_dp + dheat_dp)
                
                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
                
                idx = idx + 1
             end do
          end if
          
          psi_l = 0.
          if (evolve_flow) then
             
             if (turb_heat) then
                ! this is the contributions to dS/dL coming
                ! from turbulent energy exchange
                dheat_dL(l:u) = fac*Gam(llim:ulim)*(rad_grid(ix)*b_coefs(l:u,ix) &
                     * dheat_grid(ix,is,idx)/(qval_grid(ix)*drad) + djk(l:u)*dheat_grid(ix,is,idx+np))
             else
                dheat_dL(l:u) = 0.
             end if
                
             psi_l = G_fac(ix)* ( psi_l_fnc(dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                  cp_coefs(:,is),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
                  rad_cc(ix)/qval_cc(ix),acapf(ix)) ) - dheat_dL

             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)

             idx = idx + 1
          end if

          ! extra bdfac factor for diagonal
          jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)

          ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
          if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
             ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
             ! 'Discretization' section of trin_norm_update.
             d0 = -(ntdelt + ntdelt_old)/ntdelt_old
             dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
             
             ! nt_out is the RHS of the transport equation; i.e., it is B in
             ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
             nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
                  + ntdelt*( src_pres(ix,is)/1.5 + G_fac(ix)*(-capf(ix,is)) &
                  + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is))
          else
             nt_out(base+ix) = nt_m(base+ix) &
                  + ntdelt*( impfac*( -G_fac(ix)*capf(ix,is) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is) ) &
                  + (1.0-impfac)*( -G_fac(ix)*capf_m(ix,is) &
                  + dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))) &
                  + fac_m*heat_m(ix,is) ) &
                  + src_pres(ix,is)/1.5 &
                  + impfac &
                  * (sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) ) )

!             write (*,*) 'ntout', is, ix, nt_out(base+ix), nt_m(base+ix), &
!                  G_fac(ix), capf(ix,is), dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))), &
!                  fac*heat(ix,is), capf_m(ix,is), dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))), &
!                  fac_m*heat_m(ix,is), src_pres(ix,is)/1.5, sum(dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:)), &
!                  sum(pres(llim:ulim,:)*psi_p(l:u,:)), sum(mom(llim:ulim)*psi_l(l:u))

          end if
          
          if (write_balance) then
             write (balance_unit,201) is, time, rad_grid(ix), nt_out(base+ix)-nt_m(base+ix), &
                  -ntdelt*G_fac(ix)*capf(ix,is), ntdelt*src_pres(ix,is)/1.5, &
                  ntdelt*(sum (dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u))), &
                  ntdelt*dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))), &
                  ntdelt*fac*heat(ix,is)
          end if
          
       end if
       ! end inner BC

       do ix = 2, nrad-1
          
          idx = 1

          ! get upper and lower limits of stencil
          ! these limits must change near boundaries
          llim = max(ix-2,1) ; ulim = min(ix+3,nrad)
          l = max(-2,llim-ix) ; u = min(3,ulim-ix)

          fac = pres(ix,is_ref)**2.5/(1.5*dens(ix,is_ref)**1.5*bmag_grid(ix)**2)
          fac_m = pres_m(ix,is_ref)**2.5/(1.5*dens_m(ix,is_ref)**1.5*bmag_grid(ix)**2)

          psi_n = 0.
          if (evolve_density) then

             do isp = 1, n_ion_spec

                if (turb_heat) then
                   ! this is the contributions to dS/dn coming
                   ! from turbulent energy exchange
                   dheat_dn(l:u) = fac*(-(djk(l:u)*fprim_grid(ix,isp) &
                        + b_coefs(l:u,ix)/drad)*dheat_grid(ix,is,idx)/dens(ix,isp) &
                        + djk(l:u)*dheat_grid(ix,is,idx+np))
                   ! additional contribution if s' is reference species
                   if (isp == is_ref) then
                      dheat_dn(l:u) = dheat_dn(l:u) - fac*1.5*heat(ix,is)*djk(l:u)/dens(ix,is_ref)
                   end if
                else
                   dheat_dn(l:u) = 0.
                end if

                if (.not. te_equal_ti .and. temp_equil) then
                   call get_deq_dn (is, isp, dens(ix,:), temp(ix,:), &
                        t_ratio(ix,:), nueq(ix,is,:), deq_dn(0))
                end if

                psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                     dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),A_fac(3),capf(ix,is),acapf(ix),is_ref_dens(isp)) &
                     - psi_nt_fnc(denscc(ix-1,isp),fprim_cc(ix-1,isp), &
                     dqflxdg(ix-1,is,idx),dqflxdg(ix-1,is,idx+np), &
                     cm_coefs(:,ix),dm_coefs(:,ix),A_fac(3),capf(ix-1,is),acapf(ix-1),is_ref_dens(isp)) ) &
                     - (deq_dn + dheat_dn)

!                if (is == 1) then
!                   write (*,*) 'psi_n2', deq_dn
!                   stop
!                end if

                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
                
                idx = idx + 1
             end do
          end if

          psi_p = 0.
          if (evolve_temperature) then

             do isp = is_low, is_up
                
                if (turb_heat) then
                   ! this is the contributions to dS/dp coming
                   ! from turbulent energy exchange
                   dheat_dp(l:u) = fac*(-(djk(l:u)*pprim_grid(ix,isp) &
                        + b_coefs(l:u,ix)/drad)*dheat_grid(ix,is,idx)/pres(ix,isp) &
                        + djk(l:u)*dheat_grid(ix,is,idx+np))
                   ! additional contribution if s' is reference species
                   if (isp == is_ref) then
                      dheat_dp(l:u) = dheat_dp(l:u) + fac*2.5*heat(ix,is)*djk(l:u)/pres(ix,is_ref)
                   end if
                else
                   dheat_dp(l:u) = 0.
                end if

                if (.not. te_equal_ti .and. temp_equil) then
                   call get_deq_dp (is, isp, dens(ix,:), temp(ix,:), pres(ix,:), &
                        t_ratio(ix,:), nueq(ix,is,:), deq_dp(0))
!                   deq_dp(0) = 0.
                end if

                psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                     dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),B_fac(3),capf(ix,is),acapf(ix),is_ref_temp(isp)) &
                     - psi_nt_fnc(prescc(ix-1,isp),pprim_cc(ix-1,isp), &
                     dqflxdg(ix-1,is,idx),dqflxdg(ix-1,is,idx+np), &
                     cm_coefs(:,ix),dm_coefs(:,ix),B_fac(3),capf(ix-1,is),acapf(ix-1),is_ref_temp(isp)) ) &
                     - (deq_dp + dheat_dp)
                
                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
                
                idx = idx + 1
             end do
          end if

          psi_l = 0.
          if (evolve_flow) then
             
             if (turb_heat) then
                ! this is the contributions to dS/dL coming
                ! from turbulent energy exchange
                dheat_dL(l:u) = fac*Gam(llim:ulim)*(rad_grid(ix)*b_coefs(l:u,ix) &
                     * dheat_grid(ix,is,idx)/(qval_grid(ix)*drad) + djk(l:u)*dheat_grid(ix,is,idx+np))
             else
                dheat_dL(l:u) = 0.
             end if

             psi_l = G_fac(ix)* ( psi_l_fnc(dqflxdg(ix,is,idx),dqflxdg(ix,is,idx+np), &
                  cp_coefs(:,is),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
                  rad_cc(ix)/qval_cc(ix),acapf(ix)) &
                  - psi_l_fnc(dqflxdg(ix-1,is,idx),dqflxdg(ix-1,is,idx+np), &
                  cm_coefs(:,is),dm_coefs(:,ix),Gam(ix),Gamcc(ix-1), &
                  rad_cc(ix-1)/qval_cc(ix-1),acapf(ix-1)) ) - dheat_dL
             
             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
             
             idx = idx + 1
          end if

          ! extra bdfac factor for diagonal
          jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)
          
          ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
          if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
             ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
             ! 'Discretization' section of trin_norm_update.
             d0 = -(ntdelt + ntdelt_old)/ntdelt_old
             dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
             
             ! nt_out is the RHS of the transport equation; i.e., it is B in
             ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
             nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
                  + ntdelt*( src_pres(ix,is)/1.5 + G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                  + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is) )
          else
             nt_out(base+ix) = nt_m(base+ix) &
                  + ntdelt*( impfac*( G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                  + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  + fac*heat(ix,is) ) &
                  + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)) &
                  + dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))) &
                  + fac_m*heat_m(ix,is) ) &
                  + src_pres(ix,is)/1.5 &
                  + impfac &
                  * (sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) )) )

             ! write (*,*) 'ntout', is, ix, nt_out(base+ix), nt_m(base+ix), &
                  ! ntdelt*( impfac*( G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                  ! + dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))) &
                  ! + fac*heat(ix,is) ) ), &
                  ! ntdelt*( (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)) &
                  ! + dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))) &
                  ! + fac_m*heat_m(ix,is) ) )&
                  ! + ntdelt*src_pres(ix,is)/1.5, &
                  ! ntdelt*G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)), &
                  ! ntdelt*dens_m(ix,is)*sum(nueq_m(ix,is,:)*(temp_m(ix,:)-spread(temp_m(ix,is),1,nspec))), &
                  ! ntdelt*src_pres(ix,is)/1.5

          end if

          if (write_balance) then
             write (balance_unit,201) is, time, rad_grid(ix), nt_out(base+ix)-nt_m(base+ix), &
                  ntdelt*G_fac(ix)*(capf(ix-1,is) - capf(ix,is)), &
                  ntdelt*src_pres(ix,is)/1.5, &
                  ntdelt*(sum (dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u))), &
                  ntdelt*dens(ix,is)*sum(nueq(ix,is,:)*(temp(ix,:)-spread(temp(ix,is),1,nspec))), &
                  ntdelt*fac*heat(ix,is)
          end if
       
       end do

!       stop

!       do ix = 1, nrad
!          write (*,*) 'ntout', ix, nt_out(base+ix)
!       end do
!       write (*,*)
       
!       do ix = 1, nrad
!          do iy = 1, size(jacob,2)
!             write (*,*) 'jacob', ix, iy, jacob(base+ix,iy)
!          end do
!          write (*,*)
!       end do

       base = base + nrad

    end do

!    stop

    if (write_balance) call flush_output_file (balance_unit)
201 format (i6,8(1x,1e11.4))

    deallocate (psi_n, psi_p, psi_l)
    deallocate (dheat_dn, dheat_dp, dheat_dL)
    deallocate (deq_dn, deq_dp)
    deallocate (nu_grid_m, nueq, nueq_m)
    deallocate (t_ratio)

  end subroutine get_pcoefs

  subroutine get_ncoefs (base, pflx, jacob, &
       nt_out, nt_vec_in, time, itstep, nt_m, ntold, flx_m)

    use mp_trin, only: abort_mp
    use file_utils_trin, only: flush_output_file
    use trinity_input, only: nrad, rad_out, ntdelt, impfac
    use trinity_input, only: flux_option_switch, flux_option_test2
    use trinity_input, only: testing, dfac, ncc
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: is_ref_dens, is_ref_temp, is_ref
    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use interp, only: get_cc

    implicit none
    
    integer :: ix, llim, ulim, l, u
    integer :: is_low, is_up, isp, is, idx, np
    real :: d0, dm1
    
    real, dimension (nrad,nspec) :: dens, pres, temp
    real, dimension (nrad,nspec) :: dens_m, pres_m, temp_m
    real, dimension (nrad) :: G_fac, mom, mom_m, Gam
    real, dimension (ncc) :: acapf, acapf_m, Gamcc
    real, dimension (ncc,nspec) :: denscc, prescc
    real, dimension (ncc,nspec) :: denscc_m, prescc_m
    real, dimension (ncc,nspec) :: capf, pflx_m, capf_m
    real, dimension (:,:), allocatable :: psi_n, psi_p
    real, dimension (:), allocatable :: psi_l
    real, dimension (:,:), allocatable, save :: cm_coefs, cp_coefs
    real, dimension (:,:), allocatable, save :: dm_coefs, dp_coefs

    integer, intent (in) :: itstep
    integer, intent (in out) :: base
    real, intent (in) :: time
    real, dimension (:,:), intent (in) :: pflx
    real, dimension (:), intent (in) :: nt_vec_in, nt_m, ntold, flx_m
    real, dimension (:), intent (in out) :: nt_out
    real, dimension (:,:), intent (in out) :: jacob

    ! these are the coefficients multiplying the pressure at the m+1,i+1 time level
    ! for more details, see the 'Discretization' section of trin_norm_update.pdf
    allocate (psi_n(-2:3,nspec-1))
    allocate (psi_p(-2:3,nspec))
    allocate (psi_l(-2:3)) ; psi_l = 0.0

    np = nprofs_evolved

    ! these are the 'c' and 'd' coefficients used in calculating the 'psi' coefficients
    ! found in the 'Discretization' section of trin_norm_update.pdf
    if (.not. allocated(cm_coefs)) then
       allocate (cm_coefs(-2:3,nrad-1)) ; cm_coefs = 0.0
       allocate (cp_coefs(-2:3,nrad-1)) ; cp_coefs = 0.0
       allocate (dm_coefs(-2:3,nrad-1)) ; dm_coefs = 0.0
       allocate (dp_coefs(-2:3,nrad-1)) ; dp_coefs = 0.0

       ! corresponds with table 1 of trin_norm_update.pdf
       cp_coefs(-1,2:nrad-2) = -0.0625 ; cp_coefs(-1,nrad-1) = -0.125
       cp_coefs(0,1) = 0.375 ; cp_coefs(0,2:nrad-2) = 0.5625 ; cp_coefs(0,nrad-1) = 0.75
       cp_coefs(1,1) = 0.75 ; cp_coefs(1,2:nrad-2) = 0.5625 ; cp_coefs(1,nrad-1) = 0.375
       cp_coefs(2,1) = -0.125 ; cp_coefs(2,2:nrad-2) = -0.0625

       cm_coefs(-2,3:nrad-1) = -0.0625
       cm_coefs(-1,2) = 0.375 ; cm_coefs(-1,3:) = 0.5625
       cm_coefs(0,2) = 0.75 ; cm_coefs(0,3:) = 0.5625
       cm_coefs(1,2) = -0.125 ; cm_coefs(1,3:) = -0.0625

       ! corresponds with table 2 of trin_norm_update.pdf
       dp_coefs(-2,nrad-1) = 1./24.
       dp_coefs(-1,2:nrad-2) = 1./24. ; dp_coefs(-1,nrad-1) = -0.125
       dp_coefs(0,1) = -23./24. ; dp_coefs(0,2:nrad-2) = -1.125 ; dp_coefs(0,nrad-1) = -0.875
       dp_coefs(1,1) = 0.875 ; dp_coefs(1,2:nrad-2) = 1.125 ; dp_coefs(1,nrad-1) = 23./24.
       dp_coefs(2,1) = 0.125 ; dp_coefs(2,2:nrad-2) = -1./24.
       dp_coefs(3,1) = -1./24.
       
       dm_coefs(-2,3:) = 1./24.
       dm_coefs(-1,2) = -23./24. ; dm_coefs(-1,3:) = -1.125
       dm_coefs(0,2) = 0.875 ; dm_coefs(0,3:) = 1.125
       dm_coefs(1,2) = 0.125 ; dm_coefs(1,3:) = -1./24.
       dm_coefs(2,2) = -1./24.
    end if

    ! for convenience, break nt vector into constituent elements
    ! (density, ion pressure, electron pressure, toroidal angular momentum)
    ! these are the values at the time level m+1, iteration i
    call get_profs_from_nt_vector (nt_vec_in, dens, pres, mom)
    call get_profs_from_nt_vector (nt_m, dens_m, pres_m, mom_m)

    ! ion and electron pressure are derived quantities
    temp = pres/dens

    Gam = inv_mominertia_fnc(dens,rmajor_grid)

    ! the particle flux from the beginning of the time step;
    ! i.e., time level m
    do is = 1, nspec
       pflx_m(:,is) = flx_m((is-1)*ncc+1:is*ncc)
    end do

    ! ion and electron pressure are derived quantities
    temp_m = pres_m/dens_m

    ! obtain necessary quantities at cell centers
    do is = 1, nspec
       call get_cc (dens(:,is), denscc(:,is))
       call get_cc (pres(:,is), prescc(:,is))
       call get_cc (dens_m(:,is), denscc_m(:,is))
       call get_cc (pres_m(:,is), prescc_m(:,is))
    end do
    call get_cc (Gam, Gamcc)

    ! corresponds to -G / (Delta rho) from notes
    G_fac = grho_grid/(area_grid*drad)

    ! capf is F from notes
    acapf = area_cc*prescc(:,is_ref)**1.5/(sqrt(denscc(:,is_ref))*bmag_cc**2)
    capf = spread(acapf,2,nspec)*pflx

    ! this is capf at the m (old) time level
    acapf_m = area_cc*prescc_m(:,is_ref)**1.5/(sqrt(denscc_m(:,is_ref))*bmag_cc**2)
    capf_m = spread(acapf_m,2,nspec)*pflx_m

    if (te_equal_ti .or. te_fixed) then
       is_low = 2 ; is_up = 2
    else if (equal_ion_temps) then
       is_low = 1 ; is_up = 2
    else
       is_low = 1 ; is_up = nspec
    end if

    do is = 1, nspec-1

       ! unless testing, outer BC is constant pressure (no need to do anything)
       ! and inner BC is area*flux = 0
       if (testing .and. flux_option_switch /= flux_option_test2) then
          call set_test_BCs (jacob, nt_out, time, is-1)
       else
          idx = 1
          ix = 1
       
          llim = 1 ; ulim = 4
          l = 0 ; u = 3

          psi_n = 0.
          ! note that density is being evolved or would not have
          ! made it into this subroutine
          do isp = 1, nspec-1
             psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                  dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),A_fac(1),capf(ix,is),acapf(ix),is_ref_dens(isp)) )
             
             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
             idx = idx + 1
          end do

          psi_p = 0.
          if (evolve_temperature) then
             do isp = is_low, is_up
                psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                     dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),B_fac(1),capf(ix,is),acapf(ix),is_ref_temp(isp)) )
                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
                idx = idx + 1
             end do

          end if

          psi_l = 0.
          if (evolve_flow) then
             psi_l = G_fac(ix)* ( psi_l_fnc(dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
                  rad_cc(ix)/qval_cc(ix),acapf(ix)) )
             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
             
             idx = idx + 1
          end if

          ! extra bdfac factor for diagonal
          jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)

          ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
          if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
             ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
             ! 'Discretization' section of trin_norm_update.
             d0 = -(ntdelt + ntdelt_old)/ntdelt_old
             dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
          
             ! nt_out is the RHS of the transport equation; i.e., it is B in
             ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
             nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
                  + ntdelt*( src_dens(ix,is) - G_fac(ix)*capf(ix,is) &
                  + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum (pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) )
          else
             nt_out(base+ix) = nt_m(base+ix) &
                  + ntdelt*( impfac*( -G_fac(ix)*capf(ix,is) ) &
                  + (1.0-impfac)*( -G_fac(ix)*capf_m(ix,is) ) &
                  + src_dens(ix,is) &
                  + impfac*( sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) ))
          end if
       
       end if
       ! end inner BC

       do ix = 2, nrad-1
       
          idx = 1

          ! get upper and lower limits of stencil
          ! these limits must change near boundaries
          llim = max(ix-2,1) ; ulim = min(ix+3,nrad)
          l = max(-2,llim-ix) ; u = min(3,ulim-ix)

          psi_n = 0.
          do isp = 1, n_ion_spec
             psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                  dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),A_fac(1),capf(ix,is),acapf(ix),is_ref_dens(isp)) &
                  - psi_nt_fnc(denscc(ix-1,isp),fprim_cc(ix-1,isp), &
                  dpflxdg(ix-1,is,idx),dpflxdg(ix-1,is,idx+np), &
                  cm_coefs(:,ix),dm_coefs(:,ix),A_fac(1),capf(ix-1,is),acapf(ix-1),is_ref_dens(isp)) )

             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
             idx = idx + 1
          end do

          psi_p = 0.
          if (evolve_temperature) then
             do isp = is_low, is_up
                psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                     dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                     cp_coefs(:,ix),dp_coefs(:,ix),B_fac(1),capf(ix,is),acapf(ix),is_ref_temp(isp)) &
                     - psi_nt_fnc(prescc(ix-1,isp),pprim_cc(ix-1,isp), &
                     dpflxdg(ix-1,is,idx),dpflxdg(ix-1,is,idx+np), &
                     cm_coefs(:,ix),dm_coefs(:,ix),B_fac(1),capf(ix-1,is),acapf(ix-1),is_ref_temp(isp)) )

                jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) &
                     = ntdelt*impfac*psi_p(l:u,isp)
                idx = idx + 1
             end do
          end if

          psi_l = 0.
          if (evolve_flow) then
             psi_l = G_fac(ix)* ( psi_l_fnc(dpflxdg(ix,is,idx),dpflxdg(ix,is,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
                  rad_cc(ix)/qval_cc(ix),acapf(ix)) - psi_l_fnc(dpflxdg(ix-1,is,idx), &
                  dpflxdg(ix-1,is,idx+np),cm_coefs(:,ix),dm_coefs(:,ix),Gam(ix),Gamcc(ix-1), &
                  rad_cc(ix-1)/qval_cc(ix-1),acapf(ix-1)) )

             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
             idx = idx + 1
          end if
             
          ! extra bdfac factor for diagonal
          jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)
       
          ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
          if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
             ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
             ! 'Discretization' section of trin_norm_update.
             d0 = -(ntdelt + ntdelt_old)/ntdelt_old
             dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
          
             ! nt_out is the RHS of the transport equation; i.e., it is B in
             ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
             nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
                  + ntdelt*( src_dens(ix,is) + G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) &
                  + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) )
          else
             nt_out(base+ix) = nt_m(base+ix) &
                  + ntdelt*( impfac*( G_fac(ix)*(capf(ix-1,is)-capf(ix,is)) ) &
                  + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1,is)-capf_m(ix,is)) ) &
                  + src_dens(ix,is) &
                  + impfac*( sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
                  + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
                  + sum( mom(llim:ulim)*psi_l(l:u) ) ) )
          end if
       
       end do

       base = base + nrad

    end do

    deallocate (psi_n, psi_p, psi_l)

  end subroutine get_ncoefs

  subroutine get_lcoefs (base, lflx, jacob, &
       nt_out, nt_vec_in, time, itstep, nt_m, ntold, flx_m)

    use mp_trin, only: abort_mp
    use file_utils_trin, only: flush_output_file
    use trinity_input, only: nrad, rad_out, ntdelt, impfac
    use trinity_input, only: flux_option_switch, flux_option_test2
    use trinity_input, only: testing, dfac, ncc
    use trinity_input, only: n_ion_spec, nspec
    use trinity_input, only: m_ion
    use trinity_input, only: is_ref_dens, is_ref_temp, is_ref
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use interp, only: get_cc

    implicit none
    
    integer :: ix, llim, ulim, l, u
    integer :: isp, is_low, is_up, is
    integer :: np, idx
    real :: d0, dm1
    
    real, dimension (nrad,nspec) :: dens, temp, pres
    real, dimension (nrad,nspec) :: dens_m, temp_m, pres_m
    real, dimension (ncc,nspec) :: denscc, prescc
    real, dimension (ncc,nspec) :: denscc_m, prescc_m
    real, dimension (nrad) :: G_fac, mom, Gam, mom_m
    real, dimension (ncc) :: acapf, acapf_m, lflx_m, Gamcc
    real, dimension (ncc) :: capf, capf_m
    real, dimension (:,:), allocatable :: psi_n, psi_p
    real, dimension (:), allocatable :: psi_l
    real, dimension (:,:), allocatable, save :: cm_coefs, cp_coefs
    real, dimension (:,:), allocatable, save :: dm_coefs, dp_coefs

    integer, intent (in) :: itstep
    integer, intent (in out) :: base
    real, intent (in) :: time
    real, dimension (:), intent (in) :: nt_vec_in, nt_m, ntold, flx_m, lflx
    real, dimension (:), intent (in out) :: nt_out
    real, dimension (:,:), intent (in out) :: jacob

    ! these are the coefficients multiplying the pressure at the m+1,i+1 time level
    ! for more details, see the 'Discretization' section of trin_norm_update.pdf
    allocate (psi_n(-2:3,n_ion_spec))  ; psi_n = 0.0
    allocate (psi_p(-2:3,nspec)) ; psi_p = 0.0
    allocate (psi_l(-2:3))  ; psi_l = 0.0

    ! these are the 'c' and 'd' coefficients used in calculating the 'psi' coefficients
    ! found in the 'Discretization' section of trin_norm_update.pdf
    if (.not. allocated(cm_coefs)) then
       allocate (cm_coefs(-2:3,nrad-1)) ; cm_coefs = 0.0
       allocate (cp_coefs(-2:3,nrad-1)) ; cp_coefs = 0.0
       allocate (dm_coefs(-2:3,nrad-1)) ; dm_coefs = 0.0
       allocate (dp_coefs(-2:3,nrad-1)) ; dp_coefs = 0.0

       ! corresponds with table 1 of trin_norm_update.pdf
       cp_coefs(-1,2:nrad-2) = -0.0625 ; cp_coefs(-1,nrad-1) = -0.125
       cp_coefs(0,1) = 0.375 ; cp_coefs(0,2:nrad-2) = 0.5625 ; cp_coefs(0,nrad-1) = 0.75
       cp_coefs(1,1) = 0.75 ; cp_coefs(1,2:nrad-2) = 0.5625 ; cp_coefs(1,nrad-1) = 0.375
       cp_coefs(2,1) = -0.125 ; cp_coefs(2,2:nrad-2) = -0.0625

       cm_coefs(-2,3:nrad-1) = -0.0625
       cm_coefs(-1,2) = 0.375 ; cm_coefs(-1,3:) = 0.5625
       cm_coefs(0,2) = 0.75 ; cm_coefs(0,3:) = 0.5625
       cm_coefs(1,2) = -0.125 ; cm_coefs(1,3:) = -0.0625

       ! corresponds with table 2 of trin_norm_update.pdf
       dp_coefs(-2,nrad-1) = 1./24.
       dp_coefs(-1,2:nrad-2) = 1./24. ; dp_coefs(-1,nrad-1) = -0.125
       dp_coefs(0,1) = -23./24. ; dp_coefs(0,2:nrad-2) = -1.125 ; dp_coefs(0,nrad-1) = -0.875
       dp_coefs(1,1) = 0.875 ; dp_coefs(1,2:nrad-2) = 1.125 ; dp_coefs(1,nrad-1) = 23./24.
       dp_coefs(2,1) = 0.125 ; dp_coefs(2,2:nrad-2) = -1./24.
       dp_coefs(3,1) = -1./24.
       
       dm_coefs(-2,3:) = 1./24.
       dm_coefs(-1,2) = -23./24. ; dm_coefs(-1,3:) = -1.125
       dm_coefs(0,2) = 0.875 ; dm_coefs(0,3:) = 1.125
       dm_coefs(1,2) = 0.125 ; dm_coefs(1,3:) = -1./24.
       dm_coefs(2,2) = -1./24.
    end if

    np = nprofs_evolved

    call get_profs_from_nt_vector (nt_vec_in, dens, pres, mom)
    call get_profs_from_nt_vector (nt_m, dens_m, pres_m, mom_m)

    temp = pres/dens
    temp_m = pres_m/dens_m

    Gam = inv_mominertia_fnc(dens,rmajor_grid)

    lflx_m = flx_m(2*nspec*ncc+1:(2*nspec+1)*ncc)

    ! obtain necessary quantities at cell centers
    do is = 1, nspec
       call get_cc (dens(:,is), denscc(:,is))
       call get_cc (pres(:,is), prescc(:,is))
       call get_cc (pres_m(:,is), prescc_m(:,is))
       call get_cc (dens_m(:,is), denscc_m(:,is))
    end do
    call get_cc (Gam, Gamcc)

    ! corresponds to -G / (Delta rho) from notes
    G_fac = grho_grid/(area_grid*drad)

    ! capf is F from notes
    acapf = area_cc*prescc(:,is_ref)**2*m_ion(1)/(denscc(:,is_ref)*bmag_cc**2)
    capf = acapf*lflx

    ! this is capf at the m (old) time level
    acapf_m = area_cc*prescc_m(:,is_ref)**2*m_ion(1)/(denscc_m(:,is_ref)*bmag_cc**2)
    capf_m = acapf_m*lflx_m

    if (te_equal_ti .or. te_fixed) then
       is_low = 2 ; is_up = 2
    else if (equal_ion_temps) then
       is_low = 1 ; is_up = 2
    else
       is_low = 1 ; is_up = nspec
    end if

    ! unless testing, outer BC is constant pressure (no need to do anything)
    ! and inner BC is area*flux = 0
    if (testing .and. flux_option_switch /= flux_option_test2) then
       call set_test_BCs (jacob, nt_out, time, is=3)
    else
       idx = 1
       ix = 1
       
       llim = 1 ; ulim = 4
       l = 0 ; u = 3

       psi_n = 0.
       if (evolve_density) then
          do isp = 1, n_ion_spec
             psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                  dlflxdg(ix,idx),dlflxdg(ix,idx), &
                  cp_coefs(:,ix),dp_coefs(:,ix),A_fac(2),capf(ix),acapf(ix),is_ref_dens(isp)) )
             
             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
             idx = idx + 1
          end do
       end if

       psi_p = 0.
       if (evolve_temperature) then
          do isp = is_low, is_up
             psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                  dlflxdg(ix,idx),dlflxdg(ix,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),B_fac(2),capf(ix),acapf(ix),is_ref_temp(isp)) )
             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
             idx = idx + 1
          end do
       end if

       psi_l = 0.
       if (evolve_flow) then
          psi_l = G_fac(ix)* ( psi_l_fnc(dlflxdg(ix,idx),dlflxdg(ix,idx), &
               cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
               rad_cc(ix)/qval_cc(ix),acapf(ix)) )
       
          jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
          idx = idx + 1
       end if

       ! extra bdfac factor for diagonal
       jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)
       
       ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
       if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
          ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
          ! 'Discretization' section of trin_norm_update.
          d0 = -(ntdelt + ntdelt_old)/ntdelt_old
          dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
          
          ! nt_out is the RHS of the transport equation; i.e., it is B in
          ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
          nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
               + ntdelt*( src_mom(ix) - G_fac(ix)*capf(ix) &
               + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
               + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
               + sum( mom(llim:ulim)*psi_l(l:u) ) )
       else
          nt_out(base+ix) = nt_m(base+ix) &
               + ntdelt*( impfac*( -G_fac(ix)*capf(ix) ) &
               + (1.0-impfac)*( -G_fac(ix)*capf_m(ix) ) &
               + src_mom(ix) &
               + impfac &
               * (sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
               + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
               + sum( mom(llim:ulim)*psi_l(l:u) ) ) )
       end if
       
    end if
    ! end inner BC

    do ix = 2, nrad-1
       
       idx = 1

       ! get upper and lower limits of stencil
       ! these limits must change near boundaries
       llim = max(ix-2,1) ; ulim = min(ix+3,nrad)
       l = max(-2,llim-ix) ; u = min(3,ulim-ix)

       psi_n = 0.
       if (evolve_density) then
          do isp = 1, n_ion_spec
             psi_n(:,isp) = G_fac(ix)* ( psi_nt_fnc(denscc(ix,isp),fprim_cc(ix,isp), &
                  dlflxdg(ix,idx),dlflxdg(ix,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),A_fac(2),capf(ix),acapf(ix),is_ref_dens(isp)) &
                  - psi_nt_fnc(denscc(ix-1,isp),fprim_cc(ix-1,isp), &
                  dlflxdg(ix-1,idx),dlflxdg(ix-1,idx+np), &
                  cm_coefs(:,ix),dm_coefs(:,ix),A_fac(2),capf(ix-1),acapf(ix-1),is_ref_dens(isp)) )

             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_n(l:u,isp)
             idx = idx + 1
          end do
       end if

       psi_p = 0.
       if (evolve_temperature) then
          do isp = is_low, is_up
             psi_p(:,isp) = G_fac(ix)* ( psi_nt_fnc(prescc(ix,isp),pprim_cc(ix,isp), &
                  dlflxdg(ix,idx),dlflxdg(ix,idx+np), &
                  cp_coefs(:,ix),dp_coefs(:,ix),B_fac(2),capf(ix),acapf(ix),is_ref_temp(isp)) &
                  - psi_nt_fnc(prescc(ix-1,isp),pprim_cc(ix-1,isp), &
                  dlflxdg(ix-1,idx),dlflxdg(ix-1,idx+np), &
                  cm_coefs(:,ix),dm_coefs(:,ix),B_fac(2),capf(ix-1),acapf(ix-1),is_ref_temp(isp)) )

             jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_p(l:u,isp)
             idx = idx + 1
          end do
       end if
       
       psi_l = 0.
       if (evolve_flow) then
          psi_l = G_fac(ix)* ( psi_l_fnc(dlflxdg(ix,idx),dlflxdg(ix,idx+np), &
               cp_coefs(:,ix),dp_coefs(:,ix),Gam(ix),Gamcc(ix), &
               rad_cc(ix)/qval_cc(ix),acapf(ix)) &
               - psi_l_fnc(dlflxdg(ix-1,idx),dlflxdg(ix-1,idx+np), &
               cm_coefs(:,ix),dm_coefs(:,ix),Gam(ix),Gamcc(ix-1), &
               rad_cc(ix-1)/qval_cc(ix-1),acapf(ix-1)) )
          
          jacob(base+ix,(idx-1)*nrad+llim:(idx-1)*nrad+ulim) = ntdelt*impfac*psi_l(l:u)
          idx = idx + 1
       end if

       ! extra bdfac factor for diagonal
       jacob(base+ix,base+ix) = bdfac + jacob(base+ix,base+ix)
       
       ! use BDF2 if impfac=1. otherwise, use general single-step scheme.
       if (abs(impfac-1.0) < epsilon(0.0) .and. itstep > 1) then
          ! d0 and dm1 are dt^{m}*Delta^{0} and dt^{m}*Delta^{-1} from the
          ! 'Discretization' section of trin_norm_update.
          d0 = -(ntdelt + ntdelt_old)/ntdelt_old
          dm1 = ntdelt**2 / ( ntdelt_old * (ntdelt + ntdelt_old) )
          
          ! nt_out is the RHS of the transport equation; i.e., it is B in
          ! A x = B, where x is the density, pressure, etc. at the future timestep and iteration
          nt_out(base+ix) = -nt_m(base+ix)*d0 - ntold(base+ix)*dm1 &
               + ntdelt*( src_mom(ix) + G_fac(ix)*(capf(ix-1)-capf(ix)) &
               + sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
               + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
               + sum( mom(llim:ulim)*psi_l(l:u) ) )
       else
          nt_out(base+ix) = nt_m(base+ix) &
               + ntdelt*( impfac*( G_fac(ix)*(capf(ix-1)-capf(ix)) ) &
               + (1.0-impfac)*( G_fac(ix)*(capf_m(ix-1)-capf_m(ix)) ) &
               + src_mom(ix) &
               + impfac &
               * (sum( dens(llim:ulim,:n_ion_spec)*psi_n(l:u,:) ) &
               + sum( pres(llim:ulim,:)*psi_p(l:u,:) ) &
               + sum( mom(llim:ulim)*psi_l(l:u) ) ) )
       end if
       
    end do
    
    deallocate (psi_n, psi_p, psi_l)

  end subroutine get_lcoefs

  subroutine set_test_BCs (psi, nt, t, is)

    use trinity_input, only: flux_option_switch, flux_option_test1, flux_option_test3
    use trinity_input, only: nrad, dfac, rad_out

    implicit none

    real :: tmod

    real, dimension (:,:), intent (in out) :: psi
    real, dimension (:), intent (in out) :: nt
    real, intent (in) :: t
    integer, intent (in) :: is

    select case (flux_option_switch)
    case (flux_option_test1)
       ! diffusion of Gaussian
       tmod = t + 1.0
       ! outer BC
       psi((is)*nrad,(is)*nrad) = 1.0
       nt((is)*nrad) = sqrt(1./tmod)*exp(-0.25*rad_out**2/(dfac*tmod))
       ! inner BC
       psi((is-1)*nrad+1,(is-1)*nrad+1) = 1.0
       nt((is-1)*nrad+1) = sqrt(1./tmod)*exp(-0.25*rad_in**2/(dfac*tmod))
    case (flux_option_test3)
       ! advection of Gaussian in ion/electron pressures

       ! outer BC
       psi((is+1)*nrad,(is+1)*nrad) = 1.0
       if (is == 1 .or. is == 2) then
          nt((is+1)*nrad) = exp(-(rad_out+t))
       end if
       ! inner BC
       psi(is*nrad+1,is*nrad+1) = 1.0
       if (is == 1 .or. is == 2) then
          nt(is*nrad+1) = exp(-(rad_in+t))
       end if
    end select

  end subroutine set_test_BCs

  ! w should be n, pi, pe, etc. at desired cell-center
  ! dw is kappa_w, dflxdk is dK/dkappa, dflx is dK/dw, c and d are
  ! the c and d coefs from tables 1 and 2
  ! F and FoK are F and F/K, respectively
  function psi_nt_fnc (w,dw,dflxdk,dflx,c,d,fac,F,FoK,ref_species)

    implicit none

    real, dimension (-2:3) :: psi_nt_fnc

    real, intent (in) :: w, F, Fok, dw, dflxdk, dflx, fac
    real, dimension (-2:), intent (in) :: c, d
    logical, intent (in) :: ref_species

    psi_nt_fnc = FoK*(c*dflx - (c*dw + d/drad)*dflxdk/w)
    ! take into account Dirac delta function if this is the ref species
    if (ref_species) psi_nt_fnc = psi_nt_fnc + F*fac*c/w

!     if (.not. ref_species) then
! !       psi_nt_fnc = -FoK*(c*dw + d/drad)*dflxdk/w
!        psi_nt_fnc = FoK*(c*dflx - (c*dw + d/drad)*dflxdk/w)
!     else
! !       psi_nt_fnc = F*fac*c/w - FoK*(c*dw + d/drad)*dflxdk/w
!        psi_nt_fnc = F*fac*c/w + FoK*(c*dflx- (c*dw + d/drad)*dflxdk/w)
!     end if

  end function psi_nt_fnc

  ! dflxdk is dK/dkappa, dflx is dK/dom, 
  ! gam and gamc are 1/(m*n*<R**2>) at grid and cell center, respectively
  ! rho_over_q is rho/q, c and d are the c and d coefs from tables 1 and 2
  ! FoK is F/K
  function psi_l_fnc (dflxdk,dflx,c,d,gam,gamc,rho_over_q,FoK)

    implicit none

    real, dimension (-2:3) :: psi_l_fnc

    real, intent (in) :: Fok, gam, gamc, rho_over_q, dflxdk, dflx
    real, dimension (-2:), intent (in) :: c, d

    psi_l_fnc = FoK*(c*gamc*dflx + gam*rho_over_q*d*dflxdk/drad)
!    psi_l_fnc = FoK*gam*rho_over_q*d*dflxdk/drad

  end function psi_l_fnc

  function afnc (w,f,dflx,pm,idx)

    implicit none
    
    real :: afnc

    real, dimension (:), intent (in) :: w
    real, intent (in) :: f, dflx
    integer, intent (in) :: pm, idx

    real :: wp, wm

    if (pm == 1) then
!       dflxp = 0.5*(dflx(idx)+dflx(idx+1))
       wp = 0.5*(w(idx)+w(idx+1))
!       afnc = -f*w(idx)*dflxp/(drad*wp**2)
       afnc = -f*w(idx)*dflx/(drad*wp**2)
    else
!       dflxm = 0.5*(dflx(idx)+dflx(idx-1))
       wm = 0.5*(w(idx)+w(idx-1))
!       afnc = -f*w(idx)*dflxm/(drad*wm**2)
       afnc = -f*w(idx)*dflx/(drad*wm**2)
    end if

  end function afnc

  function bfnc (w,fp,fm,dflxp,dflxm,idx)

    implicit none
    
    real :: bfnc

    real, dimension (:), intent (in) :: w
    real, intent (in) :: fp, fm, dflxp, dflxm
    integer, intent (in) :: idx

    real :: wp, wm

    wp = 0.5*(w(idx)+w(idx+1))

    if (idx > 1) then
       wm = 0.5*(w(idx)+w(idx-1))
       bfnc = (fp*w(idx+1)*dflxp/wp**2+fm*w(idx-1)*dflxm/wm**2)/drad
    else
       ! make sure this is right -- MAB
       bfnc = (fp*w(idx+1)*dflxp/wp**2)/drad
    end if

  end function bfnc

  function cfnc (w,f,dflx)

    implicit none
    
    real :: cfnc

    real, intent (in) :: f, w, dflx

    cfnc = f*w*dflx/drad

  end function cfnc

  subroutine distribute_profs (time)

    use trinity_input, only: nglobalrad, ncc
    use trinity_input, only: nrad, fork_flag
    use trinity_input, only: nspec
    use trinity_input, only: is_ref
    use trinity_input, only: njac, njobs, zeff_in, z_ion
    use trinity_input, only: evolve_geometry
    use mp_trin, only: all_to_group, proc0, broadcast, abort_mp
    use interp, only: get_cc

    implicit none

    integer :: is, igr

    real, intent (in) :: time

    if (distribute_profs_first) then
       ! get_nu already called just before first
       ! time distribute profs is called
!       if (proc0) call get_nu

       !if (fork_flag) then
          !call all_to_group (drhotordrho_cc, drhotordrho_ms, njobs)
          !call all_to_group (drmindrho_cc, drmindrho_ms, njobs)
          !call all_to_group (rhotor_cc, rhotor_ms, njobs)
          !call all_to_group (rmin_cc, rmin_ms, njobs)
          !call all_to_group (rad_cc, rad_ms, njobs)
          !call all_to_group (qval_cc, qval_ms, njobs)
          !call all_to_group (grho_cc, grho_ms, njobs)
          !call all_to_group (shat_cc, shat_ms, njobs)
          !call all_to_group (rmajor_cc, rmajor_ms, njobs)
          !call all_to_group (kappa_cc, kappa_ms, njobs)
          !call all_to_group (kapprim_cc, kapprim_ms, njobs)
          !call all_to_group (delta_cc, delta_ms, njobs)
          !call all_to_group (deltprim_cc, deltprim_ms, njobs)
          !call all_to_group (bunit_cc, bunit_ms, njobs)
          !call all_to_group (bgeo_cc, bgeo_ms, njobs)
          !call all_to_group (bmag_cc, bmag_ms, njobs)
          !call all_to_group (bmag_old_cc, bmag_old_ms, njobs)
          !call all_to_group (btori_cc, btori_ms, njobs)
       !end if
       distribute_profs_first = .false.
    else if (proc0) then
       ! dens_grid, pres_grid, and mom_grid calculated on proc0
       ! before calling distribute_profs
       omega_grid = mom_grid*inv_mominertia_fnc(dens_grid,rmajor_grid)
       temp_grid = pres_grid/dens_grid

       if (zeff_in .lt. epsilon(0.0) .and. any(z_ion .gt. 1.)) then
         zeff_grid = sum(spread(z_ion**2,1,nrad)*dens_grid(:,2:),2)/dens_grid(:,1)
       end if

       ! this is nu_ss * a / v_ti
       call get_nu

       ! need to understand how Btor enters the GS2 calculation -- MAB
       ! is bmag_grid below total |B| or |Btor|?  should be |Btor|, but
       ! not sure if it actually is
       ! beta_cc is the gs2 reference beta (8*pi*p_ref / Ba**2)
       ! also have to consider the difference necessary to make gene work
       beta_grid = pres_grid(:,is_ref)/bmag_grid**2 * 403.e-4
       betaprim_grid = -beta_grid/pres_grid(:,is_ref) * sum(pprim_grid*pres_grid,2)

       ! cell-centered quantities
       do is = 1, nspec
          call get_cc (dens_grid(:,is), dens_cc(:,is))
          call get_cc (pres_grid(:,is), pres_cc(:,is))
          call get_cc (temp_grid(:,is), temp_cc(:,is))
       end do
       call get_cc (mom_grid, mom_cc)
       call get_cc (omega_grid, omega_cc)
       call get_cc (beta_grid, beta_cc)
       call get_cc (betaprim_grid, betaprim_cc)
       call get_cc (zeff_grid, zeff_cc)

    end if

    if (.not. distribute_profs_first) then
       ! get updated alpha heating power
       ! and obtain other time-dependent sources
       ! if desired
       call get_sources (time)
    end if

    ! following broadcast may not be necessary...should check -- mab
    do is = 1, nspec
       call broadcast (dens_grid(:,is))
       call broadcast (pres_grid(:,is))
       call broadcast (temp_grid(:,is))
       call broadcast (nu_grid(:,is))
    end do
    call broadcast (mom_grid)
    call broadcast (omega_grid)

    ! if evolving magnetic geometry, then
    ! broadcast grid values and use them
    ! to get cell-centered values
    if (evolve_geometry) then
       call broadcast_geometry
       call get_geometry_cc
    end if

    ! get perturbed gradients (differs depending on which profiles being evolved)
    ! perturbed grads stored in fprim_perturb, tprim_perturb, gexb_perturb (a/Ln, a/LT, gexb)
    if (proc0) then
       call get_perturbed_grads (fprim_cc, dens_cc, tprim_cc, temp_cc, gexb_cc, omega_cc, &
            fprim_perturb, dens_perturb, tprim_perturb, temp_perturb, gexb_perturb, omega_perturb)

       pres_perturb = dens_perturb*temp_perturb
       pprim_perturb = fprim_perturb + tprim_perturb

       ! beta_perturb set to be ion (reference) beta
       do igr = 1, njac
          beta_perturb((igr-1)*ncc+1:igr*ncc) = pres_perturb((igr-1)*ncc+1:igr*ncc,is_ref) / bmag_cc**2 * 403.e-4
       end do
       betaprim_perturb = -beta_perturb/pres_perturb(:,is_ref) * sum(pprim_perturb*pres_perturb,2)

       do is = 1, nspec
          nu_perturb(:,is) = nufac(is)*loglamfnc(is,is,dens_perturb,temp_perturb) &
               *dens_perturb(:,is)/(sqrt(temp_perturb(:,is_ref))*temp_perturb(:,is)**1.5)
       end do

       ! if running globally with GENE, need perturbed profiles corresponding
       ! to perturbed gradients
       if (nglobalrad > 1) then
          write (*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
          write (*,*) 'Significant changes have been made to Trinity since last running with global GENE option.'
          write (*,*) 'There will likely be errors unless further modifications are made!'
          write (*,*) '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
          write (*,*) 'Aborting.'
          call abort_mp
!          call get_perturbed_profs (dens_grid(nrad,1), dens_cc(:,1), temp_grid(nrad,:), temp_cc, &
!               fprim_perturb(:,1), tprim_perturb, dens_global, temp_global)
       endif
    end if

    do is = 1, nspec
       call broadcast (dens_cc(:,is))
       call broadcast (pres_cc(:,is))
       call broadcast (temp_cc(:,is))
       call broadcast (nu_cc(:,is))
    end do
    call broadcast (mom_cc)
    call broadcast (omega_cc)
    call broadcast (beta_cc)
    call broadcast (betaprim_cc)
    call broadcast (zeff_cc)
    
    call broadcast (omega_perturb)
    call broadcast (beta_perturb)
    call broadcast (betaprim_perturb)
    call broadcast (gexb_perturb)
    do is = 1, nspec
       call broadcast (dens_perturb(:,is))
       call broadcast (temp_perturb(:,is))
       call broadcast (pres_perturb(:,is))
       call broadcast (nu_perturb(:,is))
       call broadcast (fprim_perturb(:,is))
       call broadcast (tprim_perturb(:,is))
       call broadcast (pprim_perturb(:,is))
    end do
    
    if (nglobalrad > 1) then
       call broadcast (dens_global)
       do is = 1, 2
          call broadcast (temp_global(:,is))
       end do
    end if

    if (fork_flag) then
       if (nglobalrad > 1) then
          ! TODO: will likely need to fix this if ever use it again !
          ! for now take ni=ne
          call all_to_group (dens_global, dens_ms(:,1), njobs)
          call all_to_group (dens_global, dens_ms(:,2), njobs)
          if (nspec > 2) dens_ms(:,3:) = 0.
          call all_to_group (temp_global, temp_ms(:,:2), njobs)
       else
          ! TODO: distribute_ms does not work for nglobalrad > 1
          call distribute_ms
          !call all_to_group (dens_perturb, dens_ms, njobs)
          !call all_to_group (fprim_perturb, fprim_ms, njobs)
          !call all_to_group (temp_perturb, temp_ms, njobs)
       end if
       !call all_to_group (pres_perturb, pres_ms, njobs)
       !call all_to_group (omega_perturb, omega_ms, njobs)
       !call all_to_group (nu_perturb, nu_ms, njobs)
       !call all_to_group (tprim_perturb, tprim_ms, njobs)
       !call all_to_group (gexb_perturb, gexb_ms, njobs)
       !call all_to_group (beta_perturb, beta_ms, njobs)
       !call all_to_group (shift_cc, shift_ms, njobs)
       !call all_to_group (betaprim_perturb, betaprim_ms, njobs)
       !call all_to_group (zeff_cc, zeff_ms, njobs)
    end if

    call set_gyrobohm_norm_arrays

  end subroutine distribute_profs

  subroutine get_geometry_cc

    use interp, only: get_cc
    use nteqns_arrays, only: rmajor_grid, rmajor_cc
    use nteqns_arrays, only: shift_grid, shift_cc
    use nteqns_arrays, only: rmin_grid, rmin_cc
    use nteqns_arrays, only: drmindrho_grid, drmindrho_cc
    use nteqns_arrays, only: qval_grid, qval_cc
    use nteqns_arrays, only: shat_grid, shat_cc
    use nteqns_arrays, only: kappa_grid, kappa_cc
    use nteqns_arrays, only: kapprim_grid, kapprim_cc
    use nteqns_arrays, only: delta_grid, delta_cc
    use nteqns_arrays, only: deltprim_grid, deltprim_cc
    use nteqns_arrays, only: area_grid, area_cc
    use nteqns_arrays, only: grho_grid, grho_cc
    use nteqns_arrays, only: psipol_grid, psipol_cc
    use nteqns_arrays, only: dpsipoldrho_grid, dpsipoldrho_cc
    use nteqns_arrays, only: rhotor_grid, rhotor_cc
    use nteqns_arrays, only: drhotordrho_grid, drhotordrho_cc
    use nteqns_arrays, only: rhopol_grid, rhopol_cc
    use nteqns_arrays, only: drhopoldrho_grid, drhopoldrho_cc
    use nteqns_arrays, only: btori_grid, btori_cc
    use nteqns_arrays, only: bunit_grid, bunit_cc

    implicit none

    call get_cc (rmajor_grid, rmajor_cc)
    call get_cc (shift_grid, shift_cc)
    call get_cc (rmin_grid, rmin_cc)
    call get_cc (drmindrho_grid, drmindrho_cc)
    call get_cc (qval_grid, qval_cc)
    call get_cc (shat_grid, shat_cc)
    call get_cc (kappa_grid, kappa_cc)
    call get_cc (kapprim_grid, kapprim_cc)
    call get_cc (delta_grid, delta_cc)
    call get_cc (deltprim_grid, deltprim_cc)
    call get_cc (area_grid, area_cc)
    call get_cc (grho_grid, grho_cc)
    call get_cc (psipol_grid, psipol_cc)
    call get_cc (dpsipoldrho_grid, dpsipoldrho_cc)
    call get_cc (rhotor_grid, rhotor_cc)
    call get_cc (drhotordrho_grid, drhotordrho_cc)
    call get_cc (rhopol_grid, rhopol_cc)
    call get_cc (drhopoldrho_grid, drhopoldrho_cc)
    call get_cc (btori_grid, btori_cc)
    call get_cc (bunit_grid, bunit_cc)

  end subroutine get_geometry_cc

  subroutine broadcast_geometry

    use mp_trin, only: broadcast
    use nteqns_arrays, only: rmajor_grid, shift_grid
    use nteqns_arrays, only: rmin_grid, drmindrho_grid
    use nteqns_arrays, only: qval_grid, shat_grid
    use nteqns_arrays, only: kappa_grid, kapprim_grid
    use nteqns_arrays, only: delta_grid, deltprim_grid
    use nteqns_arrays, only: area_grid, grho_grid
    use nteqns_arrays, only: psipol_grid, dpsipoldrho_grid
    use nteqns_arrays, only: rhotor_grid, drhotordrho_grid
    use nteqns_arrays, only: rhopol_grid, drhopoldrho_grid
    use nteqns_arrays, only: btori_grid, bunit_grid
    
    implicit none

    call broadcast (rmajor_grid)
    call broadcast (shift_grid)
    call broadcast (rmin_grid)
    call broadcast (drmindrho_grid)
    call broadcast (qval_grid)
    call broadcast (shat_grid)
    call broadcast (kappa_grid)
    call broadcast (kapprim_grid)
    call broadcast (delta_grid)
    call broadcast (deltprim_grid)
    call broadcast (area_grid)
    call broadcast (psipol_grid)
    call broadcast (dpsipoldrho_grid)
    call broadcast (rhotor_grid)
    call broadcast (drhotordrho_grid)
    call broadcast (rhopol_grid)
    call broadcast (drhopoldrho_grid)
    call broadcast (btori_grid)
    call broadcast (bunit_grid)
    call broadcast (grho_grid)

  end subroutine broadcast_geometry

  ! takes in R/Ln, R/Lpi, R/Lpe profiles and outputs these profiles, 
  ! along with perturbed gradient profiles
  subroutine get_perturbed_grads (ngrad, dens, tgrad, temp, lgrad, om, &
       pngrad, pn, ptgrad, pt, plgrad, pom)

    use trinity_input, only: dfprim, dtprim, dgexb
    use trinity_input, only: nrad, ncc
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: njac, nprofs_evolved
    use trinity_input, only: evolve_grads_only
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use trinity_input, only: z_ion

    implicit none
    
    real, dimension (:,:), intent (in) :: ngrad, dens
    real, dimension (:,:), intent (in) :: tgrad, temp
    real, dimension (:), intent (in) :: lgrad, om
    real, dimension (:,:), intent (out) :: pngrad, pn
    real, dimension (:,:), intent (out) :: ptgrad, pt
    real, dimension (:), intent (out ) :: plgrad, pom

    integer :: igr, idx, np, is
    real :: z_ratio

    np = nprofs_evolved

    do igr = 1, njac
       pngrad(1+(igr-1)*ncc:igr*ncc,:) = ngrad
       ptgrad(1+(igr-1)*ncc:igr*ncc,:) = tgrad
       plgrad(1+(igr-1)*ncc:igr*ncc) = lgrad

       pn(1+(igr-1)*ncc:igr*ncc,:) = dens
       pt(1+(igr-1)*ncc:igr*ncc,:) = temp
       pom(1+(igr-1)*ncc:igr*ncc) = om
    end do

    idx = 1

    if (evolve_density) then
       ! perturb density gradient at fixed pressure gradient
       ! requires changing temperature gradient accordingly
       ! first take care of perturbing electron density gradient
       is = 1
       pngrad(idx*ncc+1:(idx+1)*ncc,is) = ngrad(:,is) + dprims(:,idx)
       ! must add density gradient perturbation / Zi to satisfy quasineutrality
       pngrad(idx*ncc+1:(idx+1)*ncc,nspec) = ngrad(:,nspec) + dprims(:,idx)/z_ion(n_ion_spec)
       ! want to keep pressure gradient fixed so must modify temperature gradient
       ptgrad(idx*ncc+1:(idx+1)*ncc,is) = tgrad(:,is) - dprims(:,idx)
       ptgrad(idx*ncc+1:(idx+1)*ncc,nspec) = tgrad(:,nspec) - dprims(:,idx)/z_ion(n_ion_spec)

       ! perturb density at fixed pressure
       ! requires changing temperature accordingly
       ! first take care of perturbing electron density
       if (.not. evolve_grads_only) then
          pn((idx+np)*ncc+1:(idx+np+1)*ncc,is) = dens(:,is) - dnt(:,idx)
          ! must modify final ion density to satisfy quasineutrality
          pn((idx+np)*ncc+1:(idx+np+1)*ncc,nspec) = dens(:,nspec) - dnt(:,idx)/z_ion(n_ion_spec)
          ! want to keep pressure fixed so must also modify temperature
          pt((idx+np)*ncc+1:(idx+np+1)*ncc,is) = temp(:,is)*dens(:,is) &
               / (dens(:,is)-dnt(:,idx))
          pt((idx+np)*ncc+1:(idx+np+1)*ncc,nspec) = temp(:,nspec)*dens(:,nspec) &
               / (dens(:,nspec)-dnt(:,idx)/z_ion(n_ion_spec))
       end if

       idx = idx + 1

       if (n_ion_spec > 1) then
          do is = 2, n_ion_spec
             z_ratio = z_ion(is-1)/z_ion(n_ion_spec)

             ! perturb density gradient at fixed pressure gradient
             pngrad(idx*ncc+1:(idx+1)*ncc,is) = ngrad(:,is) + dprims(:,idx)
             ! must modify final ion density gradient to satisfy quasineutrality
             pngrad(idx*ncc+1:(idx+1)*ncc,nspec) = ngrad(:,nspec) - dprims(:,idx)*z_ratio
             ! want to keep pressure gradient fixed so must modify temperature gradient
             ptgrad(idx*ncc+1:(idx+1)*ncc,is) = tgrad(:,is) - dprims(:,idx)
             ptgrad(idx*ncc+1:(idx+1)*ncc,nspec) = tgrad(:,nspec) + dprims(:,idx)*z_ratio

             ! perturb density at fixed pressure
             if (.not. evolve_grads_only) then
                pn((idx+np)*ncc+1:(idx+np+1)*ncc,is) = dens(:,is) - dnt(:,idx)
                ! must modify final ion density to satisfy quasineutrality
                pn((idx+np)*ncc+1:(idx+np+1)*ncc,nspec) = dens(:,nspec) + dnt(:,idx)*z_ratio
                ! want to keep pressure fixed so must also modify temperature
                pt((idx+np)*ncc+1:(idx+np+1)*ncc,is) = temp(:,is)*dens(:,is)/(dens(:,is)-dnt(:,idx))
                pt((idx+np)*ncc+1:(idx+np+1)*ncc,nspec) = temp(:,nspec)*dens(:,nspec)/(dens(:,nspec)+dnt(:,idx)*z_ratio)
             end if

             idx = idx + 1
          end do
       end if
       
    end if

    if (evolve_temperature) then

       ! perturb the electron temperature, either because we are evolving it
       ! or because it has to equal the ion temperature
       ptgrad(idx*ncc+1:(idx+1)*ncc,1) = tgrad(:,1) + dprims(:,idx)
       if (.not. evolve_grads_only) pt((idx+np)*ncc+1:(idx+np+1)*ncc,1) = temp(:,1) + dnt(:,idx)

       ! if Te being evolved, perturb it independent of Ti
       if (.not. te_equal_ti .and. .not. te_fixed) idx = idx + 1

       ! perturb the main ion temperature
       ptgrad(idx*ncc+1:(idx+1)*ncc,2) = tgrad(:,2) + dprims(:,idx)
       if (.not. evolve_grads_only) then
          pt((idx+np)*ncc+1:(idx+np+1)*ncc,2) = temp(:,2) + dnt(:,idx)
       end if
       if (n_ion_spec > 1) then
          if (equal_ion_temps) then
             ptgrad(idx*ncc+1:(idx+1)*ncc,3:) = spread(ptgrad(idx*ncc+1:(idx+1)*ncc,2),2,n_ion_spec-1)
             if (.not. evolve_grads_only) then
                pt((idx+np)*ncc+1:(idx+np+1)*ncc,3:) &
                     = spread(pt((idx+np)*ncc+1:(idx+np+1)*ncc,2),2,n_ion_spec-1)
             end if
             idx = idx + 1
          else
             ! increment idx for the main ion temperature perturbation above
             idx = idx + 1

             ! perturb the minority ion temperature(s)
             do is = 3, nspec
                ptgrad(idx*ncc+1:(idx+1)*ncc,is) = tgrad(:,is) + dprims(:,idx)
                if (.not. evolve_grads_only) then
                   pt((idx+np)*ncc+1:(idx+np+1)*ncc,is) = temp(:,is) + dnt(:,idx)
                end if
                idx = idx + 1
             end do
          end if
       end if

    end if

    if (evolve_flow) then
       plgrad(idx*ncc+1:(idx+1)*ncc) = lgrad + dprims(:,idx)
       if (.not. evolve_grads_only) pom((idx+np)*ncc+1:(idx+np+1)*ncc) = om + dnt(:,idx)
       idx = idx + 1
    end if

  end subroutine get_perturbed_grads

  ! takes in (n,pi,pe) and (R/Ln, R/Lpi, R/Lpe) profiles and outputs these profiles, 
  ! along with perturbed (n,pi,pe) profiles
!  subroutine get_perturbed_profs (densbc, denscc, tempbc, tempcc, mombc, momcc, pfp, ptp, pn, pt, pl)
  !!! will need updating before use -- mab !!!

!   subroutine get_perturbed_profs (densbc, denscc, tempbc, tempcc, pfp, ptp, pn, pt)

!     use trinity_input, only: nrad, dfprim, dtprim, dgexb, nprofs_evolved, ncc
!     use trinity_input, only: grad_option_switch, grad_option_tigrad, grad_option_lgrad
!     use trinity_input, only: grad_option_ngrad, grad_option_tgrads, grad_option_all
!     use trinity_input, only: grad_option_ntgrads

!     implicit none

!     integer :: igr, ix
    
!     real, intent (in) :: densbc
!     real, dimension (:), intent (in) :: denscc, tempbc, pfp
!     real, dimension (:,:), intent (in) :: tempcc, ptp
! !    real, dimension (:), intent (out) :: pn, pl
!     real, dimension (:), intent (out) :: pn
!     real, dimension (:,:), intent (out) :: pt

!     real :: np

!     np = nprofs_evolved

!     ! initialize perturbed profiles to be equal to unperturbed profiles
!     ! at cell centers
!     do igr = 1, np+1
!        pn(1+(igr-1)*ncc:igr*ncc) = denscc
!        pt(1+(igr-1)*ncc:igr*ncc,1) = tempcc(:,1)
!        pt(1+(igr-1)*ncc:igr*ncc,2) = tempcc(:,2)
! !       pl(1+(igr-1)*ncc:igr*ncc) = momcc
!     end do

!     ! start at boundary and integrate perturbed derivative profiles inwards
!     ! to obtain perturbed profiles
!     select case (grad_option_switch)
!     case (grad_option_tgrads)
!        do igr = 1, ngrads/2
!           ! calculate perturbed temperature at first cell in from boundary
!           pt((igr+1)*ncc,1) = tempbc(1) + 0.5*drad*tempcc(ncc,1)*ptp((igr+1)*ncc,1)
!           pt((ngrads/2+igr+1)*ncc,2) = tempbc(2) + 0.5*drad*tempcc(ncc,2)*ptp((ngrads/2+igr+1)*ncc,2)
!           do ix = ncc-1, 1, -1
!              pt(igr*ncc+ix,1) = pt(igr*ncc+ix+1,1) + drad*pt(igr*ncc+ix,1)*ptp(igr*ncc+ix,1)
!              pt((ngrads/2+igr)*ncc+ix,2) = pt((ngrads/2+igr)*ncc+ix+1,2) + drad*pt((ngrads/2+igr)*ncc+ix,2)*ptp((ngrads/2+igr)*ncc+ix,2)
!           end do
!        end do
!     case (grad_option_tigrad)
!        do igr = 1, ngrads
!           pt((igr+1)*ncc,1) = tempbc(1) + 0.5*drad*tempcc(ncc,1)*ptp((igr+1)*ncc,1)
!           do ix = ncc-1,1,-1
!              pt(igr*ncc+ix,1) = pt(igr*ncc+ix+1,1) + drad*pt(igr*ncc+ix,1)*ptp(igr*ncc+ix,1)
!           end do
!        end do
!     case (grad_option_all)
!        do igr = 1, ngrads/4
!           pn((igr+1)*ncc) = densbc + 0.5*drad*denscc(ncc)*pfp((igr+1)*ncc)
!           pt((igr+1)*ncc,:) = tempbc + 0.5*drad*tempcc(ncc,:)*ptp((igr+1)*ncc,:)
!           pt((ngrads/4+igr+1)*ncc,1) = tempbc(1) + 0.5*drad*tempcc(ncc,1)*ptp((ngrads/4+igr+1)*ncc,1)
!           pt((2*ngrads/4+igr+1)*ncc,2) = tempbc(2) + 0.5*drad*tempcc(ncc,2)*ptp((2*ngrads/4+igr+1)*ncc,2)
! !          pl((3*ngrads/4+igr+1)*ncc) = mombc(2) + 0.5*drad*momcc(ncc)*pup((3*ngrads/4+igr+1)*ncc)
!           do ix = ncc-1,1,-1
!              pn(igr*ncc+ix) = pn(igr*ncc+ix+1) + drad*pn(igr*ncc+ix)*pfp(igr*ncc+ix)
!              pt(igr*ncc+ix,:) = pt(igr*ncc+ix+1,:) + drad*pt(igr*ncc+ix,:)*ptp(igr*ncc+ix,:)
!              pt((ngrads/4+igr)*ncc+ix,1) = pt((ngrads/4+igr)*ncc+ix+1,1) + drad*pt((ngrads/4+igr)*ncc+ix,1)*ptp((ngrads/4+igr)*ncc+ix,1)
!              pt((2*ngrads/4+igr)*ncc+ix,2) = pt((2*ngrads/4+igr)*ncc+ix+1,2) + drad*pt((2*ngrads/4+igr)*ncc+ix,2)*ptp((2*ngrads/4+igr)*ncc+ix,2)
! !             pl((3*ngrads/4+igr)*ncc+ix) = pt((3*ngrads/4+igr)*ncc+ix+1) + drad*pt((3*ngrads/4+igr)*ncc+ix)*ptp((3*ngrads/4+igr)*ncc+ix)
!           end do
!        end do
!     case (grad_option_ngrad)
!        do igr = 1, ngrads
!           pn((igr+1)*ncc) = densbc + 0.5*drad*denscc(ncc)*pfp((igr+1)*ncc)
!           pt((igr+1)*ncc,:) = tempbc + 0.5*drad*tempcc(ncc,:)*ptp((igr+1)*ncc,:)
!           do ix = ncc-1,1,-1
!              pn(igr*ncc+ix) = pn(igr*ncc+ix+1) + drad*pn(igr*ncc+ix)*pfp(igr*ncc+ix)
!              pt(igr*ncc+ix,:) = pt(igr*ncc+ix+1,:) + drad*pt(igr*ncc+ix,:)*ptp(igr*ncc+ix,:)
!           end do
!        end do
!     case (grad_option_lgrad)
!        do igr = 1, ngrads
! !          pl((igr+1)*ncc) = mombc + 0.5*drad*momcc(ncc)*pup((igr+1)*ncc)
!           do ix = ncc-1,1,-1
! !             pl(igr*ncc+ix) = pl(igr*ncc+ix+1) + drad*pl(igr*ncc+ix)*pup(igr*ncc+ix)
!           end do
!        end do
!     case (grad_option_ntgrads)
!        do igr = 1, ngrads/3
!           pn((igr+1)*ncc) = densbc + 0.5*drad*denscc(ncc)*pfp((igr+1)*ncc)
!           pt((igr+1)*ncc,:) = tempbc + 0.5*drad*tempcc(ncc,:)*ptp((igr+1)*ncc,:)
!           pt((ngrads/3+igr+1)*ncc,1) = tempbc(1) + 0.5*drad*tempcc(ncc,1)*ptp((ngrads/3+igr+1)*ncc,1)
!           pt((2*ngrads/3+igr+1)*ncc,2) = tempbc(2) + 0.5*drad*tempcc(ncc,2)*ptp((2*ngrads/3+igr+1)*ncc,2)
!           do ix = ncc-1,1,-1
!              pn(igr*ncc+ix) = pn(igr*ncc+ix+1) + drad*pn(igr*ncc+ix)*pfp(igr*ncc+ix)
!              pt(igr*ncc+ix,:) = pt(igr*ncc+ix+1,:) + drad*pt(igr*ncc+ix,:)*ptp(igr*ncc+ix,:)
!              pt((ngrads/3+igr)*ncc+ix,1) = pt((ngrads/3+igr)*ncc+ix+1,1) + drad*pt((ngrads/3+igr)*ncc+ix,1)*ptp((ngrads/3+igr)*ncc+ix,1)
!              pt((2*ngrads/3+igr)*ncc+ix,2) = pt((2*ngrads/3+igr)*ncc+ix+1,2) + drad*pt((2*ngrads/3+igr)*ncc+ix,2)*ptp((2*ngrads/3+igr)*ncc+ix,2)
!           end do
!        end do
!     end select

!   end subroutine get_perturbed_profs



  subroutine get_delgrads (dens, pres, mom)

    use mp_trin, only: abort_mp
    use trinity_input, only: evolve_grads_only, ncc
    use trinity_input, only: dfprim, dtprim, dgexb
    use trinity_input, only: ddens, dtemp, dom
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: m_ion
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed
    use interp, only: get_cc

    implicit none

    real, dimension (:,:), intent (in) :: dens, pres
    real, dimension (:), intent (in) :: mom

    integer :: idx, is
    real, dimension (ncc,nspec) :: denscc, prescc, tempcc
    real, dimension (ncc) :: momcc, omcc

    if (.not. allocated(dprims)) then
       allocate (dprims(ncc,nprofs_evolved))
       if (.not. evolve_grads_only) allocate (dnt(ncc,nprofs_evolved))
    end if

    do is = 1, nspec
       call get_cc (dens(:,is), denscc(:,is))
       call get_cc (pres(:,is), prescc(:,is))
    end do
    call get_cc (mom, momcc)

    tempcc = prescc/denscc

    omcc = momcc/(sum(spread(m_ion,1,ncc)*denscc(:,2:)*spread(rmajor_cc**2,2,nspec-1)))

    idx = 1
    if (evolve_density) then
       ! perturb density grad scale length for all species but one
       ! final ion species is constrained by quasineutrality
       do is = 1, nspec-1
          ! avoid problems in case gradient is zero
          !where (abs(fprim_cc(:,is)) < epsilon(0.))
           dprims(:,idx) = -dprim_function(fprim_cc(:,is), dfprim)
             !dprims(:,idx) = -0.1*dfprim
          !elsewhere
             !dprims(:,idx) = -dfprim*fprim_cc(:,is)
          !end where

          if (.not. evolve_grads_only) dnt(:,idx) = -ddens*denscc(:,is)
          idx = idx + 1
       end do
    end if
    if (evolve_temperature) then
       ! if evolving Te separately from Ti, prepare
       ! a perturbed temperature and temperature gradient for electrons
       if (.not. te_equal_ti.and..not.te_fixed) then
         dprims(:,idx) = dprim_function(tprim_cc(:,1), dtprim)
          !! avoid problems in case gradient is zero
          !where (abs(tprim_cc(:,1)) < epsilon(0.))
             !dprims(:,idx) = dtprim*0.1
          !elsewhere
             !dprims(:,idx) = dtprim*tprim_cc(:,1)
          !end where
          if (.not. evolve_grads_only) dnt(:,idx) = dtemp*tempcc(:,1)
          idx = idx + 1
       end if

       !! avoid problems in case gradient is zero
       !where (abs(tprim_cc(:,2)) < epsilon(0.))
          !dprims(:,idx) = dtprim*0.1
       !elsewhere
          !dprims(:,idx) = dtprim*tprim_cc(:,2)
       !end where
       dprims(:,idx) = dprim_function(tprim_cc(:,2), dtprim)
       if (.not. evolve_grads_only) dnt(:,idx) = dtemp*tempcc(:,2)
       idx = idx + 1

       if (n_ion_spec > 1 .and. .not.equal_ion_temps) then
          do is = 3, nspec
             dprims(:,idx) = dprim_function(tprim_cc(:,is), dtprim)
             !! avoid problems in case gradient is zero
             !where (abs(tprim_cc(:,is)) < epsilon(0.))
                !dprims(:,idx) = dtprim*0.1
             !elsewhere
                !dprims(:,idx) = dtprim*tprim_cc(:,is)
             !end where
             if (.not. evolve_grads_only) dnt(:,idx) = dtemp*tempcc(:,is)
             idx = idx + 1
          end do
       end if

    end if

    if (evolve_flow) then
       dprims(:,idx) = dprim_function(gexb_cc,dgexb)
       !! avoid problems in case gradient is zero
       !where (abs(gexb_cc) < epsilon(0.))
          !dprims(:,idx) = dgexb*0.1
       !elsewhere
          !dprims(:,idx) = dgexb*gexb_cc
       !end where
       !if (.not. evolve_grads_only) dnt(:,idx) = dom*omcc
       if (.not. evolve_grads_only) dnt(:,idx) = dprim_function(omcc,dom)
       idx = idx + 1
    end if
    contains
      function dprim_function(gradient, d)
        use trinity_input, only: dprim_option
        use mp_trin, only: abort_mp
        real,intent(in) :: d
        real,dimension(:),intent(in) :: gradient
        real,dimension(size(gradient)) :: dprim_function

        select case (trim(dprim_option))
        case ('constant')
          dprim_function = d
        case ('proportional')
          dprim_function = d * max(abs(gradient), 1.0) * gradient/abs(gradient)
        case ('propabs')
          dprim_function = d * max(abs(gradient), 1.0) 
        case default
          write (*,*) 'ERROR: Unknown dprim_option ', dprim_option
          call abort_mp
        end select
        !dprim_function = d*gradient
      end function dprim_function

  end subroutine get_delgrads

  subroutine set_dflxdg (idx, pflx, qflx, heat, lflx)

    use trinity_input, only: nprofs_evolved, evolve_grads_only

    implicit none

    integer, intent (in) :: idx
    real, dimension (:,:,:), intent (in) :: pflx, qflx, heat
    real, dimension (:,:), intent (in) :: lflx

    integer :: np, ulim

    np = nprofs_evolved
    ulim = size(pflx,2)

    ! get derivatives of fluxes and heating w.r.t. changes in dens, pres, mom gradients
    dpflxdg(:,:,idx) = (pflx(:,:,idx+1) - pflx(:,:,1))/spread(dprims(:,idx),2,ulim)
    dqflxdg(:,:,idx) = (qflx(:,:,idx+1) - qflx(:,:,1))/spread(dprims(:,idx),2,ulim)
    dheatdg(:,:,idx) = (heat(:,:,idx+1) - heat(:,:,1))/spread(dprims(:,idx),2,ulim)
    dlflxdg(:,idx) = (lflx(:,idx+1) - lflx(:,1))/dprims(:,idx)
    
    ! obtain the derivatives of fluxes and heating w.r.t. changes in local dens, pres, mom
    if (.not. evolve_grads_only) then
       dpflxdg(:,:,idx+np) = (pflx(:,:,np+idx+1) - pflx(:,:,1))/spread(dnt(:,idx),2,ulim)
       dqflxdg(:,:,idx+np) = (qflx(:,:,np+idx+1) - qflx(:,:,1))/spread(dnt(:,idx),2,ulim)
       dheatdg(:,:,idx+np) = (heat(:,:,np+idx+1) - heat(:,:,1))/spread(dnt(:,idx),2,ulim)
       dlflxdg(:,idx+np) = (lflx(:,np+idx+1) - lflx(:,1))/dnt(:,idx)
    end if
    
  end subroutine set_dflxdg

  subroutine get_nt_vector_from_profs (dens, pres, mom, nt_vec)

    use trinity_input, only: nrad
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed

    implicit none

    real, dimension (:,:), intent (in) :: dens, pres
    real, dimension (:), intent (in) :: mom
    real, dimension (:), intent (out) :: nt_vec

    integer :: is, idx

    idx = 1

    if (evolve_density) then
       do is = 1, n_ion_spec
          nt_vec((idx-1)*nrad+1:idx*nrad) = dens(:,is)
          idx = idx + 1
       end do
    end if
    
    if (evolve_temperature) then
       if (.not.te_equal_ti.and..not. te_fixed) then
          nt_vec((idx-1)*nrad+1:idx*nrad) = pres(:,1)
          idx = idx + 1
       end if
       nt_vec((idx-1)*nrad+1:idx*nrad) = pres(:,2)
       idx = idx + 1
       if (n_ion_spec > 1 .and. .not.equal_ion_temps) then
          do is = 3, nspec
             nt_vec((idx-1)*nrad+1:idx*nrad) = pres(:,is)
             idx = idx + 1
          end do
       end if
    end if
    
    if (evolve_flow) then
       nt_vec((idx-1)*nrad+1:idx*nrad) = mom
       idx = idx + 1
    end if

  end subroutine get_nt_vector_from_profs

  subroutine get_profs_from_nt_vector (nt_vec, dens, pres, mom)

    use trinity_input, only: nrad
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: z_ion
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed

    implicit none

    real, dimension (:), intent (in) :: nt_vec
    real, dimension (:,:), intent (out) :: dens, pres
    real, dimension (:), intent (out) :: mom

    integer :: is, idx

    idx = 1

    ! if density not evolved, it will remain equal to initial value
    if (evolve_density) then
       do is = 1, n_ion_spec
          dens(:,is) = nt_vec((idx-1)*nrad+1:idx*nrad)
          idx = idx + 1
       end do
       ! use quasineutrality to obtain density of final ion species
       dens(:,nspec) = (dens(:,1) - sum(spread(z_ion(:n_ion_spec-1),1,nrad)*dens(:,2:n_ion_spec),2)) &
            / z_ion(n_ion_spec)
    else
       dens = dens_init
    end if
    
    if (evolve_temperature) then
       ! this is P_i or P_e depending on whether Te=Ti is assumed
       pres(:,1) = nt_vec((idx-1)*nrad+1:idx*nrad)
       if (te_equal_ti) then
          ! if Te=Ti assumed, convert from P_i to P_e
          pres(:,1) = pres(:,1)*dens(:,1)/dens(:,2)
       else if (te_fixed) then
          !pres(:,1) = temp_grid(:,1)*dens(:,1)
          pres(:,1) = pres_init(:,1)/dens_init(:,1) * dens(:,1)
       else
          idx = idx + 1
       end if

       pres(:,2) = nt_vec((idx-1)*nrad+1:idx*nrad)
       idx = idx + 1

       if (n_ion_spec > 1 .and. equal_ion_temps) then
          pres(:,3:) = spread(pres(:,2)/dens(:,2),2,n_ion_spec-1)*dens(:,3:)
       else
          do is = 3, nspec
             pres(:,is) = nt_vec((idx-1)*nrad+1:idx*nrad)
             idx = idx + 1
          end do
       end if
    else
       pres = pres_init/dens_init * dens
    end if

    if (evolve_flow) then
       mom = nt_vec((idx-1)*nrad+1:idx*nrad)
       idx = idx + 1
    else
       mom = mom_init
    end if

  end subroutine get_profs_from_nt_vector

  subroutine get_flx_vector_from_fluxes (pflx, qflx, lflx, flx_vec)

    use trinity_input, only: ncc
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed

    implicit none

    real, dimension (:,:), intent (in) :: pflx, qflx
    real, dimension (:), intent (in) :: lflx
    real, dimension (:), intent (out) :: flx_vec

    integer :: is, idx

    idx = 1

    if (evolve_density) then
       do is = 1, n_ion_spec
          flx_vec((idx-1)*ncc+1:idx*ncc) = pflx(:,is)
          idx = idx + 1
       end do
    end if

    if (evolve_temperature) then
       if (.not.te_equal_ti .and. .not. te_fixed) then
          flx_vec((idx-1)*ncc+1:idx*ncc) = qflx(:,1)
          idx = idx + 1
       end if
       flx_vec((idx-1)*ncc+1:idx*ncc) = qflx(:,2)
       idx = idx + 1
       if (n_ion_spec > 1 .and. .not.equal_ion_temps) then
          do is = 3, nspec
             flx_vec((idx-1)*ncc+1:idx*ncc) = qflx(:,is)
             idx = idx + 1
          end do
       end if
    end if

    if (evolve_flow) then
       flx_vec((idx-1)*ncc+1:idx*ncc) = lflx
       idx = idx + 1
    end if

  end subroutine get_flx_vector_from_fluxes

  subroutine get_fluxes_from_flx_vector (flx_vec, pflx, qflx, lflx)

    use trinity_input, only: ncc
    use trinity_input, only: nspec, n_ion_spec
    use trinity_input, only: z_ion
    use trinity_input, only: evolve_density, evolve_temperature, evolve_flow
    use trinity_input, only: te_equal_ti, equal_ion_temps, te_fixed

    implicit none

    real, dimension (:), intent (in) :: flx_vec
    real, dimension (:,:), intent (out) :: pflx, qflx
    real, dimension (:), intent (out) :: lflx

    integer :: is, idx

    idx = 1

    ! if density not evolved, set particle flux to zero (will not be needed)
    if (evolve_density) then
       do is = 1, n_ion_spec
          pflx(:,is) = flx_vec((idx-1)*ncc+1:idx*ncc)
          idx = idx + 1
       end do
       ! use quasineutrality to obtain density of final ion species
       pflx(:,nspec) = (pflx(:,1) - sum(spread(z_ion(:n_ion_spec-1),1,ncc)*pflx(:,2:n_ion_spec),2)) &
            / z_ion(n_ion_spec)
    else
       pflx = 0.0
    end if
    
    if (evolve_temperature) then
       if (te_equal_ti .or. te_fixed) then
          ! if Te=Ti assumed, do not evolve Te so 
          ! qflx_e is not needed
          qflx(:,1) = 0.0
       else
          qflx(:,1) = flx_vec((idx-1)*ncc+1:idx*ncc)
          idx = idx + 1
       end if

       qflx(:,2) = flx_vec((idx-1)*ncc+1:idx*ncc)
       idx = idx + 1

       if (n_ion_spec > 1 .and. equal_ion_temps) then
          ! if minority ion temperatures not evolved, do not need their heat fluxes
          qflx(:,3:) = 0.
       else
          do is = 3, nspec
             qflx(:,is) = flx_vec((idx-1)*ncc+1:idx*ncc)
             idx = idx + 1
          end do
       end if
    else
       qflx = 0.
    end if

    if (evolve_flow) then
       lflx = flx_vec((idx-1)*ncc+1:idx*ncc)
       idx = idx + 1
    else
       lflx = 0.
    end if

  end subroutine get_fluxes_from_flx_vector

  function loglamfnc_array (is, iu, dens, temp)

    use trinity_input, only: m_ion, z_ion

    implicit none

    integer, intent (in) :: is, iu
    real, dimension (:,:), intent (in) :: dens, temp
    real, dimension (size(dens,1)) :: loglamfnc_array

    if (is == 1 .and. iu == 1) then
       ! Coulomb logarithm for thermal electron-electron collisions
       loglamfnc_array = 23.5 - log(10**(3.25)*sqrt(dens(:,1))/temp(:,1)**1.25) &
            - sqrt(1e-5 + (log(1e3*temp(:,1))-2.)**2/16.)
    else if (is == 1 .or. iu == 1) then
       ! Coulomb lograithm for thermal electron-ion or ion-electron collisions
       loglamfnc_array = 24.0 - log(1e4*sqrt(dens(:,1))/temp(:,1))
    else
       ! Coulomb logarithm for thermal ion-ion collisions
       loglamfnc_array = 23.0 - log(10**(2.5)*z_ion(is-1)*z_ion(iu-1)*(m_ion(is-1)+m_ion(iu-1)) &
            * sqrt(dens(:,is)*z_ion(is-1)**2/temp(:,is) + dens(:,iu)*z_ion(iu-1)**2/temp(:,iu)) &
            / (m_ion(is-1)*temp(:,iu) + m_ion(iu-1)*temp(:,is)) )
    end if

    ! if (present(is)) then
    !    if (is==1) then
    !       ! Coulomb logarithm for thermal electron-electron collisions
    !       loglamfnc_array = 23.5 - log(10**(3.25)*sqrt(dens(:,1))/temp(:,1)**1.25) &
    !            - sqrt(1e-5 + (log(1e3*temp(:,1))-2.)**2/16.)
    !    else
    !    ! Coulomb logarithm for thermal ion-ion collisions
    !       loglamfnc_array = 23.0 - log(10**(2.5)*z_ion(is-1)*z_ion(iu-1)*(m_ion(is-1)+m_ion(iu-1)) &
    !            * sqrt(dens(:,is)*z_ion(is-1)**2/temp(:,is) + dens(:,iu)*z_ion(iu-1)**2/temp(:,iu)) &
    !            / (m_ion(is-1)*temp(:,iu) + m_ion(iu-1)*temp(:,is)) )
    !    end if
    ! else
    !    ! Coulomb logarithm for thermal electron-electron collisions
    !    loglamfnc_array = 23.5 - log(10**(3.25)*sqrt(dens(:,1))/temp(:,1)**1.25) &
    !         - sqrt(1e-5 + (log(1e3*temp(:,1))-2.)**2/16.)
    ! end if

  end function loglamfnc_array

  function loglamfnc_scalar (is, iu, dens, temp)

    use trinity_input, only: m_ion, z_ion

    implicit none

    integer, intent (in), optional :: is, iu
    real, dimension (:), intent (in) :: dens, temp
    real :: loglamfnc_scalar

    if (is == 1 .and. iu == 1) then
       ! Coulomb logarithm for thermal electron-electron collisions
       loglamfnc_scalar = 23.5 - log(10**(3.25)*sqrt(dens(1))/temp(1)**1.25) &
            - sqrt(1e-5 + (log(1e3*temp(1))-2.)**2/16.)
    else if (is == 1 .or. iu == 1) then
       ! Coulomb lograithm for thermal electron-ion or ion-electron collisions
       loglamfnc_scalar = 24.0 - log(1e4*sqrt(dens(1))/temp(1))
    else
       ! Coulomb logarithm for thermal ion-ion collisions
       loglamfnc_scalar = 23.0 - log(10**(2.5)*z_ion(is-1)*z_ion(iu-1)*(m_ion(is-1)+m_ion(iu-1)) &
            * sqrt(dens(is)*z_ion(is-1)**2/temp(is) + dens(iu)*z_ion(iu-1)**2/temp(iu)) &
            / (m_ion(is-1)*temp(iu) + m_ion(iu-1)*temp(is)) )
    end if

    ! if (present(is)) then
    !    if (is==1) then
    !       ! Coulomb logarithm for thermal electron-electron collisions
    !       loglamfnc_scalar = 23.5 - log(10**(3.25)*sqrt(dens(1))/temp(1)**1.25) &
    !            - sqrt(1e-5 + (log(1e3*temp(1))-2.)**2/16.)
    !    else
    !    ! Coulomb logarithm for thermal ion-ion collisions
    !       loglamfnc_scalar = 23.0 - log( 10**(2.5)*z_ion(is-1)*z_ion(iu-1)*(m_ion(is-1)+m_ion(iu-1)) &
    !            * sqrt(dens(is)*z_ion(is-1)**2/temp(is) + dens(iu)*z_ion(iu-1)**2/temp(iu)) &
    !            / (m_ion(is-1)*temp(iu) + m_ion(iu-1)*temp(is)) )
    !    end if
    ! else
    !    ! Coulomb logarithm for thermal electron-electron collisions
    !    loglamfnc_scalar = 23.5 - log(10**(3.25)*sqrt(dens(1))/temp(1)**1.25) &
    !         - sqrt(1e-5 + (log(1e3*temp(1))-2.)**2/16.)
    ! end if

  end function loglamfnc_scalar



!  subroutine update_init_nt
!
!    implicit none
!
!    call chease_to_trinity_grid
!
!  end subroutine update_init_nt
  
end module nteqns
