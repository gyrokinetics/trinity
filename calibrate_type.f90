!> This module defines the custom type used for calibrating 
!! fluxes
!!
!! This is free software released under the MIT license.
!! Written by:
!!     Edmund Highcock (edmund.highcock@users.sourceforge.net)
module calibrate_type_module
  !> Stores the state of the calibration
  !! in particular the current factor
  public :: calibrate_type

  private

  type calibrate_type
    logical :: calibrated = .false.
    integer :: ncc_calibrate = -1
    integer :: neval_calibrate = -1
    integer :: flux_option
    real, dimension(:,:), pointer :: qflux_calibration_factor
    real, dimension(:,:), pointer :: reduced_qflux
    real, dimension(:,:), pointer :: full_qflux
    real, dimension(:,:), pointer :: pflux_calibration_factor
    real, dimension(:,:), pointer :: reduced_pflux
    real, dimension(:,:), pointer :: full_pflux
    integer, dimension(:), pointer :: calibration_job
  end type calibrate_type
end module calibrate_type_module
