# This ruby script generates the source file
# nteqns_arrays.f90. It requires ruby >= 1.8.7 which
# is available on every system we can think of.
# To invoke this script:
#
#     ruby nteqns_arrays.rb nteqns_arrays.f90
#
# This is free software released under the MIT license.
# Written by:
#            Edmund Highcock (edmundhighcock@users.sourceforge.net)
#            Michael Barnes
#
# In this script we first define the variables, their type, their 
# dimensions and their help. This script then opens the template
# nteqns_arrays_template.f90 and generates all the variable declarations
# and subroutine contents, subsitutes them into the the template
# and writes the results to nteqns_arrays.f90
#
# It should be very obvious how to add a variable: just copy and paste
# one of the lines below (and add the name of the variable to the peturb
# array if necessary). The one gotcha is that the dimensions need to be
# listed as they go into the netCDF file, not as they are for the variable
# in the code. This is so that the dimensions 't' and 'iter' can be 
# specified as required.
variables = [
  ['dens', 'real', 'rad,tspec,iter,t', 'Density of each species (inout)', '10^20 m^-3'], 
  ["temp",  'real', "rad,tspec,iter,t",  "Temperatures  (inout)", "keV"], 
  ["pres",  'real', "rad,tspec,iter,t",  "Pressures ", "10^20 keV m^-3"], 
  ["nu",  'real', "rad,tspec,iter,t",  "Collisionality ", "aref/vth,r"], 
  ["omega",  'real', "rad,iter,t",  "Toroidal angular velocity  (inout)", "rad s^-1"], 
  ["mom",  'real', "rad,iter,t",  "Toroidal angular momentum density ", "m_p aref^2 rad s^-1 m^-3"], 

  ["tprim",  'real', "rad,tspec,iter,t",  "Logarithmic temperature gradient: 1/T dT/drho ", "1"], 
  ["fprim",  'real', "rad,tspec,iter,t",  "Logarithmic density gradient: 1/n dn/drho ", "1"], 
  ["pprim",  'real', "rad,tspec,iter,t",  "Logarithmic pressure gradient: 1/p dp/drho ", "1"], 
  ["gexb",  'real', "rad,iter,t",  "Perpendicular flow shear (rho/q)(domega/drho)", "rad s^-1"], 

  ["zeff",  'real', "rad,iter,t",  "Average ion species charge (inout)", "e"], 

  ['rad', 'real', 'rad,iter,t', 'Trinity flux label  (inout)', 'aref'], 
  ['rmajor', 'real', 'rad,iter,t', '(Rout+Rin)/2 at the height of the axis  (inout)', 'aref'], 
  ["rmin", 'real', "rad,t",  "(Rout-Rin)/2 at the height of the axis. Note normalisation is amin_ref not aref.  (inout)", "amin_ref"],
  ["qval", 'real', "rad,t",  "Magnetic safety factor  (inout)", "1"],
  ["shat", 'real', "rad,t",  "Magnetic shear (rho/q)(dq/drho)  (inout)", "1"],
  ["kappa", 'real', "rad,t",  "Elongation  (inout)", "1"],
  ["shift", 'real', "rad,t",  "Shafranov shift (d rmajor/d rho) ", "aref"],
  ["betaprim", 'real', "rad,t",  "Pressure gradient  - 8 pi * ptot / Bref^2 *   (1/ptot dptot/drho)  ", "1"],
  ["kapprim", 'real', "rad,t",  "Gradient of elongation (d kappa / d rho)  ", "1"],
  ["delta", 'real', "rad,t",  "Triangularity  (inout) ", "1"],
  ["deltprim", 'real', "rad,t",  "Gradient of triangularity (d delta / d rho)  ", "1"],
  ["beta", 'real', "rad,t",  "Magnetic beta  ", "TBC"],
  ["rhotor", 'real', "rad,t",  "Square root of the toroidal flux (inout)", "psitor_a^0.5"],
  ["drhotordrho", 'real', "rad,t",  "Gradient of square root of the toroidal flux with   respect to the Trinity flux label (rho)", "psitor_a^0.5"],
  ["rhopol", 'real', "rad,t",  "Square root of the poloidal flux (inout)", "psipol_a^0.5"],
  ["drhopoldrho", 'real', "rad,t",  "Gradient of square root of the poloidal flux with   respect to the Trinity flux label (rho)", "psipol_a^0.5"],
  ["psipol", 'real', "rad,t",  "Poloidal flux (0 at the centre, 1 at the edge) (inout)", "psipol_a"],
  ["dpsipoldrho", 'real', "rad,t",  "Gradient of the poloidal flux with respect to rho", "psipol_a"],
  #["rmin", 'real', "rad,t",  "Minor radius (Miller r, or half width of the flux surface at the   midplane, depending on geometry)  ", "amin_ref"],
  ["drmindrho", 'real', "rad,t",  "Gradient of minor radius (Miller r or half-width in the midplane)   wrt rho ", "aref"],
  ["grho", 'real', "rad,t",  "Flux surface average of grad rho   (inout)", "aref"],
  ["area", 'real', "rad,t",  "Area of flux surface  (inout)", "aref^2"],
  ["bmag", 'real', "rad,t",  "Normalising magnetic field . This is   equal to btori(psi)/rmajor(psi)  where btori is the poloidal current flux function,   often referred to as f, I, or T and rmajor is R_center,   that is (Rout+Rin)/2. Note that most flux tube codes make   different choices for the normalising magnetic fields to this. All  quantities within Trinity are normalised using this magnetic  field and then converted to and from the flux code normalisation  at the point the flux code is called. Note also that this is different  to bgeo which is btori(psi)/rmajor(LCFS) (inout)", "Tesla"],
  ["bmag_old", 'real', "rad,t",  "The old normalising field used prior to Oct 2015 ", "Tesla"],
  ["bunit", 'real', "rad,t",  "The normalising field used by GA tools like TGLF, NEO,   equal to (1/r)(d psi_toroidal/dr)/(2 pi)  (inout)", "Tesla"],
  ["btori", 'real', "rad,t",  "Poloidal current flux function (ie. R B_t); known as   f, I or T depending on which paper you read.  (inout)", "Tesla m"],
  ["bgeo", 'real', "rad,t",  "The normalising magnetic field used for GS2 and GRYFX,  equal to btori(psi)/R_centre(LCFS) ", "Tesla"],
  ["current", 'real', "rad,t",  "Toroidal current density (0 when not available) (inout)", "A m^-3"],
  ["bootstrap_current_import", 'real', "rad,t",  "Bootstrap current density (0 when not available). This is what is read in from the input data; it is not calculated within Trinity and thus may not be consistent with the final pressure profile (unless, for example, the equilibrium is being self-consistently updated using CHEASE). (inout)", "A m^-3"],

  # Sources are handled differently: there are no cell centred quantities created.
  
  ["src_part_ionisation", 'real',  "rad,tspec,t",  "Density source resulting from direct ionisation of beam electrons (0 except for species 1) (inout)", "m^-3 s^-1"],
  ["src_part_nbi", 'real',  "rad,tspec,t",  "Density source from neutral beam injection (inout)",  "m^-3 s^-1"],
  ["src_part_wall", 'real',  "rad,tspec,t",  "Density source from the wall (inout)",  "m^-3 s^-1"],
  ["src_part_gas", 'real',  "rad,tspec,t",  "Density source from gas puff (inout)",  "m^-3 s^-1"],
  ["src_part_pellet", 'real',  "rad,tspec,t",  "Density source from pellet injection (inout)",  "m^-3 s^-1"],
  ["src_part_unspecified", 'real',  "rad,tspec,t",  "Unknown or unspecified density source (only used by input formats that do not differentiate between different sources. e.g. analytic/expro (inout)", "m^-3 s^-1"],
  ["src_part_dndt", 'real',  "rad,tspec,t",  "Rate of change of density in a non-steady state situation.  Allows Trinity to converge to a solution of an  evolving profile. (inout)",  "m^-3 s^-1"],
  ["src_heat_nbi", 'real',  "rad,tspec,t",  "Heat source from neutral beam injection (inout)",  "W m^-3"],
  ["src_heat_icrh", 'real',  "rad,tspec,t",  "Heat source from ion cyclotron resonance heating (inout)",  "W m^-3"],
  ["src_heat_ecrh", 'real',  "rad,tspec,t",  "Heat source from electron cyclotron resonance heating (inout)",  "W m^-3"],
  ["src_heat_lh", 'real',  "rad,tspec,t",  "Heat source from lower hybrid waves (inout)",  "W m^-3"],
  ["src_heat_ibw", 'real',  "rad,tspec,t",  "Heat source from ion Bernstein waves (inout)",  "W m^-3"],
  ["src_heat_wall", 'real',  "rad,tspec,t",  "Heat source as a result of particles from the wall (inout)",  "W m^-3"],
  ["src_heat_ohm", 'real',  "rad,tspec,t",  "Ohmic heat source (inout)",  "W m^-3"],
  ["src_heat_unspecified", 'real',  "rad,tspec,t",  "Unknown or unspecified heat source (only used by input formats that do not differentiate between different sources. e.g. analytic/expro (inout)",  "W m^-3"],
  ["src_heat_dwdt", 'real',  "rad,tspec,t",  "Rate of change of heat in a non-steady state situation.  Allows Trinity to converge to a solution of an  evolving profile. (inout)",  "W m^-3"],

  ["src_mom_nbi", 'real',  "rad,t",  "Angular momentum source from neutral beam injection. (inout)",  "N m^-2"],
  ["src_mom_unspecified", 'real',  "rad,t",  "Unknown or unspecified angular momentum source (only used by input formats that do not differentiate between different sources. e.g. analytic/expro  (inout)",  "N m^-2"],

  # Variables for storing the equilibrium
   ["Rofpsitheta", 'real', "rad,ntheta", "Major radius as a function of flux surface label and poloidal angle (0 when not available) (inout) ", "aref"],
   ["Zofpsitheta", 'real', "rad,ntheta", "Vertical height as a function of flux surface label and poloidal angle (0 when not available) (inout) ","aref"],
]

# This is a list of all the variables for which there is a perturbed array
# like dens_perturb (which contains perturbed profiles used for calculating
# flux gradients in the implicit solve).
perturb = 
  ['dens', 'temp', 'pres', 'omega', 'beta', 'betaprim', 'nu', 'fprim', 'tprim', 'gexb', 'pprim']

class String
  def split_fortran(indent)
    str = ""
    need_to_split = false
    for i in 0...length
      str << self[i]
      if i%50 == 0 and i>0
        need_to_split = true
      end
      if need_to_split and self[i] =~ /\s/
        str << "&\n" << " " * indent << "&"
        need_to_split = false
      end
    end
    str
  end
  def split_comment(indent)
    str = ""
    need_to_split = false
    for i in 0...length
      str << self[i]
      if i%50 == 0 and i>0
        need_to_split = true
      end
      if need_to_split and self[i] =~ /\s/
        str << "\n" << " " * indent << "!!"
        need_to_split = false
      end
    end
    str
  end
end

class VariableGenerator
  def initialize(data)
    @name, @type, @dimensions, @description, @units = data
  end 
  def fortran_dimensions
    @dimensions.gsub(
      /,?\bt\b|,?eval|,?\biter\b/, '').gsub(
      /(rad|cc)/, 'n\1').gsub(
      /tspec/, 'nspec')
  end
  def ms_dimensions
    @dimensions =~ /spec/ ? 
      'nglobalrad,nspec' :
      'nglobalrad'
  end
  def append_grid
    '_grid'
  end
  def dimension_declaration
    ([":"] * fortran_dimensions.split(',').size).join(",")
  end
  def fortran_cc_dimensions
    fortran_dimensions.sub(/nrad/, 'ncc')
  end
  def ms_dimension_declaration
    ([":"] * ms_dimensions.split(',').size).join(",")
  end
  def declaration
    <<EOF
  !> #{@description.split_comment(2)}
  #@type, dimension(#{dimension_declaration}), allocatable :: #{@name}#{append_grid}
  logical :: #{@name}#{append_grid}_missing
  #@type, dimension(#{dimension_declaration}), allocatable :: #{@name}_cc
  #@type, dimension(#{ms_dimension_declaration}), allocatable :: #{@name}_ms
EOF
  end
  def allocation
    <<EOF
      allocate(#{@name}#{append_grid}(#{fortran_dimensions})); #{@name}#{append_grid} = 0.0
      allocate(#{@name}_cc(#{fortran_cc_dimensions}))
      allocate(#{@name}_ms(#{ms_dimensions}))
EOF
  end
  def deallocation
    <<EOF
      deallocate(#{@name}#{append_grid})
      deallocate(#{@name}_cc)
      deallocate(#{@name}_ms)
EOF
  end
  def get_cc
    if @dimensions=~/spec/
      <<EOF
      do is = 1, nspec
         call get_cc (#{@name}#{append_grid}(:,is), #{@name}_cc(:,is))
      end do
EOF
    else
      <<EOF
      call get_cc(#{@name}#{append_grid}, #{@name}_cc)
EOF
    end
  end
  def distribute_ms
      <<EOF
      call all_to_group (#{@name}_cc, #{@name}_ms, njobs)
EOF
  end
  def broadcast_grid(group)
    return "" unless group == grid_group
    if @dimensions=~/spec/
      <<EOF
        do is = 1, nspec
           call broadcast (#{@name}#{append_grid}(:,is))
        end do
EOF
    else
      <<EOF
        call broadcast (#{@name}#{append_grid})
EOF
    end
  end
  def trindiag_type
    case @type
    when 'real'
      'gnostics%rtype'
    when 'integer'
      'SDATIO_INT'
    end
  end
  def spline_to_netcdf
    #return "" if @name == "rad"
    if @dimensions=~/spec/
      <<eof
          do is = 1, nspec
             call spline (rad_grid, #{@name}#{append_grid}(:,is), &
                          rad_grid_netcdf, array_2d_temp_real(:,is), &
                          array_2d_temp_dummy(:,is))
          end do
eof
    else
      <<eof
          call spline (rad_grid, #{@name}#{append_grid}, &
                       rad_grid_netcdf, array_1d_temp_real, &
                       array_2d_temp_dummy(:,1))
eof
    end
  end
  def spline_from_netcdf
    return "" if @name == "rad"
    if @dimensions=~/spec/
      <<eof
          do is = 1, nspec
             call spline (rad_grid_netcdf, array_2d_temp_real(:,is), &
                          rad_grid, #{@name}#{append_grid}(:,is), &
                          array_2d_temp_dummy(:nrad,is))
          end do
eof
    else
      <<eof
          call spline (rad_grid_netcdf, array_1d_temp_real, &
                       rad_grid, #{@name}#{append_grid}, &
                       array_2d_temp_dummy(:nrad,1))
eof
    end
  end
  def netcdf_grid_output
    if @dimensions=~/ntheta/
      "array_2d_temp_real(:,:ntheta)"
    elsif @dimensions=~/spec/
      "array_2d_temp_real(:,:nspec_netcdf)"
    else
      "array_1d_temp_real"
    end
  end
  def write_grid
    <<EOF
        if (allocated(#{@name}#{append_grid})) then
          if (gnostics%wryte) then
#{spline_to_netcdf}
          else if (gnostics%reed) then
            !Default values if the variable is missing
            array_1d_temp_real = 0.0
            array_2d_temp_real = 0.0
          end if
          call debug_message(verb_high, &
            'nteqns_arrays::write_grids writing #{@name}#{append_grid}')
          if (gnostics%reed) then
            #{@name}#{append_grid}_missing = .not. &
              variable_exists(gnostics%sfile, "#{@name}#{append_grid}")
          end if
          if (gnostics%reed .or. gnostics%wryte) &
            call create_and_write_variable(gnostics, #{trindiag_type}, &
              "#{@name}#{append_grid}", "#@dimensions", & 
              "#{(@description + " [#{grid_group}]").split_fortran(8)}", &
              "#@units", #{netcdf_grid_output}) 
          if (gnostics%reed) then
#{spline_from_netcdf}
          end if
        end if
EOF
  end
  def grid_group
    case @name
    when /^src/
      'sources'
    when /pres|dens|temp|^([tpf]|beta)prim|gexb|omega|^nu$|^mom|zeff$/
      'profiles'
    else
      'geo'
    end
  end
  def write_grid_geo
    grid_group == 'geo' ? write_grid : ""
  end
  def write_grid_profiles
    grid_group == 'profiles' ? write_grid : ""
  end
  def write_grid_sources
    grid_group == 'sources' ? write_grid : ""
  end
  def write_cc_and_perturb
    <<EOF
      if (allocated(#{@name}_cc)) call create_and_write_variable(gnostics, #{trindiag_type}, &
        "#{@name}_cc", "#{@dimensions.sub(/rad/, 'cc')}", & 
        "#{(@description.sub(/\(inout\)/, '') + ' at cell centres'
          ).split_fortran(8)}", &
        "#@units", #{@name}_cc) 
EOF
  end
end

# A subclass of VariableGenerator for dealing
# with variables that have perturbed values used
# in the implict solve
class VariableGeneratorPerturb < VariableGenerator
  def perturb_dimension_declaration
    ms_dimension_declaration
  end
  def perturb_dimensions
    ms_dimensions.sub(/nglobalrad/, 'njobs')
  end
  def declaration
    super +
      <<EOF
  #@type, dimension(#{perturb_dimension_declaration}), allocatable :: #{@name}_perturb
EOF
  end
  def allocation
    super+
      <<EOF
      allocate(#{@name}_perturb(#{perturb_dimensions}))
EOF
  end
  def deallocation
    super+
      <<EOF
      deallocate(#{@name}_perturb)
EOF
  end
  def distribute_ms
      <<EOF
      call all_to_group (#{@name}_perturb, #{@name}_ms, njobs)
EOF
  end
  def write_cc_and_perturb
    super + <<EOF
      if (allocated(#{@name}_perturb)) call create_and_write_variable(gnostics, #{trindiag_type}, &
        "#{@name}_perturb", "#{@dimensions.sub(/rad/, 'job')}", & 
        "#{(@description.sub(/\(inout\)/, '') + ' for each perturbation'
          ).split_fortran(8)}", &
        "#@units", #{@name}_perturb) 
EOF
  end
end

class VariableGeneratorSource < VariableGenerator

  def declaration
    <<EOF
  !> #{@description.split_comment(2)}
  #@type, dimension(#{dimension_declaration}), allocatable :: #{@name}
  logical :: #{@name}#{append_grid}_missing
EOF
  end
  def allocation
    <<EOF
      allocate(#{@name}(#{fortran_dimensions})); #@name = 0.0
EOF
  end
  def deallocation
    <<EOF
      deallocate(#{@name})
EOF
  end
  def append_grid
    ''
  end
  def write_cc_and_perturb
  end
  def get_cc
  end
  def distribute_ms
  end
end

class VariableGeneratorEqbm < VariableGeneratorSource
  def allocation
  end
  def spline_to_netcdf
    <<eof
          do is = 1, ntheta
             call spline (rad_grid, #{@name}#{append_grid}(:,is), &
                          rad_grid_netcdf, array_2d_temp_real(:,is), &
                          array_2d_temp_dummy(:,is))
          end do
eof
  end
  def spline_from_netcdf
      <<eof
          do is = 1, ntheta
             call spline (rad_grid_netcdf, array_2d_temp_real(:,is), &
                          rad_grid, #{@name}#{append_grid}(:,is), &
                          array_2d_temp_dummy(:nrad,is))
          end do
eof
  end
  def broadcast_grid(group)
    return "" unless group == grid_group
    <<EOF
        do is = 1, ntheta
           call broadcast (#{@name}#{append_grid}(:,is))
        end do
EOF
  end
end 

variable_generators = 
  variables.map do |data| 
    has_perturb =  perturb.include?(data[0]) 
    is_src = data[0] =~ /^src/
    is_eqbm = data[0] =~ /[RZ]ofpsitheta/
    if has_perturb
      VariableGeneratorPerturb.new(data)
    elsif is_src
      VariableGeneratorSource.new(data)
    elsif is_eqbm
      VariableGeneratorEqbm.new(data)
    else
      VariableGenerator.new(data)
    end
  end

template = File.read('templates/nteqns_arrays_template.f90')

template.sub!(/[\t ]+![\t ]+TEMPLATE_DECLARATIONS/, 
  variable_generators.map{|vg| vg.declaration}.join("\n"))
template.sub!(/[\t ]+![\t ]+TEMPLATE_ALLOCATE/, 
  variable_generators.map{|vg| vg.allocation}.join("\n"))
template.sub!(/[\t ]+![\t ]+TEMPLATE_DEALLOCATE/, 
  variable_generators.map{|vg| vg.deallocation}.join("\n"))
#template.sub!(/[\t ]+![\t ]+TEMPLATE_ALLOCATE_NETCDF_ARRAYS/, 
  #variable_generators.map{|vg| vg.netcdf_allocation}.join("\n"))
#template.sub!(/[\t ]+![\t ]+TEMPLATE_DEALLOCATE_NETCDF_ARRAYS/, 
  #variable_generators.map{|vg| vg.netcdf_deallocation}.join("\n"))
template.sub!(/[\t ]+![\t ]+TEMPLATE_GET_CC_ALL/, 
  variable_generators.map{|vg| vg.get_cc}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_DISTRIBUTE_MS/, 
  variable_generators.map{|vg| vg.distribute_ms}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_BROADCAST_GEO/, 
  variable_generators.map{|vg| vg.broadcast_grid('geo')}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_BROADCAST_PROFILES/, 
  variable_generators.map{|vg| vg.broadcast_grid('profiles')}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_BROADCAST_SOURCES/, 
  variable_generators.map{|vg| vg.broadcast_grid('sources')}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_WRITE_GRIDS_GEO/, 
  variable_generators.map{|vg| vg.write_grid_geo}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_WRITE_GRIDS_PROFILES/, 
  variable_generators.map{|vg| vg.write_grid_profiles}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_WRITE_GRIDS_SOURCES/, 
  variable_generators.map{|vg| vg.write_grid_sources}.join(""))
template.sub!(/[\t ]+![\t ]+TEMPLATE_WRITE_CC_AND_PERTURB/, 
  variable_generators.map{|vg| vg.write_cc_and_perturb}.join(""))

template.sub!(/\A.*TEMPLATE_END_HEAD/m, 
              <<EOF
! DO NOT EDIT THIS FILE
! This file is automatically generated by nteqns_arrays.rb
! Any changes you make will be lost
EOF
             )

File.open(ARGV[-1], 'w'){|f| f.puts template}

