!> This module implements the reduced/full calibration
!! for faster Trinity running.
!!
!! This is free software released under the MIT license.
!! Written by:
!!     Edmund Highcock (edmund.highcock@users.sourceforge.net)
module calibrate
  use calibrate_type_module, only: calibrate_type
  use flux_results, only: flux_results_type

  implicit none

  !> Return the calibration factor for the given 
  !! job. 
  !public :: calibration_factor

  !> Set inputs and allocate arrays etc
  public :: init_calibrate

  !> Deallocate arrays etc
  public :: finish_calibrate

  !> Store the results of the last call to get_fluxes
  public :: calibrate_store

  !> Adjust the last calculated value of the fluxes 
  public :: calibrate_adjust

  !> Load the calibration factor from the itercalib_file
  public :: calibrate_restart

  private 

  integer :: calib_unit

 
contains 
  subroutine init_calibrate(calib)
    use trinity_input, only: ncc_calibrate
    use trinity_input, only: neval_calibrate
    use trinity_input, only: nspec
    use trinity_input, only: ncc
    use trinity_input, only: flux_option_switch
    use file_utils_trin, only: open_output_file, flush_output_file
    use mp_trin, only: iproc, proc0
    type(calibrate_type), intent(inout) :: calib
    integer :: i

    calib%calibrated = .false.

    calib%ncc_calibrate = ncc_calibrate
    calib%neval_calibrate = neval_calibrate
!    write (*,*) 'VALUE OF NEVAL_CALIB', neval_calibrate, iproc

    ! This is temporary, obviously
    calib%flux_option = flux_option_switch

    allocate(calib%qflux_calibration_factor(ncc, nspec))
    allocate(calib%pflux_calibration_factor(ncc, nspec))

    calib%qflux_calibration_factor = 1.0
    calib%pflux_calibration_factor = 1.0

    allocate(calib%reduced_qflux (calib%ncc_calibrate, nspec))
    allocate(calib%reduced_pflux (calib%ncc_calibrate, nspec))

    allocate(calib%full_qflux    (calib%ncc_calibrate, nspec))
    allocate(calib%full_pflux    (calib%ncc_calibrate, nspec))

    allocate(calib%calibration_job     (calib%ncc_calibrate))

    if (ncc_calibrate .eq. 1) then
      calib%calibration_job(1) = ncc/2
    else if (ncc_calibrate .eq. ncc) then 
      do i = 1,ncc
        ! job numbers are zero based
        calib%calibration_job(i) = i-1
      end do
    else if (ncc_calibrate .lt. ncc) then
      calib%calibration_job(1) = 0
      calib%calibration_job(ncc_calibrate) = ncc-1
      if (ncc_calibrate .gt. 2) then
        do i = 2,ncc_calibrate-1
          ! job numbers are zero based
          ! Note in the line below we are dealing with fences not fenceposts.
          ! E.g. if ncc = 8 and ncc_calibrate = 3, the width of the long fence
          ! is 7 and the width of the short fence is two, and we want the
          ! centrepoint to be at 1 / 2 * 7 = 3.5 = job 3 or 4 (either is ok, but
          ! we choose 4 by using anint instead of floor).
          ! ie. we need (i-1) / (ncc_calibrate -1) * (ncc-1)
          calib%calibration_job(i)=anint(real(i-1)/real(ncc_calibrate-1)*real(ncc-1)) 
        end do
      end if

    else
      write (*,*) "ncc_calibrate = ", ncc_calibrate, " not supported!"
      stop 1
    end if

    
    if (proc0) call open_output_file (calib_unit,".calib")
    if (proc0) write(calib_unit,*) 'calibration_job', calib%calibration_job
    if (proc0) call flush_output_file(calib_unit) 

  end subroutine init_calibrate

  subroutine finish_calibrate(calib)
    use file_utils_trin, only: close_output_file
    use mp_trin, only: proc0
    type(calibrate_type), intent(inout) :: calib
    deallocate(calib%qflux_calibration_factor)
    deallocate(calib%pflux_calibration_factor)
    deallocate(calib%reduced_qflux)
    deallocate(calib%reduced_pflux)
    deallocate(calib%full_qflux)
    deallocate(calib%full_pflux)
    deallocate(calib%calibration_job)

    if (proc0) call close_output_file(calib_unit)
  end subroutine finish_calibrate

  subroutine calibrate_store(calib, model, gnostics, flx)
    use trinity_input,only: nspec, ncc
    use trinity_input, only: calib_option_switch
    use trinity_input, only: calib_option_piecewise_linear
    use trinity_input, only: calib_option_spline
    use mp_trin, only: proc0
    use file_utils_trin, only: open_output_file, close_output_file
    use trinity_input, only: calibrate_abs, ncc
    use nteqns_arrays, only: rad_cc
    use interp, only: spline, plinterp
    use trindiag_config, only: trindiag_type
    type(calibrate_type), intent(inout) :: calib
    character(*), intent(in) :: model
    type(trindiag_type), intent(inout) :: gnostics
    type(flux_results_type), intent(in) :: flx
    integer :: i, restart_unit
    !> Used to store the calibration factor before
    !! any interpolation
    real, dimension(calib%ncc_calibrate,nspec) :: temp_qflux_factor
    real, dimension(calib%ncc_calibrate,nspec) :: temp_pflux_factor
    real, dimension(calib%ncc_calibrate) :: rad_calibrate
    real, dimension(ncc) :: dummy
    integer :: ix
    if (proc0) then 
      if (model .eq. "reduced") then 
        do i = 1,calib%ncc_calibrate
          ! Remember job numbers are zero based.
          calib%reduced_qflux(i,:) = flx%q_cc(calib%calibration_job(i)+1, :, 1)
          calib%reduced_pflux(i,:) = flx%p_cc(calib%calibration_job(i)+1, :, 1)
        end do
        !write (calib_unit, *) "qflx_cc", qflx_cc
      else if (model .eq. "calibrate") then
        do i = 1,calib%ncc_calibrate
          ! Remember job numbers are zero based
          ix = calib%calibration_job(i)+1
          ! The radius at the calibration points,
          ! used for spline fits
          rad_calibrate(i) = rad_cc(ix)

          !write (*,*) 'SETTING FULL QFLUX', qflx_cc
          calib%full_qflux(i,:) = flx%q_cc(ix, :, 1)
          !write (*,*) 'SETTING FULL QFLUX2', calib%full_qflux
          calib%full_pflux(i,:) = flx%p_cc(ix, :, 1)
          if (any(abs(calib%reduced_qflux(i,1:nspec)) .lt. 1.0e-10) .or. &
              any(abs(calib%reduced_pflux(i,1:nspec)) .lt. 1.0e-10)) then
            ! If any of the new reduced fluxes are zero use the old
            ! calibration factor.
            write(calib_unit, *) 'Using old factor for job', i
            temp_qflux_factor(i,1:nspec) = calib%qflux_calibration_factor(ix,:)
            temp_pflux_factor(i,1:nspec) = calib%pflux_calibration_factor(ix,:)
          else
            temp_qflux_factor(i,:) = calib%full_qflux(i,:)/calib%reduced_qflux(i,:)
            temp_pflux_factor(i,:) = calib%full_pflux(i,:)/calib%reduced_pflux(i,:)
          end if

        end do

          !write (*,*) 'SETTING FULL QFLUX4', calib%ncc_calibrate, ncc, calib%full_qflux
        if (calib%ncc_calibrate.eq.1) then
          do i = 1,nspec
            calib%qflux_calibration_factor(:,i) = temp_qflux_factor(1,i)
            calib%pflux_calibration_factor(:,i) = temp_pflux_factor(1,i)
          end do
        else if (calib%ncc_calibrate.eq.ncc) then
          calib%qflux_calibration_factor(:,:)=temp_qflux_factor(:,1:nspec)
          calib%pflux_calibration_factor(:,:)=temp_pflux_factor(:,1:nspec)
        else
          do i = 1,nspec
            select case(calib_option_switch)
            case(calib_option_spline)
              call spline(rad_calibrate, temp_qflux_factor(:,i), &
                          rad_cc, calib%qflux_calibration_factor(:,i), dummy) 
              call spline(rad_calibrate, temp_pflux_factor(:,i), &
                          rad_cc, calib%pflux_calibration_factor(:,i), dummy) 
            case(calib_option_piecewise_linear)
              call plinterp(rad_calibrate, temp_qflux_factor(:,i), &
                          rad_cc, calib%qflux_calibration_factor(:,i), dummy) 
              call plinterp(rad_calibrate, temp_pflux_factor(:,i), &
                          rad_cc, calib%pflux_calibration_factor(:,i), dummy) 
            end select
          end do
        end if
        if (calibrate_abs) then 
          calib%qflux_calibration_factor = abs(calib%qflux_calibration_factor)
          calib%pflux_calibration_factor = abs(calib%pflux_calibration_factor)
        end if
          !write (*,*) 'SETTING FULL QFLUX3', calib%full_qflux

        call calibrate_write(calib, gnostics)
        call open_output_file (restart_unit,".itercalib")
        write (restart_unit, *) calib%ncc_calibrate
        write (restart_unit, *) calib%qflux_calibration_factor
        write (restart_unit, *) calib%pflux_calibration_factor
        call close_output_file(restart_unit)
      end if
    end if ! if proc0
    calib%calibrated = .true.
  end subroutine calibrate_store

  subroutine calibrate_write(calib, gnostics)
    use calibrate_type_module, only: calibrate_type
    use mp_trin, only: proc0
    use file_utils_trin, only: flush_output_file
    use trindiag_calibrate, only: write_calib
    use trindiag_calibrate, only: trindiag_advance_calib
    use trindiag_config, only: trindiag_type
    type(trindiag_type), intent(inout) :: gnostics
    type(calibrate_type), intent(inout) :: calib
    if (proc0) then
      write (*,*) 'I AM PROC0'
      write (calib_unit, *) "reduced_qflux", calib%reduced_qflux
      write (calib_unit, *) "full_qflux", calib%full_qflux
      write (calib_unit, *) "qflux factor", calib%qflux_calibration_factor
      write (calib_unit, *) "reduced_pflux", calib%reduced_pflux
      write (calib_unit, *) "full_pflux", calib%full_pflux
      write (calib_unit, *) "pflux factor", calib%pflux_calibration_factor
      call flush_output_file(calib_unit)
    end if
    call write_calib(gnostics, calib)
    call trindiag_advance_calib(gnostics)

  end subroutine calibrate_write

  subroutine calibrate_adjust(calib, flx)
    use trinity_input,only: nspec, ncc
    use mp_trin, only: abort_mp, proc0
    implicit none
    type(calibrate_type), intent(in) :: calib
    type(flux_results_type), intent(inout) :: flx
    integer :: i, ix

    if (.not. calib%calibrated) then
      write(*,*) 'ERROR: calibrate_adjust called with no calibration'
      call abort_mp
    end if
      !write (calib_unit, *) "qflx_cc calc", qflx_cc
    !if (proc0) write (*,*) 'ADJUST ', qflx_cc
    do i = 1,nspec
      do ix = 1,ncc
        flx%q_cc(ix, i, :) = flx%q_cc(ix, i, :)*calib%qflux_calibration_factor(ix, i) 
        flx%p_cc(ix, i, :) = flx%p_cc(ix, i, :)*calib%pflux_calibration_factor(ix, i) 
      end do
    end do
    !if (proc0) write (*,*) 'ADJUST 2', qflx_cc
  end subroutine calibrate_adjust

  subroutine calibrate_restart(calib, gnostics)
    use file_utils_trin, only: get_unused_unit
    use trinity_input, only: itercalib_file
    use mp_trin, only: proc0, broadcast
    use trindiag_config, only: trindiag_type
    implicit none
    type(trindiag_type), intent(inout) :: gnostics
    type(calibrate_type), intent(inout) :: calib
    integer :: restart_unit, ierr
    integer :: ncc_calibrate_temp
    if (proc0) then
      call get_unused_unit (restart_unit)
      open (unit=restart_unit, file=trim(itercalib_file), status="old", &
           action="read",iostat=ierr)
      if (ierr.ne.0) then
         write(*,'(A,A)') "I/O ERROR: could not open ", trim(itercalib_file)
         calib%calibrated = .false.
      else
        read (restart_unit, *) ncc_calibrate_temp
        if (ncc_calibrate_temp.ne.calib%ncc_calibrate) then
          write (*,*) "Restarted with different ncc_calibrate, recalibrating"
          calib%calibrated = .false.
        else
          read (restart_unit, *) calib%qflux_calibration_factor
          read (restart_unit, *) calib%pflux_calibration_factor
          calib%calibrated = .true.
        end if
      end if

      !do ix = 1, 4*nrad
         !read (restart_unit,*) nt_save(ix), ntold_save(ix), rhs_save(ix), rmserr
      !end do
      close (restart_unit)
    end if
    if (proc0) write(calib_unit, *) 'Restarted.'
    call calibrate_write(calib, gnostics)
    call broadcast(calib%calibrated)
  end subroutine calibrate_restart



end module calibrate
