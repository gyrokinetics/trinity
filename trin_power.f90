module trin_power

  implicit none

  public :: alpha_power, radiated_power

contains

!  subroutine alpha_power (dens, densi, temp, zeff, alph_pwr)
  subroutine alpha_power (dens, temp, alph_pwr)

!    use trinity_input, only: deuterium, tritium, impurity
!    use trinity_input, only: tritium_fraction
    use trinity_input, only: z_ion, m_ion, n_ion_spec

    implicit none

!    real, dimension (:), intent (in) :: dens, zeff
!    real, dimension (:,:), intent (in) :: temp, densi
    real, dimension (:,:), intent (in) :: dens, temp
    real, dimension (:,:), intent (out) :: alph_pwr
    
    integer :: is, nr
    real :: zimp, mimp
    real, dimension (:), allocatable :: x, y, Ecrit, z2ova, sigv, frac, pwr_tot
    real, dimension (size(dens,1)) :: ddens, tdens, idens
!    real, dimension (:), allocatable, save :: dfrac, tfrac

!    logical, save :: first = .true.

    nr = size(dens,1)

    allocate (x(nr), y(nr), Ecrit(nr), z2ova(nr))
    allocate (sigv(nr), frac(nr), pwr_tot(nr))

    call get_sigv (temp(:,2), sigv)

    ! total alpha power in units of Watts/cm^3 = MW/m^3

    ! Use quasineutrality and assume only species present are deuterium and possibly
    ! tritium + impurity:
    ! ne = n_D + n_T + Z_I * n_I
    ! also, zeff * ne = n_D + n_T + n_I * Z_I**2

    ! if (first) then
    !    ! default case is n_D = ne, with n_T = n_I = 0
    !    ddens = dens ; tdens = 0.0 ; idens = 0.0
    !    ! if deuterium is given from experiment, then use that density
    !    if (deuterium <= size(densi,2)) then
    !       !write(*,*) 'densi deuterium', densi(:,deuterium)
    !       if (any(densi(:,deuterium) > epsilon(0.0))) ddens = densi(:,deuterium)
    !    end if
    !    ! if tritium is given from experiment, then use that density
    !    if (tritium <= size(densi,2)) then
    !       if (any(densi(:,tritium) > epsilon(0.0))) tdens = densi(:,tritium)
    !    end if
    !    ! if impurity is given from experiment, then use that density
    !    if (impurity <= size(densi,2)) then
    !       if (any(densi(:,impurity) > epsilon(0.0))) idens = densi(:,impurity)
    !    end if

    !    allocate (dfrac(nr), tfrac(nr))
    !    dfrac = ddens/dens
    !    tfrac = tdens/dens
    !    !dfrac = 0.5
    !    !tfrac = 0.5
    !    if (tritium_fraction > 0.0) then
    !      dfrac = 1.0 - tritium_fraction
    !      tfrac = tritium_fraction
    !    end if

    !    first = .false.
    ! else
    !    ddens = dfrac*dens
    !    tdens = tfrac*dens
    !   !write (*,*) 'ddens', ddens, 'tdens', tdens
    ! end if

    ddens = 0. ; tdens = 0. ; idens = 0. ; mimp = 1.0
    do is = 1, n_ion_spec
       ! identify deuterium and tritium if present
       if (abs(z_ion(is)-1.0) < epsilon(0.)) then
          if (abs(m_ion(is)-2.0) < epsilon(0.)) then
             ddens = dens(:,is+1)
          else if (abs(m_ion(is)-3.0) < epsilon(0.)) then
             tdens = dens(:,is+1)
          else
             idens = dens(:,is+1)
             zimp = z_ion(is)
             mimp = m_ion(is)
          end if
       else
          idens = dens(:,is+1)
          zimp = z_ion(is)
          mimp = m_ion(is)
       end if
    end do

    ! D-T reaction, assuming 50-50 D-T split
!    pwr_tot = 5.6e-13*sigv*1.e28*dens**2/4.
    ! D-T reaction (neglects power from D-D reaction since
    ! this is negligible for realistic parameters)
    pwr_tot = 5.6e-13*sigv*1.e28*ddens*tdens
   
    ! hack for now
    ! assumes pure 50:50 D:T plasma, slowing
    ! down all on electrons above Ecrit, and all on ions below Ecrit
!    Ecrit=33.0*temp(:,2)
!    frac=Ecrit/3500

    ! MAB: is following formula okay? differs from above hack by factor of ~2

    ! z2ova = sum_i n_i Z_i**2/A_i
    ! Assumes 50:50 DT plasma for now:
    ! ignoring self-collisions and fast ion contribution to z2ova 
    ! (as perhaps we should?):
!    z2ova=((0.5/2+0.5/3)*dens) !+ denHe_r(k) &
!         +Z_imp**2/A_imp*denc_r(k))/den_r(k)

    ! use ne*zeff = n_D + n_T + n_I * z_I**2 and assume carbon impurity
!    z2ova=ddens/2+tdens/3 + (dens*zeff-(ddens+tdens))/6
    z2ova=ddens/2+tdens/3+idens*zimp**2/mimp
    Ecrit=4*14.8*temp(:,1)*z2ova**(2./3.)
    x=3.5e3/Ecrit
! GWH: Here's the formula I got long ago from Goldston for frac:
    y=sqrt(x)
    frac=alog((1.+y**3)/(1.+y)**3)/(3.*x) &
         +2.*atan2(2.*y-1.,sqrt(3.))/(sqrt(3.)*x) &
         -2.*atan2(-1.,sqrt(3.))/(sqrt(3.)*x)

    alph_pwr(:,2) = frac*pwr_tot
    alph_pwr(:,1) = pwr_tot - alph_pwr(:,2)
    if (n_ion_spec > 1) alph_pwr(:,3:) = 0.

    deallocate (x, y, Ecrit, z2ova, sigv, frac)

  end subroutine alpha_power

  subroutine radiated_power (dens, temp, zeff, rad_pwr)

    implicit none

    real, dimension (:), intent (in) :: dens, temp, zeff
    real, dimension (:,:), intent (out) :: rad_pwr

    rad_pwr = 0.0

    ! Bremsstrahlung radiation from NRL formulary (in units of W/cm^3 = MW/m^3)
    rad_pwr(:,1) = 5.35e-3*dens**2*zeff*sqrt(temp)

  end subroutine radiated_power

  subroutine get_sigv (ti, sig)

    use interp, only: fpintrp

    implicit none

    real, dimension (:), intent (in) :: ti
    real, dimension (:), intent (out) :: sig

    integer :: ix
    real :: delta_E
    real, dimension (:), allocatable :: sigv_rad
    real, dimension (:), allocatable :: logT
    real, dimension (300) :: sigv_table

    sigv_table = (/ &
         0.00000000E+00, 0.00000000E+00, 0.00000000E+00, 0.00000000E+00, &
         0.00000000E+00, 0.00000000E+00, 0.00000000E+00, 4.78733916E-32, &
         5.98646770E-32, 7.55710683E-32, 1.19214523E-31, 1.49225223E-31, &
         2.05170290E-31, 2.44748362E-31, 3.15580596E-31, 4.02007313E-31, &
         5.01097725E-31, 6.23417175E-31, 7.74121005E-31, 9.59446671E-31, &
         1.20350017E-30, 1.50475211E-30, 1.85448678E-30, 2.28162682E-30, &
         2.80241671E-30, 3.43631831E-30, 4.20662747E-30, 5.14114285E-30, &
         6.27299495E-30, 7.64165969E-30, 9.29399775E-30, 1.12775634E-29, &
         1.36383375E-29, 1.65621128E-29, 1.99930050E-29, 2.41701695E-29, &
         2.90836306E-29, 3.50406162E-29, 4.20930557E-29, 5.04896859E-29, &
         6.04718049E-29, 7.23221361E-29, 8.63689182E-29, 1.02995616E-28, &
         1.22652656E-28, 1.45859221E-28, 1.73201600E-28, 2.05383240E-28, &
         2.43209394E-28, 2.87607496E-28, 3.39649855E-28, 4.00569402E-28, &
         4.71781529E-28, 5.54928354E-28, 6.51868401E-28, 7.64746144E-28, &
         8.96013581E-28, 1.04847087E-27, 1.22531486E-27, 1.43018470E-27, &
         1.66723345E-27, 1.94116829E-27, 2.25735610E-27, 2.62184759E-27, &
         3.04152679E-27, 3.52417446E-27, 4.07858652E-27, 4.71467650E-27, &
         5.44363675E-27, 6.27807594E-27, 7.23214967E-27, 8.32179230E-27, &
         9.56486144E-27, 1.09814060E-26, 1.25938316E-26, 1.44272814E-26, &
         1.65097417E-26, 1.88725434E-26, 2.15505906E-26, 2.45827624E-26, &
         2.80123336E-26, 3.18874156E-26, 3.62613196E-26, 4.11931116E-26, &
         4.67493856E-26, 5.30018817E-26, 6.00314028E-26, 6.79268835E-26, &
         7.67863708E-26, 8.67182406E-26, 9.78419435E-26, 1.10288671E-25, &
         1.24203253E-25, 1.39744343E-25, 1.57086513E-25, 1.76421518E-25, &
         1.97959024E-25, 2.21928759E-25, 2.48582570E-25, 2.78195964E-25, &
         3.11068809E-25, 3.47530182E-25, 3.87937117E-25, 4.32680494E-25, &
         4.82185953E-25, 5.36914756E-25, 5.97370344E-25, 6.64099595E-25, &
         7.37695042E-25, 8.18799508E-25, 9.08110938E-25, 1.00638437E-24, &
         1.11443701E-24, 1.23315368E-24, 1.36348661E-24, 1.50646686E-24, &
         1.66320791E-24, 1.83490703E-24, 2.02284880E-24, 2.22850109E-24, &
         2.45321286E-24, 2.69856142E-24, 2.96643353E-24, 3.25859468E-24, &
         3.57700616E-24, 3.92378823E-24, 4.30117810E-24, 4.71159048E-24, &
         5.15759390E-24, 5.64190085E-24, 6.16742815E-24, 6.73724447E-24, &
         7.35464094E-24, 8.02305935E-24, 8.74617580E-24, 9.52787385E-24, &
         1.03722240E-23, 1.12835390E-23, 1.22663224E-23, 1.33253359E-23, &
         1.44655775E-23, 1.56922436E-23, 1.70107173E-23, 1.84267100E-23, &
         1.99461145E-23, 2.15750097E-23, 2.33197523E-23, 2.51868378E-23, &
         2.71830329E-23, 2.93152640E-23, 3.15906481E-23, 3.40163259E-23, &
         3.65998139E-23, 3.93486849E-23, 4.22703985E-23, 4.53727328E-23, &
         4.86634756E-23, 5.21503512E-23, 5.58411032E-23, 5.97435666E-23, &
         6.38652070E-23, 6.82136544E-23, 7.27963999E-23, 7.76203222E-23, &
         8.26926221E-23, 8.80200648E-23, 9.36087531E-23, 9.94647831E-23, &
         1.05593879E-22, 1.12001096E-22, 1.18691078E-22, 1.25668130E-22, &
         1.32935684E-22, 1.40496683E-22, 1.48353687E-22, 1.56508009E-22, &
         1.64960345E-22, 1.73711374E-22, 1.82761073E-22, 1.92106816E-22, &
         2.01746973E-22, 2.11678164E-22, 2.21896904E-22, 2.32398271E-22, &
         2.43176358E-22, 2.54224954E-22, 2.65536538E-22, 2.77107247E-22, &
         2.88914841E-22, 3.00962830E-22, 3.13235713E-22, 3.25722182E-22, &
         3.38410145E-22, 3.51285996E-22, 3.64336179E-22, 3.77546583E-22, &
         3.90902086E-22, 4.04387013E-22, 4.17985789E-22, 4.31681045E-22, &
         4.45456399E-22, 4.59294610E-22, 4.73177653E-22, 4.87087253E-22, &
         5.01006242E-22, 5.14915640E-22, 5.28797118E-22, 5.42632855E-22, &
         5.56403514E-22, 5.70091374E-22, 5.83678461E-22, 5.97144027E-22, &
         6.10477669E-22, 6.23655054E-22, 6.36661389E-22, 6.49479558E-22, &
         6.62094063E-22, 6.74488342E-22, 6.86647150E-22, 6.98556299E-22, &
         7.10201502E-22, 7.21568924E-22, 7.32665130E-22, 7.43420296E-22, &
         7.53880516E-22, 7.64015548E-22, 7.73815296E-22, 7.83269914E-22, &
         7.92371425E-22, 8.01111398E-22, 8.09483118E-22, 8.17479769E-22, &
         8.25096000E-22, 8.32327015E-22, 8.39168471E-22, 8.45616885E-22, &
         8.51668572E-22, 8.57323228E-22, 8.62577622E-22, 8.67432764E-22, &
         8.71887645E-22, 8.75943273E-22, 8.79600861E-22, 8.82858996E-22, &
         8.85724644E-22, 8.88206185E-22, 8.90295240E-22, 8.92000897E-22, &
         8.93328000E-22, 8.94280894E-22, 8.94865434E-22, 8.95086870E-22, &
         8.94952069E-22, 8.94467090E-22, 8.93653945E-22, 8.92473860E-22, &
         8.90980754E-22, 8.89165541E-22, 8.87038217E-22, 8.84605345E-22, &
         8.81875911E-22, 8.78856884E-22, 8.75559268E-22, 8.71990133E-22, &
         8.68159273E-22, 8.64074362E-22, 8.59745498E-22, 8.55180252E-22, &
         8.50388421E-22, 8.45378637E-22, 8.40161249E-22, 8.34742318E-22, &
         8.29136937E-22, 8.23346016E-22, 8.17377987E-22, 8.11253145E-22, &
         8.04975932E-22, 7.98541453E-22, 7.91974495E-22, 7.85277482E-22, &
         7.78459200E-22, 7.71519799E-22, 7.64482806E-22, 7.57348121E-22, &
         7.50111704E-22, 7.42807029E-22, 7.35423038E-22, 7.27958067E-22, &
         7.20447101E-22, 7.12881054E-22, 7.05265883E-22, 6.97596691E-22, &
         6.89906395E-22, 6.82186363E-22, 6.74441692E-22, 6.66680008E-22, &
         6.58905146E-22, 6.51161435E-22, 6.43343155E-22, 6.35568545E-22 /)

    ! assumes D-T reaction

    ! old formula for
    ! fusion cross section from NRL formulary, multiplied
    ! by 1.3 fac to make more accurate for higher energies
    ! (good for few keV --> approx 30 keV)
!    sig = 1.3*3.68e-12*ti**(-2./3)*exp(-19.94*ti**(-1./3))

    ! improved fusion cross section from lookup table

    delta_E = (log(200.0)-log(0.2))/(300-1)
    allocate (sigv_rad(300))
    do ix = 1, 300
       sigv_rad(ix) = log(0.2) + delta_E*(ix-1)
    end do

    allocate (logT(size(ti)))
    logT = log(ti)

    ! sig is in cgs (i.e. cm^3 / s)
    do ix = 1, size(ti)
       sig(ix)=1.e6*fpintrp(logT(ix),sigv_table,sigv_rad,300)
    end do

    deallocate (logT, sigv_rad)

  end subroutine get_sigv

end module trin_power
