!> A module for getting fluxes from global GENE.
!! As of March 2016, this hasn't been run or tested for 
!! a long time. 
module fluxes_gene
  use flux_results, only: flux_results_type
  implicit none
  type fluxes_gene_type
    logical :: first = .true.
  end type fluxes_gene_type
  contains
    subroutine get_fluxes_gene(flx, gene)
      use mp_trin, only: abort_mp, broadcast
      use nteqns_arrays, only: temp_ms, dens_ms, tprim_ms, fprim_ms
      use nteqns_arrays, only: shat_ms, qval_ms, drhotordrho_ms, omega_ms
      use nteqns_arrays, only: gexb_ms
      use trinity_input, only: m_ion, z_ion, nspec, nglobalrad, nrad_global
      use trinity_input, only: aspr, bmag, ncc
      use norms, only: psitor_a, aref, vtref
      use constants_trin, only: pi
      implicit none
      type(flux_results_type), intent(inout), target :: flx
      type(fluxes_gene_type), intent(inout) :: gene
      real, dimension(:,:), pointer :: pflx, qflx
      integer :: is

#ifdef USE_GENE
    interface

!to do in GENE:
!* momentum flux?
        subroutine tringene (mpi_comm, job, ncalls, electrostatic, & !administrative stuff
             &nglobalrad, nrad_global, rad_in_gene, rad_out_gene,& !grids
             &nspec, temp_gene, dens_gene, mass_gene, charge_gene, vrot_gene, & !profiles
             &tprim_gene, fprim_gene, & !only required in local sims
             &qval_gene, shat_gene,aspr,agene, bmag,& !only required for analytical geometries
             &dvdrho, grho, pflx_gene, qflx_gene, temp_bin, dens_bin) ! return values


         integer :: mpi_comm ! mpi communicator associated with the gene simulation
         integer, intent (in) :: job, ncalls ! for GENE output files index
         integer, intent (in) :: nglobalrad ! number of radial points used for input profiles (1 for local)
         integer, intent (in) :: nrad_global ! number of radial points at which each GENE realization should return fluxes, etc. (1 for local)
         logical, intent (in) :: electrostatic
         real, dimension (nglobalrad), intent (in) :: rad_in_gene !grid for input profiles (r/a for analytical geom., rho_tor else)
         real, dimension (nrad_global), intent (in) :: rad_out_gene !grid for output profiles (r/a for analytical geom., rho_tor else)
         integer, intent (in) :: nspec !number of species
         real, dimension (nglobalrad,nspec), intent (inout) :: temp_gene, dens_gene !temperature/density profile in keV and 1E20m^-3
         real, dimension (nspec), intent (in) :: mass_gene, charge_gene !mass in proton mass, charge in elementary charge
         real, dimension (nglobalrad), intent (in) :: vrot_gene !tor. angular vel. vtor (global) or g_exb (local)
         real, dimension (nglobalrad,nspec), intent (inout) :: tprim_gene, fprim_gene !input will only be used in local sims.
         real, dimension (nglobalrad), intent (in) :: qval_gene, shat_gene !for analytical geometries *only*
         real, intent(in) :: aspr, agene, bmag !for analytical geometries and consistency checks *only*
         ! following are binned (averaged) mean quantities and gradients from global GENE sim on rad_out_gene grid
         real, dimension (nrad_global), intent (out) :: dvdrho, grho
         real, dimension (nrad_global,nspec), intent (out) :: pflx_gene, qflx_gene
         real, dimension (nrad_global,nspec), intent (out) :: temp_bin, dens_bin
       end subroutine tringene
    end interface
# endif

    real, save :: agene  ! NEED TO LOOK INTO THIS -- MAB

!    real, dimension (nglobalrad)  :: rad_in_gene, 
    real, dimension (nglobalrad,nspec)  :: tprim_gene, fprim_gene
    real, dimension (nglobalrad)  :: vrot_gene
!    real, dimension (nrad_global) :: rad_out_gene
    real, dimension (nspec)      :: mass_gene, charge_gene
!    real, dimension (nglobalrad)  :: qval_gene, shat_gene !for analytical geometries *only*
    real, dimension (nrad_global,nspec) :: pflx_gene, qflx_gene
    real, dimension (nrad_global,nspec) :: temp_bin, dens_bin

# ifndef USE_GENE
      write (*,*) "To use flux_option='gene', you have to make Trinity with the option FLUXCODE=gene."
      call abort_mp()
# endif

      ! Assign convenience pointers
      qflx => flx%q
      pflx => flx%p

       ! corresponding quantities in gene:
       ! mpi_comm_world_in, n_spec_in, temp_in, omt_in, dens_in, omn_in
       ! mass_in, charge_in, q_in, shat_in, fluxlabel_in, coll_in,
       ! beta_in, n_fluxtube_in, n_transp_it_in, dvdx, sqrtgxx_fs
       
       if (gene%first) then
          ! if psitor_a = 1.0, then it was not found in ITER DB file
          if (abs(psitor_a) < epsilon(0.0)) then
             agene = aref
          else
             agene = sqrt(abs(psitor_a/(pi*bmag)))
          end if
          gene%first = .false.
       end if
       
       ! mass normalized to proton mass
       mass_gene(1) = m_ion(1)
       charge_gene(1) = z_ion(1)

       ! second species is electrons
       if (nspec > 1) then
       ! (electron mass)/(ion mass) = (electron mass)/(proton mass) * (proton mass)/(ion mass)
       	  mass_gene(2) = 5.44617E-4
       	  charge_gene(2) = -1.0
       end if

       !\TODO CHECK WHICH FLUX COORDINATE IS USED FOR PROFILES AND MAKE TRANSFORM
       !!IF NOT rho_tor !!!!!  (however, torflux should be the default, anyway)

       ! obtain turbulent fluxes from gene simulation
       ! new interface
       ! if nglobalrad > 1, then interpolate mean profiles onto global gene radial grid
       if (nglobalrad > 1) then
!          call construct_geneprofs_from_grads (dens_grid(:,1), fprim_ms, dens_ms)
          vrot_gene = omega_ms(1:ncc)/(aref/vtref)
#ifdef USE_GENE
          call tringene (mpi_comm, job, ncalls, electrostatic, & !administrative stuff
!               &ncc,nrad_global,rad_cc,rad_ms,& !grids
               &ncc,ncc,rad_cc,rad_cc,& !grids -- CHECK THIS
               &nspec,temp_ms(1:ncc,1:nspec), dens_ms(1:ncc,1:nspec), mass_gene, charge_gene,&
               &vrot_gene(1:ncc), & !profiles
               &tprim_ms(1:ncc,1:nspec), fprim_ms(1:ncc,1:nspec), & !only required in local sims
               &qval_ms, shat_ms, aspr, agene, bmag,& !only required for analytical geometries
               &dvdrho, grho, pflx_gene, qflx_gene, temp_bin, dens_bin) ! return values
# endif
       else
          do is = 1, nspec
             ! convert -(1/T)*dT/d(rho) to -(1/T)*dT/d(rho_tor)
             ! drhotordrho=1 if rho = rho_tor
             fprim_gene(:,is) = fprim_ms(:,is)/drhotordrho_ms
             tprim_gene(:,is) = tprim_ms(:,is)/drhotordrho_ms
          end do
          vrot_gene = gexb_ms/drhotordrho_ms
#ifdef USE_GENE
          call tringene (mpi_comm, job, ncalls, electrostatic, & !administrative stuff
               &1,1,rad_ms,rad_ms,& !grids
               &nspec,temp_ms(:,1:nspec), dens_ms(:,1:nspec), mass_gene, charge_gene, & !profiles
               &vrot_gene(1),&
               &tprim_ms(:,1:nspec), fprim_gene(:,1:nspec), & !only required in local sims
               &qval_ms, shat_ms, aspr, agene, bmag,& !only required for analytical geometries
               &flx%dvdrho, flx%grho, pflx_gene, qflx_gene, temp_bin, dens_bin) ! return values
# endif
       endif
       
       pflx = pflx_gene*(aref/agene)**2 ; qflx = qflx_gene*(aref/agene)**2

       ! convert from | (dV/drho_tor)/(a_gene)**2 | to | (dV/drho)/aref**2 |
       flx%dvdrho = flx%dvdrho*drhotordrho_ms*(agene/aref)**2

       ! convert from | agene * grad sqrt(rhotor/rhotor_separatrix) | 
       ! to | aref * grad rho |
       flx%grho = (aref/agene)*flx%grho/drhotordrho_ms

       do is = 1, nspec
          call broadcast (pflx(:,is))
          call broadcast (qflx(:,is))
       end do
       ! gather fluxes from group proc0s to global proc0


    end subroutine get_fluxes_gene
end module fluxes_gene
