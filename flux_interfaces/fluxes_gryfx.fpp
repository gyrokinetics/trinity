!> Contains the code for interfacing with the flux
!! tube code GRYFX. Links against the GRYFX dummy interface
!! if GRYFX not available.
module fluxes_gryfx

  use gs2_gryfx_zonal, only: gryfx_parameters_type
  use iso_c_binding, only: c_double

  implicit none

  !> Run gryfx and copy the fluxes into qflx etc. Model
  !! can be 'calculate', 'reduced' or 'calibrate'. The
  !! first two currently have exactly the same effect.
  public :: get_fluxes_gryfx

  public :: finish_fluxes_gryfx

  public :: fluxes_gryfx_type


  !public :: fluxes_gryfx_results_type

  private



  type, bind(c) :: gryfx_outputs_type
    real(c_double), dimension(20) :: pflux
    real(c_double), dimension(20) :: qflux
    real(c_double), dimension(20) :: heat
    real(c_double) :: dvdrho
    real(c_double) :: grho
  end type gryfx_outputs_type

  !> This is a type which contains all the data
  !! needed for interacting with gryfx, including
  !! the two objects which are passed to gryfx via
  !! its interface. 
  type fluxes_gryfx_type
    logical :: initialized = .false.
    logical :: first = .true.
    type(gryfx_parameters_type), dimension(:,:), allocatable :: pars
    type(gryfx_outputs_type), dimension(:,:), allocatable :: outs
  end type fluxes_gryfx_type

contains

  subroutine finish_fluxes_gryfx(gryfx)
    use mp_trin, only: destroy_comm_gryfx
    implicit none
    type(fluxes_gryfx_type), intent(inout) :: gryfx
    deallocate(gryfx%pars)
    deallocate(gryfx%outs)
    gryfx%first = .true.
    gryfx%initialized = .false.
    call destroy_comm_gryfx
  end subroutine finish_fluxes_gryfx

  subroutine initialize_fluxes_gryfx(gryfx)
    use mp_trin, only: fork_gryfx
    use trinity_input, only: n_gpus
    use trinity_input, only: njac, ncc
    implicit none
    type(fluxes_gryfx_type), intent(inout) :: gryfx
    allocate(gryfx%pars(ncc, njac))
    allocate(gryfx%outs(ncc, njac))
    call fork_gryfx(n_gpus)
    gryfx%initialized = .true.
  end subroutine initialize_fluxes_gryfx

  subroutine get_fluxes_gryfx(flx, flx_prev, flx_old, gryfx, clock)
    use mp_trin, only: mpi_comm => communicator, iproc
    use mp_trin, only: scope, allprocs, proc0
    use mp_trin, only: sum_allreduce_real_3array
    use mp_trin, only: gryfxprocs, igpu, broadcast, abort_mp
    use trinity_input, only: subfolders, n_gpus
    use trinity_input, only: nspec, njobs
    use trinity_input, only: ncc, fork_flag, njac, single_radius
    use file_utils_trin, only: run_name
    use flux_results, only: flux_results_type
    use unit_tests_trin, only: debug_message, verb_iteration
    use nteqns_arrays, only: bmag_cc, bgeo_cc
    use trinity_type_module, only: trinity_clock_type

    use iso_c_binding
    implicit none


    !! Declare interfaces to C gryfx driver routines
    interface
      subroutine gryfx_get_default_parameters(gryfx_parameters, input_file, mp_comm) &
          bind(c, name='gryfx_get_default_parameters_')
        use iso_c_binding
        import gryfx_parameters_type
        type(gryfx_parameters_type), intent(inout) :: gryfx_parameters
        character(kind=c_char), intent(in) :: input_file
        integer(c_int), intent(in), value :: mp_comm
      end subroutine gryfx_get_default_parameters
      subroutine gryfx_get_fluxes(gryfx_parameters, gryfx_outputs,&
          input_file, mp_comm) &
          bind(c, name='gryfx_get_fluxes_')
        use iso_c_binding
        import gryfx_parameters_type
        import gryfx_outputs_type
        type(gryfx_parameters_type), intent(inout) :: gryfx_parameters
        type(gryfx_outputs_type), intent(inout) :: gryfx_outputs
        character(kind=c_char), intent(in) :: input_file
        integer(c_int), intent(in), value :: mp_comm

      end subroutine gryfx_get_fluxes
    end interface

    type(fluxes_gryfx_type), intent(inout), target :: gryfx
    type(flux_results_type), intent(inout) :: flx, flx_prev, flx_old
    !type(gryfx_parameters_type), save :: gryfx_parameters
    !type(gryfx_outputs_type), save :: gryfx_outputs
    type(gryfx_parameters_type), pointer :: pars
    type(gryfx_outputs_type), pointer :: outs
    type(trinity_clock_type), intent(in) :: clock


    integer :: i, ix, igr, counter, gpu_counter
    character (10000) :: gryfx_infile
    character (10) :: ext
    integer :: gpuid, gpuid_broadcast

    call debug_message(verb_iteration, &
      'fluxes_gryfx::get_fluxes_gryfx starting')


    if (.not. gryfx%initialized) then
      call debug_message(verb_iteration, &
        'fluxes_gryfx::get_fluxes_gryfx calling initialize_fluxes_gryfx')
      call initialize_fluxes_gryfx(gryfx)
    end if
    if (fork_flag) then
      write (*,*) 'GRYFX not tested with fork_flag = .true.'
      stop 1
    else
      counter = -1
      gpu_counter = -1

      if (single_radius.gt.0) then 
        flx%l_cc(single_radius,:) = 0.0
        flx%p_cc(single_radius, :, :) = 0.0
        flx%q_cc(single_radius, :, :) = 0.0
        flx%heat_cc(single_radius, :, :) = 0.0
      else
        flx%l_cc = 0.0
        flx%p_cc = 0.0
        flx%q_cc = 0.0
        flx%heat_cc = 0.0
      end if

      call scope(gryfxprocs)

      do igr = 1, njac
        do ix = 1, ncc

          ! counter counts the job number, which corresponds to
          ! the gryfx input file suffix
          counter = counter + 1
          if (single_radius > 0 .and. single_radius .ne. ix) cycle

          ! If n_gpus < njobs, we can save time by not repeatedly rerunning converged jobs 
          if (&
            !n_gpus.lt.njobs.and.&
            all(flx%q_converged(ix,:,igr)).and.flx%conv_count.gt.0) then
            ! Because of the sum_allreduce_real_3array at the end we only want
            ! the global proc0 to assign it
            if (proc0 .and. igpu .eq. 0) then
              flx%q_cc(ix,:,igr) = flx_prev%q_turb_cc(ix,:,igr)/ &
                (bmag_cc(ix)/bgeo_cc(ix))**2 ! Need to reconvert to Gryfx field norm
            end if
            cycle
          end if

          ! gpu_counter determines which gpu the job will run on.
          ! There need be no relationship between the number of
          ! jobs (i.e. the number of cell centres and evolved
          ! profiles) and the number of gpus.
          gpu_counter = gpu_counter + 1
          if (.not. (mod(gpu_counter, n_gpus).eq. igpu)) cycle

          if (proc0) write(*,*) 'Starting job ', counter, ' on gpu ', igpu

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Assign local pointers
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          pars => gryfx%pars(ix, igr)
          outs => gryfx%outs(ix, igr)

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Set name of gryfx input file
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          write (ext,'(i9)') counter+1
          ext = adjustl(ext)
          if (subfolders) then
            gryfx_infile = "flux_tube_"//trim(ext)//"/"//trim(run_name)//trim(ext)//".in"//char(0)
          else
            gryfx_infile = trim(run_name)//".in"//char(0)
          end if


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Get default gryfx parameters from gryfx
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          call debug_message(verb_iteration, &
            'fluxes_gryfx::get_fluxes_gryfx calling gryfx_get_default_parameters')
          write (*,*) 'Communicator is ', mpi_comm
          call gryfx_get_default_parameters(pars, gryfx_infile, mpi_comm);
          call debug_message(verb_iteration, &
            'fluxes_gryfx::get_fluxes_gryfx called gryfx_get_default_parameters')

          pars%mpirank = iproc ! TODO: I think this is unnecessary 


          gpuid = pars%job_id
          gpuid_broadcast = gpuid
          call broadcast(gpuid_broadcast)
          if (.not. gpuid_broadcast .eq. gpuid) then
            write (*,*) 'ERROR: fork_gryfx has failed; not all processes &
              & in this subcommunicator are attached to the same gpu'
            write (*,*) 'gpuid = ', gpuid, 'gpuid_broadcast = ', gpuid_broadcast
            call abort_mp
          else if (proc0) then
          !else
            write (*,*) 'gpu serial number (last 8 digits) is ', gpuid
          end if

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Tell gryfx which iteration we are in
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          pars%trinity_timestep = clock%itstep
          pars%trinity_iteration = clock%iter
          pars%trinity_conv_count = flx%conv_count

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Overwrite default parameters with new
          !! profile/geometry data (where appropriate)
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          call set_gryfx_physics_parameters(pars, ix, igr)

          pars%job_id = counter

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! The first gryfx runs in a Trinity simulation
          !! start from noise; subsequent runs are started
          !! from the previous run with the same counter,
          !! i.e. job number
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (gryfx%first) then
            !Now let it be whatever the user sets
            !pars%restart = 0
          else
            if (flx%conv_count .eq. 0 .and. flx_old%error_flag) then
              pars%restart = 0
            else if (flx%conv_count .eq. 0 .and. &
              all(abs(flx_prev%q_turb_cc(ix,:,igr)) .lt. 1e-7)) then
              pars%restart = 0
            else if (any(abs(flx_prev%q_turb_cc(ix,:,igr)) .gt. 1e10) .or. &
              any(flx_prev%q_turb_cc(ix,:,igr) .ne. flx_prev%q_turb_cc(ix,:,igr))) then 
              ! above line is a nice check for nans
              call debug_message(verb_iteration, &
                'fluxes_gryfx::get_fluxes_gryfx restarted gryfx because of &
                &too large or NaN qflx value')
              pars%restart = 0
            else if (flx%conv_count .lt. 2) then
              ! This triggers zero restart avg
              pars%restart = 2
            else
              pars%restart = 1
            end if
            !pars%nstep = pars%nstep/2
          end if

          pars%end_time = clock%end_wall_clock

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Obtain turbulent fluxes from gryfx simulation
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          call debug_message(verb_iteration, &
            'fluxes_gryfx::get_fluxes_gryfx calling gryfx_get_fluxes')
          call gryfx_get_fluxes(pars, outs, gryfx_infile, mpi_comm)
          call debug_message(verb_iteration, &
            'fluxes_gryfx::get_fluxes_gryfx called gryfx_get_fluxes')

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !! Copy Outputs
          !!     --Gryfx only provides results on proc0
          !!     of each gpu
          !!     --Also, as of March 2016 Gryfx only does
          !!     heat flux for 1 species, so we set pflux
          !!     and heat to 0 for all species, and qflux
          !!     for species 2 only.
          !!     --Also, dvdrho and grho haven't been 
          !!     tested yet, so don't pass them to 
          !!     init_geo
          !!     
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (proc0) then
            write (*,*) 'Copying heat flux from igpu', igpu, ' qflux = ', outs%qflux(1)
            !do i = 1,nspec
            !  flx%p_cc(ix,i,igr) = outs%pflux(i)
            !  flx%q_cc(ix,i,igr) = outs%qflux(i)
            !  flx%heat_cc(ix,i,igr) = outs%heat(i)
            !end do
            ! HARDWIRE SINGLE ION SPECIES: TODO: GENERALIZE!!!
            !flx%p_cc(ix,2,igr) = outs%pflux(1)
            flx%q_cc(ix,2,igr) = outs%qflux(1)
            !flx%heat_cc(ix,2,igr) = outs%heat(1)
            !flx%p_cc(ix,1,igr) = 0.0
            !flx%q_cc(ix,1,igr) = 0.0
            !flx%heat_cc(ix,1,igr) = 0.0
            flx%dvdrho(1) = outs%dvdrho
            flx%grho(1) = outs%grho
          end if

        end do
      end do
      call scope(allprocs)
      if (proc0) write(*,*) 'qflx_cc before', flx%q_cc
      if (single_radius.gt.0) then 
        call sum_allreduce_real_3array(flx%p_cc(single_radius:single_radius,:,:))
        call sum_allreduce_real_3array(flx%q_cc(single_radius:single_radius,:,:))
        call sum_allreduce_real_3array(flx%heat_cc(single_radius:single_radius,:,:))
      else
        call sum_allreduce_real_3array(flx%p_cc)
        call sum_allreduce_real_3array(flx%q_cc)
        call sum_allreduce_real_3array(flx%heat_cc)
      end if
      if (proc0) write(*,*) 'qflx_cc after', flx%q_cc
    end if

    gryfx%first = .false.

    ! Convert fluxes from gryfx B normalisation to Trinity B normalisation
    do igr = 1,njac
      do ix = 1,ncc
        if (single_radius > 0 .and. single_radius .ne. ix) cycle
        do i = 1,nspec
          flx%p_cc(ix,i,igr) = flx%p_cc(ix,i,igr) * (bmag_cc(ix)/bgeo_cc(ix))**2
          flx%q_cc(ix,i,igr) = flx%q_cc(ix,i,igr) * (bmag_cc(ix)/bgeo_cc(ix))**2
        end do
        flx%l_cc(ix,igr) = flx%l_cc(ix,igr) * (bmag_cc(ix)/bgeo_cc(ix))**2
      end do
    end do


  end subroutine get_fluxes_gryfx

  subroutine set_gryfx_physics_parameters(pars, ix, igr)
    use nteqns_arrays, only: rmajor_cc, drmindrho_cc, tprim_perturb, fprim_perturb, bgeo_cc
    use nteqns_arrays, only: temp_cc, temp_perturb, dens_perturb, deltprim_cc
    use norms, only: amin_ref
    use nteqns_arrays, only: delta_cc, dens_cc, kappa_cc, kapprim_cc, betaprim_cc
    use nteqns_arrays, only: qval_cc, rad_cc, shift_cc, bgeo_cc
    use norms, only: aref 
    use nteqns_arrays, only: shat_cc, rmin_cc, btori_cc, bmag_cc
    use trinity_input, only: nspec, aspr, ncc
    type(gryfx_parameters_type), intent(inout) :: pars
    integer, intent(in) :: ix, igr
    integer :: i

    pars%rhoc = rad_cc(ix)
    pars%qinp = qval_cc(ix)
    pars%shat = shat_cc(ix)*rmin_cc(ix)/(rad_cc(ix)*drmindrho_cc(ix))
    if(abs(pars%shat) < .1) pars%shat = 1.0e-6
    pars%s_hat_input = pars%shat
    pars%rgeo_local = rmajor_cc(ix)*aref/amin_ref     !!this used to be setting rmaj, should be r_geo? does it matter?
    pars%eps = &
      pars%rhoc / pars%rgeo_local
    pars%rgeo_lcfs = aspr ! = rmajor_cc(ix)*aref/amin_ref     !!this is wrongish?
    ! aspr has been replaced by the expression below
    ! In fact, the choice we make for bmag happens to ensure
    ! that this is still equal to aspr; however, 
    ! changing this line makes it clear that what
    ! we are passing to gryfx is I_N = I / amin_ref B_ref
    pars%rgeo_lcfs = btori_cc(ix)/bgeo_cc(ix)/amin_ref
    pars%akappa = kappa_cc(ix)
    pars%akappri =  kapprim_cc(ix)/drmindrho_cc(ix)
    pars%tri =   asin(delta_cc(ix))
    pars%tripri =  deltprim_cc(ix)/drmindrho_cc(ix)/&
      sqrt(1.0- delta_cc(ix)**2.0)
    ! shift = d(R/aref)/drho => d(R/a)/d(r/a) = shift*(aref/a)*drho/d(r/a)
    pars%shift = shift_cc(ix)*aref/(amin_ref*drmindrho_cc(ix))
    ! Need to take derivative w.r.t rmin and also
    ! need to convert from Trinity B norm to GS2
    ! B norm
    pars%beta_prime_input =  betaprim_cc(ix)/drmindrho_cc(ix)&
      *(bmag_cc(ix)/bgeo_cc(ix))**2
    ! TODO: beta is not being passed to GRYFX at the moment.
    ! gexb_gryfx = r/q * domega/dr * a/v_tr
    ! gexb_cc = rho/q * domega/drho * aref/v_t0
    ! => gexb_gryfx = gexb_cc * (r/a)/rho * drho/d(r/a) * (a/aref) * (v_t0/v_tr)
    pars%g_exb = 0 !gexb_cc(ix)*rmin_cc(ix)*amin_ref &
    !/ (sqrt(temp_cc(ix,1))*aref*drmindrho_cc(ix)*rad_cc(ix))
    pars%ntspec = nspec
    do i = 1,nspec
      pars%dens(i)=dens_perturb(ix,i)/dens_cc(ix,2) ! No species dependence in dens
      pars%temp(i)=temp_perturb(ix,i)/temp_cc(ix,2) 
      !check species normalizations
      pars%fprim(i)=fprim_perturb(ix+(igr-1)*ncc,i)/drmindrho_cc(ix)
      pars%tprim(i)=tprim_perturb(ix+(igr-1)*ncc,i)/drmindrho_cc(ix)
      ! NEEDS TO BE FIXED
      !pars%nu(i) = 0.06 !nu_ms(1,i)
    end do

    !HARDWIRE SINGLE ION SPECIES: TODO: GENERALIZE!!
    pars%ntspec = 1
    pars%dens(1)=dens_perturb(ix,2)/dens_cc(ix,2) ! No species dependence in dens
    pars%temp(1)=temp_perturb(ix,2)/temp_cc(ix,2) 
    !check species normalizations
    pars%fprim(1)=fprim_perturb(ix+(igr-1)*ncc,2)/drmindrho_cc(ix)
    pars%tprim(1)=tprim_perturb(ix+(igr-1)*ncc,2)/drmindrho_cc(ix)
    ! NEEDS TO BE FIXED
    !pars%nu(1) = 0.0 !nu_ms(1,i)
  end subroutine set_gryfx_physics_parameters


end module fluxes_gryfx
