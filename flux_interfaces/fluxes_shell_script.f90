module fluxes_shell_script
  implicit none
  contains
    subroutine get_fluxes_shell_script(ncalls, calib, model, qflx_cc, pflx_cc, lflx_cc, heat_cc, dvdrho, grho)
      use calibrate_type_module, only: calibrate_type
      use trinity_input, only: fork_flag, flux_shell_script
      use file_utils_trin, only: get_unused_unit, run_name  
      use mp_trin, only: proc0, broadcast, abort_mp
      use mp_trin, only: sum_allreduce_real_3array
      use nteqns_arrays, only: dens_perturb, temp_perturb
      use nteqns_arrays, only: fprim_perturb, tprim_perturb
      use nteqns_arrays, only: grho_cc, area_cc
      implicit none
      integer, intent (in) :: ncalls
      type(calibrate_type), intent(in) :: calib
      character(*), intent(in) :: model
      real, dimension (:,:,:), intent(out) :: pflx_cc, qflx_cc, heat_cc
      real, dimension (:,:), intent(out) :: lflx_cc
      real, dimension (:), intent(out) :: dvdrho, grho
      integer :: shell_output_unit, shell_input_unit
      if (fork_flag) then
        write (*,*) 'Can`t use fork_flag with flux_option_shell_script'
        call abort_mp()
      end if

      pflx_cc = 0.0; qflx_cc = 0.0; lflx_cc = 0.0; heat_cc = 0.0

      if (proc0) then
        call get_unused_unit(shell_input_unit)
        write(*,*) 'size dens_perturb', size(dens_perturb)
        open(unit=shell_input_unit, file=trim(run_name)//'.flux_inputs')
        write (shell_input_unit, "(5E16.9)") dens_perturb
        write (shell_input_unit, "(5E16.9)") temp_perturb
        write (shell_input_unit, "(5E16.9)") fprim_perturb
        write (shell_input_unit, "(5E16.9)") tprim_perturb
        close (shell_input_unit)
        call system(flux_shell_script)
        call get_unused_unit(shell_output_unit)
        open(unit=shell_output_unit, file=trim(run_name)//'.flux_results')
        read(shell_output_unit,*) pflx_cc
        read(shell_output_unit,*) qflx_cc
        read(shell_output_unit,*) heat_cc
        read(shell_output_unit,*) lflx_cc
        read(shell_output_unit,*) area_cc
        read(shell_output_unit,*) grho_cc
        close(shell_output_unit)
      end if
      call sum_allreduce_real_3array(pflx_cc)
      call sum_allreduce_real_3array(qflx_cc)
      call sum_allreduce_real_3array(heat_cc)
      call broadcast(lflx_cc(:,1))
      call broadcast(dvdrho)
      call broadcast(grho)


    end subroutine get_fluxes_shell_script

    function fluxes_shell_script_unit_test_get_fluxes_shell_script(&
        run_test, qflux_cc_results, eps)
      use unit_tests_trin
      use mp_trin,only: proc0
      use calibrate_type_module, only: calibrate_type
      use mp_trin, only: proc0, broadcast, abort_mp
      !#use nteqns, only: qval_cc
      logical, intent(in) :: run_test
      real, intent(inout), dimension(:,:) ::  qflux_cc_results
      real, intent(in) :: eps
      logical :: fluxes_shell_script_unit_test_get_fluxes_shell_script
      integer :: dummy
      integer :: i
      character(len=18) :: message
      real,dimension(size(qflux_cc_results,1),size(qflux_cc_results,2),2):: &
          pflx_cc, qflx_cc, heat_cc
      real, dimension(size(qflux_cc_results,1)) :: grho, dvdrho
      real, dimension(size(qflux_cc_results,1),2) :: lflx_cc
      type(calibrate_type) :: dummy_calib
      fluxes_shell_script_unit_test_get_fluxes_shell_script = .true.
      call get_fluxes_shell_script(dummy, &
        dummy_calib, &
        "calculate", &
        qflx_cc=qflx_cc,     &
        pflx_cc=pflx_cc,     &
        lflx_cc=lflx_cc,     &
        heat_cc=heat_cc,     &
        dvdrho=dvdrho, &
        grho=grho)
      if (proc0) write (*,*) qflx_cc, 'qflx_cc'
      if (run_test .and. proc0) then 
        do i = 1,size(qflx_cc, 2)
        write(message,'("heat flux spec ",I1)') i
        call announce_check(message)
        call process_check(&
          fluxes_shell_script_unit_test_get_fluxes_shell_script, &
          agrees_with(qflx_cc(:,i,1), qflux_cc_results(:,i), eps), &
          message)
        end do
      end if
    end function fluxes_shell_script_unit_test_get_fluxes_shell_script

  end module fluxes_shell_script
