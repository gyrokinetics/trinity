module fluxes_replay
  implicit none
  contains
    subroutine get_fluxes_replay(itstep, iter, ncalls, calib, model, &
        qflx_cc, pflx_cc, lflx_cc, heat_cc, dvdrho, grho, old_flux_option)
      use calibrate_type_module, only: calibrate_type
      use trinity_input, only: fork_flag, replay_filename,ncc, flux_option_gs2
      use file_utils_trin, only: get_unused_unit, run_name  
      use mp_trin, only: proc0, broadcast, abort_mp
      use mp_trin, only: sum_allreduce_real_3array
      use nteqns_arrays, only: dens_perturb, temp_perturb
      use nteqns_arrays, only: fprim_perturb, tprim_perturb
      use nteqns_arrays, only: grho_cc, area_cc
      use simpledataio, only: open_file, closefile
      use simpledataio_read, only: read_variable
      use simpledataio, only: sdatio_file, sdatio_init, sdatio_free
      use simpledataio, only: set_dimension_start, set_count
      use nteqns_arrays, only: grho_grid, area_grid
      use mp_trin, only: all_to_group
      use interp, only: get_gridval
      implicit none
      integer, intent (in) :: ncalls, itstep, iter
      type(calibrate_type), intent(in) :: calib
      character(*), intent(in) :: model
      real, dimension (:,:,:), intent(out) :: pflx_cc, qflx_cc, heat_cc
      real, dimension (:,:), intent(out) :: lflx_cc
      real, dimension (:), intent(out) :: dvdrho, grho
      integer, intent(out) :: old_flux_option
      integer :: shell_output_unit, shell_input_unit
      type(sdatio_file) :: replay_file
      if (fork_flag) then
        write (*,*) 'Can`t use fork_flag with flux_option_replay'
        call abort_mp()
      end if

      pflx_cc = 0.0; qflx_cc = 0.0; lflx_cc = 0.0; heat_cc = 0.0

      if (proc0) then
        call sdatio_init(replay_file, trim(replay_filename))
        call open_file(replay_file)
        !call set_dimension_start(replay_file, "t", itstep)
        call set_dimension_start(replay_file, "eval", ncalls)
        !call set_count(replay_file, "pflx_cc", "iter", 1)
        !call set_count(replay_file, "qflx_cc", "iter", 1)
        !call set_count(replay_file, "lflx_cc", "iter", 1)
        !call set_count(replay_file, "heat_cc", "iter", 1)
        call read_variable(replay_file, "pflx_cc_eval", pflx_cc)
        call read_variable(replay_file, "qflx_cc_eval", qflx_cc)
        call read_variable(replay_file, "lflx_cc_eval", lflx_cc)
        call read_variable(replay_file, "heat_cc_eval", heat_cc)
        call read_variable(replay_file, "grho_cc_eval", grho_cc)
        call read_variable(replay_file, "area_cc_eval", area_cc)
        call read_variable(replay_file, "flux_option_switch_eval",old_flux_option)
        call closefile(replay_file)
        call sdatio_free(replay_file)
      end if
      if (old_flux_option .eq. flux_option_gs2) then
        call get_gridval (area_cc, area_grid)
        call get_gridval (grho_cc, grho_grid, grho_cc(1))
      end if
      call sum_allreduce_real_3array(pflx_cc)
      call sum_allreduce_real_3array(qflx_cc)
      call sum_allreduce_real_3array(heat_cc)
      call broadcast(lflx_cc(:,1))
      call broadcast(old_flux_option)
      call broadcast(dvdrho)
      call broadcast(grho)


    end subroutine get_fluxes_replay

    !function fluxes_replay_unit_test_get_fluxes_replay(&
        !run_test, qflux_cc_results, eps)
      !use unit_tests_trin
      !use mp_trin,only: proc0
      !use calibrate_type_module, only: calibrate_type
      !use mp_trin, only: proc0, broadcast, abort_mp
      !!#use nteqns, only: qval_cc
      !logical, intent(in) :: run_test
      !real, intent(inout), dimension(:,:) ::  qflux_cc_results
      !real, intent(in) :: eps
      !logical :: fluxes_replay_unit_test_get_fluxes_replay
      !integer :: dummy
      !integer :: i
      !character(len=18) :: message
      !real,dimension(size(qflux_cc_results,1),size(qflux_cc_results,2),2):: &
          !pflx_cc, qflx_cc, heat_cc
      !real, dimension(size(qflux_cc_results,1)) :: grho, dvdrho
      !real, dimension(size(qflux_cc_results,1),2) :: lflx_cc
      !type(calibrate_type) :: dummy_calib
      !fluxes_replay_unit_test_get_fluxes_replay = .true.
      !call get_fluxes_replay(dummy, &
        !dummy_calib, &
        !"calculate", &
        !qflx_cc=qflx_cc,     &
        !pflx_cc=pflx_cc,     &
        !lflx_cc=lflx_cc,     &
        !heat_cc=heat_cc,     &
        !dvdrho=dvdrho, &
        !grho=grho)
      !if (proc0) write (*,*) qflx_cc, 'qflx_cc'
      !if (run_test .and. proc0) then 
        !do i = 1,size(qflx_cc, 2)
        !write(message,'("heat flux spec ",I1)') i
        !call announce_check(message)
        !call process_check(&
          !fluxes_replay_unit_test_get_fluxes_replay, &
          !agrees_with(qflx_cc(:,i,1), qflux_cc_results(:,i), eps), &
          !message)
        !end do
      !end if
    !end function fluxes_replay_unit_test_get_fluxes_replay

  end module fluxes_replay
