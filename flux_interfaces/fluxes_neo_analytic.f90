!> A module for calculating neoclassical fluxes analytically, using estimates
!! from Chang-Hinton 
module fluxes_neo_analytic
  use flux_results, only: flux_results_type
contains
  subroutine get_fluxes_neo_analytic(flx)
    use nteqns_arrays, only: nu_ms, qval_ms, tprim_ms, gexb_ms, rad_ms, shift_ms
    use nteqns_arrays, only: nu_perturb, qval_cc, tprim_perturb, gexb_perturb
    use nteqns_arrays, only: rad_cc, shift_cc
    use trinity_input, only: nrad_global, ncc, njac, fork_flag
    type(flux_results_type), intent(inout) :: flx
    integer :: ix, igr, idx
    if (fork_flag) then
      do ix = 1, nrad_global
      call neo_flux (flx%q_neo(ix,:), flx%l_neo(ix), nu_ms(ix,:), qval_ms(ix), tprim_ms(ix,:), &
        gexb_ms(ix), rad_ms(ix), shift_ms(ix))
      end do
    else
      do igr = 1, njac
      do ix = 1, ncc
      idx=ix+(igr-1)*ncc
      call neo_flux (flx%q_neo_cc(ix,:,igr), flx%l_neo_cc(ix,igr), nu_perturb(idx,:), qval_cc(ix), &
        tprim_perturb(idx,:), gexb_perturb(idx), rad_cc(ix), shift_cc(ix))
      end do
      end do
    end if
  end subroutine get_fluxes_neo_analytic
  subroutine neo_flux (qflx_neo, lflx_neo, nu, qval, tprim, gexb, rhoc, r0prim)

    ! computes neoclassical flux using Chang-Hinton analytic result
    ! for concentric, circular flux surfaces

    use trinity_input, only: aspr, prandtl_neo, vtfac
    use trinity_input, only: z_ion
    use constants_trin, only: pi

    implicit none

    real :: k0, a2, b2, c2, eps, nustar, k2star, k2
    real :: b02b2, b2b02inv, capf
    real :: bpol

    real, dimension (:), intent (out) :: qflx_neo
    real, intent (out) :: lflx_neo
    real, intent (in) :: qval, rhoc, r0prim, gexb
    real, dimension (:), intent (in) :: nu, tprim

    !write (*,*) 'CALLING NEO FLUX', 'nu', nu

    qflx_neo = 0. ; lflx_neo = 0.

    eps = rhoc/aspr
    !    bpol = eps/qval    ! effective poloidal magnetic field normalized by B_a
    bpol = eps/(qval*sqrt(1+(eps/qval)**2))

    k0 = 0.66 ; a2 = 1.03 ; b2 = 0.31 ; c2 = 0.74
    b02b2 = (1. + 1.5*(eps**2 + eps*r0prim) + .375*r0prim*eps**3)/(1. + 0.5*eps*r0prim)
    b2b02inv = sqrt(1.-eps**2)*(1.+0.5*eps*r0prim)/(1.+r0prim*(sqrt(1.-eps**2)-1.)/eps)

    nustar = 4./(3.*sqrt(pi))*nu(2)*qval*aspr/eps**1.5
    k2star = (0.66 + 1.88*sqrt(eps) - 1.54*eps)*b02b2

    capf = 0.5*(b02b2 - b2b02inv)/sqrt(eps)

    k2 = k0*((k2star/0.66)/(1.0 + a2*sqrt(nustar) + b2*nustar) &
      + c2**2*nustar*eps**1.5/(b2*(1.+c2*nustar*eps**1.5)) * capf)

    ! factor of vtfac below necessary to account for (a/rho_i)^2 normalization
    qflx_neo(2) = k2*(sqrt(8./pi)/3.)*sqrt(eps)*tprim(2)*nu(2) &
      / (0.5*vtfac*bpol**2)
    !         / (bpol**2*aspr*z_ion(1)**2) * aspr

    ! TEMPORARY EXPRESSION FOR NEO MOMENTUM FLUX -- MAB
    lflx_neo = -k2*(sqrt(8./pi)/3.)*sqrt(eps)*nu(2) &
      / (bpol**2*aspr*z_ion(1)**2) * qval/rhoc * aspr**2 * gexb * prandtl_neo

    !write (*,*) 'QFLUX_NEO=', qflx_neo

    ! TMP FIX TO GIVE SOME NEOCLASSICAL ELECTRON HEAT FLUX -- MAB
    !    qflx_neo(1) = qflx_neo(2)*0.02

  end subroutine neo_flux
end module fluxes_neo_analytic
