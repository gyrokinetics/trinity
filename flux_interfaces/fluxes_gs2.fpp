!> Contains the code for interfacing with the flux
!! tube code GS2. Links against the GS2 dummy interface
!! if GS2 not available.
module fluxes_gs2

#ifndef GS2_VERSION_LE_6
  use gs2_main, only: gs2_program_state_type
#endif

  implicit none

  !> Run gs2 and copy the fluxes into qflx etc. Model
  !! can be 'calculate', 'reduced' or 'calibrate'. The
  !! first two currently have exactly the same effect.
  public :: get_fluxes_gs2

  public :: finish_fluxes_gs2
  
  public :: fluxes_gs2_type

  !public :: fluxes_gs2_results_type

  private



  !type fluxes_gs2_results_type
  !real, dimension(:), pointer :: qflx
  !end type fluxes_gs2_results_type

  type fluxes_gs2_type
    logical :: initialized = .false.
    logical :: first = .true.
    logical :: calibrate_first = .true.
#ifndef GS2_VERSION_LE_6
    type(gs2_program_state_type) :: gs2_state
#endif
  end type fluxes_gs2_type

contains

  function gs2_version_gt_6()
    logical :: gs2_version_gt_6
    gs2_version_gt_6 = .true.
#ifdef GS2_VERSION_LE_6
    gs2_version_gt_6 = .false.
#endif
  end function gs2_version_gt_6

  subroutine finish_fluxes_gs2(gs2)
    use trinity_input, only: flux_option_switch, flux_option_gs2
    use trinity_input, only: dyn_load_balance
# ifndef OLD_GS2_INTERFACE
    use gs2_main, only: finish_gs2
# ifndef GS2_VERSION_LE_6
    use gs2_main, only: finalize_diagnostics, finalize_equations
    use gs2_main, only: finalize_gs2, finalize_overrides

#endif
# endif
    type(fluxes_gs2_type), intent(inout), target :: gs2
#ifndef GS2_VERSION_LE_6
    type(gs2_program_state_type), pointer :: gs2_state
    gs2_state => gs2%gs2_state
#endif
    gs2%first = .true.
    gs2%calibrate_first = .true.
# ifndef OLD_GS2_INTERFACE
# ifndef GS2_VERSION_LE_6
    ! HJL Removed for load balance stuff, may need it back
    if (gs2_version_gt_6()) then
      if (gs2%initialized) then
        call finalize_diagnostics(gs2_state)
        call finalize_equations(gs2_state)
        call finalize_gs2(gs2_state)
        call finalize_overrides(gs2_state)
        gs2%initialized = .false.
      end if
    else
      if (flux_option_switch == flux_option_gs2 .and. .not. dyn_load_balance) call finish_gs2
    end if
# endif
# endif
  end subroutine finish_fluxes_gs2

  subroutine get_fluxes_gs2(flx, gs2, ncalls, calib, model, gnostics)
    use mp_trin, only: mpi_comm => communicator, job, proc0, broadcast, iproc, subprocs
    use mp_trin, only: job_fork, scope, allprocs, iproc, barrier, abort_mp, current_focus
    use mp_trin, only: nproc, fork_single, singleprocs, barrier
    use trinity_input, only: subfolders, flux_groups, aspr
    use trinity_input, only: dyn_load_balance, nspec, use_external_geo, load_balance
    use trinity_input, only: nensembles, lgrad_mult, evolve_geometry
    use file_utils_trin, only: run_name
    use nteqns_arrays, only: rmajor_ms, omega_ms, zeff_ms
    use nteqns_arrays, only: temp_ms, dens_ms, tprim_ms, fprim_ms, shift_ms, betaprim_ms, gexb_ms
    use nteqns_arrays, only: qval_ms, shat_ms, rad_ms, nu_ms, kappa_ms, delta_ms
    use nteqns_arrays, only: drmindrho_ms, deltprim_ms, kapprim_ms, rmin_ms
    use norms, only: aref
    use nteqns_arrays, only: bmag_ms, btori_ms, bgeo_ms
    use trinity_input, only: njobs, single_radius, flux_driver, njac,ncc
    use norms, only: amin_ref
    use calibrate_type_module, only: calibrate_type
    use trindiag_config, only: trindiag_type
    use unit_tests_trin, only: debug_message, verb_iteration, verb_high
    use flux_results, only: flux_results_type
#ifndef OLD_GS2_INTERFACE
    use gs2_main, only: run_gs2,finish_gs2, gs2_trin_init, reset_gs2, trin_finish_gs2
#ifndef GS2_VERSION_LE_6
    use gs2_main, only: initialize_gs2, initialize_equations, gs2_program_state_type
    use gs2_main, only: evolve_equations, initialize_diagnostics
    use gs2_main, only: finalize_diagnostics, finalize_gs2
    use gs2_main, only: prepare_miller_geometry_overrides
    use gs2_main, only: prepare_profiles_overrides
    use gs2_main, only: calculate_outputs, reset_equations
    use gs2_main, only: prepare_initial_values_overrides
    use gs2_main, only: set_initval_overrides_to_current_vals
    use gs2_main, only: finalize_diagnostics, finalize_equations
    use gs2_main, only: finalize_gs2
    use gs2_main, only: initialize_wall_clock_timer
    use trinity_type_module, only: trinity_clock_type
    !use run_parameters, only: use_old_diagnostics
#endif

#else
#include "old_gs2_interface_definition.f90"
#endif
    implicit none

    type(flux_results_type), intent(inout) :: flx
    type(fluxes_gs2_type), intent(inout), target :: gs2
#ifndef GS2_VERSION_LE_6
    type(gs2_program_state_type), pointer :: gs2_state
#endif
    integer, intent (in) :: ncalls
    type(calibrate_type), intent(in) :: calib
    character(*), intent(in) :: model
    type(trindiag_type), intent(inout) :: gnostics
    !type(trinity_clock_type), intent(in) :: clock

    integer :: i
    real :: rhoc_gs2, rmajor_gs2, shat_gs2, kapprim_gs2, triprim_gs2, shift_gs2
    real :: gexb_gs2, betaprim_gs2, mach_gs2
    real :: bmag_over_bgeo, btori_gs2
    ! EGH Added qval_gs2 etc so that we never mess with 
    ! Trinity variables like qval_ms in this module.
    real :: qval_gs2, kappa_gs2, tri_gs2
    real, dimension (:), allocatable :: fprim_gs2, tprim_gs2, nu_gs2, dens_gs2, temp_gs2
    character (2000), save :: gs2_infile
    logical :: gs2_converged = .false., gs2_reserved = .false.
    character (10) :: ext
    integer :: groups(0:njobs-1), count = 0
    logical :: gs2_reset = .false.
    integer :: key, color, igr
    real :: btori_norm

    call scope(allprocs)
    call debug_message(verb_high, &
      'fluxes_gs2::get_fluxes_gs2 starting')
    call scope(subprocs)

    allocate(fprim_gs2(nspec)) ; fprim_gs2 = 0.0
    allocate(tprim_gs2(nspec)) ; tprim_gs2 = 0.0
    allocate(dens_gs2(nspec)) ; dens_gs2 = 0.0
    allocate(temp_gs2(nspec)) ; temp_gs2 = 0.0
    allocate (nu_gs2(nspec)) ; nu_gs2 = 0.0

#ifndef GS2_VERSION_LE_6
    !Assign local pointer for convenience
    gs2_state => gs2%gs2_state
    if (gs2%first) call initialize_wall_clock_timer
#endif
    call scope(allprocs)
    call debug_message(verb_high, &
      'fluxes_gs2::get_fluxes_gs2 initialized wall clock timer')
    call scope(subprocs)

    flx%q = 0.0
    flx%p = 0.0
    flx%l = 0.0
    flx%heat = 0.0
    flx%dvdrho = 0.0
    flx%grho = 0.0

    !write (*,*) 'MODEL AT START', model

    !if (model .eq. "reduced" .or. model .eq. "calibrate") then
    !get_fluxes_gs2_first = .true.
    !end if
    if (dyn_load_balance .or. model .eq. "calibrate") then
      if (.not. gs2_version_gt_6()) then
        call scope(allprocs)
        write (*,*) "Cannot use dynamic load balance or calibration with gs2 &
          & version 6 or less"
        call abort_mp
      end if
    end if
   ! Note for GS2, reduced is the same as calculate.
   if (model .eq. "calibrate") then
      ! We only want to run certain flux tubes. The
      ! dynamic load balancing framework is perfect. We set all proc0s
      ! to be reserved. Then we  mark all other
      ! processors as converged to make them available.
      ! Then we mark the proc0s we are interested in running (between 1
      ! and nrad of them) as being unreserved and unconverged.
      ! Then we call init_jobs. This has the desired effect of
      ! assigning all processors to the 1 or more jobs we want to run
      if (proc0) then
        gs2_reserved = .true.
        gs2_converged = .false.
      else
        gs2_reserved = .false.
        gs2_converged = .true.
      end if


      ! This loop relies on jobs 1 to ncc being the set of cell-centered
      ! unperturbed jobs.
      do i = 1,calib%ncc_calibrate
      if (proc0 .and. job .eq. calib%calibration_job(i)) then
        !if (proc0 .and. job .ne. 1) then
        gs2_reserved = .false.
        gs2_converged = .false.
      end if
      end do
      !call trin_finish_gs2
      ! Reassign converged procs, this routine also checks for completion of all jobs
      call scope(allprocs)
      !if (iproc.eq.15) gs2_converged = .false.
      groups = flux_groups
      groups = 1
      !write (*,*) 'CALLING JOBFORK'
      call job_fork(njobs, groups, load_balance=.true., &
        available=gs2_converged, reserved=gs2_reserved)
      call scope(subprocs)


    end if
    !call scope(allprocs)
    !call barrier
    !call scope(subprocs)

    ! obtain turbulent fluxes from gs2 simulation
    if(dyn_load_balance .or. calib%neval_calibrate > 0) then
      call scope(allprocs)
      call debug_message(verb_high, &
        'fluxes_gs2::get_fluxes_gs2 starting dynamic gs2 run')
      call scope(subprocs)

      if (.not. (model .eq. "calibrate")) then
        gs2_reserved = .false.
        !else if (gs2_reserved) then
      end if
      gs2_converged = .false.

      ! The next section is used for calibrating gs2 linear fluxes against
      ! nonlinear
      do while(.not. gs2_converged)
      if(.not. gs2_reserved) then
#ifndef GS2_VERSION_LE_6
        if (model .eq. "calibrate" .and. gs2%calibrate_first) then
          ! We can't restart the first calibrate run, obviously.
          ! Note, if the Trinity run itself has been restarted,
          ! this should be reflected in the gs2 input files rather
          ! than here. 
          gs2_state%init%initval_ov%override = .false.
          gs2%calibrate_first = .false.
        end if
        !if (calib%neval_calibrate > 0) gs2_state%init%initval_ov%override=.false.
        !write (*,*) 'IN NEW SEQUENCE', job+1, dyn_load_balance, model, calib%neval_calibrate
        call calculate_and_broadcast_gs2_inputs
        call setup_gs2_program_state
        call initialize_gs2(gs2_state)
        call prepare_miller_geometry_overrides(gs2_state)
        call set_miller_geometry_overrides
        call prepare_profiles_overrides(gs2_state)
        call set_profiles_overrides
        call initialize_equations(gs2_state)
        !use_old_diagnostics = .true.
        call initialize_diagnostics(gs2_state)
        gs2_state%converged = gs2_converged
        gs2_state%print_full_timers = .false.
        call evolve_equations(gs2_state, gs2_state%nstep)
        !> If we are not doing dynamic load balancing, just run 
        !! for the precribed number of steps then exit. Eventually
        !! we want to have a second flag, run_till_converged
        if (proc0) write (*,*) 'Called evolve_equations'
        if (.not. dyn_load_balance) gs2_state%converged = .true.
        gs2_converged = gs2_state%converged
        call calculate_outputs(gs2_state)
        if (proc0) write (*,*) 'Called calculate_outputs'
        call get_gs2_outputs
        if (proc0) write (*,*) 'Called get_gs2_outputs'
        !> This line is necessary for, among other things, resetting the magnitude
        !! of perturbations when running linearly and resetting diagnostics
        !! counters and time averages.
        if (gs2_state%converged) call reset_equations(gs2_state)
        if (proc0) write (*,*) 'Called reset_equations'
        call prepare_initial_values_overrides(gs2_state)
        call set_initval_overrides_to_current_vals(gs2_state%init%initval_ov)
        gs2_state%init%initval_ov%override = .true.
        write (*,*) 'VALUE OF IN MEMORY', gs2_state%init%initval_ov%in_memory
        call finalize_diagnostics(gs2_state)
        call finalize_equations(gs2_state)
        gs2_state%print_full_timers = .true.
        call finalize_gs2(gs2_state)
#else
        write (*,*) "HELP: Should never reach here in fluxes_gs2!"
        call abort_mp
#endif
      endif

      if(proc0 .and. gs2_converged) then
        gs2_reserved = .true.
        gs2_converged = .false.
      endif

      ! Reassign converged procs, this routine also checks for completion of all jobs
      if (.not. proc0) gs2_converged = .true.
      call scope(allprocs)
      groups = flux_groups
      groups = 1
      ! By the end of this call, gs2_converged is false for ALL procs
      call job_fork(njobs, groups, load_balance=.true., &
        available=gs2_converged, reserved=gs2_reserved)
      call scope(subprocs)



      ! If not converged and reserved we need to broadcast the correct gs2 inputs

      ! EGH I think we need to broadcast and reset them every time... particularly
      ! at the end of the loop when everything is converged and we
      ! are resetting to the original configuration.

      !if(.not. gs2_converged .and. .not. gs2_reserved .and. .false.) then
      ! NB No need to broadcast fluxes here because in group_to_all
      ! they are all read from the proc0 of each group.
      !call barrier
      !endif

      enddo
    else ! if (.not. dyn_load_balance)
      call scope(allprocs)
      call debug_message(verb_high, &
        'fluxes_gs2::get_fluxes_gs2 starting standard gs2 run')
      call scope(subprocs)
      !if (model .eq. "calibrate") then
        !if (proc0) write (*,*) "ERROR: GS2 calibration only works with dynamic load balancing"
        !stop 1
      !end if
      if (gs2_version_gt_6()) then
        !write (*,*) 'IN RIGHT SEQUENCE', job+1
        if (single_radius>0) then
          if (gs2%first) then
            key = 1
            if (flux_driver) then 
              ! In this case we only run the single base case
              ! The new proc0 is the group proc0 of given cell centre
              ! All procs are in the same communicator
              if (proc0 .and. single_radius .eq. job+1) key=0
              color = 0
            else
              ! In this case we have njac communicators. The proc0
              ! of each communicator is the group proc0 of the cell
              ! centre for the igr'th element of the jacobian. All
              ! procs that were calculating some cell centre of the igr'th
              ! element are now assigned to this proc0
              do igr = 1,njac
                if (proc0 .and. single_radius .eq. mod(job, ncc)+1) key=0
                if (job .ge. (igr-1)*ncc) color = igr
              end do
              write (*,*) 'IPROC = ', iproc, color, key, job
            end if
            call scope(allprocs)
            call debug_message(verb_iteration, &
              'trinity_main::run_trinity forking single_radius')
            call fork_single(key,color)
            call debug_message(verb_iteration, &
              'trinity_main::run_trinity forked single_radius')
          end if ! get_fluxes_gs2_first
          call scope(singleprocs)
        end if 
        call calculate_and_broadcast_gs2_inputs
#ifndef GS2_VERSION_LE_6
        if (gs2%first) then 
          call debug_message(verb_high, &
            'fluxes_gs2::get_fluxes_gs2 initializing gs2')
          call setup_gs2_program_state
          gs2_state%replay=.false.
          !write (*,*) 'state%included 5', gs2_state%included, current_focus
          call initialize_gs2(gs2_state)
          gs2%initialized = .true.
          !write (*,*) 'state%included 6', gs2_state%included
        end if
        if ((gs2%first.or.evolve_geometry).and..not.use_external_geo) then 
          ! This basically completely reinitializes gs2
          ! so we don't do it if we don't have to.
          call prepare_miller_geometry_overrides(gs2_state)
          call set_miller_geometry_overrides
          call debug_message(verb_high, &
            'fluxes_gs2::get_fluxes_gs2 set_miller_geometry_overrides')
        end if
        if (gs2_state%replay) call initialize_equations(gs2_state)
        !if (.not. gs2_state%replay) then 
        if (.not. gs2_state%replay .or. gs2_state%replay_full_initialize) then 
          call prepare_profiles_overrides(gs2_state)
          call set_profiles_overrides
          call initialize_equations(gs2_state)
          call debug_message(verb_high, &
            'fluxes_gs2::get_fluxes_gs2 initialized equations')
        end if
        !call initialize_equations(gs2_state)
        if (gs2%first) call initialize_diagnostics(gs2_state)
        !gs2_state%trinity_timestep = clock%itstep
        !gs2_state%trinity_iteration = clock%iter
        gs2_state%converged = .false.
        call debug_message(verb_high, &
          'fluxes_gs2::get_fluxes_gs2 running gs2')
        call barrier
        !write (*,*) 'state%included 1', gs2_state%included
        call evolve_equations(gs2_state, gs2_state%nstep)
        call calculate_outputs(gs2_state)
        call get_gs2_outputs
        call reset_equations(gs2_state)
        if (.not. gs2_state%replay) then
          call debug_message(verb_high, &
            'fluxes_gs2::get_fluxes_gs2 setting initval overrides')
          !write (*,*) 'state%included', gs2_state%included
          ! The next three lines ensure that we continue from
          ! the current solution
          call prepare_initial_values_overrides(gs2_state)
          call set_initval_overrides_to_current_vals(gs2_state%init%initval_ov)
          gs2_state%init%initval_ov%override = .true.
        end if
        !write (*,*) 'FINISHED GS2 CALL SEQUENCE', job+1
        call flush(6)
        
#else
        write (*,*) "HELP: Should never reach here in fluxes_gs2!"
        call abort_mp
#endif

      else ! if .not. gs2_version_gt_6
        !write (*,*) 'IN OLD SEQUENCE', job+1
        call calculate_and_broadcast_gs2_inputs
        if (gs2%first) then
          btori_norm = btori_ms(1)/bgeo_ms(1)/amin_ref

          call gs2_trin_init (rhoc_gs2, qval_ms(1), shat_gs2, &
            ! aspr has been replaced by btori_norm
            ! The choice we make for bmag happens to ensure
            ! that this is still equal to aspr; however, 
            ! changing this line makes it clear that what
            ! we are passing to GS2 is I_N = I / amin_ref B_ref
             btori_norm,&
             rmajor_gs2, kappa_ms(1), kapprim_gs2, &
            tri_gs2, triprim_gs2, shift_gs2, betaprim_gs2, nspec, dens_ms(1,:), &
            temp_ms(1,:), fprim_gs2, tprim_gs2, gexb_gs2, mach_gs2, nu_gs2, use_external_geo)
          gs2_reset = .false.
        else
          ! NB the normalisation of dens_ms and temp_ms is handled within 
          ! the old gs2 interface
          call reset_gs2 (nspec, dens_ms(1,:), temp_ms(1,:), &
            fprim_gs2, tprim_gs2, gexb_gs2, mach_gs2, nu_gs2, nensembles)
        end if
        call run_gs2 (mpi_comm, job, gs2_infile, nensembles, flx%p(1,:), &
          flx%q(1,:), flx%l(1), flx%heat(1,:), flx%dvdrho(1), flx%grho(1))
      end if
    end if ! if dyn_load_balance


    ! convert dvdrho and grho from r/a to sqrt(torflux) if torflux is the fluxlabel chosen
    flx%dvdrho = flx%dvdrho*drmindrho_ms ; flx%grho = flx%grho*aref/(amin_ref*drmindrho_ms)

    !flx%p(1,3) = fprim_ms(1,3) * 0.1

    ! Convert fluxes from GS2 B normalisation to Trinity B normalisation
    flx%p = flx%p * bmag_over_bgeo**2
    flx%q = flx%q * bmag_over_bgeo**2
    flx%l = flx%l * bmag_over_bgeo**2

    gs2%first = .false.

    if (single_radius>0) call scope(subprocs)

    call write_gs2_inputs_and_outputs(gnostics)

    deallocate (fprim_gs2, tprim_gs2)

    contains
      subroutine calculate_and_broadcast_gs2_inputs
#ifndef GS2_VERSION_LE_6
        use gs2_main, only: determine_gs2spec_from_trin
        use gs2_main, only: gs2spec_from_trin, densrefspec
#endif
        use trinity_input, only: match_gs2_species
        integer :: idens

        !gs2_infile = trim(run_name)//trim(ext)//".in"
        if (model.eq."calibrate") then 
          do i = 1,calib%ncc_calibrate
            if (proc0 .and. job .eq. calib%calibration_job(i)) then
              write (ext,'(i9)') i
              ext = adjustl(ext)
            end if
          end do
          if (subfolders) then
            gs2_infile = "calibrate_"//trim(ext)//"/calibrate_"//trim(run_name)//trim(ext)//".in"
          else
            gs2_infile = "calibrate_"//trim(run_name)//trim(ext)//".in"
          end if
        else
          write (ext,'(i9)') job+1
          ext = adjustl(ext)
          if (subfolders) then
            gs2_infile = "flux_tube_"//trim(ext)//"/"//trim(run_name)//trim(ext)//".in"
            !write (*,*) 'gs2_infile is', gs2_infile
          else
            gs2_infile = trim(run_name)//trim(ext)//".in"
          end if
        end if

        call broadcast(gs2_infile)

        !if (proc0) write (*,*) 'GS2 INFILE', job+1, gs2_infile

        ! for shat << 1, the box width in x becomes so large that simulations are intractable
        if (abs(shat_ms(1)) < 0.08) then
          if (abs(shat_ms(1)) > 0.04) then
            shat_ms(1) = sign(0.08,shat_ms(1))
          else
            ! set shat so small that gs2 uses shat=0 (periodic BC)
            shat_ms(1) = 1.e-6
          end if
        end if

        bmag_over_bgeo = bmag_ms(1)/bgeo_ms(1)
        call broadcast(bmag_over_bgeo)

        btori_gs2= btori_ms(1)/bgeo_ms(1)/amin_ref
        call broadcast(btori_gs2)

        ! If using sqrt(torflux) as radial label, convert to r/a as radial variable
        ! because Miller equilibrium in GS2 is hard-wired to use r/a

        ! division by drmindrho_ms = d(r/a)/drho to convert between
        ! trin derivatives wrt rho and gs2 derivatives wrt r/a
        rhoc_gs2 = rmin_ms(1)
        call broadcast(rhoc_gs2)

        qval_gs2 = qval_ms(1)
        call broadcast(qval_gs2)

        rmajor_gs2 = rmajor_ms(1)*aref/amin_ref
        call broadcast(rmajor_gs2)

        shat_gs2 = shat_ms(1)*rmin_ms(1)/(rad_ms(1)*drmindrho_ms(1))
        call broadcast(shat_gs2)

        kappa_gs2 = kappa_ms(1)
        call broadcast(kappa_gs2)

        kapprim_gs2 = kapprim_ms(1)/drmindrho_ms(1)
        call broadcast(kapprim_gs2)

        ! TODO: check delta everywhere
        tri_gs2 = asin(delta_ms(1))
        !tri_gs2 = delta_ms(1)
        call broadcast(tri_gs2)

        triprim_gs2 = deltprim_ms(1)/sqrt(1.0-delta_ms(1)**2.0)/drmindrho_ms(1)
        !triprim_gs2 = deltprim_ms(1)/drmindrho_ms(1)
        call broadcast(triprim_gs2)

        ! shift = d(R/aref)/drho => d(R/a)/d(r/a) = shift*(aref/a)*drho/d(r/a)
        shift_gs2 = shift_ms(1)*aref/(amin_ref*drmindrho_ms(1))
        call broadcast(shift_gs2)

        ! Need to take derivative w.r.t rmin and also
        ! need to convert from Trinity B norm to GS2
        ! B norm
        betaprim_gs2 = betaprim_ms(1)/drmindrho_ms(1)&
          *bmag_over_bgeo**2
        call broadcast(betaprim_gs2)

        ! TODO: beta is not being passed to GS2 at the moment.

        fprim_gs2 = fprim_ms(1,:)/drmindrho_ms(1)
        call broadcast(fprim_gs2)

        tprim_gs2 = tprim_ms(1,:)/drmindrho_ms(1)
        call broadcast(tprim_gs2)

#ifndef GS2_VERSION_LE_6
!        if (match_gs2_species) then
!          call determine_gs2spec_from_trin(nspec, .true.)
!          idens = densrefspec
!        else
!          !idens = min(2,nspec)
!          idens = 2
!        end if
!        ! NB  dens_gs2 and temp_gs2 are not used by the
!        ! old interface
!        dens_gs2 = dens_ms(1,:)/dens_ms(1,idens)
!        call broadcast(dens_gs2)
!
!        temp_gs2 = temp_ms(1,:)/temp_ms(1,idens)
!        call broadcast(temp_gs2)
# else
        dens_gs2 = -1.0
        temp_gs2 = -1.0
#endif


        ! gexb_gs2 = r/q * domega/dr * a/v_tr
        ! gexb_ms = rho/q * domega/drho * aref/v_t0
        ! => gexb_gs2 = gexb_ms * (r/a)/rho * drho/d(r/a) * (a/aref) * (v_t0/v_tr)
        gexb_gs2 = gexb_ms(1)*rmin_ms(1)*amin_ref &
          / (sqrt(temp_ms(1,1))*aref*drmindrho_ms(1)*rad_ms(1)) * lgrad_mult
        call broadcast(gexb_gs2)
        ! mach_gs2 = omega * a/v_tr
        ! omega_ms = omega * a/v_t0
        ! => mach_gs2 = omega_ms * v_t0/v_tr
        mach_gs2 = omega_ms(1)/sqrt(temp_ms(1,1))
        call broadcast(mach_gs2)

        ! GS2 collision frequency is normalized by (a/vtr), where a is minor radius
        ! while the 'a' in the TRINITY collision frequency can be either minor radius
        ! or sqrt(torflux) at separatrix, so convert here
        nu_gs2 = (amin_ref/aref)*nu_ms(1,:)
        call broadcast(nu_gs2)


      end subroutine calculate_and_broadcast_gs2_inputs

      subroutine get_gs2_outputs
        use trinity_input, only: nspec, match_gs2_species
#ifndef GS2_VERSION_LE_6
        use gs2_main, only: determine_gs2spec_from_trin
        use gs2_main, only: gs2spec_from_trin, densrefspec
        integer :: is, isg
        if (match_gs2_species) call determine_gs2spec_from_trin(nspec, .true.)
        do is = 1,min(nspec, size(gs2_state%outputs%pflux))
          if (match_gs2_species) then
            isg = gs2spec_from_trin(is)
          else
            isg = is
          end if
          flx%p(1,is) = gs2_state%outputs%pflux(isg)
          flx%q(1,is) = gs2_state%outputs%qflux(isg)
          flx%heat(1,is) = gs2_state%outputs%heat(isg)
        end do
        flx%l(1) = gs2_state%outputs%vflux
        flx%dvdrho = gs2_state%outputs%dvdrho
        flx%grho = gs2_state%outputs%grho
#endif 
      end subroutine get_gs2_outputs

      subroutine setup_gs2_program_state
#ifndef GS2_VERSION_LE_6
        gs2_state%is_trinity_job = .true.
        gs2_state%external_job_id = job + 1
        gs2_state%mp_comm_external = .true.
        gs2_state%mp_comm = mpi_comm
        gs2_state%run_name_external = .true.
        gs2_state%run_name = gs2_infile
        gs2_state%nensembles = nensembles
#endif
      end subroutine setup_gs2_program_state

      subroutine set_miller_geometry_overrides
#ifndef GS2_VERSION_LE_6
        gs2_state%init%mgeo_ov%override_rhoc = .true.
        gs2_state%init%mgeo_ov%rhoc = rhoc_gs2

        gs2_state%init%mgeo_ov%override_qinp = .true.
        gs2_state%init%mgeo_ov%qinp = qval_gs2

        gs2_state%init%mgeo_ov%override_shat = .true.
        gs2_state%init%mgeo_ov%shat = shat_gs2

        gs2_state%init%mgeo_ov%override_rgeo_lcfs = .true.
        gs2_state%init%mgeo_ov%rgeo_lcfs = aspr
        ! aspr has been replaced by the expression below
        ! The choice we make for bmag happens to ensure
        ! that this is still equal to aspr; however, 
        ! changing this line makes it clear that what
        ! we are passing to GS2 is I_N = I / amin_ref B_ref
        gs2_state%init%mgeo_ov%rgeo_lcfs = btori_gs2 
        !write (*,*) 'aspr', aspr, gs2_state%init%mgeo_ov%rgeo_lcfs, btori_gs2

        gs2_state%init%mgeo_ov%override_rgeo_local = .true.
        gs2_state%init%mgeo_ov%rgeo_local = rmajor_gs2

        gs2_state%init%mgeo_ov%override_akappa = .true.
        gs2_state%init%mgeo_ov%akappa = kappa_gs2

        gs2_state%init%mgeo_ov%override_akappri = .true.
        gs2_state%init%mgeo_ov%akappri = kapprim_gs2

        gs2_state%init%mgeo_ov%override_tri = .true.
        gs2_state%init%mgeo_ov%tri = tri_gs2

        gs2_state%init%mgeo_ov%override_tripri = .true.
        gs2_state%init%mgeo_ov%tripri = triprim_gs2

        gs2_state%init%mgeo_ov%override_shift = .true.
        gs2_state%init%mgeo_ov%shift = shift_gs2

        gs2_state%init%mgeo_ov%override_betaprim = .true.
        gs2_state%init%mgeo_ov%betaprim = betaprim_gs2
#endif
      end subroutine set_miller_geometry_overrides

      subroutine set_profiles_overrides
        use trinity_input, only: match_gs2_species
#ifndef GS2_VERSION_LE_6
        use gs2_main, only: determine_gs2spec_from_trin
        use gs2_main, only: gs2spec_from_trin, densrefspec
        integer :: is, isg, idens
        !write (*,*) 'SET_PROFILES_OVERRIDES'
        if (match_gs2_species) then
          call determine_gs2spec_from_trin(nspec, .true.)
          idens = densrefspec
        else
          !idens = min(2,nspec)
          idens = 2
        end if
        do is = 1,min(nspec, size(gs2_state%init%prof_ov%temp))
          if (match_gs2_species) then
            !write (*,*) 'MATCHING SPECIES'
            isg = gs2spec_from_trin(is)
          else
            isg = is
          end if

          ! dens_gs2 and temp_gs2 can't be set in 
          ! calculate_and_broadcast_gs2_inputs because
          ! we need to have initialised species in gs2 
          ! in order to determine the gs2 species order.
          ! NB  dens_gs2 and temp_gs2 are not used by the
          ! old interface
          dens_gs2 = dens_ms(1,:)/dens_ms(1,idens)
          call broadcast(dens_gs2)

          temp_gs2 = temp_ms(1,:)/temp_ms(1,idens)
          call broadcast(temp_gs2)

          gs2_state%init%prof_ov%override_dens(isg) = .true.
          gs2_state%init%prof_ov%dens(isg) = dens_gs2(is)
          !gs2_state%init%prof_ov%dens(isg) = dens_ms(1,is)/dens_ms(1,idens)

          gs2_state%init%prof_ov%override_temp(isg) = .true.
          gs2_state%init%prof_ov%temp(isg) = temp_gs2(is)
          !gs2_state%init%prof_ov%temp(isg) = temp_ms(1,is)/temp_ms(1,idens)

          gs2_state%init%prof_ov%override_fprim(isg) = .true.
          gs2_state%init%prof_ov%fprim(isg) = fprim_gs2(is)

          gs2_state%init%prof_ov%override_tprim(isg) = .true.
          gs2_state%init%prof_ov%tprim(isg) = tprim_gs2(is)

          gs2_state%init%prof_ov%override_vnewk(isg) = .true.
          gs2_state%init%prof_ov%vnewk(isg) = nu_gs2(is)
        end do
        !gs2_state%init%prof_ov%override_g_exb = .true.
        !gs2_state%init%prof_ov%g_exb = gexb_gs2
        !gs2_state%init%prof_ov%override_mach = .true.
        !gs2_state%init%prof_ov%mach = mach_gs2

#endif
      end subroutine set_profiles_overrides

      subroutine group_to_jobs(var, jobs_array, nj)
        use mp_trin, only: scope, group_to_all,  allprocs, subprocs
        use trinity_input, only: njobs, ncc, njac
        real, dimension(ncc) :: cc_temp
        real, intent(in) :: var
        real, dimension(:), intent(out) :: jobs_array
        integer, intent(in) :: nj
        integer :: i,j
        call group_to_all(var, cc_temp, njobs)
        do i = 1,njac
          do j = 1,ncc
            jobs_array((i-1)*ncc+j) = cc_temp(j)
          end do
        end do
      end subroutine group_to_jobs

      subroutine write_gs2_inputs_and_outputs(gnostics)
        use trindiag_config,only: trindiag_type
        use mp_trin, only: scope, group_to_all,  allprocs, subprocs
        use trinity_input, only: njobs, ncc,nspec
        use trindiag_create_and_write, only: create_and_write_variable
        implicit none
        type(trindiag_type), intent(inout) :: gnostics
        real, dimension(njobs) :: job_var_temp
        real, dimension(njobs,nspec) :: job_spec_var_temp
        call scope(allprocs)
        call group_to_jobs(shat_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "shat_gs2", &
          "job,eval", &
           "Magnetic shear for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(rhoc_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "rhoc_gs2", &
          "job,eval", &
           "r/a for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(qval_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "qinp_gs2", &
          "job,eval", &
           "Magnetic safety factor for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(aspr, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "rgeo_lcfs_gs2", &
          "job,eval", &
           "Rgeo=(Rmidplaneout + Rmidplanein)/2 at the LCFS, &
           & used for magnetic field normalistion, &
           & for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(rmajor_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "rgeo_local_gs2", &
          "job,eval", &
           "Major radius (Rout + Rin)/2 (i.e. the Miller parameter) &
          & for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(kappa_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "kappa_gs2", &
          "job,eval", &
           "Elongation for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(kapprim_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "kapprim_gs2", &
          "job,eval", &
          "Elongation gradient (dkappa/d(r/a) for each gs2 evaluation", &
          "1", job_var_temp) 
        call group_to_jobs(tri_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "tri_gs2", &
          "job,eval", &
           "Triangularity for each gs2 evaluation. Note that &
           & tri = asin(delta) where delta is the Miller parameter.", &
           "1", job_var_temp) 
        call group_to_jobs(triprim_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "triprim_gs2", &
          "job,eval", &
           "Gradient of triangularity (d(tri)/d(r/a)) &
           & for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(shift_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "shift_gs2", &
          "job,eval", &
           "Shafranov shift for each gs2 evaluation", "1", job_var_temp) 
        call group_to_jobs(betaprim_gs2, job_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "betaprim_gs2", &
          "job,eval", &
           "Pressure gradient for each gs2 evaluation", "1", job_var_temp) 

        ! Note that the species are written in TRINITY order, regardless
        ! of the value of match_gs2_species
        call group_to_all(dens_ms(1,:)/dens_ms(1,2), job_spec_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "dens_gs2", &
          "job,tspec,eval", &
           "Species density for each gs2 evaluation", "n_i", job_spec_var_temp) 
        call group_to_all(temp_ms(1,:)/temp_ms(1,2), job_spec_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "temp_gs2", &
          "job,tspec,eval", &
           "Species temperature for each gs2 evaluation", "T_i", job_spec_var_temp) 
        call group_to_all(fprim_gs2, job_spec_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "fprim_gs2", &
          "job,tspec,eval", &
           "Logarithmic density gradient &
          & for each gs2 evaluation", "1/aref", job_spec_var_temp) 
        call group_to_all(tprim_gs2, job_spec_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "tprim_gs2", &
          "job,tspec,eval", &
           "Logarithmic temperature gradient &
          & for each gs2 evaluation", "1/aref", job_spec_var_temp) 
        call group_to_all(nu_gs2, job_spec_var_temp, njobs)
        call create_and_write_variable(gnostics, gnostics%rtype, "nu_gs2", &
          "job,tspec,eval", &
           "Collisionality &
          & for each gs2 evaluation", "vthi/aref", job_spec_var_temp) 
        call scope(subprocs)
      end subroutine write_gs2_inputs_and_outputs

    end subroutine get_fluxes_gs2


  end module fluxes_gs2
