    interface

       subroutine gs2_trin_init (rhoc, qval_cc, shat, rgeo_lcfs, rgeo_local, kap, kapprim, &
            delta, deltprim, shift, betaprim, ntspec, dens, temp, fprim, tprim, &
            gexb, mach, nu, &
            use_external_geo)
         integer, intent (in) :: ntspec
         real, intent (in) :: rhoc, qval_cc, shat, rgeo_lcfs, rgeo_local, kap, kapprim, &
              delta, deltprim, shift, betaprim, gexb, mach
         real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu
         logical, intent (in) :: use_external_geo
       end subroutine gs2_trin_init

       subroutine reset_gs2 (ntspec, dens, temp, fprim, tprim, gexb, mach, nu, nensembles)
         integer, intent (in) :: ntspec, nensembles
         real, intent (in) :: gexb, mach
         real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu
       end subroutine reset_gs2

       subroutine run_gs2 (mpi_comm, job_id, filename, nensembles, &
            pflux, qflux, vflux, heat, dvdrho, grho, trinity_reset, converged)
         integer, intent (in), optional :: mpi_comm, job_id, nensembles
         character (*), intent (in), optional :: filename
         real, dimension (:), intent (out), optional :: pflux, qflux, heat
         real, intent (out) :: vflux
         real, intent (out), optional :: dvdrho, grho
         logical, intent(in), optional :: trinity_reset
         logical, intent(out), optional :: converged
       end subroutine run_gs2

       subroutine trin_finish_gs2

       end subroutine trin_finish_gs2

    end interface
