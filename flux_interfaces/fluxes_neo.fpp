!> A module which contains subroutines for obtaining 
!! neoclassical fluxes from the NEO code.
module fluxes_neo
  use flux_results, only: flux_results_type
  implicit none
  type fluxes_neo_type
    logical :: first = .true.
  end type fluxes_neo_type

contains
  subroutine get_fluxes_neo(flx, neo)
    
    use mp_trin, only: job, communicator
    use norms, only: rhostar, aref, amin_ref
    use trinity_input, only: m_ion, nspec, charge, njac, vtfac, ncc, fork_flag
    use trinity_input, only: is_ref, njobs
    use nteqns_arrays, only: bunit_ms, bmag_ms
    use nteqns_arrays, only: temp_ms, rmin_ms, rmajor_ms, kappa_ms, delta_ms
    use nteqns_arrays, only: drmindrho_ms, kappa_ms, kapprim_ms, deltprim_ms
    use nteqns_arrays, only: shift_ms, qval_ms, shat_ms, rad_ms, bunit_ms
    use nteqns_arrays, only: fprim_ms, tprim_ms, nu_ms, dens_ms
    use nteqns_arrays, only: temp_perturb, dens_perturb
    use nteqns_arrays, only: rmin_cc, rmajor_cc, kappa_cc, kapprim_cc, delta_cc
    use nteqns_arrays, only: drmindrho_cc, deltprim_cc, shift_cc, qval_cc
    use nteqns_arrays, only: shat_cc, rad_cc, bunit_cc, fprim_perturb
    use nteqns_arrays, only: tprim_perturb, bmag_cc
    use nteqns_arrays, only: nu_perturb, pres_perturb, dens_perturb
    use constants_trin, only: mp_over_me
    use mp_trin, only: fork_gryfx, scope, gryfxprocs, igpu, allprocs
    use mp_trin, only: proc0, sum_allreduce_real_3array, abort_mp
    use trinity_input, only: n_gpus
# ifdef USE_NEO
    use neo_nclass_dr, only:  eflux_nc, pflux_nc
    use neo_interface
# endif
    type(flux_results_type), intent(inout), target :: flx
    type(fluxes_neo_type), intent(inout) :: neo
    real, dimension(:,:), pointer :: pflx_neo, qflx_neo
    character (10) :: suffix
    real :: mref
    real :: tref, nref
    real :: vneo_over_vtrin
    real :: rhostar_loc
    integer :: igr, ix, idx, gpu_counter

    ! Assign local convenience pointers

# ifndef USE_NEO
    write (*,*) 'ERROR: neo_option = "neo" but trinity was not built &
    & with neo '
    call abort_mp
# endif
    pflx_neo=>flx%p_neo
    qflx_neo=>flx%q_neo

    if (.not. fork_flag) then
      if (neo%first) call fork_gryfx(n_gpus)
      call scope(gryfxprocs)
    end if

    mref = m_ion(1)
    if (fork_flag) then
      write (suffix,'(i9)') job+1
      suffix = adjustl(suffix)

      if (neo%first) then
# ifdef USE_NEO
        call neo_init (trim(suffix)//'.', communicator)
# endif
        neo%first = .false.
      end if

      ! set reference temperature in NEO to main ion temperature
      tref = temp_ms(1,2)
      ! set reference density in NEO to main ion density
      nref = dens_ms(1,2)

      vneo_over_vtrin = sqrt(tref*m_ion(1)/(vtfac*mref*temp_ms(1,is_ref)))

      ! miller local equilibrium
# ifdef USE_NEO
      neo_equilibrium_model_in = 2
      neo_rmin_over_a_in = rmin_ms(1)
      neo_rmaj_over_a_in = rmajor_ms(1)
      neo_kappa_in = kappa_ms(1)
      neo_s_kappa_in = kapprim_ms(1)*rmin_ms(1)/(drmindrho_ms(1)*kappa_ms(1))

      ! TODO: Michael are you happy with this?
      !neo_delta_in = sin(delta_ms(1))
      ! neo_delta_in is just Miller parameter, so just
      neo_delta_in = delta_ms(1)
      ! this is r (ddelta_Miller/dr) and so the formula
      ! below seems to be correct.
      neo_s_delta_in = deltprim_ms(1)*rmin_ms(1)/drmindrho_ms(1)

      neo_shift_in = shift_ms(1)*aref/(amin_ref*drmindrho_ms(1))
      neo_q_in = qval_ms(1)
      neo_shear_in = shat_ms(1)*rmin_ms(1)/(rad_ms(1)*drmindrho_ms(1))
      neo_rho_star_in = rhostar*sqrt(mref*tref/m_ion(1))/(vtfac*bunit_ms(1))

      neo_n_species_in = nspec
      ! neo allows up to 4 kinetic species

      neo_z_1_in = charge(1)
      neo_mass_1_in = 1./(mref*mp_over_me)
      neo_dens_1_in = dens_ms(1,1)/nref
      neo_temp_1_in = temp_ms(1,1)/tref
      neo_dlnndr_1_in = fprim_ms(1,1)/drmindrho_ms(1)
      neo_dlntdr_1_in = tprim_ms(1,1)/drmindrho_ms(1)
      neo_nu_1_in = nu_ms(1,1)/vneo_over_vtrin
      if (nspec > 1) then
        neo_z_2_in = charge(2)
        neo_mass_2_in = m_ion(1)/mref
        neo_dens_2_in = dens_ms(1,2)/nref
        neo_temp_2_in = temp_ms(1,2)/tref
        neo_dlnndr_2_in = fprim_ms(1,2)/drmindrho_ms(1)
        neo_dlntdr_2_in = tprim_ms(1,2)/drmindrho_ms(1)
        if (nspec > 2) then
          neo_z_3_in = charge(3)
          neo_mass_3_in = m_ion(2)/mref
          neo_dens_3_in = dens_ms(1,3)/nref
          neo_temp_3_in = temp_ms(1,3)/tref
          neo_dlnndr_3_in = fprim_ms(1,3)/drmindrho_ms(1)
          neo_dlntdr_3_in = tprim_ms(1,3)/drmindrho_ms(1)
          if (nspec > 3) then
            neo_z_4_in = charge(4)
            neo_mass_4_in = m_ion(3)/mref
            neo_dens_4_in = dens_ms(1,4)/nref
            neo_temp_4_in = temp_ms(1,4)/tref
            neo_dlnndr_4_in = fprim_ms(1,4)/drmindrho_ms(1)
            neo_dlntdr_4_in = tprim_ms(1,4)/drmindrho_ms(1)
            if (nspec > 4) then
              write (*,*) 'Neo only supports up to four kinetic species.'
              write (*,*) 'No fluxes will be calculated for remaining species.'
            end if
          end if
        end if
      end if

      call neo_run

      rhostar_loc = rhostar*sqrt(temp_ms(1,is_ref))/bmag_ms(1)

      pflx_neo(1,:) = neo_pflux_dke_out(:nspec)*nref*vneo_over_vtrin &
        / (dens_ms(1,is_ref)*rhostar_loc**2)
      qflx_neo(1,:) = neo_efluxtot_dke_out(:nspec)*nref*tref*vneo_over_vtrin &
        / (dens_ms(1,is_ref)*temp_ms(1,is_ref)*rhostar_loc**2)
# endif

    else
      flx%p_neo_cc = 0.0
      flx%q_neo_cc = 0.0
      gpu_counter = -1
      !neo_sim_model_in = 0
      do igr = 1, njac
        do ix = 1, ncc

          ! gpu_counter determines which gpu the job will run on.
          ! There need be no relationship between the number of
          ! jobs (i.e. the number of cell centres and evolved
          ! profiles) and the number of gpus.
          gpu_counter = gpu_counter + 1
          if (.not. (mod(gpu_counter, n_gpus).eq. igpu)) cycle


          idx=ix+(igr-1)*ncc

          write (suffix,'(i9)') idx
          suffix = adjustl(suffix)
# ifdef USE_NEO
          if (neo%first) then
            call neo_init(trim(suffix)//'.', communicator)
          end if
# endif

          tref = temp_perturb(ix,2)
          nref = dens_perturb(ix,2)

          vneo_over_vtrin = sqrt(tref*m_ion(1)/(vtfac*mref*temp_perturb(idx,is_ref)))

          ! miller local equilibrium
# ifdef USE_NEO
          neo_equilibrium_model_in = 2
          neo_rmin_over_a_in = rmin_cc(ix)
          neo_rmaj_over_a_in = rmajor_cc(ix)*aref/amin_ref
          neo_kappa_in = kappa_cc(ix)
          neo_s_kappa_in = kapprim_cc(ix)*rmin_cc(ix)/(drmindrho_cc(ix)*kappa_cc(ix))
          ! TODO: Michael are you happy with this?
          neo_delta_in = delta_cc(ix)
          neo_s_delta_in = deltprim_cc(ix)*rmin_cc(ix)/drmindrho_cc(ix)
          neo_shift_in = shift_cc(ix)*aref/(amin_ref*drmindrho_cc(ix))
          neo_q_in = qval_cc(ix)
          neo_shear_in = shat_cc(ix)*rmin_cc(ix)/(rad_cc(ix)*drmindrho_cc(ix))
          neo_rho_star_in = rhostar*sqrt(mref*tref/m_ion(1))/(vtfac*bunit_cc(ix))

          neo_n_species_in = nspec
          ! neo allows up to 4 kinetic species

          neo_z_1_in = charge(1)
          neo_mass_1_in = 1./(mref*mp_over_me)
          neo_dens_1_in = dens_perturb(idx,1)/nref
          neo_temp_1_in = temp_perturb(idx,1)/tref
          neo_dlnndr_1_in = fprim_perturb(idx,1)/drmindrho_cc(ix)
          neo_dlntdr_1_in = tprim_perturb(idx,1)/drmindrho_cc(ix)
          neo_nu_1_in = nu_perturb(idx,1)/vneo_over_vtrin

          if (nspec > 1) then
            neo_z_2_in = charge(2)
            neo_mass_2_in = m_ion(1)/mref
            neo_dens_2_in = dens_perturb(idx,2)/nref
            neo_temp_2_in = temp_perturb(idx,2)/tref
            neo_dlnndr_2_in = fprim_perturb(idx,2)/drmindrho_cc(ix)
            neo_dlntdr_2_in = tprim_perturb(idx,2)/drmindrho_cc(ix)
            if (nspec > 2) then
              neo_z_3_in = charge(3)
              neo_mass_3_in = m_ion(2)/mref
              neo_dens_3_in = dens_perturb(idx,3)/nref
              neo_temp_3_in = temp_perturb(idx,3)/tref
              neo_dlnndr_3_in = fprim_perturb(idx,3)/drmindrho_cc(ix)
              neo_dlntdr_3_in = tprim_perturb(idx,3)/drmindrho_cc(ix)
              if (nspec > 3) then
                neo_z_4_in = charge(4)
                neo_mass_4_in = m_ion(3)/mref
                neo_dens_4_in = dens_perturb(idx,4)/nref
                neo_temp_4_in = temp_perturb(idx,4)/tref
                neo_dlnndr_4_in = fprim_perturb(idx,4)/drmindrho_cc(ix)
                neo_dlntdr_4_in = tprim_perturb(idx,4)/drmindrho_cc(ix)
                if (nspec > 4) then
                  write (*,*) 'Neo only supports up to four kinetic species.'
                  write (*,*) 'No fluxes will be calculated for remaining species.'
                end if
              end if
            end if
          end if

          call neo_run
# endif

          write (*,'(a17,i3,a7,i3)') 'finished neo run', idx, 'out of', njobs

          rhostar_loc = rhostar*sqrt(temp_perturb(idx,is_ref))/bmag_cc(ix)

          !                 write (*,*) 'neo_flux', rad_cc(ix), neo_pflux_dke_out(1)*nref/dens_cc(ix,1)*sqrt(tref/temp_cc(ix,1)) &
          !                      *sqrt(2./mref)*vtfac*temp_cc(ix,is_ref)/temp_cc(ix,1)*(bunit_cc(ix)/bmag_cc(ix))**2 &
          !                      *m_ion(1)/2./rhostar_loc**2, &
          ! !                     /(mref/m_ion(1)*bmag_cc(ix)/bunit_cc(ix)*rhostar_loc*vneo_over_vtrin)**2, &
          !                      neo_pflux_dke_out(2)*nref/dens_cc(ix,1)*sqrt(tref/temp_cc(ix,1)) &
          !                      *sqrt(2./mref)*vtfac*temp_cc(ix,is_ref)/temp_cc(ix,1)*(bunit_cc(ix)/bmag_cc(ix))**2 &
          !                      *m_ion(1)/2./rhostar_loc**2, &
          ! !                     neo_pflux_dke_out(2)*nref/dens_cc(ix,1)*sqrt(tref/temp_cc(ix,1)) &
          ! !                     /(mref/m_ion(1)*bmag_cc(ix)/bunit_cc(ix)*rhostar_loc*vneo_over_vtrin)**2, &
          !                      neo_efluxtot_dke_out(1)*nref/dens_cc(ix,1)*(tref/temp_cc(ix,1))**1.5 &
          !                      *(bunit_cc(ix)/bmag_cc(ix))**2*sqrt(2./mref)*vtfac*temp_cc(ix,is_ref)/temp_cc(ix,1) &
          !                      * m_ion(1)/2./rhostar_loc**2, &
          !                      neo_efluxtot_dke_out(2)*nref/dens_cc(ix,1)*(tref/temp_cc(ix,1))**1.5 &
          !                      *(bunit_cc(ix)/bmag_cc(ix))**2*sqrt(2./mref)*vtfac*temp_cc(ix,is_ref)/temp_cc(ix,1) &
          !                      * m_ion(1)/2./rhostar_loc**2
          ! !                     /(mref/m_ion(1)*bmag_cc(ix)/bunit_cc(ix)*rhostar_loc*vneo_over_vtrin)**2, &
          !  !                    neo_efluxtot_dke_out(2)*nref/dens_cc(ix,1)*sqrt(tref/temp_cc(ix,1))*tref/temp_cc(ix,1) &
          !  !                    /(mref/m_ion(1)*bmag_cc(ix)/bunit_cc(ix)*rhostar_loc*vneo_over_vtrin)**2


# ifdef USE_NEO
        if (proc0) then
          flx%p_neo_cc(ix,:,igr) = neo_pflux_dke_out(:nspec)*nref*vneo_over_vtrin &
            / (dens_perturb(idx,is_ref)*rhostar_loc**2)
          flx%q_neo_cc(ix,:,igr) = neo_efluxtot_dke_out(:nspec)*nref*tref*vneo_over_vtrin &
            / (pres_perturb(idx,is_ref)*rhostar_loc**2)
          !flx%p_neo_cc(ix,:,igr) = pflux_nc(:nspec)*nref*vneo_over_vtrin &
            !/ (dens_perturb(idx,is_ref)*rhostar_loc**2)
          !flx%q_neo_cc(ix,:,igr) = eflux_nc(:nspec)*nref*tref*vneo_over_vtrin &
            !/ (pres_perturb(idx,is_ref)*rhostar_loc**2)
          write (*,*) 'NEO FLUX', flx%q_neo_cc(ix,:,igr), neo_efluxtot_dke_out
        end if
# endif

        end do
      end do
      neo%first = .false.
      call scope(allprocs)
      call  sum_allreduce_real_3array(flx%p_neo_cc)
      call  sum_allreduce_real_3array(flx%q_neo_cc)
      if (proc0) write (*,*) 'NEO FLUX FINAL', flx%q_neo_cc

    end if
  end subroutine get_fluxes_neo
end module fluxes_neo
