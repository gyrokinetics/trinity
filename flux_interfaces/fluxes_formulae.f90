module fluxes_formulae
  use flux_results, only: flux_results_type
  use flux_results, only: gather_forked_fluxes
  use trinity_type_module, only: trinity_clock_type
  use calibrate_type_module, only: calibrate_type
  implicit none
  type fluxes_formulae_type
  end type fluxes_formulae_type

contains
  subroutine get_fluxes_formulae(flx, clock, calib, model, formulae, flux_option_switch)
    use mp_trin, only: abort_mp
    use norms, only: rhostar, aref, amin_ref
    use trinity_input, only: m_ion, nspec, charge, njac, ncc, vtfac, fork_flag
    use trinity_input, only: flux_option_test2, flux_option_test3, flux_option_pb
    use trinity_input, only: flux_option_ifspppl, flux_option_offlin
    use trinity_input, only: flux_option_test1, nrad_global
    use trinity_input, only: flux_option_test_calibrate
    use trinity_input, only: single_radius, ifspppl_bmag
    use nteqns_arrays, only: bunit_ms, bmag_ms
    use nteqns_arrays, only: temp_ms, rmin_ms, rmajor_ms, kappa_ms, delta_ms
    use nteqns_arrays, only: drmindrho_ms, kappa_ms, kapprim_ms, deltprim_ms
    use nteqns_arrays, only: shift_ms, qval_ms, shat_ms, rad_ms, bunit_ms
    use nteqns_arrays, only: fprim_ms, tprim_ms, nu_ms, dens_ms
    use nteqns_arrays, only: temp_perturb, dens_perturb
    use nteqns_arrays, only: rmin_cc, rmajor_cc, kappa_cc, kapprim_cc, delta_cc
    use nteqns_arrays, only: drmindrho_cc, deltprim_cc, shift_cc, qval_cc
    use nteqns_arrays, only: beta_ms, beta_perturb, grho_ms, zeff_ms, gexb_ms
    use nteqns_arrays, only: shat_cc, rad_cc, bunit_cc, fprim_perturb
    use nteqns_arrays, only: grho_cc, shat_cc, zeff_cc, gexb_cc, bmag_cc
    use nteqns_arrays, only: tprim_perturb, pres_ms, omega_ms
    use nteqns_arrays, only: pres_perturb, gexb_perturb, omega_perturb
    use constants_trin, only: mp_over_me
    use nteqns, only: init_geo
    implicit none
    type(flux_results_type), intent(inout), target :: flx
    type(fluxes_formulae_type), intent(inout) :: formulae
    type(trinity_clock_type), intent(inout), target :: clock
    type(calibrate_type), intent(in) :: calib
    integer, intent(in) :: flux_option_switch
    character(*), intent(in) :: model
    integer :: igr, ix, idx, i

    select case(flux_option_switch)
    case (flux_option_ifspppl)

      if (fork_flag) then
        call ifspppl_fluxes (rad_ms(1), &
          grho_ms(1), &
          dens_ms(1,:), & 
          temp_ms(1,:), &
          fprim_ms(1,:), &
          tprim_ms(1,:), &
          qval_ms(1), &
          shat_ms(1), &
          kappa_ms(1), &
          flx%p(1,:), &
          flx%q(1,:), &
          flx%l(1), &
          flx%heat(1,:), &
          zeff_ms(1), &
          rmajor_ms(1), &
          gexb_ms(1))

        ! gather fluxes from group proc0s to global proc0
        call gather_forked_fluxes(flx)
      else
        do igr = 1, njac
          do ix = 1, ncc
            idx = ix+(igr-1)*ncc
            if (single_radius>0.and..not.ix.eq.single_radius) cycle 
            call ifspppl_fluxes (rad_cc(ix), &
              grho_cc(ix), &
              dens_perturb(idx,:), &
              temp_perturb(idx,:), &
              fprim_perturb(idx,:), &
              tprim_perturb(idx,:), &
              qval_cc(ix), &
              shat_cc(ix), &
              kappa_cc(ix), &
              flx%p_cc(ix,:,igr), &
              flx%q_cc(ix,:,igr), &
              flx%l_cc(ix,igr), &
              flx%heat_cc(ix,:,igr), &
              ! TMP FOR TGYRO COMPARISON
            ! 1.0, & ! set zeff = 1
            zeff_cc(ix), &
              rmajor_cc(ix), &
              gexb_cc(ix))
            !                write (*,*) igr, ix, rad_cc(ix), &
            !                        'dens, 10^20', dens_perturb(idx,1), &
            !                        'i, e temp', temp_cc(ix,:), &
            !                        'dens grad (a/ln)', fprim_perturb(ix+(igr-1)*ncc,1), &
            !                        'temp grad (a/lT)', tprim_perturb(ix+(igr-1)*ncc,:), &
            !                        'q', qval_cc(ix), &
            !                        'shat', shat_cc(ix), &
            !                        'elong (kappa)', kappa_cc(ix), &
            !                        'flx%heat flux i, e', flx%q_cc(ix, :, igr)
          end do
          !write (*,*)
        end do
        !          do ix = 1, ncc
        !             write (*,*) 'flx%qi', ix, flx%q_cc(ix,2,1)
        !          end do

        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if
      select case (trim(ifspppl_bmag))
      case ('trinity')
        ! do nothing
      case ('bunit')
        ! In this case we are defining the normalising field within
        ! ifspppl to be bunit and thus we need to convert it to bmag
        do i = 1,nspec
          do igr = 1,njac
            flx%q_cc(:,i,igr) = flx%q_cc(:,i,igr) * (bmag_cc / bunit_cc)**2.0
            flx%p_cc(:,i,igr) = flx%p_cc(:,i,igr) * (bmag_cc / bunit_cc)**2.0
            flx%l_cc(:,igr) = flx%l_cc(:,igr) * (bmag_cc / bunit_cc)**2.0
          end do
        end do
      end select
    case (flux_option_test1)

      ! artificially setup the diffusion equation for the electron and ion pressure
      ! evolution equations.  initial and boundary conditions setup for diffusion
      ! of Gaussian (see MAB thesis p. 129 at arXiv:0901.2868)

      if (fork_flag) then
        do ix = 1, nrad_global
          call test1_fluxes (flx%p(ix,:), flx%q(ix,:), &
            flx%heat(ix,:), flx%l(ix), &
            fprim_ms(ix,:), tprim_ms(ix,:), pres_ms(ix,:), dens_ms(ix,:), &
            qval_ms(ix), rad_ms(ix), &
            rmajor_ms(ix), gexb_ms(ix), omega_ms(ix))
        end do
        ! gather fluxes from group proc0s to global proc0
        call gather_forked_fluxes(flx)
      else
        do igr = 1, njac
          do ix = 1, ncc
            idx = ix+(igr-1)*ncc 
            call test1_fluxes (&
              flx%p_cc(ix,:,igr), &
              flx%q_cc(ix,:,igr), &
              flx%heat_cc(ix,:,igr), flx%l_cc(ix,igr), &
              fprim_perturb(idx,:), tprim_perturb(idx,:), &
              pres_perturb(idx,:), dens_perturb(idx,:), &
              qval_cc(ix), rad_cc(ix), rmajor_cc(ix), &
              gexb_perturb(idx), omega_perturb(idx))
          end do
        end do
        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if

    case (flux_option_test2)

      if (fork_flag) then
        flx%p = 0.0 ; flx%q = 0.0 ; flx%heat = 0.0 ; flx%l = 0.0
        ! gather fluxes from group proc0s to global proc0
        call gather_forked_fluxes(flx)
      else
        flx%p_cc = 0.0 ; flx%q_cc = 0.0 ; flx%heat_cc = 0.0 ; flx%l_cc = 0.0
        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if

    case (flux_option_test3)

      ! artificially setup advection equation for ion and electron pressure evolution
      ! and dn/dt = 0.  initial and boundary conditions setup for advection of
      ! decaying exponential (see MAB thesis p. 129 at arXiv:0901.2868)

      if (fork_flag) then
        do ix = 1, nrad_global
          call test3_fluxes (flx%p(ix,:), flx%q(ix,:), flx%heat(ix,:), dens_ms(ix,:), pres_ms(ix,:), fprim_ms(ix,:), tprim_ms(ix,:))
        end do
        ! gather fluxes from group proc0s to global proc0
        call gather_forked_fluxes(flx)
      else
        do igr = 1, njac
          do ix = 1, ncc
            idx = ix+(igr-1)*ncc
            call test3_fluxes (flx%p_cc(ix,:,igr), flx%q_cc(ix,:,igr), flx%heat_cc(ix,:,igr), &
              dens_perturb(idx,:), pres_perturb(idx,:), fprim_perturb(idx,:), &
              tprim_perturb(idx,:))
          end do
        end do
        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if

    case (flux_option_test_calibrate)

      !write (*,*) 'MODEL IS', model

      if (fork_flag) then
        if (model.eq."calibrate") then
          !write(*,*) 'IS CALIBRATE'
          flx%p = 2.0 ; flx%q = 3.0 ; flx%heat = 1.0 ; flx%l = 1.0
        else
          if (clock%neval.eq.1.and.model.eq."reduced") then
            flx%p = 0.0 ; flx%q = 0.0 ; flx%heat = 0.0 ; flx%l = 0.0
          else
            flx%p = rad_ms(1) ; flx%q = rad_ms(1) ; flx%heat = 1.0 ; flx%l = 1.0
          end if
        end if
        ! gather fluxes from group proc0s to global proc0
        call gather_forked_fluxes(flx)
      else
        do ix = 1,ncc
          if (model.eq."calibrate") then
            flx%p_cc(ix,:,:) = 2.0 ; flx%q_cc(ix,:,:) = 3.0 ; flx%heat_cc(ix,:,:) = 1.0 ; flx%l_cc = 1.0
          else
            if (clock%neval.eq.1.and.model.eq."reduced") then
              flx%p_cc = 0.0 ; flx%q_cc = 0.0 ; flx%heat_cc = 0.0 ; flx%l_cc = 0.0
            else
              flx%p_cc(ix,:,:) = rad_cc(ix) ; flx%q_cc(ix,:,:) = rad_cc(ix) 
              flx%heat_cc(ix,:,:) = rad_cc(ix) ; flx%l_cc(ix, :) = rad_cc(ix)
            end if
          end if
        end do
        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if


    case (flux_option_pb)

      if (fork_flag) then
        do ix = 1, nrad_global
          call pb_fluxes (flx%p(ix,:), flx%q(ix,:), flx%heat(ix,:), flx%l(ix), &
            tprim_ms(ix,:), gexb_ms(ix), shat_ms(ix), qval_ms(ix), rad_ms(ix), rmajor_ms(ix))
          ! gather fluxes from group proc0s to global proc0
        end do

        call gather_forked_fluxes(flx)
      else
        do igr = 1, njac
          do ix = 1, ncc
            idx = ix+(igr-1)*ncc
            call pb_fluxes (flx%p_cc(ix,:,igr), flx%q_cc(ix,:,igr), &
              flx%heat_cc(ix,:,igr), flx%l_cc(ix,igr), &
              tprim_perturb(idx,:), gexb_perturb(idx), shat_cc(ix), &
              qval_cc(ix), rad_cc(ix), rmajor_cc(ix))
          end do
        end do

        if (flx%geo_first) then
          call init_geo
          flx%geo_first = .false.
        end if
      end if

  case (flux_option_offlin)

    if (fork_flag) then
      call offlin_fluxes (flx%p(1,:), flx%q(1,:), flx%heat(1,:), fprim_ms(1,:), tprim_ms(1,:), temp_ms(1,:), rad_ms(1))

      ! gather fluxes from group proc0s to global proc0
      call gather_forked_fluxes(flx)
    else
      do igr = 1, njac
        do ix = 1, ncc
          idx = ix+(igr-1)*ncc
          call offlin_fluxes (flx%p_cc(ix,:,igr), flx%q_cc(ix,:,igr), &
            flx%heat_cc(ix,:,igr), fprim_perturb(idx,:), &
            tprim_perturb(idx,:), temp_perturb(idx,:), rad_cc(ix))
        end do
      end do

      if (flx%geo_first) then
        call init_geo
        flx%geo_first = .false.
      end if
    end if
  end select

end subroutine get_fluxes_formulae

subroutine ifspppl_fluxes (rhoc, grho, dens, temp, fprim, tprim, qval, shat, kap, &
    pflux, qflux, lflux, heating, zth, rmaj, gexb_in)

  use mp_trin, only: abort_mp
  use trinity_input, only: flxmult, pflx_rlnfac, pflx_rltfac, pflx_nfac, vtfac
  use trinity_input, only: prandtl, lgrad_mult
  use trinity_input, only: m_ion, z_ion, n_ion_spec
  use ifs_pppl, only: ip_chi
  use norms, only: aref

  implicit none

  real, intent (in) :: rhoc, qval, shat, kap, zth, rmaj
  real, intent (in) :: grho
  real, dimension (:), intent (in) :: dens, fprim
  real, dimension (:), intent (in) :: temp, tprim
  real, dimension (:), intent (out) :: pflux, qflux, heating
  real, intent (out) :: lflux 
  real, intent (in) :: gexb_in

  real :: rlni, rlne, rlti, rlte, gnu, tau, rhoi, rhoe, vte, vti
  real :: nbeam, eps, rmajor
  real :: chii, chie, rltcrit, rltcritz, chi0, gfac, gamma
  real :: vshear
  real :: gradomega
  real :: mi_fac

  mi_fac = m_ion(1)
  !mi_fac = 1.0


  !write (*,*) 'tprim, qval is', tprim, qval, shat, kap
  !write (*,*) 'fprim, rmaj is', fprim, rmaj
  !write (*,*) 'grho is', grho
  !write (*,*) 'dens is', dens
  !write (*,*) 'zth is ', zth
  !write (*,*) 'temp is ', temp
  pflux = 0. ; qflux = 0. ; heating = 0.

  ! convert from a/L to R/L
  rlti = tprim(2)*rmaj
  rlte = tprim(1)*rmaj
  rlni = fprim(2)*rmaj
  rlne = fprim(1)*rmaj

  nbeam = 0.0
  rhoi = 1.0 ; vti = 1.0 ; rmajor = 1.0

  ! gnu = 2.5e-7 * n_e / (T_e**1.5 T_i**0.5) * rmajor,
  ! with n_e in cm**-3, T_e and T_i in eV,
  ! and rmajor in m
  tau = temp(2)/temp(1)
  gnu = 2.5e+1*dens(1)/(sqrt(temp(2))*temp(1)**1.5)*rmaj*aref
  eps = rhoc/rmaj
  !vshear = 0.0
  !gexb = vshear/(vth_trin/a) = vshear/(vth*vtfac^0.5/a)
  vshear = gexb_in * vti * vtfac**0.5 / rmajor * lgrad_mult
  gradomega = gexb_in * qval / rhoc / grho 
  ! rhoe and vte are dummy variables
  rhoe = 1.0 ; vte = 1.0

  ! assumes Miller local equilibrium
  ! returns chi_i in units of rho_i^2 v_ti / a (consistent with gs2)
  call ip_chi (abs(rlti), rlni, rlne, qval, kap, shat, zth, nbeam, tau, eps, gnu, &
    vshear, rmajor, rhoi, vti, rhoe, vte, .false., abs(rlte), &
    rltcrit, rltcritz, chi0, gfac, chii, chie, gamma)

  !write (*,*) 'ip_chi results', rhoc, chii, 'other', qval, kap, zth, tau, gnu
  ! ifspppl.pdf explains the heat flux normalization
  qflux(2) = flxmult*chii*tprim(2)/(vtfac**1.5*rmaj*sqrt(mi_fac))*rmajor/(vti*rhoi**2)!*rlti/abs(rlti)
  !    pflux(2) = (pflx_rlnfac*rlni + pflx_nfac*dens(2) + pflx_rltfac*rlte)
  pflux(2) = pflx_rlnfac*rlni + pflx_nfac
  lflux = -gradomega*rmajor**2.0*prandtl*chii*rmajor/(rhoi**2*vti**2*vtfac**2.0)
  !    heating(1) = -tprim(1)*qflux(1) + (1.5*tprim(1)-fprim)*pflux(1)
  qflux(1) = flxmult*(dens(1)/dens(2))*(temp(1)/temp(2))*chie*tprim(1)/(vtfac**1.5*rmaj*sqrt(mi_fac))*rmajor/(vti*rhoi**2)
  if (n_ion_spec > 1) then
    pflux(3) = dens(3)/dens(2)*(pflx_rlnfac*fprim(3)*rmaj + pflx_nfac)
    qflux(3) = dens(3)/dens(2)*(temp(3)/temp(2))**1.5*sqrt(mi_fac/m_ion(2))*qflux(2)
  end if
  pflux(1) = sum(z_ion*pflux(2:))

  !       heating(1) = -tprim(2)*qflux(2) + (1.5*tprim(2)-fprim)*pflux(2)/tau

end subroutine ifspppl_fluxes

subroutine offlin_fluxes (pflux, qflux, heating, fprim, tprim, temp, rad)

  implicit none

  real, intent (in) :: rad
  real, dimension (:), intent (in) :: fprim, tprim, temp
  real, dimension (:), intent (out) :: pflux, qflux, heating
  real :: sgntprim

  pflux = 0. ; qflux = 0. ; heating = 0.

  pflux(2) = 100.*(fprim(2)-0.5*rad)
  qflux(2) = 100.*max(0.,tprim(2)-2.5*rad)
  pflux(1) = pflux(2)
  qflux(1) = 100.*max(0.,tprim(1)-2.0*rad)

end subroutine offlin_fluxes

subroutine test1_fluxes (pflux, qflux, heating, lflux, fprim, tprim, pres, dens, &
    qval, rad, rgeo, gexb, omega)

  use trinity_input, only: dfac
  use trinity_input, only: nspec
  use trinity_input, only: m_ion
  use trinity_input, only: evolve_density, evolve_temperature, evolve_flow

  implicit none

  real, intent (in) :: qval, rad, rgeo, gexb, omega
  real, dimension (:), intent (in) :: fprim, tprim, pres, dens
  real, dimension (:), intent (out) :: pflux, qflux, heating
  real, intent (out) :: lflux

  integer :: ulim

  ulim = min(size(pres),nspec)

  pflux = 0.0 ; qflux = 0.0 ; heating = 0.0
  if (evolve_density .and. evolve_temperature) then
    pflux(:ulim) = dfac*fprim
    qflux(:ulim) = dfac*1.5*(fprim+tprim)*pres/pres(2)
    if (evolve_flow) then
      lflux = dfac*(fprim(1)-gexb*qval*sqrt(pres(2)/dens(1))/(rad*omega))
    end if
  else if (evolve_flow) then
    lflux = -dfac*(qval/rad)*dens(1)*m_ion(1)*rgeo**2*gexb
    !lflux = -1.5*dfac*(qval/rad)*sqrt(pres(2)/dens(1))*m_ion(1)*rgeo**2*gexb
    !lflux = -dfac*1.5*gexb/(sqrt(pres(2)/dens(1)))*m_ion(1)*rgeo**2
  else
    qflux(:ulim) = dfac*1.5*(fprim+tprim)*pres/pres(2)**2.5
    pflux(:ulim) = dfac*fprim*dens**1.5
  end if

end subroutine test1_fluxes

subroutine test3_fluxes (pflux, qflux, heating, dens, pres, fprim, tprim)

  use trinity_input, only: nspec

  implicit none

  real, dimension (:), intent (in) :: dens, pres, fprim, tprim
  real, dimension (:), intent (out) :: pflux, qflux, heating

  integer :: ulim

  ulim = min(size(pres),nspec)

  pflux = 0. ; qflux = 0. ; heating = 0.

  pflux(:ulim) = sqrt(dens)/pres(2)**1.5
  qflux(:ulim) = pres/pres(2)**2.5
  heating(:ulim) = 3.*(fprim+tprim)*pres/pres(2)**2.5

end subroutine test3_fluxes

subroutine pb_fluxes (pflux, qflux, heating, lflux, tprim, gexb, shat, qval, rad, rgeo)

  use trinity_input, only: chi0, tp0, alph1, prandtl
  !    use trinity_input, only: m_ion

  implicit none

  real, intent (in) :: gexb, shat, qval, rad, rgeo
  real, dimension (:), intent (in) :: tprim
  real, intent (out) :: lflux
  real, dimension (:), intent (out) :: pflux, qflux, heating

  real :: tpcrit, chit, alph, alph2, tpcrit0

  pflux = 0. ; qflux = 0. ; heating = 0. ; lflux = 0.

  ! all done for R/LT (instead of a/LT)
  tpcrit0 = tp0 + shat
  !    chit = chi0*(1.+shat)*(tprim(1)/tpcrit0)**(0.7)
  chit = chi0*(1.+shat)
  alph = alph1/(1.+shat)
  alph2 = alph/(alph+2.*tpcrit0)
  tpcrit = max(0.0,(tpcrit0+alph*abs(gexb*rgeo))/(1.+alph2*(gexb*rgeo)**2))

  ! aspr factors present because model constructed with R as normalizing length
  qflux(2) = max(0.,chit/rgeo*(tprim(2)-tpcrit/rgeo))
  !       qflux(2) = qflux(1)/(42.*sqrt(m_ion(1)))
  qflux(1) = 0.0
  lflux = -qflux(2)*(qval/rad)*rgeo**2*gexb/tprim(2)*prandtl

end subroutine pb_fluxes
end module fluxes_formulae
