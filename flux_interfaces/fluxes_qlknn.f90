!> A module to calculate the fluxes using 
!! a neural network interpolation of qualikiz.
!! See Citrin, Breton et al NF
module fluxes_qlknn
      REAL, DIMENSION(:,:), ALLOCATABLE :: IW,L1W  ! Weight matrices
      REAL, DIMENSION(:), ALLOCATABLE :: OW  ! Output weight matrices
      REAL, DIMENSION(:), ALLOCATABLE :: b1,b2 ! hidden array bias vectors
      REAL :: b3 !output bias vector
  contains
    subroutine get_fluxes_qlknn(ncalls, calib, model, qflx_cc, pflx_cc, lflx_cc, heat_cc, dvdrho, grho)
      use calibrate_type_module, only: calibrate_type
      use trinity_input, only: fork_flag, flux_shell_script, ncc
      use file_utils_trin, only: get_unused_unit, run_name  
      use mp_trin, only: proc0, broadcast, abort_mp
      use mp_trin, only: sum_allreduce_real_3array
      use nteqns_arrays, only: dens_perturb, temp_perturb
      use nteqns_arrays, only: fprim_perturb, tprim_perturb
      use nteqns_arrays, only: shat_cc, qval_cc
      use nteqns_arrays, only: amin_ref, bmag_grid
      use nteqns_arrays, only: tprim_cc, drmindrho_cc
      use nteqns_arrays, only: temp_cc
      implicit none
      integer, intent (in) :: ncalls
      type(calibrate_type), intent(in) :: calib
      character(*), intent(in) :: model
      real, dimension (:,:,:), intent(out) :: pflx_cc, qflx_cc, heat_cc
      real, dimension (:,:), intent(out) :: lflx_cc
      real, dimension (:), intent(out) :: dvdrho, grho
      REAL, DIMENSION(ncc) :: ti,tip,q,s,tite !input parameters into NN
      REAL, DIMENSION(ncc) :: rlti !for convenience, vector to directly specify R/LTi
      REAL, DIMENSION(ncc) :: chii,dchii_dti,dchii_dtip !output parameters from NN
      REAL :: R0,a0,B0,Amain,te !normalization parameters for gradient lengths and GB heat conductivity
      integer :: idx, igr, ix
      if (fork_flag) then
        write (*,*) 'Can`t use fork_flag with flux_option_qlknn'
        call abort_mp()
      end if
      s = shat_cc
      q = qval_cc

      !ti = temp_cc(:,1)

      ! Set plasma constants
      R0 = amin_ref ; a0 = amin_ref ; B0 = bmag_grid(1) ; Amain = 2.0 
      !te = 3d3 / 1.2 !te in keV (used to calculate ti/te)




!!$  ti(:) = 3d3 ! temperature in keV
  !ti(1) = 3.0001d3 ; ti(2) = 3.000d3 ; ti(3) = 2.9998d3 !temperature in keV
  !tip(:)  = -6d3
!!$  tip(1) = -6d3 ; tip(2) = -6.0001d3 ; tip(3) = -6.0002d3 !temperature gradient

  rlti = -R0 * tip / ti
  tite = ti/te
!!$  rlti(1)=6. ; rlti(2)=6.001 ; rlti(3)=6.002

  WRITE(*,*)
  WRITE(*,*) 'Welcome to the QuaLiKiz adiabatic electron neural network test driver!'
  WRITE(*,*) 
  WRITE(*,'(A,3G15.7,A)') 'Input Ti     = (',ti,') eV'
  WRITE(*,'(A,3G15.7,A)') 'Input dTi/dr = (',tip,') eV/m'
  WRITE(*,'(A,3G15.7,A)') 'Input R/LTi  = (',rlti,')'
  WRITE(*,'(A,3G15.7,A)') 'Input Ti/Te  = (',tite,')'

  !Initialize neural network weights and biases based on data found in NNdata folder
  CALL initmat()

  do igr = 1, njac
    idx = (igr-1)*ncc + 1
      !Magic time. Call neural network transport model and obtain output fluxes and flux derivatives
    ti = temp_perturb(idx:idx+ncc-1, 1)
    rlti = tprim_perturb(idx:idx+ncc-1, 1)/drmindrho_cc
    tip = rlti*ti/r0
    tite = temp_perturb(idx:idx+ncc-1,1)/temp_perturb(idx:idx+ncc-1,2)

    CALL qlkNN(ti,tip,tite,q,s,R0,a0,B0,Amain,IW,L1W,OW,b1,b2,b3,chii,dchii_dti,dchii_dtip)
    !call ifspppl_fluxes (rad_cc(ix), &
      !grho_cc(ix), &
      !dens_perturb(idx), &
      !temp_perturb(idx,:), &
      !fprim_perturb(idx), &
      !tprim_perturb(idx,:), &
      !qval_cc(ix), &
      !shat_cc(ix), &
      !kappa_cc(ix), &
      !pflx_cc(ix,:,igr), &
      !qflx_cc(ix,:,igr), &
      !lflx_cc(ix,igr), &
      !heat_cc(ix,:,igr), &
      !! TMP FOR TGYRO COMPARISON
    !! 1.0, & ! set zeff = 1
    !zeff_cc(ix), &
      !rmajor_cc(ix), &
      !gexb_cc(ix))
    !                write (*,*) igr, ix, rad_cc(ix), &
    !                        'dens, 10^20', dens_perturb(idx), &
    !                        'i, e temp', temp_cc(ix,:), &
    !                        'dens grad (a/ln)', fprim_perturb(ix+(igr-1)*ncc), &
    !                        'temp grad (a/lT)', tprim_perturb(ix+(igr-1)*ncc,:), &
    !                        'q', qval_cc(ix), &
    !                        'shat', shat_cc(ix), &
    !                        'elong (kappa)', kappa_cc(ix), &
    !                        'heat flux i, e', qflx_cc(ix, :, igr)
    end do
  !write (*,*)
  end do
  if (include_neo) then
    do igr = 1, njac
    do ix = 1, ncc
    idx = ix+(igr-1)*ncc
    call neo_flux (qflx_neo_cc(ix,:,igr), lflx_neo_cc(ix,igr), nu_perturb(idx,:), qval_cc(ix), &
      tprim_perturb(idx,:), gexb_perturb(idx), rad_cc(ix), shift_cc(ix))
    end do
    qflx_cc(:,:,igr) = qflx_cc(:,:,igr) + qflx_neo_cc(:,:,igr)
    lflx_cc(:,igr) = lflx_cc(:,igr) + lflx_neo_cc(:,igr)
    end do
  end if
  if (get_fluxes_first) then
    call init_geo
    get_fluxes_first = .false.
  end if

  WRITE(*,*) 
  WRITE(*,'(A,3G15.7,A)') 'Output chii                         = (',chii,') m^2/s'
  WRITE(*,'(A,3G15.7,A)') 'Output dchii_dTi (analytical)       = (',dchii_dti,') m^2/(s*eV)'
  WRITE(*,'(A,3G15.7,A)') 'Output dchii_d(dTi/dr) (analytical) = (',dchii_dtip,') m^3/(s*eV)'
  WRITE(*,*)
  WRITE(*,'(A,G15.7,A)') 'dchii_dTi from finite differences       = ',(chii(3)-chii(1))/(ti(3)-ti(1))
  WRITE(*,'(A,G15.7,A)') 'dchii_d(dTi/dr) from finite differences = ',(chii(3)-chii(1))/(tip(3)-tip(1))
  WRITE(*,*)
  !Deallocate arrays
  CALL deallocateall()

    end subroutine get_fluxes_qlknn

  SUBROUTINE initmat()
    !Initializes the NN weights and biases based on data files in the NNdata folder
    !IW, L1W, OW, b1,b2,b3 are all defined in the calling driver program, and are in the scope of this subroutine

    CHARACTER(len=20) :: fmti,fmtIW,fmtL1W,fmtOW,fmt1
    INTEGER :: i,j,myunit=700
    INTEGER, DIMENSION(2) :: dims !dimensions of input, hidden layer nodes

    WRITE(fmti, '(A)') '(I2)'
    OPEN(unit=myunit, file="NNdata/NNtopology.dat", action="read")
    READ(myunit,fmti) (dims(i),i=1,2) ; CLOSE(myunit)

    !Allocate weight and bias matrices based on provided dimensions
    ALLOCATE(IW(dims(2),dims(1)))
    ALLOCATE(L1W(dims(2),dims(2)))
    ALLOCATE(OW(dims(2)))
    ALLOCATE(b1(dims(2)))    
    ALLOCATE(b2(dims(2)))    

    WRITE(fmtIW,'(A,I1,A)') '(',dims(1),'F15.7)'
    WRITE(fmtL1W,'(A,I2,A)') '(',dims(2),'F15.7)'
    WRITE(fmtOW,'(A,I2,A)') '(',dims(2),'F15.7)'
    WRITE(fmt1, '(A)') '(F15.7)'; 

    OPEN(unit=myunit, file="NNdata/IW.dat", action="read")
    READ(myunit,fmtIW) ((IW(i,j),j=1,dims(1)),i=1,dims(2)) ; CLOSE(myunit)

    OPEN(unit=myunit, file="NNdata/L1W.dat", action="read")
    READ(myunit,fmtL1W) ((L1W(i,j),j=1,dims(2)),i=1,dims(2)) ; CLOSE(myunit)

    OPEN(unit=myunit, file="NNdata/OW.dat", action="read")
    READ(myunit,fmtOW) (OW(i),i=1,dims(2)) ; CLOSE(myunit)

    OPEN(unit=myunit, file="NNdata/b1.dat", action="read")
    READ(myunit,fmt1) (b1(i),i=1,dims(2)) ; CLOSE(myunit)

    OPEN(unit=myunit, file="NNdata/b2.dat", action="read")
    READ(myunit,fmt1) (b2(i),i=1,dims(2)) ; CLOSE(myunit)

    OPEN(unit=myunit, file="NNdata/b3.dat", action="read")
    READ(myunit,fmt1) b3 ; CLOSE(myunit)

  END SUBROUTINE initmat

  SUBROUTINE qlkNN(ti,tip,tite,q,shear,R0,a0,B0,Amain,IW,L1W,OW,b1,b2,b3,chii,dchii_dti,dchii_dtip)

    !PHYSICAL INPUTS  
    !ti - ion temperature in eV
    !tip - ion temperature gradient. 
    !tite - ion to electron temperature ratio
    !q - safety factor
    !shear - magnetic shear
    !R0/a0, major/minor radius (for GB normalization)
    !B0, magnetic field on axis (for GB normalization)
    !Amain, atomic mass (in amu) of main ion (for GB normalization)

    !!FIXED NN INPUTS
    !IW,L1W,OW,b1,b2,b3 are all the weights and biases of the NN

    !OUTPUTS
    !chii - ion heat conductivity [m^2/s]. Note: ion heat flux = chii * ni * tigrad [W/m^2]
    !dchii_dti - chii derivative with respect to ti (ti in eV)
    !dchii_dtigrad - chii derivative with respect to tip (ti in eV)

    !NOTE: Inputs and outputs can be vectors (should be the same length each, this isn't tested in the code)
    !      Normalization factors here below are hardwired for a specific 4D adiabatic ion QuaLiKiz NN

    REAL, DIMENSION(:,:), INTENT(IN) :: IW,L1W
    REAL, DIMENSION(:), INTENT(IN) :: OW
    REAL, DIMENSION(:), INTENT(IN) :: ti,tip,tite,q,shear,b1,b2
    REAL, INTENT(IN) :: b3
    REAL, INTENT(IN) :: R0,a0,B0,Amain
    REAL, DIMENSION(:), INTENT(OUT) :: chii,dchii_dti,dchii_dtip
    INTEGER :: dimin=4 !dimensionality of NN input array

    REAL, DIMENSION(:), ALLOCATABLE :: chiGB,rlti !chi in GB units, GB normalization factor, R/LTi
    REAL, DIMENSION(:,:), ALLOCATABLE :: invec,unorm ! array concatenating the input profiles 
    REAL, DIMENSION(:,:), ALLOCATABLE :: g,sg,f,sf,dsfdf,dsgdg,dgdu,dchiiGB,tmparr ! work arrays
    REAL, DIMENSION(4) :: scalefac = (/5.,2.,1.35,1.45/)   !normalization factors for input (NN neurons expect  [-1 1] input range)
    REAL, DIMENSION(4) :: dispfac = (/7.,3.,1.65,1.55/)    !normalization factors for input (NN neurons expect  [-1 1] input range)
    REAL :: maxfac = 28.378817 !normalization factor for output to chiGB (NN output is from [0 1])
    REAL :: qel=1.6022d-19, mp=1.67262d-27 !electron charge, hydrogen mass (SI)

    INTEGER :: lenR,lenNN !lengths of input arrays
    INTEGER :: i,j !loop counters
    !BEGINNING OF CODE BODY

    lenR = SIZE(ti) !find length of "radial" array
    lenNN = SIZE(b1) !find number of neurons per hidden layer

    !allocate arrays
    ALLOCATE(chiGB(lenR))
    ALLOCATE(rlti(lenR))
    ALLOCATE(invec(dimin,lenR))
    ALLOCATE(unorm(dimin,lenR))
    ALLOCATE(g(lenNN,lenR))
    ALLOCATE(sg(lenNN,lenR))
    ALLOCATE(f(lenNN,lenR))
    ALLOCATE(sf(lenNN,lenR))  
    ALLOCATE(dsfdf(lenNN,lenR))
    ALLOCATE(dsgdg(lenNN,lenR))
    ALLOCATE(dgdu(lenNN,dimin))
    ALLOCATE(dchiiGB(lenR,dimin))

    ALLOCATE(tmparr(lenNN,lenR))

    !Define gyroBohm normalization. NN outputs were defined in GB normalized units (with respect to minor radius and without sqrt(2)), so must unnormalize to SI 

    !GB normalization factor
    chiGB = SQRT(Amain*mp)/(qel**2*B0**2*a0)*(ti*qel)**1.5;

    rlti = -R0 * tip/ ti ;

    !construct invec (don't shoot me, I know there are smarter ways)
    invec(1,:) = rlti
    invec(2,:) = q
    invec(3,:) = tite
    invec(4,:) = shear

    !Normalize input matrix to match NN normalization
    DO i=1,lenR
       unorm(:,i) = (invec(:,i) - dispfac(:))/scalefac(:)  
    ENDDO

    !calculate neural network mapping piece by piece, for all radii
    g=MATMUL(IW,unorm)
    DO i=1,lenNN
       g(i,:) = g(i,:) + b1(i)
    ENDDO
    sg = sigmo(g);

    f=MATMUL(L1W,sg)
    DO i=1,lenNN
       f(i,:) = f(i,:) + b2(i)
    ENDDO

    sf = sigmo(f)

    !set output vectors
    chii = chiGB*maxfac*(MATMUL(OW,sf) + b3)

    ! derivatives w.r.t. NN input parameters. Evalation by chain rule
    dsgdg = (sg+1.)**2.*EXP(-2.*g) ! d(sig(g))/dg
    dsfdf = (sf+1.)**2.*EXP(-2.*f) ! d(sig(f))/df
    DO i=1,lenNN
       dgdu(i,:) = IW(i,:)/scalefac(:)  ! dg/duphys
    ENDDO

    DO i=1,dimin

       DO j=1,lenR
          tmparr(:,j)=dsgdg(:,j)*dgdu(:,i)
       ENDDO
       dchiiGB(:,i)=maxfac*MATMUL(OW,dsfdf*MATMUL(L1W,tmparr))
    ENDDO

    !WARNING. Think about chiGB normalization to Te or Ti and keep things consistent

    ! derivatives w.r.t. physical inputs
    dchii_dti = 1.5/ti*chii + (dchiiGB(:,3)*tite - rlti*dchiiGB(:,1))*chiGB/ti

    dchii_dtip = -chiGB*R0/ti*dchiiGB(:,1)

    !Where output is negative (due to extrapolation down below stability limit) set outputs to zero
    WHERE(chii<0) 
       chii=0
       dchii_dti=0
       dchii_dtip=0
    ENDWHERE

    DEALLOCATE(chiGB)
    DEALLOCATE(rlti)
    DEALLOCATE(invec)
    DEALLOCATE(unorm)
    DEALLOCATE(f)
    DEALLOCATE(sf)
    DEALLOCATE(g)
    DEALLOCATE(sg)
    DEALLOCATE(dsfdf)
    DEALLOCATE(dsgdg)
    DEALLOCATE(dgdu)
    DEALLOCATE(tmparr)

  END SUBROUTINE qlkNN

  FUNCTION sigmo(x)
    !NN nonlinear neuron function
    REAL, DIMENSION(:,:), INTENT(IN) :: x
    REAL, DIMENSION(SIZE(x(:,1)),SIZE(x(1,:))) :: sigmo
    sigmo = 2./(1.+EXP(-2.*x))-1.
  END FUNCTION sigmo

  SUBROUTINE deallocateall()
    DEALLOCATE(IW)
    DEALLOCATE(L1W)
    DEALLOCATE(OW)
    DEALLOCATE(b1)
    DEALLOCATE(b2)
  END SUBROUTINE deallocateall
end module fluxes_qlknn
