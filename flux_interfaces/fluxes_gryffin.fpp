!> A module for interacting with Bill & Mike's old f90 code.
!! Under construction and untested
module fluxes_gryffin
  use flux_results, only: flux_results_type
  type fluxes_gryffin_type
    logical :: first = .true.
  end type fluxes_gryffin_type
  contains
    subroutine get_fluxes_gryffin(flx, gryffin)
      type(flux_results_type), intent(inout), target :: flx
      type(fluxes_gryffin_type), intent(inout), target :: gryffin
# ifdef USE_GRYFFIN
    interface
       subroutine gryffin_trin_init (rhoc, qval_cc, shat, aspr, kap, kapprim, &
! flow
!       		  delta, deltprim, shift, betaprim, nspec, dens, temp, fprim, tprim, gexb, nu)
       		  delta, deltprim, shift, betaprim, nspec, dens, temp, fprim, tprim, nu)
         integer, intent (in) :: nspec
         real, intent (in) :: rhoc, qval_cc, shat, aspr, kap, kapprim, delta, &
! flow
!	       deltprim, shift, betaprim, dens, fprim, gexb
	       deltprim, shift, betaprim
         real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu
       end subroutine gryffin_trin_init

! flow
!       subroutine reset_gryffin (nspec, dens, temp, fprim, tprim, gexb, nu, nensembles)
       subroutine reset_gryffin (nspec, dens, temp, fprim, tprim, nu, nensembles)
         integer, intent (in) :: nspec, nensembles
! flow
!         real, intent (in) :: gexb
         real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu
       end subroutine reset_gryffin

! flow
!       subroutine run_gryffin (mpi_comm, filename, nensembles, pflux, qflux, heat, lflux, dvdrho, grho) !,converged, con_heat)
       subroutine run_gryffin (mpi_comm, filename, nensembles, pflux, qflux, heat, dvdrho, grho) !,converged, con_heat)
         integer, intent (in), optional :: mpi_comm, nensembles
         character (*), intent (in), optional :: filename
         real, dimension (:), intent (out), optional :: pflux, qflux, heat
! flow
!         real, intent (out), optional :: dvdrho, grho, lflux
         real, intent (out), optional :: dvdrho, grho
       end subroutine run_gryffin
    end interface
# endif
    real :: gexb_gryffin
    character (100), save :: gryffin_infile
    character (10) :: ext
# ifdef USE_GRYFFIN
       if (gryffin%first) then
          
          ! for shat << 1, the box width in x becomes so large that simulations are intractable
       	  if (abs(shat_ms(1)) < 0.08) then
             if (abs(shat_ms(1)) > 0.04) then
                shat_ms(1) = sign(0.08,shat_ms(1))
             else
                ! set shat so small that gryffin uses shat=0 (periodic BC)
                shat_ms(1) = 1.e-6
             end if
          end if
          
          ! gexb_gryffin = r/q * domega/dr * a/v_tr
          ! gexb_ms = rho/q * domega/drho * aref/v_t0
          ! => gexb_gryffin = gexb_ms * (r/a)/rho * drho/d(r/a) * (a/aref) * (v_t0/v_tr)
          gexb_gryffin = gexb_ms(1)*rmin_ms(1)*amin_ref &
               / (sqrt(temp_ms(1,2))*aref*drmindrho_ms(1)*rad_ms(1))
          
          call gryffin_trin_init (rad_ms(1), qval_ms(1), shat_ms(1), aspr, kappa_ms(1), kapprim_ms(1), &
               asin(delta_ms(1)), deltprim_ms(1)/sqrt(1.0-delta_ms(1)**2.0), shift_ms(1), betaprim_ms(1), nspec, dens_ms(1,:), &
! flow
!                  temp_ms(1,:), fprim_ms(1), tprim_ms(1,:), gexb_gryffin, nu_ms(1,:))
               temp_ms(1,:), fprim_ms(1,:), tprim_ms(1,:), nu_ms(1,:))
          write (ext,'(i9)') job+1
          ext = adjustl(ext)
          gryffin_infile = trim(run_name)//trim(ext)//".in"
          gryffin%first = .false.
       else
          ! flow
!          call reset_gryffin (nspec, dens_ms(1), temp_ms(1,:), fprim_ms(1), &
          !               tprim_ms(1,:), gexb_gryffin, nu_ms(1,:), nensembles)
          call reset_gryffin (nspec, dens_ms(1,:), temp_ms(1,:), &
               fprim_ms(1,:), tprim_ms(1,:), nu_ms(1,:), nensembles)
       end if
       
       ! obtain turbulent fluxes from gryffin simulation
       call run_gryffin (mpi_comm, gryffin_infile, nensembles, pflx(1,:), &
!            qflx(1,:nspec), heat(1,:nspec), lflx(1), dvdrho(1), grho(1)) !,converged, con_heat)
            qflx(1,:), heat(1,:), flx%dvdrho(1), flx%grho(1)) !,converged, con_heat)
       ! gather fluxes from group proc0s to global proc0
!       call gather_forked_fluxes (dvdrho(1), grho(1))

       call gather_forked_fluxes (dvdrho, grho)
# else
       write (*,*) "To use flux_option='gryffin', you have to make Trinity with the option FLUXCODE=gryffin."
# endif
    end subroutine get_fluxes_gryffin
end module fluxes_gryffin
