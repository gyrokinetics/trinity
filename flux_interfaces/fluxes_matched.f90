!> A module which calculates artificial fluxes which will
!! keep the profile gradients constant by being set equal to the 
!! integrated sources.
module fluxes_matched
  implicit none
contains
  function grid_oscillation_factor(profile, k2vec)
    use constants_trin, only: pi, zi
    use nteqns_arrays, only: rad_grid
    real, intent(in), dimension(:) :: profile
    real :: grid_oscillation_factor
    complex, dimension(size(profile)-1) :: kvec
    real, dimension(:), intent(out) :: k2vec
    real, dimension(size(profile)) :: profile_nolin
    real :: offset
    integer :: j,k,nrad,ncc
    nrad = size(profile)
    ncc = nrad-1

    offset = profile(nrad)-profile(1)

    ! Remove linear offest (i.e. make it periodic)
    do k=1,nrad
      profile_nolin(k) = profile(k) - &
      (rad_grid(k)-rad_grid(1))/(rad_grid(nrad) - rad_grid(1)) * &
      offset
    end do
    ! Subtract constant part
    profile_nolin = profile_nolin - profile(1)
    ! Do a Fourier transform
    do k = 1,nrad-1
      kvec(k) = 0.0
      do j = 1,nrad-1
      kvec(k) = kvec(k) + profile_nolin(j) * &
        exp(-2*pi*real(j-1)*real(k-1)*zi/real(nrad))
      end do
    end do
    k2vec = kvec*conjg(kvec)
    ! Work out the magnitude of the grid scale oscillation compared to 
    ! the magnitude of everything else.
    if (mod(ncc,2)==0) then 
      !if (ncc.gt.4) then
        !grid_oscillation_factor = k2vec(ncc/2+1)/(sum(k2vec(2:(ncc/2)))/(ncc/2-1))
      !else
        grid_oscillation_factor = k2vec(ncc/2+1)/(sum(k2vec(1:(ncc/2)))/(ncc/2))
      !end if
      !grid_oscillation_factor = k2vec(ncc/2+1)/k2vec(ncc/2)
    else
      !if (ncc.gt.4) then
        !grid_oscillation_factor = k2vec((ncc+1)/2)/(sum(k2vec(2:((ncc-1)/2)))/((ncc-1)/2-1))
      !else
        grid_oscillation_factor = k2vec((ncc+1)/2)/(sum(k2vec(1:((ncc-1)/2)))/((ncc-1)/2))
      !end if
      !grid_oscillation_factor = k2vec((ncc+1)/2)/k2vec((ncc-1)/2)
    end if
    !grid_oscillation_factor = max(grid_oscillation_factor-1.0, 0.0)
  end function grid_oscillation_factor

  subroutine get_fluxes_matched(flx, ncalls)
    use trinity_input, only: ncc, njac, nspec, fork_flag
    use nteqns_arrays, only: tprim_perturb, fprim_perturb, pprim_perturb
    !use nteqns_arrays, only: tprim_cc_init, fprim_cc_init
    use nteqns_arrays, only: pres_init, dens_init, dens_grid, pres_grid
    use nteqns_arrays, only: mom_grid, gexb_perturb, fprim_cc_init, rad_grid
    use nteqns_arrays, only: int_pflux, int_power, area_cc, grho_cc, int_equil
    use mp_trin, only: scope, allprocs, subprocs, all_to_group, current_focus, barrier
    use trinity_input, only: njobs
    use nteqns, only: get_sources, get_sources_first
    use norms, only: aref, qflx_gb_cc, pflx_gb_cc
    use flux_results, only: flux_results_type
    use unit_tests_trin, only: debug_message, verb_high
    !use ran, only: ranf
    implicit none
    integer, intent (in) :: ncalls
    type(flux_results_type), intent(inout) :: flx
    integer :: igr,ix, is, idx
    real :: deviation, diff
    real, dimension(ncc) :: init_grad

    if (fork_flag) then
      call scope(allprocs)
    end if

    call debug_message(verb_high, &
      'fluxes_matched::get_fluxes_matched starting')

    ! We need to do this to get int_pflux and int_power
    if (get_sources_first) call get_sources(0.0)
    
    do is = 1,nspec
      flx%p_oscill(is) = &
      grid_oscillation_factor(dens_grid(:,is),flx%p_oscill_measure(:,is))
      flx%q_oscill(is) = &
      grid_oscillation_factor(pres_grid(:,is),flx%q_oscill_measure(:,is))
    end do
    flx%l_oscill = grid_oscillation_factor(mom_grid(:), flx%l_oscill_measure)

    call debug_message(verb_high, &
      'fluxes_matched::get_fluxes_matched calculated oscillation factors')

    do igr = 1, njac
      do ix = 1, ncc
        idx = ix+(igr-1)*ncc
        do is = 1,nspec
          ! Note that we use int_pflux and not int_pflux_cc because we 
          ! want the power inside the radius of the cell centre. Ditto
          ! for int_power
          !pflx_cc(ix,is,igr) = int_pflux(ix,is)*1.0e20 / (area_cc(ix)*aref**2.0)/&
            !pflx_gb_cc(ix,is) * (fprim_perturb(idx,is) / fprim_cc_init(ix,is))**3
          deviation = (log(dens_grid(ix,is)/dens_grid(ix+1,is)) - &
              log(dens_init(ix,is)/dens_init(ix+1,is)))/ &
              abs(log(dens_init(ix,is)/dens_init(ix+1,is))) + 1.0
          !if (abs(deviation)<0.5) deviation = deviationa
          flx%p_cc(ix,is,igr) = (int_pflux(ix,is)*1.0e20 / (area_cc(ix)*aref**2.0)/&
            pflx_gb_cc(ix,is))* &
              (deviation) * 0.5 * &
              (fprim_perturb(idx,is)/fprim_perturb(ix,is))
          !call get_cc(dens_init(:,is), init_grad)
          !flx%p_cc(ix,is,igr) = 
          flx%p_cc(ix,is,igr) = fprim_perturb(idx,is) - fprim_cc_init(ix,is) + deviation*0.1

          deviation = log(dens_grid(ix,is)/dens_grid(ix+1,is)) - &
              log(dens_init(ix,is)/dens_init(ix+1,is))
          deviation = deviation / (rad_grid(ix+1) - rad_grid(ix))
          !diff = fprim_perturb(idx,is)-fprim_perturb(ix,is)
          !flx%p_cc(ix,is,igr) = (abs(diff))**(1.0/3.0) * diff / abs(diff) + deviation
          !if (abs(diff) .lt. epsilon(0.0)) flx%p_cc(ix,is,igr) = deviation 
          !flx%p_cc(ix,is,igr) = 1.0*((fprim_perturb(idx,is) - fprim_perturb(ix,is))  + deviation * (0.5+ ranf()))
          if (abs(int_pflux(ix,is)) .lt. epsilon(0.0)) then
            flx%p_cc(ix,is,igr) = 1.0*((fprim_perturb(idx,is) - fprim_perturb(ix,is))  + deviation )
          else
            flx%p_cc(ix,is,igr) = int_pflux(ix,is) * 1.0e20 / &
              (area_cc(ix)*aref**2.0)/ pflx_gb_cc(ix, is)* 0.5 * &
              ((fprim_perturb(idx,is) - fprim_perturb(ix,is))  + deviation + 1.0)
          end if 
          !flx%p_cc(ix,is,igr) = 1.0*((fprim_perturb(idx,is) - fprim_perturb(ix,is))  + deviation )
          !flx%p_cc(ix,is,igr) = deviation

          if (is.eq.2.and.ix.eq.14) write (*,*) &
            'p_cc is ', flx%p_cc(14,2,:)
              
          !qflx_cc(ix,is,igr) = (int_power(ix,is)+int_equil(ix,is))/&
            !(area_cc(ix)*aref**2.0) / &
            !qflx_gb_cc(ix,is) * (tprim_perturb(idx,is) / tprim_cc_init(ix,is))**3.0
          deviation = log(pres_grid(ix,is)/pres_grid(ix+1,is)) / &
              log(pres_init(ix,is)/pres_init(ix+1,is)) 
          !if (deviation>0.0) deviation = deviation**3.0
          flx%q_cc(ix,is,igr) = (int_power(ix,is)+int_equil(ix,is))/&
            (area_cc(ix)*aref**2.0) / &
            qflx_gb_cc(ix,is) * &
              (deviation) * &
              (pprim_perturb(idx,is)/pprim_perturb(ix,is))

          ! Also calc damped fluxes, these damp differences between the profile
          ! at ix and the profile at ix as interpolated between ix-1 and ix+1
          if (ix.gt.1) then
            flx%p_damp(ix,is,igr) = (&
              log(dens_grid(ix,is)/dens_grid(ix+1,is)) - &
              log(dens_grid(ix-1,is)/dens_grid(ix+1,is))/2 &
              ) * &
              abs((fprim_perturb(idx,is)/fprim_perturb(ix,is))) * &
              flx%p_oscill(is) *  &
              flx%p_cc(ix,is,igr)

            flx%q_damp(ix,is,igr) = (&
              log(pres_grid(ix,is)/pres_grid(ix+1,is)) - &
              log(pres_grid(ix-1,is)/pres_grid(ix+1,is))/2 &
              ) * &
              abs((pprim_perturb(idx,is)/pprim_perturb(ix,is))) * &
              flx%q_oscill(is) * &
              flx%q_cc(ix,is,igr)

          end if

          

        end do
        if (ix.gt.1) then
          flx%l_damp(ix,igr) = (&
            log(mom_grid(ix)/mom_grid(ix+1)) - &
            log(mom_grid(ix-1)/mom_grid(ix+1))/2 &
            ) * &
            abs(gexb_perturb(idx)/gexb_perturb(ix)) * &
            flx%l_oscill * &
            0.0 ! Need to get matched lflux
        end if

      end do
    end do

    call debug_message(verb_high, &
      'fluxes_matched::get_fluxes_matched getting volume factors')

    if (fork_flag .and. all(grho_cc.ne.0.0)) then
      !TODO: fix this for gryfx, which doesn't use fork flag
      ! but does define grho and dvdrho
    ! Copy what ever is in grho_cc and area_cc to grho and dvdrho 
    ! in case we subsequently call gs2/gryfx
      !call barrier
      !write (*,*) 'njobs = ', njobs, 'size', size(grho_cc), 'size', size(flx%grho)
      !write (*,*) 'njobs = ', njobs, 'size a', size(area_cc), 'size', size(flx%dvdrho), 'focus', current_focus
      call all_to_group (grho_cc, flx%grho(1), njobs)
      call all_to_group (area_cc, flx%dvdrho(1), njobs)
      !call barrier
      flx%dvdrho = flx%dvdrho/flx%grho
    end if

    if (fork_flag) then
      call scope(subprocs)
    end if

    call debug_message(verb_high, &
      'fluxes_matched::get_fluxes_matched finished')
  end subroutine get_fluxes_matched
end module fluxes_matched
