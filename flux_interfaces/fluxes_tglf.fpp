!> A module for getting fluxes from the TGLF quaslinear model
module fluxes_tglf
  use flux_results, only: flux_results_type
  implicit none
  type fluxes_tglf_type
    logical :: first = .true.
  end type fluxes_tglf_type

contains
  subroutine get_fluxes_tglf(flx, tglf)
# ifdef USE_TGLF
    use tglf_interface
# endif
    use mp_trin, only: abort_mp, job, communicator, fork_gryfx, igpu, proc0
    use mp_trin, only: scope, gryfxprocs, allprocs, sum_allreduce_real_3array
    use mp_trin, only: sum_allreduce_real_2array
    use trinity_input, only: n_gpus
    use norms, only: rhostar, aref, amin_ref
    use trinity_input, only: m_ion, nspec, charge, njac, vtfac, ncc, fork_flag
    use trinity_input, only: is_ref, njobs, n_ion_spec
    use nteqns_arrays, only: bunit_ms, bmag_ms
    use nteqns_arrays, only: temp_ms, rmin_ms, rmajor_ms, kappa_ms, delta_ms
    use nteqns_arrays, only: drmindrho_ms, kappa_ms, kapprim_ms, deltprim_ms
    use nteqns_arrays, only: shift_ms, qval_ms, shat_ms, rad_ms, bunit_ms
    use nteqns_arrays, only: fprim_ms, tprim_ms, nu_ms, dens_ms, betaprim_ms
    use nteqns_arrays, only: gexb_ms, omega_ms, pres_ms, zeff_ms
    use nteqns_arrays, only: temp_perturb, dens_perturb
    use nteqns_arrays, only: rmin_cc, rmajor_cc, kappa_cc, kapprim_cc, delta_cc
    use nteqns_arrays, only: drmindrho_cc, deltprim_cc, shift_cc, qval_cc
    use nteqns_arrays, only: beta_ms, beta_perturb, temp_cc
    use nteqns_arrays, only: shat_cc, rad_cc, bunit_cc, fprim_perturb
    use nteqns_arrays, only: tprim_perturb, zeff_cc, bmag_cc, gexb_perturb
    use nteqns_arrays, only: dens_cc, pres_perturb, omega_perturb, nu_perturb
    use nteqns_arrays, only: pprim_perturb, pres_cc
    use nteqns_arrays, only: betaprim_perturb
    use constants_trin, only: mp_over_me, pi
    use flux_results, only: gather_forked_fluxes
    use nteqns, only: init_geo
    type(flux_results_type), intent(inout), target :: flx
    type(fluxes_tglf_type), intent(inout) :: tglf
    real, dimension(:,:), pointer :: pflx, qflx
    real, dimension(:,:,:), pointer :: pflx_cc, qflx_cc
    character (10) :: suffix
    real :: pflx_gbfac, qflx_gbfac, lflx_gbfac
    real :: bunit_over_bref
    real :: mref
    real :: tref, nref
    integer :: igr, ix, idx
    integer :: gpu_counter

# ifndef USE_TGLF
    write (*,*) "To use flux_option='tglf', you have to compile Trinity with the option FLUXCODE=tglf."
    call abort_mp
# endif
    ! Assign convenience pointers
    qflx => flx%q
    pflx => flx%p
    qflx_cc => flx%q_cc
    pflx_cc => flx%p_cc

    if (.not. fork_flag) then
      if (tglf%first) call fork_gryfx(n_gpus)
      call scope(gryfxprocs)
    end if

# ifdef USE_TGLF
    if (tglf%first) call tglf_init ('dum', communicator)

    ! if .true., dumps tglf input to file
    tglf_dump_flag_in = .true.

    ! several modifications to default TGLF values suggested by J. Candy Aug. 1, 2013
    tglf_use_bper_in = .true.
    tglf_use_bper_in = .false. ! hack to agree with JETTO
    tglf_sign_bt_in = -1.0
    tglf_sign_it_in = -1.0
    tglf_filter_in = 2.0

    tglf_vpar_model_in = 0 ! low mach-number limit (whatever that means)

    ! reference mass in tglf is deuterium mass
    mref = 2.0

    tglf_ns_in = nspec

    tglf_zs_in(:nspec) = charge
    ! species mass normalized to deuterium mass
    tglf_mass_in(1) = 5.44625e-4/mref
    tglf_mass_in(2:nspec) = m_ion/mref
# endif

    if (fork_flag) then

      write (suffix,'(i9)') job+1
      suffix = adjustl(suffix)

      ! reference temperature and density seem to be Te and ne in tglf
      tref = temp_ms(1,1)
      nref = dens_ms(1,1)

# ifdef USE_TGLF

      tglf_path_in = trim(suffix)//'.'

      ! 'loc' is miller
      tglf_rmin_loc_in = rmin_ms(1)
      tglf_rmaj_loc_in = rmajor_ms(1)*aref/amin_ref
      tglf_q_loc_in = qval_ms(1)
      tglf_q_prime_loc_in = shat_ms(1)*qval_ms(1)**2/(rmin_ms(1)*drmindrho_ms(1)*rad_ms(1))

      ! note that this currently assumes that bmag = bunit
      ! this is only true if using expro as geo_option at present
      tglf_p_prime_loc_in = betaprim_ms(1)*tglf_q_loc_in/(rmin_ms(1)*drmindrho_ms(1)*8.*pi)

      tglf_drmajdx_loc_in = shift_ms(1)*aref/(amin_ref*drmindrho_ms(1))
      tglf_kappa_loc_in = kappa_ms(1)
      tglf_s_kappa_loc_in = kapprim_ms(1)*rmin_ms(1)/(drmindrho_ms(1)*kappa_ms(1))
      ! TODO: Michael are you happy with this?
      !tglf_delta_loc_in = sin(delta_ms(1))
      tglf_delta_loc_in = delta_ms(1)
      tglf_s_delta_loc_in = deltprim_ms(1)*rmin_ms(1)/drmindrho_ms(1)

      ! convert from betai to betae
      tglf_betae_in = beta_ms(1)*(pres_ms(1,1)/pres_ms(1,is_ref))

      ! nu_ms(1,1) is nu_ee * a / vt_ref
      ! xnue is nu_ei * a / cs, with cs = sqrt(Te/mD)
      !          tglf_xnue_in = nu_ms(1,1)*zeff_ms(1)*sqrt(vtfac*mref*temp_ms(1,is_ref)/(m_ion(1)*tref))
      tglf_xnue_in = nu_ms(1,1)*sqrt(vtfac*mref*temp_ms(1,is_ref)/(m_ion(1)*tref))

      tglf_rlns_in(:nspec) = fprim_ms(1,:)/drmindrho_ms(1)
      tglf_rlts_in(:nspec) = tprim_ms(1,:)/drmindrho_ms(1)
      tglf_taus_in(:nspec) = temp_ms(1,:)/tref
      tglf_as_in(:nspec) = dens_ms(1,:)/nref
      ! based on definitions given at https://fusion.gat.com/theory/tglfinput
      tglf_vpar_in(:nspec) = -tglf_sign_it_in*tglf_rmaj_loc_in*omega_ms(1)*sqrt(vtfac*mref/(tref*m_ion(1)))
      tglf_vpar_shear_in(:nspec) = tglf_sign_it_in*tglf_rmaj_loc_in*sqrt(vtfac*mref/(tref*m_ion(1))) &
        * gexb_ms(1)*qval_ms(1)/(drmindrho_ms(1)*rad_ms(1))

      tglf_zeff_in = zeff_ms(1)
      tglf_debye_in = 0.0

      tglf_vexb_shear_in = tglf_vpar_shear_in(1)*rmin_ms(1)/(tglf_q_loc_in*tglf_rmaj_loc_in)

      ! 'sa' is s-alpha
      ! do not currently support s-alpha, but could be easily added
      !       tglf_rmin_sa_in = rmin_ms(1)
      !       tglf_rmaj_sa_in = rmajor_ms(1)
      !       tglf_q_sa_in = qval_ms(1)
      !       tglf_shat_sa_in = shat_ms(1)*rmin_ms(1)/(rad_ms(1)*drmindrho_ms(1))

      call tglf_run

      bunit_over_bref = bunit_ms(1)/bmag_ms(1)

      ! factor for converting from TGLF GB pflux normalization of n_e*c_s*(rho_unit/a)**2
      ! to Trinity GB pflux normalization of n_i*v_ti*(rho_ref/a)**2
      ! i.e., (n_e/n_i) * (c_s/v_ti) * (rho_unit/rho_ref)**2
      ! c_s = sqrt(T_e/m_D), rho_unit = c_s m_D c / e B_unit
      ! v_ti = sqrt(vtfac*T_i/m_i), rho_ref = v_ti m_i c / e B_ref
      pflx_gbfac = (dens_ms(1,1)/dens_ms(1,is_ref)) * bunit_over_bref**2 &
        * (temp_ms(1,1)/temp_ms(1,is_ref))**1.5*sqrt(mref/m_ion(1))/vtfac**1.5
      ! TGLF GB qflux norm is n_e*T_e*c_s*(rho_unit/a)**2
      ! Trinity GB qflux norm is n_i*T_i*v_ti*(rho_ref/a)**2
      qflx_gbfac = pflx_gbfac*(temp_ms(1,1)/temp_ms(1,is_ref))
      ! TGLF GB lflux norm is n_e*T_e*a*(rho_unit/a)**2
      ! Trinity GB lflux norm is vtfac*n_i*T_i*a*(rho_ref/a)**2
      lflx_gbfac = pflx_gbfac*sqrt(m_ion(1)*temp_ms(1,1)/(mref*temp_ms(1,is_ref)*vtfac))

      flx%p(1,1) = tglf_elec_pflux_out*pflx_gbfac
      flx%q(1,1) = tglf_elec_eflux_out*qflx_gbfac
      flx%p(1,2:) =tglf_ion_pflux_out(1:n_ion_spec)*pflx_gbfac
      flx%q(1,2:) = tglf_ion_eflux_out(1:n_ion_spec)*qflx_gbfac
      flx%l(1) = (sum(tglf_ion_mflux_out(1:nspec)) + tglf_elec_mflux_out)*lflx_gbfac

      flx%heat(1,:) = 0.0
# endif

      call gather_forked_fluxes(flx)
    else
      flx%l_cc = 0.0
      flx%p_cc = 0.0
      flx%q_cc = 0.0
      flx%heat_cc = 0.0
      gpu_counter = -1
      do igr = 1, njac
        do ix = 1, ncc

          ! gpu_counter determines which gpu the job will run on.
          ! There need be no relationship between the number of
          ! jobs (i.e. the number of cell centres and evolved
          ! profiles) and the number of gpus.
          gpu_counter = gpu_counter + 1
          if (.not. (mod(gpu_counter, n_gpus).eq. igpu)) cycle

          idx=ix+(igr-1)*ncc

          write (suffix,'(i9)') idx
          suffix = adjustl(suffix)

          ! reference temperature and density seems to be Te and ne in tglf
          tref = temp_perturb(idx,1)
          nref = dens_perturb(idx,1)

# ifdef USE_TGLF

          tglf_path_in = trim(suffix)//'.'

          ! 'loc' is miller
          tglf_rmin_loc_in = rmin_cc(ix)
          tglf_rmaj_loc_in = rmajor_cc(ix)*aref/amin_ref
          tglf_q_loc_in = qval_cc(ix)

          !tglf_q_prime_loc_in = shat_cc(ix)*qval_cc(ix)**2 &
          !  / (rmin_cc(ix)*drmindrho_cc(ix)*rad_cc(ix))

          ! Rewrite to make it obvious what is happening: this expression
          ! is identical to the one commented out above.
          tglf_q_prime_loc_in = qval_cc(ix)**2/rmin_cc(ix)**2.0 & ! This is q^2 a^2 / r^2 from the TGLF website
            * rmin_cc(ix)*amin_ref / rad_cc(ix) & ! Convert from rho/q dq/drho to r/q dq/dr
            * shat_cc(ix) / (drmindrho_cc(ix)*amin_ref) ! Note that drmindrho is d(r/a)/drho

          ! note that this currently assumes that bmag = bunit
          ! this is only true if using expro as geo_option at present
          ! p_prim_loc = 8 * pi * (dp_tot/dr) / B^2 * (q/r) * a^2
          !tglf_p_prime_loc_in = betaprim_perturb(idx)*tglf_q_loc_in&
          !  /(rmin_cc(ix)*drmindrho_cc(ix)*8.*pi)
            !*bmag_cc(ix)**2.0/bunit_cc(ix)**2.0 * 

          ! Generalize the expression to include all geometries
          ! and flux label options.
          ! From the website p_prim_loc = q a^2 / (r * Bunit^2) * dp/dr
          ! But we must remember they are using cgs units so that
          ! Bunit^2 is equal to the magnetic pressure multiplied by 8 pi,
          ! which for us is equal to Bunit^2/(2mu0) * 8 pi
          tglf_p_prime_loc_in = - tglf_q_loc_in * amin_ref**2.0 & ! This is q a^2
            / (rmin_cc(ix)*amin_ref)  & ! Divide by r = rmin_cc * amin_ref
            /(&
              bunit_cc(ix)**2.0 / (8.* pi * 1e-7) & ! Divide by magnetic pressure
             * (8.0 * pi) & ! for some reason TGLF takes the factor of 8pi out.
             ) &
             * & ! The line below is the total pressure gradient dp / dr in SI units
            sum(pprim_perturb(idx, :)*pres_cc(ix,:)*1e3*1.6e-19*1e20/drmindrho_cc(ix)/amin_ref) 

          tglf_drmajdx_loc_in = shift_cc(ix)*aref/(amin_ref*drmindrho_cc(ix))
          tglf_kappa_loc_in = kappa_cc(ix)
          tglf_s_kappa_loc_in = kapprim_cc(ix)*rmin_cc(ix)/(drmindrho_cc(ix)*kappa_cc(ix))
          ! TODO: Michael are you happy with this?
          !tglf_delta_loc_in = sin(delta_cc(ix))
          tglf_delta_loc_in = delta_cc(ix)
          tglf_s_delta_loc_in = deltprim_cc(ix)*rmin_cc(ix)/drmindrho_cc(ix)

          ! convert from betai to betae
          !tglf_betae_in = beta_perturb(idx)*(pres_perturb(idx,1)/pres_perturb(idx,is_ref))

          ! Rewrite this explicitly in terms of Bunit
          tglf_betae_in = pres_perturb(idx,1)*1e3*1.6e-19*1e20 & ! Electron pressure in SI units
            /(bunit_cc(ix)**2.0/(8.*pi*1.0e-7)) ! Magnetic pressure in SI units

          ! nu_perburb(idx,1) is nu_ee * aref / vt_ref
          ! xnue_in is nu_ei * amin_ref / cs, with cs = sqrt(Te/mD)
          !                tglf_xnue_in = nu_perturb(idx,1)*zeff_cc(ix)*sqrt(vtfac*mref*temp_perturb(idx,is_ref) &
          tglf_xnue_in = nu_perturb(idx,1)*zeff_cc(ix)*sqrt(vtfac*mref*temp_perturb(idx,is_ref) &
            /(m_ion(1)*tref)) * amin_ref/aref

          tglf_rlns_in(:nspec) = fprim_perturb(idx,:)/drmindrho_cc(ix)
          tglf_rlts_in(:nspec) = tprim_perturb(idx,:)/drmindrho_cc(ix)
          tglf_taus_in(:nspec) = temp_perturb(idx,:)/tref
          tglf_as_in(:nspec) = dens_perturb(idx,:)/nref
          ! based on definitions given at https://fusion.gat.com/theory/tglfinput
          !tglf_vpar_in(:nspec) = -tglf_sign_it_in*tglf_rmaj_loc_in*omega_perturb(idx)*sqrt(vtfac*mref/(tref*m_ion(1)))

          ! EGH: This is equal to omega * R / c_s = omega * rmajor_grid*aref sqrt(tref/mref)
          tglf_vpar_in(:nspec) = tglf_sign_it_in*rmajor_cc(ix)*aref*omega_perturb(idx)/sqrt(tref*1e3*1.6e-19/mref/1.67e-27)

          !tglf_vpar_shear_in(:nspec) = tglf_sign_it_in*tglf_rmaj_loc_in*sqrt(vtfac*mref/(tref*m_ion(1))) &
            !* gexb_perturb(idx)*qval_cc(ix)/(drmindrho_cc(ix)*rad_cc(ix))

          ! EGH: tglf_vpar_shear_in = -sign(Itor) * R * d (Vt/R) / dr * a / c_s
          ! = rmajor * aref * d omega / d rmin * amin_ref / c_s
          ! gexb_perturb = rho / q * d omega / d rho  
          ! domega / d rmin = d omega / d rho / (d rmin / d rho) = q / rho *
          ! gexb_perturb / drmindrho_cc
          ! so tglf_vpar_shear_in = rmajor * aref * q / rho * gexb_perturb /
          ! drmindrho_cc * amin_ref / sqrt(tref/mref)
          tglf_vpar_shear_in(:nspec) = -tglf_sign_it_in* rmajor_cc(ix) * aref * qval_cc(ix) / &
            rad_cc(ix) * gexb_perturb(idx) / (drmindrho_cc(ix) * amin_ref) * amin_ref / sqrt(tref*1e3*1.6e-19/mref/1.67e-27)
          tglf_vpar_shear_model_in = 0
          
          !tglf_rmaj_loc_in*sqrt(vtfac*mref/(tref*m_ion(1))) &
            !* gexb_perturb(idx)*qval_cc(ix)/(drmindrho_cc(ix)*rad_cc(ix))

          tglf_zeff_in = zeff_cc(ix)
          tglf_debye_in = 0.0

          tglf_vexb_shear_in = tglf_vpar_shear_in(1)*rmin_cc(ix)/(tglf_q_loc_in*tglf_rmaj_loc_in)

          ! 'sa' is s-alpha
          ! do not currently support s-alpha, but could be easily added
          !       tglf_rmin_sa_in = rmin_ms(1)
          !       tglf_rmaj_sa_in = rmajor_ms(1)
          !       tglf_q_sa_in = qval_ms(1)
          !       tglf_shat_sa_in = shat_ms(1)*rmin_ms(1)/(rad_ms(1)*drmindrho_ms(1))

          call tglf_run

          if (proc0) write (*,'(a18,i3,a7,i3)') 'finished tglf run', idx, 'out of', njobs

          bunit_over_bref = bunit_cc(ix)/bmag_cc(ix)

          ! factor for converting from TGLF GB pflux normalization of n_e*c_s*(rho_unit/amin_ref)**2
          ! to Trinity GB pflux normalization of n_i*v_ti*(rho_ref/aref)**2
          ! i.e., (n_e/n_i) * (c_s/v_ti) * (rho_unit/rho_ref)**2 *
          ! (aref/amin_ref)**2
          ! c_s = sqrt(T_e/m_D), rho_unit = c_s m_D c / e B_unit
          ! v_ti = sqrt(vtfac*T_i/m_i), rho_ref = v_ti m_i c / e B_ref
          pflx_gbfac = (dens_cc(ix,1)/dens_cc(ix,is_ref)) / bunit_over_bref**2 &
            * (temp_cc(ix,1)/temp_cc(ix,is_ref))**1.5*sqrt(mref/m_ion(1))/vtfac**1.5 &
            * (aref/amin_ref)**2.0
          ! TGLF GB qflux norm is n_e*T_e*c_s*(rho_unit/a)**2
          ! Trinity GB qflux norm is n_i*T_i*v_ti*(rho_ref/a)**2
          qflx_gbfac = pflx_gbfac*(temp_cc(ix,1)/temp_cc(ix,is_ref))
          ! TGLF GB lflux norm is n_e*T_e*a*(rho_unit/a)**2
          ! Trinity GB lflux norm is vtfac*n_i*T_i*a*(rho_ref/a)**2
          lflx_gbfac = pflx_gbfac*sqrt(m_ion(1)*temp_cc(ix,1)/(mref*temp_cc(ix,is_ref)*vtfac))

          flx%p_cc(ix,1,igr) = tglf_elec_pflux_out*pflx_gbfac
          flx%q_cc(ix,1,igr) = tglf_elec_eflux_out*qflx_gbfac
          flx%p_cc(ix,2:,igr) =tglf_ion_pflux_out(1:n_ion_spec)*pflx_gbfac
          flx%q_cc(ix,2:,igr) = tglf_ion_eflux_out(1:n_ion_spec)*qflx_gbfac

          write (*,*) 'tglf_ion_eflux_out', ix, tglf_ion_eflux_out(1)

          flx%l_cc(ix,igr) = (sum(tglf_ion_mflux_out(1:nspec)) + tglf_elec_mflux_out)*lflx_gbfac

# endif
        end do
      end do
      flx%heat_cc = 0.0
      call scope(allprocs)
      if (proc0) write(*,*) 'qflx_cc before', flx%q_cc
      call  sum_allreduce_real_3array(flx%p_cc)
      call  sum_allreduce_real_3array(flx%q_cc)
      call  sum_allreduce_real_2array(flx%l_cc)
      call  sum_allreduce_real_3array(flx%heat_cc)
      if (proc0) write(*,*) 'qflx_cc after', flx%q_cc

      if (tglf%first) call init_geo
    end if

    tglf%first = .false.
  end subroutine get_fluxes_tglf


  end module fluxes_tglf
