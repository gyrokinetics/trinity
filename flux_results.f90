module flux_results
  implicit none
  !> This type contains all the arrays needed to store and transmit
  !! results from the flux codes.
  type flux_results_type
    logical :: initialized = .false.
    logical :: gather_first = .true.
    logical :: geo_first = .true.

    !> If this is true 
    !! the iteration was restarted just after
    !! the results in this object were calculated,
    !! i.e. these results are 'tainted'
    !! This will cause certain flux models, e.g. 
    !! gryfx, to reset, i.e. start from noise
    logical :: error_flag = .false.

    !> Records which flux code was used to obtain these results
    integer :: flux_option_actual = -1

    !> Then number of calls to get_fluxes for this iteration,
    !! i.e. the number of calls needed to converge the fluxes
    integer :: conv_count = 0

    !> These are the quantities returned by codes that use fork_flag
    !! p is particle flux, q is heat flux, l is momentum flux and
    !! heat is turbulent heating.
    !! 
    !! The dimension of p, q, and heat are nglobalrad, nspec,
    !! where nglobalrad is 1 for local codes and > 1 for global codes
    !! like GENE.
    real, dimension(:,:), allocatable :: p, q, heat

    !> l is the momentum flux returned by a flux code and has the
    !! dimension nglobalrad
    real, dimension(:), allocatable :: l

    !> Cell centred results from the flux codes; modified directly by
    !! non-fork_flag codes, and set by gather_forked_fluxes otherwise.
    !! Dimensions are (ncc, nrad, nspec)
    real, dimension(:,:,:), allocatable :: p_cc, q_cc, heat_cc

    !> Used for measuring convergence
    real, dimension(:,:,:), allocatable :: q_relative_change
    logical, dimension(:,:,:), allocatable :: q_converged

    !> A separate store of the turbulent fluxes 
    !! needed for the convergence check
    real, dimension(:,:,:), allocatable :: q_turb_cc

    !> Cell centred momentum flux, dimensions (ncc,nrad)
    real, dimension(:,:), allocatable :: l_cc

    !> Neoclassical fluxes returned by a code which uses
    !! fork flag, like NEO. Dimensions are (nglobalrad,nspec).
    real, dimension(:,:), allocatable :: p_neo, q_neo

    !> Momentum flux for fork_flag codes
    real, dimension(:), allocatable :: l_neo

    !> Neoclassical cell-centred heat and particle fluxes, modified directly
    !! by a non-fork_flag neoclassical flux model, or set by
    !! gather_forked_fluxes  otherwise
    real, dimension(:,:,:), allocatable :: p_neo_cc, q_neo_cc

    !> Neoclassical cell-centred momentum flux
    real, dimension(:,:), allocatable :: l_neo_cc

    !> Cell centred matched results, being the fluxes that will keep
    !! the local logarithmic gradients pinned to their original values. 
    real, dimension(:,:,:), allocatable :: p_matched, q_matched, heat_matched

    !> Cell-centred matched momentum flux, being the flux that will keep
    !! the local flow gradient pinned to its original value. 
    real, dimension(:,:), allocatable :: l_matched

    !> Cell centred diffusion terms, that will damp severe oscillations in
    !! the profiles that the centered finite differencing doesn't catch 
    real, dimension(:,:,:), allocatable :: p_damp, q_damp, heat_damp

    !> Cell-centred diffusive momentum flux, that will damp severe oscillations in
    !! the profiles that the centered finite differencing doesn't catch  
    real, dimension(:,:), allocatable :: l_damp

    !> Factor measuring the magnitude of gridscale oscillations  needing to be
    ! damped
    real, dimension(:,:), allocatable :: p_oscill_measure, q_oscill_measure
    real, dimension(:), allocatable :: p_oscill, q_oscill
    real, dimension(:), allocatable :: l_oscill_measure
    real :: l_oscill

    !> Turbulent heating at cell boundaries
    real, dimension(:,:), allocatable :: heat_grid

    !> Heat flux in physical units (MW m^-2)  and particle flux in physical
    !! units (10^20 m^-2) at cell centres 
    real, dimension (:,:), allocatable :: q_phys, p_phys

    real, dimension (:), allocatable :: grho, dvdrho
      
    logical, dimension (:), allocatable :: con_heat, converged

    
  end type flux_results_type
contains
  subroutine init_flux_results(flx, &
      nspec, ncc, njac, nrad, nrad_global, fork_flag, check_flux_converge)
    use mp_trin, only: scope, subprocs
    implicit none
    type(flux_results_type), intent(inout) :: flx
    integer, intent(in) :: nspec, ncc, njac, nrad, nrad_global
    logical, intent(in) :: fork_flag, check_flux_converge
    ! allocate arrays for radial flux profiles (at cell centers)
    allocate (flx%p_cc(ncc,nspec,njac)) ; flx%p_cc = 0.0
    allocate (flx%q_cc(ncc,nspec,njac)) ; flx%q_cc = 0.0
    allocate (flx%heat_cc(ncc,nspec,njac)) ; flx%heat_cc = 0.0
    allocate (flx%l_cc(ncc,njac)) ; flx%l_cc = 0.0
    allocate (flx%q_neo_cc(ncc,nspec,njac)) ; flx%q_neo_cc = 0.0
    allocate (flx%p_neo_cc(ncc,nspec,njac)) ; flx%p_neo_cc = 0.0
    allocate (flx%l_neo_cc(ncc,njac)) ; flx%l_neo_cc = 0.0
    allocate (flx%p_matched(ncc,nspec,njac)) ; flx%p_matched = 0.0
    allocate (flx%q_matched(ncc,nspec,njac)) ; flx%q_matched = 0.0
    allocate (flx%l_matched(ncc,njac)) ; flx%l_matched = 0.0
    allocate (flx%p_damp(ncc,nspec,njac)) ; flx%p_damp = 0.0
    allocate (flx%q_damp(ncc,nspec,njac)) ; flx%q_damp = 0.0
    allocate (flx%l_damp(ncc,njac)) ; flx%l_damp = 0.0
    allocate (flx%p_oscill(nspec)) ; flx%p_oscill = 0.0
    allocate (flx%q_oscill(nspec)) ; flx%q_oscill = 0.0
    allocate (flx%p_oscill_measure(ncc,nspec)) ; flx%p_oscill_measure = 0.0
    allocate (flx%q_oscill_measure(ncc,nspec)) ; flx%q_oscill_measure = 0.0
    allocate (flx%l_oscill_measure(ncc)) ; flx%l_oscill_measure = 0.0

    allocate (flx%q_relative_change(ncc,nspec,njac)) ; flx%q_relative_change = 0.0
    allocate (flx%q_converged(ncc,nspec,njac)) ; flx%q_converged = .false.
    allocate (flx%q_turb_cc(ncc,nspec,njac)) ; flx%q_turb_cc = 0.0

    ! allocate arrays for radial heating profile (at grid points)
    allocate (flx%heat_grid(nrad,nspec)) ; flx%heat_grid = 0.0

    ! allocate arrays for radial fluxes in physical units
    ! (MW/m^2 for heat fluxes), (10^20/m^2/s for particle fluxes)
    allocate (flx%q_phys(ncc,nspec)) ; flx%q_phys = 0.0
    allocate (flx%p_phys(ncc,nspec)) ; flx%p_phys = 0.0

    if (fork_flag) then
      call scope(subprocs)
      allocate (flx%p(nrad_global,nspec), flx%q(nrad_global,nspec), flx%heat(nrad_global,nspec), flx%l(nrad_global))
      flx%p = 0.0 ; flx%q = 0.0 ; flx%heat = 0.0 ; flx%l = 0.0

      allocate (flx%p_neo(nrad_global,nspec), flx%q_neo(nrad_global,nspec), flx%l_neo(nrad_global))
      flx%p_neo = 0.0 ; flx%q_neo = 0.0 ; flx%l_neo = 0.0

       if (check_flux_converge) then
          allocate (flx%con_heat(nspec)) ; flx%con_heat = .false.
          allocate (flx%converged(2*nspec+1)) ; flx%converged = .false.
       end if
    end if

    allocate(flx%grho(nrad_global), flx%dvdrho(nrad_global))

    flx%initialized = .true.
  end subroutine init_flux_results

  subroutine finish_flux_results(flx, fork_flag, check_flux_converge)
    use mp_trin, only: scope, subprocs
    implicit none
    type(flux_results_type), intent(inout) :: flx
    logical, intent(in) :: fork_flag, check_flux_converge
    ! deallocate arrays for radial flux profiles (at cell centers)
    deallocate (flx%p_cc) 
    deallocate (flx%q_cc) 
    deallocate (flx%heat_cc) 
    deallocate (flx%l_cc) 
    deallocate (flx%q_relative_change) 
    deallocate (flx%q_converged) 
    deallocate (flx%q_turb_cc) 
    deallocate (flx%q_neo_cc) 
    deallocate (flx%p_neo_cc) 
    deallocate (flx%l_neo_cc) 
    deallocate (flx%p_matched) 
    deallocate (flx%q_matched) 
    deallocate (flx%l_matched) 
    deallocate (flx%p_damp) 
    deallocate (flx%q_damp) 
    deallocate (flx%l_damp) 
    deallocate (flx%p_oscill) 
    deallocate (flx%q_oscill) 
    deallocate (flx%p_oscill_measure) 
    deallocate (flx%q_oscill_measure) 
    deallocate (flx%l_oscill_measure) 

    ! deallocate arrays for radial heating profile (at grid points)
    deallocate (flx%heat_grid) 

    deallocate (flx%q_phys) 
    deallocate (flx%p_phys) 

    if (fork_flag) then
      call scope(subprocs)
      deallocate (flx%p, flx%q, flx%heat, flx%l)
      if (fork_flag) then
         if (check_flux_converge) deallocate (flx%con_heat, flx%converged)
      end if
      deallocate (flx%p_neo, flx%q_neo, flx%l_neo)
    end if

    deallocate(flx%dvdrho, flx%grho)

    flx%conv_count = 0
    flx%initialized = .false.
    flx%gather_first = .true.
    flx%geo_first = .true.
end subroutine finish_flux_results

  subroutine gather_forked_fluxes (flx, dvdrho, grho)

    use mp_trin, only: scope, allprocs, group_to_all
    use trinity_input, only: check_flux_converge, include_neo, turb_heat
    use trinity_input, only: nspec, ncc, nrad_global, njac, njobs
    use trinity_input, only: single_radius, flux_driver
    use nteqns_arrays, only: nu_ms, qval_ms, tprim_ms, rad_ms
    use nteqns_arrays, only: shift_ms, gexb_ms
    use nteqns, only: init_geo

    implicit none

    type(flux_results_type), intent(inout) :: flx
    real, dimension(nrad_global), intent (in), optional :: dvdrho, grho

    real, dimension (:,:), allocatable :: pflxtmp_rad, qflxtmp_rad, heattmp_rad
    real, dimension (:), allocatable :: lflxtmp_rad
    
    !logical, save :: first = .true.
    integer :: is, igr, ix

    ! if fluxes did not converge to steady state, set to
    ! user-specified minimum values
    if (check_flux_converge) call check_converge(flx)
    
    ! do not necessarily want to include turbulent heating
    if (.not. turb_heat) flx%heat = 0.0
    
    ! change mp scope to all processors
    call scope (allprocs)

    allocate (pflxtmp_rad(ncc*njac,nspec), qflxtmp_rad(ncc*njac,nspec))
    allocate (heattmp_rad(ncc*njac,nspec), lflxtmp_rad(ncc*njac))

    pflxtmp_rad = 0.0 ; qflxtmp_rad = 0.0 ; heattmp_rad = 0.0 ; lflxtmp_rad = 0.0

    ! send turbulent fluxes from group proc0s to global proc0
    do is = 1, nspec
       call group_to_all (flx%p(:,is), pflxtmp_rad(:,is), njobs)
       call group_to_all (flx%q(:,is), qflxtmp_rad(:,is), njobs)
       call group_to_all (flx%heat(:,is), heattmp_rad(:,is), njobs)
    end do
    call group_to_all (flx%l, lflxtmp_rad, njobs)

    do igr = 1, njac
      if (single_radius > 0 .and. .not. flux_driver) then
       ! In this case we only copy the single calculated flux
       ! (the rest have been preset to the matched fluxes)
       flx%p_cc(single_radius,:,igr) = pflxtmp_rad(single_radius+(igr-1)*ncc,:)
       flx%q_cc(single_radius,:,igr) = qflxtmp_rad(single_radius+(igr-1)*ncc,:)
       flx%heat_cc(single_radius,:,igr) = heattmp_rad(single_radius+(igr-1)*ncc,:)
       flx%l_cc(single_radius,igr) = lflxtmp_rad(single_radius+(igr-1)*ncc)
      else
       flx%p_cc(:,:,igr) = pflxtmp_rad(1+(igr-1)*ncc:igr*ncc,:)
       flx%q_cc(:,:,igr) = qflxtmp_rad(1+(igr-1)*ncc:igr*ncc,:)
       flx%heat_cc(:,:,igr) = heattmp_rad(1+(igr-1)*ncc:igr*ncc,:)
       flx%l_cc(:,igr) = lflxtmp_rad(1+(igr-1)*ncc:igr*ncc)
     end if
    end do

    ! send neoclassical fluxes from group proc0s to global proc0
    do is = 1, nspec
       call group_to_all (flx%p_neo(:,is), pflxtmp_rad(:,is), njobs)
       call group_to_all (flx%q_neo(:,is), qflxtmp_rad(:,is), njobs)
    end do
    call group_to_all (flx%l_neo, lflxtmp_rad, njobs)

    do igr = 1, njac
       flx%p_neo_cc(:,:,igr) = pflxtmp_rad(1+(igr-1)*ncc:igr*ncc,:)
       flx%q_neo_cc(:,:,igr) = qflxtmp_rad(1+(igr-1)*ncc:igr*ncc,:)
       flx%l_neo_cc(:,igr) = lflxtmp_rad(1+(igr-1)*ncc:igr*ncc)
    end do

    ! initialize geometric factors.  cannot do this earlier because need
    ! to be in allprocs and have factors passed from turbulence code
    if (flx%gather_first) then
       if (present(dvdrho)) then
          call init_geo (dvdrho, grho)
       else
          call init_geo
       end if
       flx%gather_first = .false.
    end if

    deallocate (pflxtmp_rad, qflxtmp_rad, lflxtmp_rad, heattmp_rad)
    
  end subroutine gather_forked_fluxes

  !> This subroutine is old and has nothing to do with the current method
  !! for checking flux convergence.
  subroutine check_converge(flx)

    use trinity_input, only: nspec, qflx_min, pflx_min, heat_min, lflx_min, nrad_global

    implicit none
    type(flux_results_type), intent(inout) :: flx
    
    integer :: ix

    do ix = 1, nrad_global
       where (.not. flx%converged(:nspec))
          flx%q(ix,:) = qflx_min
       end where
       where (.not. flx%converged(nspec+1:2*nspec))
          flx%p(ix,:) = pflx_min
       end where
       where (.not. flx%con_heat)
          flx%heat(ix,:) = heat_min
       end where
       if (.not. flx%converged(2*nspec+1)) flx%l(ix)=lflx_min
    end do

  end subroutine check_converge
end module flux_results
