#include "define.inc"

!> This module provides a platform independent
!! function for issuing system (shell) commands.
module syscall_pli

contains
  function syscall(command)
#if FCOMPILER == _INTEL_
    use ifport, only: system
#endif
    implicit none
    character(*), intent(in) :: command
    integer :: syscall

    syscall = system(command)

  end function syscall 
end module syscall_pli
