module miller_geometry

  implicit none

  public :: init_miller_geo, finish_miller_geo
  public :: get_gradrho, get_Iflxfnc
  public :: get_grho_av
  !  public :: get_surfarea, get_gradrho, get_Iflxfnc

  real, dimension (:), allocatable:: rhoc
  real, dimension (:), allocatable:: rmaj, shift
  real, dimension (:), allocatable:: kappa, kapprim
  real, dimension (:), allocatable:: tri, triprim

  integer :: nrad
  integer :: ntg = 512
  real :: drho = 0.0001
  
  real, dimension (:), allocatable :: theta, delthet
  real, dimension (:,:,:), allocatable :: Rr, Zr

  real, dimension (:,:), allocatable :: dRdrho, dZdrho
  real, dimension (:,:), allocatable :: dRdth, dZdth
  real, dimension (:,:), allocatable :: jacrho

contains
  
  subroutine init_miller_geo (rhoc_in, rmaj_in, shift_in, kappa_in, kapprim_in, tri_in, triprim_in)  

    use constants_trin, only: pi
    use mp_trin,only: proc0
    
    implicit none
    
    real, dimension (:), intent (in) :: rhoc_in              
    real, dimension (:), intent (in) :: rmaj_in, shift_in    
    real, dimension (:), intent (in) :: kappa_in, kapprim_in 
    real, dimension (:), intent (in) :: tri_in, triprim_in   

    integer :: i, j, k
    real :: rho


    nrad = size(rhoc_in)
    
    if(.not.allocated(Rr)) call alloc_arrays

    rhoc=rhoc_in
    rmaj = rmaj_in
    shift = shift_in
    kappa = kappa_in
    kapprim = kapprim_in
    tri = tri_in
    triprim = triprim_in

    ! calculate R and Z at 3 closely space points
    ! for each Trinity radius
    do k = 1, nrad
       do j=-ntg,ntg
          theta(j) = j*pi/real(ntg)
          do i=1,3
             rho = rhoc(k) + (i-2)*drho
             Rr(i,j,k) = Rpos(rho,theta(j),j,k)
             Zr(i,j,k) = Zpos(rho,theta(j),j,k)
          end do
       end do
    end do
       
    ! get delta theta as a function of theta
    delthet = theta(-ntg+1:)-theta(:ntg-1)

    ! get dR/drho and dZ/drho
    do k = 1, nrad
       call get_drho (Rr(:,:,k), dRdrho(:,k))
       call get_drho (Zr(:,:,k), dZdrho(:,k))

       ! get dR/dtheta and dZ/dtheta
       call get_dthet(Rr(2,:,k), dRdth(:,k))
       call get_dthet(Zr(2,:,k), dZdth(:,k))
    end do

    !if (proc0) write (*,*) 'Rr', Rr
    !if (proc0) write (*,*) 'dRdth', dRdth
    !if (proc0) write (*,*) 'dRdrho', dRdrho
    !if (proc0) write (*,*) 'dZdth', dZdth
    !if (proc0) write (*,*) 'dZdrho', dZdrho

    ! get the Jacobian of the transformation from (rho,theta,zeta) to (R,Z,zeta)
    ! this is what I call jacr or jacrho in following comments
    ! as opposed to jacobian, which is for tranformation from (psi,theta,zeta) to (R,Z,zeta)
    call get_jacrho

    !if (proc0) write (*,*) 'jacrho', jacrho

  end subroutine init_miller_geo

  subroutine alloc_arrays
    
    allocate (theta(-ntg:ntg))
    allocate (delthet(-ntg:ntg-1))
    allocate (Rr(3,-ntg:ntg,nrad), Zr(3,-ntg:ntg,nrad))
    allocate (dRdrho(-ntg:ntg,nrad), dZdrho(-ntg:ntg,nrad))
    allocate (dRdth(-ntg:ntg,nrad), dZdth(-ntg:ntg,nrad))
    allocate (jacrho(-ntg:ntg,nrad))
    allocate (rhoc(nrad))
    allocate (rmaj(nrad), shift(nrad))
    allocate (kappa(nrad), kapprim(nrad))
    allocate (tri(nrad), triprim(nrad))

  end subroutine alloc_arrays

  subroutine get_jacrho

    implicit none

    !write (*,*) 'Rr', Rr(2,1,nrad), '---', dRdrho(1,nrad), &
      !dRdth(1,nrad), dZdrho(1,nrad), dZdth(1,nrad)

    ! jacrho = R*(dR/drho * dZ/dtheta - dR/dtheta * dZ/drho)
    jacrho = Rr(2,:,:)*(dRdrho*dZdth - dRdth*dZdrho)

  end subroutine get_jacrho

  subroutine get_Iflxfnc (rad_trin, rho_tor_exp, rmin_exp, torflx_lcfs, Iflxfnc, bunit)

    use trinity_input, only: fluxlabel_option_switch
    use trinity_input, only: fluxlabel_option_aminor, fluxlabel_option_torflux
    use interp, only: fpintrp, fd3pt
    use constants_trin, only: pi
    
    implicit none

    real, dimension (:), intent (in) :: rad_trin, rho_tor_exp, rmin_exp
    real, intent (in) :: torflx_lcfs
    real, dimension (:), intent (out) :: Iflxfnc
    real, dimension (:), intent (out), optional :: bunit
    !real, intent (out) :: bmag_axis

    integer :: k, nrad, nrad_exp
    real :: jacrho_integral
    real, dimension (:), allocatable :: drmin_exp, bunit_exp
    real, dimension (:), allocatable :: drhotor2_drmin, drhotor2_drmin_exp

    nrad= size(rad_trin)
    nrad_exp = size(rho_tor_exp)

    allocate (drmin_exp(nrad_exp-1))
    allocate (drhotor2_drmin_exp(nrad_exp))
    allocate (drhotor2_drmin(nrad))
    allocate (bunit_exp(nrad_exp))
    
    ! calculate the grid spacings of the rmin grid
    ! from experimental data
    drmin_exp = rmin_exp(2:)-rmin_exp(:nrad_exp-1)

    ! get d (rho_tor**2) / dr
    call fd3pt (rho_tor_exp**2, drhotor2_drmin_exp, drmin_exp)
    bunit_exp = torflx_lcfs*drhotor2_drmin_exp/(2.*pi*rmin_exp)
    
    ! interpolate d (rho_tor**2) / dr from experimental data grid
    ! to grid used in Trinity
    select case (fluxlabel_option_switch)
    case (fluxlabel_option_aminor)
       do k = 1, nrad
          drhotor2_drmin(k) = fpintrp (rad_trin(k),drhotor2_drmin_exp,rmin_exp/rmin_exp(nrad_exp),nrad_exp)
       end do
       if (present(bunit)) then
          do k = 1, nrad
             bunit(k) = fpintrp (rad_trin(k),bunit_exp,rmin_exp/rmin_exp(nrad_exp),nrad_exp)
          end do
       end if
    case (fluxlabel_option_torflux)
       do k = 1, nrad
          drhotor2_drmin(k) = fpintrp (rad_trin(k),drhotor2_drmin_exp,rho_tor_exp,nrad_exp)
       end do
       if (present(bunit)) then
          bunit(k) = fpintrp (rad_trin(k),bunit_exp,rho_tor_exp,nrad_exp)
       end if
    end select

    do k = 1, nrad
       ! theta_integrate returns integral from 0 -> 2*pi
       ! jacrho_integral is needed when calculating the flux function I(rho)
       call theta_integrate (jacrho(:,k)/Rr(2,:,k)**2, jacrho_integral)
       
       Iflxfnc(k) = torflx_lcfs*drhotor2_drmin(k)/(jacrho_integral)
    end do

    !> This is an approximate formula valid as long as rmin_exp(1) is sufficiently
    !! small.
    !bmag_axis = torflx_lcfs*drhotor2_drmin_exp(2) / (2.0 * pi * rmin_exp(2))
    !bmag_axis = torflx_lcfs*rho_tor_exp(1)**2 / (pi * rmin_exp(1)**2)

    deallocate (drmin_exp)
    deallocate (drhotor2_drmin_exp)
    deallocate (drhotor2_drmin)
    deallocate (bunit_exp)

  end subroutine get_Iflxfnc

  ! takes in f(r), with r given at three radial locations
  ! and returns df = df/dr at the middle radius
  subroutine get_drho (f, df)

    implicit none

    real, dimension (:,-ntg:), intent (in) :: f
    real, dimension (-ntg:), intent (out) :: df

    df = 0.5*(f(3,:)-f(1,:))/drho

  end subroutine get_drho

  ! given function f(theta:-pi->pi), calculate theta derivative
  ! second order accurate, with equal grid spacing assumed
  ! assumes periodic in theta -- may need to change this in future
  subroutine get_dthet (f, df)

    implicit none

    real, dimension (-ntg:), intent (in) :: f
    real, dimension (-ntg:), intent (out) :: df

    ! assuming equal grid spacing in theta here
    df(-ntg+1:ntg-1) = (f(-ntg+2:)-f(:ntg-2))/(delthet(:ntg-2)+delthet(-ntg+1:))

    ! use periodicity at boundary
    df(-ntg) = (f(-ntg+1)-f(ntg-1))/(delthet(-ntg)+delthet(ntg-1))
    df(ntg) = df(-ntg)

  end subroutine get_dthet

  ! get grad r/a
  subroutine get_gradrho (grho)

    implicit none

    real, dimension (:,:), intent (out) :: grho

    grho = Rr(2,:,:)*sqrt(dRdth**2 + dZdth**2)/jacrho

  end subroutine get_gradrho

  subroutine get_grho_av(grho)
    real, dimension(:), intent(inout) :: grho
    real, dimension(-ntg:ntg, nrad) :: integrand
    real, dimension(-ntg:ntg) :: ones 
    real :: denom
    integer :: ix
    ones = 1.0
    call get_gradrho(integrand)
    do ix = 1,nrad
      call theta_integrate(ones, denom)
      call theta_integrate(integrand(:,ix), grho(ix))
      grho(ix) = grho(ix)/denom
    end do

  end subroutine get_grho_av

  subroutine theta_integrate (integrand, integral)

    implicit none

    real, dimension (-ntg:), intent (in) :: integrand
    real, intent (out) :: integral

    ! use trapezoidal rule to integrate in theta
    integral = 0.5*sum(delthet(-ntg:ntg-1)*(integrand(-ntg:ntg-1) + integrand(-ntg+1:ntg)))

  end subroutine theta_integrate

  ! get indefinite integral of integrand
  subroutine theta_integrate_indef (integrand, integral)

    implicit none

    real, dimension (-ntg:), intent (in) :: integrand
    real, dimension (-ntg:), intent (out) :: integral

    integer :: i

    ! use trapezoidal rule to integrate in theta
    integral(0) = 0.0
    do i = 1, ntg
       integral(i) = integral(i-1)+0.5*delthet(i-1)*(integrand(i-1)+integrand(i))
    end do
    do i = -1, -ntg, -1
       integral(i) = integral(i+1)-0.5*delthet(i)*(integrand(i+1)+integrand(i))
    end do

  end subroutine theta_integrate_indef

  function Rpos (rho, theta, j, irad)
   
    use constants_trin, only: pi

    integer, intent (in) :: j,irad
    real, intent (in) :: rho, theta
    real :: Rpos
    real :: g, gp, drho
    
    drho = rho - rhoc(irad)

    g = cos(theta + tri(irad) * sin(theta))
    gp = -sin(theta + tri(irad) * sin(theta))*triprim(irad)*sin(theta)

    Rpos = rmaj(irad) + shift(irad)*drho + g*rhoc(irad) + (g+rhoc(irad)*gp)*drho
    
  end function Rpos

  function Zpos (rho, theta, j, irad)
   
    integer, intent (in) :: j, irad
    real, intent (in) :: rho, theta
    real :: Zpos, drho

    drho = rho - rhoc(irad)
    Zpos = kappa(irad)*sin(theta)*rhoc(irad) + &
      (rhoc(irad)*kapprim(irad) + kappa(irad))*sin(theta)*drho
    
  end function Zpos

  subroutine finish_miller_geo

    implicit none

    if (allocated(theta)) then
       deallocate (theta, delthet)
       deallocate (Rr, Zr)
       deallocate (dRdrho, dZdrho)
       deallocate (dRdth, dZdth)
       deallocate (jacrho)
       deallocate (rhoc)
       deallocate (rmaj, shift)
       deallocate (kappa, kapprim)
       deallocate (tri, triprim)
    end if
       
  end subroutine finish_miller_geo
  
end module miller_geometry
