!> Contains a set of parameters related to 
!! the progress of the simulation.
module trinity_time

  !> After the iteration has converged, this is equal to the
  !! actual size of the timestep taken in the just-completed timestep, regardless
  !! of whether the timestep increased or decreased.
  real :: ntdelt_old

end module trinity_time
