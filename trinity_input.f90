module trinity_input

  implicit none

  public

  logical :: fork_flag, check_flux_converge, testing
  logical :: write_pwr_profs, include_alphas, include_radiation, write_balance
  logical :: evolve_sources, evolve_boundary
  logical :: adjust_beam_modulation
  logical :: load_balance, dyn_load_balance, evolve_grads_only
  logical :: temp_equil, include_neo, turb_heat, electrostatic
  logical :: evolve_density, evolve_temperature, evolve_flow
  logical :: te_equal_ti, equal_ion_temps, te_fixed
  logical :: subfolders
  logical :: evolve_geometry
  real :: modulation_factor
  real :: grho_in
  integer :: nrad, nrad_netcdf, ncc
  integer :: ntstep, niter, dflx_stencil
  integer :: n_ion_spec, nspec
  integer :: nprofs_evolved, njac, njobs
  integer :: nensembles, nglobalrad, nrad_global
  logical :: flux_reset
  real :: deltadj, avail_cpu_time
  integer :: flux_option_switch, init_option_switch
  integer :: geo_option_switch, source_option_switch
  integer :: fluxlabel_option_switch, ntgrid, ntcc
  integer, dimension (:), allocatable :: flux_groups
  real :: peaking_factor
  real :: density_boost
  integer, parameter :: flux_option_gs2 = 1
  !integer, parameter :: flux_option_gs2_nonlinear = 101 ! This forces gs2 nonlinear
  integer, parameter :: flux_option_gryffin = 2
  integer, parameter :: flux_option_gene = 3
  integer, parameter :: flux_option_global = 4
  integer, parameter :: flux_option_ifspppl = 5
  integer, parameter :: flux_option_tglf = 6
  integer, parameter :: flux_option_offlin = 7
  integer, parameter :: flux_option_quasilinear = 8
  integer, parameter :: flux_option_test1 = 9
  integer, parameter :: flux_option_test2 = 10
  integer, parameter :: flux_option_test3 = 11
  integer, parameter :: flux_option_pb = 12
  integer, parameter :: flux_option_neo = 13
  integer, parameter :: flux_option_gryfx = 14
  integer, parameter :: flux_option_test_calibrate = 15
  integer, parameter :: flux_option_shell_script = 16
  integer, parameter :: flux_option_replay = 17
  integer, parameter :: flux_option_qlknn = 18
  integer, parameter :: flux_option_matched = 19
  integer, parameter :: init_option_analytic = 1
  integer, parameter :: init_option_iterdb = 2
  integer, parameter :: init_option_restart = 3
  integer, parameter :: init_option_expro = 4
  integer, parameter :: init_option_chease = 5
  integer, parameter :: init_option_trinity = 6
  integer, parameter :: init_option_transp = 7
  integer, parameter :: geo_option_analytic = 1
  integer, parameter :: geo_option_iterdb = 2
  integer, parameter :: geo_option_expro = 3
  integer, parameter :: geo_option_iterdb_chease = 4
  integer, parameter :: geo_option_chease = 5
  integer, parameter :: geo_option_ecom = 6
  integer, parameter :: geo_option_trinity = 7
  integer, parameter :: geo_option_transp = 8
  integer, parameter :: source_option_analytic = 1
  integer, parameter :: source_option_iterdb = 2
  integer, parameter :: source_option_expro = 3
  integer, parameter :: source_option_trinity = 4
  integer, parameter :: source_option_transp = 5
  integer, parameter :: fluxlabel_option_torflux = 1
  integer, parameter :: fluxlabel_option_aminor = 2

  character(len=10) :: ifspppl_bmag
  real :: flux_convergetol

  integer :: calib_option_switch
  integer, parameter :: calib_option_piecewise_linear = 1
  integer, parameter :: calib_option_spline = 2

  real :: bt_in, phia_in, rgeo_in, amin_in, zeff_in
  real :: rad_out, bmag, aspr, rgeo, q0, qa, alph_q, sh0
  real :: rad_out_netcdf
  real :: ka, alph_k, kap0, geo_time
  real :: zeff
  real :: ntdelt, impfac, errtol, errflr, ntdelt_max, flrfac
  real :: convergetol
  real :: flxmult, pflx_min, qflx_min, heat_min, numult, vtfac, lflx_min
  real :: tp0, alph1, chi0, prandtl, prandtl_neo
  real :: dfprim, dtprim, dgexb, ddens, dtemp, dom
  real :: dfac, dprimfac, pflx_nfac, pflx_rlnfac, pflx_rltfac
  real :: rlti, rlte, tiedge, teedge, init_time, ledge, rll
  real :: rlni, rlne, niedge, needge
  real :: densfac, tifac, tefac
  real :: psig, densin, nsig, source_time, radin
  real, dimension (:), allocatable :: powerin
  real ::  pioq
  real ::  nbi_mult
  real ::  dens_mult
  real ::  icrh_mult
  real ::  ech_mult
  real ::  lh_mult
  real ::  ibw_mult
  real ::  dwn_mult
  real ::  lgrad_mult
  integer :: ncc_calibrate
  integer :: neval_calibrate
  logical :: calibrate_abs
  logical :: match_gs2_species
  integer :: n_gpus
  character (5000) :: flux_shell_script, replay_filename
  integer :: nifspppl_initial
  integer :: single_radius
  logical :: flux_driver
  real :: tritium_fraction
  character(100) :: dprim_option
  character(100) :: neo_option
  character(100) :: flux_match_switches
  real :: damping

  integer :: is_ref
  logical, dimension (:), allocatable :: is_ref_dens, is_ref_temp
  real, dimension (:), allocatable :: m_ion
  integer, dimension (:), allocatable :: z_ion, charge
  real, dimension (:,:), allocatable :: m_ratio

  logical :: use_external_geo, write_dbinfo, overwrite_db_input
  character (2000) :: init_file, geo_file, source_file, species_file
  character (2000) :: iternt_file, iterflx_file, itercalib_file
  character (2000) :: chease_infile, chease_outfile, restart_file

contains

  subroutine read_parameters

    use unit_tests_trin, only: debug_message, verb_main

    implicit none

    call debug_message(verb_main, 'trinity_input::read_parameters starting')
    call read_geometry
    call debug_message(verb_main, 'trinity_input::read_parameters read geometry')
    call read_species
    call debug_message(verb_main, 'trinity_input::read_parameters read species')
    call read_time
    call debug_message(verb_main, 'trinity_input::read_parameters read time')
    call read_fluxes
    call debug_message(verb_main, 'trinity_input::read_parameters read fluxes')
    call read_sources
    call debug_message(verb_main, 'trinity_input::read_parameters read sources')
    ! read_physics must be called after
    ! read_fluxes because it cares about the 
    ! value of single_radius
    call read_physics
    call debug_message(verb_main, 'trinity_input::read_parameters read physics')
    call read_init
    call debug_message(verb_main, 'trinity_input::read_parameters read init')

    if (single_radius>0) then 
      ! This is a hack to ensure we can use however many 
      ! processes we like for the single flux calc (but
      ! must still be greater than ncc)
      load_balance = .true.
    end if
    if (flux_driver) then
      include_neo = .false. 
      temp_equil = .false. 
      turb_heat = .false. 
    end if
    call debug_message(verb_main, 'trinity_input::read_parameters finished')

  end subroutine read_parameters

  subroutine read_geometry

    use text_options_trin
    use mp_trin, only: proc0, broadcast
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit

    implicit none

    integer :: in_file
    logical :: exist

    type (text_option), dimension (9), parameter :: geoopts = &
         (/ text_option('default', geo_option_analytic), &
         text_option('analytic', geo_option_analytic), &
         text_option('iterdb', geo_option_iterdb), &
         text_option('expro', geo_option_expro),  &
         text_option('iterdb_chease', geo_option_iterdb_chease), &
         text_option('chease', geo_option_chease), &
         text_option('trinity', geo_option_trinity), &
         text_option('ecom', geo_option_ecom), &
         text_option('transp', geo_option_transp) /)
    character (20) :: geo_option

    type (text_option), dimension (3), parameter :: fluxlabelopts = &
         (/ text_option('default', fluxlabel_option_torflux), &
         text_option('torflux', fluxlabel_option_torflux), &
         text_option('aminor', fluxlabel_option_aminor) /)
    character (10) :: fluxlabel_option

    namelist / geometry / geo_option, geo_file, geo_time, rad_out, nrad, bt_in, amin_in, rgeo_in, &
         q0, qa, alph_q, sh0, kap0, ka, alph_k, fluxlabel_option, phia_in, use_external_geo, &
         write_dbinfo, overwrite_db_input, evolve_geometry, grho_in, nrad_netcdf, &
         rad_out_netcdf

    if (proc0) then
       geo_option = "default"             ! construct geo profiles from following parameters
       geo_file = 'pr08_jet_42982_2d.dat' ! file with input data
       chease_infile = 'EXPEQ'            ! file with input data
       chease_outfile = 'ogyroopsi.dat'   ! file with input data
       fluxlabel_option = 'default'       ! sets choice of normalized flux label
       geo_time = 14.0                    ! target time to sample experimental data (in seconds)
       rad_out = 0.95                     ! outer radial boundary
       rad_out_netcdf = -1.0              ! outer radial boundary (=rad_out if < 0)
       nrad = 9                           ! number of radial grid points
       nrad_netcdf = -1                   ! # of radial points in netcdf output (=nrad if <0)
       amin_in = 1.0                      ! geometric minor radius at LCFS (in meters)
       rgeo_in = 3.0                      ! geometric major radius at LCFS (in meters)
       kap0 = 1.35                        ! value of kappa (elongation) at core of plasma
       ka = 1.65                          ! value of kappa at edge of plasma (r/a=1)
       alph_k = 0.8                       ! controls flatness of kappa profile
       q0 = 1.1                           ! value of q at core of profile
       qa = 2.73                          ! value of q at edge of plasma
       alph_q = 0.6                       ! controls flatness of q profile
       sh0 = 1.0                          ! sh0 > 0.0 makes shat increase sharply from zero near rad=0
       bt_in = 0.0                        ! if bt_in > 0.0 and overwrite_db_input=T, set bmag = bt_in (B-field strength in Tesla)
       phia_in = 0.0                      ! if phia_in > 0.0 and overwrite_db_input=T, set psitor_a = phia_in (psitor_a is sqrt(tor flux) at separatrix)
       overwrite_db_input = .false.       ! true if user wants to overwrite 1D variable(s) with input file values
       use_external_geo = .false.         ! true if specifying geometry for flux code externally (e.g. in gs2 input files)
       write_dbinfo = .false.             ! set true to write un-interpolated ITER DB info to screen
       evolve_geometry = .false.          ! if true, recalculate the magnetic equilibrium at each timestep
       grho_in = -1.0 ! overrides the value of grho if > 0

       in_file = input_unit_exist("geometry", exist)
       if (exist) read (unit=input_unit("geometry"), nml=geometry)

       call get_option_value &
            (geo_option, geoopts, geo_option_switch, &
            error_unit(), "geo_option in init")
       call get_option_value &
            (fluxlabel_option, fluxlabelopts, fluxlabel_option_switch, &
            error_unit(), "fluxlabel_option in init")

       if (nrad < 5) then
         write (*,*) "The fitting functions require nrad >= 5; aborting..."
         stop 1
       end if

    end if

    if (nrad_netcdf < 0) nrad_netcdf = nrad
    if (rad_out_netcdf < 0.) rad_out_netcdf = rad_out

    call broadcast (geo_option_switch)
    call broadcast (fluxlabel_option_switch)
    call broadcast (geo_file)
    call broadcast (chease_infile)
    call broadcast (chease_outfile)
    call broadcast (geo_time)
    call broadcast (nrad)
    call broadcast (nrad_netcdf)
    call broadcast (bt_in)
    call broadcast (amin_in)
    call broadcast (rgeo_in)
    call broadcast (kap0)
    call broadcast (ka)
    call broadcast (alph_k)
    call broadcast (q0)
    call broadcast (qa)
    call broadcast (alph_q)
    call broadcast (sh0)
    call broadcast (rad_out)
    call broadcast (rad_out_netcdf)
    call broadcast (phia_in)
    call broadcast (use_external_geo)
    call broadcast (write_dbinfo)
    call broadcast (evolve_geometry)
    call broadcast (grho_in)

    ! number of cell centers is one less than number of grid points
    ncc = nrad-1

    ! length of vectors containing density, ion/electron pressure, and 
    ! toroidal angular momentum at grid points (ntgrid) and at cell centers (ntcc)
    ntgrid = 4*nrad
    ntcc = 4*ncc

    ! initialize 0d values to input values
    rgeo = rgeo_in
    aspr = rgeo/amin_in
    bmag = bt_in

  end subroutine read_geometry

  subroutine read_species

    use text_options_trin
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit

    implicit none

    integer :: in_file
    logical :: exist

    ! qi, mi, zimp, and mimp are being kept here for backwards compatibility
    real :: qi, mi, zimp, mimp
    ! deuterium, tritium, impurity are being kept for backwards compatibility
    integer :: deuterium, tritium, impurity

    integer :: z_ion_1, z_ion_2, z_ion_3
    real :: m_ion_1, m_ion_2, m_ion_3

    namelist / species / n_ion_spec, &
         z_ion_1, z_ion_2, z_ion_3, m_ion_1, m_ion_2, m_ion_3, zeff_in, is_ref, &
         deuterium, tritium, impurity, mimp, zimp, qi, mi

    if (proc0) then
       n_ion_spec = 1                   ! number of ion species to evolve
       z_ion_1 = -1                     ! simulated primary ion species charge (in units of fundamental charge)
       z_ion_2 = -1                     ! simulated secondary ion species charge (in units of fundamental charge)
       z_ion_3 = -1                     ! simulated tertiary ion species charge (in units of fundamental charge)
       m_ion_1 = -1.0                   ! simulated primary ion mass (in units of proton mass)
       m_ion_2 = -1.0                   ! simulated secondary ion mass (in units of proton mass)
       m_ion_3 = -1.0                   ! simulated tertiary ion mass (in units of proton mass)
       zeff_in = 0.0                    ! effective charge
       is_ref = 2                       ! index of reference species in flux calculation
       ! following variables only kept for backward compatibility
       mi = -1.0                        ! old variable for primary ion species mass (kept for backward compatibility)
       qi = -1.0                        ! outdated main ion charge (kept for backward compatibility)       
       mimp = -1.0                      ! old variable for main impurity mass (kept for backward compatibility)
       zimp = -1.0                      ! old variable for main impurity charge number (kept for backward compatibility)
       deuterium = -1                   ! x-label in iter profile database to look for main ion density ("NMx")
       tritium = -1                     ! x-label in iter profile database to look for secondary ion density ("NMx")
       impurity = -1                    ! x-label in iter profile database to look for impurity density ("NMx")

       in_file = input_unit_exist("species", exist)
       if (exist) read (unit=input_unit("species"), nml=species)

    end if

    call broadcast (n_ion_spec)
    call broadcast (is_ref)
    call broadcast (z_ion_1)
    call broadcast (z_ion_2)
    call broadcast (z_ion_3)
    call broadcast (m_ion_1)
    call broadcast (m_ion_2)
    call broadcast (m_ion_3)
    call broadcast (zeff_in)
    call broadcast (mi)
    call broadcast (qi)
    call broadcast (mimp)
    call broadcast (zimp)
    call broadcast (deuterium)
    call broadcast (tritium)
    call broadcast (impurity)

    if (deuterium > -1 .or. tritium > -1 .or. impurity > -1) then
       write (*,*) 'use of deuterium, tritium, and impurity variables'
       write (*,*) 'to identify different ion species is deprecated.'
       write (*,*) 'please use m_ion_1 - m_ion_3 and z_ion_1 - z_ion_3 instead.'
       write (*,*) 'running with defaults'
    end if

    ! initialize 0d value to input value
    zeff = zeff_in

    ! total number of species in calculation
    nspec = n_ion_spec + 1

    ! set up logical array indicating which species
    ! has the reference density and temperature in the flux calculation
    allocate (is_ref_dens(nspec))
    allocate (is_ref_temp(nspec))
    is_ref_dens = .false.
    is_ref_temp = .false.
    is_ref_dens(is_ref) = .true.
    is_ref_temp(is_ref) = .true.
    ! if only one ion species, ne = ni = nref
    ! so electron density is also reference density
    ! needs to be taken into account when forming jacobian
    if (n_ion_spec == 1) is_ref_dens(1) = .true.

    ! fill z_ion and m_ion arrays with ion charges and masses
    ! could easily extend below to include more ion species than 3
    allocate (z_ion(n_ion_spec))
    allocate (m_ion(n_ion_spec))

    ! charge array contains charge number for each species
    allocate (charge(nspec))

    ! m_ratio contains mass ratios for all species combinations
    ! pre-computed for later use in collision frequencies, etc.
    allocate (m_ratio(nspec,nspec))

    if (z_ion_1 >= 1) then
       z_ion(1) = z_ion_1
    else if (qi >= 0.) then
       z_ion(1) = int(qi)
    else
       if (proc0) then
          write (*,*) "No value for z_ion_1 given."
          write (*,*) "Assuming z_ion_1 = 1 unless specified in experimental inputs."
       end if
       z_ion(1) = 1
    end if
    if (m_ion_1 >= epsilon(0.)) then
       m_ion(1) = m_ion_1
    else if (mi >= epsilon(0.)) then
       m_ion(1) = mi
    else
       if (proc0) then
          write (*,*) "No value for m_ion_1 given."
          write (*,*) "Assuming m_ion_1 = 2 (deuterium) unless specified in experimental inputs."
       end if
       m_ion(1) = 2.0
    end if

    if (n_ion_spec > 1) then
       if (z_ion_2 >= 1) then
          z_ion(2) = z_ion_2
       else if (zimp >= 0.) then
          z_ion(2) = int(zimp)
       else
          if (proc0) then
             write (*,*) "No value for z_ion_2 given."
             write (*,*) "Assuming z_ion_2 = 1 unless specified in experimental inputs."
          end if
          z_ion(2) = 1
       end if
       if (m_ion_2 >= epsilon(0.)) then
          m_ion(2) = m_ion_2
       else if (mimp >= epsilon(0.)) then
          m_ion(2) = mimp
       else
          if (proc0) then
             write (*,*) "No value for m_ion_2 given."
             write (*,*) "Assuming m_ion_2 = 3 (tritium) unless specified in experimental inputs."
          end if
          m_ion(2) = 3.0
       end if

       if (n_ion_spec > 2) then
          if (z_ion_3 >= 1) then
             z_ion(3) = z_ion_3
          else if (zimp >= 0.) then
             z_ion(3) = int(zimp)
          else
             if (proc0) then
                write (*,*) "No value for z_ion_3 given."
                write (*,*) "Assuming z_ion_3 = 1 unless specified in experimental inputs."
             end if
             z_ion(3) = 1
          end if
          if (m_ion_3 >= epsilon(0.)) then
             m_ion(3) = m_ion_3
          else if (mimp > epsilon(0.)) then
             m_ion(3) = mimp
          else
             if (proc0) then
                write (*,*) "No value for m_ion_3 given."
                write (*,*) "Assuming m_ion_3 = 12 (carbon) unless specified in experimental inputs."
             end if
             m_ion(3) = 12.0
          end if

          if (n_ion_spec > 3) then
             if (proc0) then
                write (*,*) "more than 3 ion species is not currently supported."
                write (*,*) "aborting simulation."
             end if
             call abort_mp
          end if
       end if
    end if

    call set_mratio_and_charge

  end subroutine read_species

  subroutine set_mratio_and_charge

    use constants_trin, only: mp_over_me

    implicit none

    ! initialize all elements of mass ratio array to 1
    m_ratio = 1.0
    ! now get mass ratio for elements different from unity
    ! first the ion / electron mass ratio
    m_ratio(1,2:) = mp_over_me*m_ion
    m_ratio(2:,1) = 1./m_ratio(1,2:)
    m_ratio(2:,2:) = spread(m_ion,2,n_ion_spec)/spread(m_ion,2,n_ion_spec)

    charge(1) = -1
    charge(2:) = z_ion

  end subroutine set_mratio_and_charge

  subroutine read_time

    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit

    implicit none

    integer :: in_file
    logical :: exist

    namelist / time / ntstep, niter, ntdelt, ntdelt_max, impfac, &
         errtol, errflr, flrfac, nensembles, convergetol, flux_reset, &
         deltadj, avail_cpu_time

    if (proc0) then
       ntstep = 20                      ! number of transport time steps
       niter = 2                        ! max number of newton iterations per time step
       ntdelt = 0.01                    ! initial size of transport time step
       ntdelt_max = 0.1                 ! maximum size of transport time step
       impfac = 1.0                     ! time centering (1=implicit, 0=explicit)
       errtol = 2.0e-1                  ! maximum allowed relative error upon end of newton iteration
       errflr = 5.0e-3                  ! relative error below which we stop iterating
       convergetol = -1.                ! convergence tolerance when seeking steady state
       flrfac = 2.0                     ! if relative error below errflr/flrfac, decrease ntdelt by factor of 2
       nensembles = 1                   ! # of ensembles to use in ensemble (time) average of fluxes
       flux_reset = .false.              ! If redoing a timestep, recalculate fluxes for 1st iteration
       deltadj = 2.0                    ! If the iteration fails, reduce timestep by deltadj
       avail_cpu_time = 1e10        ! Number of seconds of wall clock time available

       in_file = input_unit_exist("time", exist)
       if (exist) read (unit=input_unit("time"), nml=time)
    end if

    if (errflr > errtol) then
      write(error_unit(), *) 'ERROR: you have set errflr > errtol: &
        &this makes no sense and can lead to infinite loops'
      call abort_mp
    end if 

    call broadcast (ntstep)
    call broadcast (niter)
    call broadcast (ntdelt)
    call broadcast (ntdelt_max)
    call broadcast (impfac)
    call broadcast (errtol)
    call broadcast (errflr)
    call broadcast (convergetol)
    call broadcast (flrfac)
    call broadcast (nensembles)
    call broadcast (flux_reset)
    call broadcast (deltadj)
    call broadcast (avail_cpu_time)

  end subroutine read_time

  subroutine read_fluxes

    use text_options_trin
    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit

    implicit none

    integer :: in_file
    logical :: exist

    type (text_option), dimension (20), parameter :: fluxopts = &
         (/ text_option('default', flux_option_gs2), &
         text_option('gs2', flux_option_gs2), &
         text_option('gryffin', flux_option_gryffin), &
         text_option('gene', flux_option_gene), &
         text_option('global', flux_option_global), &
         text_option('ifspppl', flux_option_ifspppl), &
         text_option('tglf', flux_option_tglf), &
         text_option('offset', flux_option_offlin), &
         text_option('quasilinear', flux_option_quasilinear), &
         text_option('test1', flux_option_test1), &
         text_option('test2', flux_option_test2), &
         text_option('test3', flux_option_test3), &
         text_option('pb', flux_option_pb), &
         text_option('gryfx', flux_option_gryfx), &
         text_option('test_calibrate', flux_option_test_calibrate), &
         text_option('shell', flux_option_shell_script), &
         text_option('replay', flux_option_replay), &
         text_option('qlknn', flux_option_qlknn), &
         text_option('matched', flux_option_matched), &
         text_option('neo', flux_option_neo) /)
    character (20) :: flux_option

    type (text_option), dimension (3), parameter :: calibopts = &
         (/ text_option('default', calib_option_piecewise_linear), &
         text_option('piecewise_linear', calib_option_piecewise_linear), &
         text_option('spline', calib_option_spline) /)
    character (20) :: calib_option

    namelist / fluxes / flux_option, flxmult, pflx_min, qflx_min, heat_min, lflx_min, dfac, &
         dfprim, dtprim, dgexb, fork_flag, check_flux_converge, testing, &
         dflx_stencil, dprimfac, pflx_nfac, pflx_rlnfac, pflx_rltfac, vtfac, ddens, dtemp, dom, &
         nglobalrad, alph1, tp0, chi0, prandtl, prandtl_neo, &
         evolve_grads_only, subfolders, lgrad_mult, ncc_calibrate, &
         neval_calibrate, calibrate_abs, match_gs2_species, calib_option, n_gpus, &
         flux_shell_script, replay_filename, nifspppl_initial, single_radius, &
         ifspppl_bmag, flux_driver, flux_convergetol, dprim_option, damping, &
         neo_option, flux_match_switches

    if (proc0) then
       flux_option = 'default'          ! determines flux model (default is gs2)
       vtfac = 2.0                      ! v_t=sqrt(vtfac*T/m). should be consistent with definition from flux code
       flxmult = 1.0                    ! coefficient multiplying fluxes (for testing)
       pflx_min = 1.e-5                 ! minimum particle flux (for below stability threshold)
       qflx_min = 5.e-4                 ! minimum heat flux (for below stability threshold)
       lflx_min = 0.0                   ! minimum momentum flux (for below stability threshold)
       heat_min = 0.0                   ! minimum local heating
       dfprim = 0.1                     ! step size for R/Ln is -R/Ln * dfprim
       dtprim = 0.1                     ! step size for R/LT is R/LT * dtprim
       dgexb = 0.2                      ! step size for gexb is gexb * dgexb
       ddens = 0.2                      ! step size for density is -density * ddens
       dtemp = 0.2                      ! step size for temperature is temperature * dtemp
       dom = 0.2                        ! step size for toroidal rotation frequency, omega, is omega * dom
       dprimfac = 1.0                   ! if dflx_stencil > 2, multiply dprim values that are negative by this
       dflx_stencil = 2                 ! number of fluxes to use for flux derivatives
       dfac = 0.1                       ! diffusion coefficient for testing
       fork_flag = .true.               ! false for running serially
       check_flux_converge = .false.    ! if true, use auto-convergence tests in flux solver
       testing = .false.                ! if testing, set area, grho = 1
       pflx_nfac = -0.05                ! add to ifs-pppl a particle flux of form pflx_nfac*dens
       pflx_rlnfac = 0.01               ! + pflx_rlnfac*rln
       pflx_rltfac = 0.01               ! + pflx_rltfac*rlte
       nglobalrad = 1                   ! number of radial points in global gene simulation (1 for local gene)
!       chi0 = 25.                       ! constant factor multiplying turbulent chii for pb flux option
       chi0 = 20.                       ! constant factor multiplying turbulent chii for pb flux option
       alph1 = 13.                      ! constant factor multiplying gexb for pb flux option
       tp0 = 4.3                        ! critical tprim at zero flow shear for pb flux option
       prandtl = 1.0                    ! turbulent prandtl number for pb flux option
       prandtl_neo = prandtl*0.01       ! neoclassical prandtl number
       evolve_grads_only = .true.       ! if true, calculate only dependence on grads (not profiles themselves) in jacobian
       subfolders = .false.             ! if true, run flux tubes in numbered subfolders
       lgrad_mult = 1.0                 ! Multiply flow gradient by lgrad_mult in ifspppl_fluxes
       ncc_calibrate = 1              ! Number of flux tubes to use for calibration of reduced model
       neval_calibrate = -1              ! Num. calls to get_fluxes between calibrations (set positive to activate)
       calibrate_abs = .false.          ! If true calibration factor is forced positive 
       match_gs2_species = .true.       ! If true, try to match gs2 species to 
                                        ! trinity species using their
                                        ! properties. If not, assume gs2 species
                                        ! and trinity species are in the same
                                        ! order .
       calib_option = 'default'         ! Option for interpolating calib factor
       n_gpus = 1                       ! For gryfx, number of avail gpus
       flux_shell_script = ''
       replay_filename = ''
       nifspppl_initial = -1            ! number of initial ifspppl_fluxes calls
       single_radius = -1               ! If > 0 then run single flux tube at cc_grid(single_radius). All the rest of the fluxes will be the matched fluxes
       flux_driver = .false.            ! If true, quit after get fluxes (i.e. use Trinity as driver for the flux calc)
       ifspppl_bmag = 'trinity'         ! Specify which magnetic field to use with ifspppl. Set to 'trinity' unless you know what you are doing. Other choices are: 'bunit'
       flux_convergetol = 0.5           ! If flux_convergetol > 0, for nonlinear flux models repeatedly call get_fluxes until all radii are converged within flux_convergetol. Do not set too low as fluxes are noisy
       dprim_option = 'proportional'    ! How is the delta gradient calculated.  'constant', 'proportional', 'propabs'
       damping = 0.0                    ! If > 0 damp grid scale oscillations in the profiles
       neo_option = 'analytic'          ! Choice for neoclassical fluxes: 'analytic', 'neo'
       flux_match_switches = 'nnnnnnnnnnnnnnn' ! String of flux matching choices. Each letter controls a species (electrons on the left). n = none, d = density, t = temperature, a = all
      
       

       in_file = input_unit_exist("fluxes", exist)
       if (exist) read (unit=input_unit("fluxes"), nml=fluxes)

       call get_option_value &
            (flux_option, fluxopts, flux_option_switch, &
            error_unit(), "flux_option in init")

       call get_option_value &
            (calib_option, calibopts, calib_option_switch, &
            error_unit(), "calib_option in init")

       if (dflx_stencil > 3) then
          write (*,*) "dflx_stencil > 3 not currently supported.  setting dflx_stencil = 3."
          dflx_stencil = 3
       end if

!        ! variable 'evolved' is a flag letting other bits of code
! !       ! know which profiles are evolved. 1=ne, 2=Ti, 3=Te, 4=L
!        ! know which profiles are evolved. 1=ne, 2=Te, 3=Ti, 4=L
!        select case (grad_option_switch)
!        case (grad_option_tgrads)
! !          ngrads = 2
!           ngrads = 2 * (dflx_stencil-1)
!           evolved(2:3) = .true.
!        case (grad_option_tigrad)
! !          ngrads = 1
!           ngrads = dflx_stencil - 1
!           evolved(3) = .true.
!        case (grad_option_all)
! !          ngrads = 3
!           ngrads = 4 * (dflx_stencil-1)
!           evolved = .true.
!        case (grad_option_ngrad)
! !          ngrads = 1
!           ngrads = dflx_stencil-1
!           evolved(1) = .true.
!        case (grad_option_ltgrads)
!           ngrads = 3*(dflx_stencil-1)
!           evolved(2:4) = .true.
!        case (grad_option_lgrad)
!           ngrads = dflx_stencil-1
!           evolved(4) = .true.
!        case (grad_option_ntgrads)
!           ngrads = 3 * (dflx_stencil-1)
!           evolved(1:3) = .true.
!        end select

       if (flux_option_switch == flux_option_gs2 .or. flux_option_switch == flux_option_gene &
            .or. flux_option_switch == flux_option_gryffin  &
            ) then 
            !.or.flux_option_switch == flux_option_gryfx) then
          if (.not. fork_flag) then
             fork_flag = .true.
             write (*,*) "fork_flag automatically set to TRUE when calculating fluxes"
             write (*,*) "with nonlinear gyrokinetic or gyrofluid code"
          end if
       else if (check_flux_converge) then
          check_flux_converge = .false.
          write (*,*) "check_flux_converge not a valid option for model fluxes. setting to FALSE."
       end if

!        if (evolve_grads_only) then
!           njac = ngrads+1
!        else
!           njac = 2*ngrads+1
!        end if

!        if (nglobalrad == 1) then
!           nrad_global = 1
!           njobs = ncc*njac
!        else
! !          nrad_global = nrad
!           nrad_global = ncc
!           if (nglobalrad.ne.ncc) then
!              write(*,*) 'nglobalrad must be 1 or nrad. Taking the latter ...'
!              nglobalrad = ncc
!           endif
!           njobs = njac
!        end if
   
    end if

    call broadcast (flux_option_switch)
    call broadcast (vtfac)
    call broadcast (flxmult)
    call broadcast (pflx_min)
    call broadcast (qflx_min)
    call broadcast (lflx_min)
    call broadcast (heat_min)
    call broadcast (dfprim)
    call broadcast (dtprim)
    call broadcast (dgexb)
    call broadcast (ddens)
    call broadcast (dtemp)
    call broadcast (dom)
    call broadcast (dfac)
    call broadcast (fork_flag)
    call broadcast (check_flux_converge)
    call broadcast (testing)
    call broadcast (dflx_stencil)
    call broadcast (dprimfac)
    call broadcast (pflx_nfac)
    call broadcast (pflx_rlnfac)
    call broadcast (pflx_rltfac)
    call broadcast (nglobalrad)
    call broadcast (nrad_global)
    call broadcast (chi0)
    call broadcast (alph1)
    call broadcast (tp0)
    call broadcast (prandtl)
    call broadcast (prandtl_neo)
    call broadcast (evolve_grads_only)
    call broadcast (subfolders)
    call broadcast (lgrad_mult)
    call broadcast (neval_calibrate)
    call broadcast (ncc_calibrate)
    call broadcast (calibrate_abs)
    call broadcast (match_gs2_species)
    call broadcast (n_gpus)
    call broadcast (flux_shell_script)
    call broadcast (replay_filename)
    call broadcast (nifspppl_initial)
    call broadcast (single_radius)
    call broadcast (flux_driver)
    call broadcast (calib_option_switch)
    call broadcast (ifspppl_bmag)
    call broadcast (flux_convergetol)
    call broadcast (dprim_option)
    call broadcast (damping)
    call broadcast (neo_option)
    call broadcast (flux_match_switches)

    if (nglobalrad > 1) then
       write (*,*) 'nglobalrad > 1 is not currently supported.'
       write (*,*) 'Aborting run.'
       call abort_mp
    end if

  end subroutine read_fluxes

  subroutine finish_trinity_input
    deallocate(is_ref_dens, is_ref_temp)
    deallocate(flux_groups)
    deallocate(m_ion, z_ion)
    deallocate(m_ratio, charge)
    deallocate(powerin)
  end subroutine finish_trinity_input

  subroutine read_sources

    use text_options_trin
    use mp_trin, only: proc0, broadcast
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit
    
    implicit none

    integer :: in_file
    logical :: exist

    real :: e_powerin, i_powerin_1, i_powerin_2, i_powerin_3

    type (text_option), dimension (6), parameter :: sourceopts = &
         (/ text_option('default', source_option_analytic), &
         text_option('analytic', source_option_analytic), &
         text_option('iterdb', source_option_iterdb), &
         text_option('trinity', source_option_trinity), &
         text_option('transp', source_option_transp), &
         text_option('expro', source_option_expro) /)
    character (10) :: source_option

    namelist / sources / source_option, source_file, source_time, &
         e_powerin, i_powerin_1, i_powerin_2, i_powerin_3, psig, radin, &
         densin, nsig, write_pwr_profs, include_alphas, include_radiation, &
         pioq, evolve_sources, adjust_beam_modulation, modulation_factor, &
         nbi_mult, &
         dens_mult, &
         icrh_mult, &
         ech_mult, &
         lh_mult, &
         ibw_mult, &
         dwn_mult, &
         write_balance, &
         tritium_fraction

    allocate (powerin(nspec)) ; powerin = 0.0

    if (proc0) then

       source_option = "default"        ! construct source profiles from following parameters
       source_file = geo_file           ! file with input data
       source_time = geo_time           ! target time to sample experimental data (in seconds)
       e_powerin = 10.0                 ! external electron power input (in MW)
       i_powerin_1 = 40.0               ! external primary ion power input (in MW)
       i_powerin_2 = 0.0                ! external secondary ion power input (in MW)
       i_powerin_3 = 0.0                ! external tertiary ion power input (in MW)
       densin = 0.0                     ! external density input
       radin = 0.0                      ! spatial mean of gaussian power deposition profile
       psig = 0.1                       ! width of power input profile (in units of minor radius)
       nsig = 0.1                       ! width of density input profile (in units of minor radius)
       pioq = 0.1                       ! ratio of momentum to heat input
       include_alphas = .true.          ! include alpha heating source
       include_radiation = .true.       ! include bremstrahlung radiation
       evolve_sources = .false.         ! include time-dependent sources
       adjust_beam_modulation = .false. ! allows for artifical modification of neutral beam modulation
       modulation_factor = 1.0          ! factor by which to multiply beam modulation amplitude
       write_pwr_profs = .false.        ! true writes out radial power density profiles
       write_balance = .false.          ! true writes out various terms in transport equation each time step
       nbi_mult = 1.0                   ! multiplies QNBII, QNBIE and SNBIE when using the tokamak profile db, may not be self consistent when using TORQ for torque input (as opposed to pioq)
       dens_mult = 1.0                   ! multiplies overall density source when using the tokamak profile db, may not be self consistent when using TORQ for torque input (as opposed to pioq)
       icrh_mult = 1.0                  ! multiplies QICRHI and QICRHE when using the tokamak profile db
       ech_mult = 1.0                   ! multiplies QECHI and QECHE when using the tokamak profile db
       lh_mult = 1.0                   ! multiplies QLHI and QLHE when using the tokamak profile db
       ibw_mult = 1.0                   ! multiplies QIBWI and QIBWE when using the tokamak profile db
       dwn_mult = 1.0                   ! multiplies DWIR, DWER, DNER when using the tokamak profile db
       tritium_fraction = -1.0          ! If > 0.0 sets the fraction of tritium in alpha_power, overriding anyother data

       in_file = input_unit_exist("sources", exist)
       if (exist) read (unit=input_unit("sources"), nml=sources)

       call get_option_value &
            (source_option, sourceopts, source_option_switch, &
            error_unit(), "source_option in init")

       powerin(1) = e_powerin
       powerin(2) = i_powerin_1
       if (n_ion_spec > 1) then
          powerin(3) = i_powerin_2
          if (n_ion_spec > 2) then
             powerin(4) = i_powerin_3
             if (n_ion_spec > 3) then
                write (*,*) 'n_ion_spec > 3 not currently supported.'
                write (*,*) 'ignoring ion species beyond 3.'
             end if
          end if
       end if
    end if

    call broadcast (source_option_switch)
    call broadcast (source_file)
    call broadcast (source_time)
    call broadcast (powerin)
    call broadcast (densin)
    call broadcast (radin)
    call broadcast (pioq)
    call broadcast (nbi_mult)
    call broadcast (dens_mult)
    call broadcast (icrh_mult)
    call broadcast (ech_mult)
    call broadcast (lh_mult)
    call broadcast (ibw_mult)
    call broadcast (dwn_mult)
    call broadcast (psig)
    call broadcast (nsig)
    call broadcast (write_pwr_profs)
    call broadcast (write_balance)
    call broadcast (include_alphas)
    call broadcast (include_radiation)
    call broadcast (evolve_sources)
    call broadcast (adjust_beam_modulation)
    call broadcast (modulation_factor)
    call broadcast (tritium_fraction)

  end subroutine read_sources

  subroutine read_physics

    use mp_trin, only: proc0, broadcast, abort_mp
    use file_utils_trin, only: input_unit, input_unit_exist

    implicit none

    integer :: in_file
    logical :: exist

    namelist / physics / include_neo, temp_equil, turb_heat, numult, electrostatic, &
         evolve_density, evolve_temperature, evolve_flow, te_equal_ti, equal_ion_temps, te_fixed

    if (proc0) then

       include_neo = .true.             ! true includes chang-hinton estimate for neoclassical ion heat flux
       temp_equil = .true.              ! set to false to neglect temperature equilibration
       turb_heat = .true.               ! set to false to neglect turbulent heating
       numult = 1.0                     ! multiplier of collision frequency for testing
       electrostatic = .true.           ! if true, set beta to zero (or very small)
       evolve_density = .true.          ! if true, evolve density profile(s)
       evolve_temperature = .true.      ! if true, evolve temperature profile(s)
       evolve_flow = .false.            ! if true, evolve toroidal angular momentum
       te_equal_ti = .false.            ! if true, Te is set equal to Ti instead of being evolved
       equal_ion_temps = .true.         ! if true, temperature of all ion species is assumed to be the same
       te_fixed = .false.               ! if true, electron temperature stays at initial value

       in_file = input_unit_exist("physics", exist)
       if (exist) read (unit=input_unit("physics"), nml=physics)

    end if

    call broadcast (include_neo)
    call broadcast (temp_equil)
    call broadcast (turb_heat)
    call broadcast (numult)
    call broadcast (electrostatic)
    call broadcast (evolve_density)
    call broadcast (evolve_temperature)
    call broadcast (evolve_flow)
    call broadcast (te_equal_ti)
    call broadcast (equal_ion_temps)
    call broadcast (te_fixed)

    if (flux_driver) then
      evolve_temperature = .false.
      evolve_density = .false.
      evolve_flow = .false.
    end if

    ! force ion temperatures to be the same if
    ! we are assuming plasma collisional enough
    ! to get Te = Ti
    if (te_equal_ti .or. te_fixed) then
      equal_ion_temps = .true.
      write (*,*) 'INFO: You have set te_equal_ti: automatically setting &
        & equal_ion_temps = .true.'
      if (te_equal_ti .and. te_fixed) then
        write (*,*) 'ERROR: You have set te_equal_ti and te_fixed both true'
        call abort_mp
      end if
    end if
        

    ! nprofs_evolved is the total number of plasma profiles to evolve
    nprofs_evolved = 0
    if (evolve_density) nprofs_evolved = nprofs_evolved + nspec-1
    if (evolve_temperature) then
       if (equal_ion_temps) then
          nprofs_evolved = nprofs_evolved + 1
       else
          nprofs_evolved = nprofs_evolved + n_ion_spec
       end if
       if (.not.te_equal_ti.and..not.te_fixed) nprofs_evolved=nprofs_evolved+1
    end if
    if (evolve_flow) nprofs_evolved = nprofs_evolved + 1

    if (nprofs_evolved==0 .and. .not. flux_driver) then
       if (proc0) write (*,*) 'No profiles have been selected to evolve.'
       if (proc0) write (*,*) 'Please set "evolve_density", "evolve_temperature", or "evolve_flow" to .true.'
       if (proc0) write (*,*) 'Aborting run.'
       call abort_mp
    end if

    ! njac is number of variables on which the jacobian is assumed to depend
    ! (+1 for unperturbed profile)
    ! evolve_grads_only=T means that we assume the jacobian depends primarily
    ! on profile gradient scale lengths
    ! evolve_grads_only=F means that we also take into account dependence of
    ! jacobian on local profile values themselves
    !
    if (evolve_grads_only) then
       njac = nprofs_evolved + 1
    else
       njac = 2*nprofs_evolved + 1
    end if

    ! njobs is the total number of flux tube simulations to run in parallel
    
    ! nglobalrad=1 for local flux simulation
    if (nglobalrad == 1) then
       nrad_global = 1
       njobs = ncc*njac
    else
       nrad_global = ncc
       if (nglobalrad.ne.ncc) then
          write(*,*) 'nglobalrad must be 1 or ncc. Taking the latter ...'
          nglobalrad = ncc
       endif
       njobs = njac
    end if

    allocate (flux_groups(njobs))

  end subroutine read_physics

  subroutine read_init

    use text_options_trin
    use mp_trin, only: proc0, broadcast, nproc
    use file_utils_trin, only: input_unit, input_unit_exist, error_unit

    implicit none

    integer :: in_file
    logical :: exist

    type (text_option), dimension (8), parameter :: initopts = &
         (/ text_option('default', init_option_analytic), &
         text_option('analytic', init_option_analytic), &
         text_option('iterdb', init_option_iterdb), &
         text_option('chease', init_option_chease), &
         text_option('restart', init_option_restart), &
         text_option('trinity', init_option_trinity), &
         text_option('transp', init_option_transp), &
         text_option('expro', init_option_expro) /)
    character (10) :: init_option

    namelist / init / init_option, init_file, init_time, &
         rlni, rlne, niedge, needge, rlti, rlte, tiedge, teedge, &
         densfac, tifac, tefac, ledge, rll, iternt_file, iterflx_file, load_balance, &
         dyn_load_balance, flux_groups, peaking_factor, &
         itercalib_file, density_boost, restart_file, &
         evolve_boundary

    if (proc0) then
       init_option = 'default'             ! construct initial profiles from following parameters
       init_file = geo_file                ! file with input data
       init_time = geo_time                ! target time to sample experimental data (in seconds)
       iternt_file = "restart.iternt"      ! file with old profile data for restarts in the middle of a timestep
       iterflx_file = "restart.iterflx"    ! file with old flx date for restarts in the middle of a timestep
       itercalib_file = "restart.itercalib"! file with old calibration data for restarts
       restart_file = 'old'                ! name of the new netcdf restart file, set to 'old' for old restart
       rlni = 1.0                          ! initial R/Lni
       rlne = 1.0                          ! initial R/Lne
       rlti = 4.0                          ! initial R/LTi
       rlte = 4.0                          ! initial R/LTe
       rll = 0.0                           ! initial R/LL
       niedge = -1.0                       ! initial ni offset. if positive, override init_option for dens
       needge = -1.0                       ! initial ne offset. if positive, override init_option for dens
       tiedge = -1.0                       ! initial Ti offset. if positive, override init_option for Ti
       teedge = -1.0                       ! initial Te offset. if positive, override init_option for Te
       ledge = -1.0                        ! initial L offset. if positive, override init_option for L
       densfac = 1.0                       ! factor multiplying ITER DB density
       tifac = 1.0                         ! factor multiplying ITER DB ion temperature
       tefac = 1.0                         ! factor multiplying ITER DB electron temperature
       load_balance = .false.              ! set to true to run with unequal number of procs per flux tube
       dyn_load_balance = .false.          ! set to true to dynamically reallocate processes at runtime
       evolve_boundary = .false.           ! set to true to use time-dependent boundary density, temperature, etc.

       flux_groups = nproc/njobs           ! The number of processors for each flux calculation
       peaking_factor = -1.0               ! If >0 then changes peaking of profiles (1.0 leaves profs unchanged)
       density_boost = -1.0               ! If >0 then multiplies initial dens and divides initial temp (init_option_chease only)
    
       in_file = input_unit_exist("init", exist)
       if (exist) read (unit=input_unit("init"), nml=init)

       call get_option_value &
            (init_option, initopts, init_option_switch, &
            error_unit(), "init_option in init")

    end if

    call broadcast (init_option_switch)
    call broadcast (init_file)
    call broadcast (init_time)
    call broadcast (iternt_file)
    call broadcast (itercalib_file)
    call broadcast (iterflx_file)
    call broadcast (restart_file)
    call broadcast (rlni)
    call broadcast (rlne)
    call broadcast (rlti)
    call broadcast (rlte)
    call broadcast (rll)
    call broadcast (niedge)
    call broadcast (needge)
    call broadcast (tiedge)
    call broadcast (teedge)
    call broadcast (ledge)
    call broadcast (densfac)
    call broadcast (tifac)
    call broadcast (tefac)
    call broadcast (flux_groups)
    call broadcast (peaking_factor)
    call broadcast (density_boost)
    call broadcast (load_balance)
    call broadcast (dyn_load_balance)
    call broadcast (evolve_boundary)

  end subroutine read_init

end module trinity_input
