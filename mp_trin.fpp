# include "define.inc"

module mp_trin
!
! Easier Fortran90 interface to the MPI Message Passing Library.
!
!     (c) Copyright 1991 to 1998 by Michael A. Beer, William D. Dorland, 
!     P. B. Snyder, Q. P. Liu, and Gregory W. Hammett. ALL RIGHTS RESERVED.
!     
! Note: mp_mpi_r8.f90 is a version of mp_mpi.f90 to use when compiling 
! with -r8 (where the default real type is taken to be 8 bytes).  Just 
! replaced all occurances of MPI_REAL with MPI_DOUBLE_PRECISION and 
! MPI_COMPLEX with MPI_DOUBLE_COMPLEX.
!
# ifdef MPI
# ifndef MPIINC
  use mpi
# endif
  ! TT: I experienced a problem on Ranger with mvapich-devel.
  ! TT: Compiler complained an inconsistent variable type for reorder below.
  ! TT: In this case, the problem was solved by commenting out the above line
  ! TT: and use mpif.h below.  (4/16/08)
# endif
  implicit none
  private

  public :: init_mp, abort_mp, finish_mp
  public :: broadcast
  public :: nproc, iproc, proc0, job
  public :: send, ssend, receive
  public :: barrier
  public :: scope, allprocs, subprocs, gryfxprocs, singleprocs
  public :: all_to_group, group_to_all
  public :: job_fork
  public :: communicator
  public :: igpu
  public :: fork_single
  public :: current_focus
  !> EGH needed for GRYFX
  public :: fork_gryfx
  public :: destroy_comm_gryfx
  public :: sum_allreduce_real_3array
  public :: sum_allreduce_real_2array
  public :: timer_local

# ifdef MPI
# ifdef MPIINC
! CMR: defined MPIINC for machines where need to include mpif.h
  include 'mpif.h'
#endif

  integer, pointer :: nproc
  integer, target :: ntot_proc, ngroup_proc, ngryfx_proc, nsingle_proc

  integer, pointer :: iproc
  integer, target :: aproc, gproc, xproc, sproc

  logical, pointer :: proc0
  logical, target :: aproc0, gproc0, xproc0, sproc0

  integer, pointer :: communicator
  integer, target :: comm_all, comm_group, comm_gryfx, comm_single


  integer :: job

  integer :: igpu

  integer (kind(MPI_REAL)) :: mpireal, mpicmplx
# else
  integer, parameter :: nproc = 1, iproc = 0
  logical, parameter :: proc0 = .true.

  integer, parameter :: job = 0, communicator = -1
# endif
  integer, parameter :: allprocs = 0, subprocs = 1, gryfxprocs = 2, singleprocs=3
  integer :: current_focus = allprocs

  integer, dimension (:), allocatable :: grp0

  logical :: fork_gryfx_initialized = .false.

  interface init_jobs
     module procedure init_jobs_cart
     module procedure init_jobs_split
  end interface init_jobs

  !>EGH
  integer :: mpcom
  integer :: broadcast_count

  interface broadcast
     module procedure broadcast_integer 
     module procedure broadcast_integer_array 

     module procedure broadcast_real    
     module procedure broadcast_real_array    
     module procedure broadcast_real_2array    
     module procedure broadcast_real_3array 

     module procedure broadcast_complex 
     module procedure broadcast_complex_array

     module procedure broadcast_logical 
     module procedure broadcast_logical_array 
     module procedure broadcast_logical_2array 
     module procedure broadcast_logical_3array 

     module procedure bcastfrom_integer 
     module procedure bcastfrom_integer_array 

     module procedure bcastfrom_real    
     module procedure bcastfrom_real_array    

     module procedure bcastfrom_complex 
     module procedure bcastfrom_complex_array 

     module procedure bcastfrom_logical 
     module procedure bcastfrom_logical_array 

     module procedure broadcast_character
     module procedure bcastfrom_character
     module procedure broadcast_character_array
  end interface

  interface send
     module procedure send_integer
     module procedure send_integer_array

     module procedure send_real
     module procedure send_real_array
     module procedure send_real_array_2d

     module procedure send_complex
     module procedure send_complex_array

     module procedure send_logical
     module procedure send_logical_array
     
     module procedure send_character
  end interface

  interface receive
     module procedure receive_integer
     module procedure receive_integer_array

     module procedure receive_real
     module procedure receive_real_array
     module procedure receive_real_array_2d

     module procedure receive_complex
     module procedure receive_complex_array

     module procedure receive_logical
     module procedure receive_logical_array

     module procedure receive_character
  end interface

  interface ssend
     module procedure ssend_integer
     module procedure ssend_integer_array

     module procedure ssend_real
     module procedure ssend_real_array
     module procedure ssend_real_array_2d

     module procedure ssend_complex
     module procedure ssend_complex_array

     module procedure ssend_logical
     module procedure ssend_logical_array
  end interface

! send stuff from global proc0 to group proc0s
  interface all_to_group
     module procedure all_to_group_real
     module procedure all_to_group_real_array
     module procedure all_to_group_real_global
     module procedure all_to_group_real_global_array
  end interface

! send stuff from group proc0s to global proc0
  interface group_to_all
     module procedure group_to_all_real
     module procedure group_to_all_real_array
     module procedure group_to_all_real_global
     module procedure group_to_all_real_global_array
     module procedure group_to_all_logical
     module procedure group_to_all_logical_global
  end interface
! <MAB

contains

  subroutine init_mp(mp_communicator)
    use constants_trin, only: pi, kind_rs, kind_rd
    implicit none
    integer, intent(in), optional :: mp_communicator
# ifdef MPI
    integer :: ierror

    broadcast_count = 0
    !write (*,*) 'Assigning communicator'
    if (present(mp_communicator)) then 
       !write (*,*) 'Given communicator'
      mpcom = mp_communicator
    else
      call mpi_init (ierror)
      mpcom = mpi_comm_world
    end if

    !write (*,*) 'Getting size'
    call mpi_comm_size (mpcom, ntot_proc, ierror)
    call mpi_comm_rank (mpcom, aproc, ierror)
    comm_all = mpcom
    aproc0 = aproc == 0


    write (*,*) 'Trinity is running on ', ntot_proc, 'processors'

    call scope (allprocs)

    if ( (kind(pi)==kind_rs) .and. (kind_rs/=kind_rd) ) then
       mpireal = MPI_REAL4
       mpicmplx = MPI_COMPLEX8
    else if (kind(pi)==kind_rd) then
       mpireal = MPI_REAL8
       mpicmplx = MPI_COMPLEX16
    else
       write (*,*) 'ERROR: precision mismatch in mpi'
    end if
# endif

  end subroutine init_mp

  subroutine scope (focus)

    integer, intent (in) :: focus

    current_focus = focus

# ifdef MPI
    if (focus == allprocs) then
       communicator => comm_all
       nproc => ntot_proc
       iproc => aproc
       proc0 => aproc0
    else if (focus == gryfxprocs) then
       communicator => comm_gryfx
       nproc => ngryfx_proc
       iproc => xproc
       proc0 => xproc0
    else if (focus == singleprocs) then
       communicator => comm_single
       nproc => nsingle_proc
       iproc => sproc
       proc0 => sproc0
    else
       communicator => comm_group
       nproc => ngroup_proc
       iproc => gproc
       proc0 => gproc0
    end if
# endif

  end subroutine scope

  subroutine fork_gryfx(n_gpus)
    integer, intent(in) :: n_gpus
    integer :: ierror, i2,i3
    character(mpi_max_error_string) :: str
    integer :: test

    if (fork_gryfx_initialized) return
    fork_gryfx_initialized = .true.



#ifdef MPI
    !test = aproc
    !if (aproc0) test = 28888 
    !call broadcast(test)
    !write (*,*) 'my rank is ', aproc, ' and test is', test, ' and focus is ', current_focus
    igpu= floor(real(aproc)/real(ntot_proc)*real(n_gpus)+epsilon(1.0))
    !write (*,*) 'Before my igpu is', igpu, &
      !'comm_all is', comm_all, 'mpi_undefined', mpi_undefined
    call mpi_comm_split(comm_all,igpu,aproc,comm_gryfx,ierror)
    !write (*,*) 'split com'
    !call mpi_error_string(ierror, str, i3, i2)
    !write (*,*) 'mpi error string', trim(str)
    call mpi_comm_size (comm_gryfx, ngryfx_proc, ierror)
    call mpi_comm_rank (comm_gryfx, xproc, ierror)
    !write (*,*) 'my igpu is', igpu, 'my comm_gryfx is', comm_gryfx, &
      !'comm_all is', comm_all, 'my rank is', xproc
    xproc0 = xproc == 0
    call scope (gryfxprocs)
    !if (xproc0) test = aproc 
    !call broadcast(test)
    !write (*,*) 'my rank is ', xproc, ' and test is', test
#endif
  end subroutine fork_gryfx

  subroutine destroy_comm_gryfx
    integer :: ierror
    fork_gryfx_initialized = .false. 
#ifdef MPI
    call mpi_comm_free (comm_gryfx, ierror)
    call scope (allprocs)
#endif
  end subroutine destroy_comm_gryfx

  !> Used for running a single gs2 flux tube, i.e. using Trinity
  !! as a driver for gs2. Also used for running gs2 only for 
  !! a given cell centre (and using e.g. matched fluxes elsewhere)
  subroutine fork_single(key,color)
    integer, intent(in) :: key
    integer, intent(in) :: color
    integer :: ierror
#ifdef MPI
    call mpi_comm_split(comm_all,color,key,comm_single,ierror)
    call mpi_comm_size (comm_gryfx, nsingle_proc, ierror)
    call mpi_comm_rank (comm_gryfx, sproc, ierror)
    sproc0 = sproc == 0
    call scope (singleprocs)
#endif
  end subroutine fork_single

  subroutine abort_mp
# ifdef MPI
    implicit none
    integer :: ierror

    call mpi_abort (MPI_COMM_WORLD,1,ierror)
# else
    stop
# endif    
  end subroutine abort_mp

  subroutine finish_mp
# ifdef MPI
    implicit none
    integer :: ierror

    call mpi_finalize (ierror)
# endif
  end subroutine finish_mp

! ************** broadcasts *****************************

  subroutine broadcast_character (char)
    implicit none
    character(*), intent (in out) :: char
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (char, len(char), MPI_CHARACTER, 0, communicator, ierror)
# endif
  end subroutine broadcast_character

  subroutine broadcast_integer (i)
    implicit none
    integer, intent (in out) :: i
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (i, 1, MPI_INTEGER, 0, communicator, ierror)
# endif
  end subroutine broadcast_integer

  subroutine broadcast_integer_array (i)
    implicit none
    integer, dimension (:), intent (in out) :: i
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (i, size(i), MPI_INTEGER, 0, communicator, ierror)
# endif
  end subroutine broadcast_integer_array

  subroutine broadcast_real (x)
    implicit none
    real, intent (in out) :: x
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (x, 1, mpireal, 0, communicator, ierror)
# endif
  end subroutine broadcast_real

  subroutine broadcast_real_array (x)
    implicit none
    real, dimension (:), intent (in out) :: x
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (x, size(x), mpireal, 0, communicator, ierror)
# endif
  end subroutine broadcast_real_array

  subroutine broadcast_real_2array(x)
  implicit none
  real, dimension(:,:), intent (in out) :: x
# ifdef MPI
    integer :: ierror
    call mpi_bcast (x, size(x), mpireal, 0, communicator, ierror)
# endif
  end subroutine broadcast_real_2array

  subroutine broadcast_real_3array(x)
  implicit none
  real, dimension(:,:,:), intent (in out) :: x
# ifdef MPI
    integer :: ierror
    call mpi_bcast (x, size(x), mpireal, 0, communicator, ierror)
# endif
  end subroutine broadcast_real_3array

  subroutine broadcast_complex (z)
    implicit none
    complex, intent (in out) :: z
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (z, 1, mpicmplx, 0, communicator, ierror)
# endif
  end subroutine broadcast_complex

  subroutine broadcast_complex_array (z)
    implicit none
    complex, dimension (:), intent (in out) :: z
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (z, size(z), mpicmplx, 0, communicator, ierror)
# endif
  end subroutine broadcast_complex_array

  subroutine broadcast_logical (f)
    implicit none
    logical, intent (in out) :: f
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (f, 1, MPI_LOGICAL, 0, communicator, ierror)
# endif
  end subroutine broadcast_logical

  subroutine broadcast_logical_array (f)
    implicit none
    logical, dimension (:), intent (in out) :: f
# ifdef MPI
    integer :: ierror
    !broadcast_count = broadcast_count + 1
    !write (*,*) 'broadcast_count', broadcast_count, aproc
    call mpi_bcast (f, size(f), MPI_LOGICAL, 0, communicator, ierror)
# endif
  end subroutine broadcast_logical_array

  subroutine broadcast_logical_2array (f)
    implicit none
    logical, dimension (:,:), intent (in out) :: f
# ifdef MPI
    integer :: ierror
    call mpi_bcast (f, size(f), MPI_LOGICAL, 0, communicator, ierror)
# endif
  end subroutine broadcast_logical_2array

  subroutine broadcast_logical_3array (f)
    implicit none
    logical, dimension (:,:,:), intent (in out) :: f
# ifdef MPI
    integer :: ierror
    call mpi_bcast (f, size(f), MPI_LOGICAL, 0, communicator, ierror)
# endif
  end subroutine broadcast_logical_3array

  subroutine bcastfrom_logical (f, src)
    implicit none
    logical, intent (in out) :: f
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (f, 1, MPI_LOGICAL, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_logical

  subroutine bcastfrom_logical_array (f, src)
    implicit none
    logical, dimension (:), intent (in out) :: f
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (f, size(f), MPI_LOGICAL, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_logical_array

  subroutine bcastfrom_character (c, src)
    implicit none
    character(*), intent (in out) :: c
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (c, len(c), MPI_CHARACTER, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_character

  ! An array of characters, each of len 1
  subroutine broadcast_character_array (char)
    implicit none
    character, dimension(:), intent (in out) :: char
# ifdef MPI
    integer :: ierror
    call mpi_bcast (char, size(char), MPI_CHARACTER, 0, communicator, ierror)
# endif
  end subroutine broadcast_character_array

  subroutine bcastfrom_integer (i, src)
    implicit none
    integer, intent (in out) :: i
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (i, 1, MPI_INTEGER, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_integer

  subroutine bcastfrom_integer_array (i, src)
    implicit none
    integer, dimension (:), intent (in out) :: i
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (i, size(i), MPI_INTEGER, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_integer_array

  subroutine bcastfrom_real (x, src)
    implicit none
    real, intent (in out) :: x
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (x, 1, mpireal, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_real

  subroutine bcastfrom_real_array (x, src)
    implicit none
    real, dimension (:), intent (in out) :: x
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (x, size(x), mpireal, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_real_array

  subroutine bcastfrom_complex (z, src)
    implicit none
    complex, intent (in out) :: z
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (z, 1, mpicmplx, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_complex

  subroutine bcastfrom_complex_array (z, src)
    implicit none
    complex, dimension (:), intent (in out) :: z
    integer, intent (in) :: src
# ifdef MPI
    integer :: ierror
    call mpi_bcast (z, size(z), mpicmplx, src, communicator, ierror)
# else
    if (src /= 0) call error ("broadcast from")
# endif
  end subroutine bcastfrom_complex_array

! ********************* barrier **********************

  subroutine barrier
# ifdef MPI
    implicit none
    integer :: ierror
    call mpi_barrier (communicator, ierror)
# endif
  end subroutine barrier

! ********************* sends **********************

  subroutine send_integer (i, dest, tag)
    implicit none
    integer, intent (in) :: i
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (i, 1, MPI_INTEGER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_integer

  subroutine send_integer_array (i, dest, tag)
    implicit none
    integer, dimension (:), intent (in) :: i
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (i, size(i), MPI_INTEGER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_integer_array

  subroutine send_real (a, dest, tag)
    implicit none
    real, intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (a, 1, mpireal, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_real

  subroutine send_real_array (a, dest, tag)
    implicit none
    real, dimension (:), intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (a, size(a), mpireal, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_real_array

  subroutine send_real_array_2d (a, dest, tag)
    implicit none
    real, dimension (:,:), intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (a, size(a), MPI_DOUBLE_PRECISION, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_real_array_2d

  subroutine send_complex (z, dest, tag)
    implicit none
    complex, intent (in) :: z
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (z, 1, mpicmplx, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_complex

  subroutine send_complex_array (z, dest, tag)
    implicit none
    complex, dimension (:), intent (in) :: z
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (z, size(z), mpicmplx, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_complex_array

  subroutine send_logical (f, dest, tag)
    implicit none
    logical, intent (in) :: f
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (f, 1, MPI_LOGICAL, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_logical

  subroutine send_logical_array (f, dest, tag)
    implicit none
    logical, dimension (:), intent (in) :: f
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send (f, size(f), MPI_LOGICAL, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_logical_array

  subroutine send_character (s, dest, tag)
    implicit none
    character(*), intent (in) :: s
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_send &
         (s, len(s), MPI_CHARACTER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine send_character

! ********************* synchronous sends **********************

  subroutine ssend_integer (i, dest, tag)
    implicit none
    integer, intent (in) :: i
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (i, 1, MPI_INTEGER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_integer

  subroutine ssend_integer_array (i, dest, tag)
    implicit none
    integer, dimension (:), intent (in) :: i
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (i, size(i), MPI_INTEGER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_integer_array

  subroutine ssend_real (a, dest, tag)
    implicit none
    real, intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (a, 1, MPI_DOUBLE_PRECISION, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_real

  subroutine ssend_real_array (a, dest, tag)
    implicit none
    real, dimension (:), intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (a, size(a), MPI_DOUBLE_PRECISION, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_real_array

  subroutine ssend_real_array_2d (a, dest, tag)
    implicit none
    real, dimension (:,:), intent (in) :: a
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (a, size(a), MPI_DOUBLE_PRECISION, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_real_array_2d

  subroutine ssend_complex (z, dest, tag)
    implicit none
    complex, intent (in) :: z
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (z, 1, MPI_DOUBLE_COMPLEX, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_complex

  subroutine ssend_complex_array (z, dest, tag)
    implicit none
    complex, dimension (:), intent (in) :: z
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (z, size(z), MPI_DOUBLE_COMPLEX, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_complex_array

  subroutine ssend_logical (f, dest, tag)
    implicit none
    logical, intent (in) :: f
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (f, 1, MPI_LOGICAL, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_logical

  subroutine ssend_logical_array (f, dest, tag)
    implicit none
    logical, dimension (:), intent (in) :: f
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend (f, size(f), MPI_LOGICAL, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_logical_array

  subroutine ssend_character (s, dest, tag)
    implicit none
    character(*), intent (in) :: s
    integer, intent (in) :: dest
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_ssend &
         (s, len(s), MPI_CHARACTER, dest, tagp, communicator, ierror)
# else
    call error ("send")
# endif
  end subroutine ssend_character

! ********************* receives  **********************

  subroutine receive_integer (i, src, tag)
    implicit none
# ifdef MPI
    integer, intent (out) :: i
# else
    integer :: i
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (i, 1, MPI_INTEGER, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_integer

  subroutine receive_integer_array (i, src, tag)
    implicit none
# ifdef MPI
    integer, dimension (:), intent (out) :: i
# else
    integer, dimension (:) :: i
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (i, size(i), MPI_INTEGER, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_integer_array

  subroutine receive_real (a, src, tag)
    implicit none
# ifdef MPI
    real, intent (out) :: a
# else
    real :: a
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (a, 1, mpireal, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_real

  subroutine receive_real_array (a, src, tag)
    implicit none
# ifdef MPI
    real, dimension (:), intent (out) :: a
# else
    real, dimension (:) :: a
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (a, size(a), mpireal, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_real_array

  subroutine receive_real_array_2d (a, src, tag)
    implicit none
# ifdef MPI
    real, dimension (:,:), intent (out) :: a
# else
    real, dimension (:,:) :: a
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (a, size(a), mpireal, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_real_array_2d

  subroutine receive_complex (z, src, tag)
    implicit none
# ifdef MPI
    complex, intent (out) :: z
# else
    complex :: z
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (z, 1, mpicmplx, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_complex

  subroutine receive_complex_array (z, src, tag)
    implicit none
# ifdef MPI
    complex, dimension (:), intent (out) :: z
# else
    complex, dimension (:) :: z
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (z, size(z), mpicmplx, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_complex_array

  subroutine receive_logical (f, src, tag)
    implicit none
# ifdef MPI
    logical, intent (out) :: f
# else
    logical :: f
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (f, 1, MPI_LOGICAL, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_logical

  subroutine receive_logical_array (f, src, tag)
    implicit none
# ifdef MPI
    logical, dimension (:), intent (out) :: f
# else
    logical, dimension (:) :: f
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (f, size(f), MPI_LOGICAL, src, tagp, communicator, &
        status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_logical_array

  subroutine receive_character (s, src, tag)
    implicit none
# ifdef MPI
    character(*), intent (out) :: s
# else
    character(*) :: s
# endif
    integer, intent (in) :: src
    integer, intent (in), optional :: tag
# ifdef MPI
    integer :: ierror
    integer :: tagp
    integer, dimension (MPI_STATUS_SIZE) :: status
    tagp = 0
    if (present(tag)) tagp = tag
    call mpi_recv (s, len(s), MPI_CHARACTER, src, tagp, communicator, &
         status, ierror)
# else
    call error ("receive")
# endif
  end subroutine receive_character

  subroutine job_fork (njobs, groups, load_balance, available, reserved)

    implicit none

    integer, intent (in) :: njobs
    integer, dimension(njobs) :: groups
    logical, intent (in) :: load_balance
    logical, optional, intent (inout) :: available, reserved

    if (nproc < njobs) then
       if (proc0) then
          write (*,*) 
          write (*,*) 'Number of jobs = ',njobs,' and number of processors = ',nproc
          write (*,*) 'Number of processors must not be less than the number of jobs'
          write (*,*) 'Stopping'
          write (*,*) 
       end if
       call finish_mp
       stop 1
    end if
       
    if (mod(nproc, njobs) /= 0 .and. .not. load_balance) then
       if (proc0) then
          write (*,*) 
          write (*,*) 'Number of jobs = ',njobs,' and number of processors = ',nproc
          write (*,*) 'Number of jobs must evenly divide the number of processors.'
          write (*,*) 'Stopping'
          write (*,*) 
       end if
       call finish_mp
       stop 1
    end if
       
    if (load_balance) then
       if(present(available)) then   ! implies dynamic load balancing
          call reassign_jobs(njobs, groups, available, reserved)
       else
          call assign_jobs (njobs, groups) ! static load balancing
       endif
       call init_jobs (njobs, groups) ! MPI split
    else    
       call init_jobs (njobs) ! Cart create
    endif
    
    call scope (allprocs)
       
  end subroutine job_fork

  ! Called to work out colour and key of each processor on the first run
  subroutine assign_jobs(njobs, groups)
    integer, intent(in) :: njobs
    integer, dimension(njobs) :: groups
    real, dimension(njobs) :: group_weights
    integer :: i, ierr, spare_procs, val_to_increase

    gproc = 0
    job = 0

    if(minval(groups) .le. 0) then
       if(proc0) write(6,*) 'Groups contains zero or negative values, exiting.'
       ierr = 1
       return
    endif
    
    ! Check the number of assigned jobs and assign weights
    if (sum(groups) .ne. ntot_proc) then
       if(proc0) write(6,*) 'Number of processes in flux_groups differs from the total available'
       if(proc0) write(6,*) 'Treating as weights and recalculating'
       if(proc0) write(6,'(A,I5,A,I5)') ' sum(groups) = ',sum(groups),', number of processors = ',ntot_proc
       group_weights = real(groups) / real(sum(groups))
       groups = group_weights * ntot_proc
    endif

    ! Use up any unused procs
    val_to_increase = 0
    do while (sum(groups) .lt. ntot_proc .and. val_to_increase .lt. ntot_proc)
       spare_procs = ntot_proc - sum(groups)
       ! First check for zeroes, then single procs, etc
       do i=1,njobs
          if(groups(i) .eq. val_to_increase .and. spare_procs .gt. 0) then
             groups(i) = groups(i) + 1
             spare_procs = spare_procs - 1
          endif
       enddo
       val_to_increase = val_to_increase + 1
    enddo

    ! Check for any persistent zeroes
    do while (minval(groups) .eq. 0) 
       do i=1,njobs
          if(groups(i) .eq. 0) then
             if(proc0) write(6,'(A,I5,A,I4)') 'Moving proc from group ',i,' to group ',maxloc(groups)
             groups(i) = 1
             groups(maxloc(groups)) = groups(maxloc(groups)) - 1
          endif
       enddo       
    enddo

    ! Assign the colour and key (group and rank)
    do i = 0, aproc - 1
       gproc = gproc + 1
       if(gproc .eq. groups(job+1)) then
          job = job + 1
          gproc = 0
       endif
    enddo

    !if(proc0) write(6,'(A,100I5)') ' Assigned jobs:',(groups(i),i=1,njobs)
    !write (*,*) 'Assigned', job, aproc

  end subroutine assign_jobs

! Re-assigns jobs that have finished. Proc0 is kept the same for all groups.
! i.e. grp0 will not change in the subsequent call to init_jobs
  subroutine reassign_jobs(njobs, groups, available, reserved)
    
    logical, intent(inout) :: available ! If true can reassigned to a new group
    logical, intent(inout) :: reserved ! If true cannot be reassigned to a new group
    integer, intent(in) :: njobs
    integer, dimension(0:njobs-1)  :: groups
    logical, dimension(0:ntot_proc-1) :: availability
    logical, dimension(0:njobs-1)     :: jobs_reserved
    integer :: i = 0, myjob, ierr

    ! All procs gather information from all other procs and
    ! work out their group and rank using the same algorithm
    availability = .false.    

!    if(proc0) write(6,'(A,100I5)') ' Input groups array:',(groups(i),i=0,njobs-1)

    ! proc0 from each group tells all other procs whether it has finished
    call scope(allprocs)
    call group_to_all_logical(reserved, jobs_reserved, njobs)

    ! Broadcast the jobs_reserved array
    call broadcast(jobs_reserved)    
    
    if(all(jobs_reserved)) then
       available = .true.    ! All the processes have finished
       reserved = .false.
       call assign_jobs(njobs, groups)  ! Assign using the original groups
       return
    endif

    ! Set reserved groups to 1 proc
    do i = 0, njobs - 1
       if(jobs_reserved(i)) groups(i) = 1
    enddo

    ! Gather the availability of all processors
    call MPI_Allgather(available, 1, MPI_LOGICAL, availability, 1, MPI_LOGICAL, communicator, ierr)

    !if (proc0) write (*,*) 'availability', availability
    !if (proc0) write (*,*) 'reserved', jobs_reserved

    ! Go through the procs and add any available ones to an unfinished job
    ! the jobs array is then updated so every processor has the full, updated jobs array
!    myjob = maxloc(groups,1) ! Start after the current largest allocation    
    myjob = 0
    do i = 0, ntot_proc-1
       !if (proc0) write (*,*) 'groups', i, availability(i),  groups
       if(availability(i)) then ! This processor is available, find a job for it
          if(myjob .gt. njobs - 1) myjob = 0 ! Added 1 to all the groups, start again
          do while(jobs_reserved(myjob)) ! Find the next job still running
             myjob = myjob + 1
             if(myjob .gt. njobs - 1) myjob = 0
          enddo
          if(i .eq. iproc) then ! This is this processor so assign job and rank
             job = myjob
             gproc = groups(myjob)
             available = .false.
          endif          
          groups(myjob) = groups(myjob) + 1
          myjob = myjob + 1 ! Move to the next group for the next free proc
       endif
    enddo
    
    !if(proc0) write(6,'(A,100I5)') 'Reassigned jobs:',(groups(i),i=0,njobs-1)
    

    ! Free the existing group communicators
    if(comm_group .ne. MPI_COMM_NULL) Call MPI_Comm_free(comm_group,ierr)
    if(ierr/=0) then
       write(*,*) "Error freeing old communicator"
       call abort_mp
    endif

  end subroutine reassign_jobs


! Performs a cartesian split on the available processors assigning the same 
! number of processes to each job for flux calculations
  subroutine init_jobs_cart (ncolumns)

    implicit none  
# ifdef MPI
!    integer, parameter :: reorder=1
    ! TT: I changed variable definition by assuming integer 1 corresponds to
    ! TT: logical .true. but I am not sure if reorder is needed.
    logical, parameter :: reorder=.true.
    integer :: ip, j, comm2d, id2d, ierr, nrows
# endif
    integer, intent (in) :: ncolumns
# ifndef MPI
    integer :: ierr
    if (ncolumns /= 1) call error ("jobs")
# else
    integer, parameter :: ndim=2
    integer, dimension(ndim) :: dims
    integer, dimension(0:ndim-1) :: coords1d, coords2d
    logical, dimension(0:ndim-1) :: belongs
    logical, dimension(ndim) :: period
    
    logical :: isroot

    if (.not. allocated(grp0)) allocate (grp0(0:ncolumns-1))
    
! calculate dimensions  mpi processor grid will have and check that 
! ncolumns*nrows = number of processes

! nrows is # of processors per job (or group)    
    nrows = ntot_proc/ncolumns
    dims=(/ ncolumns, nrows /)     
    if(ntot_proc /= ncolumns*nrows) then
       ierr = 1
       if(aproc0) write(*,*) 'Number of processes must be divisible by number of groups'
       return
    endif
    ngroup_proc = nrows
    
    ! create 2d cartesian topology for processes
    
    period=(/ .false., .false. /)  !! no circular shift

    call mpi_cart_create(mpcom, ndim, dims, period, reorder, comm2d, ierr)
    call mpi_comm_rank(comm2d, id2d, ierr)
    call mpi_cart_coords(comm2d, id2d, ndim, coords2d, ierr)
    
! each processor knows which subgrid it is in from variable mpi_group
    job = coords2d(0)

! create 1d subgrids from 2d processor grid, variable belongs denotes
! whether processor grid is split by column or row

    belongs(1) = .true.    ! this dimension belongs to subgrid
    belongs(0) = .false.  

    call mpi_cart_sub(comm2d, belongs, comm_group, ierr)
    call mpi_comm_rank(comm_group, gproc, ierr)     
    call mpi_cart_coords(comm_group, gproc, 1, coords1d, ierr)
    gproc0 = (gproc == 0)
    
! find root process of each 1d subgrid and place in array grp0 indexed 
! from 0 to subgrids-1
     
    j = 0
    if (proc0 .and. gproc0) then
       grp0(0) = 0
       j = 1
    end if

    do ip = 1, ntot_proc-1
       if (proc0) then
          call receive (isroot, ip)
          if (isroot) then
             grp0(j) = ip
             j=j+1
          end if
       else if (ip == aproc) then
          call send (gproc0, 0)
       end if
       call barrier
    end do

!let all processors have the grp0 array
    call broadcast (grp0)

    call scope (subprocs)

# endif
  end subroutine init_jobs_cart

  subroutine init_jobs_split (njobs, groups)

    implicit none  
# ifdef MPI
    integer :: i, ip, j, rem, colour, ierr
# endif
    integer, intent (in) :: njobs
    integer, intent (in) :: groups(njobs)
# ifndef MPI
    integer :: ierr
    if (njobs /= 1) call error ("jobs")
# else

    logical :: isroot

! Check total number of procs against number of procs in flux_groups namelist variable array    
    if (ntot_proc < sum(groups)) then
       if(aproc0) write(*,*) 'Requested number of processors in groups larger than available'
       call abort_mp
    endif

! Check that the number of jobs is equal to the number of values in groups
    if (njobs /= size(groups)) then
       if(aproc0) write(*,*) 'Requested number of jobs does not equal the number of groups in namelist'
       call abort_mp
    endif       

    if (.not. allocated(grp0)) allocate (grp0(0:njobs-1))

    gproc0 = (gproc == 0)

! Split the MPI_World_Communicator into the individual groups
! The colour and key, job and gproc have already been assigned in either assign or reassign
    
    Call MPI_Comm_split(MPI_Comm_World, job, gproc, comm_group, ierr)

    if(ierr/=0) then
       write(*,*) "Error creating groups for flux calculation"
       call abort_mp
    endif

! find root process of each 1d subgrid and place in array grp0 indexed 
! from 0 to subgrids-1
     
    j = 0
    if (proc0 .and. gproc0) then
       grp0(0) = 0
       j = 1
    end if

    do ip = 1, ntot_proc-1
       if (proc0) then
          call receive (isroot, ip)
          if (isroot) then
             grp0(j) = ip
             j=j+1
          end if
       else if (ip == aproc) then
          call send (gproc0, 0)
       end if
       call barrier
    end do

!let all processors have the grp0 array
    call broadcast (grp0)

    !if(proc0) write(6,*) 'grp0 = ',grp0

    call scope (subprocs)

# endif
  end subroutine init_jobs_split

  subroutine all_to_group_real (all, group, njobs)
    
    implicit none

    real, dimension (:), intent (in) :: all
    real, intent (out) :: group
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("all_to_group")
# else
    tag = 1000
    do ik = 0, njobs-1
       if (proc0) then
          idx = mod(ik,size(all))
          if (iproc == grp0(ik)) then
             group = all(idx+1)
          else
# ifdef USE_SEND
             call send (all(idx+1), grp0(ik), tag)
# else
             call ssend (all(idx+1), grp0(ik), tag)
# endif
          end if
       else if (iproc == grp0(ik)) then
          call receive (group, 0, tag)
       end if
    end do
# endif
  end subroutine all_to_group_real

  subroutine all_to_group_real_array (all, group, njobs)
    
    implicit none

    real, dimension (:,:), intent (in) :: all
    real, dimension (:), intent (out) :: group
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error ("all_to_group")
# else
    tag = 1001
    do ik = 0, njobs-1
       if (proc0) then
          idx = mod(ik,size(all,dim=1))
          if (iproc == grp0(ik)) then
             group = all(idx+1,:)
          else
# ifdef USE_SEND
             call send (all(idx+1,:), grp0(ik), tag)
# else
             call ssend (all(idx+1,:), grp0(ik), tag)
# endif
          end if
       else if (iproc == grp0(ik)) then
          call receive (group, 0, tag)
       end if
    end do

# endif
  end subroutine all_to_group_real_array

  subroutine all_to_group_real_global (all, group, njobs)
    
    implicit none

    real, dimension (:), intent (in) :: all
    real, dimension (:), intent (out) :: group
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("all_to_group")
# else
    tag = 1004
    do ik = 0, njobs-1
       if (proc0) then
!          if (size(group)==1) then
!             idx = mod(ik,size(all))
!          else
          idx = mod(ik*size(group),size(all))
!          end if
          if (iproc == grp0(ik)) then
             group = all(idx+1:idx+size(group))
          else
# ifdef USE_SEND
             call send (all(idx+1:idx+size(group)), grp0(ik), tag)
# else
             call ssend (all(idx+1:idx+size(group)), grp0(ik), tag)
# endif
          end if
       else if (iproc == grp0(ik)) then
          call receive (group, 0, tag)
       end if
    end do
# endif
  end subroutine all_to_group_real_global

  subroutine all_to_group_real_global_array (all, group, njobs)
    
    implicit none

    real, dimension (:,:), intent (in) :: all
    real, dimension (:,:), intent (out) :: group
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error ("all_to_group")
# else
    tag = 1005
    do ik = 0, njobs-1
       if (proc0) then
!          if (size(group,dim=1)==1) then
!             idx = mod(ik,size(all,dim=1))
!          else
          idx = mod(ik*size(group,dim=1),size(all,dim=1))
!             idx = ik
!          end if
          if (iproc == grp0(ik)) then
             group = all(idx+1:idx+size(group,dim=1),:)
          else
# ifdef USE_SEND
             call send (all(idx+1:idx+size(group,dim=1),:), grp0(ik), tag)
# else
             call ssend (all(idx+1:idx+size(group,dim=1),:), grp0(ik), tag)
# endif
          end if
       else if (iproc == grp0(ik)) then
          call receive (group, 0, tag)
       end if
    end do

# endif
  end subroutine all_to_group_real_global_array

  subroutine group_to_all_real (group, all, njobs)
    
    implicit none

    real, intent (in) :: group
    real, dimension (:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1002
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
             idx = mod(ik,size(all))
             all(idx+1) = group
          end if
       else if (proc0) then
          idx = mod(ik,size(all))
          call receive (all(idx+1), grp0(ik), tag)
       end if
    end do

# endif    
  end subroutine group_to_all_real

  subroutine group_to_all_real_array (group, all, njobs)
    
    implicit none

    real, dimension (:), intent (in) :: group
    real, dimension (:,:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1003
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
             idx = mod(ik,size(all,dim=1))
             all(idx+1,:) = group
          end if
       else if (proc0) then
          idx = mod(ik,size(all,dim=1))
          call receive (all(idx+1,:), grp0(ik), tag)
       end if
    end do
    
# endif
  end subroutine group_to_all_real_array

  subroutine group_to_all_real_global (group, all, njobs)
    
    implicit none

    real, dimension (:), intent (in) :: group
    real, dimension (:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1006
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
!             if (size(group)==1) then
!                idx = mod(ik,size(all))
!             else
             idx = mod(ik*size(group),size(all))
!                idx = ik
!             end if
             all(idx+1:idx+size(group)) = group
          end if
       else if (proc0) then
!          if (size(group)==1) then
!             idx = mod(ik,size(all))
!          else
          idx = mod(ik*size(group),size(all))
!             idx = ik
!          end if
          call receive (all(idx+1:idx+size(group)), grp0(ik), tag)
       end if
    end do

# endif    
  end subroutine group_to_all_real_global

  subroutine group_to_all_real_global_array (group, all, njobs)
    
    implicit none

    real, dimension (:,:), intent (in) :: group
    real, dimension (:,:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1007
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
!             if (size(group,dim=1)==1) then
!                idx = mod(ik,size(all,dim=1))
!             else
             idx = mod(ik*size(group,dim=1),size(all,dim=1))
!                idx =ik
!             end if
             all(idx+1:idx+size(group,dim=1),:) = group
          end if
       else if (proc0) then
!          if (size(group,dim=1)==1) then
!             idx = mod(ik,size(all,dim=1))
!          else
          idx = mod(ik*size(group,dim=1),size(all,dim=1))
!             idx = ik
!          end if
          call receive (all(idx+1:idx+size(group,dim=1),:), grp0(ik), tag)
       end if
    end do
    
# endif
  end subroutine group_to_all_real_global_array


  subroutine group_to_all_logical (group, all, njobs)
    
    implicit none

    logical, intent (in) :: group
    logical, dimension (:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ij, ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1002
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
             idx = mod(ik,size(all))
             all(idx+1) = group
          end if
       else if (proc0) then
          idx = mod(ik,size(all))
          call receive (all(idx+1), grp0(ik), tag)
       end if
    end do

# endif    
  end subroutine group_to_all_logical

  subroutine group_to_all_logical_global (group, all, njobs)
    
    implicit none

    logical, dimension (:), intent (in) :: group
    logical, dimension (:), intent (out) :: all
    integer, intent (in) :: njobs

    integer :: ij, ik, tag, idx

# ifndef MPI
    call error("group_to_all")
# else
    tag = 1006
    do ik = 0, njobs-1
       if (iproc == grp0(ik)) then
          if (.not. proc0) then
# ifdef USE_SEND
             call send (group, 0, tag)
# else
             call ssend (group, 0, tag)
# endif
          else
!             if (size(group)==1) then
!                idx = mod(ik,size(all))
!             else
             idx = mod(ik*size(group),size(all))
!                idx = ik
!             end if
             all(idx+1:idx+size(group)) = group
          end if
       else if (proc0) then
!          if (size(group)==1) then
!             idx = mod(ik,size(all))
!          else
          idx = mod(ik*size(group),size(all))
!             idx = ik
!          end if
          call receive (all(idx+1:idx+size(group)), grp0(ik), tag)
       end if
    end do

# endif    
  end subroutine group_to_all_logical_global

  subroutine sum_allreduce_real_3array (a)
    implicit none
    real, dimension (:,:,:), intent (in out) :: a
# ifdef MPI
    integer :: ierror
    !write (*,*) 'Calling sum_allreduce_real_3array ', iproc
    call mpi_allreduce &
         (MPI_IN_PLACE, a, size(a), MPI_DOUBLE_PRECISION, MPI_SUM, comm_all, ierror)
    !write (*,*) 'Finished sum_allreduce_real_3array ', iproc
# endif
  end subroutine sum_allreduce_real_3array

  subroutine sum_allreduce_real_2array (a)
    implicit none
    real, dimension (:,:), intent (in out) :: a
# ifdef MPI
    integer :: ierror
    !write (*,*) 'Calling sum_allreduce_real_3array ', iproc
    call mpi_allreduce &
         (MPI_IN_PLACE, a, size(a), MPI_DOUBLE_PRECISION, MPI_SUM, comm_all, ierror)
    !write (*,*) 'Finished sum_allreduce_real_3array ', iproc
# endif
  end subroutine sum_allreduce_real_2array

  function timer_local()
!THIS SHOULD BE PROVIDED BY THE MP MODULE INSTEAD
# ifdef MPI
# ifndef MPIINC
    use mpi, only: mpi_wtime
# else
    ! this may cause malfunction of timer if double precision is promoted to quad.
    include "mpif.h" ! CMR following Michele Weiland's advice
# endif

# endif
    real :: timer_local

    timer_local=0.

# if defined MPI && !defined MPIINC
    timer_local=mpi_wtime()
# else
    ! this routine is F95 standard
    call cpu_time(timer_local)
# endif
  end function timer_local

# ifndef MPI
  subroutine error (msg)
    implicit none
    character(*), intent (in) :: msg

    print *, "mp error: "//msg
    call coredump
    stop
  end subroutine error

  subroutine coredump
    real, dimension (:), allocatable :: a
    deallocate (a)
  end subroutine coredump
# endif

end module mp_trin
