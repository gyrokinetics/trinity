#include <stdlib.h>
#include <string.h>
#include <mpi.h>
extern "C" void run_trinity_c(char * input_file, int mp_communicator);
int main(int argc, char * argv[]){
	char * longname;
	int mpcom;
  printf("Trinity/CUDA is starting\n");
  if (argc<2) {
    printf("ERROR: Please supply an input file\n");
    return 1;
  }
	longname = (char *)malloc(sizeof(char)*(strlen(argv[1])+1000));
	strcpy(longname, argv[1]);
  printf("Trinity/CUDA input file is %s\n", longname);


	MPI_Init(&argc, &argv);
	mpcom = MPI_Comm_c2f(MPI_COMM_WORLD);
	run_trinity_c(longname, mpcom);

  free(longname);
  return 0;

}
