!> Contains functions for writing text-file-based
!! output.
module trindiag_text_write
  use trinity_type_module, only: trinity_clock_type
  use save_arrays, only: save_arrays_type
  implicit none

contains
  subroutine read_save_arrays(sayv)
    use trinity_input, only: iternt_file, iterflx_file
    use trinity_input, only: nprofs_evolved, nrad
    use file_utils_trin, only: get_unused_unit
    use mp_trin, only: abort_mp
    type(save_arrays_type), intent(inout) :: sayv
    integer :: iternt_unit, iterflx_unit
    integer :: np, ierr, ix, ig, is
    ! if restarting, need to read in
    ! sayv%nt, sayv%ntold, sayv%rhs, and rmserr
    np = nprofs_evolved
    call get_unused_unit (iternt_unit)
    open (unit=iternt_unit, file=trim(iternt_file), status="old", &
      action="read",iostat=ierr)
    if (ierr.ne.0) then
      write(*,'(A,A)') "I/O ERROR: could not open ", trim(iternt_file)
      call abort_mp
    endif
    do ix = 1, np*nrad
      read (iternt_unit,*) sayv%nt(ix), sayv%ntold(ix), sayv%rhs(ix), sayv%rmserr
    end do
    close (iternt_unit)
    ! read in fluxes from previous iterations to use when
    ! restarting in middle of transport time step
    call get_unused_unit (iterflx_unit)
    open (unit=iterflx_unit, file=trim(iterflx_file), status="old", &
      action="read",iostat=ierr)
    if (ierr.ne.0) then
      write(*,'(A,A)') "I/O ERROR: could not open ", trim(iterflx_file)
      call abort_mp
    endif
    do ig = 1, size(sayv%pflx,3)
      do is = 1, size(sayv%pflx,2)
        do ix = 1, size(sayv%pflx,1)
          read (iterflx_unit,*) sayv%pflx(ix,is,ig), sayv%pflxold(ix,is,ig), &
            sayv%qflx(ix,is,ig), sayv%qflxold(ix,is,ig), sayv%heat(ix,is,ig), &
            sayv%heatold(ix,is,ig), sayv%lflx(ix,ig), sayv%lflxold(ix,ig)
        end do
      end do
    end do
    close (iterflx_unit)
  end subroutine read_save_arrays

  subroutine write_save_arrays(sayv)
    use trinity_input, only: iternt_file, iterflx_file
    use trinity_input, only: nprofs_evolved, nrad
    use file_utils_trin, only: get_unused_unit
    use file_utils_trin, only: open_output_file, close_output_file
    type(save_arrays_type), intent(inout) :: sayv
    integer :: iternt_unit, iterflx_unit
    integer :: np, ix, ig, is
    ! if restarting, need to read in
    ! sayv%nt, sayv%ntold, sayv%rhs, and rmserr
    np = nprofs_evolved
    call get_unused_unit (iternt_unit)
    call get_unused_unit (iterflx_unit)
    call open_output_file (iternt_unit,".iternt")
    do ix = 1, size(sayv%nt)
      write (iternt_unit,fmt='(4e18.11)') sayv%nt(ix), sayv%ntold(ix), sayv%rhs(ix), sayv%rmserr
    end do
    call close_output_file (iternt_unit)
    ! write fluxes from iterations in this time step to file
    ! in case need to restart from middle of time step
    call open_output_file (iterflx_unit,".iterflx")
    do ig = 1, size(sayv%pflx,3)
      do is = 1, size(sayv%pflx,2)
        do ix = 1, size(sayv%pflx,1)
          write (iterflx_unit,fmt='(8e14.5)') sayv%pflx(ix,is,ig), sayv%pflxold(ix,is,ig), &
            sayv%qflx(ix,is,ig), sayv%qflxold(ix,is,ig), &
            sayv%heat(ix,is,ig), sayv%heatold(ix,is,ig), &
            sayv%lflx(ix,ig), sayv%lflxold(ix,ig)
        end do
      end do
    end do
    call close_output_file (iterflx_unit)


  end subroutine write_save_arrays

  subroutine write_profiles (clock, flx, finish)

    use file_utils_trin, only: open_output_file, flush_output_file, close_output_file
    use trinity_input, only: nrad, ncc, ntstep, rgeo, nspec, include_neo, vtfac
    use trinity_input, only: njac
    use trinity_input, only: m_ion
    use nteqns_arrays, only: rad_grid, dens_grid, pres_grid, bmag_grid, qval_grid, shat_grid
    use nteqns_arrays, only: rad_cc, qval_cc, shat_cc, kappa_cc, kapprim_cc, delta_cc, deltprim_cc, shift_cc
    use nteqns_arrays, only: rhotor_cc, bmag_cc, grho_cc, area_cc
    use nteqns_arrays, only: temp_grid, nu_grid, kappa_grid, delta_grid, shift_grid, rmajor_grid
    use nteqns, only: volint
    use nteqns_arrays, only: chii_exp_cc, chie_exp_cc
    use norms, only: rhostar
    use nteqns_arrays, only: kapprim_grid, deltprim_grid, rhotor_grid
    use nteqns_arrays, only: net_power, aux_power, alph_power, radiate_power
    use norms, only:  aref
    use nteqns_arrays, only: area_grid, grho_grid
    use nteqns_arrays, only: dens_cc, pres_cc, fprim_cc, tprim_cc, gexb_cc, bmag_cc, temp_cc
    use nteqns_arrays, only: mom_grid, omega_grid
    use nteqns_arrays, only: int_pflux, int_lflux, int_power, int_equil
    use interp, only: spline, get_gridval
    use flux_results, only: flux_results_type

    implicit none


    type(flux_results_type), intent(inout) :: flx
    integer :: ix, is, igr
    integer, save :: nt_unit, geo_unit, info_unit, plot_unit, pbalance_unit, neo_unit
    integer, save :: cc_unit, flx_unit, phys_unit

    real :: stored_energy, boundary_energy
    real, save :: stored_energy_exp, boundary_energy_exp
    real, dimension (nrad) :: tmp
    real, dimension (ncc, nspec) :: chi
    real, dimension (:,:), allocatable :: pflx_bal, qflx_bal, pflx_grid, qflx_grid
    real, dimension (:), allocatable :: lflx_bal, lflx_grid

    character (3) :: nspec_str
    character (100) :: str, str_nt

    type(trinity_clock_type), intent(inout), target :: clock
    logical, pointer :: firstwrite
    integer, pointer :: nflxeval
    real, pointer :: nttime, seconds
    logical, intent (in) :: finish

    nflxeval => clock%neval
    nttime => clock%time
    seconds => clock%seconds
    firstwrite => clock%first_write

    allocate (pflx_grid(nrad,nspec))
    allocate (qflx_grid(nrad,nspec))
    allocate (lflx_grid(nrad))
    allocate (pflx_bal(ncc,nspec))
    allocate (qflx_bal(ncc,nspec))
    allocate (lflx_bal(ncc))

    do is = 1, nspec
       call get_gridval (flx%p_cc(:,is,1), pflx_grid(:,is))
       call get_gridval (flx%q_cc(:,is,1), qflx_grid(:,is))
    end do
    call get_gridval (flx%l_cc(:,1), lflx_grid)

    do is = 1, nspec
       where (abs(tprim_cc(:,is)) > epsilon(0.0))
          chi(:,is) = rgeo*4.38e5*sqrt(m_ion(1))*(pres_cc(:,2)/dens_cc(:,2))**1.5/bmag_cc**2*rhostar**2 &
               *pres_cc(:,2)/pres_cc(:,is)*flx%q_cc(:,is,1)/tprim_cc(:,is)
       elsewhere
          chi(:,is) = 0.0
       end where
    end do


    if (firstwrite) then

       write (nspec_str,'(i3)') 4+nspec*3
       str_nt = trim('(1(1x,1e19.12),'//trim(nspec_str)//'(1x,1e11.4),i9)')

       call open_output_file (geo_unit,".geo")
       write (geo_unit,201) "# 1: rhoc", "2: qinp", "3: shat", "4: kappa", "5: kapprim", &
            "6: tri", "7: triprim", "8: bmag", "9: rho*", "10: shift", "11: rhotor", "12: grho", &
            "13: area", "14: aref"
       ! write geometric quantities at both grid points and cell centers
       do ix = 1, ncc
          write (geo_unit,202) rad_grid(ix), qval_grid(ix), shat_grid(ix), kappa_grid(ix), kapprim_grid(ix), &
               delta_grid(ix), deltprim_grid(ix), bmag_grid(ix), rhostar, shift_grid(ix), rhotor_grid(ix), &
               grho_grid(ix), area_grid(ix), aref
          write (geo_unit,202) rad_cc(ix), qval_cc(ix), shat_cc(ix), kappa_cc(ix), kapprim_cc(ix), &
               delta_cc(ix), deltprim_cc(ix), bmag_cc(ix), rhostar, shift_cc(ix), rhotor_cc(ix), &
               grho_cc(ix), area_cc(ix), aref
       end do
       ix = nrad
       write (geo_unit,202) rad_grid(ix), qval_grid(ix), shat_grid(ix), kappa_grid(ix), kapprim_grid(ix), &
            delta_grid(ix), deltprim_grid(ix), bmag_grid(ix), rhostar, shift_grid(ix), rhotor_grid(ix), &
            grho_grid(ix), area_grid(ix), aref
       call close_output_file (geo_unit)

       call open_output_file (pbalance_unit,".pbalance")
       write (pbalance_unit,'(11a12)') '# 1: time', '2: radius', '3: area', '4: pflx', '5: qflxi', '6: qflxe', &
            '7: lflx', '8: part src', '9: i+ pwr', '10: e- pwr', '11: torque'

       if (include_neo) then
          call open_output_file (neo_unit,".neo")
          write (neo_unit,'(8a13)') '# 1: time', '2: radius', '3: neo qflxi', '4: neo qflxe', &
               '5: neo lflx', '6: neo chii', '7: neo chie', '8: neo nui'
       end if

       call open_output_file (nt_unit,".nt")
       write (nt_unit,'(11a12,a10)') '# 1: time', '2: radius', '3: rmajor', '4: e- dens', '5: i+ dens', &
            '6: e- temp', '7: i+ temp', '8: ang mom', '9: e- vnewk', '10: i+ vnewk', '11: time(s)', '12: evals'

       call open_output_file (cc_unit,".cc")
       write (cc_unit,'(20a12)') '### 1: time', '2: radius', '3: e- pflx', '4: i+ pflx', '5: e- qflx', &
            '6: i+ qflx', '7: e- heat', '8: i+ heat', '9: vflx', '10: a/Lne', '11: a/Lni', &
            '12: e- a/LT', '13: i+ a/LT', '14: gexb', '15: chie', '16: chii', '17: chie_exp', '18: chii_exp', &
            '19: ne', '20: ni', '21: e- temp', '22: i+ temp'

       call open_output_file (flx_unit,".fluxes")
       write (flx_unit,'(9a15)') '# 1: time', '2: radius', '3: e- pflx', '4: i+ pflx', '5: Qe (GB)', &
            '6: Qi (GB)', '7: Qe (MW/m^2)', '8: Qi (MW/m^2)', '9: Pi (GB)'

       call open_output_file (phys_unit,".physunits")
       write (phys_unit,'(10a15)') '# 1: time', '2: radius', '3: Gamma_e', '4: Gamma_i', &
            '5: Q_e', '6: Q_i', '7: Gamma_e', '8: Gamma_i', '9: Q_e', '10: Q_i'
       write (phys_unit,'(10a15)') '# seconds', '', '10^20/m^2/s', '10^20/m^2/s', &
            'MW/m^2', 'MW/m^2', 'GB', 'GB', 'GB', 'GB'
       
       call open_output_file (plot_unit,".plot")
       do ix = 1, nrad
          write (plot_unit,str_nt) nttime, rad_grid(ix), rmajor_grid(ix), &
               dens_grid(ix,:), pres_grid(ix,:)/dens_grid(ix,:), mom_grid(ix), &
               nu_grid(ix,:),seconds, nflxeval
       end do
       write (plot_unit,*) !using two blank lines allows usage of index option
       write (plot_unit,*) !in gnuplot (e.g., plot 'file' i 1 u 1:4 w lp)

       call open_output_file (info_unit,".info")
       call volint(aref**3*(pres_grid(:,1)+pres_grid(:,2)), stored_energy_exp)
       tmp = pres_grid(nrad,1)+pres_grid(nrad,2)
       call volint(aref**3*(tmp), boundary_energy_exp)

       firstwrite = .false.

    end if

    write (nspec_str,'(i3)') 4+nspec*3
    str_nt = trim('(1(1x,1e19.12),'//trim(nspec_str)//'(1x,1e11.4),i9)')
    do ix = 1, nrad
       write (nt_unit,str_nt) nttime, rad_grid(ix), rmajor_grid(ix), &
            dens_grid(ix,:), pres_grid(ix,:)/dens_grid(ix,:), mom_grid(ix), &
            nu_grid(ix,:), seconds, nflxeval
    end do
    write (nt_unit,*)
    call flush_output_file(nt_unit)

    write (nspec_str,'(i3)') 6+nspec*8
    str = trim('('//trim(nspec_str)//'(1x,1e11.4))')
    do ix = 1, ncc
       write (cc_unit,str) nttime, rad_cc(ix), flx%p_cc(ix,:,1), flx%q_cc(ix,:,1), &
            flx%heat_cc(ix,:,1), flx%l_cc(ix,1), fprim_cc(ix,:), tprim_cc(ix,:), gexb_cc(ix), &
            chi(ix,:), chie_exp_cc(ix), chii_exp_cc(ix), dens_cc(ix,:), temp_cc(ix,:)
    end do

    write (cc_unit,*)
    write (cc_unit,*)
    call flush_output_file(cc_unit)

    write (nspec_str,'(i3)') 2+nspec*4
    str = trim('(1(1x,e19.8),'//trim(nspec_str)//'(1x,1e14.4))')
    do igr = 1, njac
       do ix = 1, ncc
          write (flx_unit,str) nttime, rad_cc(ix), flx%p_cc(ix,:,igr), &
               flx%q_cc(ix,:,igr), flx%q_phys(ix,:), flx%l_cc(ix,1)
       end do
       write (flx_unit,*)
    end do
    write(flx_unit,*)
    call flush_output_file(flx_unit)

    write (nspec_str,'(i3)') 2+nspec*4
    str = trim('('//trim(nspec_str)//'(1x,1e14.4))')
    do ix = 1, ncc
       write (phys_unit,str) seconds, rad_cc(ix), flx%p_phys(ix,:), flx%q_phys(ix,:), flx%p_cc(ix,:,1), flx%q_cc(ix,:,1)
    end do
    write (phys_unit,*)
    write (phys_unit,*)
    call flush_output_file (phys_unit)

    if (include_neo) then
       write (nspec_str,'(i3)') 4+nspec*2
       str = trim('('//trim(nspec_str)//'(1x,1e12.4))')
       do ix = 1, ncc
          write (neo_unit,str) nttime, rad_cc(ix), flx%q_neo_cc(ix,:,1), &
               flx%l_neo_cc(ix,1), flx%q_neo_cc(ix,:,1)/tprim_cc(ix,:), &
               flx%l_neo_cc(ix,1)/(gexb_cc(ix)+epsilon(0.0))
       end do
       call flush_output_file(neo_unit)         
    end if

    do is = 1, nspec
       ! integrated particle flux in 10^20/s (see Eq. 17 of normalization document)
       pflx_bal(:,is) = 3.22*flx%p_cc(:,is,1)*dens_cc(:,2) &
            *(vtfac*pres_cc(:,2)/dens_cc(:,2))**1.5*sqrt(m_ion(1))*area_cc/bmag_cc**2
       ! integrated heat flux in MW
       qflx_bal(:,is) = 5.17655e-2*(flx%q_cc(:,is,1)+flx%q_neo_cc(:,is,1)) &
            *dens_cc(:,2)*vtfac**1.5*(pres_cc(:,2)/dens_cc(:,2))**2.5*sqrt(m_ion(1))*area_cc/bmag_cc**2
    end do
    lflx_bal = flx%l_cc(:,1)*dens_cc(:,1)*(pres_cc(:,2)/dens_cc(:,2))**2*sqrt(m_ion(1))*area_cc/bmag_cc**2/1.5

    write (nspec_str,'(i3)') 5+nspec*5
    str = trim('('//trim(nspec_str)//'(1x,1e11.4))')
    do ix = 1, ncc
       write (pbalance_unit,str) nttime, rad_grid(ix), area_grid(ix)*aref**2, &
            pflx_bal(ix,:), qflx_bal(ix,:), lflx_bal(ix), int_pflux(ix,:), &
            int_power(ix,:)+int_equil(ix,:), int_power(ix,:)+int_equil(ix,:), int_lflux(ix)
    end do
    write(pbalance_unit,*)
    write(pbalance_unit,*)
    call flush_output_file(pbalance_unit)
    
201 format (14A12)
202 format (14(1x,1e11.4))

    if (finish) then
       call close_output_file (pbalance_unit)
       call close_output_file (nt_unit)
       do ix = 1, nrad
          write (plot_unit,str_nt) nttime, rad_grid(ix), rmajor_grid(ix), &
               dens_grid(ix,:), pres_grid(ix,:)/dens_grid(ix,:), mom_grid(ix), &
               nu_grid(ix,:), seconds, nflxeval
       end do
       write (plot_unit,*)
       call close_output_file (plot_unit)
       write (info_unit,*) "Q: ", sum(alph_power)*17.6/(3.5*sum(aux_power))
       write (info_unit,*) "Fusion power: ", sum(alph_power)*17.6/3.5
       write (info_unit,*) "Net power: ", sum(net_power)
       write (info_unit,*) "Auxiliary power: ", sum(aux_power)
       write (info_unit,*) "Alpha power: ", sum(alph_power)
       write (info_unit,*) "Radiated power: ", sum(radiate_power)
       write (info_unit,*)       
       write (info_unit,*) "Core electron density: ", dens_grid(1,1)
       write (info_unit,*) "Edge electron density: ", dens_grid(nrad,1)
       write (info_unit,*) "Core T_i: ", temp_grid(1,2)
       write (info_unit,*) "Core T_e: ", temp_grid(1,1)
       write (info_unit,*) "Edge T_i: ", temp_grid(nrad,2)
       write (info_unit,*) "Edge T_e: ", temp_grid(nrad,1)
       write (info_unit,*) "Core omega: ", omega_grid(1)
       write (info_unit,*) "Edge omega: ", omega_grid(nrad)
       write (info_unit,*)
       call volint(aref**3*(pres_grid(:,1)+pres_grid(:,2)), stored_energy)
       tmp = pres_grid(nrad,1)+pres_grid(nrad,2)
       call volint(aref**3*(tmp), boundary_energy)
       write (info_unit,*) "Total stored thermal energy (MJ): ", 1.5*stored_energy*1.602e-2
       write (info_unit,*) "Total stored thermal energy (MJ) from initial condition: ", &
            1.5*stored_energy_exp*1.602e-2
       write (info_unit,*) "Incremental stored thermal energy (MJ): ", &
            1.5*(stored_energy-boundary_energy)*1.602e-2
       write (info_unit,*) "Incremental stored thermal energy (MJ) from initial condition: ", &
            1.5*(stored_energy_exp-boundary_energy_exp)*1.602e-2
       call close_output_file (info_unit)
    else
       call flush_output_file (nt_unit)
    end if

    deallocate (pflx_bal, qflx_bal, lflx_bal, pflx_grid, qflx_grid, lflx_grid)

  end subroutine write_profiles

  subroutine dump_tmp_profiles (clock)

    use file_utils_trin, only: open_output_file, flush_output_file, close_output_file
    use trinity_input, only: nrad, ntdelt
    use trinity_input, only: nspec
    use nteqns_arrays, only: dens_grid, pres_grid, mom_grid
    use trinity_time, only: ntdelt_old
    use nteqns_arrays, only: rmajor_grid

    implicit none

    type(trinity_clock_type), intent(inout), target :: clock

    integer :: ix, tmp_unit
    character (3) :: nspec_str
    character (100) :: str

    !integer, intent (in) :: nflxeval
    real :: nttime

    nttime = clock%time


    call open_output_file (tmp_unit,".tmp")

    write (nspec_str,'(i3)') 5+nspec*2
    str = trim('(2i5,'//trim(nspec_str)//'(1x,1pg18.11),i5)')
    do ix = 1, nrad
       write (tmp_unit,str) clock%itstep_tot, clock%iter, ntdelt_old, ntdelt, nttime, &
            rmajor_grid(ix), dens_grid(ix,:), pres_grid(ix,:)/dens_grid(ix,:), &
            mom_grid(ix), clock%neval
    end do
    call close_output_file (tmp_unit)

  end subroutine dump_tmp_profiles
end module trindiag_text_write
