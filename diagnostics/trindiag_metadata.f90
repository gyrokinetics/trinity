!> A module for for writing help, metadata and the input file 
!! to the netcdf file. 
!! 
!! To extract the input file use the bash utility scripts/extract_input_file
module trindiag_metadata
  use trindiag_config, only: trindiag_type
  implicit none
  character, dimension(100000) :: inputfile_array
  integer :: inputfile_length

contains
  subroutine write_metadata(gnostics)
    use simpledataio, only: add_metadata
    use simpledataio, only: add_standard_metadata
    !use runtime_tests, only: build_identifier
    !use runtime_tests, only: get_svn_rev
    !use run_parameters, only: user_comments
    type(trindiag_type), intent(in) :: gnostics
    character (20) :: datestamp, timestamp, timezone
    character (31) :: strtime
    !integer :: inttime
    integer, dimension(8) :: values

    call add_metadata(gnostics%sfile, "file_specification", &
      "1.1")
    call add_metadata(gnostics%sfile, "trinity_version", &
      "development")
    call add_metadata(gnostics%sfile, "title", &
      jline("")//&
      jline("=================================================")//&
      jline("                Trinity Output File")//&
      jline("=================================================")//&
      jline(""))
    call add_metadata(gnostics%sfile, "file_description", &
      jline("")//&
      jline(" This file contains output from the transport")//&
      jline(" solver Trinity. Trinity is a code")//&
      jline(" which evolves the profiles of density, ")//&
      jline(" pressure and flow within a fusion plasma.")//& 
      jline(" Among other information this file may contain: ")//&
      jline(" (*) Values of density, temperature and rotation. ")//&
      jline(" (*) Values of turbulent and neoclassical fluxes. ")//&
      jline(" (*) Values of particle, heat and momentum sources")//&
      jline(" The details of what is contained are controlled")//&
      jline(" by the namelist trindiag_config. ")//&
      jline(""))
    call add_metadata(gnostics%sfile, "data_description", &
      jline("")//&
      jline(" Most data consists of physical quantities given")//&
      jline(" as a function of one more of:")//&
      jline(" (*) Normalised minor radius ")// &
      jline(" (*) Time ")// &
      jline(" (*) Newton solve iteration number ")// &
      jline(" Data is typically double precision real numbers.  ")//&
      jline(" Complex numbers are handled by having an extra")//&
      jline(" dimension ri. ")//&
      jline(""))
    call add_metadata(gnostics%sfile, "normalization_description", &
      jline("")//&
      jline(" Quantities in this file are given in ")//&
      jline(" dimensional form in the following units: ")//&
      jline(" (*) densities: 10^19 m^-3 ")//&
      jline(" (*) temperatures: keV ")//&
      jline(""))

    call add_metadata(gnostics%sfile, "gs2_help", &
      jline("")//&
      jline(" At the time of writing you can obtain ")//&
      jline(" help for using Trinity in the following")//&
      jline(" places: ")//&
      jline(" (*) http://gyrokinetics.sourceforge.net/wiki")//&
      jline("       (User help: installing, running) ")//&
            new_line("")//& 
            " (*) http://gyrokinetics.sourceforge.net/trinity_documentation/ "//&
      jline("       (Doxygen documentation) ")//&
      jline(" (*) http://sourceforge.net/projects/gyrokinetics ")//& 
      jline("       (Downloads, bug tracker) ")//&
      jline(""))
    call add_metadata(gnostics%sfile, "input_file_extraction", &
      jline("")//&
      jline(" A bash utility for extracting the input file ")//&
      jline(" from this file is included in the scripts ")//&
      jline(" folder in the GS2 source. To invoke it:")//&
      jline(" $ ./extract_input_file <netcdf_file>")//&
      jline(""))
    !call add_metadata(gnostics%sfile, "user_comments", &
      !trim(user_comments))
    !call add_metadata(gnostics%sfile, "svn_revision", &
      !trim(get_svn_rev()))
    !call add_metadata(gnostics%sfile, "build_identifier", &
      !trim(build_identifier()))

    call add_standard_metadata(gnostics%sfile)
  end subroutine write_metadata

  function jline(inputline)
    use mp_trin, only: abort_mp
    character(*), intent(in) :: inputline
    character(len=52) :: jline
    integer :: lenin
    lenin = len(trim(inputline))
    if (lenin .gt. 50) then
      write (*,*) "Long line: "//inputline, .true.
      call abort_mp
    end if

    jline = ''

    jline(2:lenin+1) = inputline(1:lenin)
    jline(1:1) = new_line("")
    
  end function jline

  subroutine read_input_file_and_get_size(gnostics)
    use file_utils_trin, only: get_input_unit
    use mp_trin, only: proc0, broadcast
    type(trindiag_type), intent(in) :: gnostics
    character(len=1000) :: line
    integer :: inputunit
    integer :: ios, i
    !Note : The input file used here is not directly the input file passed to GS2
    !as during init_input_unit (file_utils) the passed input file is processed to 
    !a) Strip out any comments
    !b) Deal with any included files.
    !It is this processed file (written to .<run_name>.in) which is stored.
    !We may wish to add the option to retain comments as these may be useful
    !when revisting old files to remind the user why the file is setup as it is.

    ! Find out the size of the input file and read it in
    ! This is unnecessarily done twice, as this function 
    ! is called once to create the variable and once to
    ! write, but the overhead is trivial
    if (proc0) then
      call get_input_unit (inputunit)
      !write (*,*) 'inputunit', inputunit
      rewind (unit=inputunit)
      ios = 0
      inputfile_length = 0
      do while (ios .eq. 0)
        line = ''
        read(unit=inputunit, iostat=ios, fmt=('(A,$)')) &
          line

        !if (len(trim(line)) .gt. 1) then
          do i = 1, len(trim(line))
            inputfile_length = inputfile_length + 1
            inputfile_array(inputfile_length) = line(i:i)
          end do
          inputfile_length = inputfile_length + 1
          inputfile_array(inputfile_length) = " "
          inputfile_length = inputfile_length + 1
          inputfile_array(inputfile_length) = "\" !" !These comment characters are for editors which think \ is escaping "
          inputfile_length = inputfile_length + 1
          inputfile_array(inputfile_length) = "n"
        !end if 
      end do
    end if
    call broadcast(inputfile_length)
    call broadcast(inputfile_array)
  end subroutine read_input_file_and_get_size

  !> Write the input file.
 
  subroutine write_input_file(gnostics)
    use file_utils_trin, only: get_input_unit
    use trindiag_create_and_write, only: create_and_write_variable
    use simpledataio, only: SDATIO_CHAR
    type(trindiag_type), intent(in) :: gnostics

    !write (*,*) inputfile_array, inputfile_length

    call create_and_write_variable(gnostics, SDATIO_CHAR, "input_file", &
      "input_file_dim", &
       "Full text of the input file", "text", inputfile_array) 



  end subroutine write_input_file
end module trindiag_metadata
