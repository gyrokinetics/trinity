!> A module for writing the sources of density
!! temperature etc to the netcdf file.
module trindiag_sources
  use trindiag_config, only: trindiag_type
  use flux_results, only: flux_results_type
  implicit none
  contains
    subroutine write_sources(gnostics, flx)
      use nteqns_arrays, only: src_aux, src_dens, int_pflux, int_power
      use trindiag_create_and_write, only: create_and_write_variable
      use trindiag_create_and_write, only: create_and_write_variable_noread
      use trinity_input, only: m_ion, vtfac, ncc, nspec
      use nteqns_arrays, only: pres_cc, dens_cc, area_cc, bmag_cc
      use nteqns_arrays, only: src_radiate, src_pres, src_alph
      use trinity_input, only: nrad, nspec
      use norms, only: pflx_gb_cc, qflx_gb_cc, aref
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(flux_results_type), intent(inout) :: flx
      real, dimension (:,:), allocatable :: pflx_si, qflx_si
      real, dimension (nrad, nspec) :: dummy
      integer :: is

      dummy = -1.0
      
      allocate (pflx_si(ncc,nspec), qflx_si(ncc, nspec))

      call create_and_write_variable(gnostics, gnostics%rtype, "src_dens", &
        "rad,tspec,t", &
         "Total density source in internal units", "10^20/nsrcnorm", src_dens) 

      call create_and_write_variable(gnostics, gnostics%rtype, "src_aux", &
        "rad,tspec,t", &
         "Total heat source (in internal units) from combined auxiliary heating, &
         & that is, all externally applied heat sources, &
         & not including alpha heating, turbulent heating and &
         & equilibration.", "1/srcnorm", &
         src_aux) 

      call create_and_write_variable(gnostics, gnostics%rtype, "src_alph", &
        "rad,tspec,t", &
         "Total heat source (in internal units) from alpha heating.", &
         "1/srcnorm", src_alph) 

      call create_and_write_variable(gnostics, gnostics%rtype, "src_pres", &
        "rad,tspec,t", &
         "Total combined heat source .", &
         "1/srcnorm", src_pres) 

      call create_and_write_variable(gnostics, gnostics%rtype, "src_radiate", &
        "rad,tspec,t", &
         "Heat loss via radiation .", &
         "1/srcnorm", src_radiate) 

      call create_and_write_variable(gnostics, gnostics%rtype, "int_pflux", &
        "rad,tspec,t", &
         "Integrated particle source", "TBC", int_pflux) 

      call create_and_write_variable(gnostics, gnostics%rtype, "int_pflux_cc", &
        "rad,tspec,t", &
         "Integrated particle source", "TBC", int_pflux) 
      call create_and_write_variable(gnostics, gnostics%rtype, "int_power_cc", &
        "rad,tspec,t", &
         "Integrated particle source", "TBC", int_power)

       do is=1,nspec
       ! integrated particle flux in 10^20/s (see Eq. 17 of normalization document)
         !pflx_si(:,is) = 3.22*pflx_cc(:,is,1)*dens_cc(:,2) &
            !*(vtfac*pres_cc(:,2)/dens_cc(:,2))**1.5*sqrt(m_ion(1))*area_cc/bmag_cc**2
         pflx_si(:,is) = flx%p_cc(:,is,1)*pflx_gb_cc(:,is)*area_cc*aref**2.0/1.0e20
          ! integrated heat flux in MW
          !qflx_si(:,is) = 5.17655e-2*(qflx_cc(:,is,1)+qflx_neo_cc(:,is,1)) &
            !*dens_cc(:,2)*vtfac**1.5*(pres_cc(:,2)/dens_cc(:,2))**2.5*sqrt(m_ion(1))*area_cc/bmag_cc**2
          qflx_si(:,is) = (flx%q_cc(:,is,1)+flx%q_neo_cc(:,is,1)*0.0) &
            *qflx_gb_cc(:,is)*area_cc*aref**2.0
       end do

      call create_and_write_variable(gnostics, gnostics%rtype, "pflx_si", &
        "cc,tspec,t", &
         "Particle flow in SI units (compare with int_pflux_cc)", "10^20 s^-1", pflx_si) 
      call create_and_write_variable(gnostics, gnostics%rtype, "qflx_si", &
        "cc,tspec,t", &
         "Heat flow in SI units (compare with int_power_cc)", "MW", qflx_si) 

      call create_and_write_variable_noread(gnostics, gnostics%rtype, "qflx_bal", &
        "cc,tspec,t", &
         "Heat flow divided by integrated heat source (should &
        &be 1 in steady state) ", "1", qflx_si/int_power(1:ncc,:)) 

      call create_and_write_variable_noread(gnostics, gnostics%rtype, "pflx_bal", &
        "cc,tspec,t", &
         "Particle flow divided by integrated density source (should &
        &be 1 in steady state) ", "1", pflx_si/int_pflux(1:ncc,:)) 


       deallocate(pflx_si, qflx_si)
    end subroutine write_sources
end module trindiag_sources
