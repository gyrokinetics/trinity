!> A module for writing out flux gradients
!! d Gamma / d L_n etc
module trindiag_fluxgrads
  use trindiag_config, only: trindiag_type
  public :: write_fluxgrads
  contains 
    subroutine write_fluxgrads(gnostics)
      use trindiag_create_and_write, only: create_and_write_variable
      use nteqns, only: dpflxdg, dqflxdg, dprims
      type(trindiag_type), intent(inout) :: gnostics

      call create_and_write_variable(gnostics, gnostics%rtype, "dpflxdg", &
        "cc,tspec,ivar,iter,t", &
         "The derivative of the particle flux with respect to the given &
         & implicitly treated variable (e.g. L_n, n, L_Ti etc).", &
         "TBC", dpflxdg) 
      call create_and_write_variable(gnostics, gnostics%rtype, "dqflxdg", &
        "cc,tspec,ivar,iter,t", &
         "The derivative of the heat flux with respect to the given &
         & implicitly treated variable (e.g. L_n, n, L_Ti etc).", &
         "TBC", dqflxdg) 
      call create_and_write_variable(gnostics, gnostics%rtype, "dprims", &
        "cc,prof,iter,t", &
         "The gradient delta used to calculate dflux/dgradient.", &
         "TBC", dprims) 

      
    end subroutine write_fluxgrads
end module trindiag_fluxgrads
