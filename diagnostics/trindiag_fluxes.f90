!> A module for writing out the fluxes returned from
!! get_fluxes
module trindiag_fluxes
  use trindiag_config, only: trindiag_type
  contains
    subroutine write_fluxes(gnostics,flx)
      use trindiag_create_and_write, only: create_and_write_variable
      use flux_results, only: flux_results_type
      type(trindiag_type), intent(inout) :: gnostics
      type(flux_results_type), intent(inout) :: flx

      call create_and_write_variable(gnostics, gnostics%rtype, "pflx_cc", &
        "cc,tspec,jac,iter,t", &
         "Particle flux at cell centres", "Gamma_gB", flx%p_cc) 
      call create_and_write_variable(gnostics, gnostics%rtype, "qflx_cc", &
        "cc,tspec,jac,iter,t", &
         "Heat flux at cell centres", "Q_gB", flx%q_cc) 
      call create_and_write_variable(gnostics, gnostics%rtype, "heat_cc", &
        "cc,tspec,jac,iter,t", &
         "Turbulent heating at cell centres", "TBC", flx%heat_cc) 
      call create_and_write_variable(gnostics, gnostics%rtype, "lflx_cc", &
        "cc,jac,iter,t", &
         "Momentum flux at cell centres", "Pi_gB", flx%l_cc) 


    end subroutine write_fluxes

end module trindiag_fluxes
