!> A module for writing out the results of each
!! calibration step
module trindiag_calibrate
  use trindiag_config, only: trindiag_type
  implicit none
  contains

    subroutine trindiag_advance_calib(gnostics)
      use simpledataio, only: increment_start, syncfile
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics
      if (gnostics%parallel .or. (proc0.and.gnostics%serial_netcdf4)) then
        call increment_start(gnostics%sfile, "calib")
        call syncfile(gnostics%sfile)
      end if
    end subroutine trindiag_advance_calib
    
    subroutine write_calib(gnostics, calib)
      use trindiag_create_and_write, only: create_and_write_variable
      use calibrate_type_module, only: calibrate_type
      type(trindiag_type), intent(inout) :: gnostics
      type(calibrate_type), intent(inout) :: calib
      if (gnostics%parallel .or. gnostics%serial_netcdf4) then
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_reduced_pflux", &
          "cc,tspec,calib", &
           "Particle flux at cell centres (reduced model)", "Gamma_gB", calib%reduced_pflux) 
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_reduced_qflux", &
          "cc,tspec,calib", &
           "Heat flux at cell centres (reduced model)", "Q_gB", calib%reduced_qflux) 
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_full_pflux", &
          "cc,tspec,calib", &
           "Particle flux at cell centres (full model)", "Gamma_gB", calib%full_pflux) 
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_full_qflux", &
          "cc,tspec,calib", &
           "Heat flux at cell centres (full model)", "Q_gB", calib%full_qflux) 
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_pflux_factor", &
          "cc,tspec,calib", &
           "Particle flux calibration factor", "1", calib%pflux_calibration_factor) 
        call create_and_write_variable(gnostics, gnostics%rtype, "calib_qflux_factor", &
          "cc,tspec,calib", &
           "Heat flux calibration factor", "1", calib%qflux_calibration_factor) 
      end if
    end subroutine write_calib

end module trindiag_calibrate
