
!> A module for writing and reading all the data
!! needed to restart a trinity run.
module trindiag_restart
  use trindiag_config, only: trindiag_type
  implicit none
  contains
    subroutine open_restart_file(gnostics_restart, file_name)
      use simpledataio, only: open_file
      use simpledataio, only: sdatio_init
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics_restart
      character(*), intent(in) :: file_name
      gnostics_restart%parallel = .false.
      gnostics_restart%wryte = .false.
      gnostics_restart%create = .false.
      if (proc0) then
        gnostics_restart%reed = .true.
      else
        gnostics_restart%reed = .false.
      end if
      if (proc0) then
        call sdatio_init(gnostics_restart%sfile, trim(file_name))
        call open_file(gnostics_restart%sfile)
        gnostics_restart%cegrid_created = .true.
      end if
    end subroutine open_restart_file

    subroutine close_restart_file(gnostics_restart)
      use simpledataio, only: closefile
      use simpledataio, only: sdatio_free
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics_restart
      if (proc0) then
        call closefile(gnostics_restart%sfile)
        call sdatio_free(gnostics_restart%sfile)
      end if
    end subroutine close_restart_file

    subroutine restart_trinity(file_name, clock)
      use trinity_type_module, only: trinity_clock_type
      implicit none
      type(trinity_clock_type), intent(inout) :: clock
      type(trindiag_type) :: gnostics_restart
      character(len=*), intent(in) :: file_name
      real :: rad_out_input
      integer :: nrad_input
      call open_restart_file(gnostics_restart, file_name)
      ! Function open_restart_file sets up the gnostics_restart
      ! object so that write_restart actually reads instead
      call write_restart(gnostics_restart, clock, nrad_input, rad_out_input)
      call close_restart_file(gnostics_restart)
    end subroutine restart_trinity

    subroutine write_restart(gnostics, clock, nrad_netcdf, rad_out_netcdf)
      use trinity_time, only: ntdelt_old
      use trinity_input, only: ntdelt
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: SDATIO_INT
      use simpledataio, only: dimension_size, set_dimension_start, syncfile
      use nteqns_arrays, only: write_grids, dens_init, pres_init, mom_init
      use trinity_type_module, only: trinity_clock_type
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(trinity_clock_type), intent(inout) :: clock
      integer, intent(inout) :: nrad_netcdf
      real, intent(inout) :: rad_out_netcdf
      integer ::  ntstep_netcdf


      call create_and_write_variable(gnostics, gnostics%rtype, "time_restart", &
        "", &
         "Simulation time saved for restart", "s", clock%time) 
      call create_and_write_variable(gnostics, SDATIO_INT, "itstep_tot_restart", &
        "", &
         "Total number of timesteps saved for restart", "", clock%itstep_tot) 
      call create_and_write_variable(gnostics, SDATIO_INT, "iter_restart", &
        "", &
         "Current iteration saved for restart", "", clock%iter) 
      call create_and_write_variable(gnostics, SDATIO_INT, "neval_tot_restart", &
        "", &
         "Current count of flux evaluations saved for restart", "", clock%neval) 
      call create_and_write_variable(gnostics, gnostics%rtype, "ntdelt_restart", &
        "", &
         "Current timestep saved for restart", "s", ntdelt) 
      call create_and_write_variable(gnostics, gnostics%rtype, "ntdelt_old_restart", &
        "", &
         "Old timestep saved for restart", "s", ntdelt_old) 

      call create_and_write_variable(gnostics, gnostics%rtype, "dens_init", &
        "rad,tspec", &
         "Initial density saved for restart", "10^20 m^-3", dens_init)
      call create_and_write_variable(gnostics, gnostics%rtype, "pres_init", &
        "rad,tspec", &
         "Initial pressure saved for restart", "10^20 keV m^-3", pres_init)
      call create_and_write_variable(gnostics, gnostics%rtype, "mom_init", &
        "rad", &
         "Initial toroidal angular momentum density &
         &saved for restart", "m_p aref^2 rad s^-1 m^-3", mom_init)

      if (gnostics%reed) then
        ! Read last time and set iter then read grids.
        call dimension_size(gnostics%sfile, "t", ntstep_netcdf)
        call set_dimension_start(gnostics%sfile, "t", ntstep_netcdf)
        call set_dimension_start(gnostics%sfile, "iter", clock%iter)
      end if

      ! If we are actually reading the restart, this function will 
      ! read instead.
      call write_grids(gnostics, nrad_netcdf, rad_out_netcdf, 'profiles')

      if (gnostics%wryte .or. gnostics%reed) call syncfile(gnostics%sfile)

    end subroutine write_restart


    subroutine restart_chease_ecom(checom, file_name)
      use chease_ecom, only: chease_ecom_type
      use simpledataio, only: variable_exists
      use mp_trin, only: abort_mp
      use file_utils_trin, only: error_unit
      implicit none
      type (chease_ecom_type), intent(inout) :: checom
      type(trindiag_type) :: gnostics_restart
      character(*), intent(in) :: file_name
      call open_restart_file(gnostics_restart, file_name)
      if (.not. &
        variable_exists(gnostics_restart%sfile,"cegrid_original")) then 
        write(error_unit(), *) "ERROR: you are trying to restart a run which &
          & uses CHEASE to evolve the G-S equation, but no data about the &
          & original pressure gradient is available in the restart file. This &
          & may be because CHEASE was not being used in the previous run."
        call abort_mp
      end if
      ! Function open_restart_file sets up the gnostics_restart
      ! object so that write_chease_ecom actually reads instead
      call write_chease_ecom(gnostics_restart, checom)
      call close_restart_file(gnostics_restart)
    end subroutine restart_chease_ecom

    !> Write input arrays and internal grids of the Grad-Shafranov 
    !! solvers CHEASE or ECOM (if either is being used).
    subroutine write_chease_ecom(gnostics, checom)
      use chease_ecom, only: chease_ecom_type, init_chease_ecom
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: add_dimension, dimension_size, SDATIO_INT
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(chease_ecom_type), intent(inout) :: checom

      ! Because we don't know the dimensions of the G-S solver grids
      ! at the beginning of the Trinity simulation we have to handle
      ! them here.
      if (gnostics%wryte .and. .not.  gnostics%cegrid_created) then
        call add_dimension(gnostics%sfile, "cegrid", checom%ncegrid, "","")
        call add_dimension(gnostics%sfile, "cesurf", checom%ncesurf, "","")
        gnostics%cegrid_created = .true.
      else if (gnostics%reed .and. .not. checom%initialized) then
        call dimension_size(gnostics%sfile, "cegrid", checom%ncegrid)
        call dimension_size(gnostics%sfile, "cesurf", checom%ncesurf)
        call init_chease_ecom(checom, checom%ncegrid, checom%ncesurf)
      end if 
      call create_and_write_variable(gnostics, SDATIO_INT, "nruns", &
        "", &
         "Number of chease runs, used for labeling chease &
         & output files", "", checom%nruns) 
      call create_and_write_variable(gnostics, gnostics%rtype, "cegrid_original", &
        "cegrid", &
         "Initial values of the chease/ecom grid, needed &
         & when evolving geometry with chease", "", checom%cegrid_original) 
      call create_and_write_variable(gnostics, gnostics%rtype, "dpdpsi_cegrid_original", &
        "cegrid", &
         "Initial pressure gradient profile on the chease/ecom grid, needed &
         & when evolving geometry with chease",&
         "(b0exp_chse^2/mu0) / (b0exp_chse * r0exp_chse^2)",&
         checom%dpdpsi_cegrid_original) 
      call create_and_write_variable(gnostics, gnostics%rtype, "cegrid", &
        "cegrid,t", &
         "Current values of the chease/ecom grid s (=rhopol)", "", checom%cegrid) 
      call create_and_write_variable(gnostics, gnostics%rtype, "dpdpsi_cegrid", &
        "cegrid,t", &
         "Pressure gradient profile on the chease/ecom grid" ,&
         "(b0exp_chse^2/mu0) / (b0exp_chse * r0exp_chse^2)", checom%dpdpsi_cegrid) 

      call create_and_write_variable(gnostics, gnostics%rtype, "boundary_r_cesurf", &
        "cesurf,t", &
         "Major radius values of chease/ecom boundary surface" ,&
         "r0exp_chse", checom%boundary_r) 
      call create_and_write_variable(gnostics, gnostics%rtype, "boundary_z_cesurf", &
        "cesurf,t", &
         "Vertical height values of chease/ecom boundary surface" ,&
         "r0exp_chse", checom%boundary_z) 
    end subroutine write_chease_ecom 

    subroutine restart_iteration(file_name, sayv)
      use save_arrays, only: save_arrays_type
      implicit none
      type(save_arrays_type), intent(inout) :: sayv
      type(trindiag_type) :: gnostics_restart
      character(*), intent(in) :: file_name
      call open_restart_file(gnostics_restart, file_name)
      call check_restart_file_ok(gnostics_restart, sayv)
      ! Function open_restart_file sets up the gnostics_restart
      ! object so that write_iteration actually reads instead
      call write_iteration(gnostics_restart,sayv)
      call close_restart_file(gnostics_restart)
    end subroutine restart_iteration

    subroutine check_restart_file_ok(gnostics, sayv)
      use simpledataio, only: dimension_size
      use mp_trin, only: proc0, abort_mp
      use file_utils_trin, only: error_unit
      use save_arrays, only: save_arrays_type
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(save_arrays_type), intent(inout) :: sayv
      integer :: sz
      if (proc0) then
        call dimension_size(gnostics%sfile, "mcol", sz)
        if (.not. sz .eq. size(sayv%nt)) then
          write (error_unit(), *) 'ERROR: You cannot &
          &restart with a different nrad or jacobian size. &
          &Suggest using this file as the init file.'
          call abort_mp
        end if
      end if
    end subroutine check_restart_file_ok

    subroutine write_iteration(gnostics, sayv)
      use trindiag_create_and_write, only: create_and_write_variable
      use trindiag_create_and_write, only: create_and_write_variable_noread
      use save_arrays, only: save_arrays_type
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(save_arrays_type), intent(inout) :: sayv

      call create_and_write_variable_noread(gnostics, gnostics%rtype, "rmserr_t", &
        "iter,t", &
         "Rmserr of minimisation algorithm as a function of time", "1", sayv%rmserr) 
      call create_and_write_variable(gnostics, gnostics%rtype, "rmserr", &
        "", &
         "Rmserr of minimisation algorithm", "1", sayv%rmserr) 
      call create_and_write_variable(gnostics, gnostics%rtype, "nt_save", &
        "mcol", &
         "Profile state at the beginning of the current timestep", "mixed", sayv%nt) 
      call create_and_write_variable(gnostics, gnostics%rtype, "rhs_save", &
        "mcol", "Profile state at the end of the current iteration (not needed?)", &
        "mixed", sayv%rhs) 
      call create_and_write_variable(gnostics, gnostics%rtype, "flx_save", &
        "fluxvec", "Profile state at the beginning of the current timestep", &
        "mixed", sayv%flx) 
      call create_and_write_variable(gnostics, gnostics%rtype, "pflx_save", &
        "cc,tspec,jac", "Particle flux at the beginning of the current timestep", &
        "Gamma_gB", sayv%pflx) 
      call create_and_write_variable(gnostics, gnostics%rtype, "qflx_save", &
        "cc,tspec,jac", "Heat flux at the beginning of the current timestep", &
        "Q_gB", sayv%qflx) 
      call create_and_write_variable(gnostics, gnostics%rtype, "heat_save", &
        "cc,tspec,jac", "Turbulent heating at the beginning of the current timestep", &
        "TBC", sayv%heat) 
      call create_and_write_variable(gnostics, gnostics%rtype, "lflx_save", &
        "cc,jac", "Momentum flux at the beginning of the current timestep", &
        "Pi_gB", sayv%lflx) 
      call create_and_write_variable(gnostics, gnostics%rtype, "heatgrid_save", &
        "rad,tspec", "Turbulent heating at the beginning of the current timestep", &
        "TBC", sayv%heatgrid) 
      ! Variables from the beginning of the previous timestep... needed for a multistep
      ! scheme?
      call create_and_write_variable(gnostics, gnostics%rtype, "ntold_save", &
        "mcol", &
         "Profile state at the beginning of the previous timestep", "mixed", sayv%ntold) 
      call create_and_write_variable(gnostics, gnostics%rtype, "pflxold_save", &
        "cc,tspec,jac", "Particle flux at the beginning of the previous timestep", &
        "Gamma_gB", sayv%pflxold) 
      call create_and_write_variable(gnostics, gnostics%rtype, "qflxold_save", &
        "cc,tspec,jac", "Heat flux at the beginning of the previous timestep", &
        "Q_gB", sayv%qflxold) 
      call create_and_write_variable(gnostics, gnostics%rtype, "heatold_save", &
        "cc,tspec,jac", "Turbulent heating at the beginning of the previous timestep", &
        "TBC", sayv%heatold) 
      call create_and_write_variable(gnostics, gnostics%rtype, "lflxold_save", &
        "cc,jac", "Momentum flux at the beginning of the previous timestep", &
        "Pi_gB", sayv%lflxold) 

    end subroutine write_iteration
end module trindiag_restart
