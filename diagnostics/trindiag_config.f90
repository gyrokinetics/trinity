
! DO NOT EDIT THIS FILE
! This file has been automatically generated using generate_trindiag_config.rb

!> A module for handling the configuration of the diagnostics
!! module via the namelist trindiag_config.
module trindiag_config
  use simpledataio, only: sdf=>sdatio_file
  use trindiag_ascii, only: trindiag_ascii_type
  implicit none

  private

  public :: init_trindiag_config
  public :: finish_trindiag_config
  public :: trindiag_type
  public :: set_created_flags


  !> A type for storing the diagnostics configuration,
  !! a reference to the output file, and current 
  !! results of the simulation
  type trindiag_type
     type(sdf) :: sfile
     type(trindiag_ascii_type) :: ascii_files
     !> Integer below gives the sdatio type 
     !! which corresponds to a gs2 real
     integer :: rtype
     integer :: itype
     integer :: istep
     integer :: verbosity = 3
     logical :: create
     logical :: wryte
     logical :: reed
     logical :: replay
     logical :: distributed
     logical :: parallel
     logical :: exit
     logical :: appending
     integer :: neval_this_run
     real :: wallclock_start
     real :: wallclock_last_eval
     logical :: created_profiles
     logical :: created_time
     logical :: created_cc
     logical :: created_iteration 
     real :: user_time
     real :: user_time_old
     real :: start_time
     !> We can't create the cegrid dim at the same
     !! time as every other, so we have to keep track
     !! of its status
     logical :: cegrid_created = .false.
     real, dimension(:), pointer :: fluxfac
     logical :: write_any
     logical :: append_old
     logical :: enable_parallel
     logical :: serial_netcdf4
  end type trindiag_type

contains
  subroutine init_trindiag_config(gnostics)
    implicit none
    type(trindiag_type), intent(inout) :: gnostics
    call read_parameters(gnostics)
    call set_created_flags(gnostics, .true.)
  end subroutine init_trindiag_config


  subroutine finish_trindiag_config(gnostics)
    implicit none
    type(trindiag_type), intent(inout) :: gnostics
  end subroutine finish_trindiag_config

  subroutine set_created_flags(gnostics, flag)
    implicit none
    type(trindiag_type), intent(inout) :: gnostics
    logical, intent(in) :: flag
    gnostics%created_profiles=flag
    gnostics%created_time=flag
    gnostics%created_cc=flag
    gnostics%created_iteration=flag
    
  end subroutine set_created_flags


  subroutine read_parameters(gnostics)
    use file_utils_trin, only: input_unit, error_unit, input_unit_exist
    use text_options_trin, only: text_option, get_option_value
    use mp_trin, only: proc0, broadcast, nproc, iproc
    implicit none
    type(trindiag_type), intent(out) :: gnostics
    logical :: write_any
    logical :: append_old
    logical :: enable_parallel
    logical :: serial_netcdf4
    namelist /trindiag_config/ &
         write_any, &
         append_old, &
         enable_parallel, &
         serial_netcdf4

    integer :: in_file
    logical :: exist

    if (proc0) then
       write_any = .true.
       append_old = .false.
       enable_parallel = .false.
       serial_netcdf4 = .true.

       in_file = input_unit_exist ("trindiag_config", exist)
       if (exist) read (unit=in_file, nml=trindiag_config)

       gnostics%write_any = write_any
       gnostics%append_old = append_old
       gnostics%enable_parallel = enable_parallel
       gnostics%serial_netcdf4 = serial_netcdf4

    end if

    

    call broadcast (gnostics%write_any)
    call broadcast (gnostics%append_old)
    call broadcast (gnostics%enable_parallel)
    call broadcast (gnostics%serial_netcdf4)
    
  end subroutine read_parameters
end module trindiag_config

