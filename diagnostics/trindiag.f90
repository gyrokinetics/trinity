!> A module for writing results to the netcdf output file.
module trindiag

  use trindiag_config, only: trindiag_type
  implicit none
  public :: init_trindiag
  public :: finish_trindiag
  public :: trindiag_advance_time
  public :: trindiag_write_time
  public :: write_transport_solution
  
  !public :: run_trindiag

contains

  subroutine  init_trindiag(gnostics, replay)
    use trindiag_config, only: init_trindiag_config
    use simpledataio, only: SDATIO_DOUBLE, SDATIO_FLOAT, SDATIO_INT
    use simpledataio, only: create_file, open_file, set_dimension_start
    use simpledataio, only: sdatio_init, set_parallel
    use file_utils_trin, only: run_name
    use mp_trin, only: proc0, abort_mp, broadcast, communicator
    use mp_trin, only: timer_local
    use trindiag_metadata, only: read_input_file_and_get_size
    use trindiag_metadata, only: write_metadata, write_input_file
    use trindiag_config, only: set_created_flags
    use netcdf, only: NF90_CLOBBER
    type(trindiag_type), intent(inout) :: gnostics
    logical, intent(in) :: replay
    logical :: ex

    call init_trindiag_config(gnostics)

    if (.not. gnostics%write_any) return

    gnostics%wallclock_start = timer_local()
    gnostics%wallclock_last_eval = timer_local()
    !if (init_options%default_double) then
       gnostics%rtype = SDATIO_DOUBLE
    !else
       !gnostics%rtype = SDATIO_FLOAT
    !end if

    gnostics%parallel = .false.
    call sdatio_init(gnostics%sfile, trim(run_name)//'.out.nc')
    if (gnostics%parallel) then 
       call set_parallel(gnostics%sfile, communicator)
    else
       if (.not. gnostics%serial_netcdf4) then 
          gnostics%sfile%mode = NF90_CLOBBER
       end if
    end if

    call read_input_file_and_get_size(gnostics)
        
    if (gnostics%parallel.or.proc0) then 
       inquire(file=trim(run_name)//'.out.nc', exist=ex)
       if (replay) then
         if (ex) then
           call open_file(gnostics%sfile)
           call set_dimension_start(gnostics%sfile, "t", 1)
           !call print_dimensions(gnostics%sfile)
         else 
           write (*,*) "replay set to true but no file exists"
           call abort_mp
         end if
         gnostics%appending=.false.
         gnostics%cegrid_created = .true.
       else if (gnostics%append_old .and. ex) then
         gnostics%appending=.true.
         call open_file(gnostics%sfile)
         gnostics%cegrid_created = .true.
       else
         gnostics%appending=.false.
         call create_file(gnostics%sfile)
         call set_created_flags(gnostics, .false.)
         call create_dimensions(gnostics)
         call write_metadata(gnostics)
         gnostics%cegrid_created = .false.
       end if
       !call write_metadata(gnostics)
    end if
    call broadcast(gnostics%appending)

    gnostics%replay = replay
  
    gnostics%reed = .false.
    if (proc0 .or. gnostics%parallel) then 
      !gnostics%create = .true.
      gnostics%wryte = .true.
      gnostics%create = .true.
      call write_input_file(gnostics)
      call write_radial_grid(gnostics)
      !gnostics%create = .false.
      !gnostics%wryte = .true.
      !call write_input_file(gnostics)
      !call write_radial_grid(gnostics)
    else
      gnostics%wryte=.false.
      gnostics%create = .false.
    end if
    
    gnostics%neval_this_run = 1

    

  end subroutine  init_trindiag

  subroutine finish_trindiag(gnostics)
    use simpledataio, only: closefile, sdatio_free
    use mp_trin, only: proc0
    type(trindiag_type), intent(inout) :: gnostics
    if (gnostics%parallel .or. proc0) then
      call closefile(gnostics%sfile)
      call sdatio_free(gnostics%sfile)
    end if
  end subroutine finish_trindiag

  subroutine trindiag_advance_time(gnostics)
    use simpledataio, only: increment_start
    use mp_trin, only: proc0
    use simpledataio, only: syncfile
    type(trindiag_type), intent(inout) :: gnostics
    if (gnostics%parallel .or. proc0) then
      call increment_start(gnostics%sfile, "t")
      call syncfile(gnostics%sfile)
    end if
  end subroutine trindiag_advance_time

  subroutine trindiag_write_time(gnostics, clock)
    use trindiag_create_and_write, only: create_and_write_variable
    use mp_trin, only: proc0
    use mp_trin, only: timer_local
    use trinity_time, only: ntdelt_old
    use trinity_type_module, only: trinity_clock_type
    use simpledataio, only: syncfile
    type(trindiag_type), intent(inout) :: gnostics
    type(trinity_clock_type), intent(inout) :: clock
    real :: wallclock_time
    wallclock_time = timer_local() - gnostics%wallclock_start
    if (gnostics%parallel .or. proc0) then
      call create_and_write_variable(gnostics, gnostics%rtype, "wall_clock", &
        "t", "Wall clock time since start", "s", &
        wallclock_time) 
      call create_and_write_variable(gnostics, gnostics%rtype, "t", &
        "t", "Time", "s", clock%time) 
      call syncfile(gnostics%sfile)
    end if
  end subroutine trindiag_write_time

  !subroutine run_trindiag(itstep, gnostics)
    !integer, intent(in) :: itstep
    !type(trindiag_type), intent(inout) :: gnostics
  !end subroutine run_trindiag

  subroutine create_dimensions(gnostics)
    use simpledataio, only: add_dimension
    use simpledataio, only: SDATIO_UNLIMITED
    use trindiag_metadata, only: inputfile_length
    use trinity_input, only: nspec, niter, nrad_netcdf, nrad, ncc
    use trinity_input, only: njac, nprofs_evolved, njobs
    use nteqns_arrays, only: ntheta, nR_eq, nZ_eq
    type(trindiag_type), intent(in) :: gnostics

    call add_dimension(gnostics%sfile, "input_file_dim", inputfile_length,"","")
    call add_dimension(gnostics%sfile, "t", SDATIO_UNLIMITED, "time", "")
    call add_dimension(gnostics%sfile, "tspec", nspec, "species", "")
    call add_dimension(gnostics%sfile, "ion", nspec-1, "ion species", "")
    call add_dimension(gnostics%sfile, "iter", niter, "iteration", "")
    call add_dimension(gnostics%sfile, "rad", nrad_netcdf, "cell boundary radius", "")
    call add_dimension(gnostics%sfile, "cc", ncc, "cell centre radius", "")
    call add_dimension(gnostics%sfile, "jac", njac, "jacobian element", "")
    call add_dimension(gnostics%sfile, "job", njobs, "flux tube number", "")
    call add_dimension(gnostics%sfile, "prof", nprofs_evolved, "indexes evolved profile", "")
    call add_dimension(gnostics%sfile, "fluxvec", (2*nspec+1)*ncc, "specialised index for flux save", "")

    !call add_dimension(gnostics%sfile, "npsi_eq", npsi_eq, "dimension of flux surface data", "")
    call add_dimension(gnostics%sfile, "ntheta", ntheta, "dimension of flux surface data", "")
    call add_dimension(gnostics%sfile, "nR_eq", nR_eq, "dimension of flux surface data", "")
    call add_dimension(gnostics%sfile, "nZ_eq", nZ_eq, "dimension of flux surface data", "")
    !call add_dimension(gnostics%sfile, "ivar", njac-1, &
        !"indexes variables treated implicitly", "")
    !call add_dimension(gnostics%sfile, "imat", ngrads*nrad, &
        !"element of the iteration matrix ", "")
    !> The lines above are correct but we hardwire it to 8 and 4 for the
    !! time being, pending changes in trunk
    call add_dimension(gnostics%sfile, "ivar", 8, &
        "indexes variables treated implicitly", "")
    call add_dimension(gnostics%sfile, "mrow", nprofs_evolved*nrad, &
        "row element of the iteration matrix ", "")
    call add_dimension(gnostics%sfile, "mcol", nprofs_evolved*nrad, &
        "column element of the iteration matrix ", "")
    if (gnostics%serial_netcdf4) then
      call add_dimension(gnostics%sfile, &
          "eval", SDATIO_UNLIMITED, "flux_evaluation", "")
      call add_dimension(gnostics%sfile, &
          "conv", SDATIO_UNLIMITED, "convergence of flux calls", "")
      call add_dimension(gnostics%sfile, &
          "calib", SDATIO_UNLIMITED, "calibration adjustment", "")
    end if
  end subroutine create_dimensions

  subroutine write_radial_grid(gnostics)
    use nteqns_arrays, only: rad_cc, rad_grid
    use trindiag_create_and_write, only: create_and_write_variable
    type(trindiag_type), intent(in) :: gnostics
    call create_and_write_variable(gnostics, gnostics%rtype, "rad", &
      "rad", &
       "Values of the trinity flux label rho . &
       & The definition of rho depends on the value of fluxlabel_option.", "1", rad_grid) 
    call create_and_write_variable(gnostics, gnostics%rtype, "cc", &
      "cc", &
       "Normalised radius at cell centres", "1", rad_cc) 
  end subroutine write_radial_grid

  subroutine write_transport_solution(gnostics, flx, globals, nrad_netcdf, rad_out_netcdf)
    use unit_tests_trin, only: debug_message, verb_iteration
    use nteqns_arrays, only: write_grids, write_cc_and_perturb
    use trindiag_sources, only: write_sources
    use trindiag_geometry, only: write_geometry
    use trindiag_globals, only: write_globals
    use norms, only: write_norms
    use flux_results, only: flux_results_type
    use trinity_type_module, only: globals_type
    use simpledataio, only: syncfile
    implicit none
    type(trindiag_type), intent(inout) :: gnostics
    type(flux_results_type), intent(inout) :: flx
    type(globals_type), intent(inout) :: globals
    integer, intent(inout) :: nrad_netcdf
    real, intent(inout) :: rad_out_netcdf

    call write_geometry(gnostics)
    call write_globals(gnostics, globals)
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_globals')
    call write_norms(gnostics)
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_norms')
    call write_sources(gnostics, flx)
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_sources')
    call write_grids(gnostics, nrad_netcdf, rad_out_netcdf, 'geo')
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_grids(geo)')
    call write_grids(gnostics, nrad_netcdf, rad_out_netcdf, 'profiles')
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_grids(profiles)')
    !At the moment sources  are not a function of time but
    ! we  put this here for future proofing
    call write_grids(gnostics, nrad_netcdf, rad_out_netcdf, 'sources')
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_grids(sources)')
    call write_cc_and_perturb(gnostics)
    call debug_message(verb_iteration, &
      'trindiag::write_transport_solution called write_cc_and_perturb')
    if (gnostics%wryte .or. gnostics%reed) call syncfile(gnostics%sfile)
  end subroutine write_transport_solution


end module trindiag


