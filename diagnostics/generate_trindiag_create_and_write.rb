class Generator
  def initialize(type, dimsize, noread)
    @dimsize = dimsize
    @type = type
    @noread = noread
    if dimsize==0
      @dimension = ""
    else
      @dimension = ", dimension(#{([":"]*dimsize).join(",")}) "
    end
  end
  def procedure_name
    "create_and_write_variable_#{@type.gsub(' ', '_')}_#{@dimsize}#{@noread?"_nr":""}"
  end
  def val_get
    "val(#{@dimsize.times.map{|i| "starts(#{i+1}):"}.join(",")})"
  end
  def function_string
    _string = <<EOF
  subroutine #{procedure_name}(gnostics, variable_type, variable_name, dimension_list, variable_description, variable_units, val)
    use simpledataio, only: create_variable
    use simpledataio_write, only: write_variable
    use simpledataio_read, only: read_variable
    use simpledataio, only: variable_exists
    use simpledataio, only: set_count
    use trindiag_config, only: trindiag_type
    type(trindiag_type), intent(in) :: gnostics
    integer, intent(in) :: variable_type
    character(*), intent(in) :: variable_name
    character(*), intent(in) :: dimension_list
    character(*), intent(in) :: variable_description
    character(*), intent(in) :: variable_units
    #{@type.sub(/_/, '*')}, intent(#{@noread?"in":"inout"})#{@dimension} :: val
    logical :: created_this_call = .false.

    logical :: reed
    reed = gnostics%reed
 
    if (gnostics%create.and..not.variable_exists(gnostics%sfile,variable_name)) then 
       call create_variable(gnostics%sfile, variable_type, variable_name, trim(dimension_list), variable_description, variable_units)
       created_this_call = .true.
    end if

    #{@noread ? nil : "
    if (reed) then 
      if (.not. variable_exists(gnostics%sfile, variable_name)) then
        reed = .false.
        write (*,*) 'WARNING: Could not read variable ', variable_name
      end if
    end if"} 

    if (created_this_call .or. reed) then
      !> If this variable has iter as one of its dimensions
      !! set the count to 1
      if (index(dimension_list, "iter") .ne. 0) &
        call set_count(gnostics%sfile, variable_name, "iter", 1)
    end if

    #{@noread ? nil : "
    if (reed) call read_variable(gnostics%sfile, variable_name, val)
    "} 

    if (gnostics%wryte) call write_variable(gnostics%sfile, variable_name, val)

  end subroutine #{procedure_name}
EOF
  end

end


#$max_dims = 6
$max_dims = 4

begin
  4.times.map{|i|}
rescue
  puts "You appear to be running ruby 1.8.6 or lower... suggest you upgrade your ruby version!"
  class Integer
    def times(&block)
      if block
        (0...self).to_a.each{|i| yield(i)}
      else
        return  (0...self).to_a
      end
    end
  end
end
generators = []
generators_noread = []
['real', 'integer', 'character', 'double precision', 'complex', 'complex_16'].each do |type| # 
  (0..$max_dims).each do |dimsize|
    generators.push Generator.new(type, dimsize, false)
    generators_noread.push Generator.new(type, dimsize, true)
  end
end

string = <<EOF

! DO NOT EDIT THIS FILE
! This file is automatically generated by generate_trindiag_create_and_write

!> A module which contains two high level interfaces for writing 
!! to the netcdf file, one for variables which are local to all
!! processors, and one for variables which may be distributed in 
!! the same manner as the fields.
module trindiag_create_and_write

  implicit none

  private

  !> Create and/or write the given variable depending on the values
  !! of the flags gnostics%create and gnostics%wryte
  public :: create_and_write_variable

  !> Create and/or write the given variable depending on the values
  !! of the flags gnostics%create and gnostics%wryte
  !! This version will not attempt to read the variable during a
  !! replay, useful for constants such as input parameters, 
  !! normalisations
  public :: create_and_write_variable_noread


  interface create_and_write_variable
#{generators.map{|g| "     "+"module procedure " + g.procedure_name}.join("\n")}
  end interface create_and_write_variable

  interface create_and_write_variable_noread
#{generators_noread.map{|g| "     "+"module procedure " + g.procedure_name}.join("\n")}
  end interface create_and_write_variable_noread

contains

#{generators.map{|g| g.function_string}.join("\n")}

#{generators_noread.map{|g| g.function_string}.join("\n")}

end module trindiag_create_and_write

EOF


File.open(ARGV[-1], 'w'){|f| f.puts string}
