!> A module for writing the global quantities such as
!! fusion power to the the netcdf file.
module trindiag_globals
  use trindiag_config, only: trindiag_type
  use trinity_type_module, only: globals_type
  implicit none
  contains
    subroutine calculate_globals(globals)
      use nteqns_arrays, only: temp_grid, dens_grid, omega_grid
      use nteqns_arrays, only: alph_power, aux_power, net_power, radiate_power
      use nteqns, only: volint
      use trinity_input, only: nrad, m_ion, vtfac, fluxlabel_option_switch
      real, dimension(nrad) :: tmp_grid
      type(globals_type), intent(inout) :: globals

      if (.not. globals%initialized) call initialize_globals(globals)
      globals%fusion_gain = sum(alph_power)*17.6/(3.5*sum(aux_power))
      globals%fusion_power = sum(alph_power)*17.6/3.5
      globals%net_power = net_power
      globals%alph_power = alph_power
      globals%radiate_power = radiate_power
      globals%aux_power = aux_power
      globals%ti_core = temp_grid(1,2)
      globals%te_core = temp_grid(1,1)
      globals%ne_core = dens_grid(1,1)
      globals%ti_edge = temp_grid(nrad,2)
      globals%te_edge = temp_grid(nrad,1)
      globals%ne_edge = dens_grid(nrad,1)
      tmp_grid = 1.0
      call volint(tmp_grid, globals%plasma_volume)
    end subroutine calculate_globals
    
    subroutine initialize_globals(globals)
      use trinity_input, only: nspec
      type(globals_type), intent(inout) :: globals
      globals%initialized = .true.
      allocate(globals%net_power(nspec))
      allocate(globals%aux_power(nspec))
      allocate(globals%alph_power(nspec))
      allocate(globals%radiate_power(nspec))
    end subroutine initialize_globals

    subroutine write_globals(gnostics, globals)
      use nteqns_arrays, only: temp_grid, dens_grid, omega_grid
      use nteqns_arrays, only: current_grid, bmag_axis
      use nteqns_arrays, only: bootstrap_current_import_grid
      use nteqns, only: volint
      use trinity_input, only: nrad, m_ion, vtfac, fluxlabel_option_switch
      use trindiag_create_and_write, only: create_and_write_variable_noread
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: SDATIO_INT
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      type(globals_type), intent(inout) :: globals
      !gnostics%create = .not. gnostics%created_globals
      !gnostics%created_globals = .true.
      !gnostics%wryte = .true.
      real :: tmp
      real, dimension(nrad) :: tmp_grid

      call calculate_globals(globals)

      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "fusion_gain", &
        "t", "Fusion Q, i.e. fusion power/heating power", &
        "1", globals%fusion_gain) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "fusion_power", &
        "t", "Fusion power, i.e. combined alpha and neutron power", &
        "MW", globals%fusion_power) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "net_power", &
        "t", "Net heating power, i.e. auxiliary heating + alpha heating&
        & - radiative losses ", &
        "MW", sum(globals%net_power)) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "alpha_power_total", &
        "t", "Total alpha heating power", &
        "MW", sum(globals%alph_power)) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "aux_power_total", &
        "t", "Total auxiliary heating power (beams, RF heating, etc)", &
        "MW", sum(globals%aux_power)) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "radiate_power_total", &
        "t", "Total radiated power (excluding neutrons) ", &
        "MW", sum(globals%radiate_power)) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "alpha_power", &
        "tspec,t", "Alpha heating power", &
        "MW", globals%alph_power) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "aux_power", &
        "tspec,t", "Auxiliary heating power (beams, RF heating, etc)", &
        "MW", globals%aux_power) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "radiate_power", &
        "tspec,t", "Radiated power (excluding neutrons) ", &
        "MW", globals%radiate_power) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "ne_core", &
        "t", "Core electron density ", &
        "10^20 m^-3", globals%ne_core) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "ne_edge", &
        "t", "Edge electron density ", &
        "10^20 m^-3", globals%ne_edge) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "ti_core", &
        "t", "Core main ion (i.e. the first ion species) temperature  ", &
        "keV", globals%ti_core) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "ti_edge", &
        "t", "Edge main ion (i.e. the first ion species) temperature  ", &
        "keV", globals%ti_edge) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "te_core", &
        "t", "Core electron temperature  ", &
        "keV", globals%te_core) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "te_edge", &
        "t", "Edge electron temperature  ", &
        "keV", globals%te_edge) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "omega_core", &
        "t", "Core bulk toroidal angular velocity  ", &
        "rad s^-1", omega_grid(1)) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "omega_edge", &
        "t", "Core bulk toroidal angular velocity  ", &
        "rad s^-1", omega_grid(1)) 
      call volint(current_grid, tmp)
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "plasma_current", &
        "t", "Total plasma current (0 when not available)", &
        "MA", tmp) 
      call volint(bootstrap_current_import_grid, tmp)
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "bootstrap_current_import", &
        "t", "Total bootstrap current imported from input data (see notes on &
        &bootstrap_current_import_grid); (0 when not available)", &
        "MA", tmp) 
      call create_and_write_variable_noread(gnostics, gnostics%rtype, &
        "plasma_volume", &
        "t", "Total plasma volume", &
        "m^-3", globals%plasma_volume) 
      

    end subroutine write_globals
end module trindiag_globals

