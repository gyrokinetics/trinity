!> A module for writing out intermediate quantities used
!! in the Gauss-Newton or Levenberg-Marquardt iteration
!! to solve for the steady state profiles 
module trindiag_iteration
  use trindiag_config, only: trindiag_type
  public :: write_matrix_solve
  contains 
    !> Write quantities used in the matrix solve.
    !! matrix * nt_new = rhs. NB rhs is not the old 
    !! profiles, just the right hand side of the equation.
    !! matrix is just J, the jacobian for Gauss-Newton,
    !! and is (J^T J + lambda diag [J^T J]) for Levenberg-Marquardt  
    subroutine write_matrix_solve(gnostics, matrix, jacobian, rhs)
      use simpledataio, only: add_dimension, set_count
      use simpledataio, only: variable_exists
      use trindiag_create_and_write, only: create_and_write_variable
      use mp_trin, only: proc0

      type(trindiag_type), intent(inout) :: gnostics
      real, dimension (:,:), intent(inout) :: matrix
      real, dimension (:,:), intent(inout) :: jacobian
      real, dimension (:), intent(inout) :: rhs

      if (.not. variable_exists(gnostics%sfile, "iteration_matrix")) then
        !gnostics%created_iteration = .true.
        !gnostics%create = .true.
        !call add_dimension(gnostics%sfile, "nmat", size(rhs), "", "")
      !else
        !gnostics%create = .false.
      end if
      !gnostics%wryte = .true.

      call create_and_write_variable(gnostics, gnostics%rtype, "iteration_matrix", &
        "mrow,mcol,iter,t", &
         "The matrix used to iterate the profiles in the minimisation &
         & problem: iteration_matrix * profiles_new = rhs. See trin_norm_update.pdf", &
         "TBC", matrix) 
      

      call create_and_write_variable(gnostics, gnostics%rtype, "rhs", &
        "mcol,iter,t", &
         "The rhs in the minimisation problem: iteration_matrix * nt_new = rhs. &
         & For Gauss-Newton interation, roughly speaking this is &
         & dprof/dt - d flux/dr + S: the minimisation problem seeks to find &
         & profiles such that this expression is zero. &
         & See trin_norm_update.pdf", &
         "TBC", rhs) 

      !! We only write 1 iteration at a time
      if (gnostics%parallel .or. proc0) then
        call set_count(gnostics%sfile, "iteration_matrix", "iter", 1)
        call set_count(gnostics%sfile, "rhs", "iter", 1)
      end if


    end subroutine write_matrix_solve

    subroutine write_current_solution(gnostics, nt)
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: syncfile
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics
      real, dimension (:), intent(inout) :: nt
      call create_and_write_variable(gnostics, gnostics%rtype, "nt_current", &
        "mcol,iter,t", &
         "The current solution of the matrix solve. Useful &
         & for debugging", &
         "mixed", nt) 
      if (gnostics%parallel .or. proc0) then
         call syncfile(gnostics%sfile)
       end if
    end subroutine write_current_solution

    subroutine write_convergeval(gnostics, convergeval)
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: syncfile
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics
      real, intent(inout) :: convergeval
      call create_and_write_variable(gnostics, gnostics%rtype, "convergeval", &
        "iter,t", &
         "The current value of the steady state convergence parameter", &
         "1", convergeval) 
      if (gnostics%parallel .or. proc0) then
         call syncfile(gnostics%sfile)
       end if
    end subroutine write_convergeval

    subroutine trindiag_set_iter(gnostics, iter)
      use simpledataio, only: increment_start
      use mp_trin, only: proc0
      use simpledataio, only: set_dimension_start, syncfile, SDATIO_INT
      use trindiag_create_and_write, only: create_and_write_variable
      type(trindiag_type), intent(inout) :: gnostics
      integer, intent(inout) :: iter
      if (gnostics%parallel .or. proc0) then
        call set_dimension_start(gnostics%sfile, "iter", iter)
        call syncfile(gnostics%sfile)
      end if
      call create_and_write_variable(gnostics, SDATIO_INT, "iter_final", &
        "t", &
        "The final number of iterations needed to converge each timestep", &
        "1", iter) 
      !call syncfile(gnostics%sfile)
    end subroutine trindiag_set_iter
end module trindiag_iteration
