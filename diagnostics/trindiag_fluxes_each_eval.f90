!> A module for writing out the fluxes returned from
!! get_fluxes, after every call to get_fluxes, rather
!! than at each iteration
module trindiag_fluxes_each_eval
  use trindiag_config, only: trindiag_type
  use flux_results, only: flux_results_type
  implicit none
  contains

    subroutine trindiag_advance_eval(gnostics)
      use simpledataio, only: increment_start, syncfile
      use mp_trin, only: proc0
      type(trindiag_type), intent(inout) :: gnostics
      if (gnostics%parallel .or. (proc0.and.gnostics%serial_netcdf4)) then
        call increment_start(gnostics%sfile, "eval")
        call syncfile(gnostics%sfile)
      end if
    end subroutine trindiag_advance_eval
    
    subroutine write_fluxes_each_eval(gnostics, flx)
      use trindiag_create_and_write, only: create_and_write_variable
      use trindiag_create_and_write, only: create_and_write_variable_noread
      use mp_trin, only: timer_local
      type(trindiag_type), intent(inout) :: gnostics
      type(flux_results_type), intent(inout) :: flx
      !if (gnostics%parallel .or. gnostics%serial_netcdf4) then
        call create_and_write_variable(gnostics, gnostics%rtype, "pflx_cc_eval", &
          "cc,tspec,jac,eval", &
           "Particle flux at cell centres", "Gamma_gB", flx%p_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_cc_eval", &
          "cc,tspec,jac,eval", &
           "Heat flux at cell centres", "Q_gB", flx%q_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, "heat_cc_eval", &
          "cc,tspec,jac,eval", &
           "Turbulent heating at cell centres", "TBC", flx%heat_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, "lflx_cc_eval", &
          "cc,jac,eval", &
           "Momentum flux at cell centres", "Pi_gB", flx%l_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_relative_change_eval", &
          "cc,tspec,jac,eval", &
           "Relative change in heat flux at cell centres &
           & from one flux call to the next.", "1", flx%q_relative_change) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_cc_conv", &
          "cc,tspec,jac,conv,iter,t", &
           "Heat flux at cell centres showing convergence&
          & for each iteration", "Q_gB", flx%q_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_turb_cc_conv", &
          "cc,tspec,jac,conv,iter,t", &
           "Turbulent heat flux at cell centres showing convergence&
          & for each iteration", "Q_gB", flx%q_turb_cc) 

        call create_and_write_variable_noread(gnostics, gnostics%rtype, &
          "wall_clock_each_eval", "eval", &
          "The wall clock time for each flux call", &
          "s", timer_local() - gnostics%wallclock_last_eval) 
        call create_and_write_variable_noread(gnostics, gnostics%rtype, &
          "wall_clock_eval", "eval", &
          "The wall clock time since start after each flux call", &
          "s", timer_local() - gnostics%wallclock_start) 
        call create_and_write_variable(gnostics, gnostics%rtype, "pflx_damp_eval", &
          "cc,tspec,jac,eval", &
           "Particle flux at cell centres used to damp grid scale oscillations&
           & enabled when damping > 0.0", "Gamma_gB", flx%p_damp) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_damp_eval", &
          "cc,tspec,jac,eval", &
           "Heat flux at cell centres used to damp grid scale oscillations&
           & enabled when damping > 0.0", "Q_gB", flx%q_damp) 
        call create_and_write_variable(gnostics, gnostics%rtype, "lflx_damp_eval", &
          "cc,jac,eval", &
           "Momentum flux at cell centres used to damp grid scale oscillations&
           & enabled when damping > 0.0", "Pi_gB", flx%l_damp) 
        call create_and_write_variable(gnostics, gnostics%rtype, "pflx_oscill_eval", &
          "tspec,eval", &
           "Particle flux damping factor", "1", flx%p_oscill) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_oscill_eval", &
          "tspec,eval", &
           "Heat flux damping factor", "1", flx%q_oscill) 
        call create_and_write_variable(gnostics, gnostics%rtype, "lflx_oscill_eval", &
          "eval", &
           "Momentum flux damping factor", "Pi_gB", flx%l_oscill) 
        call create_and_write_variable(gnostics, gnostics%rtype, "qflx_oscill_measure_eval", &
          "cc,tspec,eval", &
           "Heat flux damping factor", "1", flx%q_oscill_measure) 

        gnostics%wallclock_last_eval = timer_local()
      !end if
    end subroutine write_fluxes_each_eval

    subroutine write_neo_fluxes_each_eval(gnostics, flx)
      use trindiag_create_and_write, only: create_and_write_variable
      type(trindiag_type), intent(inout) :: gnostics
      type(flux_results_type), intent(inout) :: flx
      !if (gnostics%parallel .or. gnostics%serial_netcdf4) then
        call create_and_write_variable(gnostics, gnostics%rtype, &
          "qflx_neo_cc_eval", "cc,tspec,jac,eval", &
           "Neoclassical heat flux at cell centres", "Q_gB", flx%q_neo_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, &
          "lflx_neo_cc_eval", "cc,jac,eval", &
           "Neoclassical momentum flux at cell centres", "Pi_gB", flx%l_neo_cc) 
      !end if
    end subroutine write_neo_fluxes_each_eval

    subroutine write_geo_each_eval(gnostics)
      use trindiag_create_and_write, only: create_and_write_variable
      use trinity_input, only: ncc, fork_flag
      use mp_trin, only: group_to_all
      use nteqns_arrays, only: grho_cc, area_cc
      type(trindiag_type), intent(inout) :: gnostics
      if (gnostics%parallel .or. gnostics%serial_netcdf4) then
        call create_and_write_variable(gnostics, gnostics%rtype, &
          "grho_cc_eval", "cc,eval", &
           "<|grad rho|>", "TBC", grho_cc) 
        call create_and_write_variable(gnostics, gnostics%rtype, &
          "area_cc_eval", "cc,eval", &
           "Flux surface area", "TBC", area_cc) 
      end if

    end subroutine write_geo_each_eval
    subroutine write_flux_option_each_eval(gnostics, flux_option_switch)
      use trindiag_create_and_write, only: create_and_write_variable
      use simpledataio, only: SDATIO_INT
      type(trindiag_type), intent(inout) :: gnostics
      integer, intent(inout) :: flux_option_switch
      if (gnostics%parallel .or. gnostics%serial_netcdf4) then
        call create_and_write_variable(gnostics, SDATIO_INT, &
          "flux_option_switch_eval", "eval", &
          "The flux option selected for the given flux evaluation", &
          "none", flux_option_switch) 
      end if

    end subroutine write_flux_option_each_eval

    subroutine trindiag_set_conv(gnostics, conv)
      use simpledataio, only: increment_start
      use mp_trin, only: proc0
      use simpledataio, only: set_dimension_start, syncfile, SDATIO_INT
      use trindiag_create_and_write, only: create_and_write_variable
      type(trindiag_type), intent(inout) :: gnostics
      integer, intent(inout) :: conv
      if (gnostics%parallel .or. proc0) then
        call set_dimension_start(gnostics%sfile, "conv", conv)
        call syncfile(gnostics%sfile)
      end if
        call create_and_write_variable(gnostics, SDATIO_INT, &
          "conv_current", "eval", &
          "The current value of conv_count", &
          "none", conv) 
        call create_and_write_variable(gnostics, SDATIO_INT, &
          "conv_final", "iter,t", &
          "The value of conv_count for each iteration", &
          "none", conv) 
    end subroutine trindiag_set_conv
end module trindiag_fluxes_each_eval
