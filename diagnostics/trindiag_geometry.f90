!> A module for writing the geometry, e.g. q, shat 
!!  to the netcdf file.
module trindiag_geometry
  use trindiag_config, only: trindiag_type
  implicit none
  contains
    subroutine write_geometry(gnostics)
      !use nteqns_arrays, only: qval_grid, shat_grid, kappa_grid, bmag_axis
      !use nteqns_arrays, only: shift_grid, rmajor_grid, betaprim_grid
      !use nteqns_arrays, only: kapprim_grid, delta_grid, deltprim_grid
      !use nteqns_arrays, only: beta_grid, bmag_grid, zeff_grid
      !use nteqns_arrays, only: rhotor_grid, rmin_grid, drhotordrho_grid
      !use nteqns_arrays, only: drmindrho_grid, grho_grid, area_grid
      !use nteqns_arrays, only: bmag_old_grid, btori_grid, rmin_grid
      use nteqns_arrays, only: bmag_vac_geo, rmajor_axis, bmag_axis
      !use nteqns_arrays, only: psipol_grid, dpsipoldrho_grid
      !use nteqns_arrays, only: bgeo_grid, bunit_grid, current_grid
      use trindiag_create_and_write, only: create_and_write_variable
      implicit none
      type(trindiag_type), intent(inout) :: gnostics
      !gnostics%create = .not. gnostics%created_geometry
      !gnostics%created_geometry = .true.
      !gnostics%wryte = .true.

      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"rmin_grid", "rad,t", &
         !"(Rout-Rin)/2 at the height of the axis . &
         !& Note normalisation is amin_ref not aref. ", "amin_ref", rmin_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"rmajor_grid", "rad,t", &
         !"(Rout+Rin)/2 at the height of the axis  ", "aref", rmajor_grid) 
      call create_and_write_variable(gnostics, gnostics%rtype,  &
        "rmajor_axis", "t", &
         "Major radius of the magnetic axis.", "aref", rmajor_axis) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"qval_grid", "rad,t", &
         !"Magnetic safety factor ", "1", qval_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"shat_grid", "rad,t", &
         !"Magnetic shear (rho/q)(dq/drho) ", "1", shat_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"kappa_grid", "rad,t", &
         !"Elongation ", "1", kappa_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"shift_grid", "rad,t", &
         !"Shafranov shift (d rmajor/d rho) ", "aref", shift_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"betaprim_grid", "rad,t", &
         !"Pressure gradient  - 8 pi * ptot / Bref^2 * &
         !& (1/ptot dptot/drho)  ", "1", betaprim_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"kapprim_grid", "rad,t", &
         !"Gradient of elongation (d kappa / d rho)  ", "1", kapprim_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"delta_grid", "rad,t", &
         !"Triangularity  ", "1", delta_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"deltprim_grid", "rad,t", &
         !"Gradient of triangularity (d delta / d rho)  ", "1", deltprim_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"beta_grid", "rad,t", &
         !"Magnetic beta  ", "TBC", beta_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"rhotor_grid", "rad,t", &
        !"Square root of the toroidal flux", "psitor_a^0.5", rhotor_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"drhotordrho_grid", "rad,t", &
         !"Gradient of square root of the toroidal flux with &
         !& respect to the Trinity flux label (rho)", "psitor_a^0.5", drhotordrho_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"psipol_grid", "rad,t", &
        !"Poloidal flux (0 at the centre, 1 at the edge)", "psipol_a", psipol_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"dpsipoldrho_grid", "rad,t", &
        !"Gradient of the poloidal flux with respect to rho", "psipol_a", dpsipoldrho_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"rmin_grid", "rad,t", &
         !"Minor radius (Miller r, or half width of the flux surface at the &
         !& midplane, depending on geometry)  ", "amin_ref", rmin_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"drmindrho_grid", "rad,t", &
         !"Gradient of minor radius (Miller r or half-width in the midplane) &
         !& wrt rho ", "aref", drmindrho_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"grho_grid", "rad,t", &
         !"Flux surface average of grad rho  ", "aref", grho_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"area_grid", "rad,t", &
         !"Area of flux surface ", "m^2 ?", area_grid) 


      !> Here we write all of the information about the magnetic field
      !! which may be needed to cope with different flux tube normalisations. 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"bmag_grid", "rad,t", &
         !"Normalising magnetic field . This is &
        !& equal to btori(psi)/rmajor(psi)&
        !& where btori is the poloidal current flux function, &
        !& often referred to as f, I, or T and rmajor is R_center, &
        !& that is (Rout+Rin)/2. Note that most flux tube codes make &
        !& different choices for the normalising magnetic fields to this. All&
        !& quantities within Trinity are normalised using this magnetic&
        !& field and then converted to and from the flux code normalisation&
        !& at the point the flux code is called. Note also that this is different&
        !& to bgeo_grid which is btori(psi)/rmajor(LCFS)", "Tesla", bmag_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"bmag_old_grid", "rad,t", &
         !"The old normalising field used prior to Oct 2015 ", "Tesla", &
         !bmag_old_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"bunit_grid", "rad,t", &
         !"The normalising field used by GA tools like TGLF, NEO, &
         !& equal to (1/r)(d psi_toroidal/dr)/(2 pi) ", "Tesla", &
         !bunit_grid) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"btori_grid", "rad,t", &
         !"Poloidal current flux function (ie. R B_t); known as &
        !& f, I or T depending on which paper you read. ", "Tesla m", btori_grid) 
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "bmag_axis", &
        "t", "Magnetic field strength on axis (0 when not available)", &
        "T", bmag_axis) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"bgeo_grid", "rad,t", &
         !"The normalising magnetic field used for GS2 and GRYFX,&
        !& equal to btori(psi)/R_centre(LCFS) ", "Tesla", bgeo_grid) 
      call create_and_write_variable(gnostics, gnostics%rtype, &
        "bmag_vac_geo", &
        "t", "Vacuum toroidal magnetic field strength at R_geo where &
        & R_geo = R_center(LCFS)&
        & = (Rout+Rin)/2 at the LCFS. NB this is the vacuum field: as &
        & btori varies with flux label this quantity is undefined when there&
        & is a plasma. Note additionally, however, that R_geo is a plasma&
        & quantity which varies with time. Thus, although this quantity&
        & relates to the vacuum field, it varies with time. (Set to 0&
        & when not available.", &
        "T", bmag_vac_geo) 
      !call create_and_write_variable(gnostics, gnostics%rtype,  &
        !"current_grid", "rad,t", &
         !"Toroidal current density (0 when not available)", "A m^-3", &
         !current_grid) 



    end subroutine write_geometry
end module trindiag_geometry
