
!> A module which contains a series of high level tests used in the
!! linear and nonlinear test cases, as well as a driver function 
!! which runs GS2.
module functional_tests_trinity
  use unit_tests_trin

  !> Check that the relative error of sum(alph_power) with
  !! respect to the given rslt is less than
  !! err.
  !! Returns .true. for pass and .false. for fail
  public :: check_alpha_power

  public :: check_qflux_cc_1

  !> Check the results of the temperature grid
  !! for the specified species
  public :: check_temp_grid

  !> Check the results of the temperature gradient at cell centres
  !! for the specified species
  public :: check_tprim_cc

  !> Check the results of the density gradient at cell centres
  !! for the specified species
  public :: check_fprim_cc
 
  !> Check the results of the diffusion benchmark
  public :: diffusion_check
   
  !> Run gs2 and then call the test_function to check the results 
  !! corresponding to the input file provided. test_name should 
  !! be a character(*) containing the title of the tests, and 
  !! test_function should be a logical function which returns
  !! .true. for pass and false for .fail.
  public :: test_trinity
  



  private

  !> A temporary variable to store the trinity time in for the diffusion tests.
  !! This is a hack which should be fixed.
  real :: time

  real, dimension(:,:,:), pointer :: qflx_cc


contains

  subroutine announce_functional_test(test_name)
    use unit_tests_trin, only: verbosity
    character(*), intent(in) :: test_name
    if (verbosity() .gt. 0) call print_with_stars('Starting functional test: ', test_name)
  end subroutine announce_functional_test
  subroutine close_functional_test(test_name)
    use unit_tests_trin, only: verbosity
    character(*), intent(in) :: test_name
    if (verbosity() .gt. 0) call print_with_stars('Completed functional test: ', test_name)
  end subroutine close_functional_test

  subroutine check_alpha_power(check_result, rslt, err)
    use unit_tests_trin
    use nteqns_arrays, only: alph_power
    use mp_trin, only: proc0
    real, intent(in) :: rslt, err
    logical,intent(inout) :: check_result
    if (proc0) then
      write (*,*) 'alph_power', sum(alph_power)
      call announce_check('sum(alph_power)')
      call process_check(check_result, agrees_with(sum(alph_power), rslt, err),&
        'sum(alph_power)')
    end if
    !check_alpha_power = .true.
  end subroutine check_alpha_power
  
  subroutine check_qflux_cc_1(check_result, rslt, err)
    use unit_tests_trin
    use mp_trin, only: proc0
    real, intent(in), dimension(:) :: rslt
    real, intent(in) :: err
    logical,intent(inout) :: check_result
    if (proc0) then
      call announce_check('qflux_cc')
      write (*,*) 'qflux_cc', qflx_cc(:,2,1)
      call process_check(check_result, &
        agrees_with(qflx_cc(:,2,1), rslt, err), 'qflx_cc')
    end if
  end subroutine check_qflux_cc_1

  subroutine check_temp_grid(check_result, spec, rslt, err)
    use unit_tests_trin
    use nteqns_arrays, only: temp_grid
    use mp_trin, only: proc0
    real, intent(in), dimension(:) :: rslt
    real, intent(in) :: err
    integer, intent(in) :: spec
    logical,intent(inout) :: check_result
    character(len = 25) :: message
    write (message, "(A,I2)") 'temp_grid for species ', spec
    if (proc0) then
      call announce_check(message)
      write (*,*) 'spec', spec, 'temp_grid', temp_grid(:,spec)
      call process_check(check_result, &
        agrees_with(temp_grid(:,spec), rslt, err), message)
    end if
  end subroutine check_temp_grid

  subroutine check_tprim_cc(check_result, spec, rslt, err)
    use unit_tests_trin
    use nteqns_arrays, only: tprim_cc
    use mp_trin, only: proc0
    real, intent(in), dimension(:) :: rslt
    real, intent(in) :: err
    integer, intent(in) :: spec
    logical,intent(inout) :: check_result
    character(len = 25) :: message
    write (message, "(A,I2)") 'tprim_cc for species ', spec
    if (proc0) then
      call announce_check(message)
      write (*,*) 'spec', spec, 'tprim_cc', tprim_cc(:,spec)
      call process_check(check_result, &
        agrees_with(tprim_cc(:,spec), rslt, err), message)
    end if
  end subroutine check_tprim_cc

  subroutine check_fprim_cc(check_result, spec, rslt, err)
    use unit_tests_trin
    use nteqns_arrays, only: fprim_cc
    use mp_trin, only: proc0
    real, intent(in), dimension(:) :: rslt
    real, intent(in) :: err
    integer, intent(in) :: spec
    logical,intent(inout) :: check_result
    character(len = 25) :: message
    write (message, "(A,I2)") 'fprim_cc for species ', spec
    if (proc0) then
      call announce_check(message)
      write (*,*) 'spec', spec, 'fprim_cc', fprim_cc(:,spec)
      call process_check(check_result, &
        agrees_with(fprim_cc(:,spec), rslt, err), message)
    end if
  end subroutine check_fprim_cc

  subroutine diffusion_check(check_result, err)
    use unit_tests_trin
    use mp_trin, only: proc0
    use nteqns_arrays, only: temp_grid, mom_grid
    use nteqns_arrays, only: rad_grid
    use trinity_input, only: dfac, evolve_flow
    real, intent(in) :: err
    logical,intent(inout) :: check_result

    if (proc0) then
      call announce_check('diffusion_check')
      if (evolve_flow) then 
        call process_check(check_result, &
          agrees_with(mom_grid(:), sqrt(1.0/(1.0+time))*exp(-rad_grid**2/(4.0*dfac*(1.0+time))), &
          err), 'flow')
      else
        call process_check(check_result, &
          agrees_with(temp_grid(:,1), sqrt(1.0/(1.0+time))*exp(-rad_grid**2/(4.0*dfac*(1.0+time))), &
          err), 'electron temp')
        call process_check(check_result, &
          agrees_with(temp_grid(:,2), sqrt(1.0/(1.0+time))*exp(-rad_grid**2/(4.0*dfac*(1.0+time))), &
          err), 'ion temp')
      end if
    end if
  end subroutine diffusion_check

  subroutine test_trinity(test_name, test_function)
    !use unit_tests_trin, only: functional_test_flag, ilast_step
    !use unit_tests_trin
    use trinity_main, only: run_trinity, finish_trinity, functional_test_flag
    use trinity_main, only: trinity_type

    implicit none
    character(*), intent(in) :: test_name
    logical, external :: test_function
    type(trinity_type), target :: trin

    functional_test_flag = .true.

    call run_trinity(trin)

    time = trin%clock%time

    ! The line below is terrible coding which violates the trinity
    ! data model on many levels. Someone with energy and enthusiasm
    ! should fix it!
    qflx_cc => trin%fx%flx%q_cc

    call announce_test('results')
    call process_test(test_function(), 'results')

    !write (*,*) 'ORANGES!!'
    call finish_trinity(trin)

  end subroutine test_trinity

end module functional_tests_trinity

