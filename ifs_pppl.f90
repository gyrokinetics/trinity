module ifs_pppl

  public :: ip_chi

contains

  subroutine ip_chi(RLT,RLN,RLNe,q,kappa, &
       shat,zth,nbeam,tau,eps,gnu, &
       g_perp,rmajor,rho_i,v_ti,rho_e,v_te,etg,rlte, &
       RLTcrit,RLTcritz,chi_0,gfac,chi_i,chi_e,gamma)
!
! The formulas embodied in this subroutine are documented in the Physics
! of Plasmas article entitled "Quantitative Predictions of Tokamak Energy 
! Confinement from First-Principles Simulations with Kinetic Effects", 
! by M. Kotschenreuther, W. Dorland, M.A. Beer, and G.W. Hammett, 
! Vol. 2, p. 2381, (1995).  Extensions to non-circular cross-sections are
! described below. 
!
! There is a significant typographical error in that paper.  R/Ln* is 
! defined to be max(6,R/Ln); it should be min(6,R/Ln) as defined in 
! this subroutine.
!
! Also, note that in deriving these formulas, we assumed that the 
! density gradient scale lengths for the different species were equal. 
! This is an approximation that needs to be relaxed.
!
! As emphasized in the paper, these formulas were derived numerically and 
! are therefore not trustworthy outside a particular region of parameter 
! space.  For example, we did not parameterize the heat flux in the weak 
! magnetic shear limit; thus, one should not use the model in this limit.
! I have attempted to reduce related strange numerical behaviors by 
! limiting some inputs to be roughly within their range of validity.  
! 
! Questions, problems, errors, etc. should be reported to
! bdorland@zonker.ph.utexas.edu or bdorland@pppl.gov.  
!
! I will reply as quickly as possible.  
! 
! Stiffness:
! **********
! For many cases that we have simulated, the transport equations
! tend to be very stiff.  That is, the plasma temperature gradient scale 
! length tends to adjust itself to be close to the critical gradient scale
! length over some region of the plasma, because chi becomes very large
! very fast for shorter temperature gradient scale lengths.  Typically, 
! we have had to be very careful with the numerical algorithm used in the 
! transport equation solver with this experience in mind.  The details 
! of our implementation are available to anyone that is interested.
!
! Geometry:
! ********
!
! The nonlinear simulations that were done to obtain these formulas were 
! mostly done in a simplified geometry, using a shifted circle, low beta, 
! high aspect ratio expansion.  Some modifications due to more sophisticated 
! geometrical models have been calculated and have been included here, 
! but should be considered preliminary.  There are two important issues 
! that must be noted.  First, we derived our formulas using a different 
! radial coordinate.  Second, since we are actually calculating the 
! transport coefficients in general geometry, we require less assumptions 
! for the form of the transport equation to be solved.  
!
! Let me describe the <|grad rho|> issue first:
!
!     The database standard modeling assumptions that were agreed upon for
!     this exercise include the assumption that the anomalous fluxes for 
!     non-circular cross-sections are simply related to the anomalous 
!     fluxes for related circular cross-section plasmas.  That is, in order 
!     to get the factor of < |grad rho|**2 > that appears as a coefficient 
!     of chi in the energy transport equations, one assumes that 
!
!     chi_anom_general = chi_anom_circular * (grad rho).
!
!     One need not make this assumption; one can just calculate the quantity 
!     chi_anom_general directly.  One would then have a transport equation
!     of the form
!
!  (3/2) d(n T)/dt = (1/V') d/drho [V' <|grad rho|> n chi d/drho(T)] + ...
!
!     in which (grad rho) appears to the first power, rather than the second,
!     and chi is the thermal diffusivity from a general geometry theory.
!
!     This is arguably the better way to proceed, since 
!
!          V' <|grad rho|> = A,
!
!     where A is the surface area.  In this form, the quantity 
!
!          -n chi dT/drho 
!
!     can be identified as the heat flux per unit area, a natural 
!     quantity from a theoretical/simulation point of view.  This chi is 
!     the quantity returned by this subroutine.
!
!     If you are solving the transport equations in the form that
!     the ITER Expert Group agreed upon, e.g., 
!
!  (3/2) d(n T)/dt = (1/V') d/drho [V' <|grad rho|**2> n chi d/drho(T)] + ...
!
!     then you need to multiply the chi_i and chi_e reported by this 
!     subroutine by the factor <|grad rho|>/<|grad rho|**2>.  This should
!     result in only small corrections to the predicted profiles.
!
! The choice of radial coordinate is more difficult to resolve:
!
!     We did not use the sqrt(toroidal flux) radial coordinate in our 
!     non-circular cross-section simulations.  Instead, we used "rho_d", 
!     where rho_d is defined to be the average horizontal minor radius 
!     at the elevation of the magnetic axis, normalized to the value of 
!     this quantity at the LCFS.  
!
!     In other words, denote by "d" the horizontal diameter of a given flux 
!     surface measured at the elevation of the magnetic axis.  Denote by
!     "D" the horizontal diameter of the last closed flux surface at the 
!     elevation of the magnetic axis.  Then rho_d = d/D.  I believe this 
!     is variable number 67 (RMINOR) in the ITER Profile Database Standard 
!     List.  
!
!     It is not difficult to allow for an arbitrary radial coordinate
!     in one's transport code.  One must obtain all of the radial 
!     quantities as functions of rho_d rather than rho via interpolation.  
!
!     However, I do not expect everyone to go to this length to test our 
!     model, since you agreed to use the sqrt(toroidal flux) definition 
!     of the radial coordinate.  Thus, I suggest the following alternative:
!     Simply use the rho_d coordinate to define the scale lengths that 
!     appear in the formulas below.  For most quantities (such as R/LT), 
!     this simply amounts to including an additional factor d rho/d rho_d 
!     in the expressions passed to this subroutine.  While not completely 
!     correct, this workaround captures the dominant effect, related to 
!     evaluating the flux near the critical gradient.
!
! ****** Summary of comments:  ***********
!
! (1) The general geometry extensions to the IFS/PPPL model were derived 
!     using rho = d/D = RMINOR as the radial coordinate.  To be 
!     most accurate, the transport equation should be solved using d/D 
!     as the radial coordinate.
!
!     If you use rho proportional to sqrt(toroidal flux) instead of rho=d/D
!     as your radial coordinate, you should at least carefully define the 
!     scale lengths as indicated below (using rho=d/D).
!     
! (2) This routine should be used to return the thermal transport
!     coefficients (chi_i, chi_e) for energy transport equations of the form
!
!  (3/2) d(n T)/dt = (1/V') d/drho [V' <|grad rho|> n chi d/drho(T)] + ...
!                                           
!     Note that <|grad rho|> only appears to the first power according
!     to this definition of chi.  If your code is hardwired to solve an
!     equation of the form 
!
!  (3/2) d(n T)/dt = (1/V') d/drho [V' <|grad rho|**2> n chi d/drho(T)] + ...
!
!     then multiply the chi_i and chi_e obtained from this routine by 
!     the factor <|grad rho|>/<|grad rho|**2>.
!     
! *****************************************************************
!
!     RLT  R/L_Ti, where R = the major radius and 
!                  1/L_Ti = -1/T_i dT_i/drho_d
!     RLN  R/L_ni, where 1/L_ni = -1/n_i dn_i/drho_d
!     RLNe R/L_ne, where 1/L_ne = -1/n_e dn_e/drho_d
!     q    The safety factor.
!     kappa  The elongation, defined here to be the 
!          kappa = maximum height/maximum diameter
!     shat == rho_d/q dq/drho_d
!     zth  Thermal Z_eff.  The simulations that were carried out to 
!          generate the formulae in this subroutine assumed the plasma 
!          was composed of a thermal hydrogenic species, thermal carbon, 
!          a hydrogenic beam species, and electrons.  We found that low-Z
!          impurities primarily act to dilute the main ion concentration, 
!          and can accounted for to first order by modifying the 
!          definition of Z_eff.  Some of the more important effects of 
!          the fast ions in the plasma are also partially accounted 
!          for by this parameter, which is:
!          zth == (n_i + 36 n_C)/(n_e - n_beam)
!
!     nbeam == local fast ion (beam) density normalized to the electron
!          density.  
!     tau  == T_i/T_e.  Note that this is opposite to a widely
!                       used convention. 
!     eps  == rho_d/R, the local minor radius normalized to the major radius. 
!     gnu   Dimensionless collisionality parameter.
!          gnu == 2.5e-7 * n_e / (T_e**1.5 T_i**0.5) * rmajor
!          where n_e is in units of cm**-3, T_e and T_i are in eV, and 
!          rmajor is in units of m.  For an R = 2.4 m, 100 eV, 1e13 plasma, 
!          gnu=600.
!     g_perp == velocity shear parameter.  Use g_perp=0 (actual value
!          discussed in the Waltz paper cited below).
!     rmajor == major radius of the plasma
!     rho_i == local thermal gyroradius of thermal hydrogenic species.  
!     v_ti == sqrt(T_i/m_i) where T_i and m_i are the local thermal 
!          hydrogenic temperature and average thermal hydrogenic mass.
!
!     rho_e,v_te,etg,rlte: dummy parameters at present.  Ignore.
!
!     Units: The only dimensional parameters in the inputs are the major
!     radius, rho_i, and v_t.  Their units should be consistent; the chi's
!     that are returned will be in units of rho_i**2 v_ti / rmajor.
!
! OUTPUT:
!     RLTcrit: R/L_Tcrit for ITG mode
!     RLTcritz: R/L_Tcrit for carbon branch
!     chi_0: normalized chi (ignore)
!     gfac: L_Tc/L_T, where L_Tc is the critical temperature gradient 
!        scale length for the deuterium branch of the ITG mode.
!     chi_i: Anomalous ion thermal diffusivity from toroidal ITG mode.
!     chi_e: Anomalous electron thermal diffusivity from toroidal ITG mode.
!
!     This parameterization of chi is not complete.  There are significant
!     neglected physical processes that are known to be important in 
!     many operational regimes.  
!
!     The most significant problems are: 
!
!     (1) Trapped ion/long wavelength ITG modes.  These modes are known 
!     to be unstable for typical edge tokamak parameters.  However, until 
!     we have nonlinear estimates of the associated thermal diffusivity, 
!     these modes are ignored, leading to overly optimistic predictions of
!     edge thermal confinement.  
!     (2) Trapped electron modes, which can alter the stability boundary 
!     significantly for low collisionality.  At high collisionality these 
!     modes are generally stable and thus largely irrelevant.  When present, 
!     they are associated most strongly with particle transport, although 
!     there is also an associated heat transport.  
!     (3) Minority ion density gradients, which can strongly change chi 
!     and LT_crit. 
!     (4) Sheared flows, which are stabilizing.  This includes diamagnetic 
!     and eExB shear flows.
!     (5) Finite beta effects, generally stabilizing.
!     
    implicit none

    real RLT,RLN,RLNe,shat,zth,tau,rmajor,chi_i,q, &
         nbeam,taub,rho_i,v_ti,eps,chi_e,nu,chi_0,g_perp,gnu
    real RLTcrit,RLTcritz,RLTcritg,chi0,f_0,f_z,chiz
    real trln,trlne,tshat,kappa,rho_e,v_te,rlte
    real chie1,chie2,gamma,tgamma,rot,g_fac1,g_facz,gfac
    real etg_rltecrit,f_e
    real reduced
    logical etg
    real RLT_mult
    external reduced

    data RLT_mult/1.0/  ! multiplier on R/L_Tcrit, for sensitivity studies

    taub=tau/(1.-nbeam)
    nu=gnu*0.84
    tRLN=min(abs(RLN),6.0)*sign(1.0,RLN)
    tRLNe=min(abs(RLNe),6.0)*sign(1.0,RLNe)
!
! Formula is not applicable for shat<0.5; does not matter most of the time, 
! since low shear regions of the plasma tend to be in the region affected
! by sawteeth.  We will include the effects of weak or negative
! magnetic shear in a future release.
!

! modified 19 November 2012 to match J. Candy specification
!    tshat=min(max(shat,0.25),2.5)

    if (shat>0.5) then
       tshat=shat
!       tshat=min(shat,2.5)
    else
       tshat=1.0-shat
    end if

!
!     critical ion temperature gradient:
!     (If this number is negative, check the input variable definitions, 
!     especially gnu.  Then ask me about it, at 
!     bdorland@hagar.ph.utexas.edu)
!
    if(q.le.0.) write(*,*) 'q= ',q
    if(taub.le.0.) write(*,*) 'taub= ',taub
    if(eps.le.0.) write(*,*) 'eps= ',eps
    if(zth.le.0.) write(*,*) 'zrh= ',zth
    if(nu.le.0.) write(*,*) 'nu= ',nu
    
    RLTcrit=2.46*(1.+2.78/q**2)**0.26*(zth/2.)**0.7*taub**0.52 &
         *( (0.671+0.570*tshat-0.189*tRLNe)**2 &
         +0.335*tRLNe+0.392-0.779*tshat+0.210*tshat**2) &
         *( 1.-0.942*(2.95*eps**1.257/nu**0.235-0.2126) &
         *zth**0.516/abs(tshat)**0.671)
    RLTcrit=RLTcrit*RLT_mult

!      if(RLTcrit.le.0) then
!         write(*,*) 'rltcrit,q,zth,taub,tshat,trlne,eps,nu'
!         write(*,*) rltcrit,q,zth,taub,tshat,trlne,eps,nu
!         write(*,*)  1.-0.942*(2.95*eps**1.257/nu**0.235-0.2126)
!     .     *zth**0.516/abs(tshat)**0.671
!         write(*,*) (0.671+0.570*tshat-0.189*tRLNe)**2
!     .     +0.335*tRLNe+0.392-0.779*tshat+0.210*tshat**2
!      endif
    RLTcritz=0.75*(1.+taub)*(1.+tshat)*max(1.,3.-2.*tRLNe/3.) &
         *(1.+6.*max(0.,2.9-zth))
    RLTcritz=RLTcritz*RLT_mult

    f_0=11.8 &
         *min(1.,(3./zth)**1.8) &
         *q**1.13 &
         /(1.+tshat**0.84) &
         /taub**1.07 &
         *(1.+6.72*eps/nu**0.26/q**0.96) &
         /(1.+((kappa-1.)*q/3.6)**2)
      
    f_z=7.88 &
         /(1.+tshat) &
         *max(0.25,zth-3) &
         /taub**0.8 &
         /(1.+((kappa-1.)*q/3.6)**2)
     
    chi0=f_0 * rho_i**2*v_ti/rmajor
    chiz=f_z * rho_i**2*v_ti/rmajor

    g_fac1=max(0.,min(esqrt(RLT-RLTcrit),RLT-RLTcrit))
    g_facz=max(0.,min(esqrt(RLT-RLTcritz),RLT-RLTcritz))
    chi_i=max(chi0*g_fac1,chiz*g_facz)

    gfac=RLT/RLTcrit
    chi_0=chi0*sqrt(abs(RLTcrit))
    
    chie1=chi0*g_fac1*1.44*tau**0.4*(q/tshat)**0.3*nu**0.14 &
         *max(0.16667,eps)
    chie1=0.5*chie1*(1.+tRLNe/3.)
    chie2=0.5*max(2.,(1.+RLNe/3.))*0.526*tau*nu**0.22*chiz*g_facz
!
! Correction for n_i/n_e and ratio of heat fluxes rather than chi's:
!
    chi_e=max(chie1,chie2)*(7.-zth)/6.

!      if(chi_i.gt.0.) then
!         write(*,*) chi_i/chi_e,eps,(q/tshat)**0.3
!      endif

!
!     **Preliminary** model of shear flow stabilization.  Based on 
!     work described in Waltz, et al., Phys. of Plasmas, Vol. 1, p. 2229 
!     (1992), and in Hahm and Burrell, Phys. Plasmas 2, 1648 (1995).
!     The fundamental concept of sheared-flow stabilization of turbulence
!     in plasmas originated in the seminal paper by 
!     Biglari, Diamond, Terry, Phys. Plasmas B2, 1 (1990), which was
!     particularly fundamental in understanding H-mode experiments.
!     [There are some roots to this concept in general fluid turbulence 
!     also, such as the shearing rate or eddy turn-over time in Orszag's 
!     EDQNM, general concepts of Kolmogorov scaling, etc. But there are 
!     also important differences such as the 2-D vs. 3-D nature of the
!     plasma vs. fluid nonlinearities, and the existence of the 
!     Kelvin-Helmholtz instability in sheared flows in normal fluids.  
!     This instability is different in magnetized plasmas.)
!
! gperp is essentially the shearing rate d/dr(v_perp), and approximate 
! stabilization is assumed when the shearing rate is of order the linear 
! growth rate (note that gperp has units of 1/sec, i.e. the units of a 
! growth rate).  In the absence of full-torus nonlinear simulations, 
! gperp should include the shear in the perpendicular phase velocity of
! the plasma turbulence waves, but we will assume this is primarily the
! shear in ExB flow for now, and ignore shear in omega/k_theta.  Also, 
! we will ignore the Kelvin-Helmholtz destabilization which Waltz points
! out can occur due to parallel flow shear.  In the limit where v_parallel 
! combines with v_perp to make the net flow purely toroidal (a common 
! assumption), gperp can be written as (Waltz p. 2235):
!
!     gperp=(r/q) d/dr(v_phi/R)
!
! Waltz didn't write down a more general form, but the ballooning
! coordinates he uses implies that it is the shear in the perpendicular 
! flow which matters.  For general geometry and general flows, the above 
! expression still applies if r/q is interpreted as the local value of
! R*B_theta/B, if d/dr is interpreted as a local gradient, and 
! and if v_phi/R is replaced by (B/B_theta)*v_ExB/R.  [This may not be 
! obvious to some, but it is a consequence of the field-line-following
! ballooning coordinates we use, which involve a parallel coordinate and 
! a toroidal angle coordinate, i.e., the toroidal angle (at fixed parallel
! coordinate) is the effective perpendicular coordinate.  This is related 
! to the way one changes the "slab" definition of omega_* into a general
! geometry definition involving k_phi=n/R instead of k_theta, using m=n*q
! etc, and is related to the fact that the properly expressed omega_* is a 
! flux function.] Note that v_phi/R and (B/B_theta)*v_ExB/R are both flux 
! functions, i.e., constant on a flux surface.
!
! Hahm and Burrell derive an expression directly in general geometry,
! using a more formal 2-pt correlation function theory.  It is essentially
! equivalent to the above expression, if one makes the correct assumptions
! above implied by the ballooning coordinates, and if one reduces the Hahm
! expression for practical experimental comparisons by assuming the radial 
! and poloidal correlation lengths are equal. In flux coordinates 
! (dpsi = R*B_theta*dr), and in CGS units (where v_ExB = c*E_r/B),
! this can be written as
!
!     gperp = (R**2*B_theta**2/B) c d**2(Phi)/dpsi**2
!
! where Phi(psi) is the electrostatic potential.  Note that the factor out
! front gives significant poloidal variation, particularly when the 
! Shafranov shift is large so that B_theta is compressed.  This is good,
! since theta=0 is where the bad-curvature drive of the toroidal ITG mode
! is worst.  Thus gperp should be evaluated at theta=0.
!
! Rough growth rate parameterization.  Crudely assume the form of the 
! growth rate for the carbon and deuterium branches is the same, with
! just a different threshold:

    RLTcritg=RLTcrit
    if (chiz*g_facz .gt. chi0*g_fac1) RLTcritg=RLTcritz

    gamma=0.25/(1+0.5*tshat**2)/tau &
         *(1+3.*max(0.,eps-0.16667)) &
         /(1+max(0.,q-3)/15.) &
         *max(0.0,RLT-RLTcritg)*v_ti/rmajor
    tgamma=max(gamma,1.e-8*v_ti/rmajor)

    rot=1.-abs(g_perp)/tgamma
    rot=min(1.,max(0.0,rot))

    chi_i=max(0.,chi_i*rot)
    chi_e=max(0.,chi_e*rot)

    if(etg) then 
       etg_rltecrit=2.184*sqrt(.5+1./q)*(zth/tau)**.61 &
            *(1.-0.85*eps/tshat**.25) &
            *(abs(.1976-.4550*tshat+.1616*RLNe)**.769 &
            +.7813+.2762*tshat+.3967*tshat**2)
       etg_rltecrit=etg_rltecrit*RLT_mult

       f_e=14.*(1.+eps/3.)*(1+0.1*tau)*tau*q/(2+tshat)
       if(RLTe.gt.etg_rltecrit) then
          chi_e=chi_e+f_e*rho_e**2*v_te/rmajor &
               *min((RLTe-etg_RLTecrit)**0.5,(RLTe-etg_RLTecrit))
       endif
    endif

!      chi_i=reduced(eps)
!      chi_e=reduced(eps)

    return
  end subroutine ip_chi
      
  function esqrt(x)
    real :: esqrt
    real, intent (in) :: x
    esqrt=0.
    if(x.gt.0.) esqrt=sqrt(x)
    return
  end function esqrt

  function reduced(chi)

    real :: reduced
    real, intent (in) :: chi

    reduced=chi
      
    return
  end function reduced
      
end module ifs_pppl
