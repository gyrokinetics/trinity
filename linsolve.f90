module linsolve

  implicit none

  public :: lusolve

contains

  ! solves matrix equation A x = B
  ! where A = mat and B is the input sol
  ! after running x = output sol; i.e., input sol is overwritten
  ! returns 0 for success, > 0 otherwise
  !   1 = singular matrix in ludcmp
  function lusolve (mat, sol)

    implicit none

    real, dimension (:), intent (in out) :: sol
    real, dimension (:,:), intent (in out) :: mat

    real :: sgn
    integer, dimension (:), allocatable :: indx
    integer :: lusolve
    integer :: stat

    lusolve = 0

    allocate (indx(size(sol)))

    stat = ludcmp (mat, indx, sgn)
    if (stat==1) lusolve = 1
    call lubksb (mat, indx, sol)

    deallocate (indx)

  end function lusolve
  
  function ludcmp (a, indx, d)
    
    implicit none
    
    real, dimension (:,:), intent (in out) :: a
    integer, dimension (:), intent (out) :: indx
    real, intent (out) :: d

    real, dimension (size(a,1)) :: vv
    real, parameter :: tiny = 1.0e-20
    integer :: j, n, imax
    integer :: ludcmp

    ludcmp = 0

    n = assert_eq(size(a,1),size(a,2), size(indx), 'ludcmp')
    d = 1.0
    vv = maxval(abs(a),dim=2)
    if (any(vv == 0.0)) then
       write (*,*) 'singular matrix in ludcmp'
       ludcmp = 1
    end if
    vv = 1.0/vv
    do j = 1, n
       imax = (j-1) + imaxloc(vv(j:n)*abs(a(j:n,j)))
       if (j /= imax) then
          call swap(a(imax,:),a(j,:))
          d = -d
          vv(imax) = vv(j)
       end if
       indx(j) = imax
       if (a(j,j) == 0.0) a(j,j) = tiny
       a(j+1:n,j) = a(j+1:n,j) / a(j,j)
       a(j+1:n,j+1:n) = a(j+1:n,j+1:n) - spread(a(j+1:n,j),dim=2,ncopies=n-j) * &
            spread(a(j,j+1:n),dim=1,ncopies=n-j)
    end do

  end function ludcmp

  subroutine lubksb (a, indx, b)

    implicit none

    real, dimension (:,:), intent (in) :: a
    integer, dimension (:), intent (in) :: indx
    real, dimension (:), intent (in out) :: b

    integer :: i, n, ii, ll
    real :: summ

    n = assert_eq(size(a,1),size(a,2),size(indx),'lubksb')
    ii = 0
    do i = 1, n
       ll = indx(i)
       summ = b(ll)
       b(ll) = b(i)
       if (ii /= 0) then
          summ = summ - dot_product(a(i,ii:i-1),b(ii:i-1))
       else if (summ /= 0.0) then
          ii = i
       end if
       b(i) = summ
    end do
    do i = n, 1, -1
       b(i) = (b(i) - dot_product(a(i,i+1:n),b(i+1:n))) / a(i,i)
    end do

  end subroutine lubksb

  subroutine swap (a,b)
    real, dimension (:), intent (in out) :: a, b
    real, dimension (size(a)) :: dum
    dum=a
    a=b
    b=dum
  end subroutine swap

  function assert_eq (n1,n2,n3,string)
    character (*), intent (in) :: string
    integer, intent (in) :: n1,n2,n3
    integer :: assert_eq
    if (n1 == n2 .and. n2 == n3) then
       assert_eq=n1
    else
       write (*,*) 'error: an assert_eq failed with this tag:', &
            string
       stop 'program terminated by assert_eq3'
    end if
  end function assert_eq

  function imaxloc (arr)
    real, dimension (:), intent (in) :: arr
    integer :: imaxloc
    integer, dimension (1) :: imax
    imax = maxloc(arr(:))
    imaxloc = imax(1)
  end function imaxloc

end module linsolve
