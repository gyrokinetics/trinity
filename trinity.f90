module trinity_main

  use mp_trin, only: init_mp, finish_mp, scope, job_fork, broadcast
  use mp_trin, only: proc0, nproc, subprocs, allprocs, job
  use file_utils_trin, only: init_file_utils, finish_file_utils
  use file_utils_trin, only: open_output_file, close_output_file
  use file_utils_trin, only: run_name
  use trinity_input, only: ntstep, niter
  use trinity_input, only: fork_flag, flux_groups, load_balance, dyn_load_balance
  use trinity_input, only: evolve_geometry, njobs
  use trinity_input, only: nprofs_evolved
  use nteqns, only: init_nt, distribute_profs, broadcast_profiles
  use chease_ecom, only: update_chease
  use chease_ecom, only: chease_ecom_type, init_chease_ecom
  use trindiag_restart, only: restart_chease_ecom, write_chease_ecom
  use nteqns_arrays, only: temp_grid, temp_cc, broadcast_grids, get_cc_all
  use fluxes, only: init_fluxes, get_fluxes, finish_fluxes
  use calibrate_type_module, only: calibrate_type 
  use trindiag, only: init_trindiag
  use trindiag_config, only: trindiag_type
  use trinity_type_module, only: trinity_clock_type
  use trinity_type_module, only: trinity_control_type
  use trinity_type_module, only: globals_type
  use save_arrays, only: save_arrays_type
  use fluxes, only: fluxes_type
  use flux_results, only: flux_results_type

  implicit none

  type trinity_type
    type(trinity_control_type) :: ctrl
    type(trinity_clock_type) ::  clock
    type(calibrate_type)   ::    calib
    type(globals_type)    ::   globals
    type(trindiag_type)  ::   gnostics
    type(chease_ecom_type) ::   checom
    type(save_arrays_type)  ::    sayv
    type(fluxes_type)        ::     fx
  end type trinity_type

  public :: functional_test_flag
  public :: run_trinity
  public :: finish_trinity
  !public :: trin
  public :: check_restart_from_crash
  public :: trinity_type

  private

  logical :: check_restart_from_crash = .false.

  logical :: error_flag = .false., converged
  real :: old_time
  integer :: focus
  character (10000), target :: cbuff
  logical :: functional_test_flag

  ! nt is a vector containing the current (iteration i+1) density, pressure, and momentum profiles
  ! nt_i is the same vector at iteration i of time level m+1
  ! nt_m is the same vector at time level m
  ! nt_mm1 is the same vector at time level m-1
  real, dimension (:), allocatable :: nt, nt_i, nt_m, nt_mm1

  ! flx is a vector containing the current (iteration i+1) particle, energy, and momentum flux profiles
  ! flx_m is the same vector at time level m
  real, dimension (:), allocatable :: flx, flx_m

  ! turbulent heating (energy exchange) at time level m
  real, dimension (:,:), allocatable :: heat_grid_m

  ! residual of transport equations at time level m
  real, dimension (:), allocatable :: H_m

  logical :: check_error_reset = .false.


contains

  !> Returns the time since the POSIX epoch: the time
  !! in seconds since midnight GMT on the 1 Jan 1970
  function epoch()
    use iso_c_binding
    use mp_trin, only: broadcast, current_focus
    real :: epoch
    real(c_double) :: epoch_double
    interface
      subroutine trinity_c_time(time_out) &
          bind(c, name='trinity_time_double')
        use iso_c_binding
        real(c_double), intent(out) :: time_out
      end subroutine trinity_c_time
    end interface
    call trinity_c_time(epoch_double)
    epoch = epoch_double
    call broadcast(epoch)

  end function epoch

  subroutine run_trinity (trin)

    use trinity_input, only: nrad, ncc
    use trinity_input, only: nspec
    use trinity_input, only: nprofs_evolved, avail_cpu_time
    use nteqns, only: get_nt_vector_from_profs
    use nteqns, only: get_flx_vector_from_fluxes
    use nteqns, only: get_Hm
    use fluxes, only: fluxes_converged
    use trinity_input, only: init_option_restart, init_option_switch
    use trinity_input, only: flux_driver
    use trinity_input, only: nifspppl_initial
    use calibrate, only: init_calibrate, finish_calibrate, calibrate_store
    use calibrate, only: calibrate_adjust, calibrate_restart
    use trindiag, only: trindiag_advance_time
    use trindiag, only: trindiag_write_time
    use trindiag, only: write_transport_solution
    use import_export, only: import_geometry
    use trindiag_iteration, only: trindiag_set_iter
    use trindiag_fluxes, only: write_fluxes
    use trindiag_restart, only: write_restart
    use nteqns_arrays, only: write_grids, write_cc_and_perturb
    use trinity_input, only: rad_out_netcdf, nrad_netcdf
    use trinity_input, only: restart_file, flux_reset
    use unit_tests_trin, only: debug_message, verb_main, verb_timestep
    use unit_tests_trin, only: verb_iteration
    use trindiag_text_write, only: dump_tmp_profiles, write_profiles
    use chease_ecom, only: should_update_chease_ecom
    use chease_ecom, only: should_restart_chease_ecom
    use mp_trin, only: abort_mp
    !use mp_trin, only: fork_gryfx
    
    implicit none
    
    !integer, intent(in) :: strlen
    !character(len=strlen), intent(in), optional :: input_file
    !integer, intent(in), optional :: mp_communicator
    type(trinity_type), target, intent(inout) :: trin
    
    !> Local pointers for convenience.
    type(trinity_clock_type), pointer :: clock
    type(trindiag_type), pointer :: gnostics
    type(calibrate_type), pointer :: calib
    type(fluxes_type), pointer :: fx
    integer :: ierror, millisecond_epoch
    real :: epch
    
    clock => trin%clock
    gnostics => trin%gnostics
    calib => trin%calib
    fx => trin%fx
    
    ! initialize message passing
    if (trin%ctrl%mp_comm_external) then 
       write (*,*) 'Initialising message passing, communicator is ', trin%ctrl%mp_comm
       call init_mp(trin%ctrl%mp_comm)
    else
       call init_mp
    end if
    
    !call debug_message(verb_main, 'trinity_main::run_trinity initialised mpi')
    
    clock%first_time  = .true.
    ! initially using global mpi_communicator in which all processors are in one global group
    focus = allprocs
    
    if (proc0) then
       if (nproc == 1) then
          write (*,fmt='(a22,i2,a10)') 'Trinity is running on ', nproc, ' processor'
       else
          write (*,fmt='(a22,i6,a11)') 'Trinity is running on ', nproc, ' processors'
       end if
       write (*,*)
       ! initialize trinity i/o
       if(trin%ctrl%run_name_external) then 
          call init_file_utils(trin%ctrl%run_name)
       else
          call init_file_utils
       end if
       cbuff = trim(run_name)
    end if
    
    call broadcast (cbuff)
    if (.not. proc0) run_name => cbuff

    !call debug_message(verb_main, &
     !'trinity_main::run_trinity initialised file_utils_trin')
    
    ! initialize transport profiles
    call init_nt (clock)

    call debug_message(verb_main, &
     'trinity_main::run_trinity initialised nteqns')

    call init_trindiag(gnostics, .false.)

    call debug_message(verb_main, &
     'trinity_main::run_trinity initialised diagnostics')
    !write(*,*) 'gnostics%reed is', gnostics%reed
    !stop 1
    
    call init_calibrate(calib)
    call debug_message(verb_main, &
     'trinity_main::run_trinity initialised calibration')
    
    if(proc0) then
       write(6,*)
       if(dyn_load_balance) then
          write(6,*) 'Running in dynamic load balanced mode'
       else if(load_balance) then
          write(6,*) 'Running in static load balance mode'
       else
          write(6,*) 'Running with cartesian decompostion'
       endif
    endif
    
    if (fork_flag) then
       ! split processors amongst different flux tubes
       call job_fork (njobs, flux_groups, load_balance)
       call debug_message(verb_main, &
       'trinity_main::run_trinity called job fork')
    end if

    ! distribute profiles amongst different flux tube processor sets
    call distribute_profs (trin%clock%time)
    call debug_message(verb_main, &
         'trinity_main::run_trinity distributed initial profs')
    
    ! allocate flux arrays and broadcast profiles
    ! scope changes to subprocs in init_fluxes
    !    ! obtain fluxes coming from initial profiles
    !    ! scope changes to subprocs and back to allprocs in init_fluxes
    call init_fluxes (fx)
    call debug_message(verb_main, &
         'trinity_main::run_trinity initialised fluxes')

    focus = subprocs
    
    allocate (nt(nprofs_evolved*nrad))
    allocate (nt_i(nprofs_evolved*nrad))
    allocate (nt_m(nprofs_evolved*nrad))
    allocate (nt_mm1(nprofs_evolved*nrad))
    allocate (flx(nprofs_evolved*ncc))
    allocate (flx_m(nprofs_evolved*ncc))
    allocate (H_m(nprofs_evolved*nrad))
    allocate (heat_grid_m(nrad,nspec))

!    if (proc0) then

       ! ! dens_grid, pres_grid, and mom_grid were computed in init_nt
       ! ! fill nt vector with their values
       ! call get_nt_vector_from_profs (dens_grid, pres_grid, mom_grid, nt)
       ! nt_i = nt ; nt_m = nt ; nt_mm1 = nt

       ! ! pflx_cc, qflx_cc, and lflx_cc were computed in init_fluxes
       ! ! fill flx vector with their values
       ! call get_flx_vector_from_fluxes (pflx_cc(:,:,1), qflx_cc(:,:,1), lflx_cc(:,1), flx)
       ! flx_m = flx
       
       ! heat_grid_m = heat_grid

       ! ! transport equations can be written as H(nt) = 0, with
       ! ! nt the vector containing density, pressure, and tor. ang. mom at each grid point
       ! ! get_Hm evaluates H for the dens, pres, mom profiles at time level m
       ! ! H_m is the deviation from the true H (as it should be zero if exact)
       ! call get_Hm (0, nt_i, nt_m, nt_mm1, flx, flx_m, heat_grid, heat_grid_m, H_m)

!    end if

    ! write message to screen stating that the main timestepping is about to begin
    call write_start_message
    
    ! Load calibration factor
    if (init_option_switch.eq.init_option_restart .and. calib%neval_calibrate.gt.0) call calibrate_restart(calib, gnostics)
    call debug_message(verb_main, &
    'trinity_main::run_trinity called calibrate_restart (if appropriate)')

  
    ! Set up the wall clock check if it's not already set
    if (clock%end_wall_clock .lt. 0.0) then
      clock%end_wall_clock = epoch() + avail_cpu_time
    end if

    clock%itstep = 1

    error_flag = .false.

    do while (clock%itstep .le. ntstep)
       old_time = clock%time
       ! EGH put this at the beginning in case of restarting with a different eq.
       call debug_message(verb_timestep, &
            'trinity_main::run_trinity starting timestep')
       ! only needed for special cases when running with CHEASE or ECOM
       call update_chease_ecom_check
       
       ! iterate within each transport time step
       ! until niter (specified in input file) is reached
       do while (clock%iter <= niter)
          if (error_flag) then
             ! We are restarting the iteration
             call debug_message(verb_iteration, &
                  'trinity_main::run_trinity restarted iteration')
          else
             call debug_message(verb_iteration, &
                  'trinity_main::run_trinity started iteration')
          end if
          
          ! dump profs to file in case need to restart in middle of timestep
          ! will need to use ".tmp" file instead of ".plot" file for restart
          if (fork_flag) then
             call scope (allprocs)
             focus = allprocs
          end if
          call convert_time(clock)
          if (proc0) call dump_tmp_profiles (clock)
          call trindiag_set_iter(gnostics, clock%iter)
          ! Write current transport solution to file, just so we
          ! can see what's going on during the call to
          ! get_fluxes
          call write_transport_solution(gnostics, fx%flx, trin%globals, &
               nrad_netcdf, rad_out_netcdf)
          if (clock%iter==1) call trindiag_write_time(gnostics, clock)
          ! Write profiles and other information to file before starting
          ! flux call, needed when restarting. 
          call write_restart(gnostics, clock, nrad_netcdf, rad_out_netcdf)
          if (fork_flag) then
             call scope (subprocs)
             focus = subprocs
          end if
          
          if (flux_reset .or. .not. error_flag) then 
             ! If we are not restarting the iteration we 
             ! calculate the fluxes.
             ! If we are restarting the iteration and flux_reset is true
             ! calculate the fluxes.
             
             fx%flx_old%error_flag = error_flag
             
             ! This next clause is only for the experimental 
             ! calibration mechanism
             call calibration_check

             ! Loop over the flux call until the fluxes are converged.
             ! Simple flux modules like ifspppl, TGLF, are only called once.
             ! The convergence criteria are complex and subjective: no one 
             ! prescription is foolproof. See note by EGH, 'Convergence Criteria
             ! when using Trinity with Gryfx'
             call get_converged_fluxes
             
             if (calib%neval_calibrate .gt. 0 .and. proc0) call calibrate_adjust(calib, fx%flx)
             
          end if ! flux_reset .or. .not. error_flag
          
          ! if re-doing timestep because error too large,
          ! and flux_reset is false
          ! need to switch focus to allprocs
          if (focus == subprocs) then
             call scope (allprocs)
             focus = allprocs
          end if

          call write_fluxes(gnostics, fx%flx)
          ! We need to duplicate the call to write_transport_solution
          ! because if the iteration was restarted we won't pass through
          ! the earlier call.
          !call write_transport_solution(gnostics, fx%flx, trin%globals, &
          !nrad_netcdf, rad_out_netcdf)
          !call convert_time(clock)
          !if (clock%iter==1) call trindiag_write_time(gnostics, clock)
          
          ! write profiles to file and let user know which transport time step we are in
          if (proc0) then
             if (clock%iter == 1) then
                if (.not. error_flag) call write_profiles (clock, fx%flx, .false.)
                if (.not. functional_test_flag) call print_timestep_info (clock)
                if (.not. functional_test_flag) call print_solution_status (trin)
             end if
          end if
          
          ! evolve equilibrium profiles
          call advance_profiles (trin, converged, error_flag, &
               trin%ctrl%steady_state_converged)

          call debug_message(verb_iteration, &
               'trinity_main::run_trinity called advance_profiles')
          
          if (fork_flag) then
             call scope (subprocs)
             focus = subprocs
             call broadcast_profiles
          end if
          
          if (error_flag) then
             clock%iter = 1
          else if (converged) then
             exit
          else
             clock%iter = clock%iter + 1
          end if
          
       end do  ! iter loop
       if (fork_flag) then
          call scope (allprocs)
          focus = allprocs
          !call broadcast_profiles
       end if
       
       clock%iter = 1
       clock%itstep_tot = clock%itstep_tot + 1
       
       if (proc0) call trindiag_advance_time(gnostics)
       
       call debug_message(verb_iteration, &
            'trinity_main::run_trinity called advance_time')
       
       if (fork_flag) then
          call scope (subprocs)
          focus = subprocs
       end if
       
       if (trin%ctrl%steady_state_converged .and. &
            .not. (nifspppl_initial .gt. clock%itstep - 2)) then 
          exit
       end if
       
       clock%itstep = clock%itstep + 1
       
    end do
    
    if (fork_flag) then
       call scope (allprocs)
       focus = allprocs
    end if

  contains

    subroutine write_start_message
      if (fork_flag) then
         call scope (allprocs)
         focus = allprocs
      end if
      if (proc0) then
         write (*,*)
         write (*,*) "-----------------------"
         write (*,*) "Starting time step loop"
         write (*,*) "-----------------------"
         write (*,*)
      end if
      if (fork_flag) then
         call scope (subprocs)
         focus = subprocs
      end if
    end subroutine write_start_message

    subroutine update_chease_ecom_check
      if (mod(clock%itstep,1).eq.0) then
         
         if (should_update_chease_ecom(trin%checom)) then
            if (fork_flag) then
               call scope (allprocs)
               focus = allprocs
            end if
            if (proc0) then
               if (should_restart_chease_ecom(trin%checom)) &
                    call restart_chease_ecom(trin%checom, restart_file)
               ierror = update_chease(trin%checom, evolve_geometry)
            end if
            call write_chease_ecom(gnostics, trin%checom)
            ! We duplicate the call to write_restart which appears later in 
            ! its proper place: this is to help us debug if chease/ecom fails.
            call write_restart(gnostics, clock, nrad_netcdf, rad_out_netcdf)
            call broadcast(ierror)
            if (ierror.ne.0) call abort_mp
            call import_geometry
            call broadcast_grids('geo')
            ! This call assumes that the '_grid' variables always contain more
            ! up-to-date information than the '_cc' variables (with the 
            ! exception of the fluxes which are handled separately).
            call get_cc_all
            call debug_message(verb_timestep, &
                 'trinity_main::run_trinity called update_chease')
            if (fork_flag) then
               call scope (subprocs)
               focus = subprocs
            end if
         end if
      end if
    end subroutine update_chease_ecom_check

    subroutine get_converged_fluxes

      ! Loop over the flux call until the fluxes are converged.
      ! Simple flux modules like ifspppl, TGLF, are only called once.
      ! The convergence criteria are complex and subjective: no one 
      ! prescription is foolproof. See note by EGH, 'Convergence Criteria
      ! when using Trinity with Gryfx'
      do while (.not. fluxes_converged(fx))
         
         ! Here we exit run_trinity abruptly if we have exceeded the available
         ! wall clock time. We place the call here so we don't start another
         ! flux evaluation that is unlikely to finish. Exiting here also 
         ! leaves the diagnostics in the right place, and leaves all the
         ! counters like itstep exactly as if we had finished trinity 
         ! at the end of the previous timestep.
         epch = epoch()
         if (clock%end_wall_clock .gt. 0.0 .and. &
              epch .gt. clock%end_wall_clock) then
            if (proc0) write (*,*) 'Exceeded available time'
            return
         else if (proc0) then
            !write (*,*) 'Wall clock: ', epch, &
            !' End wall clock: ', clock%end_wall_clock
         end if
         
         if (fork_flag) then
            call scope (subprocs)
            focus = subprocs
         end if
         call debug_message(verb_iteration, &
              'trinity_main::run_trinity calling get_fluxes')
         
         ! obtain turbulent fluxes and heating
         ! scope changes to allprocs by end of get_fluxes
         !do while (.true.)
         call get_fluxes (fx, clock, calib, "calculate", gnostics)
         !end do
         
         call debug_message(verb_iteration, &
              'trinity_main::run_trinity called get_fluxes')
         
         ! This line is used by the test suite to make sure a restart from a crash
         ! works as expected
         if (check_restart_from_crash .and. clock%itstep == 4 .and. clock%iter == 2) stop 1
         
         ! If the flux_driver flag is set we merely calculate the fluxes and
         ! exit run_trinity; in this case Trinity is being used a driver 
         ! for the flux code
         if (flux_driver) return
         
         !call calibrate_adjust(calib)
         focus = allprocs
         clock%neval = clock%neval + 1
      end do
      
    end subroutine get_converged_fluxes

    subroutine calibration_check
      if (calib%neval_calibrate .gt. 0 .and. &
           (mod(clock%neval, calib%neval_calibrate) .eq. 0 .or. &
           .not. calib%calibrated)) then 
         call get_fluxes(fx, clock, calib, "reduced", gnostics)
         call calibrate_store(calib, "reduced", gnostics, fx%flx)
         !stop 1
         if (fork_flag) then
            call scope (subprocs)
            focus = subprocs
         end if
         call get_fluxes(fx, clock, calib, "calibrate", gnostics)
         call calibrate_store(calib, "calibrate", gnostics, fx%flx)
         if (fork_flag) then
            call scope (subprocs)
            focus = subprocs
         end if
      end if
    end subroutine calibration_check

  end subroutine run_trinity

  subroutine finish_trinity(trin)

    use file_utils_trin, only: finish_file_utils
    use trinity_input, only: finish_trinity_input
    use nteqns, only: finish_nteqns
    use mp_trin, only: proc0
    use expt_profs, only: init_iter_db_io_reset
    use trindiag, only: finish_trindiag
    use trindiag, only: trindiag_write_time
    use trindiag, only: write_transport_solution
    use trinity_input,only: nrad_netcdf, rad_out_netcdf, evolve_geometry
    use chease_ecom, only: finish_chease_ecom
    use trindiag_restart, only: write_restart
    use trindiag_iteration, only: trindiag_set_iter
    use trindiag_text_write, only: dump_tmp_profiles, write_profiles
    use save_arrays, only: finalize_save_arrays
    use trindiag_iteration, only: write_convergeval

    implicit none

    type(trinity_type), intent(inout), target ::  trin

    !> Local pointer for convenience
    type(trindiag_type), pointer :: gnostics
    integer :: ierror



    gnostics=>trin%gnostics


    call trindiag_set_iter(gnostics, trin%clock%iter)
    call write_transport_solution(gnostics, trin%fx%flx, trin%globals, &
      nrad_netcdf, rad_out_netcdf)
    call write_restart(gnostics, trin%clock, nrad_netcdf, rad_out_netcdf)
    call convert_time(trin%clock)
    call trindiag_write_time(gnostics, trin%clock)
    call write_convergeval(trin%gnostics, trin%ctrl%convergeval)
    !if (proc0) call trindiag_advance_time(gnostics, time)

    ! If we have been updating chease/ecom then do it for the final
    ! profiles
    if (trin%checom%initialized) then 
      if (proc0) ierror = update_chease(trin%checom, evolve_geometry)
      call write_chease_ecom(gnostics, trin%checom)
      call finish_chease_ecom(trin%checom)
    end if
    
    if (proc0) then
       call write_profiles (trin%clock, trin%fx%flx, .true.)
       call dump_tmp_profiles (trin%clock)
       call finish_file_utils
       write (*,*)
       write (*,*) '-------------------------------'
       write (*,*) '------ Trinity finished -------'
       write (*,*) '-------------------------------'
       write (*,*)
    end if
    call finish_trindiag(gnostics)

    deallocate (nt, nt_i, nt_m, nt_mm1)
    deallocate (flx, flx_m)
    deallocate (heat_grid_m)
    deallocate (H_m)

    init_iter_db_io_reset = .true.
    trin%clock%first_write = .true.
    check_error_reset = .true.
    trin%clock%first_time = .true.
    error_flag = .false.
    if (trin%sayv%initialized) call finalize_save_arrays(trin%sayv)
    if (trin%fx%initialized) call finish_fluxes(trin%fx)
    call finish_trinity_input
    call finish_nteqns
    if (.not. trin%ctrl%mp_comm_external) call finish_mp

  end subroutine finish_trinity

  subroutine advance_profiles (trin,  conv_flag, eflag, &
     steady_state_converged)
    
    !use calibrate, only: calibrate_restart
    use mp_trin, only: proc0, broadcast, abort_mp, scope, allprocs
    use trinity_input, only: nrad, nspec, ntdelt, njac, ncc
    use trinity_input, only: init_option_switch, init_option_restart
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: evolve_boundary
    use trinity_input, only: geo_option_switch, geo_option_transp
    use linsolve, only: lusolve
    use interp, only: get_gridval
    use nteqns, only: get_nt_vector_from_profs
    use nteqns, only: get_profs_from_nt_vector
    use nteqns, only: get_flux_grads, get_grads, get_nt_coefs, distribute_profs
    use nteqns_arrays, only: dens_grid, pres_grid, mom_grid
    use nteqns, only: get_delgrads
    use trindiag_iteration, only: write_matrix_solve, write_current_solution
    use trindiag_iteration, only: write_convergeval
    use trindiag_fluxgrads, only: write_fluxgrads
    use trindiag_restart, only: write_iteration, restart_iteration
    use trinity_input, only: convergetol, restart_file
    use file_utils_trin, only: error_unit
    use trindiag_text_write, only: read_save_arrays, write_save_arrays
    use save_arrays, only: initialize_save_arrays
    use trinity_input, only: deltadj
    use import_export, only: update_boundary, import_geometry
    use nteqns_arrays, only: rad_grid

    implicit none

    type(trinity_type), intent(inout), target ::  trin
    type(save_arrays_type), pointer :: sayv
    type(flux_results_type), pointer :: flx
    integer, pointer :: iter, step
    real, pointer :: nttime
    logical, intent (out) :: conv_flag, eflag
    logical, intent (out) :: steady_state_converged

    integer :: is, ig, ierr
    integer :: np
    real, dimension (:,:), allocatable :: psimat
    !real :: convergeval

    ! Assign local convenience pointers
    iter => trin%clock%iter
    step => trin%clock%itstep
    nttime => trin%clock%time
    sayv => trin%sayv
    flx => trin%fx%flx

    np = nprofs_evolved

    allocate (psimat(np*nrad,np*nrad))

    ! nt gets time level m+1, iteration i of evolved profiles
    ! note that (m+1,i=0) = m here (zeroth iteration is old time step)
    call get_nt_vector_from_profs (dens_grid, pres_grid, mom_grid, nt)

    if (.not. sayv%initialized) call initialize_save_arrays(sayv)
       
    if (trin%clock%first_time .and. &
         (init_option_switch == init_option_restart)) then
       if (restart_file /= 'old') then
          call restart_iteration(restart_file, sayv)
       else
          call read_save_arrays(sayv)
       end if
       if (iter == 1) then
          sayv%ntold = sayv%nt
          sayv%pflxold = sayv%pflx ; sayv%qflxold = sayv%qflx
          sayv%heatold = sayv%heat ; sayv%lflxold = sayv%lflx
          sayv%nt = nt
          nttime = nttime + ntdelt 
       end if
       do is = 1, nspec
          sayv%flx((is-1)*ncc+1:is*ncc) = sayv%pflx(:,is,1)
          sayv%flx((nspec+is-1)*ncc+1:(nspec+is)*ncc) = sayv%qflx(:,is,1)
          call get_gridval (sayv%heat(:,is,1), sayv%heatgrid(:,is))
       end do
       sayv%flx(2*nspec*ncc+1:(2*nspec+1)*ncc) = sayv%lflx(:,1)
    else if (iter == 1) then
       if (step == 1) then
          sayv%ntold = nt
          sayv%pflxold = flx%p_cc ; sayv%qflxold = flx%q_cc
          sayv%heatold = flx%heat_cc ; sayv%lflxold = flx%l_cc
       else
          sayv%ntold = sayv%nt
          sayv%pflxold = sayv%pflx ; sayv%qflxold = sayv%qflx
          sayv%heatold = sayv%heat ; sayv%lflxold = sayv%lflx
       end if
       if (evolve_boundary) then
          call update_boundary (nttime)
          call get_nt_vector_from_profs (dens_grid, pres_grid, mom_grid, nt)
       end if
       if (evolve_geometry .and. geo_option_switch == geo_option_transp) then
          call import_geometry (nttime)
       end if
       
       ! save dens, pres, and flux profiles in case time step is too large
       ! and must repeat time step
       sayv%nt = nt
       sayv%pflx = flx%p_cc ; sayv%qflx = flx%q_cc ; sayv%heat = flx%heat_cc ; sayv%lflx = flx%l_cc
       nttime = nttime + ntdelt
       
       do is = 1, nspec
          sayv%flx((is-1)*ncc+1:is*ncc) = sayv%pflx(:,is,1)
          sayv%flx((nspec+is-1)*ncc+1:(nspec+is)*ncc) = sayv%qflx(:,is,1)
          call get_gridval (sayv%heat(:,is,1), sayv%heatgrid(:,is))
       end do
       sayv%flx(2*nspec*ncc+1:(2*nspec+1)*ncc) = sayv%lflx(:,1)
       
    end if

    trin%clock%first_time = .false.

    if (proc0) then
       call get_flux_grads (flx%p_cc, flx%q_cc, flx%heat_cc, flx%l_cc)
       call write_fluxgrads(trin%gnostics)
       
       ! pass heat_grid instead of heat_cc because it enters as source instead of derivative
       call get_nt_coefs (flx%p_cc(:,:,1), flx%q_cc(:,:,1), flx%heat_grid, flx%l_cc(:,1), &
            psimat, nt, nttime, trin%clock%itstep_tot, sayv%nt, sayv%ntold, sayv%flx, sayv%heatgrid)
       
       ! do ix = 1, size(nt)/2
       !    write (*,*) 'dens', ix, nt(ix)
       ! end do
       ! do ix = size(nt)/2+1, size(nt)
       !    write (*,*) 'temp', ix, nt(ix)/nt(ix-size(nt)/2)
       ! end do
       ! write (*,*)

       ! SEEMS TO WORK BUT KEEP AN EYE ON IT -- MAB
!       if (iter == 1) then
!          sayv%rhs = nt
!       else
!          nt = sayv%rhs
!       end if

       call write_matrix_solve(trin%gnostics, psimat, psimat, nt)  
       
       ierr = lusolve (psimat, nt)
       
       if (ierr > 0) then
          write (error_unit(), *) 'ERROR: failure in lusolve, return code: ', ierr
          call abort_mp
       end if
       
       call write_current_solution(trin%gnostics, nt)
       
       call check_error (trin%clock, nt, conv_flag, eflag, sayv%rmserr)
       
       ! do not update dens_grid, pres_grid, and mom_grid
       ! until after check_error, as they are assumed
       ! to be values from previous iteration in check_error
       call get_profs_from_nt_vector (nt, dens_grid, pres_grid, mom_grid)
       
       trin%ctrl%convergeval = sum(abs(nt(:)-sayv%ntold(:))/nt(:))/size(nt(:)) / max(ntdelt,0.0001)
       
       if (step>2 .and. convergetol > 0.0) then 
          steady_state_converged = trin%ctrl%convergeval < convergetol
          write (*,*) 'steady_state_converged', steady_state_converged, &
               trin%ctrl%convergeval
       else
          steady_state_converged = .false.
       end if
       
       call write_convergeval(trin%gnostics, trin%ctrl%convergeval)
       
       ! Write all info needed to restart current iteration to the new netcdf file
       call write_iteration(trin%gnostics, sayv)
       
       if (eflag) then
          nt = sayv%nt
          call get_profs_from_nt_vector (nt, dens_grid, pres_grid, mom_grid)
          flx%p_cc = sayv%pflx ; flx%q_cc = sayv%qflx ; flx%heat_cc = sayv%heat ; flx%l_cc = sayv%lflx
          nttime = nttime - deltadj*ntdelt
       end if
       
       ! write sayv%nt, sayv%ntold, sayv%rhs to file in case
       ! need to restart from middle of timestep
       ! This function outputs the info into the deprecated text restart files.
       call write_save_arrays(sayv)
       
       call get_grads (dens_grid, pres_grid, mom_grid)
       call get_delgrads (dens_grid, pres_grid, mom_grid)
       
    end if
    
    call scope(allprocs)
    call distribute_profs (nttime)

    call broadcast (steady_state_converged)
    call broadcast (nttime)
    call broadcast (eflag)
    call broadcast (conv_flag)
    call broadcast (trin%clock%first_time)

    do ig = 1, njac
       do is = 1, nspec
          call broadcast (flx%p_cc(:,is,ig))
          call broadcast (flx%q_cc(:,is,ig))
          call broadcast (flx%heat_cc(:,is,ig))
       end do
       call broadcast (flx%l_cc(:,ig))
    end do

    deallocate (psimat)
    
  end subroutine advance_profiles

  subroutine check_error (clock, nt, cflag, errflag, errrms)

    use file_utils_trin, only: open_output_file, flush_output_file, close_output_file
    use trinity_input, only: nrad, errtol, errflr, niter, ntdelt, ntdelt_max, flrfac
    use trinity_input, only: nprofs_evolved
    use trinity_input, only: nspec, nrad, deltadj
    use trinity_input, only: single_radius
    use nteqns, only: get_profs_from_nt_vector
    use nteqns_arrays, only: dens_grid, pres_grid, mom_grid
    use trinity_time, only: ntdelt_old

    implicit none

    !integer, intent (in) :: iter
    type(trinity_clock_type), intent(inout), target :: clock
    integer, pointer :: iter, itstep
    real, dimension (:), intent (in) :: nt
    real, intent (in out) :: errrms
    logical, intent (out) :: cflag, errflag

    integer, save :: time_unit
    integer :: maxrad_idx
    real :: max_err
    real, dimension (:,:), allocatable :: dens, pres
    real, dimension (:), allocatable :: mom
    real, dimension (:), allocatable, save :: rms_err, mom_err
    real, dimension (:,:), allocatable, save :: n_err, p_err

    ! don't want to use the boundary grid point (which is
    ! set by boundary condition) in error estimates
    maxrad_idx = nrad

    ! Assign convenience pointers
    iter => clock%iter
    itstep => clock%itstep

    if (check_error_reset) then
      deallocate(n_err, p_err, mom_err, rms_err)
      call close_output_file(time_unit)
    end if

    if (.not. allocated(n_err)) then
       allocate (n_err(niter,nspec))
       allocate (p_err(niter,nspec))
       allocate (mom_err(niter))
       allocate (rms_err(niter))
       call open_output_file (time_unit,".time")
       write (time_unit,'(2a8,5a10)') 'iter', 'itstep', 'max_err', 'rms_err', &
            'errtol', 'errflr', 'ntdelt'
    end if

    allocate (dens(nrad,nspec))
    allocate (pres(nrad,nspec))
    allocate (mom(nrad))

    ! if restarting in middle of timestep, use rms error from
    ! previous iteration from .iter file
    if (errrms > 0.0 .and. iter>1) rms_err(iter-1)=errrms

    errflag = .false. ; cflag = .false.
    
    ! calculate max relative difference (max over differences for all radii) 
    ! between n, pi, and pe for this iteration and for previous iteration
!    n_err(iter) = maxval(abs((nt(:nrad)-dens_grid)/dens_grid))
!    pi_err(iter) = maxval(abs((nt(nrad+1:2*nrad)-pres_grid(:,1))/pres_grid(:,1)))
!    pe_err(iter) = maxval(abs((nt(2*nrad+1:3*nrad)-pres_grid(:,2))/pres_grid(:,2)))

    call get_profs_from_nt_vector (nt, dens, pres, mom)
    
    !if (.false. .and. single_radius>0) then
      !do ix = 1,nrad
        !if (ix .lt. single_radius .or. ix .gt. single_radius+1) then
          !dens(ix,:) = dens_grid(ix,:)
          !pres(ix,:) = pres_grid(ix,:)
          !mom(ix) = mom_grid(ix)
        !end if
      !end do
    !end if

    ! calculate rms relative difference (averaged over all radii)
    ! between n, pi, and pe for this iteration and for previous iteration
    if (single_radius>0) then
      n_err(iter,:) = sqrt(maxval( ( (dens(:maxrad_idx,:)-dens_grid(:maxrad_idx,:)) &
           /dens_grid(:maxrad_idx,:) )**2 , 1 ))
      p_err(iter,:) = sqrt(maxval( ( (pres(:maxrad_idx,:)-pres_grid(:maxrad_idx,:)) &
           /pres_grid(:maxrad_idx,:) )**2 , 1 ))
      if (minval(abs(mom_grid)) > epsilon(0.0)) then
         mom_err(iter) = sqrt(maxval( ( (mom(:maxrad_idx)-mom_grid(:maxrad_idx)) &
              /mom_grid(:maxrad_idx) )**2 ))
      else
         mom_err(iter) = 0.0 ! need to fix later -- MAB
      end if
    else 
      n_err(iter,:) = sqrt(sum( ( (dens(:maxrad_idx,:)-dens_grid(:maxrad_idx,:)) &
           /dens_grid(:maxrad_idx,:) )**2 , 1 )/maxrad_idx)
      p_err(iter,:) = sqrt(sum( ( (pres(:maxrad_idx,:)-pres_grid(:maxrad_idx,:)) &
           /pres_grid(:maxrad_idx,:) )**2 , 1 )/maxrad_idx)
      if (minval(abs(mom_grid)) > epsilon(0.0)) then
         mom_err(iter) = sqrt(sum( ( (mom(:maxrad_idx)-mom_grid(:maxrad_idx)) &
              /mom_grid(:maxrad_idx) )**2 )/maxrad_idx)
      else
         mom_err(iter) = 0.0 ! need to fix later -- MAB
      end if
    end if

    ! get maximum of relative errors
    max_err = max(maxval(n_err(iter,:)), maxval(p_err(iter,:)), mom_err(iter))
    ! get rms of relative errors
    rms_err(iter) = sqrt(sum(n_err(iter,:)**2+p_err(iter,:)**2)+mom_err(iter)**2)/real(nprofs_evolved)

    ! set errrms, which is written to file in case need to restart in middle of time step
    errrms = rms_err(iter)

    write (time_unit,401) iter, itstep, max_err, rms_err(iter), errtol, errflr, ntdelt
    call flush_output_file (time_unit)
401 format (2(1x,i7),5(1x,1e10.3))

    ! if the density or pressure profiles are negative at any point,
    ! need to redo with smaller time step
    if (any(dens < 0.0) .or. any(pres < 0.0)) then
       errflag = .true.
       write (*,*)
       write (*,*)
       write (*,'(a39)') "One of the profiles has gone negative."
       write (*,'(a22,1pg11.4)') "Changing ntdelt from ", ntdelt
       write (*,'(a5,1pg11.4)') " to ", ntdelt/deltadj
       write (*,'(a25)') " and repeating timestep."
    end if
    if (iter > 1) then
! TMP FOR TESTING -- MAB
!       if (rms_err(iter) > 2.*rms_err(iter-1)+epsilon(0.) .and. rms_err(iter) > errflr) then
!          errflag = .true.
!          write (*,*)
!          write (*,'(a37)') "RMS error increasing upon iteration."
!          write (*,'(a22,1pg11.4)') "Changing ntdelt from ", ntdelt
!          write (*,'(a5,1pg11.4)') " to ", ntdelt/deltadj
!          write (*,'(a25)') " and repeating timestep."
!       end if
    end if
    if (iter == niter) then
       if (rms_err(iter) > errtol) then
          errflag = .true.
          write (*,*)
          write (*,*) "RMS relative error in profiles, ", rms_err(iter), &
               " greater than error tolerance, ", errtol, " after ", niter, " iterations."
          write (*,*) "Changing ntdelt from ", ntdelt, " to ", ntdelt/deltadj, &
               " and repeating timestep."
       end if
    end if
    if (rms_err(iter) < errflr) then
       ntdelt_old = ntdelt
       cflag = .true.
       if (ntdelt < ntdelt_max .and. rms_err(iter) < errflr/flrfac) then
          write (*,*)
          write (*,'(a33,1pg11.4,a1)') "RMS relative error in profiles, ", rms_err(iter), ","
          write (*,'(a38,1pg11.4,a1)') "smaller than prescribed error floor, ", errflr, ","
          write (*,'(a7,i2,a12)') "after ", iter, " iterations."
          if (ntdelt*2. <= ntdelt_max) then
             write (*,'(a24,1pg11.4,a4,1pg11.4,a19)') "Increasing ntdelt from ", &
                  ntdelt, " to ", ntdelt*2., " for next timestep."
             ntdelt = ntdelt*2.
          else
             write (*,'(a24,1pg11.4,a4,1pg11.4,a19)') "Increasing ntdelt from ", &
                  ntdelt, " to ", ntdelt_max, " for next timestep."
             ntdelt = ntdelt_max
          end if
       end if
    end if

    if (errflag) ntdelt = ntdelt/deltadj

    deallocate (dens, pres, mom)

  end subroutine check_error

  ! converts from normalized time to time in seconds
  subroutine convert_time (clock)
    use trinity_input, only: vtfac
    use trinity_input, only: m_ion
    use norms, only: aref
    implicit none
    type(trinity_clock_type), intent(inout) :: clock
    clock%seconds = clock%time*0.31*aref**3/(sqrt(m_ion(1))*vtfac**1.5)
  end subroutine convert_time

  subroutine print_solution_status(trin)
    type(trinity_type), intent(inout), target :: trin
    type(globals_type), pointer :: globals
    globals => trin%globals
    write (*,'(a7,e9.2,a5,e9.2,a6,e9.2,a2,e9.2,a6,e9.2,a2,e9.2)') &
      'pfus: ', globals%fusion_power,&
      ', Q: ', globals%fusion_gain, &
      ', Ti: ', globals%ti_core, '--', globals%ti_edge, &
      ', ne: ', globals%ne_core, '--', globals%ne_edge
  end subroutine print_solution_status

  subroutine print_timestep_info (clock)
    use trinity_input, only: ntstep

    implicit none
    type(trinity_clock_type), intent(in) :: clock

    integer :: idx, ndt, nflx
    idx = clock%itstep
    ndt = ntstep
    nflx = clock%neval

    write (*,*)
    if (ndt < 10) then
       write (*,'(a9,i1,a10,i1,a9,i2)') 'itstep= ', idx, ', ntstep= ', ndt, ', neval= ', nflx
    else if (ndt < 100) then
       write (*,'(a9,i2,a10,i2,a9,i3)') 'itstep= ', idx, ', ntstep= ', ndt, ', neval= ', nflx
    else if (ndt < 1000) then
       write (*,'(a9,i3,a10,i3,a9,i4)') 'itstep= ', idx, ', ntstep= ', ndt, ', neval= ', nflx
    else
       write (*,'(a9,i6,a10,i6,a9,i7)') 'itstep= ', idx, ', ntstep= ', ndt, ', neval= ', nflx
    end if

  end subroutine print_timestep_info


end module trinity_main


!> A simplified function for calling Trinity either from a C program
!! or from the CodeRunner optimisation framework. 
!! strlen is the length of the input file, input_file is the relative
!! or absolute path of the input_file, mp_communicator is the
!! (integer-represented) MPI communicator or sub communicator to be used
!! by Trinity and end_time is the time in seconds since the epoch before
!! which the simulation must end (set to -1.0 to disable)
subroutine run_trinity_external(strlen, input_file, mp_communicator) 
    !bind(c, name = 'run_trinity_external')
  use iso_c_binding
  use trinity_main, only: run_trinity, finish_trinity, trinity_type

  implicit none

  integer, intent(in) :: strlen
  character(len=strlen), intent(in) :: input_file
  integer(c_int), intent(in) :: mp_communicator

  type(trinity_type) :: trin

  trin%ctrl%mp_comm_external = .true.
  trin%ctrl%mp_comm = mp_communicator
  trin%ctrl%run_name_external = .true.
  trin%ctrl%run_name = trim(input_file)

  call run_trinity(trin)
  call finish_trinity(trin)

end subroutine run_trinity_external

subroutine run_trinity_test() bind(c, name='run_trinity_test')
  use trinity_main, only: run_trinity, finish_trinity, trinity_type
  type(trinity_type) :: trin
  call run_trinity(trin)
  call finish_trinity(trin)
end subroutine run_trinity_test
