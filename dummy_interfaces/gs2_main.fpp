module store
  real, dimension(10) :: qflux_store
  real :: q_store
end module store
# ifndef OLD_GS2_INTERFACE

module gs2_main
  
  public :: run_gs2, finish_gs2, reset_gs2, trin_finish_gs2
 
contains
# endif


subroutine run_gs2 (mpi_comm, job_id, filename, nensembles, &
     pflux, qflux, vflux, heat, dvdrho, grho, trinity_reset, converged)

   use store
   
    implicit none

    integer, intent (in), optional :: mpi_comm, job_id, nensembles
    character (*), intent (in), optional :: filename
    real, dimension (:), intent (out), optional :: pflux, qflux, heat
    real, intent (out), optional :: vflux
    real, intent (out), optional :: dvdrho, grho
    logical, intent (in), optional :: trinity_reset
    logical, intent (out), optional :: converged

    dvdrho = 0.5
    grho = 1.0
    qflux(:) = q_store !qflux_store(1:size(qflux))**0.87
    heat(:) = q_store**0.5 ! qflux_store(1:size(qflux))**0.91
    pflux = 0.0 ! qflux**2.0/2.0 
    vflux = 0.0 !sum(qflux)**0.45
    !write(*,*) 'job_id', job_id, 'qflux', qflux, 'qflux_store', qflux_store
    !qflux(1) = qval_store
    !qflux(2) = 1.0
    !qflux(3) = 0.0
    !converged = .true.


    
  end subroutine run_gs2
  
  subroutine trin_finish_gs2
  end subroutine trin_finish_gs2


  subroutine finish_gs2
    

  end subroutine finish_gs2
  

  subroutine reset_gs2 (ntspec, dens, temp, fprim, tprim, gexb, mach, nu, nensembles)


    use store
    implicit none

    integer, intent (in) :: ntspec, nensembles
    real, intent (in) :: gexb, mach
    real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu

    !write (*,*) 'qqq', tprim, dens
    qflux_store(1:size(tprim)) =  &
      tprim(1) * dens(:) * temp(:) * fprim(:) * tprim(:)**2.0
    qflux_store(2) = qflux_store(2)*q_store**2.0 + 1.0


  end subroutine reset_gs2

  subroutine gs2_trin_init (rhoc, qval, shat, rgeo_lcfs, rgeo_local, kap, kappri, tri, tripri, shift, &
       betaprim, ntspec, dens, temp, fprim, tprim, gexb, mach, nu, use_gs2_geo)


    use store
    implicit none

    integer, intent (in) :: ntspec
    real, intent (in) :: rhoc, qval, shat, rgeo_lcfs, rgeo_local, kap, kappri, tri, tripri, shift
    real, intent (in) :: betaprim, gexb, mach
    real, dimension (:), intent (in) :: dens, fprim, temp, tprim, nu
    logical, intent (in) :: use_gs2_geo

    qflux_store(1:size(tprim)) = &
      tprim(1) * dens(:) * temp(:) * fprim(:) * tprim(:)**2.0 
    q_store = qval
    qflux_store(2) = qflux_store(2)*q_store**2.0 + 1.0

    
  end subroutine gs2_trin_init

# ifndef OLD_GS2_INTERFACE
end module gs2_main
# endif
