#include "gryfx_lib.h"
#include "stdio.h"
void gryfx_get_default_parameters_(struct gryfx_parameters_struct * pars,  char * namelistFile  ){

  pars->job_id = 0;

};
void gryfx_get_fluxes_(struct gryfx_parameters_struct * pars, struct gryfx_outputs_struct* outs){
	/*outs->pflux[0] = pars->rhoc;*/
  if (pars->mpirank == 0) {
    outs->qflux[0] = pars->qinp;
    outs->qflux[1] = 1.0;
  }
  else {
    outs->qflux[0] = 0.0;
    outs->qflux[1] = 0.0;
  }
  /*printf("Hello! %f\n %f", pars->qinp, pars->rhoc);*/
};
void gryfx_finish_(struct gryfx_parameters_struct * pars){
};
